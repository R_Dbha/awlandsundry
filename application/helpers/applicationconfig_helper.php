<?php
/**
 * Description of applicationconfig_helper
 *
 * @author kraftlabs
 */
class ApplicationConfig {
    const IMG_BASE = "https://www.awlandsundry.com/files/images/";
    const ROOT_BASE = "https://www.awlandsundry.com/";
    const TOE_PART = "P0";
    const VAMP_PART = "P1";
    const EYESTAY_PART = "P2";
    const FOXING_PART = "P3";
    const IMG_WIDTH = 720;
    const IMG_HEIGHT = 405;
    const THUMB_WIDTH = 298;
    const THUMB_HEIGHT = 167;
    
    //--Monogram configs //
    const LEFT_SHOE = "left";
    const RIGHT_SHOE = "right";
    const LEFT_SIDE = "leftheel";
    const RIGHT_SIDE = "rightheel";
    const CHAR_WIDTH = 18;
    const A4_TOP = 175;
    const A4_LEFT = 520;
    const A0_TOP = 175;
    const A0_LEFT = 150;
    const DEFAULT_ANGLE = "A0";
    const EXTENTION = "png";
    const DEF_MAT = 'M0';
    const DEF_COLOR = 'C0';
    const DEF_MAT_ID = 1;
    const DEF_COLOR_ID = 1;
    const DEF_MAT_FOLDER = 'CP';
}

?>
