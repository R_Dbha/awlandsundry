<?php

function addCustomerToMailChimp($data) {
    $CI = & get_instance();

    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $result = $CI->mailchimp->post('ecommerce/stores/' . $storeId . '/customers', array(
        'id' => $data['user_id'] . '',
        'email_address' => $data['email'],
        'opt_in_status' => true,
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
    ));

    return $result;
}

function updateCustomerToMailChimp($data) {
    $CI = & get_instance();

    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $result = $CI->mailchimp->put('ecommerce/stores/' . $storeId . '/customers/' . $data['user_id'], array(
        'id' => $data['user_id'] . '',
        'email_address' => $data['email'],
        'opt_in_status' => true,
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
    ));

    return $result;
}

function updateCustomerAddressToMailChimp($data) {
    $CI = & get_instance();

    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $result = $CI->mailchimp->put('ecommerce/stores/' . $storeId . '/customers/' . $data['user_id'], array(
        'id' => $data['user_id'] . '',
        'opt_in_status' => true,
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
        'address' => array(
            'address1' => $data['address1'],
            'address2' => $data['address2'],
            'city' => $data['city'],
            'province' => $data['state'],
            'postal_code' => $data['zipcode'],
            'country' => $data['country']
        )
    ));

    return $result;
}

//*******************************************************************************************

function addCartToDB($data, $prodId, $sessData) {
    $CI = & get_instance();

    $temp = $CI->db->get_where('user_cart', array('user_id' => $sessData['user_id']))->row_array();
    $user_cart_id = (isset($temp['id'])) ? $temp['id'] : 0;

    $hasCode = '';
    if (!$user_cart_id > 0) {
        $hasCode = array('str' => random_string(), 'user_id' => $sessData['user_id'], 'email' => $sessData['email']);
        $hasCode = url_title(base64_encode(json_encode($hasCode)));

        $CI->db->insert('user_cart', array(
            'user_id' => $sessData['user_id'],
            'cart_hash_code' => $hasCode,
            'dt_created' => date('Y-m-d H:i:s')
        ));
        $user_cart_id = $CI->db->insert_id();
    } else {
        $hasCode = $temp['cart_hash_code'];
    }

    $CI->db->insert('user_cart_items', array(
        'user_id' => $sessData['user_id'],
        'user_cart_id' => $user_cart_id,
        'cart_row_id' => $data['rowid'],
        'cart_id' => $data['id'],
        'quantity' => $data['qty'],
        'price' => $data['price'],
        'product_id' => $prodId['id'],
        'product_variant_id' => $prodId['verId'],
        'item_name' => $data['name'],
        'item_options' => json_encode($data['options']),
        'subtotal' => $data['qty'] * $data['price'],
        'dt_created' => date('Y-m-d H:i:s')
    ));
    return base_url('checkout_cart/' . $hasCode);
}

function removeDBCartItem($userId, $removeId) {
    $CI = & get_instance();

    $temp = $CI->db->get_where('user_cart', array('user_id' => $userId))->row_array();
    $user_cart_id = (isset($temp['id'])) ? $temp['id'] : 0;

    if ($user_cart_id > 0) {
        $CI->db->where(array('user_cart_id' => $user_cart_id, 'user_id' => $userId, 'cart_row_id' => $removeId))
                ->delete('user_cart_items');
    } else {
        $CI->db->where(array('user_id' => $userId, 'cart_row_id' => $removeId))
                ->delete('user_cart_items');
    }

    return true;
}

function destroyDBCart($userId) {
    $CI = & get_instance();
    $CI->db->where(array('user_id' => $userId))->delete('user_cart');
    $CI->db->where(array('user_id' => $userId))->delete('user_cart_items');
    return true;
}

function updateDBCartQty($userId, $updateId, $qty) {
    $CI = & get_instance();

    $temp = $CI->db->get_where('user_cart', array('user_id' => $userId))->row_array();
    $user_cart_id = (isset($temp['id'])) ? $temp['id'] : 0;

    if ($user_cart_id > 0) {
        $cartData = $CI->db->get_where('user_cart_items', array('user_cart_id' => $user_cart_id, 'user_id' => $userId, 'cart_row_id' => $updateId))
                ->row_array();
    } else {
        $cartData = $CI->db->get_where('user_cart_items', array('user_id' => $userId, 'cart_row_id' => $updateId))
                ->row_array();
    }

    $CI->db->where('id', $cartData['id'])
            ->update('user_cart_items', array('quantity' => $qty, 'subtotal' => ($cartData['price'] * $qty)));

    return true;
}

//*******************************************************************************************

function addCartToMailChimp($data, $prodId, $sessData) {
    $CI = & get_instance();

    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $checkoutUrl = addCartToDB($data, $prodId, $sessData);

    $checkCart = $CI->mailchimp->get('ecommerce/stores/' . $storeId . '/carts/' . $data['cartId']);
    if (isset($checkCart['id'])) {
        $result = $CI->mailchimp->patch('ecommerce/stores/' . $storeId . '/carts/' . $data['cartId'], array(
            'order_total' => $checkCart['order_total'] + $data['price'],
            'checkout_url' => $checkoutUrl . '',
            'lines' => array(
                array(
                    'id' => $data['rowid'] . '',
                    'product_id' => $prodId['id'] . '',
                    'product_variant_id' => $prodId['verId'] . '',
                    'quantity' => $data['qty'],
                    'price' => $data['price']
                )
            )
        ));
    } else {
        $result = $CI->mailchimp->post('ecommerce/stores/' . $storeId . '/carts', array(
            'id' => $data['cartId'],
            'customer' => array(
                'id' => $sessData['user_id'],
                'email_address' => $sessData['email'],
                'opt_in_status' => true
            ),
            'currency_code' => 'USD',
            'checkout_url' => $checkoutUrl . '',
            'order_total' => $data['price'],
            'lines' => array(
                array(
                    'id' => $data['rowid'] . '',
                    'product_id' => $prodId['id'] . '',
                    'product_variant_id' => $prodId['verId'] . '',
                    'quantity' => $data['qty'],
                    'price' => $data['price']
                )
            )
        ));
    }

    return $result;
}

function destroyCart() {
    $CI = & get_instance();
    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $sessData = $CI->session->userdata('user_details');
    $CI->mailchimp->delete('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id']);
    return true;
}

function removeMailChimpCartItem($removeId) {
    $CI = & get_instance();

    $sessData = $CI->session->userdata('user_details');
    if (isset($sessData['user_id'])) {
        $CI->load->library('MailChimp');
        $storeId = $CI->config->item('storeId');
        if (isset($removeId)) {
            $cartData = $CI->mailchimp->get('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id']);
            if (isset($cartData['id'])) {
                if (count($cartData['lines']) == 1) {
                    $CI->mailchimp->delete('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id']);
                    destroyDBCart($cartData['customer']['id']);
                } else {
                    $total = 0;
                    foreach ($cartData['lines'] as $lineVal) {
                        if ($lineVal['id'] != $removeId) {
                            $total += $lineVal['price'] * $lineVal['quantity'];
                        }
                    }

                    $CI->mailchimp->patch('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id'], array('order_total' => $total));
                    $CI->mailchimp->delete('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id'].'/lines/' . $removeId);
                    removeDBCartItem($cartData['customer']['id'], $removeId);
                }
            }
        }
    }

    return true;
}

function addToMcCartAfterLogin($userId, $mailCheckout = FALSE) {
    $CI = & get_instance();

    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $sessData = $CI->db->get_where('users', array('user_id' => $userId))->row_array();

    $temp = $CI->db->get_where('user_cart', array('user_id' => $userId))->row_array();
    $user_cart_id = (isset($temp['id'])) ? $temp['id'] : 0;

    $cartDataDB = array();
    if ($user_cart_id > 0) {
        $cartDataDB = $CI->db->get_where('user_cart_items', array('user_id' => $sessData['user_id'], 'user_cart_id' => $user_cart_id))->result_array();
    }

    $cartDataTemp = $CI->cart->contents();

    if ($mailCheckout) {
        foreach ($cartDataTemp as $value) {
            $CI->cart->update(array('rowid' => $value['rowid'], 'qty' => 0));
        }
        $cartDataTemp = array();
    }

    if (count($cartDataTemp) == 0) {
        foreach ($cartDataDB as $cartDataDBVal) {
            $CI->cart->insert(array(
                'id' => $cartDataDBVal['cart_id'],
                'qty' => $cartDataDBVal['quantity'],
                'price' => $cartDataDBVal['price'],
                'name' => $cartDataDBVal['item_name'],
                'options' => json_decode($cartDataDBVal['item_options'], TRUE),
            ));
        }
    }

    if ($user_cart_id > 0) {
//        $CI->db->where('id', $user_cart_id)->delete('user_cart');
        $CI->db->where('user_id', $sessData['user_id'])->where('user_cart_id', $user_cart_id)
                ->delete('user_cart_items');
    }

    $cartData = $CI->cart->contents();

    if (count($cartData) > 0 && isset($sessData['user_id'])) {
        $mailChimpArray = array(
            'id' => 'AWL_CART_' . $sessData['user_id'],
            'customer' => array(
                'id' => $sessData['user_id'],
                'email_address' => $sessData['email'],
                'opt_in_status' => true
            ),
            'currency_code' => 'USD',
            'order_total' => 0,
        );

        foreach ($cartData as $cartDataVal) {
            $type = ($cartDataVal['name'] == 'Ready To Wear') ? 'RTW' : (($cartDataVal['name'] == 'Custom Shoe') ? 'CS' : 'CC');

            $itemData = json_decode($cartDataVal['options']['shoe'], true);
            $tmpId = ($type == 'RTW') ? $itemData['custom_collection_id'] : $itemData;

            $prodId = getMailChimpProductId($tmpId, $type);

            $checkoutUrl = addCartToDB($cartDataVal, $prodId, $sessData);

            $mailChimpArray['lines'][] = array(
                'id' => $cartDataVal['rowid'] . '',
                'product_id' => $prodId['id'] . '',
                'product_variant_id' => $prodId['verId'] . '',
                'quantity' => $cartDataVal['qty'] + 0,
                'price' => $cartDataVal['price']
            );

            $mailChimpArray['order_total'] += ($cartDataVal['price'] * $cartDataVal['qty']);
            $mailChimpArray['checkout_url'] = $checkoutUrl . '';
        }

        if (count($mailChimpArray['lines']) > 0) {
            $CI->mailchimp->delete('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id']);
            $result = $CI->mailchimp->post('ecommerce/stores/' . $storeId . '/carts', $mailChimpArray);
        }
    } else {
        $CI->mailchimp->delete('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id']);
    }

    return true;
}

function updateMailchimpCartQty($id, $quantity) {
    $CI = & get_instance();

    $sessData = $CI->session->userdata('user_details');
    if (isset($sessData['user_id'])) {
        $CI->load->library('MailChimp');
        $storeId = $CI->config->item('storeId');

        $cartData = $CI->mailchimp->get('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id']);
        $order_total = 0;

        foreach ($cartData['lines'] as $lines) {
            if ($lines['id'] == $id) {
                $order_total += $lines['price'] * $quantity;
            } else {
                $order_total += $lines['price'];
            }
        }

        $CI->mailchimp->patch('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id'], array(
            'order_total' => $order_total,
            'lines' => array(array('id' => $id, 'quantity' => $quantity * 1))
        ));
        updateDBCartQty($sessData['user_id'], $id, $quantity);
    }
}

function getMailChimpProductId($item_id, $type) {
    $CI = & get_instance();
    if ($type == 'RTW') {
        $tmpProd = $CI->db->get_where('ready_wear', array('id' => $item_id))->row_array();

        $mcId = 'RTW_' . str_replace(' ', '_', ucwords($tmpProd['shoe_name'])) . '_' . str_replace(' ', '_', ucwords($tmpProd['style_name']));
        $mc_variant_id = 'RTW_' . str_replace(' ', '_', ucwords($tmpProd['shoe_name'])) . '_' . str_replace(' ', '_', ucwords($tmpProd['color']));

        return array('id' => $mcId, 'verId' => $mc_variant_id);
    } else if ($type == 'CS') {
        $cs = $CI->db->select('last_styles.last_style_id,last_styles.style_name,last_item_def.last_item_def_id,last_item_def.last_name')
                        ->join('last_item_def', 'last_item_def.last_item_def_id = last_styles.last_id AND last_item_def.def_code != "rtw"')
                        ->get_where('last_styles', array('last_style_id' => $item_id['style']['id']))->row_array();

        $lName = $cs['last_name'];
        $sName = $cs['style_name'];

        $mcId = 'CS_' . str_replace(' ', '_', ucwords($sName));
        $mc_variant_id = $mcId . '_' . str_replace(' ', '_', ucwords(str_replace('The ', '', $lName)));

        return array('id' => $mcId, 'verId' => $mc_variant_id);
    } else if ($type == 'CC') {
        $lName = $item_id['last_name'];
        $sName = $item_id['style_name'];

        if (!isset($item_id['last_name'])) {
            if (isset($item_id['last']['last_name'])) {
                $lName = $item_id['last']['last_name'];
            } else {
                $lName = $item_id['last']['lastName'];
            }
        }

        if (!isset($item_id['style_name'])) {
            if (isset($item_id['style']['style_name'])) {
                $sName = $item_id['style']['style_name'];
            } else {
                $sName = $item_id['style']['styleName'];
            }
        }

        $mcId = 'CS_' . str_replace(' ', '_', ucwords($sName));
        $mc_variant_id = $mcId . '_' . str_replace(' ', '_', ucwords(str_replace('The ', '', $lName)));

        return array('id' => $mcId, 'verId' => $mc_variant_id);
    }
}

function addOrderToMC($orderId) {
    $CI = & get_instance();
    
    $sessData = $CI->session->userdata('user_details');

    $order = $CI->db->get_where('order', array('order_id' => $orderId))->row_array();
    $orderBilling = $CI->db->get_where('billing_details', array('order_id' => $orderId))->row_array();
    $orderShipping = $CI->db->get_where('shipping_details', array('order_id' => $orderId))->row_array();

    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $cart = $CI->mailchimp->get('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id'], array('fields' => 'lines'));
    foreach ($cart['lines'] as $ky => $val) {
        unset($cart['lines'][$ky]['_links']);
    }

    $orderArray = array(
        'id' => $order['invoice_no'] . '',
        'customer' => array('id' => $order['user_id']),
        'currency_code' => $order['currency'],
        'order_total' => $order['gross_amt'] * 1,
        'discount_total' => $order['discount'] * 1,
        'lines' => $cart['lines'],
        'processed_at_foreign' => date('Y-m-d H:i:s'),
        'shipping_address' => array(
            'name' => $orderShipping['firstname'] . ' ' . $orderShipping['lastname'],
            'address1' => $orderShipping['address1'],
            'address2' => $orderShipping['address2'],
            'city' => $orderShipping['city'],
            'postal_code' => $orderShipping['zipcode'],
            'country' => $orderShipping['country'],
            'phone' => $orderShipping['telephone'],
        ),
        'billing_address' => array(
            'name' => $orderBilling['firstname'] . ' ' . $orderBilling['lastname'],
            'address1' => $orderBilling['address1'],
            'address2' => $orderBilling['address2'],
            'city' => $orderBilling['city'],
            'postal_code' => $orderBilling['zipcode'],
            'country' => $orderBilling['country'],
            'phone' => $orderBilling['telephone'],
        ),
    );
    foreach ($orderArray['shipping_address'] as $key => $ship) {
        if ($ship == null || $ship == '') {
            unset($orderArray['shipping_address'][$key]);
        }
    }
    foreach ($orderArray['billing_address'] as $key => $bill) {
        if ($bill == null || $bill == '') {
            unset($orderArray['billing_address'][$key]);
        }
    }

    $check = $CI->mailchimp->get('ecommerce/stores/' . $storeId . '/orders/' . $order['invoice_no']);
    if (isset($check['id'])) {
        $result = $CI->mailchimp->patch('ecommerce/stores/' . $storeId . '/orders/' . $order['invoice_no'], $orderArray);
    } else {
        $result = $CI->mailchimp->post('ecommerce/stores/' . $storeId . '/orders', $orderArray);
    }

    return $result;
}

function old_getProductId($item_id, $type) {
    $CI = & get_instance();

    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $tablename = ($type == 'RTW') ? 'ready_wear' : 'custom_collection';

    if ($tablename != '') {
        $tmpProd = $CI->db->get_where($tablename, array('id' => $item_id))->row_array();

        $verTyp = ($type == 'RTW') ? $tmpProd['color'] : $tmpProd['shoe_name'];

        $name = $type . '_' . (strtoupper(str_replace(" ", "", $tmpProd['style_name'])));
        $mailChimpProduct = $CI->db->select('mailchimp_products.*,ver.id as verId')
                        ->join('mailchimp_products_variant as ver', 'ver.product_id = mailchimp_products.id AND ver.varient_name = "' . $verTyp . '"', 'left')
                        ->get_where('mailchimp_products', array('name' => $name))->row_array();

        if ($mailChimpProduct['id'] > 0 && $mailChimpProduct['verId'] > 0) {
            return $mailChimpProduct;
        } else {
            $return = array();
            if (!isset($mailChimpProduct['id'])) {
                $CI->db->insert('mailchimp_products', array('name' => $name));
                $return['id'] = $CI->db->insert_id();
            } else {
                $return['id'] = $mailChimpProduct['id'];
            }

            if (!isset($mailChimpProduct['verId'])) {
                $CI->db->insert('mailchimp_products_variant', array('product_id' => $return['id'], 'varient_name' => $verTyp));
                $return['verId'] = $CI->db->insert_id();
            } else {
                $return['verId'] = $mailChimpProduct['verId'];
            }
            $varients[] = array('id' => $return['verId'] . '', 'title' => $verTyp);

            $result = $CI->mailchimp->get('ecommerce/stores/' . $storeId . '/products/' . $return['id']);

            if (!isset($result['id'])) {
                $result = $CI->mailchimp->post('ecommerce/stores/' . $storeId . '/products', array(
                    'id' => $return['id'] . '',
                    'title' => $name,
                    'variants' => $varients,
                ));
            } else {
                $result = $CI->mailchimp->patch('ecommerce/stores/' . $storeId . '/products/' . $return['id'], array(
                    'id' => $return['id'] . '',
                    'title' => $name,
                    'variants' => $varients,
                ));
            }

            return $return;
        }
    }
    return false;
}

function old_checkGuestCart() {
    $CI = & get_instance();

    $CI->load->library('MailChimp');
    $storeId = $CI->config->item('storeId');

    $checkCart = $CI->mailchimp->get('ecommerce/stores/' . $storeId . '/carts');

    $sessData = $CI->session->userdata('user_details');
    if (isset($sessData['user_id'])) {
        $checkCart = $CI->mailchimp->get('ecommerce/stores/' . $storeId . '/carts/CART_GUEST');
        if (isset($checkCart['id'])) {
            $CI->mailchimp->post('ecommerce/stores/' . $storeId . '/carts', array(
                'id' => 'CART_' . $sessData['user_id'],
                'customer' => array(
                    'id' => $sessData['user_id'],
                    'email_address' => $sessData['email'],
                    'opt_in_status' => true
                ),
                'currency_code' => 'USD',
                'order_total' => $checkCart['order_total'],
                'lines' => $checkCart['lines'],
            ));

            $CI->mailchimp->delete('ecommerce/stores/' . $storeId . '/carts/CART_GUEST');
        }
    }
    return true;
}

?>