<?php

/**
 * Description of common_helper
 *
 * @author kraftlabs
 */
function vendorLang($str = NULL, $echo = TRUE, $langFile = NULL) {
    if ($str != NULL) {
        $CI = & get_instance();
        $userSess = $CI->session->userdata('user_details');
        if ($userSess != FALSE) {
            if (isset($userSess['vendor_language'])) {
                $langFile = ($langFile != NULL) ? str_replace(' ', '_', strtolower($langFile)) : 'common';
                $loadLang = ($userSess['vendor_language'] != '' && $userSess['vendor_language'] != NULL) ? $userSess['vendor_language'] : 'english';

                $CI->lang->load($langFile, $loadLang);
                $return = $CI->lang->line(ucwords(strtolower($str)));

                $CI->lang->is_loaded = $CI->lang->language = array();

                $return = (!$return) ? $str : $return;
                if ($echo) {
                    echo $return;
                } else {
                    return $return;
                }
            } else {
                return $str;
            }
        } else {
            return $str;
        }
    }
}

function getPricing($model, $price = array()) {
    $round = 10;

    if ($price === array()) {
        $price = ['CP' => 495, 'CG' => 495, 'CS' => 495, 'AL' => 495, 'OS' => 2450];
        $round = 5;
    }
    $config = ["quarter" => "", "toe" => "P0", "vamp" => "P1", "eyestay" => "P2", "foxing" => "P3"];
    $area = ["quarter" => 0, "toe" => 0, "vamp" => 0, "eyestay" => 0, "foxing" => 0];

    $last = $model['last']['folder'];
    $style = $model['style']['folder'];
    $styleCode = $model['style']['code'];
    $end = '_A0.png';

    $imageBase = $_SERVER['DOCUMENT_ROOT'] . '/files/images/' . $last . "/" . $style . "/";
    // $imageBase = ApplicationConfig::IMG_BASE . $last . "/" . $style . "/";

    $sole_area = 19900;
    foreach ($config as $key => $cfg) {
        $matfolder = $model[$key]['matfolder'];
        $material = $model[$key]['material'];
        $image_url = $imageBase . $matfolder . '/' . $styleCode . '_' . $model['toe']['type'] . $model['vamp']['type'] . $model['eyestay']['type'] . $model['foxing']['type'] . $material . 'C0' . $cfg . $end;
        // if (file_exists($image_url)) {
            $area[$key] = getArea(imagecreatefrompng($image_url));
            if ($key !== 'quarter') {
                $area['quarter'] -= $area[$key];
            } else {
                $total_area = $area[$key] - $sole_area;
            }
        // }
    }

    $area['quarter'] -= $sole_area;
    $shoe_price = 0;
    foreach ($config as $key => $cfg) {
        $matfolder = $model[$key]['matfolder'];
        $shoe_price += $area[$key] * ($price[$matfolder] / $total_area);
    }

    if (isset($model['sole']['stitchCharge'])) {
        $shoe_price = $shoe_price + $model['sole']['stitchCharge'];
    }
    if (isset($model['shoetree'])) {
        if ($model['shoetree'] == 'true') {
            $shoe_price = $shoe_price + SHOE_TREE_PRICE;
        }
    }

    if ($round === 5) {
        $price = round($shoe_price * 0.2, 0) / 0.2;
    } else {
        $price = round($shoe_price, 0);
    }
    return $price;
}

function getArea($imgdata) {
    $area = 0;
    $w = imagesx($imgdata);
    $h = imagesy($imgdata);
    for ($i = 0; $i < $w; $i++) {
        for ($j = 0; $j < $h; $j++) {
            $rgba = imagecolorat($imgdata, $i, $j);
            $colors = imagecolorsforindex($imgdata, $rgba);
            if ($colors["alpha"] < 125) {
                $area++;
            }
        }
    }
    return $area;
}

function generate_options($array = array(), $from = 1, $to = 12, $selected = 0) {
    $options = '';
    if (sizeof($array) == 0) {
        if ($from >= 0 && $to >= 0) {
            for ($i = $from; $i <= $to; $i++) {
                $text = (isset($selected) && $selected == $i) ? 'selected="selected" ' : '';
                $options .= '<option ' . $text . ' >' . $i . '</option>';
            }
        }
    } else {
        foreach ($array as $key => $value) {
            $text = (isset($selected) && $selected === $value['id']) ? 'selected="selected" ' : '';
            $options .= '<option value = "' . $value['id'] . '" ' . $text . ' >' . $value['name'] . '</option>';
        }
    }

    return $options;
}

?>