<?php

function addCustomerToSendy($data) {
    $CI = &get_instance();

    $url = 'https://cstm.style/sendy/subscribe';
    $list = '763LvXNnPm8EJaqwzM5SnIgQ';

    $postdata = http_build_query(
        array(
            'name' => $data['name'],
            'email' => $data['email'],
            'list' => $list,
            'boolean' => 'true',
        )
    );

    $opts = array('http' => array('method' => 'POST', 'header' => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
    $context = stream_context_create($opts);
    $result = file_get_contents($url, false, $context);

    return true;
}
