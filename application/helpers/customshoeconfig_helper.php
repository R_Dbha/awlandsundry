<?php
/**
 * Description of customshoeconfig_helper
 *
 * @author computer7
 */
class CustomShoeConfig {
    const IMG_BASE = "https://www.awlandsundry.com/files/images/";
    const FILE_BASE = "https://www.awlandsundry.com/files/";
    const DESIGN_BASE = "https://www.awlandsundry.com/files/designs/";
    const RELATED_DEF_ANGLE = "_A0";
    const PROPERTY_FOLDER = "/properties/";
    const TOE_PART = "P0";
    const VAMP_PART = "P1";
    const EYESTAY_PART = "P2";
    const FOXING_PART = "P3";
    const EXTENTION = ".png";
}

?>
