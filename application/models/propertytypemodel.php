<?php

class ProperTytypeModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTypeCodeByCat($category)
    {
        $this->db->select('property_type_id, property_code');
        $this->db->from('property_types');
        $this->db->where(array('property_cat' => $category));
        return $this->db->get()->result_array();
    }
}
