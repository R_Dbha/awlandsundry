<?php

class UserModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function register_user($user)
    {
        $this->load->model('UserTypeModel');
        $usertype = $this->UserTypeModel->get_by_typename('User');
        $user['user_type_id'] = $usertype['user_type_id'];
        $user['date_created'] = date('Y-m-d H:i:s');
        $user['facebook_id'] = null;
        $user['is_enabled'] = '1';
        $user['is_facebookuser'] = '0';
        $user['is_verified'] = 1;
        $this->db->trans_begin();
        $this->db->insert('users', $user);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $user['user_id'] = $this->db->insert_id();
            $this->db->trans_commit();
            if ($this->session->userdata('pepperjam')) {
                $pepperjamuser = $user['user_id'];
                $this->db->insert('pepperjam_users', array('user_id' => $pepperjamuser));
                $this->session->unset_userdata('pepperjam');
            }
            return $user;
        }
        return null;
    }

    public function admin_register_user($user)
    {
        $user['date_created'] = date('Y-m-d H:i:s');
        $user['facebook_id'] = null;
        $user['is_enabled'] = '1';
        $user['is_facebookuser'] = '0';
        $user['is_verified'] = 1;

        $this->db->trans_begin();
        $this->db->insert('users', $user);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $user['status'] = 'failure';
        } else {
            $user['user_id'] = $this->db->insert_id();
            $this->db->trans_commit();
            $user['status'] = 'success';
        }
        return $user;
    }

    public function register_linkedin_user($profile)
    {
        $this->load->model('UserTypeModel');
        $usertype = $this->UserTypeModel->get_by_typename('User');
        $user = array(
            'user_type_id' => $usertype['user_type_id'],
            'linkedin_id' => $profile['linkedin_id'],
            'first_name' => $profile['first_name'],
            'last_name' => $profile['last_name'],
            'email' => $profile['email'],
            'date_created' => date('Y-m-d H:i:s'),
            'birth_date' => '',
            'is_enabled' => '1',
            'is_facebookuser' => '0',
            'is_verified' => 1, // $facebookuser['verified'],
            'token' => random_string('alnum', 20),
        );
        $this->db->insert('users', $user);
        $user['user_id'] = $this->db->insert_id();
        if ($this->session->userdata('pepperjam')) {
            $pepperjamuser = $user['user_id'];
            $this->db->insert('pepperjam_users', array('user_id' => $pepperjamuser));
            $this->session->unset_userdata('pepperjam');
        }
        return $user;
    }

    public function register_facebookuser($facebookuser)
    {
        $this->load->model('UserTypeModel');
        $usertype = $this->UserTypeModel->get_by_typename('User');
        $dob = "0000-00-00";
        if (isset($facebookuser['birthday'])) {
            $time = strtotime($facebookuser['birthday']);
            $dob = date('Y-m-d', $time);
        }

        $user = array(
            'user_type_id' => $usertype['user_type_id'],
            'facebook_id' => $facebookuser['id'],
            'first_name' => $facebookuser['first_name'],
            'last_name' => $facebookuser['last_name'],
            'email' => $facebookuser['email'],
            'username' => $facebookuser['email'],
            'date_created' => date('Y-m-d H:i:s'),
            'birth_date' => $dob,
            'is_enabled' => '1',
            'is_facebookuser' => '1',
            'is_verified' => 1, // $facebookuser['verified'],
            'token' => random_string('alnum', 20),
        );
        $this->db->trans_begin();
        $this->db->insert('users', $user);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $user['user_id'] = $this->db->insert_id();
            $this->db->trans_commit();
            if ($this->session->userdata('pepperjam')) {
                $pepperjamuser = $user['user_id'];
                $this->db->insert('pepperjam_users', array('user_id' => $pepperjamuser));
                $this->session->unset_userdata('pepperjam');
            }
            return $user;
        }
        return null;
    }

    public function register_googleuser($googleuser)
    {
        $this->load->model('UserTypeModel');
        $usertype = $this->UserTypeModel->get_by_typename('User');
        $dob = "0000-00-00";

        $user = array(
            'user_type_id' => $usertype['user_type_id'],
            'google_id' => $googleuser['id'],
            'first_name' => $googleuser['given_name'],
            'last_name' => $googleuser['family_name'],
            'email' => $googleuser['email'],
            'username' => $googleuser['email'],
            'date_created' => date('Y-m-d H:i:s'),
            'birth_date' => $dob,
            'is_enabled' => '1',
            'is_facebookuser' => '1',
            'is_verified' => 1, // $googleuser['verified'],
            'token' => random_string('alnum', 20),
        );
        $this->db->trans_begin();
        $this->db->insert('users', $user);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $user['user_id'] = $this->db->insert_id();
            $this->db->trans_commit();
            if ($this->session->userdata('pepperjam')) {
                $pepperjamuser = $user['user_id'];
                $this->db->insert('pepperjam_users', array('user_id' => $pepperjamuser));
                $this->session->unset_userdata('pepperjam');
            }
            return $user;
        }
        return null;
    }

    public function isPepperjamUser($userId)
    {
        $this->db->select('*');
        $this->db->from('pepperjam_users');
        $this->db->where("user_id = '" . $userId . "'");
        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            return false;
        }
        $date_created = date('Y-m-d H:i:s', strtotime('-30 days', strtotime(date('Y-m-d H:i:s'))));
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("user_id = '" . $userId . "' AND date_created >= '" . $date_created . "'");
        $query1 = $this->db->get();
        if ($query1->num_rows() < 1) {
            return false;
        }
        return true;
    }

    public function login_with_linkedin($linkedin_id)
    {
        $this->db->select('u.user_id, u.username, u.first_name, u.last_name, u.email, u.user_type_id, ut.type_name, ut.type_status, u.is_facebookuser');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where(array(
            'u.linkedin_id' => $linkedin_id,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function loginwithfacebook($fbid, $email = null)
    {
        $this->db->select('u.user_id, u.username, u.first_name, u.last_name, u.email, u.user_type_id, ut.type_name, ut.type_status, u.is_facebookuser');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where(['u.facebook_id' => $fbid]);
        if ($email != null) {
            $this->db->or_where(['u.email' => $email]);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return = $query->row_array();
            $this->db->where('user_id', $return['user_id'])
                ->update('users', ['facebook_id' => $fbid, 'is_enabled' => '1', 'is_facebookuser' => '1', 'is_verified' => 1]);

            return $return;
        }
        return null;
    }

    public function loginwithgoogle($gid, $email = null)
    {
        $this->db->select('u.user_id, u.username, u.first_name, u.last_name, u.email, u.user_type_id, ut.type_name, ut.type_status');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where(['u.google_id' => $gid]);
        if ($email != null) {
            $this->db->or_where(['u.email' => $email]);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $return = $query->row_array();
            $this->db->where('user_id', $return['user_id'])
                ->update('users', ['google_id' => $gid, 'is_enabled' => '1', 'is_verified' => 1]);

            return $return;
        }
        return null;
    }

    public function get_states($country = "United States")
    {
        $this->db->select('state id, state name');
        $this->db->from('state_master');
        $this->db->like('country', $country, 'none');
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    public function get_all_states()
    {
        $this->db->select('state id, state name');
        $this->db->from('state_master');
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    public function get_all_countries()
    {
        $this->db->select('country_id, country_name');
        $this->db->from('country_list');
        $this->db->order_by('country_name', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    public function get_countries()
    {
        $this->db->select('country id, country name');
        $this->db->from('state_master');
        $this->db->group_by('country');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    public function save_billing_details($billing)
    {
        $billing['date_created'] = date('Y-m-d H:i:s');
        $this->db->trans_begin();
        $this->db->insert('billing_details', $billing);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $billing['billing_details_id'] = $this->db->insert_id();
            $this->db->trans_commit();
            return $billing;
        }
        return null;
    }

    public function save_shippiing($shipping)
    {
        $shipping['date_created'] = date('Y-m-d H:i:s');
        $this->db->trans_begin();
        $this->db->insert('shipping_details', $shipping);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $shipping['shipping_details_id'] = $this->db->insert_id();
            $this->db->trans_commit();
            return $shipping;
        }
        return null;
    }

    public function save_userdetails($userdetails)
    {
        $userdetails['date_created'] = date('Y-m-d H:i:s');
        $this->db->trans_begin();
        $this->db->insert('user_details', $userdetails);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $userdetails['user_details_id'] = $this->db->insert_id();
            $this->db->trans_commit();
            return $userdetails;
        }
        return null;
    }

    public function login($username, $password)
    {
        $this->db->select('u.*, ut.type_name, ut.type_status');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where(array('u.username' => $username, 'u.password' => $password, 'is_verified' => 1, 'is_enabled' => 1));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function insertKeepMail($email)
    {
        $this->db->select('*');
        $this->db->from('newsletter_users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            $data['email'] = $email;
            $data['date'] = date('Y-m-d H:i:s');
            $this->db->insert('newsletter_users', $data);
        }
    }

    public function isPendingVerification($username, $password)
    {
        $this->db->select('u.user_id, u.username, u.first_name, u.last_name, u.email, u.user_type_id, ut.type_name, ut.type_status, u.is_facebookuser');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where(array(
            'u.username' => $username,
            'u.password' => $password,
            'is_verified' => 0,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function insertPasswordToken($email, $token)
    {
        $this->db->set('password_token', $token);
        $this->db->set('reset_till', date('Y-m-d H:i:s', strtotime("+1 days")));
        $this->db->where('email', $email);
        $this->db->where('is_verified', 1);
        $this->db->trans_begin();
        $this->db->update('users');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getUserDetByEmail($email)
    {
        $this->db->select('u.user_id, u.username, u.first_name, u.last_name, u.email, u.user_type_id, ut.type_name, ut.type_status, u.is_facebookuser,u.token');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where('u.email', $email);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function setnewpwd($token)
    {
        $this->db->select('u.user_id, u.username, u.first_name, u.last_name, u.email, u.user_type_id, ut.type_name, ut.type_status, u.is_facebookuser,u.token');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where('u.token', $token);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function resetPassword($token, $newPwd)
    {
        if ($this->changeSettings('forgotpassword', $newPwd, $token)) {
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('password_token', $token);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $data = $query->row_array();
                return $data;
            }
            return null;
        } else {
            return null;
        }
    }

    public function get_user_alldetails($userid)
    {
        $this->db->select('u.user_id, u.username, u.first_name, u.last_name, u.email, u.user_type_id, ut.type_name, ut.type_status,customer_category');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where(array(
            'u.user_id' => $userid,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $user = $query->row_array();
            $user['user_details'] = $this->get_user_details($userid);
            $user['shipping_details'] = $this->get_shipping_details($userid);
            $user['billing_details'] = $this->get_billing_details($userid);
            return $user;
        }
        return null;
    }

    public function uniqueemail($email)
    {
        $this->db->from('users');
        $this->db->where(array(
            'email' => $email,
        ));
        $query = $this->db->get();
        if (sizeof($query->result_array()) > 0) {
            return false;
        }
        return true;
    }

    public function IsValidCardNo($cardNo, $userEmail)
    {
        $this->db->from('giftcard');
        $this->db->where(array(
            'card_number' => $cardNo,
            'is_active' => '1',
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function getGiftCardAmount($cardNo, $userEmail)
    {
        $this->db->select('amount');
        $this->db->from('giftcard');
        $this->db->where(array(
            'card_number' => $cardNo,
            'is_active' => '1',
        ));
        $query = $this->db->get();
        if (sizeof($query->result_array()) > 0) {
            $res = $query->row_array();
            return $res['amount'];
        }
        return 0;
    }

    public function IsgiftCardProcessed($cardNo, $userId)
    {
        $this->db->from('store_credit_trans st');
        $this->db->join('store_credit s', 's.store_credit_id = st.store_credit_id', 'inner');
        $this->db->where(array(
            'gift_card_code' => $cardNo,
            'user_id' => $userId,
        ));
        $query = $this->db->get();
        if (sizeof($query->result_array()) > 0) {
            return true;
        }
        return false;
    }

    public function insertGiftcardDetails($credit_master, $credit_trans)
    {
        $this->db->trans_begin();
        if (!$this->isUserExistsInstoreCredit($credit_master['user_id'], $credit_master['user_email'])) {
            $this->db->insert('store_credit', $credit_master);
            $store_credit_id = $this->db->insert_id();
        } else {
            $this->db->set('credit_amount', 'credit_amount +' . $credit_master['credit_amount'], false);
            $this->db->where(array(
                'user_id' => $credit_master['user_id'],
                'user_email' => $credit_master['user_email'],
            ));
            $this->db->update('store_credit');
            $this->db->where(array(
                'user_id' => $credit_master['user_id'],
                'user_email' => $credit_master['user_email'],
            ));
            $store_credit_id = $this->db->get('store_credit')->row()->store_credit_id;
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $credit_trans['store_credit_id'] = $store_credit_id;
            $this->db->insert('store_credit_trans', $credit_trans);
            $this->db->trans_commit();
            return 1;
        }
        return null;
    }

    public function isUserExistsInstoreCredit($userId, $email)
    {
        $this->db->from('store_credit');
        $this->db->where(array(
            'user_id' => $userId,
            'user_email' => $email,
        ));
        $query = $this->db->get();
        if (sizeof($query->result_array()) > 0) {
            return true;
        }
        return false;
    }

    public function getStoreCreditBalance($email, $userId)
    {
        $this->db->select('store_credit_id,credit_amount');
        $this->db->from('store_credit');
        $this->db->where(array(
            'user_id' => $userId,
            'user_email' => $email,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            $data['store_credit_id'] = $res['store_credit_id'];
            $data['credit_amount'] = $res['credit_amount'];
            return $data;
        } else {
            return 0;
        }
    }

    public function updateStoreCreditAmount($store_credit_id, $credit_perc, $orderId, $total)
    {
        $credit_amt = $total * $credit_perc / 100;
        $this->db->trans_begin();
        $this->db->set('credit_amount', 'credit_amount -' . $credit_amt, false);
        $this->db->where('store_credit_id', $store_credit_id);
        $this->db->update('store_credit');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $credit_trans = array(
                'store_credit_id' => $store_credit_id,
                'gift_card_code' => $orderId,
                'card_amount' => $credit_amt,
                'is_receipt' => 0,
                'is_issue' => 1,
            );
            $this->db->insert('store_credit_trans', $credit_trans);
            $this->db->trans_commit();
        }
    }

    public function unique_other_email($email, $userid)
    {
        $this->db->from('users');
        $this->db->where(array(
            'email' => $email,
            'user_id !=' => $userid,
        ));
        $query = $this->db->get();
        if (sizeof($query->result_array()) > 0) {
            return false;
        }
        return true;
    }

    public function get_user_details($userid)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where(array(
            'user_id' => $userid,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function get_shipping_details($userid)
    {
        $this->db->select('*');
        $this->db->from('shipping_details');
        $this->db->where(array(
            'user_id' => $userid,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return;
            $query->row_array();
        }
        return null;
    }

    public function get_billing_details($userid)
    {
        $this->db->select('*');
        $this->db->from('billing_details');
        $this->db->where(array(
            'user_id' => $userid,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return;
            $query->row_array();
        }
        return null;
    }

    public function verifyemail($token)
    {
        $this->db->select('u.user_id, u.username, u.first_name, u.last_name, u.email, u.user_type_id, ut.type_name, ut.type_status, u.is_facebookuser');
        $this->db->from('users u');
        $this->db->join('user_type ut', 'ut.user_type_id = u.user_type_id', 'inner');
        $this->db->where('token', $token);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $userdet = $query->row_array();
            $this->db->set('is_verified', 1);
            $this->db->update('users');
            return $userdet;
        }
        return null;
    }

    public function changeSettings($type, $new, $uId)
    {
        if ($type == 'password') {
            $new = md5($new);
            $this->db->where('user_id', $uId);
        } else if ($type == 'forgotpassword') {
            $new = md5($new);
            $type = 'password';
            $this->db->set('reset_till', date('Y-m-d H:i:s'));
            $this->db->where('password_token', $uId);
            $this->db->where('reset_till >', date('Y-m-d H:i:s'));
        } else if ($type == 'email') {
            $this->db->where('user_id', $uId);
        }
        $this->db->set($type, $new);
        $this->db->update('users');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function keepmail($email)
    {
        // $this->db->insert('awl_members_users', $email);
    }

    public function save_address_details($address)
    {
        if ($address['is_billing'] != "0") {
            $this->db->set('is_billing', 0);
            $this->db->where('user_id', $address['user_id']);
            $this->db->where('is_billing', 1);
            $this->db->update('address_details');
        }
        if ($address['is_shipping'] != "0") {
            $this->db->set('is_shipping', 0);
            $this->db->where('user_id', $address['user_id']);
            $this->db->where('is_shipping', 1);
            $this->db->update('address_details');
        }

        $this->db->insert('address_details', $address);
        return true;
    }

    public function get_user_address_details($userid, $type = '')
    {
        $this->db->select('*');
        $this->db->from('address_details');
        $this->db->where(array(
            'user_id' => $userid,
        ));
        if (isset($type) && $type !== '') {
            $this->db->where(array(
                $type => 1,
            ));
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function get_orders_status($userId)
    {
        $res = $this->db->query("SELECT Distinct o.order_id,o.invoice_no,o.`order_date`,o.user_id,CONCAT(sd.firstname,' ',sd.lastname) as name," . "(`gross_amt`-`discount`+`tax_amount`) As net_amt,os.status, os.status_desc " . "FROM `order` o inner join users u on u.user_id = o.user_id " . "inner join order_status os on os.order_id = o.order_id and os.is_active = 1 " . "inner join shipping_details sd on sd.order_id = o.order_id where u.user_id = " . $userId . " ORDER BY o.order_id desc");
        if ($res->num_rows() > 0) {
            return $res->result_array();
        }
        return null;
    }

    public function updateUserinfo($userDet, $userId)
    {
        $this->db->set('first_name', $userDet['first_name']);
        $this->db->set('last_name', $userDet['last_name']);
        $this->db->set('email', $userDet['email']);
        $this->db->set('username', $userDet['email']);
        $this->db->where('user_id', $userId);
        $this->db->update('users');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserInfo($userId)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where(array(
            'user_id' => $userId,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function deleteAddress($addressId, $type)
    {
        if ($type === 'billing') {
            $this->db->where(array(
                'address_details_id' => $addressId,
                'is_shipping' => '0',
            ));
            $this->db->delete('address_details');
            $this->db->where(array(
                'address_details_id' => $addressId,
            ));
            $this->db->update('address_details', array(
                'is_billing' => 0,
            ));
        } elseif ($type == "shipping") {
            $this->db->where(array(
                'address_details_id' => $addressId,
                'is_billing' => '0',
            ));
            $this->db->delete('address_details');
            $this->db->where(array(
                'address_details_id' => $addressId,
            ));
            $this->db->update('address_details', array(
                'is_shipping' => 0,
            ));
        } else {
            $this->db->where('address_details_id', $addressId);
            $this->db->delete('address_details');
        }
        return true;
    }

    public function getAddressInfoByAddressId($addressId, $userId)
    {
        $this->db->select('*');
        $this->db->from('address_details');
        $this->db->where(array(
            'address_details_id' => $addressId,
            'user_id' => $userId,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
    }

    public function update_address_details($address, $addressId)
    {
        if ($address['is_billing'] != "0") {
            $this->db->set('is_billing', 0);
            $this->db->where('user_id', $address['user_id']);
            $this->db->where('is_billing', 1);
            $this->db->update('address_details');
        }
        if ($address['is_shipping'] != "0") {
            $this->db->set('is_shipping', 0);
            $this->db->where('user_id', $address['user_id']);
            $this->db->where('is_shipping', 1);
            $this->db->update('address_details');
        }
        $this->db->where('address_details_id', $addressId);
        $this->db->where('user_id', $address['user_id']);
        $this->db->update('address_details', $address);
        return true;
    }

    public function getOrderDetailsByUser($userId)
    {
        $array = array();
        $query = $this->db->query('Select l.last_name, l.folder_name as item_type, ls.folder_name as style_name,o.order_id,o.invoice_no,os.status,os.add_date ,' . 'o.discount_perc,od.design_id,s.image_file,od.item_id, od.item_name,od.item_amt,od.left_shoe,od.right_shoe,od.left_width,od.right_width, os.status_desc ' . 'from `order` o inner join `order_details` od on od.order_id = o.order_id left outer ' . 'join shoe_design s on s.shoe_design_id = od.design_id inner join users u on u.user_id = o.user_id ' . 'inner join order_status os on os.order_id = o.order_id and os.is_active = 1 ' . 'inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id ' . 'where u.user_id = ' . $userId . ' order by o.order_id desc');
        $order_array = $query->result_array();
        return $order_array;
    }

    public function changePwd($current, $new, $uderId)
    {
        $this->db->select('*');
        $this->db->where('password', md5($current));
        $this->db->from('users');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->changeSettings('password', $new, $uderId);
            return true;
        } else {
            return false;
        }
    }

    public function getbillingNshippingDetails($order_id, $userId)
    {
        $arr = array();

        $this->db->where(array('o.order_id' => $order_id, 'o.user_id' => $userId, 'os.is_active' => 1));
        $this->db->from('order o');
        $this->db->join('order_status os', 'os.order_id = o.order_id', 'inner');
        $res = $this->db->get();
        $arr['orderstatus'] = $res->result_array();

        $this->db->from('billing_details b');
        $this->db->join('users u', 'u.user_id = b.user_id', 'inner');
        $this->db->where(array('order_id' => $order_id, 'b.user_id' => $userId));
        $query = $this->db->get();
        $arr['billing'] = $query->result_array();
        $this->db->where(array('order_id' => $order_id, 'user_id' => $userId));
        $query1 = $this->db->get('shipping_details');
        $arr['shipping'] = $query1->result_array();

        $query2 = $this->db->query('Select o.order_id, od.quantity, o.discount_perc,od.design_id,s.image_file,item_id,item_name,item_amt,od.left_shoe,od.right_shoe,od.left_width,od.right_width,l.is_lace_enabled
            from `order` o
            inner join `order_details` od on od.order_id = o.order_id
            left outer join shoe_design s on s.shoe_design_id = od.design_id
            inner join last_styles l on l.last_style_id = s.style_id
            where o.order_id = ' . $order_id . ' and o.user_id = ' . $userId . ' and s.last_id=6');
        $arr['order']['ready_wear'] = $query2->result_array();

        $query3 = $this->db->query('Select s.shoemodel, o.order_id,od.quantity,o.discount_perc,od.design_id,s.image_file,item_id,item_name,item_amt,od.left_shoe,od.right_shoe,od.left_width,od.right_width,stitch_name,lace_name,monogram_text,l.is_lace_enabled
            from `order` o
            inner join `order_details` od on od.order_id = o.order_id
            left outer join shoe_design s on s.shoe_design_id = od.design_id
            left outer join lace_types lt on lt.lace_type_id = s.lace_id
            inner join stitch_types st on st.stitch_type_id = s.stitch_id
            inner join last_styles l on l.last_style_id = s.style_id
            left outer join design_monogram dm on dm.design_id = s.shoe_design_id
            where o.order_id = ' . $order_id . ' and o.user_id = ' . $userId);
        $orderarr = $query3->result_array();
        $arr['order']['items'] = $orderarr;
        return $arr;
    }

    public function get_shoe_designsByUser($userId, $isPublic = true)
    {
        $where = array();
        if ($userId != null) {
            $where['s.user_id'] = $userId;
        } else {
            $where['s.is_public'] = $isPublic;
        }
        $result = $this->db->query(' select "save" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name, s.date_created ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount, fn_get_rework_count(s.shoe_design_id) As ReworkCount,
            s.shoemodel
        from shoe_design s
        INNER JOIN users u ON u.user_id = s.user_id
        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . ' and is_active = 1
        order by s.date_created desc');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function delete_design($shoe_design_id)
    {
        $this->db->where('shoe_design_id', $shoe_design_id);
        $this->db->set('is_active', 0);
        $this->db->update('shoe_design');
        return $this->db->affected_rows();
    }

    public function change_public_name($public_name, $shoe_design_id)
    {
        $this->db->where('shoe_design_id', $shoe_design_id);
        $this->db->set('public_name', $public_name);
        $this->db->update('shoe_design');
        return $this->db->affected_rows();
    }

}
