<?php

class shoemodel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function save_custom_model($userid = null, $shoe_design_id)
    {
        $date_created = date('Y-m-d H:i:s');
        $this->db->query('INSERT INTO `shoe_design` select null, ' . $userid . ', `shoemodel`, `name`, `image_file`, `last_id`, `style_id`, `q_base`, '
            . '`q_mat_id`, `q_clr_id`, `toe_id`, `t_mat_id`, `t_clr_id`, `vamp_id`, `v_mat_id`, `v_clr_id`, `eyestay_id`, `e_mat_id`, '
            . '`e_clr_id`, `foxing_id`, `f_mat_id`, `f_clr_id`, `stitch_id`, `lace_id`, `size_left_val`, `size_left_text`, `size_right_val`, '
            . '`size_right_text`,   `size_left_width`,`size_right_width`,`m_lsize`,`m_lwidth`,`m_rsize`,`m_rwidth`, `m_lgirth`,`m_linstep`,`m_rgirth`,`m_rinstep`, `m_lheel`,`m_rheel`,`m_lankle`,`m_rankle`, '
            . '`is_approved`, `is_public`, `public_name`, "' . $date_created . '",`is_active` from shoe_design ' . ' where `shoe_design_id` = '
            . $shoe_design_id);
        $shoe['shoe_design_id'] = $this->db->insert_id();
        $query = $this->db->query("SELECT `shoe_design_id`, `user_id`, `shoemodel`, `name`, `image_file`, `last_id`, `style_id`, `q_base`, `q_mat_id`, "
            . "`q_clr_id`, `toe_id`, `t_mat_id`, `t_clr_id`, `vamp_id`, `v_mat_id`, `v_clr_id`, `eyestay_id`, `e_mat_id`, `e_clr_id`, `foxing_id`,"
            . " `f_mat_id`, `f_clr_id`, `stitch_id`, `lace_id`, `size_left_val`, `size_left_text`, `size_right_val`, `size_right_text`, "
            . "`is_approved`, `is_public`, `public_name`, `date_created` FROM `shoe_design` WHERE  `shoe_design_id` = "
            . $shoe['shoe_design_id']);
        return $query->row_array();
    }

    public function savemodel($userid = null, $model)
    {
        $mainangles = array("A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7");
        $thumbangles = array("A0", "A7");
        $imgname = $this->generate_shoename();
        $this->createDesign($model, $thumbangles, $mainangles, ApplicationConfig::THUMB_WIDTH, ApplicationConfig::THUMB_HEIGHT, $imgname);
        $shoe = array(
            'user_id' => $userid,
            'shoemodel' => json_encode($model),
            'name' => $imgname,
            'image_file' => $imgname,
            'last_id' => $model['last']['id'],
            'style_id' => $model['style']['id'],
            'q_base' => $model['quarter']['base'],
            'q_mat_id' => $model['quarter']['matId'],
            'q_clr_id' => $model['quarter']['clrId'],
            'toe_id' => $model['toe']['toeId'],
            't_mat_id' => $model['toe']['matId'],
            't_clr_id' => $model['toe']['clrId'],
            'vamp_id' => $model['vamp']['vampId'],
            'v_mat_id' => $model['vamp']['matId'],
            'v_clr_id' => $model['vamp']['clrId'],
            'eyestay_id' => $model['eyestay']['eyestayId'],
            'e_mat_id' => $model['eyestay']['matId'],
            'e_clr_id' => $model['eyestay']['clrId'],
            'foxing_id' => $model['foxing']['foxingId'],
            'f_mat_id' => $model['foxing']['matId'],
            'f_clr_id' => $model['foxing']['clrId'],
            'stitch_id' => $model['stitch']['id'],
            'lace_id' => $model['lace']['id'],
            'is_approved' => 0,
            'size_left_val' => @$model['size']['left']['value'],
            'size_left_text' => @$model['size']['left']['text'],
            'size_right_val' => @$model['size']['right']['value'],
            'size_right_text' => @$model['size']['right']['text'],
            'size_left_width' => @$model['size']['left']['width'] . '',
            'size_right_width' => @$model['size']['right']['width'] . '',
            'm_lsize' => @$model['measurement']['left']['size'] . '',
            'm_lwidth' => @$model['measurement']['left']['width'] . '',
            'm_rsize' => @$model['measurement']['right']['size'] . '',
            'm_rwidth' => @$model['measurement']['right']['width'] . '',
            'm_lgirth' => @$model['measurement']['left']['girth'] . '',
            'm_linstep' => @$model['measurement']['left']['instep'] . '',
            'm_rgirth' => @$model['measurement']['right']['girth'] . '',
            'm_rinstep' => @$model['measurement']['right']['instep'] . '',
            'm_lheel' => @$model['measurement']['left']['heel'] . '',
            'm_lankle' => @$model['measurement']['left']['ankle'] . '',
            'm_rheel' => @$model['measurement']['right']['heel'] . '',
            'm_rankle' => @$model['measurement']['right']['ankle'] . '',
            'date_created' => date('Y-m-d H:i:s'),
        );

        $this->db->trans_begin();
        $this->db->insert('shoe_design', $shoe);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $shoe['shoe_design_id'] = $this->db->insert_id();
            if (isset($model['monogram'])) {
                $this->insert_monogram($shoe['shoe_design_id'], $model['monogram']);
            }
            if (isset($model['reDesignId']) && $userid !== null) {
                $this->insert_rework($shoe['shoe_design_id'], $model['reDesignId'], $userid);
            }
            $this->db->trans_commit();
            return $shoe;
        }
    }

    public function get_latest_design($user_id)
    {
        $this->db->select('size_left_text, size_right_val, size_right_text, size_left_width, size_right_width,
                           m_lsize, m_rsize, m_lwidth, m_rwidth,
                           m_lgirth, m_rgirth, m_linstep, m_rinstep,
                           m_lheel, m_rheel, m_lankle, m_rankle,
                           size_left_val');
        $this->db->from('shoe_design');
        $this->db->where('user_id', $user_id);

        $this->db->where('size_left_val is NOT null');
        $this->db->where('size_left_val <> 0');
        $this->db->not_like('size_left_val', '', 'none');

        $this->db->where('size_right_val is NOT null');
        $this->db->where('size_right_val <> 0');
        $this->db->not_like('size_right_val', '', 'none');

        $this->db->where('size_left_width is NOT null');
        $this->db->where('size_left_width <> ', '');
        $this->db->not_like('size_left_width', '', 'none');

        $this->db->order_by('shoe_design_id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            return $res;
        } else {
            return false;
        }
    }

    public function insert_monogram($designId, $monogram)
    {
        $this->db->insert('design_monogram', array(
            'design_id' => $designId,
            'left_shoe' => $monogram['leftShoe'],
            'right_shoe' => 0,
            'left_side' => $monogram['leftSide'],
            'right_side' => 0,
            'monogram_text' => $monogram['text'],
        ));
    }

    public function insert_rework($designId, $reworkId, $userId)
    {
        $this->db->insert('design_reworks', array(
            "shoe_design_id" => $reworkId,
            "user_id" => $userId,
            "output_design" => $designId,
        ));
    }

    public function updateDesign($model, $user_id = null)
    {
        $details = $this->get_shoe_designs($model['shoeDesignId']);
        $mainangles = array("A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7");
        $thumbangles = array("A0", "A7");

        $imgname = $details[0]['image_file'];
        $this->createDesign($model, $thumbangles, $mainangles, ApplicationConfig::THUMB_WIDTH, ApplicationConfig::THUMB_HEIGHT, $imgname);
        $shoe = array(
            'shoemodel' => json_encode($model),
            'last_id' => $model['last']['id'],
            'style_id' => $model['style']['id'],
            'q_base' => $model['quarter']['base'],
            'q_mat_id' => $model['quarter']['matId'],
            'q_clr_id' => $model['quarter']['clrId'],
            'toe_id' => $model['toe']['toeId'],
            't_mat_id' => $model['toe']['matId'],
            't_clr_id' => $model['toe']['clrId'],
            'vamp_id' => $model['vamp']['vampId'],
            'v_mat_id' => $model['vamp']['matId'],
            'v_clr_id' => $model['vamp']['clrId'],
            'eyestay_id' => $model['eyestay']['eyestayId'],
            'e_mat_id' => $model['eyestay']['matId'],
            'e_clr_id' => $model['eyestay']['clrId'],
            'foxing_id' => $model['foxing']['foxingId'],
            'f_mat_id' => $model['foxing']['matId'],
            'f_clr_id' => $model['foxing']['clrId'],
            'stitch_id' => $model['stitch']['id'],
            'lace_id' => $model['lace']['id'],
            'size_left_val' => $model['size']['left']['value'],
            'size_left_text' => $model['size']['left']['text'],
            'size_right_val' => $model['size']['right']['value'],
            'size_right_text' => $model['size']['right']['text'],
            'user_id' => $user_id,
        );
        $this->db->where('shoe_design_id', $model['shoeDesignId']);
        $this->db->update('shoe_design', $shoe);
        if (isset($model['monogram'])) {
            $this->updateDesignMonogram($model['monogram'], $model['shoeDesignId']);
        }
        return $details;
    }

    public function updateDesignMonogram($monogram, $designId)
    {
        $this->db->where('design_id', $designId);
        $this->db->update('design_monogram', array(
            'left_shoe' => $monogram['leftShoe'],
            'right_shoe' => $monogram['rightShoe'],
            'left_side' => $monogram['leftSide'],
            'right_side' => $monogram['rightSide'],
            'monogram_text' => $monogram['text'],
        ));
    }

    public function updateDesignName($details, $designId)
    {
        if ($this->validateUniquename($details['public_name'])) {
            $this->db->where('shoe_design_id', $designId);
            $this->db->update('shoe_design', $details);
            return true;
        } else {
            return false;
        }
    }

    public function generate_shoename()
    {
        $total = $this->db->count_all('shoe_design');
        if ($total > 0) {
            $query = <<<EOD
                    SELECT FLOOR(RAND() * 999999) AS random_num
                    FROM shoe_design
                    WHERE "random_num" NOT IN (SELECT image_file FROM shoe_design WHERE image_file IS NOT NULL)
                    LIMIT 1
EOD;
            $result = $this->db->query($query)->row_array();
            return $result['random_num'];
        }
        $result = $this->db->query('SELECT FLOOR(RAND() * 999999) AS random_num')->row_array();
        return $result['random_num'];
    }

    public function getStyleId($shoe_design_id)
    {
        $this->db->select('style_id');
        $this->db->from('shoe_design');
        $this->db->where('shoe_design_id', $shoe_design_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            return $res['style_id'];
        } else {
            return null;
        }
    }

    public function get_related_shoes($criteria)
    {
        $this->db->select('s.shoe_design_id, s.image_file,concat(last_name,"/",ls.folder_name) as style_last', false);
        $this->db->from('shoe_design s');
        $this->db->join('last_styles ls', 'ls.last_style_id = s.style_id');
        $this->db->join('last_item_def', 'last_item_def_id = ls.last_id');
        if (isset($criteria['style_id'])) {
            $this->db->where('style_id', $criteria['style_id']);
        }
        if (isset($criteria['style_name'])) {
            $this->db->like('ls.style_name', $criteria['style_name']);
        }
        $this->db->where('is_public', 1);
        if (isset($criteria['toe_id'])) {
            $this->db->where('toe_id', $criteria['toe_id']);
        }
        if (isset($criteria['vamp_id'])) {
            $this->db->where('vamp_id', $criteria['vamp_id']);
        }
        if (isset($criteria['eyestay_id'])) {
            $this->db->where('eyestay_id', $criteria['eyestay_id']);
        }
        if (isset($criteria['foxing_id'])) {
            $this->db->where('foxing_id', $criteria['foxing_id']);
        }
        if (isset($criteria['material'])) {
            $where = "((s.q_mat_id = " . $criteria['material']['matId'] . " AND s.q_clr_id = " . $criteria['material']['colorId'] . ")";
            $where .= "OR (s.t_mat_id = " . $criteria['material']['matId'] . " AND s.t_clr_id = " . $criteria['material']['colorId'] . ")";
            $where .= "OR (s.v_mat_id = " . $criteria['material']['matId'] . " AND s.v_clr_id = " . $criteria['material']['colorId'] . ")";
            $where .= "OR (s.e_mat_id = " . $criteria['material']['matId'] . " AND s.e_clr_id = " . $criteria['material']['colorId'] . ")";
            $where .= "OR (s.f_mat_id = " . $criteria['material']['matId'] . " AND s.f_clr_id = " . $criteria['material']['colorId'] . ")";
            $where .= ")";
            $this->db->where($where, null, false);
        }
        if (isset($criteria['sort'])) {
            $this->db->order_by($criteria['sort'], $criteria['order']);
        }
        $this->db->limit(10);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $related = $query->result_array();
            $shoes = array();
            foreach ($related as $shoe) {
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/designs/' . $shoe['image_file'] . '_A0.png')) {
                    $shoes[] = $shoe;
                }
            }
            return $shoes;
        }
        return null;
    }

    public function get_material_colors()
    {
        $this->db->select('material_def_id, material_code, material_name');
        $this->db->from('material_def');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $materials = array();
            $result = $query->result_array();
            foreach ($result as $material) {
                $material['colors'] = $this->get_colors($material['material_def_id']);
                $materials[] = $material;
            }
            return $materials;
        }
        return null;
    }

    public function get_material_colors_new()
    {
        $this->db->select('material_def_id, material_code, material_name');
        $this->db->from('material_def');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $materials = array();
            $result = $query->result_array();
            foreach ($result as $material) {
                $material['colors'] = $this->get_distinctcolors();
                $materials[] = $material;
            }
            return $materials;
        }
        return null;
    }

    public function get_colors($materialId)
    {
        $this->db->select('material_color_id, color_code, color_name');
        $this->db->from('material_colors');
        $this->db->where('material_id', $materialId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function get_distinctcolors()
    {
        $this->db->distinct();
        $this->db->select('color_name as material_color_id, color_name as color_code,color_name');
        $this->db->from('material_colors');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function get_shoe_designs($designId = null, $isPublic = true)
    {
        $where = array();
        if ($designId != null) {
            $where['shoe_design_id'] = $designId;
        } else {
            $where['s.is_public'] = $isPublic;
        }
        $this->db->select('s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name');
        $this->db->from('shoe_design s');
        $this->db->join('users u', 'u.user_id = s.user_id', 'LEFT');
        $this->db->join('last_item_def l', 'l.last_item_def_id = s.last_id', 'INNER');
        $this->db->join('last_styles ls', 'ls.last_style_id = s.style_id', 'INNER');
        $this->db->where($where);
        $this->db->order_by('s.date_created', 'DESC');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_shoe_image($design_id)
    {
        $this->db->select('image_file');
        $this->db->from('shoe_design');
        $this->db->where('shoe_design_id', $design_id);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            $res = $result->row_array();
            return $res['image_file'];
        }
        return null;
    }

    public function get_shoe_image_new($design_id)
    {
        $this->db->select('image_file');
        $this->db->from('shoe_design');
        $this->db->where('shoe_design_id', $design_id);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            $res = $result->row_array();
            return $res['image_file'];
        }
        return null;
    }

    public function get_shoe_design_details($designId, $userId)
    {
        $query = $this->db->query('select
            s.shoe_design_id, s.name, s.image_file, s.public_name,
            s.shoemodel,
            count(dr.design_rework_id) as rework_count,
            (select count(*) from  design_likes df inner join shoe_design s on df.shoe_design_id = s.shoe_design_id and df.like_type = "Like" and df.shoe_design_id = ' . $designId . ' and df.user_id = ' . $userId . ') As  IsUserlikes,
            (select count(*) from  design_likes df inner join shoe_design s  on df.shoe_design_id = s.shoe_design_id and df.like_type = "Favorite" and df.shoe_design_id = ' . $designId . ' and df.user_id = ' . $userId . ') As  IsUserfavs,
            (select count(*) from  design_likes df inner join shoe_design s on df.shoe_design_id = s.shoe_design_id and df.like_type = "Like" and df.shoe_design_id = ' . $designId . ') As  design_likes,
            (select count(*) from  design_likes df inner join shoe_design s  on df.shoe_design_id = s.shoe_design_id and df.like_type = "Favorite" and df.shoe_design_id = ' . $designId . ') As  design_favs,
            ls.style_name, ls.is_vamp_enabled, ls.is_lace_enabled,
            l.last_name,l.manufacturer_code,
            pt.property_name as toe_type,
            pv.property_name as vamp_type,
            pe.property_name as eyestay_type,
            pf.property_name as foxing_type,
            tsp.is_material as t_is_material, tsp.is_nothing as t_is_nothing,
            vsp.is_material as v_is_material, vsp.is_nothing as v_is_nothing,
            esp.is_material as e_is_material, esp.is_nothing as e_is_nothing,
            fsp.is_material as f_is_material, fsp.is_nothing as f_is_nothing,

            mq.material_def_id as quarter_material_id,
            mt.material_def_id as toe_material_id,
            mv.material_def_id as vamp_material_id,
            me.material_def_id as eyestay_material_id,
            mf.material_def_id as foxing_material_id,

            mq.material_name as quarter_material,
            mt.material_name as toe_material,
            mv.material_name as vamp_material,
            me.material_name as eyestay_material,
            mf.material_name as foxing_material,
            mcq.color_name as quarter_color,
            mct.color_name as toe_color,
            mcv.color_name as vamp_color,
            mce.color_name as eyestay_color,
            mcf.color_name as foxing_color,
            st.stitch_name, st.stitch_color,
            lt.lace_name, lt.color_code,
            dm.monogram_id,dm.left_shoe,dm.right_shoe,dm.left_side,dm.right_side,dm.monogram_text,is_public

        from shoe_design s
            inner join last_styles ls on ls.last_style_id = s.style_id
            inner join last_item_def l on l.last_item_def_id = s.last_id
            left outer join style_properties tsp on tsp.property_type_id = s.toe_id and tsp.last_style_id = s.style_id
            inner join property_types pt on pt.property_type_id = s.toe_id
            left outer join style_properties vsp on vsp.property_type_id = s.vamp_id and vsp.last_style_id = s.style_id
            inner join property_types pv on pv.property_type_id = s.vamp_id
            left outer join style_properties esp on esp.property_type_id = s.eyestay_id and esp.last_style_id = s.style_id
            inner join property_types pe on pe.property_type_id = s.eyestay_id
            left outer join style_properties fsp on fsp.property_type_id = s.foxing_id and fsp.last_style_id = s.style_id
            inner join property_types pf on pf.property_type_id = s.foxing_id
            inner join material_def mq on mq.material_def_id = s.q_mat_id
            inner join material_def mt on mt.material_def_id = s.t_mat_id
            inner join material_def mv on mv.material_def_id = s.v_mat_id
            inner join material_def me on me.material_def_id = s.e_mat_id
            inner join material_def mf on mf.material_def_id = s.f_mat_id
            inner join material_colors mcq on mcq.material_color_id = s.q_clr_id
            inner join material_colors mct on mct.material_color_id = s.t_clr_id
            inner join material_colors mcv on mcv.material_color_id = s.v_clr_id
            inner join material_colors mce on mce.material_color_id = s.e_clr_id
            inner join material_colors mcf on mcf.material_color_id = s.f_clr_id
            inner join stitch_types st on st.stitch_type_id = s.stitch_id
            inner join lace_types lt on lt.lace_type_id = s.lace_id
            left outer join design_monogram dm on dm.design_id = s.shoe_design_id
            left outer join design_reworks dr on dr.shoe_design_id = s.shoe_design_id
        where s.shoe_design_id = ' . $designId . '');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function get_shoe_designsByUser($userId, $isPublic = true)
    {
        $where = array();
        if ($userId != null) {
            $where['s.user_id'] = $userId;
        } else {
            $where['s.is_public'] = $isPublic;
        }

        $result = $this->db->query('select "save" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount, fn_get_rework_count(s.shoe_design_id) As ReworkCount
        from shoe_design s
        INNER JOIN users u ON u.user_id = s.user_id
        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
        ');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_purchased_shoedesignsByUser($userId, $isPublic = true)
    {
        $where = array();
        if ($userId != null) {
            $where['u.user_id'] = $userId;
        } else {
            $where['s.is_public'] = $isPublic;
        }

        $result = $this->db->query('select "pur" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount, fn_get_rework_count(s.shoe_design_id) As ReworkCount
                from shoe_design s INNER JOIN order_details od on od.design_id = s.shoe_design_id
                inner join `order` o on o.order_id = od.order_id AND payment_status = "Processed"
                INNER JOIN users u ON u.user_id = o.user_id
                inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId);

        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_Favouritshoe_designsByUser($userId, $isPublic = true)
    {
        $where = array();
        if ($userId != null) {
            $where['d.user_id'] = $userId;
        } else {
            $where['s.is_public'] = $isPublic;
        }

        $result = $this->db->query(' select "fav" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
        from shoe_design s INNER JOIN design_likes d on d.shoe_design_id = s.shoe_design_id and d.like_type = "Favorite"
        INNER JOIN users u ON u.user_id = d.user_id
        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
        ');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_purchaseNFavouriteshoe_designsByUser($userId, $isPublic = true)
    {
        $where = array();
        if ($userId != null) {
            $where['u.user_id'] = $userId;
        } else {
            $where['s.is_public'] = $isPublic;
        }
        $result = $this->db->query(' select "fav" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
        from shoe_design s INNER JOIN design_likes d on d.shoe_design_id = s.shoe_design_id and d.like_type = "Favorite"
        INNER JOIN users u ON u.user_id = d.user_id
        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
        UNION
        select "pur" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
                from shoe_design s INNER JOIN order_details od on od.design_id = s.shoe_design_id
                inner join `order` o on o.order_id = od.order_id AND payment_status = "Processed"
                INNER JOIN users u ON u.user_id = o.user_id
                inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
        ');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_saveNpurchase_shoedesignsByUser($userId, $isPublic = true)
    {
        $where = array();
        if ($userId != null) {
            $where['u.user_id'] = $userId;
        } else {
            $where['s.is_public'] = $isPublic;
        }
        $result = $this->db->query(' select "save" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
        from shoe_design s
        INNER JOIN users u ON u.user_id = s.user_id
        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
        UNION
        select "pur" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount, fn_get_rework_count(s.shoe_design_id) As ReworkCount
                from shoe_design s  INNER JOIN order_details od on od.design_id = s.shoe_design_id
                inner join `order` o on o.order_id = od.order_id AND payment_status = "Processed" INNER JOIN users u ON u.user_id = o.user_id
                inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
        ');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_savNfav_shoedesignsByUser($userId, $isPublic = true)
    {
        $where = array();
        if ($userId != null) {
            $where['u.user_id'] = $userId;
        } else {
            $where['s.is_public'] = $isPublic;
        }
        $result = $this->db->query(' select "save" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
                                        from shoe_design s
                                        INNER JOIN users u ON u.user_id = s.user_id
                                        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
                                        UNION
                                        select "fav" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
                                        from shoe_design s INNER JOIN design_likes d on d.shoe_design_id = s.shoe_design_id and d.like_type = "Favorite"
                                        INNER JOIN users u ON u.user_id = d.user_id
                                        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
                                    ');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_all_shoedesignsByUser($userId, $isPublic = true)
    {
        $where = array();
        if ($userId != null) {
            $where['u.user_id'] = $userId;
        } else {
            $where['s.is_public'] = $isPublic;
        }
        $result = $this->db->query(' select "save" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
                                        from shoe_design s
                                        INNER JOIN users u ON u.user_id = s.user_id
                                        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
                                        UNION
                                        select "fav" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
                                        from shoe_design s INNER JOIN design_likes d on d.shoe_design_id = s.shoe_design_id and d.like_type = "Favorite"
                                        INNER JOIN users u ON u.user_id = d.user_id
                                        inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
                                        UNION
                                        select "pur" as mode, s.shoe_design_id, s.image_file, s.public_name, s.name, s.user_id, u.first_name, l.last_name, ls.style_name  ,fn_get_like_count(s.shoe_design_id,"Like") As LikeCount,fn_get_like_count(s.shoe_design_id,"Favorite") As FavoriteCount,  fn_get_rework_count(s.shoe_design_id) As ReworkCount
                                                from shoe_design s  INNER JOIN order_details od on od.design_id = s.shoe_design_id
                                                inner join `order` o on o.order_id = od.order_id AND payment_status = "Processed" INNER JOIN users u ON u.user_id = o.user_id
                                                inner join last_item_def l on l.last_item_def_id = s.last_id inner join last_styles ls on ls.last_style_id = s.style_id where u.user_id = ' . $userId . '
                                ');
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_shoe_likes($designId)
    {
        $query = $this->db->query(' Select (select count(*) from  design_likes df inner join shoe_design s on df.shoe_design_id = s.shoe_design_id and df.like_type = "Like" and df.shoe_design_id = ' . $designId . ') As  design_likes,
          (select count(*) from  design_likes df inner join shoe_design s  on df.shoe_design_id = s.shoe_design_id and df.like_type = "Favorite" and df.shoe_design_id = ' . $designId . ') As  design_favs from shoe_design');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function getSavedModels($id)
    {
        $this->db->select('s.shoe_design_id, s.user_id, s.shoemodel, s.date_created, u.first_name, u.last_name, s.name, s.image_file');
        $this->db->from('shoe_design s');
        $this->db->join('users u', 'u.user_id = s.user_id', 'inner');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function ge_design_model($designId)
    {
        $this->db->select('shoemodel, public_name, name, image_file');
        $this->db->from('shoe_design');
        $this->db->where('shoe_design_id', $designId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result;
        }
        return null;
    }

    public function isPreviouslyOrderd($userId)
    {
        $this->db->select('left_shoe, right_shoe');
        $this->db->from('order_details od');
        $this->db->join('order o', 'od.order_id = o.order_id', 'inner');
        $this->db->where(array(
            'user_id' => $userId,
        ));
        $this->db->order_by('o.order_id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result;
        }
        return null;
    }

    public function get_SVG_details($last_style_id)
    {
        $this->db->select('svg_file, def_color');
        $this->db->from('last_styles');
        $this->db->where('last_style_id', $last_style_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result;
        }
        return null;
    }

    public function test($model, $name)
    {
        $mainangles = array("A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7");
        $thumbangles = array("A0", "A7");
        $this->createDesign($model, $thumbangles, $mainangles, ApplicationConfig::THUMB_WIDTH, ApplicationConfig::THUMB_HEIGHT, $name);
    }

    private function createDesign($model, $thumbangles, $mainangles, $thwidth, $thheight, $name)
    {
        if ($name != '' && $name != null) {
            $width = ApplicationConfig::IMG_WIDTH;
            $height = ApplicationConfig::IMG_HEIGHT;

            $imageBase = $_SERVER['DOCUMENT_ROOT'] . '/files/images/' . $model['last']['folder'] . "/" . $model['style']['folder'] . "/";
            $filedir = $_SERVER['DOCUMENT_ROOT'] . '/files/designs/';
            $ext = ".png";
            foreach ($mainangles as $angle) {
                $images = array();
                $base = imagecreatetruecolor($width, $height);
                $col = imagecolorallocatealpha($base, 255, 255, 255, 127);
                imagefill($base, 0, 0, $col);
                imagesavealpha($base, true);
                $basestr = $model['style']['code'] . "_" . $model['toe']['type'] . $model['vamp']['type'] . $model['eyestay']['type'] . $model['foxing']['type'];
                $images[] = array(
                    "folder" => $imageBase . $model['quarter']['matfolder'] . "/",
                    "name" => $basestr . $model['quarter']['material'] . $model['quarter']['color'] . "_" . $angle . $ext,
                );
                $images[] = array(
                    "folder" => $imageBase . $model['toe']['matfolder'] . "/",
                    "name" => $basestr . $model['toe']['material'] . $model['toe']['color'] . $model['toe']['part'] . "_" . $angle . $ext,
                );
                $images[] = array(
                    "folder" => $imageBase . $model['vamp']['matfolder'] . "/",
                    "name" => $basestr . $model['vamp']['material'] . $model['vamp']['color'] . $model['vamp']['part'] . "_" . $angle . $ext,
                );
                $images[] = array(
                    "folder" => $imageBase . $model['eyestay']['matfolder'] . "/",
                    "name" => $basestr . $model['eyestay']['material'] . $model['eyestay']['color'] . $model['eyestay']['part'] . "_" . $angle . $ext,
                );
                $images[] = array(
                    "folder" => $imageBase . $model['foxing']['matfolder'] . "/",
                    "name" => $basestr . $model['foxing']['material'] . $model['foxing']['color'] . $model['foxing']['part'] . "_" . $angle . $ext,
                );
                $images[] = array(
                    "folder" => $imageBase . $model['stitch']['folder'] . "/",
                    "name" => $basestr . $model['stitch']['code'] . "_" . $angle . $ext,
                );
                $images[] = array(
                    "folder" => $imageBase . $model['lace']['folder'] . "/",
                    "name" => $basestr . $model['lace']['code'] . "_" . $angle . $ext,
                );
                foreach ($images as $img) {
                    if (file_exists($img['folder'] . $img['name'])) {
                        $imgmain = imagecreatefrompng($img['folder'] . $img['name']);
                        imagecopyresized($base, $imgmain, 0, 0, 0, 0, $width, $height, $width, $height);
                    }
                }
                if (isset($model['monogram']) && ($angle == "A0" || $angle == "A4")) {
                    if (strlen($model['monogram']['text']) > 0) {
                        $left = ApplicationConfig::A4_LEFT;
                        $top = ApplicationConfig::A4_TOP;
                        if ($model['monogram']['leftSide'] == 1 && $angle == "A0") {
                            $left = ApplicationConfig::A0_LEFT;
                            $top = ApplicationConfig::A0_TOP;
                        }
                        if (($model['monogram']['leftSide'] == 1 && $angle == "A0") || ($model['monogram']['rightSide'] == 1 && $angle == "A4")) {
                            for ($i = 0; $i < strlen($model['monogram']['text']); $i++) {
                                $mono = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . '/files/images/monogram/' . $model['monogram']['text'][$i] . ".png");
                                list($mwidth, $mheight) = getimagesize($_SERVER['DOCUMENT_ROOT'] . '/files/images/monogram/' . $model['monogram']['text'][$i] . ".png");
                                imagecopyresized($base, $mono, $left, $top, 0, 0, $mwidth, $mheight, $mwidth, $mheight);
                                $left = $left + $mwidth;
                            }
                        }
                    }
                }
                imagepng($base, $filedir . $name . "_" . $angle . $ext);
                if (in_array($angle, $thumbangles)) {
                    $thumbbase = imagecreatetruecolor($thwidth, $thheight);
                    $col1 = imagecolorallocatealpha($thumbbase, 255, 255, 255, 127);
                    imagefill($thumbbase, 0, 0, $col1);
                    imagesavealpha($thumbbase, true);
                    $thumb = imagecreatefrompng($filedir . $name . "_" . $angle . $ext);
                    imagecopyresized($thumbbase, $thumb, 0, 0, 0, 0, $thwidth, $thheight, $width, $height);
                    imagepng($thumbbase, $filedir . "thumb/" . $name . "_" . $angle . $ext);
                }
            }

            if (isset($model['patina']) && $name != '' && $name != null) {
                $patina = $this->db->get_where('material_colors', array('material_id' => $model['patina']['matId'], 'color_code' => $model['patina']['color']))
                    ->row_array();

                $imgFile = $patina['color_image'];
                $base = imagecreatetruecolor($width, $height);
                $col = imagecolorallocatealpha($base, 255, 255, 255, 127);
                imagefill($base, 0, 0, $col);
                imagesavealpha($base, true);
                $imgmain = imagecreatefromjpeg($imgFile);
                $tmpSize = getimagesize($imgFile);
                imagecopyresized($base, $imgmain, 0, 0, 0, 0, $width, $height, $tmpSize[0], $tmpSize[1]);
                imagejpeg($base, $filedir . $name . "_" . $model['patina']['color'] . ".jpg");
            }
        }
    }

    public function insertfavlikes($favLike)
    {
        $this->db->insert('design_likes', $favLike);
        $result['fav'] = $this->db->query("select count(*) as cnt from design_likes where like_type = 'Favorite' and shoe_design_id = " . $favLike['shoe_design_id'] . "")->row_array();
        $result['like'] = $this->db->query("select count(*) as cnt from design_likes where like_type = 'like' and shoe_design_id = " . $favLike['shoe_design_id'] . "")->row_array();
        return $result;
    }

    public function IsfavlikesExists($designId, $type, $userId)
    {
        $this->db->Select('*');
        $this->db->from('design_likes');
        $this->db->where('like_type', $type);
        $this->db->where('shoe_design_id', $designId);
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function unPublish($designId)
    {
        $this->db->query(' UPDATE `shoe_design` SET `is_public` = 0 WHERE `shoe_design`.`shoe_design_id` = ' . $designId);
        return true;
    }

    public function renameShoe($designId, $shoe_name)
    {
        $this->db->query(' UPDATE `shoe_design` SET `public_name` = "' . $shoe_name . '" WHERE `shoe_design`.`shoe_design_id` = ' . $designId);
        return true;
    }

    public function DeleteSavedDesign($designId, $userId)
    {
        $this->db->select('order_id');
        $this->db->from('order_details');
        $this->db->where('design_id', $designId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return 0;
        }
        $this->db->query(' Delete from shoe_design where shoe_design_id = ' . $designId . ' and user_id = ' . $userId);
        return 1;
    }

    public function RemoveFromFav($designId, $userId)
    {
        $this->db->query(" Delete from design_likes where like_type = 'Favorite' and  shoe_design_id= " . $designId . " and user_id = " . $userId);
        return true;
    }

    public function getPromoEmail($email)
    {
        $this->db->select('*');
        $this->db->where('email', $email);
        $this->db->from('promo_users');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function isCommonPromocode($email, $promocode)
    {
        $promo_user_id = $this->insertPromoUser($email, $promocode);
        if ($promo_user_id != null) {
            $promoId = $this->getPromoId($promocode);
            $this->isPromoAssignExists($promo_user_id, $promoId);
        }
    }

    private function insertPromoUser($email, $promocode)
    {
        $this->db->select('promocode', 'discount_per', 'discount_amount');
        $this->db->from('promocode');
        $this->db->where('is_common', 1);
        $this->db->where('promocode', $promocode);
        $this->db->where('expiry_date >=', date('Y-m-d H:i:s'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $promoDetails = $query->result_array();
            $promoUserDetail = $this->getPromoEmail($email);
            if ($promoUserDetail == null) {
                $promoUser = array(
                    'email' => $email,
                    'add_date' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                );
                $this->db->insert('promo_users', $promoUser);
                $promoUserDetail[0]['id'] = $this->db->insert_id();
            }
            return $promoUserDetail[0]['id'];
        } else {
            return null;
        }
    }

    private function getPromoId($promoCode)
    {
        $this->db->select('promo_id');
        $this->db->from('promocode');
        $this->db->where('promocode', $promoCode);
        $this->db->where('expiry_date >=', date('Y-m-d H:i:s'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $promoDetails = $query->result_array();
            return $promoDetails[0]['promo_id'];
        } else {
            return 2;
        }
    }

    private function isPromoAssignExists($promo_user_id, $promoId)
    {
        $this->db->select('*');
        $this->db->where('promouser_id', $promo_user_id);
        $this->db->where('promocode_id', $promoId);
        $this->db->from('promo_assign');

        $query = $this->db->get();
        if ($query->num_rows() < 1) {
            $promoAssign = array(
                'promocode_id' => $promoId, // $promoDetails[0]['promo_id'],
                'promouser_id' => $promo_user_id, // $promoUserDetail[0]['id'],
                'is_used' => 0,
            );
            $this->db->insert('promo_assign', $promoAssign);
        }
    }

    public function getvalidPromocodes($promouser_id, $promocode)
    {
        $promoId = $this->getPromoId($promocode);

        $this->db->select('promocode_id,promocode,discount_per,discount_amount,assign_id,maximum_value', false);
        $this->db->where('(pa.is_used = 0 OR p.unlimited = 1)');
        $this->db->where('promouser_id', $promouser_id);
        $this->db->where('promocode', $promocode);
        $this->db->where('expiry_date >=', date('Y-m-d H:i:s'));
        $this->db->from('promocode p');
        $this->db->join('promo_assign pa', 'pa.promocode_id = p.promo_id', 'INNER');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function updatepromoAssign($assignId, $order_id)
    {
        $this->db->set('is_used', 1);
        $this->db->where('assign_id', $assignId);
        $this->db->update('promo_assign');

        $this->db->select('promocode_id');
        $this->db->where('assign_id', $assignId);
        $this->db->from('promo_assign');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $promocode_id = $result['promocode_id'];
            $this->db->set('is_common', 0);
            $this->db->where('promo_id', $promocode_id);
            $this->db->update('promocode');

            $this->db->set('promocode_id', $promocode_id);
            $this->db->where('order_id', $order_id);
            $this->db->update('order');

            $this->db->select('promocode');
            $this->db->where('promo_id', $promocode_id);
            $this->db->from('promocode');
            $query = $this->db->get();
            $result = $query->row_array();

            if (strtolower($result['promocode']) == "v-day") {
                $this->send_promo_card_email($order_id, $promocode_id);
            }
        }
    }

    public function send_promo_card_email($order_id, $promocode_id)
    {
        $options = $this->OrderModel->promo_card_order($order_id, $promocode_id);
        $this->load->library('email');

        $data['email'] = $options['email'];
        $data['promocode'] = $options['promocode'];

        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to($this->config->item('developer_email'));

        $this->email->bcc($this->config->item('developer_email'));
        $this->email->subject(' promocode used by ' . $data['email']);

        $msg = $this->load->view('emails/promoCodeEmail', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);

        $this->email->send();
    }

    public function validateUniquename($name)
    {
        $this->db->select('public_name');
        $this->db->where('public_name', $name);
        $this->db->from('shoe_design');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getCommonPromoCodeDet($promocode)
    {
        $this->db->select('promocode,discount_per,discount_amount,-1 as assign_id', false);
        $this->db->where('is_common', 1);
        $this->db->where('promocode', $promocode);
        $this->db->where('expiry_date >=', date('Y-m-d H:i:s'));
        $this->db->from('promocode p');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function isCommonPromo($promocode)
    {
        $this->db->select('promo_id');
        $this->db->from('promocode');
        $this->db->where(array(
            'promocode' => $promocode,
            'is_common' => 1,
        ));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateShoeDesign($model, $user_id = null)
    {
        $details = $this->ge_design_model($model['shoeDesignId']);
        $mainangles = array("A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7");
        $thumbangles = array("A0", "A7");
        $imgname = $details[0]['image_file'];
        $this->createDesign($model, $thumbangles, $mainangles, ApplicationConfig::THUMB_WIDTH, ApplicationConfig::THUMB_HEIGHT, $imgname);
        $shoe = array(
            'shoemodel' => json_encode($model),
            'last_id' => $model['last']['id'],
            'style_id' => $model['style']['id'],
            'q_base' => $model['quarter']['base'],
            'q_mat_id' => $model['quarter']['matId'],
            'q_clr_id' => $model['quarter']['clrId'],
            'toe_id' => $model['toe']['toeId'],
            't_mat_id' => $model['toe']['matId'],
            't_clr_id' => $model['toe']['clrId'],
            'vamp_id' => $model['vamp']['vampId'],
            'v_mat_id' => $model['vamp']['matId'],
            'v_clr_id' => $model['vamp']['clrId'],
            'eyestay_id' => $model['eyestay']['eyestayId'],
            'e_mat_id' => $model['eyestay']['matId'],
            'e_clr_id' => $model['eyestay']['clrId'],
            'foxing_id' => $model['foxing']['foxingId'],
            'f_mat_id' => $model['foxing']['matId'],
            'f_clr_id' => $model['foxing']['clrId'],
            'stitch_id' => $model['stitch']['id'],
            'lace_id' => $model['lace']['id'],
            'size_left_val' => $model['size']['left']['value'],
            'size_left_text' => $model['size']['left']['text'],
            'size_right_val' => $model['size']['right']['value'],
            'size_right_text' => $model['size']['right']['text'],
            'user_id' => $user_id,
        );
        $this->db->where('shoe_design_id', $model['shoeDesignId']);
        $this->db->update('shoe_design', $shoe);
        if (isset($model['monogram'])) {
            $this->updateDesignMonogram($model['monogram'], $model['shoeDesignId']);
        }
        return $details;
    }

    public function get_shoe_structure()
    {
        $model = array();
        if ($this->session->userdata('ShoeModel')) {
            $model = $this->session->userdata('ShoeModel');
        }
        $userdetails = $this->session->userdata('user_details');

        if ($userdetails != false) {
            $user_id = $userdetails['user_id'];
            $latest = $this->get_latest_design($user_id);

            if (!$latest || !is_array($latest)) {
                $this->load->model('customshoemodel');
                $userShoeSize = $this->customshoemodel->get_shoe_size();
                $latest['m_lsize'] = $userShoeSize[0]['left_length'];
                $latest['m_lwidth'] = $userShoeSize[0]['left_height'];
                $latest['m_lgirth'] = $userShoeSize[0]['left_girth'];
                $latest['m_linstep'] = $userShoeSize[0]['left_instep'];
                $latest['m_rsize'] = $userShoeSize[0]['right_length'];
                $latest['m_rwidth'] = $userShoeSize[0]['right_height'];
                $latest['m_rinstep'] = $userShoeSize[0]['right_instep'];
                $latest['m_rgirth'] = $userShoeSize[0]['right_girth'];

                $latest['m_lheel'] = $userShoeSize[0]['left_heel'];
                $latest['m_rheel'] = $userShoeSize[0]['right_heel'];
                $latest['m_lankle'] = $userShoeSize[0]['left_ankle'];
                $latest['m_rankle'] = $userShoeSize[0]['right_ankle'];

                $latest['size_left_val'] = $userShoeSize[0]['left_size'];
                $latest['size_left_text'] = $userShoeSize[0]['left_size'];
                $latest['size_left_width'] = $userShoeSize[0]['left_width'];
                $latest['size_right_val'] = $userShoeSize[0]['right_size'];
                $latest['size_right_text'] = $userShoeSize[0]['right_size'];
                $latest['size_right_width'] = $userShoeSize[0]['right_width'];
            }
        }

        $model['angle'] = (!isset($model['angle'])) ? 'A0' : $model['angle'];
        $model['ext'] = (!isset($model['ext'])) ? '.png' : $model['ext'];

        if (!isset($model['last'])) {
            $model['last'] = ['id' => '4', 'code' => 'L4', 'folder' => 'L4', 'last_name' => 'The Harvey'];
        }
        if (!isset($model['style'])) {
            $model['style'] = ['id' => '14', 'code' => 'OX_L4', 'folder' => 'Oxford', 'styleName' => 'Oxford', 'svg' => 'Oxford', 'isVampEnabled' => '1', 'color' => 'C0', 'isLaceEnabled' => '1'];
        }
        if (!isset($model['quarter'])) {
            $model['quarter'] = ['base' => 'OX_L4_T00V00E00F00M0C0', 'material' => 'M0', 'matfolder' => 'CP', 'matId' => '1', 'clrId' => '1', 'color' => 'C0'];
        }
        if (!isset($model['toe'])) {
            $model['toe'] = ['type' => 'T00', 'toeId' => '1', 'part' => 'P0', 'isMaterial' => '0', 'isNothing' => '1', 'material' => 'M0', 'matfolder' => 'CP', 'matId' => '1', 'clrId' => '1', 'color' => 'C0'];
        }
        if (!isset($model['vamp'])) {
            $model['vamp'] = ['type' => 'V00', 'vampId' => '15', 'part' => 'P1', 'isMaterial' => '0', 'isNothing' => '1', 'material' => 'M0', 'matfolder' => 'CP', 'matId' => '1', 'clrId' => '1', 'color' => 'C0'];
        }
        if (!isset($model['eyestay'])) {
            $model['eyestay'] = ['type' => 'E00', 'eyestayId' => '21', 'part' => 'P2', 'isMaterial' => '0', 'isNothing' => '1', 'material' => 'M0', 'matfolder' => 'CP', 'matId' => '1', 'clrId' => '1', 'color' => 'C0'];
        }
        if (!isset($model['foxing'])) {
            $model['foxing'] = ['type' => 'F00', 'foxingId' => '48', 'part' => 'P3', 'isMaterial' => '0', 'isNothing' => '1', 'material' => 'M0', 'matfolder' => 'CP', 'matId' => '1', 'clrId' => '1', 'color' => 'C0'];
        }
        if (!isset($model['stitch'])) {
            $model['stitch'] = ['code' => 'S0', 'id' => '1', 'name' => 'Black', 'folder' => 'CP'];
        }
        if (!isset($model['lace'])) {
            $model['lace'] = ['code' => 'L0', 'id' => '1', 'name' => 'Black', 'folder' => 'CP'];
        }
        if (!isset($model['measurement'])) {
            if (isset($latest) && $latest) {
                $model['measurement'] = array(
                    'left' => array('size' => $latest['m_lsize'], 'width' => $latest['m_lwidth'], 'girth' => $latest['m_lgirth'], 'instep' => $latest['m_linstep'], 'heel' => $latest['m_lheel'], 'ankle' => $latest['m_lankle']),
                    'right' => array('size' => $latest['m_rsize'], 'width' => $latest['m_rwidth'], 'girth' => $latest['m_rgirth'], 'instep' => $latest['m_rinstep'], 'heel' => $latest['m_rheel'], 'ankle' => $latest['m_rankle']),
                );
            } else {
                $model['measurement'] = array(
                    'left' => array('size' => '', 'width' => '', 'girth' => '', 'instep' => '', 'heel' => '', 'ankle' => ''),
                    'right' => array('size' => '', 'width' => '', 'girth' => '', 'instep' => '', 'heel' => '', 'ankle' => ''),
                );
            }
        }
        if (!isset($model['sole'])) {
            $model['sole'] = array('code' => "S1", 'color' => "C1", 'colorId' => "1", 'id' => "1", 'stitch' => "ST1", 'stitchCharge' => "0", 'stitchId' => "1");
        }
        if (!isset($model['size']) || !isset($model['size']['left']) || !isset($model['size']['right'])) {

            if (isset($latest) && $latest && isset($latest['size_left_val']) && isset($latest['size_right_val'])) {
                $model['size'] = array(
                    'left' => array('value' => $latest['size_left_val'], 'text' => $latest['size_left_text'], 'width' => $latest['size_left_width']),
                    'right' => array('value' => $latest['size_right_val'], 'text' => $latest['size_right_text'], 'width' => $latest['size_right_width']),
                );
            } else {
                $model['size'] = array(
                    'left' => array('value' => '', 'text' => '', 'width' => ''),
                    'right' => array('value' => '', 'text' => '', 'width' => ''),
                );
            }
        }

        return $model;
    }

}
