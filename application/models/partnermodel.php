<?php

class PartnerModel extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_partner_details($user_id)
    {
        $this->db->select('p.*,a.file_name');
        $this->db->from('partners p');
        $this->db->join('partner_users pu', 'p.partner_id = pu.partner_id');
        $this->db->join('attachments a', 'p.attachment_id = a.id', 'left');
        $this->db->where('pu.user_id', $user_id);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->row_array();
            return $result;
        }
        return false;
    }

    public function get_partner_designs($partner_id, $style, $last)
    {
        $this->db->select('sd.image_file,sd.shoe_design_id,pd.design_name');
        $this->db->from('partner_designs pd');
        $this->db->join('last_styles ls', 'ls.last_style_id = pd.style_id');
        $this->db->join('shoe_design sd', 'sd.shoe_design_id = pd.shoe_design_id');
        $this->db->where('pd.partner_id', $partner_id);
        $this->db->where('sd.last_id', $last);
        $this->db->where('pd.source', 'template');
        $this->db->where('pd.is_active', 1);
        $this->db->like('ls.style_name', $style);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->result_array();
            return $result;
        }
        return false;
    }

    public function get_partner_designs_by_design_number($partner_id, $design_number)
    {
        $this->db->select('sd.image_file,sd.shoe_design_id,pd.design_name,cc.attachment_ids');
        $this->db->from('partner_designs pd');
        $this->db->join('last_styles ls', 'ls.last_style_id = pd.style_id');
        $this->db->join('shoe_design sd', 'sd.shoe_design_id = pd.shoe_design_id');
        $this->db->join('custom_collection cc', 'sd.shoe_design_id = cc.shoe_design_id', 'left');
        $this->db->where('pd.partner_id', $partner_id);
        $this->db->where('pd.source', 'shop');
        $this->db->where('pd.is_active', 1);
        $this->db->where('pd.design_name', $design_number);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->result_array();
            return $result;
        }
        return false;
    }

    public function getPartnerOrders($partner_id)
    {

        $this->db->select('o.order_id,o.invoice_no, o.gross_amt, o.discount_perc, o.discount, o.tax_amount, o.order_date, order_modified,
			u.first_name,u.last_name, o.belt_needed, o.shoe_horn,o.shoe_trees,kickstarter_no,
			if(os.status = "order shipped", os.add_date,"") AS shipped_date,
				if(os.status = "order shipped", os.status_desc,"") AS status_desc,
					if(os.status = "order shipped", DATEDIFF(os.add_date,o.order_date) ,"") AS day_diff,
						os.status,
					(SELECT CONCAT(city,", ", state,", ", country) FROM shipping_details WHERE o.order_id = order_id limit 1) AS location,
					GROUP_CONCAT( " ",ls.style_name) style_name,

					sum(item_amt*quantity) total_amt,  SUM( quantity) item_count,
					sum(1) as number_of_items  ', false);
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id AND is_active = 1', 'inner');
        $this->db->join('order_details od', "od.order_id = o.order_id AND od.item_name NOT LIKE  'gift card' ", 'INNER');
        $this->db->join('shoe_design sd', 'od.design_id = sd.shoe_design_id');
        $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id');
        $this->db->join('partner_orders po', 'po.order_id = o.order_id');
        $this->db->where('o.payment_status', 'Processed');
        $this->db->group_by('o.order_id');
        $this->db->order_by('o.order_id', 'DESC');
        $query = $this->db->get();
        $order['rows'] = $query->result_array();
        return $order;
    }

    public function get_partner_orders()
    {
        $this->db->select('o.order_id', false);
        $this->db->from('order o');
        $this->db->join('partner_orders po', 'po.order_id = o.order_id');
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);
        return $order_ids;
    }
    public function get_partner_orderId($partner_id)
    {
        $this->db->select('o.order_id', false);
        $this->db->from('order o');
        $this->db->join('partner_orders po', 'po.order_id = o.order_id');
        $this->db->where('po.partner_id', $partner_id);
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);

        return $order_ids;
    }
    public function get_partners()
    {
        $this->db->select('partner_id id, partner_name name');
        $this->db->from('partners');
        $this->db->where('is_active', 1);
        $query = $this->db->get();
        $partner = $query->result_array();

        return $partner;
    }

}
