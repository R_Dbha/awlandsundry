<?php

class CommonModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_custom_collections($style_name, $type_id = '')
    {
        $this->db->select(' *, l.last_name,cct.name,cc.id as custom_collection_id,');
        $this->db->from('custom_collection cc');
        $this->db->join('last_item_def l', 'l.last_item_def_id = cc.last_id');
        $this->db->join('custom_collection_types cct', 'cc.type_id = cct.id', 'inner');
        $this->db->like('cc.style_name', $style_name);
        $this->db->where('cc.type_id', $type_id);
        $this->db->order_by('cc.sort_order');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function get_readywear_collections($style_name)
    {
        $query = $this->db->select('*, id as ready_wear_id')
            ->like('LOWER(style_name)', $style_name)
            ->order_by('rw.id')->get('ready_wear');

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function insert_referral_discount($email, $amount)
    {
        $query = $this->db->get_where('store_credit', ['user_email' => $email]);

        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            $this->db->where(['user_email' => $email]);
            $this->db->set('credit_amount', 'credit_amount +' . $amount, false);
            $this->db->update('store_credit');
        } else {
            $query = $this->db->select('user_id')->get_where('users', ['email' => $email]);
            if ($query->num_rows() > 0) {
                $row = $query->row_array();
                $user_id = $row['user_id'];
                $this->db->insert('store_credit', array(
                    'user_id' => $user_id,
                    'user_email' => $email,
                    'credit_amount' => $amount,
                    'date_added' => date('Y-m-d H:i:s'),
                    'date_modified' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                ));
            }
        }
    }

    public function is_already_referred($referrer_email, $referred_email)
    {
        $this->db->select('referral_trans_id');
        $this->db->from('referral_trans');
        $this->db->where('referrer_email', $referrer_email);
        $this->db->or_where('referred_email', $referred_email);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function insert_resferral_trans($referrer)
    {
        $user = $this->session->userdata('user_details');
        $this->db->insert('referral_trans', ['referrer_email' => $referrer, 'referred_email' => $user['email']]);
    }

    public function get_custom_collection_item_by_name($style, $type, $name)
    {
        $this->db->select('cc.id');
        $this->db->from('custom_collection cc');
        $this->db->join('custom_collection_types cct', 'cc.type_id = cct.id');
        $this->db->like('cc.style_name', $style, 'none');
        $this->db->like('cct.name', $type, 'none');
        $this->db->like('cc.shoe_name', $name, 'none');
        $this->db->where('cc.is_active', '1');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['id'];
        }
        return null;
    }

    public function get_special_item_by_name($name)
    {
        $this->db->select('cc.id');
        $this->db->from('custom_collection cc');
        $this->db->join('custom_collection_types cct', 'cc.type_id = cct.id');
        $this->db->like('cc.shoe_name', $name, 'none');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['id'];
        }
        return null;
    }

    public function get_custom_collection_item($item_id)
    {
        $this->db->select('cc.*,l.last_name,ls.folder_name');
        $this->db->from('custom_collection cc');
        $this->db->join('last_item_def l', 'l.last_item_def_id = cc.last_id');
        $this->db->join('shoe_design sd', 'sd.shoe_design_id = cc.shoe_design_id');
        $this->db->join('last_styles ls', 'sd.style_id = ls.last_style_id');
        $this->db->where('id', $item_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function get_styles()
    {
        $this->db->select('style_name as id, style_name as name');
        $this->db->from('custom_collection_types');
        $this->db->group_by('style_name');
        $this->db->order_by('id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_custom_collection_types($style_name)
    {
        $this->db->select('*');
        $this->db->from('custom_collection_types');
        $this->db->like('style_name', $style_name, 'none');
        $this->db->order_by('`order`');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_custom_collections_by_type($type_id = '')
    {
        $this->db->select('*, l.last_name,cct.name');
        $this->db->from('custom_collection cc');
        $this->db->join('last_item_def l', 'l.last_item_def_id = cc.last_id');
        $this->db->join('custom_collection_types cct', 'cc.type_id = cct.id', 'inner');
        $this->db->where('cc.type_id', $type_id);
        $this->db->order_by('cc.sort_order');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function get_style_details($style_name)
    {
        $this->db->select('last_style_id, style_name, style_description');
        $this->db->from('last_styles');
        $this->db->like('style_name', $style_name);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function get_custom_images($item)
    {
        $attachment_ids = explode(',', $item['attachment_ids']);
        $this->db->select('file_name');
        $this->db->from('attachments a');
        $this->db->where_in('a.id', $attachment_ids);
        $query = $this->db->get();
        $image = array();
        if ($query->num_rows() > 0) {
            $path = ApplicationConfig::ROOT_BASE . 'files/';
            $result = $query->result_array();
            foreach ($result as $row) {
                $image[] = $path . $row['file_name'];
            }
            return $image;
        }

        $this->db->select('image_file');
        $this->db->from('shoe_design sd');
        $this->db->where('sd.shoe_design_id', $item['shoe_design_id']);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $path = ApplicationConfig::ROOT_BASE . 'files/designs/';
            $url = $path . $result['image_file'] . '_A7.png';
            $image[] = $url;
            return $image;
        }
        return array('');
    }

    private function remote_file_exists($url)
    {
        return true;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_NOBODY, true); // don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 100);
        $result = curl_exec($curl);
        $ret = false;
        if ($result !== false) {
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($statusCode == 200) {
                $ret = true;
            }
        }
        curl_close($curl);
        return $ret;
    }

    public function insert_feedback($data)
    {
        $this->db->insert('feedback', $data);
    }

    public function get_similar_shoes_special($item_id, $type_id)
    {
        $this->db->select('*', false);
        $this->db->from('custom_collection cc');
        $this->db->join('custom_collection_types cct', 'cc.type_id = cct.id');
        $this->db->where('cc.id <> ', $item_id);
        $this->db->where('cc.type_id  ', $type_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $result[$key]['images'] = $this->get_custom_images($value);
                $result[$key]['link'] = 'special/' . $value['style_name'] . '/' . $value['shoe_name'];
            }
            return $result;
        }
        return null;
    }

    public function get_similar_shoes($item_id)
    {
        $this->db->select('price, shoe_name,style_name, last_id, type_id');
        $this->db->from('custom_collection cc');
        $this->db->where('id', $item_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $criteria = $query->row_array();

            $types = $this->get_custom_collection_types($criteria['style_name']);
            $custom_sort_order = sizeof($types) + 1;
            $order_string = " CASE type_id ";
            $inc = -1;
            foreach ($types as $key => $type) {
                if ($criteria['type_id'] === $type['id']) {
                    $inc = 1;
                    $custom_sort_order = 0;
                }
                $custom_sort_order += $inc;
                $order_string .= " WHEN " . $type['id'] . " THEN " . $custom_sort_order;
            }
            $order_string .= " END";
            $this->db->_protect_identifiers = false;
            $this->db->select('*', false);
            $this->db->from('custom_collection cc');
            $this->db->join('custom_collection_types cct', 'cc.type_id = cct.id');
            $this->db->where('cc.id <> ', $item_id);
            $this->db->not_like('cct.style_name', 'valentine');
            $this->db->where('price > ', $criteria['price'] - ($criteria['price'] / 10));
            $this->db->where('price < ', $criteria['price'] + ($criteria['price'] / 10));
            $this->db->like('cc.style_name', $criteria['style_name']);
            $this->db->order_by($order_string);
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                foreach ($result as $key => $value) {
                    $result[$key]['images'] = $this->get_custom_images($value);
                    $result[$key]['link'] = 'products/' . $value['style_name'] . '/' . $value['name'] . '/' . $value['shoe_name'];
                }
                return $result;
            }
            return null;
        }

        return null;
    }

    public function get_collections($filters, $config)
    {
        $this->db->select('cc.id,shoe_name,cc.style_name,
                           lid.last_name,cct.name,cc.shoe_design_id,
                           qmd.material_name as quarter_material,
                           tmd.material_name as toe_material,
                           vmd.material_name as vamp_material,
                           emd.material_name as eyestay_material,
                           fmd.material_name as foxing_material,
                           qmc.color_name as quarter_color,
                           tmc.color_name as toe_color,
                           vmc.color_name as vamp_color,
                           emc.color_name as eyestay_color,
                           fmc.color_name as foxing_color,
                           ls.is_vamp_enabled,attachment_ids,cc.price');
        $this->db->from('custom_collection cc');
        $this->db->join('custom_collection_types cct', 'cc.type_id = cct.id');
        $this->db->join('shoe_design sd', 'sd.shoe_design_id = cc.shoe_design_id');
        $this->db->join('last_styles ls', 'sd.style_id = ls.last_style_id');
        $this->db->join('last_item_def lid', 'sd.last_id = lid.last_item_def_id');

        $this->db->join('style_properties tsp', 'sd.toe_id = tsp.property_type_id AND tsp.last_style_id = sd.style_id', 'left');
        $this->db->join('style_properties vsp', 'sd.vamp_id = vsp.property_type_id AND vsp.last_style_id = sd.style_id', 'left');
        $this->db->join('style_properties esp', 'sd.eyestay_id = esp.property_type_id AND esp.last_style_id = sd.style_id', 'left');
        $this->db->join('style_properties fsp', 'sd.foxing_id = fsp.property_type_id AND fsp.last_style_id = sd.style_id', 'left');

        $this->db->join('material_def qmd', 'sd.q_mat_id = qmd.material_def_id  and cc.is_editable = 1', 'left');
        $this->db->join('material_def tmd', 'sd.t_mat_id = tmd.material_def_id AND tsp.is_material = 1 and (tsp.is_material = 1 OR tsp.is_nothing = 0) and  cc.is_editable = 1', 'left');
        $this->db->join('material_def vmd', 'sd.v_mat_id = vmd.material_def_id AND (vsp.is_material = 1 OR vsp.is_nothing = 0) and is_vamp_enabled = 1  and cc.is_editable  = 1', 'left');
        $this->db->join('material_def emd', 'sd.e_mat_id = emd.material_def_id AND esp.is_material = 1  and (esp.is_material = 1 OR esp.is_nothing = 0) and cc.is_editable  = 1', 'left');
        $this->db->join('material_def fmd', 'sd.f_mat_id = fmd.material_def_id AND fsp.is_material = 1  and (fsp.is_material = 1 OR fsp.is_nothing = 0) and  cc.is_editable  = 1', 'left');
        $this->db->join('material_colors qmc', 'sd.q_clr_id = qmc.material_color_id  and cc.is_editable', 'left');
        $this->db->join('material_colors tmc', 'sd.t_clr_id = tmc.material_color_id AND tsp.is_material = 1  and (tsp.is_material = 1 OR tsp.is_nothing = 0) and  cc.is_editable = 1 ', 'left');
        $this->db->join('material_colors vmc', 'sd.v_clr_id = vmc.material_color_id AND (vsp.is_material = 1 OR vsp.is_nothing = 0) and is_vamp_enabled = 1  and cc.is_editable = 1', 'left');
        $this->db->join('material_colors emc', 'sd.e_clr_id = emc.material_color_id AND esp.is_material = 1  and (esp.is_material = 1 OR esp.is_nothing = 0) and  cc.is_editable = 1', 'left');
        $this->db->join('material_colors fmc', 'sd.f_clr_id = fmc.material_color_id AND fsp.is_material = 1  and (fsp.is_material = 1 OR fsp.is_nothing = 0) and   cc.is_editable = 1 ', 'left');

        $this->db->where('cc.is_active = 1');
        $this->db->where('cc.type_id <> 1000');
        $this->db->where('cc.type_id <> 19');
        $this->db->where('cc.type_id <> 20');
        $this->db->where('cc.type_id <> 21');
        $conditions = '';
        if (isset($filters['type']) && sizeof($filters['type'])) {
            foreach ($filters['type'] as $key => $type) {
                $conditions .= "cct.name LIKE '" . $type . "' OR ";
            }
            $conditions = ' (' . substr($conditions, 0, -3) . ') ';
            $this->db->where($conditions, '', false);
            $conditions = '';
        }

        if (isset($filters['style']) && sizeof($filters['style'])) {
            foreach ($filters['style'] as $key => $style) {
                $conditions .= "cc.style_name LIKE '" . $style . "' OR ";
            }
            $conditions = ' (' . substr($conditions, 0, -3) . ') ';
            $this->db->where($conditions, '', false);
            $conditions = '';
        }

        if (isset($filters['last']) && sizeof($filters['last'])) {
            foreach ($filters['last'] as $key => $last) {
                $conditions .= "lid.last_name LIKE '" . $last . "' OR ";
            }
            $conditions = ' (' . substr($conditions, 0, -3) . ') ';
            $this->db->where($conditions, '', false);
            $conditions = '';
        }

        if (isset($filters['material']) && sizeof($filters['material'])) {
            foreach ($filters['material'] as $key => $material) {
                $conditions .= "qmd.material_name LIKE '" . $material . "' OR "
                    . "tmd.material_name LIKE '" . $material . "' OR "
                    . "vmd.material_name LIKE '" . $material . "' OR "
                    . "emd.material_name LIKE '" . $material . "' OR "
                    . "fmd.material_name LIKE '" . $material . "' OR ";
            }
            $conditions = ' (' . substr($conditions, 0, -3) . ') ';
            $this->db->where($conditions, '', false);
            $conditions = '';
        }

        if (isset($filters['color']) && sizeof($filters['color'])) {
            foreach ($filters['color'] as $key => $color) {
                $conditions .= "qmc.material_color_id = '" . $color . "' OR "
                    . "tmc.material_color_id = '" . $color . "' OR "
                    . "vmc.material_color_id = '" . $color . "' OR "
                    . "emc.material_color_id = '" . $color . "' OR "
                    . "fmc.material_color_id = '" . $color . "' OR ";
            }
            $conditions = ' (' . substr($conditions, 0, -3) . ') ';
            $this->db->where($conditions, '', false);
            $conditions = '';
        }

        if (isset($filters['shoe_name']) && $filters['shoe_name'] !== '') {
            $conditions .= "cc.shoe_name LIKE '%" . $filters['shoe_name'] . "%'";
            $this->db->where($conditions, '', false);
            $conditions = '';
        }

        $this->db->limit($config['limit'], $config['start']);
        $query = $this->db->get();

        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_material_colors()
    {
        $this->db->select('mc.color_name,mc.color_image, md.material_folder');
        $this->db->from('material_colors mc');
        $this->db->join('material_def md', 'md.material_def_id = mc.material_id');
        $this->db->group_by('color_name');
        $query = $this->db->get();

        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_materials()
    {
        $this->db->select('*');
        $this->db->from('material_def');
        $query = $this->db->get();

        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_materials_by_material_id($material_id)
    {
        $this->db->select('mc.material_color_id, mc.color_name,mc.color_image, md.material_folder');
        $this->db->from('material_colors mc');
        $this->db->join('material_def md', 'md.material_def_id = mc.material_id');
        $this->db->where('material_id', $material_id);
        $query = $this->db->get();

        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_ready_wear()
    {
        $this->db->select('*');
        $this->db->from('ready_wear');
        $this->db->where('is_active', 1);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function get_ready_wear_item_by_name($color, $name)
    {
        $this->db->select('id');
        $this->db->from('ready_wear');
        $this->db->where('shoe_name', $name);
        $this->db->where('color', $color);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['id'];
        }
        return ssNULL;
    }

    public function get_ready_wear_item($item_id)
    {
        $query = $this->db->get_where('ready_wear', ['id' => $item_id]);

        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return null;
    }

    public function get_similar_ready_wear_shoe($item)
    {
        $query = $this->db->get_where('ready_wear', ['shoe_name' => $item['shoe_name']]);

        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $key = 0;
            foreach ($result as $row) {
                $activeColorClass = "";
                $result[$key]['images'] = $this->get_custom_images($row);
                $result[$key]['color'] = $row['color'];
                if ($row['id'] == $item['id']) {
                    $activeColorClass = 'active-color';
                }
                $result[$key]['colorClass'] = $activeColorClass;
                $key++;
            }
            return $result;
        }
        return null;
    }

    public function get_ready_wear_img_src($details)
    {
        $query = $this->db->get_where('ready_wear', ['shoe_name' => $details['shoe_name'], 'color' => $details['shoe_color']]);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $images = $this->get_custom_images($result);
            return $images;
        }
        return null;
    }

}
