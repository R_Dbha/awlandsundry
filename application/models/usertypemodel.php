<?php

class usertypemodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_typename($name)
    {
        $this->db->select('user_type_id, type_name');
        $this->db->from('user_type');
        $this->db->where(array('type_status' => $name));
        $query = $this->db->get();
        return $query->row_array();
    }
}
