<?php

class ordermodel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function saveorder($order)
    {
        $order_main = array();
        $items = array();
        $order_main = $order['order_main'];

        $this->db->trans_begin();
        $this->db->insert('order', $order_main);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $batch = array();
            $items = $order['items'];
            $order_id = $this->db->insert_id();
            foreach ($items as $item) {
                $item['order_id'] = $order_id;
                $batch[] = $item;
            }

            $this->db->insert_batch('order_details', $batch);

            $this->db->set('last_inserted_no', '`last_inserted_no`+1', false);
            $this->db->update('configure');
            $this->db->trans_commit();
            return $order_id;
        }
    }

    public function updateorder($order)
    {
        $order_main = array();
        $items = array();
        $order_main = $order['order_main'];
        $order_id = $order_main['order_id'];

        $this->db->trans_begin();
        $this->db->where('order_id', $order_main['order_id']);
        $this->db->update('order', $order_main);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $batch = array();
            $items = $order['items'];
            foreach ($items as $item) {
                $item['order_id'] = $order_id;
                $batch[] = $item;
            }
            if ($this->db->insert_batch('order_details', $batch)) {
                $this->db->trans_commit();
                return $order_id;
            } else {
                return false;
            }
        }
    }

    public function getinvoiceno()
    {
        $query = $this->db->get('configure');
        foreach ($query->result() as $row) {
            return $row->prefix . ($row->last_inserted_no + 1);
        }
    }

    public function savebilling($data)
    {
        $billing = $data['billing'];
        $shipping = $data['shipping'];
        $this->db->trans_begin();
        $this->db->insert('billing_details', $billing);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $this->db->insert('shipping_details', $shipping);
            $this->db->trans_commit();
            if ($billing != null) {
                $this->updatePrimaryBilling($billing);
            }
            if ($shipping != null) {
                $this->updatePrimaryShipping($shipping);
            }
        }
    }

    public function updatePrimaryBilling($billing)
    {
        $this->db->select('*');
        $this->db->where('user_id', $billing['user_id']);
        $this->db->where('is_primary', 1);
        $this->db->from('billing_details');
        $query = $this->db->get();
        if ($billing['zipcode'] != 0) {
            if ($query->num_rows > 0) {
                $this->db->where('user_id', $billing['user_id']);
                $this->db->where('is_primary', 1);
                $this->db->update('billing_details', $billing);
            } else {
                $billing['is_primary'] = 1;
                $this->db->insert('billing_details', $billing);
            }
        }
    }

    public function updatePrimaryShipping($shipping)
    {
        $this->db->select('*');
        $this->db->where('user_id', $shipping['user_id']);
        $this->db->where('is_primary', 1);
        $this->db->from('shipping_details');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $this->db->where('user_id', $shipping['user_id']);
            $this->db->where('is_primary', 1);
            $this->db->update('shipping_details', $shipping);
        } else {
            $shipping['is_primary'] = 1;
            $this->db->insert('shipping_details', $shipping);
        }
    }

    public function order_summary($order_id)
    {
        $this->db->select('count(od.order_id) number_of_items, sum(od.item_amt * od.quantity) subtotal ');
        $this->db->from('order_details od');
        $this->db->where('od.order_id', $order_id);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            return $query->row_array();
        }
        return false;
    }

    public function getbillingNshippingDetails($order_id)
    {
        $arr = array();

        $this->db->select('b.*, u.email');
        $this->db->from('billing_details b');
        $this->db->join('users u', 'u.user_id = b.user_id', 'inner');
        $this->db->where('order_id', $order_id);
        $query = $this->db->get();
        $arr['billing'] = $query->result_array();

        $this->db->where('order_id', $order_id);
        $query1 = $this->db->get('shipping_details');
        $arr['shipping'] = $query1->result_array();

        $query2 = $this->db->query('Select o.order_id, s.q_base, o.discount_perc, o.kickstarter_no, o.user_id, o.order_source,
            s.image_file, s.last_id, s.shoemodel, s.m_lsize, s.m_lwidth, s.m_rsize, s.m_rwidth, s.m_lgirth, s.m_linstep, s.m_rgirth, s.m_rinstep, s.m_lheel, s.m_rheel, s.m_lankle, s.m_rankle,
            design_id, item_id, item_name, item_amt, left_shoe, right_shoe, left_width, right_width, m_left_size, m_left_width, m_right_size,m_right_width,
            od.m_lgirth, od.m_rgirth, od.m_linstep, od.m_rinstep, od.reference_order_no, quantity
            from `order` o
            inner join `order_details` od on od.order_id = o.order_id
            left outer join shoe_design s on s.shoe_design_id = od.design_id
            where o.order_id = ' . $order_id);
        $orderarr = $query2->result_array();

        foreach ($orderarr as $orderarrVal) {
            $tempData = json_decode($orderarrVal['shoemodel'], true);
            if (isset($tempData['monogram'])) {
                $orderarrVal['monogramText'] = $tempData['monogram']['text'];
                $orderarrVal['monogramRightSide'] = $tempData['monogram']['rightSide'];
                $orderarrVal['monogramLeftSide'] = $tempData['monogram']['leftSide'];
                $orderarrVal['monogramRightShoe'] = $tempData['monogram']['rightShoe'];
                $orderarrVal['monogramLeftShoe'] = $tempData['monogram']['leftShoe'];
            }
            if (isset($tempData['sole'])) {
                $orderarrVal['soleId'] = $tempData['sole']['id'];
                $soleName = $this->db->get_where('style_sole', array('id' => $tempData['sole']['id']))->row('sole_name');
                if (!is_array($soleName) && $soleName != '' && $soleName != null) {
                    $orderarrVal['soleName'] = (!is_array($soleName) && $soleName != '' && $soleName != null) ? $soleName : '';
                }
                $orderarrVal['soleCode'] = $tempData['sole']['code'];
                $orderarrVal['soleColorId'] = $tempData['sole']['colorId'];
                $orderarrVal['soleColorCode'] = $tempData['sole']['color'];
                $soleColorName = $this->db->get_where('style_sole_color', array('id' => $tempData['sole']['colorId']))->row('color_name');
                if (!is_array($soleColorName) && $soleColorName != '' && $soleColorName != null) {
                    $orderarrVal['soleColorName'] = (!is_array($soleColorName) && $soleColorName != '' && $soleColorName != null) ? $soleColorName : '';
                }
                if (isset($tempData['sole']['stitchId'])) {
                    $stitchName = $this->db->get_where('style_sole_stitches', array('id' => $tempData['sole']['stitchId']))->row('stitch_name');
                    $orderarrVal['soleWelt'] = (!is_array($stitchName) && $stitchName != '' && $stitchName != null) ? $stitchName : '';
                }
            }

            $orderarrVal['shoetree'] = (isset($tempData['shoetree'])) ? $tempData['shoetree'] : '';
            $orderarrVal['hasPatina'] = (isset($tempData['patina'])) ? '1' : '0';
            $orderarrVal['patinaImg'] = (isset($tempData['patina'])) ? $orderarrVal['image_file'] . '_' . $tempData['patina']['color'] . '.jpg' : '';
            $orderarrVal['patina_material'] = (isset($tempData['patina'])) ? $this->db->get_where('material_def', array('material_def_id' => $tempData['patina']['matId']))->row('material_name') : '';
            $orderarrVal['patina_color'] = (isset($tempData['patina'])) ? $this->db->get_where('material_colors', array('material_color_id' => $tempData['patina']['clrId']))->row('color_name') : '';
            unset($orderarrVal['shoemodel']);
            $arr['order']['items'][] = $orderarrVal;
        }
        return $arr;
    }

    public function updateGiftCardStatus($giftcard_id)
    {
        $this->db->where('id', $giftcard_id)
            ->update('giftcard', array('is_active' => 1));
        return true;
    }

    public function get_remake_instruction($order_id, $shoe_design_id)
    {
        $query = $this->db->select('comments')
            ->get_where('order_rework', array('new_order_id' => $order_id, 'shoe_design_id' => $shoe_design_id));
        if ($query->num_rows > 0) {
            $result = $query->row_array();
            return $result['comments'];
        }
        return '';
    }

    public function is_remake_request_sent($order_id, $shoe_design_id)
    {
        $query = $this->db->select('id')->get_where('order_rework', array('order_id' => $order_id, 'shoe_design_id' => $shoe_design_id));
        if ($query->num_rows > 0) {
            return false;
        }
        return true;
    }

    public function getOrderDet($orderId)
    {
        $this->db->select('*');
        $this->db->from('order o');
        $this->db->join('store_credit_trans s', ' s.gift_card_code = o.order_id', 'left');
        $this->db->where('o.order_id', $orderId);
        $query = $this->db->get();
        $order = $query->result_array();
        return $order;
    }

    public function deleteOrderDetails($orderId)
    {
        $this->db->select('od.item_id');
        $this->db->from('order_details od');
        $this->db->join('giftcard g', 'g.id = od.item_id');
        $this->db->where('od.item_name LIKE ', 'Gift Card');
        $this->db->where('od.order_id', $orderId);
        $query1 = $this->db->get();
        if ($query1->num_rows > 0) {
            $this->deleteGiftCardOrderDetails($orderId);
        }

        $this->db->select('design_id');
        $this->db->from('order_details od');
        $this->db->join('shoe_design s', 'od.design_id = s.shoe_design_id');
        $this->db->where('od.item_name NOT LIKE ', 'Gift Card');
        $this->db->where('od.order_id', $orderId);
        $query2 = $this->db->get();
        if ($query2->num_rows > 0) {
            $this->deleteShoeOrderDetails($orderId);
        }
    }

    public function deleteShoeOrderDetails($orderId)
    {
        $this->db->where("EXISTS (SELECT design_id FROM order_details WHERE order_details.order_id=" . $orderId . " AND order_details.design_id=shoe_design.shoe_design_id)", null, false);
        if ($this->db->delete('shoe_design')) {
            $this->db->where('order_details.item_name NOT LIKE ', 'Gift Card');
            $this->db->where('order_details.order_id', $orderId);
            $this->db->delete('order_details');
            return true;
        }
    }

    public function deleteGiftCardOrderDetails($orderId)
    {
        $this->db->where("EXISTS (SELECT item_id FROM order_details WHERE order_details.order_id=" . $orderId . " AND giftcard.id=order_details.item_id)", null, false);
        if ($this->db->delete('giftcard')) {
            $this->db->where('order_details.item_name LIKE ', 'Gift Card');
            $this->db->where('order_details.order_id', $orderId);
            $this->db->delete('order_details');
            return true;
        }
    }

    public function get_shop_images($custom_collection_id)
    {
        $sql = "SELECT file_name " . " FROM `attachments` " . " WHERE FIND_IN_SET(id,(SELECT attachment_ids FROM custom_collection WHERE id = $custom_collection_id))";
        $query = $this->db->query($sql, false);

        if ($query->num_rows > 0) {
            $result = $query->result_array();
            return $result;
        }
        return false;
    }

    public function get_readytowear_images($readywear_id)
    {
        $sql = "SELECT file_name " . " FROM `attachments` " . " WHERE FIND_IN_SET(id,(SELECT attachment_ids FROM ready_wear WHERE id = $readywear_id))";
        $query = $this->db->query($sql, false);

        if ($query->num_rows > 0) {
            $result = $query->result_array();
            return $result;
        }
        return false;
    }

    public function get_readytowear_style($readywear_id)
    {
        $this->db->select('style_name');
        $this->db->from('ready_wear');
        $this->db->where('id', $readywear_id);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->row_array();
            return $result['style_name'];
        }
        return false;
    }

    public function get_latest_order_details($user_id)
    {
        $this->db->select('od.*');
        $this->db->from('order_details od');
        $this->db->join('order o', 'o.order_id = od.order_id');
        $this->db->where('o.user_id', $user_id);
        $this->db->order_by('design_id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->result_array();
            return $result;
        }
        return false;
    }

    public function can_show_details($custom_collection_id)
    {
        $this->db->select('is_editable');
        $this->db->from('custom_collection');
        $this->db->where('id', $custom_collection_id);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->row_array();
            return $result['is_editable'];
        }
        return false;
    }

    public function next_order($order_id)
    {
        $this->db->select('max(order_id) as order_id', false);
        $this->db->from('order');
        $this->db->where('order_id <', $order_id);
        $this->db->like('payment_status', 'Processed', 'none');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->row_array();
            return $result['order_id'];
        }
        return false;
    }

    public function prev_order($order_id)
    {
        $this->db->select('min(order_id) as order_id', false);
        $this->db->from('order');
        $this->db->where('order_id >', $order_id);
        $this->db->like('payment_status', 'Processed', 'none');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $result = $query->row_array();
            return $result['order_id'];
        }
        return false;
    }

    public function updatebilling($arr, $billing_id)
    {
        $this->db->where('billing_details_id', $billing_id);
        $this->db->update('billing_details', $arr);
        return true;
    }

    public function updateshipping($arr, $shipping_id)
    {
        $this->db->where('shipping_details_id', $shipping_id);
        $this->db->update('shipping_details', $arr);
        return true;
    }

    public function updateOrderComment($orderId, $comments, $taxAmt)
    {
        $this->db->set('comments', $comments);
        $this->db->set('user_agent', $this->input->user_agent());
        $this->db->set('tax_amount', $taxAmt);
        $this->db->where('order_id', $orderId);
        $this->db->update('order');
    }

    public function updateOrderStatus($orderId)
    {
        $this->db->trans_begin();
        $this->db->set('payment_status', 'Processed');
        $this->db->where('order_id', $orderId);
        $this->db->update('order');
        $this->insertorderStatus($orderId);
        $this->db->trans_commit();
        return true;
    }

    public function insertorderStatus($orderId)
    {
        $this->db->trans_begin();
        $this->db->select('*');
        $this->db->where('order_id', $orderId);
        $this->db->from('order');
        $query = $this->db->get();
        $res = $query->row_array();
        if ($query->num_rows > 0) {
            $this->db->select('*');
            $this->db->where('order_id', $orderId);
            $this->db->from('order_status');
            $query1 = $this->db->get();
            $res1 = $query1->row_array();
            $data = array(
                'order_id' => $orderId,
                'invoice_no' => $res['invoice_no'],
                'status' => 'Order acknowledged',
                'add_date' => date('Y-m-d H:i:s'),
                'user_id' => $res['user_id'],
                'status_desc' => 'Order Date :' . date('Y-m-d H:i:s'),
            );
            if ($query1->num_rows > 0) {
                if ($res1['status'] != 'Order acknowledged') {
                    $this->db->insert('order_status', $data);
                    if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            } else {
                $this->db->insert('order_status', $data);
                $this->db->trans_commit();
            }
        }
    }

    public function insertRemakeDetails($details)
    {
        $this->db->insert('order_rework', $details);
    }

    public function save_primary_billing_details($billing, $usernames)
    {
        $this->db->trans_begin();
        $this->db->where('user_id', $billing['user_id']);
        $this->db->update('users', $usernames);
        $this->db->select('*');
        $this->db->where('user_id', $billing['user_id']);
        $this->db->where('is_primary', 1);
        $this->db->from('billing_details');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $this->db->where('user_id', $billing['user_id']);
            $this->db->where('is_primary', 1);
            $this->db->update('billing_details', $billing);
            $this->db->trans_commit();
            $billing['first_name'] = $usernames['first_name'];
            $billing['last_name'] = $usernames['last_name'];
            return $billing;
        } else {
            $this->db->insert('billing_details', $billing);
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
            } else {
                $billing['billing_details_id'] = $this->db->insert_id();
                $this->db->trans_commit();
                return $billing;
            }
        }
        return null;
    }

    public function save_primary_shipping_details($shipping)
    {
        $this->db->trans_begin();
        $this->db->select('*');
        $this->db->where('user_id', $shipping['user_id']);
        $this->db->where('is_primary', 1);
        $this->db->from('shipping_details');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $this->db->where('user_id', $shipping['user_id']);
            $this->db->where('is_primary', 1);
            $this->db->update('shipping_details', $shipping);
            $this->db->trans_commit();
            return $shipping;
        } else {
            $this->db->insert('shipping_details', $shipping);
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
            } else {
                $shipping['shipping_details_id'] = $this->db->insert_id();
                $this->db->trans_commit();
                return $shipping;
            }
        }
        return null;
    }

    public function getPrimaryUserDetails($userId)
    {
        $this->db->select('*');
        $this->db->where('b.user_id', $userId);
        $this->db->where('is_primary', 1);
        $this->db->from('billing_details b');
        $this->db->join('users u', 'u.user_id = b.user_id', 'inner');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $billing = $query->row_array();
        } else {
            $billing = null;
        }
        $this->db->select('*');
        $this->db->where('user_id', $userId);
        $this->db->where('is_primary', 1);
        $this->db->from('shipping_details');
        $querys = $this->db->get();
        if ($querys->num_rows > 0) {
            $shipping = $querys->row_array();
        } else {
            $shipping = null;
        }
        $data['billing'] = $billing;
        $data['shipping'] = $shipping;
        return $data;
    }

    public function updateBillingShipping($type, $id, $data)
    {

    }

    public function insertBillingShipping($type, $id, $data)
    {

    }

    public function getOrderStatus($orderId, $userId)
    {
        $this->db->select('s.invoice_no,sd.public_name,l.last_name,ls.style_name,s.add_date,s.status_desc,s.status');
        $this->db->from('order_status s');
        $this->db->join('order o', 'o.order_id = s.order_id', 'inner');
        $this->db->join('order_details od', 'od.order_id = o.order_id', 'inner');
        $this->db->join('shoe_design sd', 'sd.shoe_design_id = od.design_id', 'inner');
        $this->db->join('last_item_def l', 'l.last_item_def_id = sd.last_id', 'inner');
        $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id', 'inner');

        $this->db->where('s.user_id', $userId);
        $this->db->where('s.invoice_no', $orderId);
        $this->db->where('s.status <> ', 'order shipped');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $ordered = $query->row_array();
        } else {
            $ordered = null;
        }
        return $ordered;
    }

    public function getPurchaseCount($userId)
    {
        $this->db->select('Count(*) As purchCount');
        $this->db->from('order_status');
        $this->db->where('user_id', $userId);
        $this->db->where('status', 'order shipped');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $purchCount = $query->row_array();
            $purchased['p_count'] = $purchCount['purchCount'];
            $purchased['orders'] = $this->getOrdersDet($userId, 'purchased');
        } else {
            $purchased['p_count'] = null;
        }
        $this->db->select('Count(*) As orderCount');
        $this->db->from('order_status');
        $this->db->where('user_id', $userId);
        $this->db->where('status <> ', 'order shipped');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $orderCount = $query->row_array();
            $ordered['o_count'] = $orderCount['orderCount'];
            $ordered['orders'] = $this->getOrdersDet($userId, 'ordered');
        } else {
            $ordered['o_count'] = null;
        }
        $result['ordered'] = $ordered;
        $result['purchased'] = $purchased;
        return $result;
    }

    private function getOrdersDet($userId, $mode)
    {
        if ($mode == 'purchased') {
            $this->db->select('s.invoice_no,sd.public_name,l.last_name,ls.style_name,s.add_date,s.status_desc,s.status');
            $this->db->from('order_status s');
            $this->db->join('order o', 'o.order_id = s.order_id', 'inner');
            $this->db->join('order_details od', 'od.order_id = o.order_id', 'inner');
            $this->db->join('shoe_design sd', 'sd.shoe_design_id = od.design_id', 'inner');
            $this->db->join('last_item_def l', 'l.last_item_def_id = sd.last_id', 'inner');
            $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id', 'inner');

            $this->db->where('s.user_id', $userId);
            $this->db->where('s.status', 'order shipped');
            $query = $this->db->get();
            if ($query->num_rows > 0) {
                $purchased = $query->result_array();
            } else {
                $purchased = null;
            }
            $result['purchased'] = $purchased;
            return $result;
        } else {
            $this->db->select('s.invoice_no,sd.public_name,l.last_name,ls.style_name,s.add_date,s.status_desc,s.status');
            $this->db->from('order_status s');
            $this->db->join('order o', 'o.order_id = s.order_id', 'inner');
            $this->db->join('order_details od', 'od.order_id = o.order_id', 'inner');
            $this->db->join('shoe_design sd', 'sd.shoe_design_id = od.design_id', 'inner');
            $this->db->join('last_item_def l', 'l.last_item_def_id = sd.last_id', 'inner');
            $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id', 'inner');

            $this->db->where('s.user_id', $userId);
            $this->db->where('s.status <> ', 'order shipped');
            $query = $this->db->get();
            if ($query->num_rows > 0) {
                $ordered = $query->result_array();
            } else {
                $ordered = null;
            }
            $result['ordered'] = $ordered;
            return $result;
        }
    }

    public function gift_card_order($order_id)
    {
        $this->db->select('g.*');
        $this->db->from('order_details od');
        $this->db->join('giftcard g', 'od.item_id = g.id', 'inner');

        $this->db->where('od.order_id', $order_id);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            return $query->row_array();
        }
        return false;
    }

    public function promo_card_order($order_id, $promocode_id)
    {
        $this->db->select('pu.email,p.promocode');
        $this->db->from('promocode p');
        $this->db->join('promo_assign pa', 'pa.promocode_id = p.promo_id', 'inner');
        $this->db->join('promo_users pu', 'pa.promouser_id = pu.id', 'inner');
        $this->db->where('pa.promocode_id', $promocode_id);
        $query = $this->db->get();

        if ($query->num_rows > 0) {
            return $query->row_array();
        }
        return false;
    }

    public function save_klarna_order_details($order_id, $order_details)
    {
        $this->db->trans_begin();
        $this->db->select('*');
        $this->db->where('order_id', $order_id);
        $this->db->from('order');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $prev_order = $query->row_array();
            $separator = "\n---------------------\n";
            $order_details['klarna_log'] = $prev_order['klarna_log'] . $separator . $order_details['klarna_log'];
            $this->db->where('order_id', $order_id);
            $this->db->update('order', $order_details);
            $this->db->trans_commit();
            return $order_details;
        } else {
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
            }
        }
        return null;
    }

    public function getOrderByklarna($klarna_order_id)
    {
        $this->db->select('*');
        $this->db->where('klarna_order_id', $klarna_order_id);
        $this->db->from('order');
        $query = $this->db->get();

        if ($query->num_rows > 0) {
            return $query->row_array();
        }
        return false;
    }

    public function getKlarnaAcknowledgedOrders()
    {
        $this->db->select('klarna_order_id');
        $this->db->where('klarna_status', 'Order Acknowledged');
        $this->db->where('order_id > ', 1950);
        $this->db->from('order');
        $query = $this->db->get();

        if ($query->num_rows > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_gift_card_order($order_id, $giftcard_id = null)
    {
        $this->db->select('g.*,od.quantity');
        $this->db->from('order_details od');
        $this->db->join('giftcard g', 'od.item_id = g.id', 'inner');

        $this->db->where('od.order_id', $order_id);
        if (isset($giftcard_id)) {
            $this->db->where('od.item_id', $giftcard_id);
        }
        $this->db->where('od.item_name', "Gift Card");
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            return $query->row_array();
        }
        return false;
    }

    public function updateShippingCost($order_id, $shipping_price = 0)
    {
        $this->db->trans_begin();
        $this->db->select('*');
        $this->db->from('order');
        $this->db->where('order.order_id', $order_id);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            $orderDet = $query->row_array();
            $this->db->where('order.order_id', $order_id);
            $this->db->update('order', array('shipping_price' => $shipping_price));
            $this->db->trans_commit();
        } else {
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
            }
        }
        return false;
    }

}
