<?php

class AppointmentModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('subquery');
    }

    public function getFutureAppointments()
    {
        $dateTimeNow = new DateTime('today');
        $futureDateTime = new DateTime('today');
        $futureDateTime->modify('+14 day');
        $futureDateTime->setTime(23, 59, 59);
        $nowStr = date_format($dateTimeNow, 'Y-m-d H:i:s');
        $futureStr = date_format($futureDateTime, 'Y-m-d H:i:s');

        $this->db->select('appointment_datetime');
        $this->db->from('appointments');
        $this->db->order_by("appointment_datetime", "asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function getFutureAppointmentObject()
    {
        $dateTimeNow = new DateTime('today');
        $futureDateTime = new DateTime('today');
        $futureDateTime->modify('+14 day');
        $futureDateTime->setTime(23, 59, 59);
        $nowStr = date_format($dateTimeNow, 'Y-m-d H:i:s');
        $futureStr = date_format($futureDateTime, 'Y-m-d H:i:s');

        $this->db->select('*');
        $this->db->from('appointments');
        $this->db->order_by("appointment_datetime", "asc");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function checkAvailability($dateTime)
    {
        $dbDateTime = DateTime::createFromFormat('d/m/Y H:i', $dateTime)->format('Y-m-d H:i:s');
        $this->db->select('appointment_datetime');
        $this->db->from('appointments');
        $this->db->where("appointment_datetime = '" . $dbDateTime . "'");
        $query = $this->db->get();
        if ($query->num_rows() >= 2) {
            return false;
        }
        return true;
    }

    public function saveAppointment($appointment)
    {
        $this->db->trans_begin();
        $this->db->insert('appointments', $appointment);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $appointment['status'] = 'failure';
        } else {
            $appointment['appointment_id'] = $this->db->insert_id();
            $this->db->trans_commit();
            $appointment['status'] = 'success';
        }
        return $appointment;
    }
}
