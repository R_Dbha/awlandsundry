<?php

class GenerateModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_last_style_details($lastStyleId, $is_processed = false)
    {
        $this->db->select('ls.last_style_id, ls.last_id, ls.def_code, ls.folder_name, ls.stitch_folder, ls.lace_folder, ls.def_toe, ls.def_vamp, ls.def_eyestay, ls.def_foxing, ls.def_material, ls.def_color, ls.def_angle, ls.is_vamp_enabled, l.def_code as last_code, l.folder_name as last_folder, m.material_folder');
        $this->db->from('last_styles ls');
        $this->db->join('last_item_def l', 'l.last_item_def_id = ls.last_id AND l.is_enabled = TRUE', 'inner');
        $this->db->join('material_def m', 'm.material_code = ls.def_material', 'inner');
        $this->db->where(array('ls.last_style_id' => $lastStyleId, 'ls.is_enabled' => true, 'is_processed' => $is_processed));
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return null;
    }

    public function get_materials()
    {
        $this->db->select('material_def_id, material_folder');
        $this->db->from("material_def");
        $this->db->where(array("is_enabled" => true));
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_stitches()
    {
        $this->db->select('stitch_type_id, stitch_code');
        $this->db->from("stitch_types");
        $this->db->where(array("is_enabled" => true));
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_laces()
    {
        $this->db->select('lace_type_id, lace_code');
        $this->db->from("lace_types");
        $this->db->where(array("is_enabled" => true));
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function insert_style_properties_old($styleId, $properties)
    {
        $this->db->select('property_type_id');
        $this->db->from("property_types");
        $this->db->where_in("property_code", $properties);
        $this->db->order_by("property_type_id", 'ASC');
        $result = $this->db->get()->result_array();
        $batch = array();
        foreach ($result as $property) {
            $batch[] = array('last_style_id' => $styleId, 'property_type_id' => $property['property_type_id']);
        }
        $this->db->insert_batch("style_properties", $batch);
    }

    public function insert_style_properties($styleId, $properties)
    {
        $batch = array();
        foreach ($properties as $property) {
            $detail = explode('_', $property['property']);
            $is_material = $detail[1] == "M" ? 1 : 0;
            $is_nothing = $detail[2] == "N" ? 1 : 0;
            $this->db->select('property_type_id');
            $this->db->from("property_types");
            $this->db->where("property_code", $detail[0]);
            $result = $this->db->get()->row_array();
            $batch[] = array('last_style_id' => $styleId, 'property_type_id' => $result['property_type_id'], 'thumb_file' => $property['img'], 'is_material' => $is_material, 'is_nothing' => $is_nothing);
        }
        $this->db->insert_batch("style_properties", $batch);
    }

    public function update_style_properties($styleId, $properties)
    {
        foreach ($properties as $property) {
            $detail = explode('_', $property['property']);
            $is_material = $detail[1] == "M" ? 1 : 0;
            $is_nothing = $detail[2] == "N" ? 1 : 0;
            $this->db->select('s.style_property_id');
            $this->db->from('style_properties s');
            $this->db->join('property_types p', "p.property_type_id = s.property_type_id and p.property_code = '" . $detail[0] . "'");
            $this->db->where('s.last_style_id', $styleId, false);
            $result = $this->db->get()->row_array();
            $update = array('thumb_file' => $property['img'], 'is_material' => $is_material, 'is_nothing' => $is_nothing);
            $this->db->where('style_property_id', $result['style_property_id']);
            $this->db->update('style_properties', $update);
        }
    }

    public function insert_style_materials($styleId, $materials)
    {
        $batch = array();
        foreach ($materials as $material) {
            $batch[] = array('last_style_id' => $styleId, 'material_id' => $material);
        }
        $this->db->insert_batch("style_materials", $batch);
    }

    public function insert_style_stitches($styleId, $stitches)
    {
        $batch = array();
        foreach ($stitches as $stitch) {
            $batch[] = array('style_id' => $styleId, 'stitch_id' => $stitch);
        }
        $this->db->insert_batch("style_stitches", $batch);
    }

    public function insert_style_laces($styleId, $laces)
    {
        $batch = array();
        foreach ($laces as $lace) {
            $batch[] = array('style_id' => $styleId, 'lace_id' => $lace);
        }
        $this->db->insert_batch("style_laces", $batch);
    }

    public function process_style($path, $filename, $styleid, $thwidth, $thheight, $width, $height, $angles, $ext, $dest)
    {
        $update = array('img_file' => $filename, 'is_processed' => true);
        $this->db->where('last_style_id', $styleid);
        $this->db->update("last_styles", $update);
        foreach ($angles as $angle) {
            $thumbbase = imagecreatetruecolor($thwidth, $thheight);
            $col1 = imagecolorallocatealpha($thumbbase, 255, 255, 255, 127);
            imagefill($thumbbase, 0, 0, $col1);
            imagesavealpha($thumbbase, true);
            $imgmain = imagecreatefrompng($path . $filename . $angle . $ext);
            imagecopyresized($thumbbase, $imgmain, 0, 0, 0, 0, $thwidth, $thheight, $width, $height);
            imagepng($thumbbase, $dest . $filename . $angle . $ext);
        }
    }

}
