<?php

class promocodemodel extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getPromoCodes($config, $search)
    {
        $this->db->_protect_identifiers = false;
        $this->db->select('promo_id,promocode, discount_per, discount_amount,expiry_date, COUNT( pa.assign_id ) assigned_users, '
            . ' COALESCE(sum(pa.is_used),0) as used_users, is_common', false);
        $this->db->from('promocode pc');
        $this->db->join('promo_assign pa', 'pc.promo_id = pa.promocode_id', 'left');
        $this->db->where('expiry_date >= ', date("Y-m-d", strtotime('-1 year')));
        if ($search !== '') {
            $this->db->where($search);
        }
        $this->db->group_by('pc.promo_id');
        $this->db->order_by($config['order_by'], $config['order']);

        $result['num_rows'] = $this->db->count_results('', false);
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $result['result'] = $query->result_array();
        return $result;
    }

    public function getPromoCodeDetails($promo_id)
    {
        $this->db->select('promo_id,promocode, discount_per, discount_amount, expiry_date,is_common,one_time,unlimited, COUNT( pa.assign_id ) assigned_users,  COALESCE(sum(pa.is_used),0) as  used_users, is_common', false);
        $this->db->from('promocode pc ');
        $this->db->join('promo_assign pa', 'pc.promo_id = pa.promocode_id', 'left');
        $this->db->group_by('pc.promo_id');
        $this->db->where('pc.promo_id', $promo_id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function savepromo($promodata, $promo_id)
    {
        if ($promo_id == '') {
            $this->db->insert('promocode', $promodata);
            return $this->db->insert_id();
        } else {
            $this->db->where('promo_id	', $promo_id);
            $this->db->update('promocode', $promodata);
            return $this->db->affected_rows();
        }
    }

    public function getPromoUsers($config, $promo_id, $search)
    {
        $this->db->select("pu.id, pu.email,pa.promocode_id, COALESCE(  pa.promocode_id , 0 ) as is_assigned, if( COALESCE(  `pa`.`is_used` , 0 ) =0 , 'No' ,  'Yes')  as is_used,1 as c  ", false);
        $this->db->from('promo_users pu');
        $this->db->join('promo_assign pa', "pu.id = pa.promouser_id and pa.promocode_id = $promo_id", 'left outer');
        if ($search !== '') {
            $this->db->where($search);
        }
        $result['num_rows'] = $this->db->count_all_results('', false);
        $this->db->order_by($config['order_by'], $config['order']);
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $result['result'] = $query->result_array();
        return $result;
    }

    public function assignPromo($promo_id, $userids)
    {
        if (sizeof($userids)) {
            $i = 0;
            foreach ($userids as $userid) {
                $this->db->select("*");
                $this->db->from('promo_assign');
                $this->db->where('promocode_id', $promo_id);
                $this->db->where('promouser_id', $userid);
                $query = $this->db->get();
                if ($query->num_rows() == 0) {
                    $i++;
                    $promo_data = array(
                        'promocode_id' => $promo_id,
                        'promouser_id' => $userid,
                        'is_used' => 0,
                    );
                    $this->db->insert('promo_assign', $promo_data);
                }
            }
            return $i;
        }
    }

    public function getPromoCodeUasge($promocode_id, $config, $search = '')
    {
        $this->db->select("o.order_id,o.invoice_no,o.gross_amt,o.discount_perc,o.discount,email", false);
        $this->db->from('promocode p');
        $this->db->join('`order` o', "p.promo_id = o.promocode_id");
        $this->db->join('users u', "o.user_id = u.user_id");
        $this->db->where('o.promocode_id', $promocode_id);
        if ($search !== '') {
            $this->db->where($search);
        }
        $result['num_rows'] = $this->db->count_all_results('', false);
        $this->db->order_by($config['order_by'], $config['order']);
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $result['result'] = $query->result_array();
        return $result;
    }

    public function insertPromoUser($userdata)
    {
        $this->db->select("id");
        $this->db->from('promo_users');
        $this->db->where('email', $userdata['email']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['id'];
        }
        $this->db->insert('promo_users', $userdata);
        return $this->db->insert_id();
    }

}
