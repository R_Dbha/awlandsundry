<?php

class GiftCardModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('subquery');
    }

    public function insert_gift_card_details($data)
    {
        $this->db->insert('giftcard', $data);
        return $this->db->insert_id();
    }

    public function save_gift_card_details($data)
    {
        $this->db->select('*');
        $this->db->from('giftcard');
        $this->db->where('id', $data['id']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $this->db->where('id', $data['id']);
            $this->db->update('giftcard', $data);
        } else {
            $this->db->insert('giftcard', $data);
            return $this->db->insert_id();
        }
    }

    public function getGiftCards($email)
    {
        $this->db->select(' card_number, g.amount, SUM( COALESCE(gt.amount,0 ) ) balance', false);
        $this->db->where('g.recipient_email', $email);
        $this->db->from('giftcard g');
        $this->db->join('giftcard_trans gt', 'g.id = gt.giftcard_id', 'LEFT');
        $this->db->group_by('gt.giftcard_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            return $result;
        }
        return false;
    }

    public function getGiftCard($email, $giftcard_number)
    {
        $this->db->select('*');
        $this->db->like('recipient_email', $email, 'none');
        $this->db->like('card_number', $giftcard_number, 'none');
        $this->db->where('is_active', 1);
        $this->db->from('giftcard');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function getBalance($giftcard_id)
    {
        $this->db->select(' COALESCE(g.amount - SUM( COALESCE(gt.amount,0 )),0) balance ', false);
        $this->db->where('g.id', $giftcard_id);
        $this->db->from('giftcard g');
        $this->db->join('giftcard_trans gt', 'g.id = gt.giftcard_id', 'LEFT');
        $this->db->group_by('gt.giftcard_id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['balance'];
        }
        return 0;
    }

    public function insertGiftCardUsage($giftcard_id, $discount, $order_id)
    {
        $data['giftcard_id'] = $giftcard_id;
        $data['amount'] = $discount;
        $data['order_id'] = $order_id;
        $this->db->insert('giftcard_trans', $data);
    }

    public function getBalanceFromEmail($email)
    {
        $query = $this->db->query('SELECT a.id, b.giftcard_id, (total - used) balance
                    FROM (SELECT id, SUM( amount ) total
                        FROM giftcard
                        WHERE recipient_email LIKE  "' . $email . '"
                        GROUP BY recipient_email ) a,
                        ( SELECT  `giftcard_id` , SUM( amount ) used
                        FROM  `giftcard_trans` GROUP BY giftcard_id ) b', true);

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['balance'];
        }
        return 0;
    }

    public function get_gift_cards_to_be_delivered_now()
    {
        $this->db->select('od.order_id, g.id', false);
        $this->db->from('giftcard g');
        $this->db->join('order_details od', 'od.item_id = g.id AND item_name like "Gift Card"');
        $this->db->where('TIMESTAMPDIFF( HOUR , CONVERT_TZ( NOW( ) ,  "SYSTEM",  "+00:00" ) , CONVERT_TZ(  `send_mail_on` ,  "SYSTEM", "+00:00" ) ) < 12', '', false);
        $this->db->where('g.is_active', 1);
        $this->db->where('g.is_sent', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function change_status($gift_card_id)
    {
        $this->db->where(array('id' => $gift_card_id));
        $this->db->update('giftcard', array('is_sent' => 1));
    }

    public function has_date($gift_card_id)
    {
        $this->db->select('id');
        $this->db->from('giftcard');
        $this->db->where('send_mail_on <> 0');
        $this->db->where('id', $gift_card_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function get_time_zone()
    {
        $this->db->select('offset as id, concat(offset," - ",canonocal_id) as name', false);
        $this->db->from('timezone');
        $this->db->order_by('name');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function getGiftCardOrders($order_id = 0, $giftcard_id = 0)
    {
        $this->db->select(' `o`.order_id, o.invoice_no, o.order_date, u`.user_id, '
            . 'u.first_name, u.last_name,u.email,sender_email,recipient_email, '
            . 'card_number, item_amt,g.id');
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id AND is_active = 1', 'inner');
        $this->db->join('order_details od', "od.order_id = o.order_id AND od.item_name LIKE  'gift card' ", 'LEFT');
        $this->db->join('giftcard g', "g.id = od.item_id", 'LEFT');
        $this->db->where('payment_status', 'Processed');
        $this->db->like('od.item_name', 'gift card', 'none');
        if ($order_id != 0) {
            $this->db->where('order_id', $order_id);
        }
        if ($giftcard_id != 0) {
            $this->db->where('g.id', $giftcard_id);
        }
        $this->db->order_by('order_date', 'DESC');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $order['rows'] = $query->result_array();
            $order['cnt'] = $query->num_rows();
        } else {
            $order['rows'] = false;
            $order['cnt'] = 0;
        }

        return $order;
    }

    public function get_giftcard_usage($giftcard_id)
    {
        $this->db->select('store_credit_id,store_credit_trans_id');
        $this->db->from('store_credit_trans sct');
        $this->db->join('giftcard g', 'g.card_number = sct.gift_card_code');
        $this->db->where('g.id', $giftcard_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $this->db->select('card_amount,gift_card_code as order_id,invoice_no,gross_amt,payment_status,o.discount');
            $this->db->from('store_credit_trans sct');
            $this->db->join('order o', 'o.order_id = sct.gift_card_code', 'left');
            $this->db->where('store_credit_id', $result['store_credit_id']);
            $this->db->where('is_issue', 1);
            $this->db->where('store_credit_trans_id > ', $result['store_credit_trans_id']);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return $result;
            }
        } else {
            return false;
        }
    }

}
