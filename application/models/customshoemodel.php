<?php

class CustomShoeModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_styles($last_id)
    {
        $this->db->select("l.last_style_id, l.is_vamp_enabled, l.is_lace_enabled, li.last_name, l.def_code, l.img_file, l.style_name, style_description, l.folder_name, m.material_folder, li.folder_name AS last_folder, concat(li.folder_name, '/', l.folder_name, '/', m.material_folder, '/', l.img_file) as location", false);
        $this->db->from('last_styles l');
        $this->db->join('material_def m', 'm.material_code = l.def_material', 'inner');
        $this->db->join('last_item_def li', 'li.last_item_def_id = l.last_id', 'inner');
        $this->db->where(array('last_id' => $last_id, 'l.is_enabled' => 1));
        $this->db->group_by('style_name');
        $this->db->order_by('l.sort_order', 'ASC');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_sole()
    {
        $return = array();
        $return['sole'] = $this->db->get('style_sole')->result_array();
        $return['soleColor'] = $this->db->get('style_sole_color')->result_array();
        $return['soleStitch'] = $this->db->order_by('id')->get('style_sole_stitches')->result_array();

        return $return;
    }

    public function get_shoe_size()
    {
        $user = $this->session->userdata('user_details');
        $this->db->select('*');
        $this->db->from('user_size');
        $this->db->where(array('userId' => $user['user_id']));
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function save_sizing($sizing)
    {
        $this->db->select('*');
        $this->db->from('user_size');
        $this->db->where(array('userId' => $sizing['userId']));
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            $this->db->where('userId', $sizing['userId']);
            $this->db->update('user_size', $sizing);
        } else {
            $this->db->insert('user_size', $sizing);
        }
    }

    public function get_last_styles($style_name)
    {
        $this->db->select("l.last_style_id,manufacturer_code, l.def_code, l.img_file, l.style_name, style_description, l.folder_name, m.material_folder,last_item_def_id, last_name, description, concat(ld.folder_name, '/', l.folder_name, '/', m.material_folder, '/', l.img_file) as location", false);
        $this->db->from('last_styles l');
        $this->db->join('material_def m', 'm.material_code = l.def_material', 'inner');
        $this->db->join('last_item_def ld', 'last_item_def_id = l.last_id', 'inner');
        $this->db->where(array('style_name' => $style_name, 'l.is_enabled' => 1));
        $this->db->order_by('ld.sort_order', 'ASC');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return null;
    }

    public function get_style_defaults($styleId)
    {
        $fields = <<<EOD
                ls.def_angle,
                l.last_item_def_id, l.def_code AS last_code, l.folder_name as last_folder, l.last_name,ls.style_name,
                ls.last_style_id, ls.def_code AS style_code, ls.folder_name AS style_folder, ls.is_vamp_enabled, ls.is_lace_enabled, ls.svg_file,
                ls.img_file, ls.def_material, m.material_folder, ls.def_color, mc.material_id, mc.material_color_id,
                ls.def_toe, t.property_type_id as toe_id, t.property_cat as toe_cat,
                tp.is_material as t_is_material, tp.is_nothing as t_is_nothing,
                ls.def_vamp, v.property_type_id as vamp_id, v.property_cat as vamp_cat,
                vp.is_material as v_is_material, vp.is_nothing as v_is_nothing,
                ls.def_eyestay, e.property_type_id as eyestay_id, e.property_cat as eyestay_cat,
                ep.is_material as e_is_material, ep.is_nothing as e_is_nothing,
                ls.def_foxing, f.property_type_id as foxing_id, f.property_cat as foxing_cat,
                fp.is_material as f_is_material, fp.is_nothing as f_is_nothing,
                ls.def_lace, lt.lace_type_id, lt.lace_name, ls.lace_folder,
                ls.def_stitch, st.stitch_type_id, st.stitch_name, ls.stitch_folder
EOD;
        $this->db->select($fields);
        $this->db->from('last_styles ls');
        $this->db->join('last_item_def l', 'l.last_item_def_id = ls.last_id', 'INNER');
        $this->db->join('material_def m', 'm.material_code = ls.def_material', 'INNER');
        $this->db->join('material_colors mc', 'mc.material_id = m.material_def_id AND mc.color_code = ls.def_color', 'INNER');
        $this->db->join('property_types t', "t.property_code = ls.def_toe and t.property_cat = 'Toe'", 'INNER');
        $this->db->join('style_properties tp', "tp.property_type_id = t.property_type_id and tp.last_style_id = ls.last_style_id", 'LEFT OUTER');
        $this->db->join('property_types v', "v.property_code = ls.def_vamp and v.property_cat = 'Vamp'", 'INNER');
        $this->db->join('style_properties vp', "vp.property_type_id = v.property_type_id and vp.last_style_id = ls.last_style_id", 'LEFT OUTER');
        $this->db->join('property_types e', "e.property_code = ls.def_eyestay and e.property_cat = 'Eyestay'", 'INNER');
        $this->db->join('style_properties ep', "ep.property_type_id = e.property_type_id and ep.last_style_id = ls.last_style_id", 'LEFT OUTER');
        $this->db->join('property_types f', "f.property_code = ls.def_foxing and f.property_cat = 'Foxing'", 'INNER');
        $this->db->join('style_properties fp', "fp.property_type_id = f.property_type_id and fp.last_style_id = ls.last_style_id", 'LEFT OUTER');
        $this->db->join('stitch_types st', "st.stitch_code = ls.def_stitch", 'INNER');
        $this->db->join('lace_types lt', "lt.lace_code = ls.def_lace", 'INNER');
        $this->db->where(array('ls.last_style_id' => $styleId));
        $result = $this->db->get()->row_array();
        $last = array("id" => $result['last_item_def_id'], "code" => $result['last_code'], "folder" => $result['last_folder'], "last_name" => $result['last_name']);
        $style = array("id" => $result['last_style_id'], "code" => $result['style_code'], "folder" => $result['style_folder'], "styleName" => $result['style_name'], "svg" => $result['svg_file'], "isVampEnabled" => $result['is_vamp_enabled'], "color" => $result['def_color'], "isLaceEnabled" => $result['is_lace_enabled']);
        $quarter = array("base" => $result['img_file'], "material" => $result['def_material'], "matfolder" => $result['material_folder'], "matId" => $result['material_id'], "clrId" => $result['material_color_id'], "color" => $result['def_color']);
        $toe = array("type" => $result['def_toe'], "toeId" => $result['toe_id'], "part" => CustomShoeConfig::TOE_PART, "isMaterial" => $result['t_is_material'], "isNothing" => $result['t_is_nothing'], "material" => $result['def_material'], "matfolder" => $result['material_folder'], "matId" => $result['material_id'], "clrId" => $result['material_color_id'], "color" => $result['def_color']);
        $vamp = array("type" => $result['def_vamp'], "vampId" => $result['vamp_id'], "part" => CustomShoeConfig::VAMP_PART, "isMaterial" => $result['v_is_material'], "isNothing" => $result['v_is_nothing'], "material" => $result['def_material'], "matfolder" => $result['material_folder'], "matId" => $result['material_id'], "clrId" => $result['material_color_id'], "color" => $result['def_color']);
        $eyestay = array("type" => $result['def_eyestay'], "eyestayId" => $result['eyestay_id'], "part" => CustomShoeConfig::EYESTAY_PART, "isMaterial" => $result['e_is_material'], "isNothing" => $result['e_is_nothing'], "material" => $result['def_material'], "matfolder" => $result['material_folder'], "matId" => $result['material_id'], "clrId" => $result['material_color_id'], "color" => $result['def_color']);
        $foxing = array("type" => $result['def_foxing'], "foxingId" => $result['foxing_id'], "part" => CustomShoeConfig::FOXING_PART, "isMaterial" => $result['f_is_material'], "isNothing" => $result['f_is_nothing'], "material" => $result['def_material'], "matfolder" => $result['material_folder'], "matId" => $result['material_id'], "clrId" => $result['material_color_id'], "color" => $result['def_color']);
        $stitch = array("code" => $result['def_stitch'], "id" => $result['stitch_type_id'], "name" => $result['stitch_name'], "folder" => $result['stitch_folder']);
        $lace = array("code" => $result['def_lace'], "id" => $result['lace_type_id'], "name" => $result['lace_name'], "folder" => $result['lace_folder']);
        $model = array("angle" => $result['def_angle'], "ext" => CustomShoeConfig::EXTENTION, "last" => $last, "style" => $style, "quarter" => $quarter, "toe" => $toe, "vamp" => $vamp, "eyestay" => $eyestay, "foxing" => $foxing, "stitch" => $stitch, "lace" => $lace);
        return $model;
    }

    public function get_style_details($last_style_id)
    {
        $sql = <<<EOD
                l.last_item_def_id, l.def_code AS last_code, l.last_name, ls.style_name, ls.folder_name AS style_folder, l.folder_name AS last_folder,
                ls.last_style_id, ls.def_code AS style_code, ls.is_vamp_enabled, ls.is_lace_enabled
EOD;
        $this->db->select($sql);
        $this->db->from('last_styles ls');
        $this->db->join('last_item_def l', 'l.last_item_def_id = ls.last_id', 'INNER');
        $this->db->where(array('ls.last_style_id' => $last_style_id));
        $result = $this->db->get()->row_array();
        return $result;
    }

    public function style_properties($styleId, $property_enabled)
    {
        $this->db->select('s.property_type_id, p.property_cat, p.property_code, p.property_name, s.is_material, s.is_nothing, s.thumb_file, p.property_image');
        $this->db->from("style_properties s");
        $this->db->join('property_types p', 'p.property_type_id = s.property_type_id AND p.is_enabled = ' . $property_enabled, 'inner');
        $this->db->where(array('s.last_style_id' => $styleId));
        $result = $this->db->get()->result_array();
        $properties = array();
        foreach ($result as $property) {
            $properties[$property['property_cat']][] = array(
                $property['property_cat'] . "Id" => $property['property_type_id'],
                "code" => $property['property_code'],
                "name" => $property['property_name'],
                "image" => $property['thumb_file'],
                "isMaterial" => $property['is_material'],
                "isNothing" => $property['is_nothing'],
            );
        }
        return $properties;
    }

    public function combinations($arrays, $i = 0)
    {
        if (!isset($arrays[$i])) {
            return array();
        }
        if ($i == count($arrays) - 1) {
            return $arrays[$i];
        }

        $tmp = $this->combinations($arrays, $i + 1);
        $result = array();

        foreach ($arrays[$i] as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                array_merge(array($v), $t) :
                array($v, $t);
            }
        }

        return $result;
    }

    public function style_properties_comb($styleId)
    {
        $lastStyles = $this->db->select('l.last_style_id as styleId')
            ->join('last_styles as l', 'l.folder_name = last_styles.folder_name AND l.last_id = "4"')
            ->get_where('last_styles', array('last_styles.last_style_id' => $styleId))->row_array();

        $styleId = $lastStyles['styleId'];

        $result = $this->db->get_where('style_combination', array('style_combination.last_style_id' => $styleId))
            ->result_array();

        $return = [];
        foreach ($result as $value) {
            $return[] = json_decode($value['comb_data'], true);
        }

        return array('Toe' => $return);
    }

    public function get_invalid_properties($data)
    {
        $retproperties = array();
        $this->db->select('l.folder_name, ls.def_code, ls.folder_name as style_folder, m.material_folder, ls.def_material, ls.def_color');
        $this->db->from('last_styles ls');
        $this->db->join('last_item_def l', 'l.last_item_def_id = ls.last_id', 'inner');
        $this->db->join('material_def m', 'm.material_code = ls.def_material', 'inner');
        $this->db->where('last_style_id', $data['styleId']);
        $styledet = $this->db->get()->row_array();
        $this->db->select('p.property_type_id, pt.property_code');
        $this->db->from('style_properties p');
        $this->db->join('property_types pt', "pt.property_type_id = p.property_type_id and pt.property_cat = '" . $data['required'] . "'", 'inner');
        $this->db->where('p.last_style_id', $data['styleId']);
        $query = $this->db->get();
        $filebase = $_SERVER['DOCUMENT_ROOT'] . "/files/images/" . $styledet['folder_name'] . "/" . $styledet['style_folder'] . "/" . $styledet['material_folder'] . "/" . $styledet['def_code'] . "_";
        $suff = $styledet['def_material'] . $styledet['def_color'] . "_A0.png";
        if ($query->num_rows() > 0) {
            $properties = $query->result_array();
            foreach ($properties as $property) {
                $data[$data['required']] = $property['property_code'];
                if (file_exists($filebase . $data['Toe'] . $data['Vamp'] . $data['Eyestay'] . $data['Foxing'] . $suff)) {
                    $retproperties['available'][] = $property;
                } else {
                    $retproperties['notavailable'][] = $property;
                }
            }
        }
        return $retproperties;
    }

    public function style_material_colors()
    {
        $model = $this->session->userdata("ShoeModel");
        $this->db->select('sm.material_id, m.material_code, m.material_folder, m.material_name');
        $this->db->from('style_materials sm');
        $this->db->join('material_def m', 'm.material_def_id = sm.material_id', 'inner');
        $this->db->where(array('sm.last_style_id' => $model['style']['id']));
        $this->db->where(array('m.is_enabled' => '1'));
        $materials = $this->db->get()->result_array();
        $imageBase = $_SERVER['DOCUMENT_ROOT'] . "/files/images/" . $model['last']['folder'] . "/" . $model['style']['folder'] . "/";
        $img = $model['style']['code'] . "_" . $model['toe']['type'] . $model['vamp']['type'] . $model['eyestay']['type'] . $model['foxing']['type'];
        $qimgs = array();
        $timgs = array();
        $vimgs = array();
        $eimgs = array();
        $fimgs = array();
        $i = 0;
        foreach ($materials as $material) {
            $qclrs = array();
            $tclrs = array();
            $vclrs = array();
            $eclrs = array();
            $fclrs = array();
            $colors = $this->get_material_colors($material['material_id']);
            foreach ($colors as $color) {
                if ($material['material_name'] != 'Patina') {
                    // if (file_exists($imageBase . $material['material_folder'] . "/" . $img . $material['material_code'] . $color['color_code'] . "_A0.png")) {
                    $qclrs[] = array("type" => "quarter", "matId" => $material['material_id'], "clrId" => $color['material_color_id'], 'clrCode' => $color['color_code'], 'clrName' => $color['color_name'], "url" => CustomShoeConfig::IMG_BASE . "materials/" . $material['material_folder'] . "/" . $color['color_image']);
                    // }
                    // if (file_exists($imageBase . $material['material_folder'] . "/" . $img . $material['material_code'] . $color['color_code'] . CustomShoeConfig::TOE_PART . "_A0.png")) {
                    $tclrs[] = array("type" => "toe", "matId" => $material['material_id'], "clrId" => $color['material_color_id'], 'clrCode' => $color['color_code'], 'clrName' => $color['color_name'], "url" => CustomShoeConfig::IMG_BASE . "materials/" . $material['material_folder'] . "/" . $color['color_image']);
                    // }
                    // if ($model['style']['isVampEnabled']) {
                    //     if (file_exists($imageBase . $material['material_folder'] . "/" . $img . $material['material_code'] . $color['color_code'] . CustomShoeConfig::VAMP_PART . "_A0.png")) {
                    $vclrs[] = array("type" => "vamp", "matId" => $material['material_id'], "clrId" => $color['material_color_id'], 'clrCode' => $color['color_code'], 'clrName' => $color['color_name'], "url" => CustomShoeConfig::IMG_BASE . "materials/" . $material['material_folder'] . "/" . $color['color_image']);
                    //     }
                    // }
                    // if (file_exists($imageBase . $material['material_folder'] . "/" . $img . $material['material_code'] . $color['color_code'] . CustomShoeConfig::EYESTAY_PART . "_A0.png")) {
                    $eclrs[] = array("type" => "eyestay", "matId" => $material['material_id'], "clrId" => $color['material_color_id'], 'clrCode' => $color['color_code'], 'clrName' => $color['color_name'], "url" => CustomShoeConfig::IMG_BASE . "materials/" . $material['material_folder'] . "/" . $color['color_image']);
                    // }
                    // if (file_exists($imageBase . $material['material_folder'] . "/" . $img . $material['material_code'] . $color['color_code'] . CustomShoeConfig::FOXING_PART . "_A0.png")) {
                    $fclrs[] = array("type" => "foxing", "matId" => $material['material_id'], "clrId" => $color['material_color_id'], 'clrCode' => $color['color_code'], 'clrName' => $color['color_name'], "url" => CustomShoeConfig::IMG_BASE . "materials/" . $material['material_folder'] . "/" . $color['color_image']);
                    // }
                } else {
                    $material['isNew'] = '1';
                    $imgs = json_decode($color['color_description'], true);

                    $arr = array(
                        "type" => "quarter",
                        "matId" => $material['material_id'],
                        "clrId" => $color['material_color_id'],
                        'clrCode' => $color['color_code'],
                        'clrName' => $color['color_name'],
                        "url" => base_url() . $color['color_image'],
                        'images' => $imgs['img'],
                    );
                    $qclrs[] = $arr;
                    $arr['type'] = 'toe';
                    $tclrs[] = $arr;
                    $arr['type'] = 'vamp';
                    $vclrs[] = $arr;
                    $arr['type'] = 'eyestay';
                    $eclrs[] = $arr;
                    $arr['type'] = 'foxing';
                    $fclrs[] = $arr;
                }
            }
            if (sizeof($qclrs) > 0) {
                $qimgs[$i] = $material;
                $qimgs[$i]['colors'] = $qclrs;
            }
            if (sizeof($tclrs) > 0) {
                $timgs[$i] = $material;
                $timgs[$i]['colors'] = $tclrs;
            }
            if (sizeof($vclrs) > 0) {
                $vimgs[$i] = $material;
                $vimgs[$i]['colors'] = $vclrs;
            }
            if (sizeof($eclrs) > 0) {
                $eimgs[$i] = $material;
                $eimgs[$i]['colors'] = $eclrs;
            }
            if (sizeof($fclrs) > 0) {
                $fimgs[$i] = $material;
                $fimgs[$i]['colors'] = $fclrs;
            }
            $i++;
        }
        $availmatcol = array();
        if (sizeof($qimgs) > 0) {
            $availmatcol['quarter'] = $qimgs;
        }
        if (sizeof($timgs) > 0) {
            $availmatcol['toe'] = $timgs;
        }
        if ($model['style']['isVampEnabled']) {
            if (sizeof($vimgs) > 0) {
                $availmatcol['vamp'] = $vimgs;
            }
        }
        if (sizeof($eimgs) > 0) {
            $availmatcol['eyestay'] = $eimgs;
        }
        if (sizeof($fimgs) > 0) {
            $availmatcol['foxing'] = $fimgs;
        }
        return $availmatcol;
    }

    public function get_material_colors($materialId)
    {
        $this->db->select('material_color_id, color_code, color_name, color_image,color_description');
        $this->db->from("material_colors");
        $this->db->where('material_id', $materialId);
        return $this->db->get()->result_array();
    }

    public function get_style_stitches($styleId)
    {
        $this->db->select('s.stitch_id, st.stitch_code, st.stitch_name, st.stitch_color');
        $this->db->from('style_stitches s');
        $this->db->join('stitch_types st', 'st.stitch_type_id = s.stitch_id', 'inner');
        $this->db->where('style_id', $styleId);
        return $this->db->get()->result_array();
    }

    public function get_style_laces($styleId)
    {
        $this->db->select('s.lace_id, l.lace_code, l.color_code, l.lace_name');
        $this->db->from('style_laces s');
        $this->db->join('lace_types l', 'l.lace_type_id = s.lace_id', 'inner');
        $this->db->where('style_id', $styleId);
        $this->db->where('is_enabled', 1);
        return $this->db->get()->result_array();
    }

    public function get_last_style_details($styleId)
    {
        $this->db->select("ls.last_id, ls.last_style_id, ls.style_name, l.last_name, l.description AS style_description, ls.price, ls.folder_name, l.folder_name as last_folder, m.material_folder, ls.def_angle, ls.img_file");
        $this->db->from('last_styles ls');
        $this->db->join('last_item_def l', 'l.last_item_def_id = ls.last_id', 'inner');
        $this->db->join('material_def m', 'm.material_code = ls.def_material');
        $this->db->where(array('ls.last_style_id' => $styleId));
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_last_style_name($design_id)
    {
        $this->db->select('last_name,style_name');
        $this->db->from('shoe_design s');
        $this->db->join('last_item_def l', 's.last_id =  l.last_item_def_id', 'inner');
        $this->db->join('last_styles ls', 's.style_id =  ls.last_style_id', 'inner');
        $this->db->where(array('s.shoe_design_id' => $design_id));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return '';
    }

}
