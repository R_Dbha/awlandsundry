<?php

class AdminViewModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function designs_vs_orders($config, $search = array())
    {
        $this->db->select('GROUP_CONCAT( s.shoe_design_id ) designs,
            ( SELECT COUNT( o1.order_id ) FROM  `order` o1
              LEFT JOIN order_details od1 ON o1.order_id = od1.order_id
              WHERE u.user_id = o1.user_id and payment_status like "Processed") ordered,
            COUNT( s.shoe_design_id ) count_designs, u.first_name, u.last_name, u.email');
        $this->db->from('shoe_design s');

        $this->db->join('(SELECT o.order_id, od.design_id, o.user_id FROM  `order` o
                          LEFT JOIN order_details od ON o.order_id = od.order_id
                          GROUP BY o.order_id) j ', 's.shoe_design_id = j.design_id AND j.order_id = NULL', 'LEFT OUTER');

        $this->db->join('users u', 'u.user_id = s.user_id', 'inner');

        $this->db->group_by('s.user_id');
        $result['cnt'] = $this->db->count_results('', false);
        $this->db->order_by('count_designs', 'ASC');
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $result['rows'] = $query->result_array();

        return $result;
    }

    public function getCustomers($config, $search = array())
    {
        $this->db->select('group_concat(o.order_id) og , IF( COALESCE( o.order_id, 0 ) =0, 0, COUNT( u.user_id ) )  number_of_orders , u.user_id,first_name,last_name,email,customer_category', false);
        $this->db->from('users u');
        $this->db->join('order o', 'u.user_id = o.user_id AND o.payment_status = "Processed"', 'left outer');
        if (sizeof($search)) {
            $this->db->like($search);
        }
        $this->db->group_by('u.user_id');
        $customers['cnt'] = $this->db->count_results('', false);
        $this->db->order_by('number_of_orders', 'DESC');
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $customers['rows'] = $query->result_array();
        return $customers;
    }

    public function change_customer_category($id, $category)
    {
        $this->db->where('user_id', $id)->update('users', array('customer_category' => $category));
    }

    public function get_order_summary($month, $year)
    {
        $first_day_this_month = date("Y-m-d H:i:s", mktime(0, 0, 0, $month, 1, $year));
        $first_day_next_month = date("Y-m-d H:i:s", mktime(0, 0, 0, $month + 1, 1, $year));

        $this->db->select('group_concat(o.order_id) og ,count(o.order_id) Total_customer_orders,
            (Select  ROUND(COALESCE(SUM( `gross_amt` - (`gross_amt` * `discount_perc` /100) + `tax_amount` ), 0), 2) from  `order` od inner join `users` us on `us`.`user_id` = `od`.`user_id`  where od.`payment_status` = "Processed" AND od.`order_date` >= "' . $first_day_this_month . '" AND od.`order_date` < "' . $first_day_next_month . '") AS Revenue,
            ROUND(COALESCE(SUM(COALESCE(pd.manufacturing_cost,0)+COALESCE(pd.shipping_cost,0)),0),2) COGS
            ', false);
        $this->db->from('order o');
        $this->db->join('order_status os', 'os.order_id = o.order_id AND os.is_active = 1', 'inner');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->where('o.payment_status', 'Processed');
        $this->db->where('order_date >=', $first_day_this_month);
        $this->db->where('order_date <', $first_day_next_month);
        $this->db->like('u.customer_category', 'customer', 'none');
        $this->db->join('payment_details pd', 'pd.order_id = o.order_id', 'left');
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getUserOrders($config, $orderId = array())
    {
        $user = $this->session->userdata('user_details');
        $this->db->select('o.order_id, o.invoice_no, o.order_date,
            u.first_name,u.last_name, if(os.status = "order shipped", os.add_date,"") AS shipped_date,
            status_desc, sd.public_name, os.status, ls.style_name, st.last_name as last ', false);
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id AND is_active = 1', 'inner');
        $this->db->join('order_details od', "od.order_id = o.order_id AND od.item_name NOT LIKE  'gift card' ", 'INNER');
        $this->db->join('shoe_design sd', 'od.design_id = sd.shoe_design_id');
        $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id');
        $this->db->join('last_item_def st', 'st.last_item_def_id = sd.last_id');
        $this->db->where('o.payment_status', 'Processed');
        $this->db->where('u.user_id ', $user['user_id']);
        if (sizeof($orderId)) {
            $this->db->where_in('o.order_id', $orderId);
        }
        $this->db->order_by('order_date', 'DESC');
        $order['cnt'] = $this->db->count_results('', false);
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $order['rows'] = $query->result_array();
        return $order;
    }

    public function getProcessedOrders($config, $orderId = array(), $not_order_id = array())
    {
        $this->db->select('o.order_id,o.invoice_no, o.gross_amt, o.discount_perc, o.discount, o.tax_amount, o.order_date, order_modified,
            u.first_name,u.last_name, pd.design_id, o.belt_needed, o.shoe_horn,o.shoe_trees,kickstarter_no, p.company_name,
            if(os.status = "order shipped", os.add_date,"") AS shipped_date,
            if(os.status = "order shipped", os.status_desc,"") AS status_desc,
            if(os.status = "order shipped", DATEDIFF(os.add_date,o.order_date) ,"") AS day_diff,
            os.status,
            (SELECT CONCAT(city,", ", state,", ", country) FROM shipping_details WHERE o.order_id = order_id limit 1) AS location,
             GROUP_CONCAT( " ",ls.style_name) style_name,
             GROUP_CONCAT( " ",concat(lid.last_name,"(",lid.manufacturer_code,") ")) last,
             sum(manufacturing_cost) as manufacturing_cost,sum(shipping_cost) as shipping_cost,
            sum(item_amt*quantity) total_amt,  SUM( quantity) item_count, IF(COALESCE(re.new_order_id,"N") ="N" ,"No", CONCAT("Order id - ",re.order_id)) is_rework , o.promocode_id,pc.promocode, pc.discount_per, pc.discount_amount,
            sc.store_credit_trans_id,sc.card_amount, sum(1) as number_of_items  ', false);
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id AND is_active = 1', 'inner');
        $this->db->join('order_details od', "od.order_id = o.order_id AND od.item_name NOT LIKE  'gift card' ", 'INNER');
        $this->db->join('shoe_design sd', 'od.design_id = sd.shoe_design_id');
        $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id');
        $this->db->join('last_item_def lid', 'lid.last_item_def_id = sd.last_id');
        $this->db->join('payment_details pd', 'pd.design_id = od.design_id', 'left');
        $this->db->join('order_rework re', 're.new_order_id = o.order_id', 'left');
        $this->db->join('promocode pc', 'pc.promo_id = o.promocode_id', 'left');
        $this->db->join('store_credit_trans sc', 'sc.gift_card_code  = o.order_id', 'left');
        $this->db->join('partner_orders po', 'po.order_id = o.order_id', 'left');
        $this->db->join('partners p', 'p.partner_id= po.partner_id', 'left');
        $this->db->where('o.payment_status', 'Processed');
        if (sizeof($orderId)) {
            $this->db->where_in('o.order_id', $orderId);
        }
        if (sizeof($not_order_id)) {
            $this->db->where_not_in('o.order_id', $not_order_id);
        }

        $this->db->group_by('o.order_id');
        $this->db->order_by('o.order_id', 'DESC');
        $order['cnt'] = $this->db->count_results('', false);
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $order['rows'] = $query->result_array();
        return $order;
    }

    public function getPromoCodeDetails($promo_id)
    {
        $this->db->select('promo_id,promocode, discount_per, discount_amount, expiry_date, COUNT( pa.assign_id ) assigned_users,  COALESCE(sum(pa.is_used),0) as  used_users, is_common', false);
        $this->db->from('promocode pc ');
        $this->db->join('promo_assign pa', 'pc.promo_id = pa.promocode_id', 'left');
        $this->db->group_by('pc.promo_id');
        $this->db->where('pc.promo_id', $promo_id);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getPromoCodes($config, $promocode = '')
    {
        $this->db->select('promo_id,promocode, discount_per, discount_amount,expiry_date, COUNT( pa.assign_id ) assigned_users,  COALESCE(sum(pa.is_used),0) as used_users, is_common', false);
        $this->db->from('promocode pc');
        $this->db->join('promo_assign pa', 'pc.promo_id = pa.promocode_id', 'left');
        $this->db->where('expiry_date >= ', date("Y-m-d"));
        if (isset($promocode) && $promocode != '') {
            $this->db->like('promocode', $promocode, 'both');
        }
        $this->db->group_by('pc.promo_id');
        $this->db->order_by('promo_id', 'desc');
        $result['cnt'] = $this->db->count_results('', false);
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $result[] = $query->result_array();
        return $result;
    }

    public function savepromo($promodata, $promo_id)
    {
        if ($promo_id == 0) {
            $this->db->insert('promocode', $promodata);
        } else {
            $this->db->where('promo_id	', $promo_id)
                ->update('promocode', $promodata);
        }
    }

    public function isExistingPromoUser($userdata)
    {
        $this->db->select("id");
        $this->db->from('promo_users');
        $this->db->where('email', $userdata['email']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['id'];
        }
        return 0;
    }

    public function insertPromoUser($userdata)
    {
        $this->db->select("id");
        $this->db->from('promo_users');
        $this->db->where('email', $userdata['email']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['id'];
        }
        $this->db->insert('promo_users', $userdata);
        return $this->db->insert_id();
    }

    public function assignPromo($promo_id, $userids)
    {
        if (sizeof($userids)) {
            $i = 0;
            foreach ($userids as $userid) {
                $query = $this->db->get('promo_assign', ['promouser_id' => $userid, 'promocode_id' => $promo_id]);
                if ($query->num_rows() == 0) {
                    $i++;
                    $this->db->insert('promo_assign', array('promocode_id' => $promo_id, 'promouser_id' => $userid, 'is_used' => 0));
                }
            }
            return $i;
        }
    }

    public function getPromoUsers($promo_id)
    {
        $this->db->select("pu.id, pu.email,pa.promocode_id, COALESCE(  pa.promocode_id , 0 ) as is_assigned, if( COALESCE(  `pa`.`is_used` , 0 ) =0 , 'No' ,  'Yes')  as is_used  ", false);
        $this->db->from('promo_users pu');
        $this->db->join('promo_assign pa', "pu.id = pa.promouser_id and pa.promocode_id = $promo_id", 'left outer');
        $this->db->order_by('is_assigned', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getOrderByStatus($mode)
    {
        $qstring = '';
        $havStatus = $mode == '' ? false : true;
        if ($havStatus) {
            $qstring = "AND status = '" . $mode . "'";
            $havStatus = false;
        }
        $this->db->select('o.*,u.*,os.*, sum(item_amt) total_amt, COUNT( item_amt ) item_count');
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id ' . $qstring . ' AND is_active = 1', 'inner');
        $this->db->join('order_details od', "od.order_id = o.order_id AND od.item_name NOT LIKE  'gift card' ", 'LEFT');
        $this->db->where('payment_status', 'Processed');
        $this->db->order_by('order_date', 'DESC');
        $this->db->group_by('o.order_id');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        $order['rows'] = $query->result_array();
        $order['cnt'] = $rowcount;
        $order['status'] = $mode;
        return $order;
    }

    public function getOrderByStatusForVendors($mode, $vendorId, $order_id = array(), $not_order_id = array())
    {
        $qstring = '';
        $havStatus = $mode == '' ? false : true;
        if ($havStatus) {
            $qstring = "AND status = '" . $mode . "'";
            $havStatus = false;
        }
        $this->db->select('o.*,u.first_name, u.last_name,os.status,os.add_date,os.status_desc, count(item_name) item_count,IF(COALESCE(re.new_order_id,"N") ="N" ,"No", CONCAT("Order id - ",re.order_id)) is_rework', false);
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id ' . $qstring . ' AND is_active = 1 AND os.status <> "Order Acknowledged"', 'inner');
        $this->db->join('order_details od', "od.order_id = o.order_id AND od.item_name NOT LIKE  'gift card' ", 'LEFT');
        $this->db->join('order_rework re', 're.new_order_id = o.order_id', 'left outer');
        $this->db->where('payment_status', 'Processed');
        $this->db->where('os.user_id', $vendorId);
        if (sizeof($order_id)) {
            $this->db->where_in('o.order_id', $order_id);
        }
        if (sizeof($not_order_id)) {
            $this->db->where_not_in('o.order_id', $not_order_id);
        }
        $this->db->order_by('o.order_id', 'DESC');
        $this->db->group_by('o.order_id');
        $query = $this->db->get();

        $rowcount = $query->num_rows();
        $order['rows'] = $query->result_array();
        $order['vendorId'] = $vendorId;
        $order['cnt'] = $rowcount;
        $order['status'] = $mode;
        return $order;
    }

    public function getVendorDet($vendorId)
    {
        return $this->db->get_where('users', ['user_id' => $vendorId])->result_array();
    }

    public function getOrderdet($orderId)
    {
        return $this->db->get_where('order_details', ['order_id' => $orderId])->result_array();
    }

    public function getStatus($orderId)
    {
        return $this->db->get_where('order_status', ['order_id' => $orderId, 'is_active' => 1])->result_array();
    }

    public function refund($orderId, $invoiceNo, $refundamount, $refundreason)
    {
        $data = array(
            'order_id' => $orderId,
            'invoice_no' => $invoiceNo,
            'status' => 'Refund',
            'add_date' => date('Y-m-d H:i:s'),
            'user_id' => 0,
            'status_desc' => 'Estimated refund date : ' . date('Y-m-d H:i:s', strtotime("+14 days")) .
            'Refund Amount:' . $refundamount . 'Refund Reason :' . $refundreason,
            'is_active' => 1,
        );
        $this->updateOrderStatus($data);
    }

    public function testing($orderId, $invoiceNo)
    {
        $data = array(
            'order_id' => $orderId,
            'invoice_no' => $invoiceNo,
            'status' => 'Testing',
            'add_date' => date('Y-m-d H:i:s'),
            'user_id' => 0,
            'status_desc' => 'Order date : ' . date('Y-m-d H:i:s'),
            'is_active' => 1,
        );
        $this->updateOrderStatus($data);
        return true;
    }

    public function cancelled($orderId, $invoiceNo)
    {
        $data = array(
            'order_id' => $orderId,
            'invoice_no' => $invoiceNo,
            'status' => 'Cancelled',
            'add_date' => date('Y-m-d H:i:s'),
            'user_id' => 0,
            'status_desc' => 'Order cancelled date : ' . date('Y-m-d H:i:s'),
            'is_active' => 1,
        );
        $this->updateOrderStatus($data);
        return true;
    }

    public function orderShipped($orderId, $invoiceNo, $trackingNo, $carrier, $shippingDate, $deliveryDate)
    {
        $userId = $this->getUserId($orderId);

        $data = array(
            'order_id' => $orderId,
            'invoice_no' => $invoiceNo,
            'status' => 'Order Shipped',
            'add_date' => date('Y-m-d H:i:s'),
            'user_id' => $userId[0]['user_id'],
            'status_desc' => 'Order Shipped date : ' . date('Y-m-d H:i:s') .
            'Tracking No :' . $trackingNo . 'Shipping Date : ' . $shippingDate .
            'Delivery Date :' . $deliveryDate,
            'is_active' => 1,
        );
        $this->updateOrderStatus($data);
        return true;
    }

    public function getUserId($orderId)
    {
        $this->db->select('user_id');
        $this->db->from('order_status');
        $this->db->where('order_id', $orderId);
        $this->db->where('status', "Manufacturing in progress");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function assignToVendor($orderId, $vendorId, $invoiceNo)
    {
        $data = array(
            'order_id' => $orderId,
            'invoice_no' => $invoiceNo,
            'status' => 'Manufacturing in progress',
            'add_date' => date('Y-m-d H:i:s'),
            'user_id' => $vendorId,
            'status_desc' => 'Estimated completion date : ' . date('Y-m-d H:i:s', strtotime("+14 days")),
            'is_active' => 1,
        );
        $this->updateOrderStatus($data);
        return true;
    }

    public function getPaymentDetails($payment_id = 0)
    {
        $this->db->select('p.*, u.first_name, o.gross_amt,o.discount_perc,o.tax_amount');
        $this->db->from('payment_details p');
        $this->db->join('users u', 'p.vendor_id = u.user_id');
        $this->db->join('order o', 'p.order_id = o.order_id');
        $this->db->order_by('p.payment_date', 'DESC');
        if (isset($payment_id) && $payment_id != 0) {
            $this->db->where('p.id', $payment_id);
        }
        return $this->db->get()->result_array();
    }

    public function getPayments($payment_id = 0)
    {
        $this->db->select('p.*, u.first_name');
        $this->db->from('payments p');
        $this->db->join('users u', 'p.vendor_id = u.user_id');
        return $this->db->get()->result_array();
    }

    public function getPaymentDetail($orderId, $vendorId)
    {
        return $this->db->get_where('payment_details', ['vendor_id' => $vendorId, 'order_id' => $orderId])->result_array();
    }

    public function savePayment($data, $payment_id)
    {
        if ($payment_id) {
            $this->db->where('id', $payment_id)->update('payment_details', $data);
        } else {
            $query = $this->db->get_where('payment_details', ['design_id' => $data['design_id'], 'order_id' => $data['order_id']]);
            if ($query->num_rows() == 0) {
                $this->db->insert('payment_details', $data);
                $payment_id = $this->db->insert_id();

                $vendor_id = $data['vendor_id'];

                $query = $this->db->get_where('payments', ['vendor_id' => $vendor_id, 'advance != ' => 0]);
                if ($query->num_rows() > 0) {
                    $result = $query->result_array();
                    $data['paid_amount'] = 0;
                    foreach ($result as $row) {
                        if ($row['advance'] >= $data['manufacturing_cost'] + $data['shipping_cost'] - $data['paid_amount']) {
                            $distribution = json_decode($row['distribution']);
                            array_push($distribution, array('id' => $payment_id, 'paid_amount' => $data['manufacturing_cost'] + $data['shipping_cost'], 'payment_status' => 'paid'));

                            $distribution = json_encode($distribution);

                            $this->db->where('id', $row['id'])->update('payments', array('advance' => $row['advance'] - $data['manufacturing_cost'] - $data['shipping_cost'] + $data['paid_amount'], 'distribution' => $distribution));
                            $this->db->where('id', $payment_id)->update('payment_details', array('paid_amount' => $data['manufacturing_cost'] + $data['shipping_cost'], 'payment_status' => 'paid', 'payment_date' => $row['payment_date']));
                            break;
                        } else {
                            $distribution = json_decode($row['distribution']);
                            array_push($distribution, array('id' => $payment_id, 'paid_amount' => $row['advance'], 'payment_status' => 'partial'));
                            $distribution = json_encode($distribution);

                            $this->db->where('id', $row['id'])->update('payments', ['advance' => 0, 'distribution' => $distribution]);
                            $this->db->where('id', $payment_id)->update('payment_details', ['paid_amount' => $row['advance'], 'payment_status' => 'partial', 'payment_date' => $row['payment_date']]);

                            $data['paid_amount'] += $row['advance'];
                        }
                    }
                }
            } else {
                $result = $query->row_array();
                $payment_id = $result['id'];
            }
        }
        return $payment_id;
    }

    public function submitPayment($payment_data)
    {

        $temp_amount = $payment_data['amount'];
        $this->db->select('*');
        $this->db->from('payment_details');
        $this->db->where('vendor_id', $payment_data['vendor_id']);
        $this->db->not_like('payment_status', 'paid');
        $query = $this->db->get();
        $result = $query->result_array();
        $distribution = array();
        $i = 0;
        foreach ($result as $row) {
            if ($row['manufacturing_cost'] + $row['shipping_cost'] - $row['paid_amount'] <= $temp_amount) {
                $data = array(
                    'paid_amount' => $row['manufacturing_cost'] + $row['shipping_cost'],
                    'payment_status' => 'paid',
                    'payment_date' => $payment_data['payment_date'],
                );
                $this->db->where('id', $row['id'])->update('payment_details', $data);
                $temp_amount -= $row['manufacturing_cost'] + $row['shipping_cost'] - $row['paid_amount'];
                $distribution[$i] = array(
                    'id' => $row['id'],
                    'paid_amount' => $row['manufacturing_cost'] + $row['shipping_cost'] - $row['paid_amount'],
                    'payment_status' => 'paid',
                );
            } else {
                if ($temp_amount !== 0) {
                    $data = array(
                        'paid_amount' => $temp_amount + $row['paid_amount'],
                        'payment_status' => 'partial',
                        'payment_date' => $payment_data['payment_date'],
                    );
                    $this->db->where('id', $row['id']);
                    $this->db->update('payment_details', $data);
                    $distribution[$i] = array(
                        'id' => $row['id'],
                        'paid_amount' => $temp_amount + $row['paid_amount'],
                        'payment_status' => 'partial',
                    );
                    $temp_amount = 0;
                }
                break;
            }
        }
        $payment_data['date'] = date('Y-m-d H:i:s');
        $payment_data['distribution'] = json_encode($distribution);
        $payment_data['advance'] = $temp_amount;

        $this->insertPayments($payment_data);
        return true;
    }

    private function insertPayments($payment_data)
    {
        $this->db->insert('payments', $payment_data);
    }

    public function statusToShipped($orderId, $trackingNo, $carrier, $shippedDate, $deliveryDate, $invoiceNo, $vendorId)
    {
        $date = str_replace('/', '-', $shippedDate); // date('Y-m-d H:i:s')
        $deliveryDate = str_replace('/', '-', $deliveryDate); // date('Y-m-d H:i:s')
        $data = array(
            'order_id' => $orderId,
            'invoice_no' => $invoiceNo,
            'status' => 'Order Shipped',
            'add_date' => date('Y-m-d H:i:s'),
            'user_id' => $vendorId,
            'status_desc' => 'Shipped date : ' . $date . ', Carrier : ' . $carrier . ', TrackingNo:' . $trackingNo . ', Estimated Delivery date : ' . $deliveryDate,
            'is_active' => 1,
        );
        $this->updateOrderStatus($data);
        return true;
    }

    public function updateOrderStatus($data)
    {
        $this->db->set('is_active', 0);
        $this->db->where('order_id', $data['order_id']);
        $this->db->update('order_status');
        $this->db->insert('order_status', $data);
        echo $data['status_desc'];
    }

    public function updateOrder($data)
    {
        $this->db->set('payment_status', "Not Processed");
        $this->db->where('order_id', $data['order_id']);
        $this->db->update('order');
    }

    public function getAmountPayable($vendor_id)
    {
        $result = $this->db->select(' COALESCE(SUM(  `manufacturing_cost` +  `shipping_cost` - `paid_amount` ),0) as amount', false)
            ->not_like('payment_status', 'paid')
            ->get('payment_details', ['vendor_id' => $vendor_id])->row_array();
        return $result['amount'];
    }

    public function getVendors()
    {
        return $this->db->where('parent_id IS NULL', null, false)->get_where('users', ['user_type_id' => '3'])->result_array();
    }

    public function insertPromoCode($promodet)
    {
        $this->db->insert('promocode', $promodet);
        return $this->db->insert_id();
    }

    public function getOrdersByStyleNameLastName($style = '', $last = '')
    {
        $this->db->select('DISTINCT (od.order_id)', false);
        $this->db->from('order_details od');
        $this->db->join('shoe_design sd', 'od.design_id = sd.shoe_design_id');
        $this->db->join('last_item_def lid', 'sd.last_id = lid.last_item_def_id');
        $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id');
        if ($style !== '') {
            $this->db->like('ls.style_name', $style, 'none');
        }
        if ($last !== '') {
            $this->db->like('lid.last_name', $last, 'none');
        }
        $this->db->order_by('od.order_id');
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);
        return $order_ids;
    }

    public function getOrdersByCustomerName($customer_name)
    {
        $this->db->select('DISTINCT (o.order_id)', false);
        $this->db->from('order o');
        $this->db->join('users u', 'o.user_id = u.user_id');
        $this->db->like('u.first_name', $customer_name);
        $this->db->or_like('u.last_name', $customer_name);
        $this->db->order_by('o.order_id');
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);
        return $order_ids;
    }

    public function getOrdersBypartnerid($partner_id)
    {
        $this->db->select('DISTINCT (o.order_id)', false);
        $this->db->from('order o');
        $this->db->join('partner_orders po', 'po.order_id = o.order_id ');
        $this->db->where('po.partner_id', $partner_id);
        $this->db->order_by('o.order_id');
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);
        return $order_ids;
    }

    public function get_kickstarter_orders()
    {
        $this->db->select('o.order_id', false);
        $this->db->from('order o');
        $this->db->like('o.order_source', 'kick starter', 'both');
        $this->db->where('o.kickstarter_no <>', 0);
        $this->db->order_by('o.order_id');
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);
        return $order_ids;
    }

    public function get_orders_by_promocode($promocode)
    {
        $this->db->select('DISTINCT (o.order_id)', false);
        $this->db->from('order o');
        $this->db->join('promocode p', 'o.promocode_id = p.promo_id');
        $this->db->like('p.promocode', $promocode);
        $this->db->order_by('o.order_id');
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);
        return $order_ids;
    }

    public function getOrders($order_number = '', $invoice_number = '')
    {
        $this->db->select('DISTINCT (o.order_id)', false);
        $this->db->from('order o');
        if ($order_number !== '') {
            $this->db->where('o.order_id', $order_number);
        }
        if ($invoice_number !== '') {
            $this->db->like('o.invoice_no', $invoice_number);
        }
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);
        return $order_ids;
    }

    public function getOrdersByStatus($status)
    {
        $this->db->select('DISTINCT (os.order_id)', false);
        $this->db->from('order_status os');
        $this->db->where('os.is_active', 1);
        $this->db->like('os.status', $status, 'none');
        $query = $this->db->get();
        $order_ids = array();
        if ($query->num_rows()) {
            $result = $query->result_array();
            foreach ($result as $key => $value) {
                $order_ids[] = $value['order_id'];
            }
        }
        array_push($order_ids, 0);
        return $order_ids;
    }

    public function inspirationReview($config, $status)
    {
        $fields = <<<EOD
                '' as mode,
                sd.shoe_design_id,
                sd.image_file,
                sd.public_name,
                CAST(sd.is_approved AS unsigned integer) AS is_approved,
                sd.name,
                sd.user_id,
                sd.date_created,
                u.first_name,
                u.last_name,
                u.email,
                ls.style_name,
EOD;

        $fields .= 'concat(cq.color_code, ' . ', ct.color_code, ' . ', cv.color_code, ' . ', ce.color_code, ' . ', ce.color_code, ' . ', cf.color_code) as colors';
        $this->db->select($fields, false);
        $this->db->from('shoe_design sd');
        $this->db->join('users u', 'u.user_id = sd.user_id', 'INNER');
        $this->db->join('last_item_def l', 'l.last_item_def_id = sd.last_id', 'INNER');
        $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id', 'INNER');
        $this->db->join('material_colors cq', 'cq.material_color_id = sd.q_clr_id', 'INNER');
        $this->db->join('material_colors ct', 'ct.material_color_id = sd.t_clr_id', 'INNER');
        $this->db->join('material_colors cv', 'cv.material_color_id = sd.v_clr_id', 'INNER');
        $this->db->join('material_colors ce', 'ce.material_color_id = sd.e_clr_id', 'INNER');
        $this->db->join('material_colors cf', 'cf.material_color_id = sd.f_clr_id', 'INNER');
        // $wheret = '(ct.color_name = "' .$color . '" AND spt.is_material = 1)' ;
        // $wherev = '(cv.color_name = "' .$color . '" AND spv.is_material = 1)' ;
        // $wheree = '(ce.color_name = "' .$color . '" AND spe.is_material = 1)' ;
        // $wheref = '(cf.color_name = "' .$color . '" AND spf.is_material = 1)' ;
        // $this->db->where($where);
        // $this->db->or_where($wheret);
        // $this->db->or_where($wherev);
        // $this->db->or_where($wheree);
        // $this->db->or_where($wheref);
        // $this->db->or_where('ct.color_name',$color);
        // $this->db->or_where('cv.color_name',$color);
        // $this->db->or_where('ce.color_name',$color);
        // $this->db->or_where('cf.color_name',$color);
        if ($status == 'accepted') {
            $this->db->where('sd.is_approved', 1);
        } else {
            $this->db->where('sd.is_approved', 0);
        }
        $this->db->where('sd.is_public', 1);
        $result['no_rows'] = $this->db->count_all_results('', false);
        $this->db->order_by('sd.shoe_design_id', 'DESC');
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result['result'] = $query->result_array();
            return $result;
        }

        return null;
    }

    public function inspired($designId, $modify)
    {
        if ($modify['shoe_' . $designId] == 'Accept') {
            $this->db->set('is_approved', 1);
        }
        if ($modify['shoe_' . $designId] == 'Reject') {
            $this->db->set('is_approved', 0);
        }
        $this->db->where('shoe_design_id', $designId)->update('shoe_design');
    }

    public function feedback()
    {
        $query = $this->db->get('feedback');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return null;
    }

    public function feedback_score()
    {
        $this->db->select('avg(score) as average', false);
        $this->db->from('feedback');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }

        return null;
    }

    public function updateStatusDesc($status_desc, $order_id)
    {
        $this->db->set('status_desc', $status_desc);
        $this->db->where('order_id', $order_id);
        $this->db->where('is_active', 1);
        $this->db->update('order_status');
        return true;
    }

    public function get_user_type($user_id)
    {
        $query = $this->db->select('user_type_id')->get_where('users', ['user_id' => $user_id]);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['user_type_id'];
        }
    }

    public function get_partner_id($user_id)
    {
        $query = $this->db->select('partner_id')->get_where('partner_users', ['user_id' => $user_id]);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['partner_id'];
        }
    }

    public function get_partner_logo($partner_id)
    {
        $this->db->select('a.file_name');
        $this->db->from('attachments a');
        $this->db->join('partners p', 'p.attachment_id=a.id', 'inner');
        $this->db->where('p.partner_id', $partner_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['file_name'];
        }
    }

    public function is_partner_order($order_id)
    {
        $query = $this->db->select('partner_id')->get_where('partner_orders', ['order_id' => $order_id]);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            return $result['partner_id'];
        }
    }

}
