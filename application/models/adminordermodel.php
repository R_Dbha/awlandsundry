<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class AdminOrderModel extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_styles()
    {
        $this->db->select('style_name as id, style_name as name');
        $this->db->from('custom_collection_types');
        $this->db->group_by('style_name');
        $this->db->order_by('id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function getProcessedOrders($config, $order_source, $filters)
    {
        $this->db->select('o.order_id,o.invoice_no, o.gross_amt, o.discount_perc, o.discount, o.tax_amount,
            o.order_date, order_modified, u.first_name,u.last_name, pd.design_id, o.belt_needed,
            o.shoe_horn,o.shoe_trees,kickstarter_no, p.company_name,
            if(os.status = "order shipped", os.add_date,"") AS shipped_date,
            if(os.status = "order shipped", os.status_desc,"") AS status_desc,
            if(os.status = "order shipped", DATEDIFF(os.add_date,o.order_date) ,"") AS day_diff,
            os.status,
            (SELECT CONCAT(city,", ", state,", ", country) FROM shipping_details WHERE o.order_id = order_id limit 1) AS location,
             GROUP_CONCAT( " ",ls.style_name) style_name,
             GROUP_CONCAT( " ",concat(lid.last_name,"(",lid.manufacturer_code,") ")) last,
             sum(manufacturing_cost) as manufacturing_cost,sum(shipping_cost) as shipping_cost,
            sum(item_amt*quantity) total_amt,  SUM( quantity) item_count,
            IF(COALESCE(re.new_order_id,"N") ="N" ,"No", CONCAT("Order id - ",re.order_id)) is_rework ,
            o.promocode_id,pc.promocode, pc.discount_per, pc.discount_amount,
            sc.store_credit_trans_id,sc.card_amount, sum(1) as number_of_items  ', false);
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id AND is_active = 1', 'inner');
        $this->db->join('order_details od', "od.order_id = o.order_id AND od.item_name NOT LIKE  'gift card' ", 'INNER');
        $this->db->join('shoe_design sd', 'od.design_id = sd.shoe_design_id');
        $this->db->join('last_styles ls', 'ls.last_style_id = sd.style_id');
        $this->db->join('last_item_def lid', 'lid.last_item_def_id = sd.last_id');
        $this->db->join('payment_details pd', 'pd.design_id = od.design_id', 'left');
        $this->db->join('order_rework re', 're.new_order_id = o.order_id', 'left');
        $this->db->join('promocode pc', 'pc.promo_id = o.promocode_id', 'left');
        $this->db->join('store_credit_trans sc', 'sc.gift_card_code  = o.order_id', 'left');
        $this->db->join('partner_orders po', 'po.order_id = o.order_id', 'left');
        $this->db->join('partners p', 'p.partner_id= po.partner_id', 'left');
        $this->db->where('o.payment_status', 'Processed');
        $this->db->like('o.order_source', $order_source);
        if ($filters) {
            if (isset($filters['order_status']) && $filters['order_status'] !== '') {
                $this->db->where('os.status', $filters['order_status']);
            }
            if (isset($filters['order_number']) && $filters['order_number'] !== '') {
                $this->db->where('o.order_id', $filters['order_number']);
            }
            if (isset($filters['style']) && $filters['style'] !== '') {
                $this->db->where('ls.style_name', $filters['style']);
            }
            if (isset($filters['last']) && $filters['last'] !== '') {
                $this->db->where('lid.last_item_def_id', $filters['last']);
            }
            if (isset($filters['invoice_number']) && $filters['invoice_number'] !== '') {
                $this->db->where('o.invoice_no', $filters['invoice_number']);
            }
            if (isset($filters['customer_name']) && $filters['customer_name'] !== '') {
                $this->db->where('(u.first_name LIKE "%' . $filters['customer_name'] . '%" OR u.last_name LIKE "%' . $filters['customer_name'] . '%" )', '', false);
            }
            if (isset($filters['partner_id']) && $filters['partner_id'] !== '') {
                $this->db->where('p.partner_id', $filters['partner_id']);
            }
        }
        $this->db->group_by('o.order_id');
        $this->db->order_by('o.order_id', 'DESC');
        $order['cnt'] = $this->db->count_results('', false);
        $this->db->limit($config['per_page'], $config['page']);
        $query = $this->db->get();
        $order['rows'] = $query->result_array();
        return $order;
    }

    public function get_rtw_styles()
    {
        $this->db->select('name as id, name as name');
        $this->db->from('custom_collection_types');
        $this->db->where('style_name', 'readywear');
        $this->db->order_by('id');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

}
