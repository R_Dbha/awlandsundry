<?php

/**
 * @author ajith
 * @date 24 Mar, 2015
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['app_title'] = 'Awl &amp; Sundry';
$config['contact_email'] = 'wecare@awlandsundry.com';
// $config['contact_phone'] = '(347) 671 4789';
$config['contact_phone'] = '(844) 785-9692';

$config['admin_email'] = 'wecare@awlandsundry.com';
$config['from_email'] = 'wecare@awlandsundry.com';
$config['nikunj_email'] = 'nikunj@awlandsundry.com';


$config['staff_email1'] = 'wecare@awlandsundry.com';
$config['staff_email2'] = 'wecare@awlandsundry.com';

$config['developer_email'] = 'ajith@kraftlabs.com';

$config['date_format'] = 'd/m/Y H:ia';

