<?php

/**
 * @author Ajith Vp <ajith@kraftlabs.com>
 * @copyright Copyright (c) 2014, Ajith Vp <ajith@kraftlabs.com>
 * @date 22 Sep, 2014
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['customer_care_number'] = '(917) 825 9404';  
//$config['customer_care_number'] = '518-522-5400';  //temporary
