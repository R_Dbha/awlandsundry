<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "common";
$route['404_override'] = '';
$route['error_404'] = 'common/error_404';


$route['my-account'] = 'useraccount';
$route['my-account/(:any)'] = 'useraccount/$1';

$route['shop/products/(:any)/(:any)'] = 'ready_wear/products/$1/$2/';
$route['products/(:any)/(:any)/(:any)'] = 'custom_collection/products/$1/$2/$3/';
$route['products/(:any)/(:any)'] = 'custom_collection/type/$1/$2/';
$route['products/(:any)'] = 'custom_collection/index/$1/';
$route['kam-chancellor-collection'] = 'custom_collection/collection/kam_chancellor/';
$route['special/(:any)/(:any)'] = 'custom_collection/special_product/$1/$2';
$route['special/(:any)'] = 'custom_collection/special/$1/';

$route['create-a-custom-shoe'] = 'customshoe/sizing';
$route['create-a-custom-shoe/select-style'] = 'customshoe/index';
$route['create-a-custom-shoe/select-last'] = 'customshoe/last';
$route['create-a-custom-shoe/sole-attributes'] = 'customshoe/sole';
$route['create-a-custom-shoe/select-last/(:any)'] = 'customshoe/last/$1/';
$route['create-a-custom-shoe/design-features'] = 'customshoe/features';
$route['create-a-custom-shoe/materials-and-colors'] = 'customshoe/styling';
$route['create-a-custom-shoe/details'] = 'customshoe/stitchlace';
$route['create-a-custom-shoe/sizing'] = 'customshoe/sizing';
$route['create-a-custom-shoe/related/(:any)'] = 'customshoe/related/$1';

$route['cart'] = 'common/cart';
$route['checkout'] = 'common/checkout';

$route['checkout_cart/(:any)'] = 'common/checkout_cart/$1';
$route['login'] = 'common/login';
$route['login_popup'] = 'common/login_popup';
$route['logout'] = 'common/logout';
$route['signup'] = 'common/signup';
$route['forgot-password'] = 'common/forgot_password';
$route['order-confirm/(:num)'] = 'common/order_confirmation/$1';

$route['faq'] = 'common/faqs';
$route['jobs'] = 'common/jobs';
$route['press'] = 'common/press';
$route['privacy-policy'] = 'common/privacy_policy';
$route['terms-and-conditions'] = 'common/terms';

$route['pay-by-klarna/(:num)'] = 'transaction/payByKlarna/$1';
$route['klarna-order-confirm/(:any)'] = 'transaction/klarnaOrderConfirm/$1';
$route['klarna-create-order/(:any)'] = 'transaction/klarnaCreateOrder/$1';
$route['validate-klarna-order/(:any)'] = 'transaction/validateKlarnaOrder/$1';
//$route['klarna-save-shipping/(:any)'] = 'transaction/klarnaSaveShipping/$1';

$route['pay-by-card/(:num)'] = 'transaction/payByCard/$1';
$route['payment-cancel/(:num)'] = 'transaction/paymentcancel/$1';
$route['ipn/(:num)'] = 'transaction/ipn/$1';
$route['payment-success/(:num)'] = 'transaction/paymentsuccess/$1';
$route['paypal/(:num)'] = 'transaction/paypal/$1';

$route['gift-card'] = 'giftcard';
$route['gift-card/order'] = 'giftcard/order';

$route['google/signup'] = 'googlelogin/signup';
$route['facebook/signup'] = 'facebooklogin/signup';
$route['linkedin/signup'] = 'linkedin_signup/initiate';

$route['shop/get_collections'] = 'custom_collection/get_collections';
$route['shop'] = 'custom_collection/shop';
$route['shop/ready-wear'] = 'ready_wear';
$route['shop/the-blacklist-collection'] = 'ready_wear';

$route['appointment'] = 'appointment';
$route['about-us'] = 'common/about_us';

/* End of file routes.php */
/* Location: ./application/config/routes.php */