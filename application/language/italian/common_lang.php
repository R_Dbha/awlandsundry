<?php

$lang["Calf Plain"] = "Pelle Liscia";
$lang["Calf Grain"] = "Pelle Ghiaia";
$lang["Calf Suede"] = "Pelle Scamosciata";
$lang["Faux Croc"] = "Pelle Cocco Stampato";

$lang["Plain Dark Brown"] = "Castagna Lisco";
$lang["Plain Cacao"] = "Cioccolato Liscio";
$lang["Plain Walnut"] = "Walnut Liscio 3404 Malaga Pellami";
$lang["Plain Mahagony"] = "Noce Liscio";
$lang["Plain Mahogany"] = "Noce Liscio";
$lang["Plain Navy"] = "Navy Liscio 3397 Malaga Pellami";
$lang["Plain Ivory"] = "Avorio Liscio 3154 Malaga Pellami";
$lang["Plain Green"] = "Verde Liscio 3753 Malaga Pellami";
$lang["Plain Black"] = "Nero Liscio";

$lang["Pebble Grain Black"] = "Nero Ghiaia";
$lang["Pebble Grain Venetian Brown"] = "Cioccolato Ghiaia";
$lang["Pebble Grain White"] = "Bianco Ghiaia";
$lang["Pebble Grain Chestnut"] = "Cuoio Ghiaia Riferimento Blacklist";

$lang["Suede Black"] = "Nero Scamosciato";
$lang["Suede Chocolate Brown"] = "Cioccolato Scamosciato 013 Conceria Zabri Art Discovery";
$lang["Suede Navy"] = "Navy Scamosciato 013 Conceria Zabri Art Discovery";
$lang["Suede Grey"] = "Grigio Scamosciato 021 Conceria Zabri Art Discovery";
$lang["Suede Aubergine"] = "Aubergine Scamosciato 021 Conceria Zabri Art Discovery";
$lang["Suede Desert Sand"] = "Desert Sand 512 Conceria Zabri";
$lang["Suede Red"] = "Rosso Scamosciato";

$lang["Black Faux Croc"] = "Nero Cocco Stampato";
$lang["Dark Brown Faux Croc (currently says Purple Brown, please change on front end)"] = "Cioccolato Cocco Stampato";
$lang["Green Faux Croc"] = "Verde Cocco Stampato 3753 Malaga Pellami";
$lang["Navy Faux Croc"] = "Navy Cocco Stampato 3397 Malaga Pellami";
$lang["Red Faux Croc"] = "Rosso Cocco Stampato 3497 Malaga Pellami";

$lang["Full Leather"] = "Cuoio";
$lang["Half Rubber"] = "Mezzo Cuoio/Gomma";
$lang["Rubber"] = "Gomma";
$lang["Full Rubber"] = "Gomma";

$lang["Black"] = "Nero";
$lang["Brown"] = "Marrone";
$lang["Natural"] = "Marrone Chiaro";

$lang["Grey"] = "Grigio";
$lang["Symphony Blue"] = "Blu Scuro";
$lang["Blue"] = "Blu Chiaro";
$lang["Teal"] = "Turchese";
$lang["Cacao"] = "Cioccolato";
$lang["Dark Brown"] = "Marrone Scuro";
$lang["Tan"] = "Marrone Chiaro";
$lang["Orange"] = "Arancia";
$lang["Burgundy"] = "Borgogna";
$lang["Pink"] = "Rosa";
$lang["Red"] = "Rosso";
$lang["Mustard"] = "Mostarda";
$lang["Yellow"] = "Giallo";
$lang["White"] = "Bianco";

$lang["Toe"] = "Punta";
$lang["Vamp"] = "Tommaio";
$lang["Quarter"] = "Tommaio";
$lang["Eyestays"] = "Gambetto";
$lang["Eyestay"] = "Gambetto";
$lang["Foxing"] = "Toppone";
$lang["Stitching"] = "Cucitura";
$lang["Laces"] = "Laci";
$lang["Monogram"] = "Monogramma";
$lang["Sole Type"] = "Tipo Suola";
$lang["Sole Edge Color"] = "Colore Suola";
$lang["Shoe Construction"] = "Construzione Scarpe";
$lang["Yes"] = "Si";
$lang["No"] = "No";
$lang["Shoe Horn"] = "Calzascarpe";
$lang["Shoe Trees"] = "Alberi di scarpe";

$lang["On Left Counter, Left Shoe"] = "Sulla Scarpa Sinistra, Tacco Estero";