<?php

$lang["Calf Plain"] = "Calf Plain";
$lang["Calf Grain"] = "Calf Grain";
$lang["Calf Suede"] = "Calf Suede";
$lang["Faux Croc"] = "Faux Croc";

$lang["Plain Dark Brown"] = "Plain Dark Brown";
$lang["Plain Cacao"] = "Plain Cacao";
$lang["Plain Walnut"] = "Plain Walnut";
$lang["Plain Mahagony"] = "Plain Mahagony";
$lang["Plain Mahogany"] = "Plain Mahogany";
$lang["Plain Navy"] = "Plain Navy";
$lang["Plain Ivory"] = "Plain Ivory";
$lang["Plain Green"] = "Plain Green";
$lang["Plain Black"] = "Plain Black";

$lang["Pebble Grain Black"] = "Pebble Grain Black";
$lang["Pebble Grain Venetian Brown"] = "Pebble Grain Venetian Brown";
$lang["Pebble Grain White"] = "Pebble Grain White";
$lang["Pebble Grain Chestnut"] = "Pebble Grain Chestnut";

$lang["Suede Black"] = "Suede Black";
$lang["Suede Chocolate Brown"] = "Suede Chocolate Brown";
$lang["Suede Navy"] = "Suede Navy";
$lang["Suede Grey"] = "Suede Grey";
$lang["Suede Aubergine"] = "Suede Aubergine";
$lang["Suede Desert Sand"] = "Suede Desert Sand";
$lang["Suede Red"] = "Suede Red";

$lang["Black Faux Croc"] = "Black Faux Croc";
$lang["Dark Brown Faux Croc (currently says Purple Brown, please change on front end)"] = "Dark Brown Faux Croc (currently says Purple Brown, please change on front end)";
$lang["Green Faux Croc"] = "Green Faux Croc";
$lang["Navy Faux Croc"] = "Navy Faux Croc";
$lang["Red Faux Croc"] = "Red Faux Croc";

$lang["Full Leather"] = "Full Leather";
$lang["Half Rubber"] = "Half Rubber";
$lang["Rubber"] = "Rubber";
$lang["Full Rubber"] = "Rubber";

$lang["Black"] = "Black";
$lang["Brown"] = "Brown";
$lang["Natural"] = "Natural";

$lang["Grey"] = "Grey";
$lang["Symphony Blue"] = "Dark Blue";
$lang["Blue"] = "Blue";
$lang["Teal"] = "Teal";
$lang["Cacao"] = "Cacao";
$lang["Dark Brown"] = "Dark Brown";
$lang["Tan"] = "Tan";
$lang["Orange"] = "Orange";
$lang["Burgundy"] = "Burgundy";
$lang["Pink"] = "Pink";
$lang["Red"] = "Red";
$lang["Mustard"] = "Mustard";
$lang["Yellow"] = "Yellow";
$lang["White"] = "White";

$lang["Toe"] = "Toe";
$lang["Vamp"] = "Vamp";
$lang["Quarter"] = "Quarter";
$lang["Eyestays"] = "Eyestays";
$lang["Eyestay"] = "Eyestay";
$lang["Foxing"] = "Foxing";
$lang["Stitching"] = "Stitching";
$lang["Laces"] = "Laces";
$lang["Monogram"] = "Monogram";
$lang["Sole Type"] = "Sole Type";
$lang["Sole Egde Color"] = "Sole Egde Color";
$lang["Shoe Construction"] = "Shoe Construction";
$lang["Yes"] = "Yes";
$lang["No"] = "No";
$lang["Shoe Horn"] = "Shoe Horn";
$lang["Shoe Trees"] = "Shoe Trees";

$lang["On Left Counter, Left Shoe"] = "On Left Counter, Left Shoe";