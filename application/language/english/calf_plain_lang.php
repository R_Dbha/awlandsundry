<?php

$lang["Black"] = "Black";
$lang["Dark Brown"] = "Dark Brown";
$lang["Cacao"] = "Cacao";
$lang["Walnut"] = "Walnut";
$lang["Mahagony"] = "Mahagony";
$lang["Mahogany"] = "Mahogany";
$lang["Navy"] = "Navy";
$lang["Ivory"] = "Ivory";
$lang["Rainforest Green"] = "Rainforest Green";
