<?php
/**
 * Description of shoemodel
 *
 * @author kraftlabs
 */
class AdminViewModel extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    public function getProcessedOrders($orderId = NULL)
    {
        $this->db->select('*');
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id AND is_active = 1', 'inner');
        $this->db->where('payment_status', 'Processed');  
        if($orderId != NULL)
            $this->db->where('order_id', $orderId); 
        $this->db->order_by('order_date', 'DESC');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        $order['rows'] = $query->result_array();
        $order['cnt'] = $rowcount;
        return $order; 
    }
    public function getOrderByStatus($mode)
    {
        $qstring = '';
        $havStatus = $mode == '' ? false : TRUE;
        if($havStatus){
            $qstring = "AND status = '".$mode."'";$havStatus = FALSE;
        }
        $this->db->select('*');
        $this->db->from('order o');
        $this->db->join('users u', 'u.user_id = o.user_id', 'inner');
        $this->db->join('order_status os', 'os.order_id = o.order_id ' .$qstring. ' AND is_active = 1', 'inner');
        $this->db->where('payment_status', 'Processed');  
        $this->db->order_by('order_date', 'DESC');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        $order['rows'] = $query->result_array();
        $order['cnt'] = $rowcount;
        $order['status'] = $mode;
        return $order; 
    }
    
    public function getOrderdet($orderId)
    {
        $this->db->select('*');
        $this->db->from('order_details o');
        //$this->db->join('order_status os', 'os.order_id = o.order_id', 'inner');
        $this->db->where('order_id', $orderId);        
        $query = $this->db->get();
        $orderdet = $query->result_array();
        //var_dump($orderdet);die();
        return $orderdet; 
    }
    public function getStatus($orderId){
        $this->db->select('*');
        $this->db->from('order_status');
        $this->db->where('order_id', $orderId); 
        $this->db->where('is_active', 1); 
        $query = $this->db->get();
        $orderstatus = $query->result_array();
        //var_dump($orderdet);die();
        return $orderstatus; 
    }

    public function assignToVendor($orderId,$vendorId)
    {
        $data = array('order_id' => $orderId,
                    'invoice_no' => '',
                    'status' => 'Manufacturing in progress',
                    'add_date'=> date('Y-m-d H:i:s'),
                    'user_id' => $vendorId,
                    'status_desc' => 'Estimated completion date : '.date('Y-m-d H:i:s',strtotime("+14 days")),
                    'is_active' => 1
                    );
        //assign to vendor required
        $this->updateOrderStatus($data);
        return TRUE;
    }
    public function updateOrderStatus($data)
    {
        $this->db->set('is_active',0);
        $this->db->where('order_id',$data['order_id']);
        $this->db->update('order_status');
        $this->db->insert('order_status', $data);
    }
    public function getVendors()
    {
        $this->db->select('*');
        $this->db->from('users');
        //$this->db->join('order_details od', 'o.order_id = od.order_id', 'inner');
        $this->db->where('user_type_id', 3);        
        $query = $this->db->get();
        return $query->result_array();
    }
    
}

?>
