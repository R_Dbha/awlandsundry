<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of basic_view
 *
 * @author ajith
 */
class basic_view extends Exception {

    private $default_js = array();
    private $default_css = array();
    private $additional_js = array();
    private $additional_css = array();

    public function get_default_view() {
        $default['meta'] = 'meta';
        $default['css'] = $this->get_css();
        $default['js'] = $this->get_js();

        $default['title'] = 'title';
        $default['header'] = 'header';
        $default['header_data']['logged_in_user'] = '';
        $default['side_bar'] = 'side_bar';
        $default['content'] = 'content';
        $default['content_data']['content_text'] = '';
        $default['footer'] = 'footer';
        return $default;
    }

    public function set_default_js($js) {
        if (is_array($js)) {
            $this->default_js = $js;
        } elseif (is_string($js)) {
            $this->default_js = array($js);
        }
    }

    public function set_default_css($css) {
        if (is_array($css)) {
            $this->default_css = $css;
        } elseif (is_string($css)) {
            $this->default_css = array($css);
        }
    }

    public function set_additonal_js($js) {
        if (is_array($js)) {
            $this->additional_js = $js;
        } elseif (is_string($js)) {
            $this->additional_js = array($js);
        }
    }

    public function set_additonal_css($css) {
        if (is_array($css)) {
            $this->additional_css = $css;
        } elseif (is_string($css)) {
            $this->additional_css = array($css);
        }
    }

    public function get_js() {
        $combined = array_merge($this->default_js, $this->additional_js);
        $result = '';
        foreach ($combined as $value) {
            $result .= '<script type="text/javascript" src="' . base_url().$value . '.js" ></script>';
        }
        return $result;
    }

    public function get_css() {
        $combined = array_merge($this->default_css, $this->additional_css);
        $result = '';
        foreach ($combined as $value) {
            $result .= '<link href="' .base_url(). $value . '.css" media="screen" rel="stylesheet" type="text/css">';
        }
        return $result;
    }

}

?>
