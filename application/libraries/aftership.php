<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class aftership
{
	private $_api_url = 'https://api.aftership.com';
	protected $_api_key = 'beeb4cc0-77e0-4f16-b69d-e005736f84ff';
	private $_api_version = 'v4';
	private $_client;
        
        public function send($url, $request_type, $data = array())
	{
		$headers = array(
			'aftership-api-key' => $this->_api_key,
			'content-type' => 'application/json'
		);
		switch (strtoupper($request_type)) {
			case "GET":
				$request = $this->get($this->_api_url . '/' . $this->_api_version . '/' . $url, $headers, array('query' => $data));
				break;
			case "POST":
				$request = $this->post($this->_api_url . '/' . $this->_api_version . '/' . $url, $headers, json_encode($data));
				break;
			case "PUT":
				$request = $this->_client->put($this->_api_url . '/' . $this->_api_version . '/' . $url, $headers, json_encode($data));
				break;
			case "DELETE":
				$request = $this->_client->delete($this->_api_url . '/' . $this->_api_version . '/' . $url, $headers, json_encode($data));
				break;
		}
		try {
			$response = $request->send()->json();
		} catch (BadResponseException $exception) {
			$response = $exception->getResponse()->json();
		} catch (GuzzleException $exception) {
			throw $exception;
		}
		return $response;
	}
        public function create($tracking_number, array $params = array())
	{
		if (empty($tracking_number)) {
			throw new \Exception('Tracking number cannot be empty');
		}
		$params['tracking_number'] = $tracking_number;
		return $this->send('trackings', 'POST', array('tracking' => $params));
	}
        public function get($slug, $tracking_number, $params)
	{
		if (empty($slug)) {
			throw new \Exception('Slug cannot be empty');
		}
		if (empty($tracking_number)) {
			throw new \Exception('Tracking number cannot be empty');
		}
		return $this->send('trackings/' . $slug . '/' . $tracking_number, 'POST', $params);
	}
}