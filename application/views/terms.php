<?php
	require_once('includes/header.php');
?>
<div id="terms-wrapper">
	<div id="terms" class="container">
		<h3 class="page-title">
			Terms Of Use<!--span style="color: #979797;">TERMS AND CONDITIONS OF USE</span-->
		</h3>
		<div id="terms-menu" class="left ">
			<ul class="slicknav">
				
			</ul>
		</div>
		<p>Welcome to Awl & Sundry. Awl & Sundry LLC ("Awl & Sundry", "we", "our", or "us") is the owner and operator of awlandsundry.com ("Website"). The following explains our terms and conditions of use (Terms). Please carefully read these Terms before using or obtaining any information, products, or services through our Website. Our goal is to provide shoppers and those who purchase from us ("Customer", "you", or "your") access to quality handmade and custom footwear using our unique online shoe design interface. By accessing or using our Website, you expressly agree to be bound by these Terms and Conditions of Use ("Terms") and our Privacy Policy, which creates a legal and enforceable agreement whether or not you make a purchase. If you do not agree to all of these Terms and our Privacy Policy, do not browse or use our Website.</p>
		<p>We encourage you to regularly review these Terms as we may amend them from time to time.</p>

		
		<h4>1.  General.</h4>
		<p>(a)  The content of the pages of our Website and the products available through Website are subject to change without notice. By using our Website, you agree to any changes to our Terms and other guidelines or policies governing our Website.</p>

		<p>(b) We are happy that you are our Customer. The language of these Terms is not in any way intended to constitute an agency relationship, joint venture, or partnership for any purpose.</p>



		<h4>2. Use of Our Website.</h4>
		<p>(a) Your use and access to our Website and the content hereon, including the use and access of our shoe design interface, constitutes a personal, non-transferable, non-assignable, revocable, limited and temporary license ("License") to use our Website subject to these Terms. Your limited License to use our Website is ongoing and continues until you cease using our Website, as determined by us in our sole discretion, or your License or ability to use our Website is terminated or restricted by us according to Section 5. You expressly agree not to attempt to hack, crack, reverse engineer, harm, copy, link or deep link to, or in any way interfere with our shoe design interface or any other portion of our Website.</p>
		<p>(b) You agree not to use our Website for any illegal, harmful, or otherwise inappropriate purpose, as is determined by us in our sole discretion. Such use is a material breach of these Terms and may result in the termination of your user account and/or your ability to use our Website.</p>

		<p>(c) You may utilize our Website only if you are over the age of eighteen (18) and are capable of entering into contracts. Individuals under the age of eighteen (18) may use our Website only with the consent of their legal parent or guardian.</p>


		<h4>3. User Account.</h4>
		<p>We provide you with the ability to create a user account in order to make it easier for you to create and purchase custom shoes from us. However, it is your obligation to maintain and control passwords to your account. You are responsible for all activities that occur in connection with your user name and password. You agree to notify us of any unauthorized uses of your user name and password and/or any other breaches of security. We will not be liable for any loss or damages of any kind, under any legal theory, caused by your failure to comply with your security obligations hereunder or caused by any person to whom you grant access to your account.</p>

		<h4>4. Linked Websites.</h4>

		<p>(a) Our Website may provide links to other websites by allowing you to leave our Website to access third-party material or by bringing third-party material into our Website via "inverse" hyperlinks and framing technology (a "Linked Website"). We do not have discretion to alter, update, or control the content on a Linked Website. The fact that we have provided a link to a Linked Website is not an endorsement, authorization, sponsorship, or affiliation with respect to such Linked Website, its owners, or its providers and is intended for your convenience. There are risks in relying upon, using or retrieving information found on the Internet, and we urge you to make sure you understand these risks before relying upon, using, or retrieving any such information on a Linked Website.</p>

		<p>(b) All content and products on our Website or obtained from a Linked Website are provided to you "AS IS" without warranty of any kind either express or implied including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose, title, non-infringement, security and accuracy.</p>

		<p>(c) We do not endorse and are not responsible for (i) the accuracy or reliability of an opinion, advice, or statement made through our Website, (ii) any content provided on Linked Websites or (iii) the capabilities or reliability of any product or Website obtained from a Linked Website. Other than as required under applicable consumer protection law, under no circumstance will we be liable for any loss or damage caused by your reliance on any information or product obtained through our Website or a Linked Website. It is your responsibility to evaluate the accuracy, completeness or usefulness of any opinion, advice, or other content available through our Website, or obtained from a Linked Website.</p>

		<h4>5. Termination, Restriction, and Suspension.</h4>
		<p>We retain the right to terminate, restrict, or suspend: your user account; your License to use or access or Website; and/or your use of or access to our Website at any time in our absolute and sole discretion, without prior notice, for any reason or no reason. Should we elect to take any of the aforementioned actions, your License shall automatically be revoked, and you agree not to browse, otherwise access or use our Website in any way. Failure to comply with these Terms, our Privacy Policy, or any of our other policies constitutes a breach of these Terms which may result in the termination your user account and/or your ability to use our Website. Failure to address any said breach caused by you or another party does not waive our right to act on similar breaches.</p>

		<h4>6. Liability.</h4>
		<p>(a) You understand and agree that your use of any information or materials from our Website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products or information available through our Website meet your specific desires, tastes, and aesthetic preferences prior to purchasing footwear or other products from us.</p>

		<p>(b) Although we strive to give you a joyful shopping experience, the information, products, and descriptions of products published on our Website may include inaccuracies or typographical errors, and we disclaim any liability for such inaccuracies or errors. However, we will from time to time do our best to update the content on our Website as new products become available or change. But we do not warrant or represent that the content on our Website is complete or up-to-date. As we grow, we may update the content on or make improvements to our Website at any time.</p>

		<p>(c) You agree that we, our affiliates, and any of our or their respective officers, directors, employees, or agents will not be liable, whether in contract, tort, strict liability or otherwise, for any indirect, punitive, special, consequential, incidental or indirect damages (including without limitation lost profits, cost of procuring substitute Website or lost opportunity) arising out of or in connection with the delay or inability to use our Website or obtain products from us even if we are made aware of the possibility of such damages. This limitation on liability includes, but is not limited to, the transmission of any viruses which may infect your equipment, failure of mechanical or electronic equipment or communication lines, telephone or interconnect problems (e.g., you cannot access your Internet Website provider), unauthorized access, theft, operator errors, strikes or other labor problems or any other instances of force majeure. We cannot and do not guarantee continuous, uninterrupted or secure access to our Website.</p>

		<h4>7. Privacy.</h4>
		<p>By accessing and using our Website, you agree to be bound by the terms found in our Privacy Policy, which is accessible via the following link: Privacy Policy</p>

		<h4>8. Intellectual Property.</h4>

		<p>(a) The trademarks, logos, and Website marks ("Marks") including the name "Awl & Sundry", our logo, and other graphics are property of Awl & Sundry and other parties. You are prohibited from using any Marks for any purpose including, but not limited to use as keywords or metatags on other pages or Websites on the World Wide Web without the written permission of Awl & Sundry or such third party which may own the Marks. All information and content located on our Website is protected by copyright and your access to such information on our Website is strictly permitted through the limited non-exclusive license granted under these Terms. You are prohibited from modifying, copying, distributing, transmitting, displaying, publishing, selling, licensing, creating derivative works or using any content available on or through our Website for commercial or public purposes. Unauthorized use of our Website may give rise to a claim for damages and/or may constitute a criminal offense.</p>
		<p>(b) We respect the intellectual property rights of others. If you believe that the content and/or the materials on our Website are infringing upon another's copyright, trademark or other intellectual property, you may send a written notice to us at: nmarwania@awlandsundry.com</p>

		<h4>9. Payment</h4>

		<p>You represent and warrant that that all personal, billing, and payment information provided to us by you is accurate, truthful, and up-to-date. Failure to provide accurate information to us may result in delayed processing of your order or our inability to provide products to you.</p>

		<h4>10. Exchange Policy</h4>

		<p>We work tirelessly to deliver to you quality and highly customized footwear. For that reason, you agree that when you purchase any product from us including but not limited to any footwear, shoe, or any other product, your sole remedy will be an exchange of that purchased product under the terms and conditions of this Section 10.</p>

		<p>(a) If your footwear or other product is for any reason not what you expected it to be (due to the fit, look, feel, size, et cetera) we will allow you to exchange the product, provided that the product is eligible for exchange. A product is eligible for exchange if: (i) you notify us, in writing (email is acceptable), within thirty (30) days of the date of shipment found on the invoice associated with the product you seek to exchange; and (ii) the product is in unworn condition and free of damage, as determined by us in our sole discretion. We reserve the absolute right to refuse to exchange any product which we determine has been worn or otherwise unsatisfactorily damaged.</p>

		<p>(b) If you ship a product to us and we determine that product is not eligible for exchange, we will ship it back to you. However, in that event, you agree to pay and expressly authorize us to charge any credit or debit account you may have on file with us for any shipping or other costs incurred by us in shipping the ineligible product back to you</p>
		<p>(c) If the product you wish to exchange is eligible for exchange, once we receive that product, we will remake a product according specifications selected by you and ship it back to you. You understand that you are responsible for paying the cost of shipping the product to be exchanged to us and that you will bear the risk of loss until we receive that product. However, we will ship the exchanged and remade product back to you free of charge. You agree that due to the highly custom nature of our products, in no event will we refund any payments made by you.</p>

		<h4>11. Severability.</h4>

		<p>If any of the Terms or provisions contained herein are deemed to be invalid or unenforceable, the other terms or remaining provisions shall remain valid and enforceable.</p>

		<h4>12. Dispute Resolution.</h4>

		<p>In the event that any dispute arises with respect to these Terms, upon the election of Awl & Sundry in its sole discretion, such dispute shall be resolved by binding arbitration in Hudson County, New Jersey, and at our option, such arbitration shall be before a single arbitrator selected in our sole and absolute discretion. In the event we elect not to require that a dispute arising hereunder be submitted to binding arbitration, any such dispute shall nevertheless be litigated in the State and Federal courts having jurisdiction over Hudson County, New Jersey. You shall be liable for and shall reimburse us for our expenses and fees, including attorneys' fees, in the event any arbitration or litigation arises out of, under, or relating to these Terms or your use of our Website or services. You by using our Website and/or services, you irrevocably agree and consent to be bound to personal jurisdiction of and venue selection in Hudson County, New Jersey whether either arbitration or litigation arises between Awl & Sundry and you.</p>

		<h4>13. Disclaimer.</h4>

		<p>THIS WEBSITE AND THE MATERIALS, PRODUCTS, AND/OR SERVICES IN THE WEBSITE ARE PROVIDED "AS IS" AND WITHOUT WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, AWL & SUNDRY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, SATISFACTORY QUALITY, QUIET ENJOYMENT, SECURITY, AND ACCURACY. AWL & SUNDRY DOES NOT REPRESENT OR WARRANT THAT THE FUNCTIONS CONTAINED IN THE WEBSITE WILL BE UNINTERRUPTED OR ERROR-FREE, THAT ANY DEFECTS WILL BE CORRECTED, OR THAT THIS WEBSITE OR THE SERVER THAT MAKES THE WEBSITE AVAILABLE IS FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. AWL & SUNDRY DOES NOT MAKE ANY WARRANTIES OR REPRESENTATIONS REGARDING THE INFORMATION, PRODUCTS, OR MATERIALS ON THIS WEBSITE IN TERMS OF THEIR CORRECTNESS, ACCURACY, ADEQUACY, USEFULNESS, TIMELINESS, RELIABILITY OR OTHERWISE.</p>

		<h4>14. Limitation of Liability.</h4>

		<p>Awl & Sundry shall not be liable for any lost profits or any incidental, indirect, special, collateral, consequential, exemplary, or punitive damages, resulting from or arising out of, under, or relating to: your use, misuse, or inability to use our Website; you entering into a transaction for the purchase of goods or services from us; a breach of warranty; alterations of, loss of, or unauthorized access to any information sent or received or not sent or received by you or us; any defamatory, offensive, or illegal use of our Website; any infringement of a third party's rights; any accidental or improper disclosure of Personal Data by us; and any violation by you of these Terms or our Privacy Policy. You expressly agree that in no event shall Awl & Sundry be liable to you or any other person for any amounts, judgments, or damages which exceed the order total price actually paid by you to us for the product(s) and/or during the transaction or purchase from which any claim arises out of or relates to. You agree that without these limitations on our liability we would not be able provide our excellent products to you and that these limitations shall apply even if it would cause your remedies under these terms to fail of their essential purpose.</p>

		<h4>15. Indemnification.</h4>

		<p>You agree to indemnify, defend, and hold harmless Awl & Sundry, its parent companies, subsidiaries, affiliated companies, joint venturers, business partners, licensors, employees, agents, and any third-party information providers (if any) from and against all claims, losses, expenses, damages and costs (including, but not limited to, direct, incidental, consequential, exemplary and indirect damages), and reasonable attorneys' fees, resulting from or arising out of, under, or relating to: your use, misuse, or inability to use our Website; you entering into a transaction for the purchase of goods or services from us; a breach of warranty; alterations of, loss of, or unauthorized access to any information sent or received or not sent or received by you or us; any defamatory, offensive, or illegal use of our Website; any infringement of a third party's rights; any accidental or improper disclosure of Personal Data by us; any activity related to your user account by you or another person accessing your user account or our Website; and any violation by you of these Terms or our Privacy Policy.</p>

		<h4>16. Choice of Law.</h4>

		<p>These Terms as well as our Privacy Policy are governed by the laws of the State of New Jersey and of the United States of America, and without regard to conflicts of law principles.</p>

		<h4>17. Entire Agreement; Modification.</h4>

		<p>These Terms together with our Privacy Policy, and any other document referenced herein, constitute the entire understanding between Awl & Sundry and you with respect to the subject matter hereof. You agree that we may amend, modify, or alter these Terms at any time in our sole discretion.</p>

		<h4>18. Feedback.</h4>
		<p>Please send your comments or questions to: nmarwania@awlandsundry.com.</p>

		<p>While we encourage you to provide feedback, comments and questions, it is possible that we may not be able to respond to all feedback we receive. You are responsible for the messages, materials, and content of all submissions to us and it is your responsibility to ensure any said message is accurate, reliable, original, and does not infringe upon the intellectual property rights of others.</p>
	</div>
</div>
<?php
	require_once('includes/footer.php');
?>