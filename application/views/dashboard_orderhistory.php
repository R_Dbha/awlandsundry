<div class="dasboard-right-content "> <?php foreach ($details as $order) { ?> 

    <div class="dashboard-content-heading-wrapper" style="clear:both;">
        <div class="dashboard-content-heading left">
            <h3 class="ordrnumcolor">Order Number : <?php echo $order['invoice_no'] ?></h3>
        </div>
        <div>
        </div>
    </div>
    <div class="order-history-content">

        <div class="row1">
            <div class="price left">
                <div class="order-price">
                        <?php $orderdate = date('d/m/y', strtotime($order['add_date'])); ?>
                        <h2 class="left ordrnumcolor"><?php echo $orderdate ?></h2>
                        <?php if ($order['status'] == 'order shipped') { ?>
                            <span class="cmpltcolor"> - Completed </span>
                        <?php } else if ($order['status'] == 'Order acknowledged') { ?>
                            <span class="cmpltcolor"> - In the works </span>
                        <?php } else { ?>
                            <span class="cmpltcolor" style="color:red;"> - <?php echo $order['status'] ?></span>
                        <?php } ?>
                    </div>
                    <div class="total-price">
                        <h3 class="left ordrnumcolor">Total : $<?php echo $order['net_amt'] ?></h3>
                    </div>
                </div>
                <div class="shoe-details-wrapper">
                    <?php foreach ($order['items'] as $item) {
                        ?>
                        <div class="shoe-details left">
                            <div class="shoe-img">
                                <!--img src="<?php echo base_url(); ?>assets/css/images/order_img1.png">-->
                                <img style="width: 180px;" src="<?php echo CustomShoeConfig::FILE_BASE . 'designs/' . $item['image_file'] . "_A0.png"; ?>" >
                            </div>
                            <p class="shoe-name"><?php echo $item['last_name'] ?> /<span><?php echo $item['style_name'] ?></span></p>
                            <!--p class="shoe-size">Size <?php echo $item['left_shoe'] . ',' . $item['right_shoe']; ?> </p-->
                            <p class="shoe-size">Size : left - <?php echo $item['left_shoe']; ?>, right - <?php echo $item['right_shoe']; ?></p>
                            <p class="shoe-size">Width : left - D, right - D</p>  
        <!--p class="shoe-price ordrnumcolor">$<?php echo $item['item_amt']; ?></p-->
                        </div>
                    <?php } ?>

                </div>
            </div>
            <div class="row2">
                <?php //if($order['status'] != "order shipped"){ ?>
                <input type="submit" value="TRACK YOUR ORDER"/>
                <?php //} ?>
                <input id="btn_order_view_details" type="submit" value="VIEW DETAILS" onclick="return btn_order_view_details('<?php echo $order['order_id'] ?>');"/>
                <input type="submit" value="EXCHANGE ITEMS" onclick="javascript:showPopUpExchangeShoe();
                        return false;"/>
            </div>
            <div class="overlay none">
                <div id="index-popup-wrapper" class="" style="background:#373737">
                    <div id="index-popup">
                        <a onclick="javascript:return closePopUp();"><span class="close"></span></a>
                        <div class="newletter-content">
                            <h1>EXCHANGE ITEMS</h1>
                        </div>
                        <div class="newletter-content">
                            <p>To exchange your Awl & Sundry pair, please send us an email at <?php echo $this->config->item('contact_email'); ?> with your order no. and reason for exchange.</p>

                        </div>
                        <div>
                            <p Style="margin-top:20px;">
                                <a href="javascript:void(0);" value="Close" onclick="javascript:return closePopUp();" Style="color: #fff;font-size: 20px;">Close</a>
                            </p>
                        </div>
                        <div class="newletter-content">
                            <p class="none show"></p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    <?php } ?>	
</div>