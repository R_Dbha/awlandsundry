<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <?php $this->load->view('common/template/begin'); ?>
    <body class="page giftcard">
        <header id="header">
            <section id="toolbar">
                <div class="toolbarInner clearfix">	
                    <div class="wrapper content clearfix">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Gift Card</span>
                        </div>

                        <div class="right"> 
                            <ul id="headerNavLinks">

                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li> 
                                <?php $this->load->view('common/login_status'); ?>
                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
            <div class="wrapper shopping_cart_pop clearfix">
                <section class="shopping_cart_mini">
                    <h4>ORDER CART</h4>
                    <h2 class="emptyCart">Cart is Empty.</h2>
                </section>
            </div>
            <div class="patterned-rule-blue"  ></div>
        </header>
        <section id="middle_container"  >

            <div class="wrapper clearfix quote">
                <h1 class="align-center">Awl &amp; Sundry Gift Card</h1>

            </div>

            <section class="wrapper clearfix main">

                <section class="content" >
                    <div id="card">
                        <form method="POST" action="<?php echo base_url(); ?>gift-card/cart">
                            <?php echo form_error('amount_option'); ?>
                            <select name="amount_option" tabindex="1" id="choose_amount" class="">
                                <option value="">Choose an amount</option>
                                <?php echo $amount_option; ?>
                            </select>
                            <?php echo form_error('custom_amount'); ?>
                             <input type="text" name="custom_amount" tabindex="2" placeholder="Custom Amount" id="custom_amount" class="" value="<?php echo set_value('custom_amount',''); ?>" />
                            <?php echo form_error('sender_name'); ?>
                             <input type="text" name="sender_name" tabindex="3" placeholder="Sender Name" id="sender_name" class="" value="<?php echo set_value('sender_name',''); ?>" />
                             <?php echo form_error('sender_email'); ?>
                             <input type="text" name="sender_email" tabindex="4" placeholder="Sender Email" id="sender_email" class="email" value="<?php echo set_value('sender_email',''); ?>" />
                            <?php echo form_error('recipient_name'); ?>
                             <input type="text" name="recipient_name" tabindex="5" placeholder="Recipient Name" id="recipient_name" class="" value="<?php echo set_value('recipient_name',''); ?>" />
                            <?php echo form_error('recipient_email'); ?>
                             <input type="text" name="recipient_email" tabindex="6" placeholder="Recipient Email" id="recipient_email" class="" value="<?php echo set_value('recipient_email',''); ?>" />
                             <textarea name="message" placeholder="Message" tabindex="7"><?php echo set_value('message',''); ?></textarea>
                             <input type="submit" name ="submit" value ="Add To Cart" />
                        </form>
                    </div>
                </section>
            </section>	


        </section>            

        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
        <script type="text/javascript" >
//                                jQuery('.giftcard form select').dropkick({
//                                    theme: 'black',
//                                    change: function(value, label) {
//                                      //  $(this).dropkick('theme', value);
//                                    }
//                                });
        </script>

    </body>
</html>        