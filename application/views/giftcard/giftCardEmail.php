<html>
    <head>
        <title>Awl and Sundry Gift Card</title>
    </head>
    <body style = "margin: 0px; font-family: gibsonregular, arial, sans-serif; color: #414141; margin: 0px ;">

        <div id="messsge_wrap" style = "width:580px; margin: 0px auto 40px auto; overflow: hidden;">
            <div  style="width:140px;height:60px;margin:4px 0px;padding: 15px 0px 0px 0px; float: left; ">
                <img width="140" height="60" src="<?php echo base_url() . 'assets/img/logo.png' ?>" >
            </div>
            <div style="font-size: 15px;float: right; width:240px; display: table-cell; text-align: right;vertical-align: bottom;
                 padding: 40px 0px 0px 0px;" >Help Center - <?php echo $this->config->item('customer_care_number'); ?><br></br>
                <span style="font-size: 12px;">or reply to this email</span>
            </div>
            <div class="content" style="width:500px;
                 margin: auto;
                 float: none;
                 clear: both;
                 background: #f1f0ee;
                 border-radius: 5px;
                 -moz-border-radius: 5px;
                 -webkit-border-radius: 5px;
                 padding: 40px;">
                <h2 style=" text-align: center;">E-Gift Card</h2>
                <div class="description" style="width:500px; margin:15px auto 15px;
                     padding:10px 0px;
                     text-align: center;">
                    <?php echo ucwords($sender_name); ?> sent you an Awl &amp; Sundry gift card! Click the card number below to redeem your new store credit.
                </div>
                <div id="giftcard">
                    <div class="card" style="background: white;
                         border-radius: 8px;
                         -moz-border-radius: 8px;
                         -webkit-border-radius: 8px; 
                         width: 350px;
                         height: 200px;
                         display: block;
                         margin: auto;">
                        <div  style="width:140px;height:60px;margin:4px auto;padding: 15px 0px 0px 0px; "><img width="140" height="60" src="<?php echo base_url() . 'assets/img/logo.png' ?>" ></div>
                        <div class="text" style="                margin:auto;
                             width:70px;
                             display: block;font-size: 16px;font-weight: bold;">Gift Card</div>
                        <div class="number" style=" width:60%;
                             float: left;">
                            <div style="padding:30px 0px 0px 30px;">CARD NUMBER</div>
                            <div style="                padding:0px 0px 0px 30px;
                                 font-size: 13px;
                                 font-weight: bold;" ><?php echo $card_number; ?></div>
                        </div>
                        <div class="value" style="width:40%; float: left;">
                            <div  style="padding:30px 0px 0px 30px;">VALUE</div>
                            <div style="padding:0px 0px 0px 30px;
                                 font-size: 18px;
                                 font-weight: bold;">$<?php echo $giftcard_price; ?></div>
                        </div>
                    </div>
                    <div class="description" style="margin:15px auto 15px;
                         padding:10px 0px;
                         text-align: center;">
                        <?php echo $message; ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
