<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <?php $this->load->view('common/template/begin'); ?>
    <body class="page giftcard">
        <header id="header">
            <section id="toolbar">
                <div class="toolbarInner clearfix">	
                    <div class="wrapper content clearfix">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Gift Card</span>
                        </div>

                        <div class="right"> 
                            <ul id="headerNavLinks">

                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li> 
                                <?php $this->load->view('common/login_status'); ?>
                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
            <div class="wrapper shopping_cart_pop clearfix">
                <section class="shopping_cart_mini">
                    <h4>ORDER CART</h4>
                    <h2 class="emptyCart">Cart is Empty.</h2>
                </section>
            </div>
            <div class="patterned-rule-blue"  ></div>
        </header>
        <section id="middle_container"  >

            <div class="wrapper clearfix quote">
                <h1 class="align-center">Awl &amp; Sundry Gift Card</h1>
                
            </div>

            <section class="wrapper clearfix main">
                
                <section class="content">
                    <div id="giftcard">
                        <div class="card">
                            <span class="logo"></span>
                        </div>
                        <h2>E-Gift Card</h2>
                        <div class="description">
                            
                        </div>
                        <a href="<?php echo base_url();?>gift-card/order"><span class="button">Shop now</span></a>
                    </div>
                </section>
            </section>	


        </section>            

        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>

    </body>
</html>        