
<div class="modal-header" >
    
        <h3 class="modal-title" align="center"> <span><a href="#signup_modal" data-rel="popup" >REGISTRATION</a></span></h3>
     <button type="button" class="close-button" style="background-color:#FCFCFC"  data-dismiss="modal" aria-label="Close"><a href="#" data-rel="back"><span aria-hidden="true">&times;</span></a></button>
</div>
<div id="registration-wrapper">
     
    <div id="register" class="container">
    
            <div id="register_form">
                <h3 class="page-title t-left"></h3>
                <div class="reg_form_inner">
                    <p class="row" style="margin-top: 5%;" >
                    <label>First Name</label>
                      <input type="text" name="fname" id="fname" value="<?php echo @$fname ?>" required/>
                            <span style="color:red;" id="fnameerror"><?php echo @$errors['fname'] ?></span>
                    </p>
                    <p class="row" style="margin-top: 5%;">
                        <label>Last Name</label>
                        <input type="text" name="lname" id="lname" value="<?php echo @$lname ?>" required/>
                        <span style="color:red;" id="lnameerror"><?php echo @$errors['lname'] ?></span>
                    </p>
                    <p class="row" style="margin-top: 5%;">
                        <label>Email address</label>
                        <input type="email" name="email" id="s_email" value="<?php echo @$email ?>" required/>
                        <span style="color:red;" id="emailerror"><?php echo @$errors['emailunique'] ?></span><span style="color:red;"><?php echo @$errors['emailvalid'] ?></span>
                    </p>
                    <p class="row" style="margin-top: 5%;">
                        <label>Password</label>
                        <input type="password" name="password" id="s_password"  required/>
                        <span style="color:red;" id="passworderror"><?php echo @$errors['passlength'] ?></span>
                    </p>

                    <div id="agree">
                        <h5 style="display:none;">AWL MEMBERSHIP</h5>
                        <div class="agree">
                            <p>By clicking on register, you agree to Awl &amp; Sundry's  <a href="<?php echo base_url(); ?>terms-and-conditions" style="color:#23A9CD;">Terms of use </a> and <a href="<?php echo base_url(); ?>privacy-policy" style="color:#23A9CD;"> Privacy Policy.</a></p>                       
                        </div>
                        <span style="color:red;" id="error_msg"></span>
                    </div>
                </div>


                <div class="bottomline"></div>
                <div class="left">
                    <a class="back" href="#login_modal" id="back" data-rel="popup" data-dismiss="modal"  data-toggle="modal" data-target="#login_modal">Back</a>
                </div>
                <div class="right">
                    <input type="submit" value="Register" name="register" id="btnRegister"/>
                </div>
            </div>
        </div>
    </div>
