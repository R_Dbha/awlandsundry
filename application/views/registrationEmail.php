<?php
require_once('includes/header.php');
?>

<div id="registration-wrapper">
    <div id="register" class="container">
        <div id="reg_container">
            <h3 class="page-title">
                    <span>ACCOUNT - REGISTER</span>
            </h3>
            <form id="register_form" method="post" action="<?php echo base_url(); ?>signup">
                <h3 class="page-title t-left"></h3>
                <div class="reg_form_inner">
                    <p>
                        <label>First Name</label>
                        <input type="text" name="fname" value="<?php echo @$fname ?>" required/>
                        <span><?php echo @$errors['fname'] ?></span>
                    </p>
                    <p>
                        <label>Last Name</label>
                        <input type="text" name="lname" value="<?php echo @$lname ?>" required/>
                    </p>
                    <p>
                        <label>Email address</label>
                        <input type="email" name="email" value="<?php echo @$email ?>" required/><span style="color:red;"><?php echo @$errors['emailunique'] ?></span><span style="color:red";><?php echo @$errors['emailvalid'] ?></span>

                    </p>
                    <p>
                        <label>Password</label>
                        <input type="password" name="password"  required/><span style="color:red";><?php echo @$errors['passlength'] ?></span>
                    </p>

                    <div id="agree">
                        <h5 style="display:none;">AWL MEMBERSHIP</h5>
                        <div class="agree">
                            <p>By clicking on register, you agree to Awl &amp; Sundry's  <a href="<?php echo base_url(); ?>terms-and-conditions" style="color:#23A9CD;">Terms of use </a> and <a href="<?php echo base_url(); ?>privacy-policy" style="color:#23A9CD;"> Privacy Policy.</a></p>                       
                        </div>
                    </div>
                </div>


                <div class="bottomline"></div>
                <div class="left">
                    <a class="back" href="<?php echo base_url(); ?>login">Back</a>
                </div>
                <div class="right">
                    <input type="submit" value="Register" name="register" id="btnRegister"/>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require_once('includes/footer.php');
?>