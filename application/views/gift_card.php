<?php $this->load->view('includes/header'); ?>

<div id="gift_card_wrapper">
    <div id="gift_card_container">
        <div id="g_card">
            <a class="gift_card_btn">SEND AN E-GIFT CARD</a>
        </div>
        <div id="g_card_form">
            <form method="POST" action="<?php echo base_url(); ?>gift-card" >
                <div id="choose_amount" class="none">
                    <h3 class="page-title"><span>Give the perfect gift to your loved ones!</span></h3>
                    <h2>Choose an amount</h2>
                    <div class="choose_amount">
                        <div class="amount_text"> $50 <input type="hidden" value="50" /></div>
                        <div class="amount_description"></div>
                    </div>
                    <div class="choose_amount">
                        <div class="amount_text"> $100 <input type="hidden" value="100" /></div>
                        <div class="amount_description"></div>
                    </div>
                    <div class="choose_amount">
                        <div class="amount_text"> $350 <input type="hidden" value="350" /></div>
                        <div class="amount_description"></div>
                    </div>
                    <div class="choose_amount">
                        <div class="amount_text"> $495 <input type="hidden" value="495" /></div>
                        <div class="amount_description"></div>
                    </div>

                    <span class="none"><?php echo @$errors['custom_amount']; ?></span>
                    <div id="other_amount">
                        <label for="other_amount">Other amount</label>
                        <div>
                            <input type="text" id="custom_amount" name="custom_amount" value="<?php echo @$custom_amount; ?>">
                            <input type="hidden" id="amount" name="choose_amount" value="<?php echo @$choose_amount; ?>" >
                            <a class="othr_amt">GO</a>
                        </div>
                    </div>
                </div>
                <div id="details_wrap" class="none">
                    <h2>Enter details</h2>
                    <div>
                        <p>
                            <label for="">To</label>
                            <input type="text" name="recipient_name" value="<?php echo @$recipient_name; ?>" placeholder="Recipient's name">
                            <span class="none"><?php echo @$errors['recipient_name']; ?></span>
                        </p>
                        <p>
                            <label for="">From</label>
                            <input type="text" name="sender_name" value="<?php echo @$sender_name; ?>" placeholder="Your name">
                            <span class="none"><?php echo @$errors['sender_name']; ?></span>
                        </p>
                    </div>
                    <div>
                        <p>
                            <label for="">To</label>
                            <input type="text" name="recipient_email" value="<?php echo @$recipient_email; ?>" placeholder="Recipient's email">
                            <span class="none"><?php echo @$errors['recipient_email']; ?></span>
                        </p>
                        <p>
                            <label for="">From</label>
                            <input type="text" name="sender_email" value="<?php echo @$sender_email; ?>" placeholder="Your email">
                            <span class="none"><?php echo @$errors['sender_email']; ?></span>
                        </p>
                    </div>
                    <div>
                        <p>
                            <label for="">Delivery Date</label>
                            <input type="text" name="send_mail_on" value="<?php echo @$send_mail_on; ?>" placeholder="Date" id="datetime" readonly="">
                            <span class="none"><?php echo @$errors['send_mail_on']; ?></span>
                        </p>
                        <p>
                            <!--                            <label for="">Time Zone</label>
                                                        <select name="time_zone" style="width:100%;">
                            <?php echo @$time_zone; ?>
                                                        </select>
                                                        <span class="none"><?php echo @$errors['time_zone']; ?></span>-->
                        </p>
                    </div>
                    <p>
                        <label for="">Note(optional)</label>
                        <textarea name="message"><?php echo @$message; ?></textarea>
                    </p>
                    <input type="submit" value="Add to cart">
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer.php'); ?>