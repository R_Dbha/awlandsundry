<?php require_once( 'includes/header.php'); ?>

<link href="assets/css/style.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css">

<div id="shop-wrapper">
    <a class="readywear_banner" href="#shop"><img class="img-responsive" src="<?php echo base_url(); ?>/assets/css/images/blacklist-collection.jpg"></a>
    <div id="shop" class="container">
        <div class="section-container ready-wear-collection-container">
            <div class="spinner" id="loader"></div>
            <?php if (isset($collections) && sizeof($collections)) { ?>
                <?php foreach ($collections as $key => $item) { ?>
                    <?php
                    $item_color = preg_replace('/[\s-]+/', '-', $item['color']);
                    $color1 = preg_replace('/[\s-]+/', '-', $item['similar_shoes'][0]['color']);
                    $color2 = preg_replace('/[\s-]+/', '-', $item['similar_shoes'][1]['color']);
                    $shoe_name = ($key % 2 == 0) ? "The " . ucwords($item['style_name']) : "";
                    ?>
                    <?php if ($shoe_name !== "") { ?>
                        <?php /* <div class="ready-wear-container-title"><?php echo $shoe_name; ?></div> */?>
                        <ul class="ready-wear-container">
                        <?php } ?>
                        <li>
                            <div class="product-block">
                                <div class="product-box">
                                    <input type="hidden" id="ready_wear_id" value="<?php echo $item['id']; ?>">
                                    <div class="product-img-container">
                                        <a href="<?php echo base_url() . 'shop/products/' . $item_color . '/' . $item['shoe_name']; ?>" class="product-image-link">
                                            <img src="<?php echo $item['image'][0]; ?>" alt="Product Image" class="img-responsive center-block product-img" id="pro-image">
                                            <img src="<?php echo $item['image'][1]; ?>" alt="Product Image" class="img-responsive center-block product-img-hover">
                                        </a>
                                        <div class="quick-add">
                                            <a href="javascript:;">+ Quick Add</a>
                                            <div class="size-group-list">
                                                <div class="size-group">
                                                    <ul>
                                                        <li><a class="size-group-li" href="javascript:;">5</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">5.5</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">6</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">6.5</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">7</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">7.5</a></li>
                                                    </ul>
                                                    <ul>
                                                        <li><a class="size-group-li" href="javascript:;">8</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">8.5</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">9</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">9.5</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">10</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">10.5</a></li>
                                                    </ul>
                                                    <ul>
                                                        <li><a class="size-group-li" href="javascript:;">11</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">11.5</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">12</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">12.5</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">13</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">13.5</a></li>
                                                        <li><a class="size-group-li" href="javascript:;">14</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-details-container">                                       
                                        <a href="<?php echo base_url() . 'shop/products/' . $item_color . '/' . $item['shoe_name']; ?>" class="product-link">
                                            <h3 class="product-name"><?php echo $item['shoe_name']; ?></h3>
                                            <div class="product-price">$375</div>
                                            <?php /*<p class="product-color"><?php echo ucwords($item['color']); ?></p> */?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php if ($shoe_name == "") { ?>
                        </ul>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>
<?php require_once( 'includes/footer.php'); ?>