<?php
/**
 * @author Ajith Vp <ajith@kraftlabs.com>
 * @copyright Copyright (c) 2014, Ajith Vp <ajith@kraftlabs.com>
 * @date 11-Dec-2014
 */
?>
<div id="track-info">
    <span class="close"></span>
    <h1>Tracking Info</h1>
    <h3 class="slug"> <?php echo $slug; ?></h3>
    <h3 class="status"> <?php echo $tracking_status; ?></h3>
    <?php foreach ($points as $key => $point) {
        ?>
        <div class="row">
            <div class="time" >
                <span><?php echo date('M d, Y',  strtotime($point->checkpoint_time)); ?></span>
                <span><?php echo date('h:ia',  strtotime($point->checkpoint_time)); ?></span>
            </div>
            <div class="point-status">
                <span class="tag <?php echo $point->tag; ?>"></span>
            </div>
            <div class="data">
                <span><?php echo $point->message; ?></span>
                <span><?php echo $point->slug; ?></span>
                <span><?php echo $point->country_name; ?></span>
            </div>
        </div>
        <?php }
    ?>

</div>