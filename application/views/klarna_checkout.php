<?php $this->load->view('includes/klarna_header'); ?>

<div id='klarna-confirmation-wrapper'>
    <div id="klarna-confirmation" class="container">
        <div><?php echo $checkout['html_snippet'] ?></div>
    </div>
</div>

<?php $this->load->view('includes/klarna_footer'); ?>
