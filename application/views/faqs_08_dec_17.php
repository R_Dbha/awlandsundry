<?php
$this->load->view('includes/header');
?>
<div id="faq-wrapper">
    <div id="faq-contact" class="container">
        <h3 class="page-title">
            <span>CONTACT US</span>
        </h3>
        <div id="contact-description">
            <!-- <p class="center">Get in touch with us</p> -->
        </div>
        <div id="contact-wrapper">
            <div class="contact-info address">
                <h4>OFFICE ADDRESS</h4>
                 <p>18 W 23rd Street, 4th Floor,</p>
                 <p>New York, NY 10010 </p>
            </div>
            <div class="contact-info">
                <h4>PHONE</h4>
                <p><?php echo $this->config->item('contact_phone'); ?></p>
            </div>
            <div class="contact-info">
                <h4>EMAIL</h4>
                <p><a href='mailto:<?php echo $this->config->item('contact_email'); ?>'><?php echo $this->config->item('contact_email'); ?></a></p>
            </div>
        </div>
    </div>
    <div id="faq-menu-wrapper">
        <div id="menu" class="container faq">
            <ul id="menuitem">
                <li ><a href="#exchange">EXCHANGE</a></li>
                <li>|</li>
                <li ><a href="#product">PRODUCT</a></li>
                <li>|</li>
                <li ><a href="#shipping">SHIPPING</a></li>
                <li>|</li>
                <li ><a href="#sizing">SIZING</a></li>

            </ul>
        </div>
    </div>
    <div id="faq" class="center">
        
        <div id="exchange" class="container">
            <h3 class="bgline section-title">
                <div class="line"></div>
                <span>EXCHANGE</span>
            </h3>
            <h4><span class="question">What is the exchange policy &#63;</span></h4>
            <p class="none answer">You can exchange your Awl & Sundry shoes within 30 days of receipt if they don't fit right or if they are faulty. Your shoes are considered faulty if they are different from the design you initially ordered. This does not apply to colors and leather texture since your actual pair of shoes may look slightly different from the colors you see on the screen. 
                <br>
                <br>
                When exchanging, you will be responsible for one way return postage to our NYC office. Please ensure the shoes are in an unworn condition. We recommend trying them on carpeted surface. 
                <br>
                <br>
                Please email <a href="mailto:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a> to exchange your shoe. 
            </p>

        </div>

		<div id="product" class="container">
            <h3 class="bgline section-title">
                <div class="line"></div>
                <span>PRODUCT</span>
            </h3>
            <h4><span class="question">How do you make your shoes &#63;</span></h4>
            <p class="none answer">All of our shoes are meticulously handcrafted by seasoned craftsmen. Each shoe goes through more than 200 different steps from clicking, sewing, hand skiving, hand drafting, toe lasting and many more.</p>

            <h4><span class="question">What is the difference between Oxford & Derby &#63;</span></h4>
            <p class="none answer">The Oxford: The Oxford, also known as Balmorals, is to be recognized by its closed lacing. The laces are threaded through five pairs of eyelets and fasten up the shoe so perfectly that only the upper edge of the tongue can be seen. This lends itself to a sleeker, more elegant appearance that takes the look of your shoe up by a few notches and is the perfect finishing touch to a sharp tailored suit. This type of shoe looks particularly good on narrow feet with a low instep. 
                <br>
                <br>
                The Derby: The Derby is characterized by open lacing. It also known as "Bluchers" after the Prussian field marshal Gebhard Leberecht von Blücher, Duke of Wahlstady (1742 - 1819), who joined forces with Wellington to defeat Napoleon at the Battle of Waterloo in 1815 and ordered laced shoes of this type to be made for his soldiers. The Derby offers pure comfort to shoe-lovers who have a wide foot or an unusually high instep. The open lacing mean that it is easier to slip into this shoe than an Oxford, and the distance between the two quarters is easier to adjust. 
            </p>

            <h4><span class="question">What is a shoe last &#63;</span></h4>
            <p class="none answer">A shoe last is a wooden mold that gives the shoe its shape.</p>

            <h4><span class="question">Where are your shoes made &#63;</span></h4>
            <p class="none answer">All our shoes are handcrafted at our own shop in China.  We have a small team of artisans that hand-make one pair a time. </p>

            <h4><span class="question">What kind of leather do you use &#63;</span></h4>
            <p class="none answer">We use full grain leather for all our uppers; it maintains its shine longer and is naturally porous allowing your pair to breathe and providing a well-balanced temperature within.</p>

            <h4><span class="question">Can I try on a shoe before making a purchase &#63;</span></h4>
            <p class="none answer">You can try an Awl & Sundry pair in person if you are based in NYC by making an appointment with us. </p>

        </div>

        <div id="shipping" class="container">
            <h3 class="bgline section-title">
                <div class="line"></div>
                <span>SHIPPING</span>
            </h3>
            <h4><span class="question">Where do you ship &#63;</span></h4>
            <p class="none answer">Currently we ship our shoes to contiguous US, UK, Canada and Australia. </p>

            <h4><span class="question">How long will it take for my shoes to arrive &#63;</span></h4>
            <p class="none answer">Since the shoes are handmade it takes 4-6 weeks for your customized Awl & Sundry pair to arrive.  Please ensure you or someone else is physically present to sign and notify that you have received your shoes or they will be shipped back.</p>

            <h4><span class="question">Do you provide expedited shipping &#63;</span></h4>
            <p class="none answer">No, currently we do not provide expedited shipping.</p>
        </div>

        <div id="sizing" class="container">
            <h3 class="bgline section-title">
                <div class="line"></div>
                <span>SIZING</span>
            </h3>
            <h4><span class="question">How do I know my size&#63;</span></h4>
            <p class="none answer">All Awl & Sundry shoes are true-to-fit. To get the right fit, please order the most frequently worn size. For instance, if you wear a size 10 for most of your shoes, we would recommend ordering a size 10 with Awl & Sundry as well. If you are unsure of your size, you can trace your foot on a blank piece of paper and send it to us at <?php echo $this->config->item('contact_email'); ?> and we will recommend a size and last for you. 
            </p>

            <h4><span class="question">Do you offer wider widths &#63;</span></h4>
            <p class="none answer">Yes, we do offer a variety of widths-D, E, EE and EEE. All Awl & Sundry shoes are defaulted to width D. If you have a wider foot, we would strongly recommend tracing your foot on a blank piece of paper and sending it to us at <?php echo $this->config->item('contact_email'); ?> and we will recommend you your width and last.</p>

            <h4><span class="question">What sizes do you offer &#63;</span></h4>
            <p class="none answer">We offer U.S. sizes 5 to 14. Outside of these sizes, please email us at <?php echo $this->config->item('contact_email'); ?>.</p>
        </div>



        

        <!--<div id="product" class="container">

        </div>-->
    </div>
</div>
<?php
$this->load->view('includes/footer.php');
?>