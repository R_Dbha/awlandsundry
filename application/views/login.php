<?php require_once('includes/header.php'); ?>

<div id="login-wrapper">
    <div id="login" class="container">
        <h4>
            <?php if ($this->session->userdata('success') != FALSE) { ?>
                <div style="text-align: center;background: #ccc;color: #fff;margin-top: 10px;padding: 10px 10px;"><h4><?php echo $this->session->userdata('success'); ?></h4></div>
            <?php } ?>
            <?php $this->session->unset_userdata('register'); ?>
        </h4>
        <h3 class="page-title">
            <span>LOGIN OR CREATE AN ACCOUNT</span>
        </h3>
        <div id="signin-form">
            <div class="signin-form-inner left">
                <h3 class="page-title">REGISTERED CUSTOMER</h3>
                <p class="signin_content">
                    If you have an account with us, login using your email address.
                </p>

                <form id="login_form" method="post" action="<?php echo base_url(); ?>login">
                    <p>
                        <label>*EMAIL ADDRESS</label>
                        <input type="email" id="email" name="email" value="<?php echo @$email ?>" required/>
                    </p>
                    <p>
                        <label>*PASSWORD</label>
                        <input type="password" name="password" required/>
                    </p>
                    <span style="color:red;"><?php echo @$errors['msg'] ?></span>
                    <p>
                        <a href="<?php echo base_url(); ?>forgot-password">FORGOT YOUR PASSWORD?</a>
                    </p>
                    <p>
                        <input type="submit" name="login" value="LOGIN" />
                    </p>
                    <p>
                        <label>* Required fields</label>
                    </p>
                </form>
            </div>
            <div id="line"></div>
            <div class="signin-form-inner right">
                <h3 class="page-title">NEW CUSTOMERS</h3>
                <p class="signin_content">
                    By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses , view and track your orders in your account and more.
                </p>
                <p>
                    <a href="<?php echo base_url(); ?>signup"><input type="submit" name="register" value="REGISTER" /></a>
                </p>
                <div  id="social_login">
                    <a href="<?php echo base_url(); ?>facebook/signup" id="fb_login" class="fbsignup"><img src="<?php echo base_url(); ?>assets/css/images/Facebook_Login.png"></a>
                    <a href="<?php echo base_url(); ?>googlelogin/signup" ><img src="<?php echo base_url(); ?>assets/css/images/Google_Login.png"></a>
                    <!--<a href="<?php echo base_url(); ?>linkedin/signup" id="linkedin_login"><img src="<?php echo base_url(); ?>assets/css/images/linkedin_login.png"></a>-->
                    <!--<a href="<?php echo @$twitter_login_link; ?>" id="twitter_login"><img src="<?php echo base_url(); ?>assets/css/images/twitter_login.png" style="height: 45px; width: 247px;"></a>-->
                </div>
            </div>
        </div>                        
    </div>
</div>

<?php require_once('includes/footer.php'); ?>