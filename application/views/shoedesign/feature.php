<?php $this->load->view('includes/header'); ?>
<style type="text/css">
    .current-item {font-weight: bold;}
    a.current-step { color: #DA090A !important; text-decoration: underline; }
    .step3_small > img { top: 0px !important; }
    .step3 .steps-main-title .sub-title { margin-bottom: 12px ; }
</style>
<div id="step3-menu-wrapper">
    <?php $this->load->view('shoedesign/shoe_menu'); ?>
    <!--    <div id="menu" class="container step3">
            <ul id="menuitem">
                <li ><a href="javascript:;">Feet First</a></li>            
                <li ><a href="javascript:;">Essential Style</a></li>            
                <li class="active-li"><a class="current-step" href="javascript:;">Design Features</a></li>            
                <li class="bg-none"><a href="javascript:;">Materials / Colors</a></li>            
                <li class="bg-none"><a href="javascript:;">Sole Attributes</a></li>            
                <li class="bg-none"><a href="javascript:;">Personal Touches</a></li>
            </ul>
        </div>-->
</div>
<div id="step3_shoes" class="container">
    <div class="steps-main-title">
        <h2>Design Features</h2>
        <p class="sub-title">PATTERNS LEND YOUR SHOE A DISTINCTIVE PERSONALITY.<span> SELECT ONE:</span></p>
    </div>
    <div class="spinner" id="loader"></div>
    <div id="step3_container" class="right" style="display:none;">
        <div id="step3_main_slider">
            <?php foreach ($properties as $type => $feature) { ?>
                <ul class="step_3_slider<?php echo $type != 'Toe' ? ' none' : ''; ?>" id="<?php echo 'All'; ?>_properties">
                    <?php foreach ($feature as $property) { ?>
                        <li class="property" data-t="<?php echo $property['Toe']['code']; ?>" data-e="<?php echo $property['Eyestay']['code']; ?>" data-f="<?php echo $property['Foxing']['code']; ?>" data-v="<?php echo $property['Vamp']['code']; ?>">
                            <a class="step3_small" data-type="All">
                                <input type="hidden" class="property-toe" value="<?php echo htmlentities(json_encode($property['Toe'])); ?>" />
                                <input type="hidden" class="property-eyestay" value="<?php echo htmlentities(json_encode($property['Eyestay'])); ?>" />
                                <input type="hidden" class="property-foxing" value="<?php echo htmlentities(json_encode($property['Foxing'])); ?>" />
                                <input type="hidden" class="property-vamp" value="<?php echo htmlentities(json_encode($property['Vamp'])); ?>" />
                                <img src="<?php echo ApplicationConfig::IMG_BASE . $styledetails['last_folder'] . '/' . $styledetails['style_folder'] . CustomShoeConfig::PROPERTY_FOLDER . 'All/' . $property['displayImage'] . '_A0.jpg'; ?>" alt="<?php echo $property['Toe']['name']; ?>" />
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>

        <div id="step3_design">
            <div id="design-visual">
                <div class="visualImgWrap">
                    <img src="" id="quarter" />
                    <img id="toe" src="" />
                    <img id="vamp" src="" />
                    <img id="eyestay" src="" />
                    <img id="foxing" src="" />
                    <img src="" id="stitch" />
                    <img src="" id="lace" />
                </div>
            </div>
        </div> 
        <div id="tab_wrap" style="z-index: 99 !important">
            <div id="rotate-side" class="left rotate-side tab">SIDE VIEW</div>
            <div id="rotate-top" class="right rotate-top center tab">TOP VIEW</div>
        </div>
        <div id="style-last-name">
            <h3><?php echo $styledetails['style_name'] . ' / ' . $styledetails['last_name'] . ' / <font style="color: goldenrod">CUSTOM SIZED</font>'; ?></h3>
        </div>
        <div id="shoe_design_button">
            <p id="custom-price"><span class="price">$0</span>&nbsp;w/Free shipping</p>
        </div>
        <div class="proceed-next" style="margin-top: 5px !important;">
            <a href="javascript:void(0);" id="back-btn" title="BACK" class="next-step proceed-btn"><span class="arrow-next arrow-back">&nbsp;</span>BACK</a>
            <!--<a href="javascript:void(0);" title="Proceed to Next" class="next-btn arrow-next-a"><span class="arrow-next">&nbsp;</span></a>-->
            <a href="javascript:void(0);" id="next-btn" title="PROCEED TO COLORS / MATERIALS " class="next-btn proceed-btn">PROCEED TO COLORS / MATERIALS </a>
        </div>
    </div>
</div>
<div class="overlay1" id="overlay1"></div>
<div class="modal fade" id="login_modal"  data-role="popup" tabindex="-1" role="dialog" aria-labelledby="login_modal" aria-hidden="true" dismissible="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content" >
            <?php $this->load->view('login_modal'); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="signup_modal" data-role="popup" tabindex="-1" role="dialog" aria-labelledby="signup_modal" aria-hidden="true" dismissible="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <?php $this->load->view('signup_modal'); ?>
        </div>
    </div>
</div>

<script>
    var imageBase = '<?php echo CustomShoeConfig::IMG_BASE; ?>';
    var svgPath = "<?php echo CustomShoeConfig::FILE_BASE . 'SVG/'; ?>";
    var designBase = "<?php echo CustomShoeConfig::FILE_BASE . 'designs/'; ?>";
    var thumbBase = "<?php echo CustomShoeConfig::FILE_BASE . 'thump/'; ?>";
</script>

<?php $this->load->view('includes/footer'); ?>