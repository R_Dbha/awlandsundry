<?php $this->load->view('includes/header'); ?>
<?php $bottmHead = $materials['quarter'][0]['material_name']; ?>
<style type="text/css">
    .current-item {font-weight: bold;}
    a.current-step { color: #DA090A !important; text-decoration: underline; }
</style>
<div id="step4-menu-wrapper">
    <?php $this->load->view('shoedesign/shoe_menu'); ?>
    <!--    <div id="menu" class="container step4">
            <ul id="menuitem">
                <li ><a href="javascript:;">Feet First</a></li>            
                <li ><a href="javascript:;">Essential Style</a></li>            
                <li ><a href="javascript:;">Design Features</a></li>            
                <li class="active-li"><a class="current-step" href="javascript:;">Materials / Colors</a></li>            
                <li class="bg-none"><a href="javascript:;">Sole Attributes</a></li>            
                <li class="bg-none"><a href="javascript:;">Personal Touches</a></li>
            </ul>
        </div>-->
</div>
<div class="spinner" id="loader"></div>
<div id="step4_shoes" class="container" style="display:none;">
    <div class="steps-main-title">
        <h2>Materials & Colors</h2>
        <p class="sub-title">Go Classic, coordinated or stand out in a crowd.<span> CLICK ON A PART OF THE SHOE TO CHOOSE MATERIAL AND COLOR:</span></p>
    </div>
    <div id="step4_container" class="right">
        <div class="step4_main_slider">
            <?php $i = 0; ?>
            <?php $features = array(); ?>
            <?php foreach ($materials as $type => $lethers) { ?>
                <?php $features[] = $type; ?>
                <div id="<?php echo $type; ?>_lethers" class="lether-container <?php echo $i == 0 ? '' : 'none'; ?>">
                    <div class="lether-types">
                        <ul>
                            <?php $j = 0; ?>
                            <?php foreach ($lethers as $lether) { ?>
                                <li>
                                    <input type="hidden" class="mat-code" value="<?php echo $lether['material_code']; ?>">
                                    <input type="hidden" class="feature-type" value="<?php echo $type; ?>">
                                    <a class="mat-name <?php echo (isset($lether['isNew'])) ? ' patina' : ''; ?>" href="javascript:void(0)" <?php echo (isset($lether['isNew'])) ? 'style="color: #14d1ff;font-weight: bold"' : ''; ?>><?php echo $lether['material_name'] . ((isset($lether['isNew'])) ? ' (NEW!)' : ''); ?></a>
                                    <span><?php echo $j < (sizeof($lethers) - 1) ? '|' : ''; ?></span>
                                </li>
                                <?php $j++; ?>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="lether-colors">
                        <?php $k = 0; ?>
                        <?php foreach ($lethers as $lether) { ?>
                            <div id="<?php echo $type . '_' . $lether['material_code'] . '_colors'; ?>" class="lether-color <?php echo $k == 0 ? 'active-leather' : 'none'; ?>" data-feature="<?php echo $type; ?>">
                                <input type="hidden" class="mat-code" value="<?php echo $lether['material_code']; ?>" />
                                <input type="hidden" class="mat-id" value="<?php echo $lether['material_id']; ?>">
                                <input type="hidden" class="mat-folder" value="<?php echo $lether['material_folder']; ?>">
                                <input type="hidden" class="let-mat-name" value="<?php echo $lether['material_name']; ?>">
                                <ul class="color-slider step_4_slider">
                                    <?php foreach ($lether['colors'] as $color) { ?>
                                        <li>
                                            <input type="hidden" class="clr-id" value="<?php echo $color['clrId']; ?>" />
                                            <input type="hidden" class="clr-code" value="<?php echo $color['clrCode']; ?>" />
                                            <input type="hidden" class="clr_name" value="<?php echo $color['clrName']; ?>" />
                                            <a class="step4_small color-selector <?php echo (isset($lether['isNew'])) ? ' show-patina' : ''; ?>" href="javascript:void(0)">
                                                <img src="<?php echo $color['url']; ?>" />
                                                <span class="round"></span>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <?php $k++; ?>
                        <?php } ?>
                    </div>
                </div>
                <?php $i++; ?>
            <?php } ?>
        </div>
        <div id="material_hover_elament" class="none">
            <div id="material_name_wrapper">
                <div class="left material_color_name">
                    <span>DARK BROWN</span>
                </div>
                <div class="right material_name">
                    <span>CALF PLAIN</span>
                </div>
            </div>
        </div>
        <div class="step4_bnt_group part-buttons" style="display: none !important; ">
            <?php $x = 0; ?>
            <?php foreach ($features as $type) { ?>
                <a href="javascript:void(0)" class="<?php echo $x == 0 ? 'active' : ''; ?>" data-feature="<?php echo $type; ?>" data-target="<?php echo $type; ?>"><?php echo strtoupper($type); ?></a>
                <?php $x++; ?>
            <?php } ?>
        </div>

        <div id="step4_design">
            <div id="design-visual">
                <div id="tooltipTmp" style="position: absolute; z-index: 5; padding: 4px 8px; border-radius: 2px; background: rgb(255, 255, 255); left: 35%; top: 20%;text-align: center;">Click on part of the shoe to <br>select Material and color</div>
                <div class="visualImgWrap">
                    <img src="" id="quarter" />
                    <img src="" id="stitch" />
                    <img src="" id="lace" />
                    <img id="toe" src="" />
                    <img id="vamp" src="" />
                    <img id="eyestay" src="" />
                    <img id="foxing" src="" />
                    <div id="style-svg" style="z-index: 0;"></div>
                </div>
            </div>
            <div id="html"></div>
            <div class="patina-new">
                <div class="patina-block">
                    <a class="patina-a patina-trigger" href="javascript:;">
                        <img src="<?php echo base_url(); ?>assets/img/patina-new-thumb.png" alt="Patina">
                    </a>
                    <p class="patina-text">
                        <img src="<?php echo base_url(); ?>assets/img/svg/advance_arrow.svg" alt="advance_arrow">
                        <a href="javascript:;" class="patina-trigger">
                            <span>Check out</span> </br>
                            <span>OUR NEW</span> </br>
                            <span>PATINA</span> </br>
                            <span>OPTIONS!</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <div id="tab_wrap" style="z-index: 99 !important">
            <div id="rotate-side" class="left rotate-side tab">SIDE VIEW</div>
            <div id="rotate-top" class="right rotate-top center tab">TOP VIEW</div>
        </div>
        <div class="step4_main_slider patinaDispImage" style="display: none;">
            <div style="text-align: center">
                <img width="600" height="340" src="" id="pat_img_disp" />
                <div class="patina-desc">
                    <h3>WHY DOESN’T THIS LOOK LIKE MY DESIGN?</h3>
                    <p>
                        Our hand-patina process
                        yields unique variations
                        in color and tonality along
                        pattern boundaries that 
                        cannot be simulated by 
                        our current technology. 
                        Rest assured that your 
                        actual shoe will be crafted
                        exactly to your design and then given the same rich
                        color patina treatment as 
                        the sample shown here.
                    </p>
                </div>
            </div>
        </div>
        <div class="step4_main_slider patinaSlider" style="display: none;">
            <?php foreach ($materials['quarter'] as $quart) { ?>
                <?php if (isset($quart['isNew'])) { ?>
                    <?php foreach ($quart['colors'] as $clr) { ?>
                        <div class="patina-container patina-container-<?php echo $clr['clrCode']; ?>">
                            <ul class="patina-slider">
                                <?php foreach ($clr['images'] as $imgs) { ?>
                                    <li>
                                        <a class="view_patina" href="javascript:void(0)" data-image="<?php echo base_url() . $imgs; ?>">
                                            <img src="<?php echo base_url() . str_replace('.jpg', '_thum.jpg', $imgs); ?>" />
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>

        <div id="style-last-name">
            <h3><?php echo $styledetails['style_name'] . ' / ' . $styledetails['last_name'] . ' / <font class="bottomHeading">' . $bottmHead . '</font>' . ' / <font style="color: goldenrod">CUSTOM SIZED</font>'; ?></h3>
        </div>
        <div id="shoe_design_button" style="top: -20px !important; ">
            <p id="custom-price"><span class="price">$0</span> w/Free shipping</p>
        </div>
        <div class="proceed-next" style="margin-top: -10px !important;">
            <!--<a href="javascript:void(0);" title="Proceed to Next" class="next-btn arrow-next-a"><span class="arrow-next">&nbsp;</span></a>-->
            <a href="javascript:void(0);" id="back-btn" title="BACK" class="next-step proceed-btn"><span class="arrow-next arrow-back">&nbsp;</span>BACK</a>
            <a href="javascript:void(0);" id="next-btn" title="PROCEED TO SOLE ATTRIBUTE " class="next-btn proceed-btn">PROCEED TO SOLE ATTRIBUTE </a>
        </div>
    </div>
</div>
<canvas style="display:none;" height="405" width="720" id="pic-canvas"></canvas>
<div class="overlay1" id="overlay1"></div>
<div class="modal fade" style="display:none;" id="login_modal" data-role="popup"  tabindex="-1" role="dialog" aria-labelledby="login_modal" aria-hidden="true" dismissible="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <?php $this->load->view('login_modal'); ?>
        </div>
    </div>
</div>

<div class="modal fade" style="display:none;" id="signup_modal" data-rol="popup" tabindex="-1" role="dialog" aria-labelledby="signup_modal" aria-hidden="true" dismissible="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <?php $this->load->view('signup_modal'); ?>
        </div>
    </div>
</div>

<script>
    var imageBase = '<?php echo CustomShoeConfig::IMG_BASE; ?>';
    var svgPath = "<?php echo CustomShoeConfig::FILE_BASE . 'SVG/'; ?>";
    var designBase = "<?php echo CustomShoeConfig::FILE_BASE . 'designs/'; ?>";
    var thumbBase = "<?php echo CustomShoeConfig::FILE_BASE . 'thump/'; ?>";
</script>

<?php $this->load->view('includes/footer'); ?>