<?php $this->load->view('includes/header'); ?>
<style scoped>
    .step1_main_slider > li:first-child.bx-clone { display:none; }
</style>
<style type="text/css">
    a.current-step { color: #DA090A !important; text-decoration: underline; }
    div#main_shoe_common { margin:0px auto !important;}
    #main_shoe_common p { width: 73% !important;}
    .bx-viewport { height: 312px;}
</style>
<div id="step1-menu-wrapper">
    <?php $this->load->view('shoedesign/shoe_menu'); ?>
    <!--    <div id="menu" class="container step1">
            <ul id="menuitem">
                <li ><a href="javascript:;">Feet First</a></li>            
                <li class="active-li"><a class="current-step" href="javascript:;">Essential Style</a></li>            
                <li class="bg-none"><a href="javascript:;">Design Features</a></li>            
                <li class="bg-none"><a href="javascript:;">Materials / Colors</a></li>            
                <li class="bg-none"><a href="javascript:;">Sole Attributes</a></li>            
                <li class="bg-none"><a href="javascript:;">Personal Touches</a></li>
            </ul>
        </div>-->
</div>
<div class="spinner" id="loader"></div>
<div id="step1_shoes" class="container" style="display: none;">
    <div class="steps-main-title">
        <h2>Essential Style</h2>
        <p class="sub-title">THE FOUNDATION OF ANY GREAT SHOE IS its fundamental FORM. <span>SELECT YOURS:</span></p>
    </div>
    <ul class="step1_shoes">
        <?php foreach ($styles as $idx => $style) { ?>
            <li>
                <input type="hidden" class="last-style-id" value="<?php echo $style['last_style_id']; ?>" />
                <input type="hidden" class="def_code" value="<?php echo $style['def_code']; ?>" />
                <input type="hidden" class="folder_name" value="<?php echo $style['folder_name']; ?>" />
                <input type="hidden" class="is_vamp_enabled" value="<?php echo $style['is_vamp_enabled']; ?>" />
                <input type="hidden" class="is_lace_enabled" value="<?php echo $style['is_lace_enabled']; ?>" />
                <a class="<?php echo ($idx === 0) ? 'current-shoe' : ''; ?>" id="<?php echo $style['style_name']; ?>" data-index="<?php echo $idx; ?>" data-last="<?php echo str_replace(" ", "-", $style['last_name']); ?>">
                    <img src="<?php echo CustomShoeConfig::IMG_BASE . 'thumb/' . $style['img_file'] . '_A0.png'; ?>" alt="<?php echo $style['style_name']; ?>" >
                </a>
            </li>
        <?php } ?>
    </ul>
</div>
<div id="main_shoe_common" class="container step6" style="display: none;">
    <div class="custom_pager"></div>
    <ul class="step1_main_slider">
        <?php foreach ($styles as $style) { ?>
            <li>
                <img class="main-shoe" src="<?php echo CustomShoeConfig::IMG_BASE . $style['location'] . '_A0.png'; ?>" alt="<?php echo $style['style_name']; ?>">
                <h3 class="main-shoe"><?php echo $style['style_name'] . ' / <font style="color: goldenrod">CUSTOM SIZED</font>'; ?></h3>
                <p><?php echo $style['style_description']; ?></p>
            </li>
        <?php } ?>
    </ul>

    <div class="proceed-next" style="margin-top: 5px !important;">
        <a href="javascript:void(0);" id="back-btn" title="BACK" class="next-step proceed-btn"><span class="arrow-next arrow-back">&nbsp;</span>BACK</a>
        <a href="javascript:void(0);" id="next-step" title="PROCEED TO FEATURES" class="next-step proceed-btn">PROCEED TO FEATURES</a>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>