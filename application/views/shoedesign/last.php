<?php
$this->load->view('includes/header');
?>
<div id="progress_wrap">
    <ul class="progress">
        <li class="active" title="Style">&#10004;</li>
        <li class="active" title="Last">2</li>
        <li title="Design Features">3</li>
        <li title="Material & Colors">4</li>
        <li title="Details">5</li>
        <li title="Sizing">6</li>
    </ul>
</div>
<div id="step2-menu-wrapper">
    <div id="menu" class="container step1">
        <ul id="menuitem">
            <li ><a href="#">STYLE</a></li>
            <li>|</li>
<!--            <li ><a class="current-step" href="#">LAST</a></li>
            <li>|</li>-->
            <li ><a href="#">DESIGN FEATURES</a></li>
            <li>|</li>
            <li ><a href="#">MATERIAL & COLORS</a></li>
            <li>|</li>
            <li ><a href="#">DETAILS</a></li>
            <li>|</li>
            <li ><a href="#">SIZING</a></li>
        </ul>
    </div>
</div>
<div class="spinner" id="loader"></div>
<div id="step2_shoes" class="container" style="display: none;">
   
    <h3>Select your shoe <span>shape (last).</span></h3>
    <ul class="step2_shoes">
        <?php
        foreach ($lasts as $idx => $last) {
            $styleClass = "";
            if ($idx === 0) {
                $styleClass = "current-shoe";
            }
            ?>
            <li>
                <input type="hidden" class="last-style-id" value="<?php echo $last['last_style_id']; ?>" />
                <a class="<?php echo $styleClass; ?>" id="<?php echo str_replace(" ", "-", $last['last_name']); ?>" data-index="<?php echo $idx; ?>">
                    <img src="<?php echo CustomShoeConfig::IMG_BASE . 'thumb/' . $last['img_file'] . '_A7.png'; ?>" alt="<?php echo $last['last_name']; ?>">
                </a>	
            </li>
            <?php
        }
        ?>
    </ul>
</div>
<div id="main_shoe_common" class="container" style="display: none;">
    <div class="custom_pager"></div>
    <ul class="step2_main_slider">
        <?php
        foreach ($lasts as $last) {
            ?>
            <li>
                <img class="main-shoe" src="<?php echo CustomShoeConfig::IMG_BASE . $last['location'] . '_A7.png'; ?>" alt="<?php echo $last['last_name'] ?>">
                <h3 class="main-shoe"><?php echo $last['last_name'] ?></h3>
                <p><?php echo $last['description'] ?></p>    
            </li>
            <?php
        }
        ?>
    </ul>

    <div id="shoe_design_button">
        <a style="margin-right: 10px;" href="javascript:void(0);" class="flat_button left" id="back-btn">BACK</a>
        <a href="javascript:void(0)" class="flat_button right" id="next-step">CONTINUE</a>
    </div>
</div>
<?php
$this->load->view('includes/footer');
?>