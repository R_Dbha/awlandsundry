<?php $this->load->view('includes/header.php'); ?>
<style type="text/css">
    #rotate_tab {display: table;margin: 0 auto;}
    /*#rotate_tab .tab {margin-bottom: 30px; padding: 0 20px; background: none; font-size: 14px; font-family: 'GravurCondensedRegular'; cursor: pointer; }*/
    #rotate_tab .tab {padding: 0 20px; background: none; font-size: 14px; cursor: pointer; margin-bottom: 20px !important; font-family: 'gibsonsemibold' !important; letter-spacing: 1.5px !important; }
    #rotate_tab .current-item {font-weight: bold;}
    a.current-step { color: #DA090A !important; text-decoration: underline; }
</style>
<div id="step5-menu-wrapper">
    <?php $this->load->view('shoedesign/shoe_menu'); ?>
    <!--    <div id="menu" class="container step5">
            <ul id="menuitem">
                <li ><a href="javascript:;">Feet First</a></li>
                <li ><a href="javascript:;">Essential Style</a></li>
                <li ><a href="javascript:;">Design Features</a></li>
                <li ><a href="javascript:;">Materials / Colors</a></li>
                <li ><a href="javascript:;">Sole Attributes</a></li>
                <li class="active-li"><a class="current-step" href="javascript:;">Personal Touches</a></li>
            </ul>
        </div>-->
</div>
<div class="spinner" id="loader"></div>
<div id="step5_shoes" class="container" style="display:none;">
    <div class="steps-main-title">
        <h2>Personal Touches</h2>
        <p class="sub-title">LACING and STITCHING colors. GENIUS IS IN THE DETAILS.<span> SELECT ONE OF EACH :</span></p>
    </div>
    <div id="color-wrapper">
        <div id="tab_wrap">
            <?php if ($styledetails['is_lace_enabled']) { ?>
                <div id="lacing-tab" class="left center  tab current-item">LACING COLOR</div>
            <?php } ?>
            <?php if (count($stitches) > 0) { ?>
                <div id="stitching-tab" class="right center  tab <?php echo ($styledetails['is_lace_enabled']) ? '' : 'current-item'; ?>">STITCHING COLOR</div>
            <?php } ?>
        </div>

        <?php if ($styledetails['is_lace_enabled']) { ?>
            <div class="lacing-tab slide-container current-item-container">
                <ul class="step5-lacing-slider" id="lacing-slider">
                    <?php foreach ($laces as $lace) { ?>
                        <?php $isactive = $lace['lace_code'] == $lace_model['code'] ? 'active-color' : ''; ?>
                        <li class="<?php echo $isactive; ?>">
                            <div class="left color" style="background: <?php echo $lace['color_code']; ?>" data-tooltip="<?php echo $lace['lace_name']; ?>">
                                <input type="hidden" class="lace-id" value="<?php echo $lace['lace_id']; ?>">
                                <input type="hidden" class="lace-code" value="<?php echo $lace['lace_code']; ?>">
                                <span class="round"></span>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>

        <div class="stitching-tab slide-container <?php echo ($styledetails['is_lace_enabled']) ? 'none' : ''; ?>">
            <?php if (count($stitches) > 0) { ?>
            <ul class="step5-stitching-slider" id="stitching-slider">
                <?php foreach ($stitches as $stitch) { ?>
                    <?php $isactive = $stitch['stitch_code'] == $stitch_model['code'] ? 'active-color' : ''; ?>
                    <li class="<?php echo $isactive; ?>">
                        <div class="left color" style="background: <?php echo $stitch['stitch_color']; ?>;" data-tooltip="<?php echo $stitch['stitch_name']; ?>">
                            <input type="hidden" class="stitch-code" value="<?php echo $stitch['stitch_code']; ?>">
                            <input type="hidden" class="stitch-id" value="<?php echo $stitch['stitch_id']; ?>">
                            <span class="round"></span>
                        </div>
                    </li>
                <?php } ?>
            </ul>
            <?php } ?>
        </div>
    </div>

    <div id="step5_container" class="right">
        <div id="monogram-complementary" class="<?php echo (isset($monogram['text'])) ? (($monogram['text'] != '' && $monogram['text'] != NULL) ? 'check-blue-1' : '' ) : ''; ?>" style="z-index: 2">
                    <!--<img src="https://www.awlandsundry.com/files/images/materials/CP/(1)Calfplain-darkbrown.jpg" class="monogram-badge" style="display: none">-->
            <p>
                <input type="checkbox" id="monoGramChk" <?php echo (isset($monogram['text'])) ? (($monogram['text'] != '' && $monogram['text'] != NULL) ? 'checked' : '' ) : ''; ?>  />
                <label for="monoGramChk" class="checkbox-click-1">Complementary Monogram</label>
                <!-- <span>Complementary Monogram</span> -->
                <input type="text" maxlength=3 id="monogram-text" placeholder="ENTER 1-3 INITIALS" value="<?php echo $monogram['text']; ?>"/>
                <a href="javascript:void(0);" id="remove-monogram" style="visibility: hidden;"></a>
            </p>
            <!--                    <div class="monogram" style="display: none;">
                                    <div class="mono-image"></div>
                                </div>-->
        </div>
        <div id="step5_design">

            <div id="design-visual">
                <div class="visualImgWrap">
                    <img src="" id="quarter" />
                    <img src="" id="stitch" />
                    <img src="" id="lace" />
                    <img id="toe" src="" />
                    <img id="vamp" src="" />
                    <img id="eyestay" src="" />
                    <img id="foxing" src="" />
                    <img id="overlay" src="<?php echo CustomShoeConfig::IMG_BASE; ?>overlay.png" style="display:none; margin-top: 25px; margin-left: -15px;" />
                    <div class="monogram">
                        <div class="mono-image"></div>
                    </div> 
                </div>
            </div>            
        </div> 
        <div id="rotate_tab" style="z-index: 99 !important">
            <div id="rotate-side" class="left rotate-side tab">SIDE VIEW</div>
            <div id="rotate-top" class="right rotate-top center tab">TOP VIEW</div>
        </div>

        <div id="style-last-name">
            <h3><?php echo $styledetails['style_name'] . ' / ' . $styledetails['last_name'] . ' / ' . $material . ' / ' . $sole . ' / <font style="color: goldenrod">CUSTOM SIZED</font>'; ?></h3>
        </div>
        <div id="shoe_design_button">
            <p id="custom-price"><span class="price">$0</span> w/Free shipping</p>
        </div>
        <div id="shoe-trees-block" style="z-index: 2;">
            <p class="shoe-tree-checkbox">
                <input type="checkbox" id="shoeTreeChk">
                <label for="shoeTreeChk" class="checkbox-click">SHOE TREES</label>
            </p>
            <div class="shoe-trees">
                <p class="shoe-trees-intro">
                    Handmade shoes are an investment. Protect yours with our genuine cedar shoe trees. 
                    <b>$30</b>
                </p>
                <img src="<?php echo base_url() . 'files/images/shoetree/shoe-tree.png'; ?>" alt="shoe-trees" class="shoe-tree-img img-responsive">
            </div>
        </div>
        
        <!-- <div class="step5-extraa">
             <div class="step5-extraa-inner">
                 
             </div>
        </div>  -->      
        <div class="proceed-next">
            <a href="javascript:void(0);" id="back-btn" title="BACK" class="next-step proceed-btn"><span class="arrow-next arrow-back">&nbsp;</span>BACK</a>
            <!--<a href="javascript:void(0);" title="Proceed to Check Out" class="next-btn arrow-next-a"><span class="arrow-next">&nbsp;</span></a>-->
            <a href="javascript:void(0);" id="next-btn" title="PROCEED TO CHECK OUT " class="next-btn proceed-btn">PROCEED TO CHECK OUT </a>
        </div>
    </div>
    <canvas style="display:none;" height="405" width="720" id="pic-canvas"></canvas>
</div>

<div class="overlay1" id="overlay1"></div>

<div class="modal fade" id="login_modal" tabindex="-1" data-role="popup" role="dialog" aria-labelledby="login_modal" aria-hidden="true" dismissible="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content" >
            <?php $this->load->view('login_modal'); ?>
        </div>
    </div>
</div>

<div class="modal fade" id="signup_modal" tabindex="-1" data-role="popup" role="dialog" aria-labelledby="signup_modal" aria-hidden="true" dismissible="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content" >
            <?php $this->load->view('signup_modal'); ?>
        </div>
    </div>
</div>

<script>
    var imageBase = '<?php echo CustomShoeConfig::IMG_BASE; ?>';
    var svgPath = "<?php echo CustomShoeConfig::FILE_BASE . 'SVG/'; ?>";
    var designBase = "<?php echo CustomShoeConfig::FILE_BASE . 'designs/'; ?>";
    var thumbBase = "<?php echo CustomShoeConfig::FILE_BASE . 'thump/'; ?>";
</script>

<?php $this->load->view('includes/footer'); ?>
