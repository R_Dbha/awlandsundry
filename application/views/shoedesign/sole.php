<?php $this->load->view('includes/header'); ?>
<style type="text/css">
    a.current-step { color: #DA090A !important; text-decoration: underline; }
    .soleSlider > li  { border: 1px solid white !important }
    .active-sole > img { border: 1px solid blue !important }
    .active-color > img { border: 1px solid blue !important }
    .active-stitch > img { border: 1px solid blue !important }
</style>
<div id="step4-menu-wrapper">
    <?php $this->load->view('shoedesign/shoe_menu'); ?>
</div>

<div class="spinner" id="loader"></div>
<div id="step4_shoes" class="container" style="display:none;">
    <div class="steps-main-title">
        <h2>Sole Attributes</h2>
        <p class="sub-title">Now let's focus on the business end.<span> SELECT AN OUT SOLE AND EDGE COLOR:</span></p>
    </div>
    <div id="step4_container" class="right">
        <div class="step4_main_slider">
            <div id="mainSlid" style="display: block;float: right;text-align: center;width: 100%;margin-right: 90px">
                <div id="slid1" style="display: inline-block;position: relative;margin-left: 160px;">
                    <div class="slide-title">
                        <span>OUTSOLE</span>
                    </div>
                    <div class="slide-no">
                        <p class="block-no">
                            <span>1</span>
                        </p>
                        <p class="choose-1">(CHOOSE ONE)</p>       
                    </div>
                    <ul class="soleSlider-1">
                        <?php foreach ($soleArr['sole'] as $soleVal) { ?>
                            <li>
                                <a class="sole-selector" href="javascript:void(0)" data-id="<?php echo $soleVal['id']; ?>" data-name="<?php echo $soleVal['sole_name']; ?>" data-code="<?php echo $soleVal['code']; ?>">
                                    <img class="soleImg img-responsive" title="<?php echo $soleVal['sole_name']; ?>"  src="<?php echo base_url() . 'files/images/sole/' . $soleVal['image']; ?>"  /> 
                                    <span class="round"></span>                                    
                                    <span class="sole-caption"><?php echo $soleVal['sole_name']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div id="slid2" style="display: inline-block;position: relative;margin-right: 10px;float: right;">
                    <div class="slide-title">
                        <span>EDGE COLOR</span>
                    </div>
                    <div class="slide-no">
                        <p class="block-no">
                            <span>2</span>
                        </p>
                        <p class="choose-1">(CHOOSE ONE)</p>       
                    </div>
                    <ul class="soleSlider-1  vertSlider-1">
                        <?php foreach ($soleArr['soleColor'] as $colorVal) { ?>
                            <?php $clrFor = explode(',', $colorVal['color_for']) ?>
                            <li class="clrFor <?php echo 'clrFor_' . (implode(' clrFor_', $clrFor)); ?> ">
                                <a class="color-selector" href="javascript:void(0)" data-id="<?php echo $colorVal['id']; ?>" data-name="<?php echo $colorVal['color_name']; ?>" data-code="<?php echo $colorVal['color_code']; ?>">
                                    <img class="soleImg img-responsive" title="<?php echo $colorVal['color_name']; ?>" src="<?php echo base_url() . 'files/images/sole/' . $colorVal['image']; ?>" />
                                    <span class="round"></span>
                                    <span class="sole-caption"><?php echo $colorVal['color_name']; ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div id="slid3">
                    <div class="slide-title"></div>
                    <div class="stich-box">
                        <?php foreach ($soleArr['soleStitch'] as $stitchVal) { ?>
                            <div class="stich-option">
                                <div class="stich-check">
                                    <input id="sole-stitch-chk-<?php echo $stitchVal['id']; ?>" type="checkbox"  class="sole-stitch-chk" data-charge="<?php echo $stitchVal['extra_charge']; ?>" data-id="<?php echo $stitchVal['id']; ?>" data-code="<?php echo $stitchVal['stitch_code']; ?>" >
                                    <label for="sole-stitch-chk-<?php echo $stitchVal['id']; ?>"><?php echo strtoupper($stitchVal['stitch_name']); ?></label>
                                </div>
                                <p class="stich-value" >(<?php echo strtoupper($stitchVal['description']); ?>)</p>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="style-last-name">
            <h3><?php echo $styledetails['style_name'] . ' / ' . $styledetails['last_name'] . ' / ' . $material . ' / <font class="bottomHeading">' . $soleArr['sole'][0]['sole_name'] . '</font>' . ' / <font style="color: goldenrod">CUSTOM SIZED</font>'; ?></h3>
        </div>
        <div id="shoe_design_button">
            <p id="custom-price"><span class="price">$0</span>w/Free shipping</p>
        </div>
        <div class="proceed-next">
            <a href="javascript:void(0);" id="back-btn" title="BACK" class="next-step proceed-btn"><span class="arrow-next arrow-back">&nbsp;</span>BACK</a>
            <a href="javascript:void(0);" id="next-btn" title="PROCEED TO PERSONAL TOUCHES" class="next-btn proceed-btn">PROCEED TO PERSONAL TOUCHES</a>
        </div>
    </div>
</div>

<div class="overlay1" id="overlay1"></div>
<div class="modal fade" style="display:none;" id="login_modal" data-role="popup"  tabindex="-1" role="dialog" aria-labelledby="login_modal" aria-hidden="true" dismissible="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content">
            <?php $this->load->view('login_modal'); ?>
        </div>
    </div>
</div>

<div class="modal fade" style="display:none;" id="signup_modal" data-rol="popup" tabindex="-1" role="dialog" aria-labelledby="signup_modal" aria-hidden="true" dismissible="true">
    <div class="modal-dialog" style="width:800px">
        <div class="modal-content" >
            <?php $this->load->view('signup_modal'); ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    var imageBase = "<?php echo CustomShoeConfig::IMG_BASE; ?>";
    var svgPath = "<?php echo CustomShoeConfig::FILE_BASE . 'SVG/'; ?>";
    var designBase = "<?php echo CustomShoeConfig::FILE_BASE . 'designs/'; ?>";
    var thumbBase = "<?php echo CustomShoeConfig::FILE_BASE . 'thump/'; ?>";
</script>

<?php $this->load->view('includes/footer'); ?>

<script type="text/javascript">
    $('document').ready(function () {
        $('body').on("change", '.sole-stitch-chk', function () {
            $('.stich-option').removeClass('check-blue');
            if ($(this).is(":checked")) {
                $(this).closest('.stich-option').addClass('check-blue');
            }
        })
    });
</script>

<div class="container">
    <div class="modal fade" id="myModal" role="dialog" data-keyboard="true" dismissible="false">
        <div class="modal-dialog " style="width: 45%">
            <div class="modal-content">
                <div class="modal-body modalOverLay check-blue">
                    <div id="size-info" style="padding:1px">
                        <span class="close" data-dismiss="modal" style='background: url("<?php echo base_url(); ?>assets/css/images/icons/newsletterclose_black.png") no-repeat; background-size:27 px;'></span>
                        <?php foreach ($soleArr['soleStitch'] as $stitchVal) { ?>
                            <img class="stitchImg img-responsive stitchImg_<?php echo $stitchVal['stitch_code']; ?>" src="<?php echo base_url() . 'files/images/sole/' . $stitchVal['image']; ?>" style="display: none" />
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>