<script type="text/javascript">
    var SHOE_TREE_PRICE = "<?php echo SHOE_TREE_PRICE; ?>";
</script>
<?php
$links = [
    'Feet First' => 'create-a-custom-shoe/',
    'Essential Style' => 'create-a-custom-shoe/select-style',
    'Design Features' => 'create-a-custom-shoe/design-features',
    'Materials / Colors' => 'create-a-custom-shoe/materials-and-colors',
    'Sole Attributes' => 'create-a-custom-shoe/sole-attributes',
    'Personal Touches' => 'create-a-custom-shoe/details',
];
?>

<div id="menu" class="container">
<div class="menu-container">
    <ul id="menuitem">
        <?php $noBg = ''; ?>
        <?php foreach ($links as $key => $value) { ?>
            <li class="<?php echo ($currentStep == $key) ? 'active-li' : $noBg ?>"><p><a data-href="<?php echo $value; ?>" class="menuLinks <?php echo ($currentStep == $key) ? 'current-step' : '' ?>" href="javascript:;"><?php echo $key; ?></p></a></li>
                <?php $noBg = ($currentStep == $key) ? 'bg-none' : $noBg; ?>
            <?php } ?>
    </ul>
</div>
</div>