<?php $this->load->view('includes/header'); ?>
<style type="text/css">
    .current-item {font-weight: bold;}
    a.current-step { color: #DA090A !important; text-decoration: underline; }
    .SumoSelect {height: 20px;margin: 0px !important;}
    .SumoSelect > .CaptionCont > span {padding-right: 0px !important;}
    .SumoSelect > .CaptionCont > label {display: none;}
    .SumoSelect > .CaptionCont {height: auto;width:79px;border: 0px; margin-left: 0px !important; font-size: 12px; font-weight: normal; font-style: normal; font-family: 'gibsonsemibold'; text-align: center;}
    .input-1 > .SumoSelect > .CaptionCont { border-left: 4px solid #209cdf !important;}
    .sizeType{ color: #b89e9e;cursor:pointer}
    .activeSize{ color: #292929;cursor:pointer}
</style>
<div id="step6-menu-wrapper">
    <?php $this->load->view('shoedesign/shoe_menu'); ?>
    <!--    <div id="menu" class="container step6">
            <ul id="menuitem">
                <li class="active-li"><p><a class="current-step" href="javascript:;">Feet First</p></a></li>            
                <li class="bg-none"><p><a href="javascript:;">Essential Style</p></a></li>            
                <li class="bg-none"><p><a href="javascript:;">Design Features</p></a></li>            
                <li class="bg-none"><p><a href="javascript:;">Materials / Colors</p></a></li>            
                <li class="bg-none"><p><a href="javascript:;">Sole Attributes</p></a></li>            
                <li class="bg-none"><p><a href="javascript:;">Personal Touches</p></a></li>
            </ul>
        </div>-->
</div>

<div class="container">
    <div class="steps-main-title">
        <h2>Feet First</h2>
        <p class="sub-title">TO Make perfect-fitting custom shoes WE MUST FIRST GET to know your feet.</p>
    </div>
    <div id="step6_shoes">
        <div class="custom-shoe-sizes-box">
            <div class="top-box">
                <p class="top-box-title">
                    <b>First,</b> tell us what standard sizes </br> you are currently wearing;
                </p>
                <div class="size-width-box">
                    <div class="lr-title">
                        <span class="t-1">LEFT</span>
                        <span class="t-2">RIGHT</span>
                    </div>
                    <div class="input-row">
                        <span class="row-title">S</span>
                        <span class="row-input input-1">
                            <!--<input type="text" name="size" placeholder="SIZE" id="left-size">-->
                            <select class="step6_sizes" id="left-size">
                                <option value="">SIZE</option>
                                <option value="5">5 </option>
                                <option value="5.5">5.5 </option>
                                <option value="6">6 </option>
                                <option value="6.5">6.5 </option>
                                <option value="7">7 </option>
                                <option value="7.5">7.5 </option>
                                <option value="8">8 </option>
                                <option value="8.5">8.5 </option>
                                <option value="9">9 </option>
                                <option value="9.5">9.5 </option>
                                <option value="10">10 </option>
                                <option value="10.5">10.5 </option>
                                <option value="11">11 </option>
                                <option value="11.5">11.5 </option>
                                <option value="12">12 </option>
                                <option value="12.5">12.5 </option>
                                <option value="13">13 </option>
                                <option value="13.5">13.5 </option>
                                <option value="14">14 </option>
                            </select>
                        </span>
                        <span class="row-input">
                            <!--<input type="text" name="size" placeholder="SIZE" id="right-size">-->
                            <select class="step6_sizes" id="right-size">
                                <option value="">SIZE</option>
                                <option value="5">5 </option>
                                <option value="5.5">5.5 </option>
                                <option value="6">6 </option>
                                <option value="6.5">6.5 </option>
                                <option value="7">7 </option>
                                <option value="7.5">7.5 </option>
                                <option value="8">8 </option>
                                <option value="8.5">8.5 </option>
                                <option value="9">9 </option>
                                <option value="9.5">9.5 </option>
                                <option value="10">10 </option>
                                <option value="10.5">10.5 </option>
                                <option value="11">11 </option>
                                <option value="11.5">11.5 </option>
                                <option value="12">12 </option>
                                <option value="12.5">12.5 </option>
                                <option value="13">13 </option>
                                <option value="13.5">13.5 </option>
                                <option value="14">14 </option>
                            </select>
                        </span>
                        <span class="size-country">(US)</span>
                        <a href="javascript:;" title="Help" class="help_button shoe-size-det">?</a>
                    </div>
                    <div class="input-row">
                        <span class="row-title">W</span>
                        <span class="row-input input-1">
                            <!--<input type="text" name="WIDTH" placeholder="WIDTH" id="left-width">-->
                            <select class="step6_sizes" id="left-width">
                                <option value="">WIDTH</option>
                                <option value="Regular">Regular</option>
                                <option value="Wide">Wide</option>
                                <option value="Extra Wide">Extra Wide</option>
                                <option value="Triple Wide">Triple Wide</option>
                            </select>
                        </span>
                        <span class="row-input">
                            <!--<input type="text" name="WIDTH" placeholder="WIDTH" id="right-width">-->
                            <select class="step6_sizes" id="right-width">
                                <option value="">WIDTH</option>
                                <option value="Regular">Regular</option>
                                <option value="Wide">Wide</option>
                                <option value="Extra Wide">Extra Wide</option>
                                <option value="Triple Wide">Triple Wide</option>
                            </select>
                        </span>
                        <span class="size-country">(US)</span>
                        <a href="javascript:;" title="Help" class="help_button shoe-size-det">?</a>
                    </div>
                    <div class="skip-cust-sizing">
                        <a href="javascript:;" class="skip-cancel next-step"><span class="x-btn">&nbsp;</span></a>
                        <a href="javascript:;" class="skip-sizing next-step">SKIP CUSTOM SIZING</a>
                    </div>
                </div>
            </div>
            <div class="top-box top-box-custom">
                <p class="top-box-title">
                    <b>Next,</b> your custom measurements:
                </p>
                <div class="size-width-box">
                    <div class="lr-title">
                        <span class="t-1">LEFT</span>
                        <span class="t-2">RIGHT</span>
                        <span class="t-3"><font class="<?php echo (isset($sizeType) ? (($sizeType == 'IN') ? 'activeSize' : '' ) : 'activeSize'); ?> sizeType">IN</font> / <font class="<?php echo (isset($sizeType) ? (($sizeType == 'CM') ? 'activeSize' : '' ) : ''); ?>  sizeType">CM</font></span>
                        <input type="hidden" id="sizeType" value="<?php echo (isset($sizeType) ? $sizeType : 'IN'); ?>" />
                    </div>
                    <div class="input-row">
                        <span class="row-title">A</span>
                        <span class="row-input input-1"><input class="onlyNumber" type="text" name="LENGTH" placeholder="LENGTH" id="m_size_left"></span>
                        <span class="row-input"><input class="onlyNumber" type="text" name="LENGTH" placeholder="LENGTH" id="m_size_right"></span>
                    </div>
                    <div class="input-row">
                        <span class="row-title">B</span>
                        <span class="row-input input-1"><input class="onlyNumber" type="text" name="WIDTH" placeholder="WIDTH" id="m_width_left"></span>
                        <span class="row-input"><input class="onlyNumber" type="text" name="WIDTH" placeholder="WIDTH" id="m_width_right"></span>
                    </div>
                    <div class="input-row">
                        <span class="row-title">C</span>
                        <span class="row-input input-1"><input class="onlyNumber" type="text" name="BALL" placeholder="BALL" id="m_girth_left"></span>
                        <span class="row-input"><input type="text" class="onlyNumber" name="BALL" placeholder="BALL" id="m_girth_right"></span>
                    </div>
                    <div class="input-row">
                        <span class="row-title">D</span>
                        <span class="row-input input-1"><input class="onlyNumber" type="text" name="INSTEP" placeholder="INSTEP" id="m_instep_left"></span>
                        <span class="row-input"><input class="onlyNumber" type="text" name="INSTEP" placeholder="INSTEP" id="m_instep_right"></span>
                    </div>
                    <div class="input-row">
                        <span class="row-title">E</span>
                        <span class="row-input input-1"><input class="onlyNumber" type="text" name="HEEL" placeholder="HEEL" id="m_heel_left"></span>
                        <span class="row-input"><input class="onlyNumber" type="text" name="HEEL" placeholder="HEEL" id="m_heel_right"></span>
                    </div>
                    <div class="input-row">
                        <span class="row-title">F</span>
                        <span class="row-input input-1"><input class="onlyNumber" type="text" name="ANKLE" placeholder="ANKLE" id="m_ankle_left"></span>
                        <span class="row-input"><input class="onlyNumber" type="text" name="ANKLE" placeholder="ANKLE" id="m_ankle_right"></span>
                    </div>                    
                </div>
            </div>
        </div>  
    </div>
    <div id="step6_container" class="">
        <div id="step6_design">
            <div class="fit-first-design">
                <img src="<?php echo base_url(); ?>assets/img/svg/Footsizing.svg" class="img-responsive" align="right" alt="fit-first-design">
                <div class="fit-descr-block">
                    <div class="fit-left">
                        <h3>THINGS YOU'LL NEED:</h3>
                        <ul>
                            <li>A cloth or soft plastic measuring tape (or a string and a ruler).</li>
                            <li>Your feet.</li>
                        </ul>
                    </div>
                    <div class="fit-right">
                        <p>Be sure to measure accurately with the tape snug but not too tight to get the best fit.</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="shoe_design_button"></div>
    </div>
    <div class="skip-cust-sizing">
        <a href="javascript:;" class="skip-cancel next-step validateSize"><img class="x-btn" src="<?php echo base_url(); ?>assets/css/advance_arrow_wht.svg" alt="Cancel"></a>
        <a href="javascript:void(0);" id="next-step" class="next-step skip-sizing validateSize">PROCEED TO STYLE</a>
    </div>
</div>
<div class="overlay">
    <div id="sizing-popup-wrapper" class="popup-wrapper">
        <div id="size-info">
            <span class="close"></span>
            <p>Awl & Sundry shoes are true to fit. To get the right fit, please order the most frequently worn size.  For instance, if you wear a 10 for most of your shoes, we would recommend ordering a 10 with Awl & Sundry as well. 
                If you are unsure of your size, you can trace your foot on a blank piece of paper and send it to us at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a> and we will recommend you your size and last.</p>
            <h1>Sizing Chart</h1>
            <table>
                <tr><th>US</th><th>CAN</th><th>UK</th><th>AUS</th></tr>
                <tr><td>5</td><td>5</td><td>4</td><td>4</td></tr>
                <tr><td>5&#189;</td><td>5&#189;</td><td>4&#189;</td><td>4&#189;</td></tr>
                <tr><td>6</td><td>6</td><td>5</td><td>5</td></tr>
                <tr><td>6&#189;</td><td>6&#189;</td><td>5&#189;</td><td>5&#189;</td></tr>
                <tr><td>7</td><td>7</td><td>6</td><td>6</td></tr>
                <tr><td>7&#189;</td><td>7&#189;</td><td>6&#189;</td><td>6&#189;</td></tr>
                <tr><td>8</td><td>8</td><td>7</td><td>7</td></tr>
                <tr><td>8&#189;</td><td>8&#189;</td><td>7&#189;</td><td>7&#189;</td></tr>
                <tr><td>9</td><td>9</td><td>8</td><td>8</td></tr>
                <tr><td>9&#189;</td><td>9&#189;</td><td>8&#189;</td><td>8&#189;</td></tr>
                <tr><td>10</td><td>10</td><td>9</td><td>9</td></tr>
                <tr><td>10&#189;</td><td>10&#189;</td><td>9&#189;</td><td>9&#189;</td></tr>
                <tr><td>11</td><td>11</td><td>10</td><td>10</td></tr>
                <tr><td>11&#189;</td><td>11&#189;</td><td>10&#189;</td><td>10&#189;</td></tr>
                <tr><td>12</td><td>12</td><td>11</td><td>11</td></tr>
                <tr><td>12&#189;</td><td>12&#189;</td><td>11&#189;</td><td>11&#189;</td></tr>
                <tr><td>13</td><td>13</td><td>12</td><td>12</td></tr>
                <tr><td>13&#189;</td><td>13&#189;</td><td>12&#189;</td><td>12&#189;</td></tr>
                <tr><td>14</td><td>14</td><td>13</td><td>13</td></tr>
            </table>
        </div>
    </div>
    <div id="width-popup-wrapper" class="popup-wrapper">
        <div id="width-info">
            <span class="close"></span>
            <p> Awl & Sundry shoes are defaulted to width D. If you have a wider foot, we would strongly recommend tracing your foot on a blank piece of paper and send it to us at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a> and we will recommend you your width and last. </p>
            <h1>Width Chart</h1>
            <table>
                <tr><td>D</td><td>Normal</td></tr>
                <tr><td>E</td><td>Wide</td></tr>
                <tr><td>EE</td><td>Extra Wide</td></tr>
                <tr><td>EEE</td><td>Extra Extra Wide</td></tr>
            </table>
        </div>
    </div>
    <div id="measure-popup-wrapper" class="popup-wrapper">
        <div id="measure-info">
            <span class="close"></span>
            <h1>How to measure your feet</h1>
            <div>
                <ul>
                    <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/1.jpg"/></li>
                    <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/2.jpg"/></li>
                    <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/3.jpg"/></li>
                    <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/4.jpg"/></li>
                    <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/5.jpg"/></li>
                    <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/6.jpg"/></li>
                    <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/7.jpg"/></li>
                    <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/8.jpg"/></li>
                </ul>
            </div>
        </div>
    </div>    
</div>
<div class="overlay1" id="overlay1"></div>

<script>
    var imageBase = '<?php echo CustomShoeConfig::IMG_BASE; ?>';
    var svgPath = "<?php echo CustomShoeConfig::FILE_BASE . 'SVG/'; ?>";
    var designBase = "<?php echo CustomShoeConfig::FILE_BASE . 'designs/'; ?>";
    var thumbBase = "<?php echo CustomShoeConfig::FILE_BASE . 'thump/'; ?>";
</script>

<?php $this->load->view('includes/footer'); ?>