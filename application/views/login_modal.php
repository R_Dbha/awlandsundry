

<div class="modal-header">
    <div class="row">
        <h3 class="page-title" align="center">LOGIN OR CREATE AN ACCOUNT</h3>
        <button type="button" class="close-button" style="background-color:#FCFCFC"  data-dismiss="modal"><a href="#" data-rel="back"><span aria-hidden="true">&times;</span></a></button>
    </div>
</div>
<div class="modal-body">
    <div id="signin-form">
        <div class="signin-form-inner left">
            <h3 class="page-title">REGISTERED CUSTOMER</h3>
            <p class="signin_content">
                If you have an account with us, login using your email address.
            </p>
            <!--form id="login_form" action="dashboard.php"-->
            <div id="login_form">
                <p>
                    <label>*EMAIL ADDRESS</label>
                    <input type="email" id="l_email" name="email" value="<?php echo @$email ?>" required/>
                </p>
                <p>
                    <label>*PASSWORD</label>
                    <input type="password" id="l_password" name="password" required/>
                </p>
                <span style="color:red;" id="errors"><?php echo @$errors['msg'] ?></span>
                <p>
                    <a href="<?php echo base_url(); ?>forgot-password">FORGOT YOUR PASSWORD?</a>
                </p>
                <p>
                    <input type="submit" id="popup_login" name="login" value="LOGIN"  />
                </p>
                <p>
                    <label>* Required fields</label>
                </p>
            </div>
        </div>
        <div id="line"></div>
        <div class="signin-form-inner right">
            <h3 class="page-title">NEW CUSTOMERS</h3>
            <p class="signin_content">
                By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses , view and track your orders in your account and more.
            </p>
            <p>
                <a href="#signup_modal" data-rel="popup"   ><input type="submit" id="register" name="register" data-rel="popup"  data-dismiss="modal"  data-toggle="modal" data-target="#signup_modal" value="REGISTER" /></a>
            </p>
            <div  id="social_login">
                <a  id="fb_login" class="fbsignup"><img src="<?php echo base_url(); ?>assets/css/images/fb_login.png"></a>
                <a href="<?php echo base_url(); ?>linkedin/signup" id="linkedin_login"><img src="<?php echo base_url(); ?>assets/css/images/linkedin_login.png"></a>
                <!--<a href="<?php echo @$twitter_login_link; ?>" id="twitter_login"><img src="<?php echo base_url(); ?>assets/css/images/twitter_login.png" style="height: 45px;
width: 247px;"></a>-->
            </div>
        </div>
    </div>
</div>

<script>
    
    function getUserData() {
        FB.api('/me', function (response) {
            $.ajax({
                type: "POST",
                url: base_url + 'facebooklogin/fb_login',
                data: response,
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        alert(data.msg);
                        $('#login_modal').hide();
                        CustomShoe.saveDesign();
                        $('#replace').remove();
                        $('.secondary-links').prepend(data.html);
                    } else {
                        $('#errors').text(data.msg);
                    }

                }
            });
        });
    }

    window.fbAsyncInit = function () {
        //SDK loaded, initialize it
        FB.init({
            appId: '250777875062389',
            xfbml: true,
            version: 'v2.2'
        });
    };

//load the JavaScript SDK
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

//add event listener to login button
    document.getElementById('fb_login').addEventListener('click', function () {
        //do the login
        FB.login(function (response) {
            if (response.authResponse) {
                //user just authorized your app

                getUserData();
            }
        }, {scope: 'email,public_profile', return_scopes: true});
    }, false);
</script>


