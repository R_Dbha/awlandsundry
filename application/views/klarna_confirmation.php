<?php
$this->load->view('includes/klarna_header');
?>
<div id='klarna-confirmation-wrapper'>
    <div id="klarna-confirmation" class="klarna-confirmation-container">
        <div><?php echo $checkout['html_snippet']; ?></div>
        <h3 class="page-title">
            <span>ORDER CONFIRMATION</span>
        </h3>
        <div id="order-confirmation-container">
            <div class="left half" id="order-confirmation-left">
                <h3>Thank you, your shoe order has been forwarded to our custom shop</h3>
                <p>Your order reference number is <?php echo @$order_details['invoice_no']; ?></p>
                <p>You may use this number to check your order status via your personal account page.</p>
                <p>Estimated delivery time is 4-6 weeks.</p>
                <p>Please check your email for a purchase order confirmation and receipt.</p>
                <p>Feel free to contact us with any questions.</p>
                <p>Thank you for joining our shoe revolution</p>
            </div>
            <div  class="right" id="order-confirmation-right">
                <h3>ORDER SUMMARY</h3> 
                <div id="order-summary-container">
                    <?php
                    $cnt = 0;
                    foreach ($items as $item) {
                        $cnt = $cnt + 1;
                        if ($item['item_name'] != 'Gift Card') {
                            ?> 
                            <label>ITEM <?php echo $cnt; ?></label>
                        <?php } else { ?>
                            <label><?php
                                echo $item['item_name'];
                                $cnt = $cnt - 1;
                                ?> </label>
                        <?php } ?>
                        <label>$ <?php echo number_format(round($item['item_amt'], 2), 2); ?></label>
                        <?php
                    }
                    if ($order_details['discount'] > 0) {
                        ?>
                        <label>PROMO DISCOUNT</label>
                        <label>$ <?php echo number_format($order_details['discount'], 2); ?></label>
                    <?php } ?>
                    <label>SHIPPING</label>
                    <label>$ <?php echo number_format($order_details['shipping_cost'], 2); ?></label>
                    <label>TAXES</label>
                    <label>$ <?php echo number_format($order_details['tax_amount'], 2); ?></label>
                </div>
                <div id="total-amt">
                    <label>ORDER TOTAL</label>
                    <label>$ <?php echo number_format(round($order_details['order_total']), 2); ?></label>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
//Facebook Conversion Code for Checkout 
    (function () {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6017866457827', {'value': '<?php echo number_format(round($order_details['order_total']), 2); ?>', 'currency': 'USD'}]);
//    (function(){
    // Purchase Details add to dataLayer
        var products = [];
        
        <?php $cnt=0;
        foreach ($items as $item) {
            $cnt = $cnt + 1;
            ?>
            products.push({ 'name': "<?php echo $item['item_name'];?>",       // Name or ID is required.
                            'price': "<?php echo number_format(round($item['item_amt'], 2), 2); ?>",
                            'brand': 'Awl & Sundry',
                            'category': 'Ready To Wear Shoes',
                            'position':<?php echo $cnt;?>});
        <?php } ?>
        dataLayer.push({
            'ecommerce': {
                'purchase': {
                    'actionField': {
                        'id': '<?php echo $order_details['order_id']?>',                         // Transaction ID. Required for purchases and refunds.
                        'affiliation': 'Online Store',
                        'revenue': '<?php echo number_format(round($order_details['order_total']), 2); ?>',                     // Total transaction value (incl. tax and shipping)
                        'tax':'<?php echo number_format($order_details['tax_amount'], 2); ?>',
                        'shipping': '<?php echo number_format($order_details['shipping_cost'], 2); ?>',
                    },
                    'products': products
                }
            }
        });
//    });
</script>
<img height=1 width=1 border=0 src="http://linktrack.info/api/track?redirect_hash=.13geb&name=success&value=<?php echo number_format(round($order_details['order_total']), 2); ?>">
<?php if($pepperJam){ ?>
    <div id="pepperjam">
    </div>
    <script type="text/javascript">
        (function(){
            var pepperDiv = document.getElementById('pepperjam');
            var itemArray = <?php echo json_encode($items) ?>;
            var itemPrice = <?php echo number_format(round($order_details['order_total']), 2) ?>;
            itemPrice = parseFloat(itemPrice);
            var i = 1;
            for(var j = 0 ; j < itemArray.length ; j++){
                item = itemArray[j];
                item.price = (Math.round(item.item_amt-(item.item_amt * (item.discount_perc/100))));
                itemPrice -= item.price;
                if(j == itemArray.length-1){
                    if(itemPrice != 0){
                        item.price += itemPrice;
                    }
                }
		item.price = (item.price)/(item.quantity);
                item.price = item.price.toFixed(2);
            }
            var pepperjamHTML = '<iframe src="https://t.pepperjamnetwork.com/track?INT=DYNAMIC&PROGRAM_ID=8417&ORDER_ID=<?php echo $order_details['invoice_no']; ?>&';
            itemArray.forEach(function(item){
                pepperjamHTML += 'ITEM_ID'+ i + '=' + item.order_id + '_' + item.design_id + '_' + item.image_file + '_' + item.image_file;
                pepperjamHTML += '&ITEM_PRICE'+i+'='+ item.price;
                pepperjamHTML += '&QUANTITY'+i+'='+item.quantity;
                pepperjamHTML += (i < itemArray.length) ? '&' : '';
                i++;
            });
            pepperjamHTML += '" width="1" height="1" frameborder="0"></iframe>';
            pepperDiv.innerHTML = pepperjamHTML;
        })();
    </script>        
<?php } ?>
<?php
$this->load->view('includes/klarna_footer');
?>

