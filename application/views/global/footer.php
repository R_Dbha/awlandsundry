<footer id="footer">
    <div class="wrapper">
        <section class="clearfix">
            <span class="close"></span>
            <div class="row">
                <h3>Handmade shoes</h3>
                <ul>
                    <li><a href="<?php echo base_url(); ?>get-inspired">Get inspired</a></li>
                    <li><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Create a custom shoe</a></li>
                    <li>
                        <a href="<?php echo base_url(); ?>gift-card">Gift Card</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <h3>About us</h3>
                <ul>
                    <li><a href="<?php echo base_url(); ?>our-story">Our story</a></li>
                    <li><a href="<?php echo base_url(); ?>our-artisanal-process">our artisanal process</a></li>
                    <li><a href="<?php echo base_url(); ?>blog/" target="_blank">Blog</a></li>
                    <li><a href="<?php echo base_url(); ?>press">Press</a></li>
                </ul>
            </div>
            <div class="row">
                <h3>Customer service</h3>
                <ul>
                    <li><a href="<?php echo base_url(); ?>faq#shipping" onclick="faqScroll('#shipping');">Shipping</a></li>
                    <li><a href="<?php echo base_url(); ?>faq#returns" onclick="faqScroll('#returns');">Exchange</a></li>
                    <li><a href="<?php echo base_url(); ?>faq">FAQs</a></li>
                    <li><a href="<?php echo base_url(); ?>faq">Contact us</a></li>
                </ul>
            </div>
            <div class="row newsletter">
                <h3>Keep in touch</h3>
                <div class="keeptouchform">
                    <input type="email" class="email" id="email" onkeydown="if (event.keyCode == 13)
                                keepEmail();">
                    <input type="submit" class="submit" onclick="keepEmail();">
                    <div class="thanksforsignup"><h2>Thanks for Signing up!</h2></div>
                </div>
                <ul>
                    <li><span>Connect with us:</span>					
                        <div id="socials">
                            <a href="https://www.facebook.com/AwlandSundry" class="facebook" target="_blank">Facebook</a>
                            <a href="https://twitter.com/AwlandSundry" class="twitter" target="_blank">Twitter</a>
                            <a href="http://www.pinterest.com/awlandsundry/" class="pinterest" target="_blank">Pinterest</a>
                            <a href="http://instagram.com/awlandsundry" target="_blank" title="Instagram" class="instagram">Instagram</a>
                            <a href="http://awl-and-sundry.tumblr.com/" target="_blank" title="Tumblr" class="tumblr">Tumblr</a>
                            <a href="https://plus.google.com/111459378470264472967" target="_blank" rel="publisher" title="google plus" class="google-plus">Google+</a>
                        </div>
                    </li>
                </ul>
            </div>				
        </section>
        <div class="bottom clearfix">
            <p class="float-left">© Awl &amp; Sundry. All Rights Reserved. 2012 - 2014</p>
            <a class="float-right" href="<?php echo base_url(); ?>privacy-policy" target="_blank">Privacy Policy</a> 
            <a class="float-right" href="<?php echo base_url(); ?>terms-and-conditions" target="_blank">Terms of Use</a>
        </div>
    </div>
</footer>