<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Awl &amp; Sundry | <?php echo $title;?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">-->
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link href="<?php echo base_url(); ?>assets/css/layout.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/main.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/mediaelementplayer.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/img/favicon.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon">		
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ie.css">
    <![endif]-->
    <script type="text/javascript">
        var baseUrl = '<?php echo base_url(); ?>';
    </script>
</head>