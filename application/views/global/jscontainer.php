<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery-easing-1.3.js"></script>
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>assets/js/vendor/html5shiv.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins.js"></script>
<!--[if IE]>       
<script src="<?php echo base_url(); ?>assets/js/vendor/jquery.placeholder.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vendor/selectivizr-min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vendor/jquery.corner.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vendor/plugins-ie.js"></script>
<![endif]-->
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://awlandsundry.com/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 1]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();

</script>
<noscript><p><img src="http://awlandsundry.com/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->
<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/<?php echo $script; ?>.js"></script>
        <?php
    }
}
?>
