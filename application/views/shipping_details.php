<div class="checkout-overlay <?php echo @$shipping_overlay; ?>" ></div>
<div class="form">
    <h3 class="page-title">
        <span>SHIPPING</span>
    </h3>
    <table>
        <tr>
            <td>FIRST NAME</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['first_name']; ?></span>
                <input class="first_name" type="text" name="shipping_details[first_name]" value="<?php echo @$shipping_details['first_name']; ?>"/>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>LAST NAME</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['last_name']; ?></span>
                <input class="last_name" type="text" name="shipping_details[last_name]" value="<?php echo @$shipping_details['last_name']; ?>" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>ADDRESS LINE 1</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['address1']; ?></span>
                <input class="address1" type="text" name="shipping_details[address1]" value="<?php echo @$shipping_details['address1']; ?>" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>ADDRESS LINE 2</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['address2']; ?></span>
                <input class="address2" type="text" name="shipping_details[address2]" value="<?php echo @$shipping_details['address2']; ?>" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>CITY</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['city']; ?></span>
                <input class="city" type="text" name="shipping_details[city]" value="<?php echo @$shipping_details['city']; ?>" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>COUNTRY</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['country']; ?></span>
                <select class="country" name="shipping_details[country]" id="shipping_country">
                    <option>select</option>
                    <?php echo @$shipping_country; ?>
                </select>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>STATE</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['state']; ?></span>
                <?php if (isset($shipping_state_isText) && $shipping_state_isText < 1) { ?>
                    <select class="state" name="shipping_details[state]" id="shipping_state">
                        <?php echo @$shipping_state; ?>
                    </select>
                <?php } else { ?>
                    <input id="shipping_state" class="state" type="text" name="shipping_details[state]" value="<?php echo @$shipping_details['state']; ?>" />
                <?php } ?>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>ZIP CODE</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['zipcode']; ?></span>
                <input class="zipcode" type="text" name="shipping_details[zipcode]" value="<?php echo @$shipping_details['zipcode']; ?>" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td>PHONE</td>
            <td>
                <span class='error'><?php echo @$errors['shipping_details']['phone']; ?></span>
                <input class="phone" type="text" name="shipping_details[phone]" value="<?php echo @$shipping_details['phone']; ?>" />
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2"><input id="copy_same_address" type="checkbox" name=""/> <label style="display: inline" for="copy_same_address">Use same address for billing</label></td>
            <td></td>
        </tr>
    </table>
    <input type="hidden" id="hid_from_state" name="hid_from_state" value="<?php echo @$shipping_details['state']; ?>"/>
    <div class="form_submit">
        <button id="shipping_next">NEXT</button>
        <input type="hidden" id="shipping_error" value="<?php echo @$shipping_error; ?>">
    </div>
</div>