<?php $this->load->view('includes/header'); ?>
<div id="get-inspired-menu-wrapper">
    <!--div id="menu" class="container get_inspired" >
        <ul id="menuitem">
    <?php
    if (isset($types) && is_array($types)) {
        foreach ($types as $key => $value) {
            ?>
                                                                                                                                                                                            <li ><a href="<?php echo base_url() . $value['link']; ?>" ><?php echo $value['name'] ?></a></li>
                                                                                                                                                                                            <li>|</li>
            <?php
        }
    }
    ?>
        </ul>
    </div-->
</div>
<div id="get-inspired-social-wrapper">
    <div id="get-inspired-social">
        <span></span>
        <a target="_blank" class="facebook" onclick="PopupCenter('http://www.facebook.com/sharer.php?u=<?php echo $url; ?>', 'myPop1', 500, 500);"></a>
        <a target="_blank" class="googleplus" onclick="PopupCenter('https://plus.google.com/share?url=<?php echo $url; ?>', 'myPop1', 500, 500);"></a>
        <a target="_blank" class="twitter" onclick="PopupCenter('http://twitter.com/share?url=<?php echo $url . '&amp;text=The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!'; ?>', 'myPop1', 500, 500);"></a>
        <a target="_blank" class="pinterest" onclick="PopupCenter('http://pinterest.com/pin/create/button/?url=<?php echo $url; ?>&amp;media=<?php echo $images[0]; ?>&amp;description=The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!', 'myPop1', 500, 500);"></a>
        <a target="_blank" class="tumblr" onclick="PopupCenter('https://www.tumblr.com/share/photo?source=<?php echo urlencode($images[0]); ?>&amp;caption=<?php echo urlencode('The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!'); ?>&amp;clickthru=<?php echo urlencode($url); ?>', 'myPop1', 500, 500);"></a>
        <a target="_blank" class="email"></a>
    </div>
</div>
<div id="get_inspired-wrapper">
    <div class="spinner" id="loader"></div>
    <div style="display: none;" id="get_inspired" class="container">
        <div id="get_inspired_slider">
            <ul class="get_inspired_slider">
                <?php if (is_array($images)) { ?>
                    <?php foreach ($images as $key => $value) { ?>
                        <li><img <?php echo (!$key) ? 'class="active"' : ''; ?> src="<?php echo $value ?>" alt="<?php echo @$alt; ?>" /></li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <div class="get_inspired_product_details">
            <h3 class="product-title"><?php echo @$item['shoe_name']; ?></h3>
            <h4 class="product-style"><?php echo @$item['last_name'] . '/' . $item['folder_name']; ?></h4>
            <h5 class="product-price">$<?php echo @$item['price']; ?></h5>
        </div>
        <div class="get_inspired_product_details shoesize">
            <div class="sizes">
                <div id="select_size">
                    <label>SELECT SIZE (US)</label>
                    <select name="select_size" id="select_left_size" tabindex="1" class="details_sizes">
                        <option value="">L</option>
                        <option value="5">5 </option>
                        <option value="5.5">5.5 </option>
                        <option value="6">6 </option>
                        <option value="6.5">6.5 </option>
                        <option value="7">7 </option>
                        <option value="7.5">7.5 </option>
                        <option value="8">8 </option>
                        <option value="8.5">8.5 </option>
                        <option value="9">9 </option>
                        <option value="9.5">9.5 </option>
                        <option value="10">10 </option>
                        <option value="10.5">10.5 </option>
                        <option value="11">11 </option>
                        <option value="11.5">11.5 </option>
                        <option value="12">12 </option>
                        <option value="12.5">12.5 </option>
                        <option value="13">13 </option>
                        <option value="13.5">13.5 </option>
                        <option value="14">14 </option>
                    </select>
                    <select name="select_size" id="select_right_size" tabindex="1"  class="details_sizes">
                        <option value="">R</option>
                        <option value="5">5 </option>
                        <option value="5.5">5.5 </option>
                        <option value="6">6 </option>
                        <option value="6.5">6.5 </option>
                        <option value="7">7 </option>
                        <option value="7.5">7.5 </option>
                        <option value="8">8 </option>
                        <option value="8.5">8.5 </option>
                        <option value="9">9 </option>
                        <option value="9.5">9.5 </option>
                        <option value="10">10 </option>
                        <option value="10.5">10.5 </option>
                        <option value="11">11 </option>
                        <option value="11.5">11.5 </option>
                        <option value="12">12 </option>
                        <option value="12.5">12.5 </option>
                        <option value="13">13 </option>
                        <option value="13.5">13.5 </option>
                        <option value="14">14 </option>
                    </select>
                    <span id="s_s_det" class="help_button">?</span>
                </div>
                <div id="select_width">
                    <label>SELECT WIDTH (US)</label>
                    <select  id="left_width"  class="details_sizes">
                        <option value="">L</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="EE">EE</option>
                        <option value="EEE">EEE</option>
                    </select>
                    <select  id="right_width"  class="details_sizes">
                        <option value="" >R</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                        <option value="EE">EE</option>
                        <option value="EEE">EEE</option>
                    </select>
                    <span id="s_w_det" class="help_button">?</span>
                </div>
            </div>
            <div id="custom_fitting_container">
                <li><label><input type="checkbox" id="custom_fitting_checkbox">Custom Fitting</label></li>
            </div>
            <?php if (strtolower($item['folder_name']) == strtolower($item['style_name'])) { ?> 
                <div id="enter-size" style="margin-top: 30px;  display: none;">
                    <div id="left-sizes">
                        <div class="group">
                            <label>LEFT</label>
                            <input type="text" placeholder="LENGTH (CM)"  id = "m_size_left" />
                            <input type="text" placeholder="WIDTH (CM)"  id = "m_width_left" />
                        </div>
                        <div class="group">
                            <label>LEFT</label>
                            <input type="text" placeholder="HEIGHT (CM)"  id = "m_girth_left" />
                            <input type="text" placeholder="INSTEP (CM)"  id = "m_instep_left" />
                            <span class="help_button">?</span>
                        </div>
                    </div>
                    <div id="right-sizes">
                        <div class="group">
                            <label>RIGHT</label>
                            <input type="text" placeholder="LENGTH (CM)" id = "m_size_right" />
                            <input type="text" placeholder="WIDTH (CM)" id = "m_width_right" />
                        </div>
                        <div class="group">
                            <label>RIGHT</label>
                            <input type="text" placeholder="HEIGHT (CM)" id = "m_girth_right" />
                            <input type="text" placeholder="INSTEP (CM)" id = "m_instep_right" />
                            <span class="help_button">?</span>  
                        </div>
                    </div>    
                </div>
            <?php } ?>
            <div>
                <?php if (strtolower($item['shoe_name']) !== 'milano' && $item['is_editable'] == '1') { ?>
                    <a class="flat_button " id="edit-costom-item" >Customize this shoe</a>
                <?php } ?>
                <a class="flat_button  " id="add-custom-item-to-cart" >Add to Cart</a>
                <input type="hidden" id="custom_collection_id" value="<?php echo $item['id']; ?>">
            </div>
        </div>
    </div>
    <div id="product-description" class="innergrid center <?php if ($item['description'] === "") echo "none"; ?>">
        <h2 class="product-title-large none"><?php echo @$item['shoe_name']; ?></h2>
        <h4 class="product-style none"><?php echo @$item['last_name'] . '/' . $item['style_name']; ?></h4>
        <p><?php echo @$item['description']; ?></p>
        <div>
            <div class="half left details">
                <?php if ($item['details'] !== "") { ?>
                    <div class="title">Details </div>
                    <ul>
                        <?php foreach (explode(',', $item['details']) as $value) { ?>
                            <?php echo (trim(str_replace("'", '', $value)) !== "Hand Welted") ? '<li>' . str_replace("'", '', $value) . '</li>' : ''; ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
            <div class="half left comes-with">
                <?php if ($item['comes_with'] !== "") { ?>
                    <div class="title">Comes with </div>
                    <ul>
                        <?php foreach (explode(',', $item['comes_with']) as $value) { ?>
                            <?php echo (trim(str_replace("'", '', $value)) !== "Shoe trees") ? '<li>' . str_replace("'", '', $value) . '</li>' : ''; ?>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div>
    <div id="bottom-slider-wrapper">
        <h3 class="product-title center">You will also love</h3>
        <div id="bottom_slider">
            <ul class="bottom_slider">
                <?php if (is_array($similar_products)) { ?>
                    <?php foreach ($similar_products as $item) { ?>
                        <li>                   
                            <a href="<?php echo base_url() . $item['link'] ?>">
                                <img src="<?php echo $item['images'][1]; ?>" alt="<?php echo $item['shoe_name']; ?>" />
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<div class="overlay"></div>
<div id="sizing-popup-wrapper" class="popup-wrapper">
    <div id="size-info">
        <span class="close"></span>
        <h1>Sizing Chart</h1>
        <p>Awl & Sundry shoes are true to fit. To get the right fit, please order the most frequently worn size.  For instance, if you wear a 10 for most of your shoes, we would recommend ordering a 10 with Awl & Sundry as well. 
            If you are unsure of your size, you can trace your foot on a blank piece of paper and send it to us at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a> and we will recommend you your size and last.</p>
        <table>
            <tr><th>US</th><th>CAN</th><th>UK</th><th>AUS</th></tr>
            <tr><td>5</td><td>5</td><td>4</td><td>4</td></tr>
            <tr><td>5&#189;</td><td>5&#189;</td><td>4&#189;</td><td>4&#189;</td></tr>
            <tr><td>6</td><td>6</td><td>5</td><td>5</td></tr>
            <tr><td>6&#189;</td><td>6&#189;</td><td>5&#189;</td><td>5&#189;</td></tr>
            <tr><td>7</td><td>7</td><td>6</td><td>6</td></tr>
            <tr><td>7&#189;</td><td>7&#189;</td><td>6&#189;</td><td>6&#189;</td></tr>
            <tr><td>8</td><td>8</td><td>7</td><td>7</td></tr>
            <tr><td>8&#189;</td><td>8&#189;</td><td>7&#189;</td><td>7&#189;</td></tr>
            <tr><td>9</td><td>9</td><td>8</td><td>8</td></tr>
            <tr><td>9&#189;</td><td>9&#189;</td><td>8&#189;</td><td>8&#189;</td></tr>
            <tr><td>10</td><td>10</td><td>9</td><td>9</td></tr>
            <tr><td>10&#189;</td><td>10&#189;</td><td>9&#189;</td><td>9&#189;</td></tr>
            <tr><td>11</td><td>11</td><td>10</td><td>10</td></tr>
            <tr><td>11&#189;</td><td>11&#189;</td><td>10&#189;</td><td>10&#189;</td></tr>
            <tr><td>12</td><td>12</td><td>11</td><td>11</td></tr>
            <tr><td>12&#189;</td><td>12&#189;</td><td>11&#189;</td><td>11&#189;</td></tr>
            <tr><td>13</td><td>13</td><td>12</td><td>12</td></tr>
            <tr><td>13&#189;</td><td>13&#189;</td><td>12&#189;</td><td>12&#189;</td></tr>
            <tr><td>14</td><td>14</td><td>13</td><td>13</td></tr>
        </table>
    </div>
</div>
<div id="width-popup-wrapper" class="popup-wrapper">
    <div id="width-info">
        <span class="close"></span>
        <h1>Width Chart</h1>
        <p>
            Awl & Sundry shoes are defaulted to width D. If you have a wider foot, we would strongly recommend tracing your foot on a blank piece of paper and send it to us at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a> and we will recommend you your width and last.
        </p>
        <table>
            <tr><td>D</td><td>Normal</td></tr>
            <tr><td>E</td><td>Wide</td></tr>
            <tr><td>EE</td><td>Extra Wide</td></tr>
            <tr><td>EEE</td><td>Extra Extra Wide</td></tr>
        </table>
    </div>
</div>
<div id="email-popup-wrapper" class="popup-wrapper" >
    <div id="email-info">
        <span class="close"></span>
        <h1>Email This Design</h1>
        <span id="errors"></span>
        <div class="content">
            <div class="half left">
                <div class="content">
                    <img src="<?php echo $images[0]; ?>" alt="<?php echo @$item['shoe_name']; ?>"/>
                </div>
            </div>
            <div class="half left">
                <div class="content">
                    <form >
                        <table>
                            <tr>
                                <td>
                                    <label>From</label>

                                    <input type="email" class="from_email" required placeholder="Your Email" />
                                    <span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>To</label>

                                    <input type="text" class="to_emails" required  placeholder="Your friends' Emails (Seperated by commas)"/>
                                    <span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Message</label>

                                    <textarea name="message" class="message" placeholder="Your Message (320 Characters limit)"></textarea>
                                    <span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="button" value="Send it" id="send-email"/>
                                    <input type="hidden" value="<?php echo $item['id']; ?>" class="shoe_id"/>
                                    <input type="hidden" value="<?php echo $item['id']; ?>" class="collection"/>
                                    <input type="hidden" value="<?php echo $images[0]; ?>" class="image"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            <div id="share_email_success" class="none">Message sent successfully</div>
        </div>
    </div>
</div>
<div id="measure-popup-wrapper" class="popup-wrapper">
    <div id="measure-info">
        <span class="close"></span>
        <h1>How to measure your feet</h1>
        <div>
            <ul>
                <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/1.jpg"/></li>
                <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/2.jpg"/></li>
                <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/3.jpg"/></li>
                <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/4.jpg"/></li>
                <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/5.jpg"/></li>
                <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/6.jpg"/></li>
                <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/7.jpg"/></li>
                <li><img src="<?php echo base_url(); ?>assets/css/images/measure-feet/8.jpg"/></li>
            </ul>
        </div>
    </div>
</div>    

<?php $this->load->view('includes/footer'); ?>
