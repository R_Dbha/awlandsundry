<div class="account_info">
    <h1 class="username"><?php echo @$user['first_name'] . ' ' . @$user['last_name']; ?></h1>
    <div class="dasboard-right-content profile">
        <div class="dashboard-content-heading-wrapper">
            <div class="dashboard-content-heading left">
                <h3>PROFILE</h3>
            </div>
            <div class="dashboard-content-heading right">
                <span><a href="#edit_accountinfo/acinfo">EDIT</a></span>
            </div>
        </div>
        <div id="recent-orders-content">
            <ul>
                <li><label>FIRST NAME :</label><?php echo @$user['first_name']; ?></li>
                <li><label>LAST NAME :</label><?php echo @$user['last_name'] ?></li>
                <li><label>EMAIL ADDRESS :</label><?php echo @$user['email']; ?></li>
                <li><label>PASSWORD :</label><a href="#reset_password">Reset Password</a></li>
            </ul>
        </div>
    </div>
</div>