<?php
	require_once('includes/header.php');
?>
<div id="get-inspired-menu-wrapper">
	<div id="menu" class="container get_inspired">
		<ul id="menuitem">
			<li ><a href="#payments">CLASSIC</a></li>
			<li>|</li>
			<li ><a href="#">FALL STYLE</a></li>
			<li>|</li>
			<li ><a href="#">ON THE STREET</a></li>
		</ul>
	</div>
</div>
<div id="get-inspired-social-wrapper">
    <div id="get-inspired-social">
    <span>SHARE</span>
        <a href="#" target="_blank" class="facebook"></a>
        <a href="#" target="_blank" class="googleplus"></a> 
        <a href="#" target="_blank" class="twitter"></a>
        <a href="#" target="_blank" class="pinterest"></a>
        <a href="#" target="_blank" class="email"></a>
              
    </div>
</div>
<div id="get_inspired-wrapper">
	<div id="get_inspired" class="container">
		<div id="get_inspired_slider">
            <ul class="get_inspired_slider">
                <li><img src="css/images/slider/inspired_slider.jpg" /></li>
                <li><img src="css/images/slider/inspired_slider.jpg" /></li>
                <li><img src="css/images/slider/inspired_slider.jpg" /></li>
                <li><img src="css/images/slider/inspired_slider.jpg" /></li>
            </ul>
		</div>
		<div class="get_inspired_product_details">
			<h3 class="product-title">PABLO</h3>
			<h4 class="product-style">THE TOWNSEND /OXFORD</h4>
			<h5 class="product-price">$250</h5>
		</div>
		<div class="get_inspired_product_details shoesize">
            <select name="select_size" id="select_left_size" tabindex="1">
                <option value="">Select Left Size Shoe</option>
                 <option value="5">5 </option>
                <option value="5.5">5.5 </option>
                <option value="6">6 </option>
                <option value="6.5">6.5 </option>
                <option value="7">7 </option>
                <option value="7.5">7.5 </option>
                <option value="8">8 </option>
                <option value="8.5">8.5 </option>
                <option value="9">9 </option>
                <option value="9.5">9.5 </option>
                <option value="10">10 </option>
                <option value="10.5">10.5 </option>
                <option value="11">11 </option>
                <option value="11.5">11.5 </option>
                <option value="12">12 </option>
                <option value="12.5">12.5 </option>
                <option value="13">13 </option>
                <option value="13.5">13.5 </option>
                <option value="14">14 </option>
            </select>
            <select name="select_size" id="select_right_size" tabindex="1">
                <option value="">Select Right Size Shoe</option>
                <option value="5">5 </option>
                <option value="5.5">5.5 </option>
                <option value="6">6 </option>
                <option value="6.5">6.5 </option>
                <option value="7">7 </option>
                <option value="7.5">7.5 </option>
                <option value="8">8 </option>
                <option value="8.5">8.5 </option>
                <option value="9">9 </option>
                <option value="9.5">9.5 </option>
                <option value="10">10 </option>
                <option value="10.5">10.5 </option>
                <option value="11">11 </option>
                <option value="11.5">11.5 </option>
                <option value="12">12 </option>
                <option value="12.5">12.5 </option>
                <option value="13">13 </option>
                <option value="13.5">13.5 </option>
                <option value="14">14 </option>
            </select>            

            <a class="flat_button">Add to Cart</a>

		</div>
		<div id="product-description" class="innergrid center">
			<h2 class="product-title-large">PABLO</h2>
            <h4 class="product-style">THE TOWNSEND /OXFORD</h4>
            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.</p>
		</div>

	</div>
	<div id="bottom-slider-wrapper">
	<h3 class="product-title center">You might like these</h3>
	    <div id="bottom_slider" style="background:url(css/images/bottom_pattern.jpg)">
            <ul class="bottom_slider">
                <li><img src="css/images/slider/1.png" /></li>
                <li><img src="css/images/slider/2.png" /></li>
                <li><img src="css/images/slider/1.png" /></li>
                <li><img src="css/images/slider/2.png" /></li>
            </ul>
		</div>
	</div>
</div>
<?php
	require_once('includes/footer.php');
?>