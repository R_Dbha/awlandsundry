<?php require_once('includes/header.php'); ?>
<div id="cart-wrapper">
    <div id="cart" class="container">
        <h3 class="page-title">
            <span>SHOPPING CART</span>
        </h3>
        <?php if ($klarna_error != "") { ?>
            <div style="color:red; text-align: center;"><?php echo $klarna_error; ?></div>
        <?php } ?>
        <div id="cart-item-container">
            <div id="cart-item-top">
                <div id="cart-products" class="left"><h4>PRODUCTS</h4></div>
                <div id="cart-size" class="left"><h4>SIZES</h4></div>
                <div id="cart-quantity" class="left"><h4>QUANTITY</h4></div>
                <div id="cart-price" class="left"><h4>PRICE</h4></div>
                <div id="cart-actions" class="left"><a href="javascript:;">EDIT</a>/<a href="javascript:;">REMOVE</a></div>
            </div>
            <?php if (is_array($items)) { ?>
                <?php foreach ($items as $key => $item) { ?>
                    <?php if (strtolower($item['name']) !== "gift card") { ?>
                        <?php $shoe = $item['shoe']; ?>
                        <div class="cart-items">
                            <div class="cart-products left">
                                <div class="cart-product-image left">
                                    <div class="shoe">
                                        <?php if (isset($shoe['image'])) { ?>
                                            <img  src="<?php echo $shoe['image']; ?>" />
                                        <?php } else { ?>
                                            <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['base']; ?>" />
                                            <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['toe']; ?>" class="float-top" />
                                            <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['eyestay']; ?>" class="float-top" />
                                            <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['vamp']; ?>" class="float-top" />
                                            <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['foxing']; ?>" class="float-top" />
                                            <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['stitch']; ?>" class="float-top" />
                                            <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['lace']; ?>" class="float-top" />
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="cart-product-details left">
                                    <p class="cart-product-name"><span><?php echo ($shoe['last_style']['last_name']==="The Ready To Wear")?"The Blacklist Collection":$shoe['last_style']['last_name']; ?></span></p>
                                    <p class="cart-product-style"><span><?php echo ucfirst($shoe['last_style']['style_name']); ?></span></p>
                                    <?php if (isset($shoe['sole'])) { ?>
                                        <p class="cart-product-style" <?php echo (!isset($shoe['sole']['soleName']) && isset($shoe['sole']['colorName'])) ? 'style="display: none;"' : '' ?> ><span><?php echo 'Sole : ' . $shoe['sole']['soleName'] . " / " . $shoe['sole']['colorName']; ?> </span></p>
                                        <p class="cart-product-style" <?php echo (!isset($shoe['sole']['stitchName'])) ? 'style="display: none;"' : '' ?> ><span><?php echo 'Welt : ' . $shoe['sole']['stitchName']; ?> </span></p>
                                    <?php } ?>
                                    <?php if (isset($shoe['shoetree'])) { ?>
                                        <p class="cart-product-style"><span> Shoetree : <?php echo ($shoe['shoetree'] == 'true') ? 'Yes' : 'No'; ?> </span></p>
                                    <?php } ?>
                                    <p class="cart-product-style" <?php echo (!isset($shoe['patina'])) ? 'style="display: none;"' : '' ?> ><span style="color: #C72733">(Patina)</span></p>
                                </div>
                            </div>
                            <div class="cart-size left center">
                                <p class="size-left"><span>LEFT SHOE : <?php echo $shoe['size']['left']['text']; ?></span></p>
                                <p class="size-right"><span>RIGHT SHOE : <?php echo $shoe['size']['right']['text']; ?></span></p>
                                <p class="width-left"><span>LEFT WIDTH : <?php echo $shoe['size']['left']['width']; ?></span></p>
                                <p class="width-right"><span>RIGHT WIDTH : <?php echo $shoe['size']['right']['width']; ?></span></p>
                            </div>
                            <div class="cart-quantity left center">
                                <p><input type="text" class="quantity"  value="<?php echo $item['quantity']; ?>" /></p>
                                <p><span>UPDATE</span></p>
                            </div>
                            <div class="cart-price left center">
                                <p><span>$<?php echo $item['price']; ?></span></p>
                            </div>
                            <div class="cart-actions left">
                                <?php if (isset($shoe['img_base'])) { ?><a class="left edit"></a><?php } ?>
                                <a class="right delete"></a>
                            </div>
                            <input type="hidden" class="cartId" value="<?php echo $item['row_id']; ?>" />
                        </div>
                    <?php } else { ?>
                        <div class="cart-items">
                            <div class="cart-products left">
                                <div class="cart-product-image left">
                                    <div class="gift_card"><img  src="<?php echo base_url() . 'assets/css/images/logo.png'; ?>" ></div>
                                </div>
                                <div class="cart-product-details left">
                                    <p class="cart-product-name">
                                        <span>From: <?php echo $item['giftcard']['from']; ?> </span>
                                    </p>
                                    <p class="cart-product-style">
                                        <span>To: <?php echo $item['giftcard']['to']; ?> </span>
                                    </p>
                                </div>
                            </div>
                            <div class="cart-size left center"></div>
                            <div class="cart-quantity left center">
                                <p><input type="text" class="quantity"  value="<?php echo $item['quantity']; ?>" /></p>
                                <p><span>UPDATE</span></p>
                            </div>
                            <div class="cart-price left center">
                                <p><span>$<?php echo $item['price']; ?></span></p>
                            </div>
                            <div class="cart-actions left">
                                <?php if (isset($shoe['img_base'])) { ?><a class="left edit"></a><?php } ?>
                                <a class="right delete"></a>
                            </div>
                            <input type="hidden" class="cartId" value="<?php echo $item['row_id']; ?>" />
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            <div id="cart-code-container-wrapper">
                <div id="cart-code-container">
                    <div id="promocode" class="left">
                        <input class="left" type="text" placeholder="Promocode" id="code" />
                        <input type="submit" value="APPLY CODE" id="apply_promocode" />
                    </div>
                    <div id="gift-card" class="right">
                        <input class="left" id="cer_code" type="text" placeholder="Gift Card Number #" />
                        <input type="submit" value="REDEEM GIFT CARD" onclick="javascript:return processGiftCard();" />
                    </div>
                </div>
            </div>
            <?php if ($show_comment_box) { ?>
                <div id="cart-code-container-wrapper">
                    <div id="cart-code-container">
                        <div >
                            <textarea class="left" placeholder="Comments" id="comments"></textarea>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div id="cart-total-wrapper">
                <div id="cart-total">
                    <?php if (@$balance > 0) { ?>
                        <div id="total-balance-text" style="width:170px;" class="left">
                            <h1>STORE CREDIT</h1>
                        </div>
                        <div id="total-balance" class="right">
                            <h1>$<?php echo @$balance; ?></h1>
                        </div>
                        <P></p>
                    <?php } ?>
                    <?php if (@$promo_discount_amt > 0) { ?>
                        <div id="total-promo-balance-text" style="width:170px;" class="left">
                            <h1>PROMO DISCOUNT</h1>
                        </div>
                        <div id="total-promo-balance" class="right">
                            <h1>$<?php echo @$promo_discount_amt; ?></h1>
                        </div>
                        <P></p>
                    <?php } else { ?>
                        <label id="lblDiscount" ></label>
                    <?php } ?>
                    <div id="total-text" class="left">
                        <h1>SUBTOTAL</h1>
                    </div>
                    <div id="total-price" class="right">
                        <h1>$<?php echo $subtotal; ?></h1>
                    </div>
                </div>
            </div>
            <div id="cart-checkout-wrapper">
                <div id="cart-checkout">
                    <input type="submit" id="cart-continue_btn"  value="CONTINUE SHOPPING" />
                    <input type="submit" id="cart-checkout_btn" class="right" value="CHECKOUT" />
                </div>
            </div>		
        </div>
    </div>
</div>		
<?php require_once('includes/footer.php'); ?>