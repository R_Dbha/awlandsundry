<div class="col-md-6 quick-view">
    <!-- Success box -->
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Order Details</h3>
            <div class="box-tools pull-right"></div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">Order Id :</div>
                <div class="col-md-8"><?php echo $order_id; ?></div>
            </div>

            <?php if ($is_rework != 'No') { ?>
                <div class="row">
                    <div class="col-md-4">Rework Details:</div>
                    <div class="col-md-8"><?php echo $is_rework; ?></div>
                </div>
            <?php } ?>

            <div class="row">
                <div class="col-md-4">Invoice No :</div>
                <div class="col-md-8"><?php echo $invoice_no; ?></div>
            </div>
            <div class="row">
                <div class="col-md-4">Style :</div>
                <div class="col-md-8"><?php echo $style_name; ?></div>
            </div>
            <div class="row">
                <div class="col-md-4">Last :</div>
                <div class="col-md-8"><?php echo $last; ?></div>
            </div>
            <?php if ($promocode_id != '0') { ?>
                <div class="row">
                    <div class="col-md-4">Promo code:</div>
                    <div class="col-md-8"><?php echo $promocode; ?></div>
                </div>
            <?php } ?>

            <?php if ($store_credit_trans_id != '') { ?>
                <div class="row">
                    <div class="col-md-4">Gift Card Amount:</div>
                    <div class="col-md-8"><?php echo $card_amount; ?></div>
                </div>
            <?php } ?>
            <?php if ($discount_per != '') { ?>
                <div class="row">
                    <div class="col-md-4">Discount Percentage:</div>
                    <div class="col-md-8"><?php echo $discount_per; ?></div>
                </div>
            <?php } ?>
            <?php if ($discount_amount != '' && $discount_amount != '0') { ?>
                <div class="row">
                    <div class="col-md-4">Discount Amount:</div>
                    <div class="col-md-8"><?php echo $discount_amount; ?></div>
                </div>
            <?php } ?>
            <?php if ($manufacturing_cost != '') { ?>
                <div class="row">
                    <div class="col-md-4">Manufacturing Cost:</div>
                    <div class="col-md-8"><?php echo $manufacturing_cost; ?></div>
                </div>
            <?php } ?>
            <?php if ($shipping_cost != '') { ?>
                <div class="row">
                    <div class="col-md-4">Shipping Cost:</div>
                    <div class="col-md-8"><?php echo $shipping_cost; ?></div>
                </div>
            <?php } ?>

            <?php if ($shipped_date != '') { ?>
                <div class="row">
                    <div class="col-md-4">Shipped Date:</div>
                    <div class="col-md-8"><?php echo $shipped_date; ?></div>
                </div>
            <?php } ?>

            <?php if ($day_diff != '') { ?>
                <div class="row">
                    <div class="col-md-4">Total days taken:</div>
                    <div class="col-md-8"><?php echo $day_diff; ?></div>
                </div>
            <?php
            }
            $tracking_no = get_string_inbetween($status_desc, 'TrackingNo:', ', Estimated');
            if ($tracking_no != '') {
                ?>

                <div class="row">
                    <div class="col-md-4">Tracking Code:</div>
                    <div class="col-md-8"><?php echo $tracking_no; ?></div>
                </div>
                <?php
            }
            ?>


            <div class="row">
                <div class="col-md-4">Shipping Location:</div>
                <div class="col-md-8"><?php echo $location; ?></div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>