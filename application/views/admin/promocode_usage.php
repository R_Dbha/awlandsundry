<?php ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">×</button>
    <h4 class="modal-title">
        <i class="fa  fa-folder-open-o"></i> Assign PromoCode
    </h4>
</div>
<div class="modal-body">		
    <div class="row seperation">

        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <div class="col-md-12 no-padding">
                    <label>PromoCode</label>
                    <div><?php echo @$promocode['promocode']; ?></div>
                </div>

            </div>
        </div>
        <?php if ($promocode['discount_amount'] != 0) { ?>
            <div class="col-md-4">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <label>Discount Amount</label>
                        <div><?php echo @$promocode['discount_amount']; ?></div>
                    </div>

                </div>
            </div>
        <?php } ?>
        <?php if ($promocode['discount_per'] != 0) { ?>
            <div class="col-md-4">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <label>Discount Percentage</label>
                        <div><?php echo @$promocode['discount_per']; ?></div>
                    </div>

                </div>
            </div>
        <?php } ?>
        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <div class="col-md-12 no-padding">
                    <label>Expiry Date</label>
                    <div><?php echo @date('Y-m-d', strtotime($promocode['expiry_date'])); ?></div>
                </div>
            </div>
        </div>
    </div>


    <div class="row seperation" >

        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <label>Available To All?</label>
                <div ><?php echo $promocode['is_common'] == 1 ? 'Yes' : 'No'; ?></div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <label>First Use Only?</label>
                <div><?php echo $promocode['one_time'] == 1 ? 'Yes' : 'No'; ?></div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <label>Unlimited?</label>
                <div ><?php echo $promocode['unlimited'] == 1 ? 'Yes' : 'No'; ?></div>

            </div>
        </div>
    </div>
    <div class="row seperation" >
        <div class="col-md-12">
            <table id="promocode_usage"  class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Order Id</th>
                        <th>Invoice Number</th>
                        <th>Email</th>
                        <th>Gross Amount</th>
                        <th>Discount Amount</th>
                    </tr>
                <thead>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer clearfix">

  

    <button type="button" data-dismiss="modal"
            class="btn btn-warning pull-right">
        <i class="fa fa-times"></i> Close
    </button>
</div>
<?php ?>