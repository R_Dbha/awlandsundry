<ul class="shoe_images">
    <?php if (is_array($collection)) { ?>
        <?php foreach ($collection as $key => $value) { ?>
            <li>
                <img src="<?php echo $value['image'][0]; ?>" >
                <div class="shoe-details">
                    <h4 class="center"><?php echo ucwords($value['last_name'] . ' / ' . $value['style_name'] . ((isset($value['shoe_name']) && $value['shoe_name'] != '' && $value['shoe_name'] != NULL) ? ' - ' . $value['shoe_name'] : '')); ?></h4>
                    <h5 class="center">$<span class="price"><?php echo $value['price']; ?></span></h5>
                    <input type="hidden" class="shoe_design_id" value="<?php echo $value['shoe_design_id']; ?>"/>
                    <input type="hidden" class="custom_collection_id" value="<?php echo @$value['custom_collection_id']; ?>"/>
                    <button class="pull-right btn btn-success select-and-proceed" >
                        Select and Proceed <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>
            </li>
        <?php } ?>
    <?php } ?>
</ul>