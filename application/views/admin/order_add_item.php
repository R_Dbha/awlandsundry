<?php $k = rand(0, 100) ?>

<div class="order_add_item" id="add-<?php echo $k; ?>">
    <div class="col-md-3">
        <div class="box box-solid">
            <div class="box-header">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Choose Item</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">Source</div>
                    <div class="col-md-7">
                        <select class="form-control show-selected">
                            <option value="">Select</option>
                            <option value="upload_images">Upload Images</option>
                            <option value="custom_collection">Custom Collection</option>
                            <option value="ready_wear">Ready To Wear</option>
                            <option value="saved_shoes">Saved Shoes</option>
                        </select>
                    </div>
                </div>
                <br />
                <div class="row upload_images group" style="display: none;">
                    <div class="col-md-12">
                        <div class="row seperation">
                            <div class="col-md-3">Images</div>
                            <div class="col-md-7">
                                <input type="hidden" class="attachment_id" id="attachment_id<?php echo $k; ?>" />
                                <div id="file-uploader<?php echo $k; ?>">
                                    <noscript>
                                    <p>Please enable JavaScript to use file uploader.</p>
                                    <!-- or put a simple form for upload here -->
                                    </noscript>
                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="row seperation">
                            <div class="col-md-3">Style</div>
                            <div class="col-md-7">
                                <select class="form-control u_style">
                                    <?php echo @$style; ?>
                                </select>
                            </div>
                        </div>
                        <br />
                        <div class="row seperation">
                            <div class="col-md-3">Last</div>
                            <div class="col-md-7">
                                <select class="form-control u_last">
                                    <?php echo @$last; ?>
                                </select>
                            </div>
                        </div>
                        <br />
                        <div class="row seperation">
                            <div class="col-md-3">Price</div>
                            <div class="col-md-7">
                                <input type="text" class="form-control u_price" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row saved_shoes group" style="display: none;">
                    <div class="col-md-3">User</div>
                    <div class="col-md-7">
                        <select class="form-control user_list">
                            <?php echo @$users; ?>
                        </select>
                    </div>
                </div>
                <div class="row custom_collection group" style="display: none;">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">Style</div>
                            <div class="col-md-7">
                                <select class="form-control style">
                                    <?php echo @$styles; ?>
                                </select>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3"> Type</div>
                            <div class="col-md-7">
                                <select class="form-control type">
                                    <?php echo @$types; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ready_wear group" style="display: none;">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">Style</div>
                            <div class="col-md-7">
                                <select class="form-control rtw_style">
                                    <?php echo @$rtw_styles; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-solid">
            <div class="box-header">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title"> Item(s)</h3>
            </div>
            <div class="box-body collection"></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row item-details"></div>
    </div>
</div>

<script>
    var u_slider<?php echo $k; ?>;
    function createUploader<?php echo $k; ?>() {
        var uploader = new qq.FileUploader({
            multiple: true,
            // allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
            element: document.getElementById('file-uploader<?php echo $k; ?>'),
            action: '<?php echo base_url(); ?>orders/upload',
            debug: false,
            onComplete: function (id, fileName, response) {
                var attachment_id = $('#attachment_id<?php echo $k; ?>').val() == '' ? '' : $('#attachment_id<?php echo $k; ?>').val() + ',';
                $('#attachment_id<?php echo $k; ?>').val(attachment_id + response.attachment_id);
                if ($('#add-<?php echo $k; ?>').find('.shoe_images').length == 0) {
                    $('#add-<?php echo $k; ?>').find('.collection').html('<ul class="shoe_images"><li><img src = "' + base_url + response.file_name + '" ></li></ul>'
                            + '<div class="save-button"><button class="pull-right btn btn-success save-custom-item" >'
                            + ' Proceed <i class="fa fa-arrow-circle-right"></i>'
                            + '</button></div>');
                    setTimeout(function () {
                        u_slider<?php echo $k; ?> = $('#add-<?php echo $k; ?>').find('.collection .shoe_images').bxSlider({
                            infiniteLoop: true,
                            preloadImages: 'all',
                            controls: true,
                            prevText: '<i class="fa fa-arrow-left"></>',
                            nextText: '<i class="fa fa-arrow-right"></>',
                        });
                    }, 1);
                } else {
                    $('#add-<?php echo $k; ?>').find('.collection .shoe_images').append('<li><img src = "' + base_url + response.file_name + '" ></li>');
                    u_slider<?php echo $k; ?>.reloadSlider();
                }
            },
        });
    }
    
    $(document).ready(function () {
        createUploader<?php echo $k; ?>();
    });
</script>