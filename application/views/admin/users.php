<?php $this->load->view('includes/admin_header'); ?>


<section class="content-header">
    <h1>
        Users <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Users</li>
    </ol>
</section>

<section id="content" class="content" style="background-color:#FFFFFF;">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <div class="overlay"></div>
                <div class="loading-img"></div>


                <div class="box-header">
                    <h3 class="box-title">Users</h3>

                    <div class="box-tools pull-right">
                        <a href="<?php echo base_url(); ?>admin/adduser">
                            <button type="button" class="btn btn-primary btn-xs">
                                <i class="fa fa-pencil"> Create New Users</i>
                            </button>
                        </a>
                    </div>
                </div>

                <div class="box-body">

                    <table id="userLists" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php if (isset($users) && sizeof($users)) { ?>
                                <?php foreach ($users as $key => $user) { ?>
                                <tr>
                                    <td><?php echo $user['user_id']; ?></td>
                                    <td><?php echo $user['first_name']; ?></td>
                                    <td><?php echo $user['last_name']; ?></td>
                                    <td><?php echo $user['email']; ?></td>
                                    <td><?php echo isset($roles[$user['user_type_id']]) ? $roles[$user['user_type_id']] : '-'; ?></td>
                                    <td>
                                        <a class="btn btn-success" href="<?php echo base_url() . 'admin/edituser/' . $user['user_id'] ?>"> <i class="fa fa-pencil"></i> Edit</a>
                                        <a class="btn btn-danger delUser" data-id="<?php echo $user['user_id'] ?>" > <i class="fa fa-trash-o"></i> Delete</a>
                                    </td>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
</script>



<?php
$this->load->view('includes/admin_footer');
?>