<div class="row seperation">
    <div class="col-md-4 right">Name</div>
    <div class="col-md-6">
        <div class="row" style="margin: 0px;">
            <div class="col-md-7 no-padding">
                <input  type="text" id="billing_first_name"
                       class="form-control"
                       value="<?php echo $billing[0]['first_name']; ?>" />
            </div>
            <div class="col-md-5 no-padding ">
                <input  type="text" id="billing_last_name"
                       class="form-control"
                       value="<?php echo $billing[0]['last_name']; ?>" />
            </div>
        </div>
    </div>
</div>

<div class="row seperation">
    <div class="col-md-4 right">Address 1</div>
    <div class="col-md-6">
        <input type="text" id="billing_address1" class="form-control"
               value="<?php echo $billing[0]['address1']; ?>" />
    </div>
</div>
<div class="row seperation">
    <div class="col-md-4 right">Address 2</div>
    <div class="col-md-6">
        <input type="text" id="billing_address2" class="form-control"
               value="<?php echo $billing[0]['address2']; ?>" />
    </div>
</div>
<div class="row seperation">
    <div class="col-md-4 right">City</div>
    <div class="col-md-6">
        <input type="text" id="billing_city" class="form-control"
               value="<?php echo $billing[0]['city']; ?>" />
    </div>
</div>
<div class="row seperation">
    <div class="col-md-4 right">Country</div>
    <div class="col-md-6">
        <input type="text" id="billing_country" class="form-control"
               value="<?php echo $billing[0]['country']; ?>" />
    </div>
</div>
<div class="row seperation">
    <div class="col-md-4 right">State</div>
    <div class="col-md-6">
        <input type="text" id="billing_state" class="form-control"
               value="<?php echo $billing[0]['state']; ?>" />
    </div>
</div>
<div class="row seperation">
    <div class="col-md-4 right">Zipcode</div>
    <div class="col-md-6">
        <input type="text" id="billing_zipcode" class="form-control"
               value="<?php echo $billing[0]['zipcode']; ?>" />
    </div>
</div>
