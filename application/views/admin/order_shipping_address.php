<div class="row seperation">
	<div class="col-md-4 right">Name</div>
	<div class="col-md-6">
		<div class="row" style="margin: 0px;">
			<div class="col-md-7 no-padding">
				<input type="text" id="shipping_firstname" class="form-control"
					value="<?php echo $shipping[0]['firstname']; ?>" />
			</div>
			<div class="col-md-5 no-padding ">
				<input type="text" id="shipping_lastname" class="form-control"
					value="<?php echo $shipping[0]['lastname']; ?>" />
			</div>
		</div>
	</div>
</div>

<div class="row seperation">
	<div class="col-md-4 right">Address 1</div>
	<div class="col-md-6">
		<input type="text" id="shipping_address1" class="form-control"
			value="<?php echo $shipping[0]['address1']; ?>" />
	</div>
</div>
<div class="row seperation">
	<div class="col-md-4 right">Address 2</div>
	<div class="col-md-6">
		<input type="text" id="shipping_address2" class="form-control"
			value="<?php echo $shipping[0]['address2']; ?>" />
	</div>
</div>
<div class="row seperation">
	<div class="col-md-4 right">City</div>
	<div class="col-md-6">
		<input type="text" id="shipping_city" class="form-control"
			value="<?php echo $shipping[0]['city']; ?>" />
	</div>
</div>
<div class="row seperation">
	<div class="col-md-4 right">Country</div>
	<div class="col-md-6">
		<input type="text" id="shipping_country" class="form-control"
			value="<?php echo $shipping[0]['country']; ?>" />
	</div>
</div>
<div class="row seperation">
	<div class="col-md-4 right">State</div>
	<div class="col-md-6">
		<input type="text" id="shipping_state" class="form-control"
			value="<?php echo $shipping[0]['state']; ?>" />
	</div>
</div>
<div class="row seperation">
	<div class="col-md-4 right">Zipcode</div>
	<div class="col-md-6">
		<input type="text" id="shipping_zipcode" class="form-control"
			value="<?php echo $shipping[0]['zipcode']; ?>" />
	</div>
</div>
<div class="row seperation">
	<div class="col-md-4 right">Phone</div>
	<div class="col-md-6">
		<input type="text" id="shipping_telephone" class="form-control"
			value="<?php echo $shipping[0]['telephone']; ?>" />
	</div>
</div>
