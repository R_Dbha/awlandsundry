<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>AWL&SUNDRY Admin</title>

        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <!-- Colour Schemes
  
        Default colour scheme is green. Uncomment prefered stylesheet to use it.
        
        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
        
        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
 
        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.3.2.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript">
                function viewOrderDetails(orderId)
                {
                    window.open("<?php echo base_url() . 'index.php/admin/detail_view/' ?>"+orderId);
                }
        </script>
        <!-- Internet Explorer .png-fix -->

        <!--[if IE 6]>
                <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('.png_bg, img, li');
                </script>
        <![endif]-->

    </head>

    <body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">AWL Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src='<?php echo base_url() . "assets/img/logo.png" ?>' alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a>
                        <br />
                        <!--a href="<?php echo base_url() . 'index.php' ?>" title="View the Site">View the Site</a--> 
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        

                   <?php $this->load->view('admin/menu'); ?>

                    <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                        <h3>3 Messages</h3>

                        <p>
                            <strong>17th May 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br />
                            Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">

                            <h4>New Message</h4>

                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>

                            <fieldset>

                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>

                                <input class="button" type="submit" value="Send" />

                            </fieldset>

                        </form>

                    </div> <!-- End #messages -->

                </div></div> <!-- End #sidebar -->

            <div id="main-content"> <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>

                <!-- Page Head -->
                <h2>Welcome <?php echo $user['first_name']; ?></h2>
                <div>

                </div>
                <!--p id="page-intro">What would you like to do?</p-->

                <ul class="shortcut-buttons-set">

<!--li><a class="shortcut-button new-article" href="#"><span class="png_bg">
        Write an Article
</span></a></li>

<li><a class="shortcut-button new-page" href="#"><span class="png_bg">
        Create a New Page
</span></a></li>

<li><a class="shortcut-button upload-image" href="#"><span class="png_bg">
        Upload an Image
</span></a></li>

<li><a class="shortcut-button add-event" href="#"><span class="png_bg">
        Add an Event
</span></a></li>

<li><a class="shortcut-button manage-comments" href="#messages" rel="modal"><span class="png_bg">
        Open Modal
</span></a></li-->

                </ul><!-- End .shortcut-buttons-set -->

                <div class="clear"></div> <!-- End .clear -->

                <div class="content-box"><!-- Start Content Box -->

                    <div class="content-box-header">

                        <h3>Payment Details</h3>

                        <!--ul class="content-box-tabs">
                                <li><a href="#tab1" class="default-tab">Table</a></li> 
                                <li><a href="#tab2">Forms</a></li>
                        </ul-->

                        <div class="clear"></div>

                    </div> <!-- End .content-box-header -->

                    <div class="content-box-content">

                        <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                            <div class="notification attention png_bg">
                                    <!--a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a-->
                                <!--div>
                                        This is a Content Box. You can put whatever you want in it. By the way, you can close this notification with the top-right cross.
                                </div-->
                            </div>

                            <form method="POST" action="<?php echo base_url() . 'index.php/admin/save_payment' ?>" >

                                <table id="grid" >




                                    <tbody>
                                        <tr>
                                            <td width="150">
                                                Order Id : 
                                            </td>
                                            <td>
                                                <a href="javascript:viewOrderDetails(<?php echo $payment_details['order_id']; ?>)" ><?php echo $payment_details['order_id']; ?></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="150">
                                                Invoice No : 
                                            </td>
                                            <td>
                                                <?php echo $payment_details['invoice_no']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="150">
                                                Vendor Name : 
                                            </td>
                                            <td>
                                                <?php echo $payment_details['first_name']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Order Amount :  
                                            </td>
                                            <td>
                                                <?php
                                                $disc = ($payment_details['gross_amt'] * $payment_details['discount_perc']) / 100;
                                                $payment_details['gross_amt'] = $payment_details['gross_amt'] - $disc;
                                                echo $payment_details['gross_amt'] + $payment_details['tax_amount'];
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Manufacturing Cost :
                                            </td>
                                            <td>
                                                <?php echo form_error('manufacturing_cost'); ?>
                                                <input type ="text"  id = "manufacturing_cost" name="manufacturing_cost" autocomplete="off"  required value="<?php echo  set_value('manufacturing_cost',isset($payment_details['manufacturing_cost']) ? $payment_details['manufacturing_cost'] : ''); ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Shipping Cost :
                                            </td>
                                            <td>
                                                <?php echo form_error('shipping_cost'); ?>
                                                <input type ="text"  id = "shipping_cost" name="shipping_cost" autocomplete="off"  required value="<?php echo set_value('shipping_cost',isset($payment_details['shipping_cost']) ? $payment_details['shipping_cost'] : ''); ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Comments :
                                            </td>
                                            <td>
                                                <?php echo form_error('comments'); ?>
                                                <textarea id = "shipping_cost" name="comments" autocomplete="off"    ><?php echo set_value('comments',isset($payment_details['comments']) ? $payment_details['comments'] : ''); ?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>

                                            </td>
                                            <td>
                                                <input type ="submit" class="" name="submit"  value="Submit" />
                                                <input type ="hidden" class="" name="payment_id"  value="<?php echo $payment_details['id']; ?>" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div> <!-- End #tab1 -->

                        <div class="tab-content" id="tab2">


                        </div> <!-- End #tab2 -->        

                    </div> <!-- End .content-box-content -->

                </div> <!-- End .content-box -->


                <div class="clear"></div>



            </div> <!-- End #main-content -->

        </div></body>

</html>
