<div>
    <div class="clear"></div>
    <br/>
    <form action="" method="post" id="filters">

    </form>
</div>
<ul class="shortcut-buttons-set">
</ul><!-- End .shortcut-buttons-set -->
<div class="clear"></div> <!-- End .clear -->
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Customer Details</h3>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
            <div class="notification attention png_bg">   
            </div>
            <form method="POST" action="<?php echo base_url() . 'index.php/admin/editcustomer'; ?>" >
                <div class="bulk-actions">
                    <table id="gr id" >
                        <tbody>
                            <tr>
                                <td width="150">
                                    First Name: 
                                </td>
                                <td>
                                    <span><?php echo $first_name; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="150">
                                    Last Name: 
                                </td>
                                <td>
                                    <span><?php echo $last_name; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="150">
                                    Email: 
                                </td>
                                <td>
                                    <span><?php echo $email; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="150">
                                    First Name: 
                                </td>
                                <td>
                                    <span> 
                                        <select name="customer_category" id="customer_category">
                                            <?php echo $customer_category; ?>
                                        </select>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <input type ="submit" class="submit button" name="submit"  value="Submit" />
                                    <input type ="hidden" class="" name="user_id"  value="<?php echo $user_id; ?>" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </form>


        </div> <!-- End #tab1 -->
        <div class="tab-content" id="tab2">
        </div> <!-- End #tab2 -->        
    </div> <!-- End .content-box-content -->
</div>