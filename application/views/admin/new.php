<?php
$this->load->view('includes/admin_header');
$user = $this->session->userdata('user_details');
if($user['user_id']==1846)
{
    ?>
   <style type="text/css">
    .edit-btn , .delete-item
    {
        display: none !important;
    }

   </style>

<?php
}

?>
<div class="modal fade order-modal" id="order-create" tabindex="-1"
     role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                <h4 class="modal-title">
                    <i class="fa  fa-folder-open-o"></i> Create New Order
                </h4>
            </div>
            <div class="modal-body">			
            </div>
            <div class="modal-footer clearfix">

                <!-- button type="button" class="btn btn-danger" data-dismiss="modal">
                                <i class="fa fa-times"></i> OK
                        </button-->

                <button type="button" data-dismiss="modal"
                        class="btn btn-primary pull-right">
                    <i class="fa fa-envelope"></i> Close
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade order-modal" id="order-details" tabindex="-1" role="dialog"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">×</button>
                <h4 class="modal-title">
                    <i class="fa  fa-folder-open-o"></i> Order Details
                </h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer clearfix">

                <!-- button type="button" class="btn btn-danger" data-dismiss="modal">
                                <i class="fa fa-times"></i> OK
                        </button-->

                <button type="button" data-dismiss="modal"
                        class="btn btn-primary pull-right">
                    <i class="fa fa-envelope"></i> Close
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<section class="content-header">
    <h1>
        Orders <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
    </ol>
</section>
<div class="container-fluid content" >
<div class="box box-header content" style="background-color:#FFFFFF ;border-top:#3C8DBC">
    <div class="row">  
        <div class="col-xs-12">
<!-- Nav tabs -->
  <ul class="nav nav-tabs "  style="margin-left:1%; border-top-color: #3c8dbc !important; border-bottom-color: #3c8dbc !important;">
    <li role="presentation" <?php echo ($this->router->method == 'mto')?'class="active"':''; ?> ><a href="<?php echo base_url() . 'index.php/orders/mto'; ?>" >MTO Hand Welt</a></li>
    <li role="presentation" <?php echo ($this->router->method == 'kso')?'class="active"':''; ?> ><a href="<?php echo base_url() . 'index.php/orders/kso'; ?>" > Kick Starter Orders </a></li>
    <li role="presentation" <?php echo ($this->router->method == 'spring')?'class="active"':''; ?> ><a href="<?php echo base_url() . 'index.php/orders/spring'; ?>" > Spring</a></li>
    <li role="presentation" <?php echo ($this->router->method == 'partner')?'class="active"':''; ?> ><a href="<?php echo base_url() . 'index.php/orders/partner'; ?>">Partner Orders</a></li>
    <li role="presentation" <?php echo ($this->router->method == 'rtw')?'class="active"':''; ?> ><a href="<?php echo base_url() . 'index.php/orders/rtw'; ?>">Ready To Wear</a></li>
  </ul>
<!-- Tab panes -->
  <div class="tab-content" style="border-top-color: #3c8dbc;" >
      <div role="tabpanel" class="tab-pane active" >
          <section id="content" class="content" style="background-color:#FFFFFF;">
    <div class="row">
        <div class="col-xs-12">
            <div class="box collapsed-box" style="border-top-color: #3c8dbc;">

                <div class="box-header" data-toggle="tooltip" title=""
                     data-original-title="Filters" style="border-top:#3C8DBC">
                    <h3 class="box-title">Filters</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-primary btn-xs" data-widget="collapse">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">


                            <form action="<?php echo base_url() . 'index.php/orders/'.$form; ?>" method="post" id="filters">
                                <div class="bulk-actions align-left" style="float:left;margin: -10px 0 30px;">
                                    <?php 
                                    if($this->router->method == 'partner'){
                                        
                                    ?>
                               
                                   
                                    <select name="partner_id" id="partner_id" style="margin-top:1%;margin-bottom: 3%;">
                                       <option value="">select Partner</option>
                                        <?php echo $partner_name; ?>
                                    </select><br>
                                   <?php }?>
                                    
                                    <select name="order_status" id="order_status">
                                        <?php echo $order_status; ?>
                                    </select>
                                    <input type='text' name="order_number" id='order_number' value='<?php echo set_value('order_number', ''); ?>'  placeholder="Order Number" />
                                    <input type='text' name="invoice_number" id='invoice_number' value='<?php echo set_value('invoice_number', ''); ?>'  placeholder="Invoice Number" />
                                    <input type='text' name="customer_name" id='customer_name' value='<?php echo set_value('customer_name', ''); ?>'  placeholder="Customer Name" />
                                    <select name="style" id="style">
                                        <option value="">Style</option>
                                        <?php echo $style; ?>
                                    </select>
                                    <select name="last" id="last">
                                        <option value="">Last</option>
                                        <?php echo $last; ?>
                                    </select>
                                    <input type='submit' class='submit button'  value='Filter' style="background-color:#3C8DBC; color:white;" />
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!--subcontent-->
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
               
                    <div class="overlay"></div>
                    <div class="loading-img"></div>

 
                    <div class="box-header" data-toggle="tooltip" title=""
                         data-original-title="Orders">
                        <h3 class="box-title">Orders</h3>

                        <div class="box-tools pull-right">
                            <button class="btn btn-primary btn-xs" id="create-order">
                                <i class="fa fa-pencil"> Create New Order</i>
                            </button>
                        </div>
                    </div>
 
                    <div class="box-body">

                        <table id="orders"
                               class="table table-striped table-bordered table-hover">
                            <thead>
                                <!--thead-->
                                <tr>
                                    <th>Order No</th>
                                    <th>Invoice No</th>
                                    <?php if ($kickstarter) { ?><th>Kick starter No</th><?php } ?>
                                    <th>Order By</th>
                                   
                                    <th>No.of Shoes</th>
                                    <th>Shoe Cost</th>
                                    <th>Order Date</th>
                                    <th>Order Status</th>
                                </tr>


                            <thead>


                            <tbody>

                                <?php
                                if (isset($orders) && sizeof($orders)) {
                                    foreach ($orders as $key => $order) {
										 $order['total_amt'] = round($order['total_amt'] - ($order['total_amt']*$order['discount_perc']/100),0);
                                        ?>

                                        <tr
                                            class="<?php echo strtolower(str_replace(' ', '-', $order['status'])); ?>">
                                            <td>

                                                <?php
                                                echo $order['order_id'];
                                                ?>

                                            </td>
                                            <td> <?php echo $order['invoice_no']; ?> </td>
                                            <?php if ($kickstarter) { ?><td><?php echo $order['kickstarter_no']; ?></td><?php } ?>
                                            <td><?php
                                                echo $order['first_name'] . ' ' . $order['last_name'];
                                                $this->load->view('admin/order_quick_view', $order);
                                                ?>
                                                <input type="hidden" value="<?php echo $order['order_id']; ?>"
                                                       class="order_id">
                                            </td>
                                            
                                            <td><?php echo $order['item_count']; ?></td>
                                            <td><?php echo $order['total_amt']; ?></td>
                                            <td><?php echo date('m/d/Y H:ia', strtotime($order['order_date'])); ?></td>
                                            <td><?php echo $order['status']; ?></td>
                                        </tr>

                                        <?php

                                    }
                                }
                                ?>

                            </tbody>
                        </table>

                    </div>

 </ul>
            </div>
            </ui>
            <!--subcontent-->
        </div>
    </div>
</section>
      </div>
    <div role="tabpanel" class="tab-pane fade" id="KickStarter">
    
    </div>
    <div role="tabpanel" class="tab-pane fade" id="Spring">...</div>
    <div role="tabpanel" class="tab-pane fade" id="Partner">...</div>
  </div>
  </div>
<!-- Main content -->
</div>
</div>
</div>


<script type="text/javascript">

</script>



<?php
$this->load->view('includes/admin_footer');
?>
