<?php ?>
<div class="box">
    <div class="overlay"></div>
    <div class="loading-img"></div>
    <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs  ui-sortable-handle">
            <li class="active "><a href="#select-customer" data-toggle="tab">Select
                    Customer</a></li>
            <li class="" id="add-item-btn"><a href="#add-item" data-toggle="tab">Add
                    Item</a></li>

            <li class=""><a href="#shipping-billing-address" data-toggle="tab">Shipping
                    And Billing Address</a></li>
            <li class=""><a href="#order-review" data-toggle="tab">Order Review </a></li>
        </ul>
        <div class="tab-content ">
            <!-- Morris chart - Sales -->
            <div class="chart tab-pane active" id="select-customer">

                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Select Customer</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <select class="form-control" id="customer"><?php echo $edit['users']; ?></select>

                        </div>
                    </div>
                </div>
            </div>
            <div class="chart tab-pane add-edit" id="add-item">

                <?php $this->load->view('admin/order_add_item', @$edit); ?>
            </div>

            <div class="chart tab-pane " id="shipping-billing-address">

                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Shipping Address</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body shipping-address">
                            <?php $this->load->view('admin/order_shipping_address', $shipping); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Billing Address</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body billing-address ">

                            <?php $this->load->view('admin/order_billing_address', $billing); ?>

                        </div>
                    </div>
                </div>
            </div>

            <div class="chart tab-pane " id="order-review">

                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Shipping Address</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body shipping-address">
                            <?php $this->load->view('admin/order_shipping_address_list', $shipping); ?>
                        </div>
                    </div>
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Billing Address</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body billing-address ">

                            <?php $this->load->view('admin/order_billing_address_list', $billing); ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Review</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="row seperation">
                                <div class="col-md-4">Number of items </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="number_of_items"
                                           disabled="disabled" />
                                </div>
                            </div>

                            <div class="row seperation">
                                <div class="col-md-4">Subtotal </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="subtotal" />
                                </div>
                            </div>

                            <div class="row seperation">
                                <div class="col-md-4">Order source </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="order-source" />
                                </div>
                            </div>

                            <div class="row seperation">
                                <div class="col-md-4">Belt Needed ?</div>
                                <div class="col-md-6">
                                    <input type="checkbox" class="" id="belt" />
                                </div>
                            </div>
                            <div class='row seperation'>
                                <div class="col-md-4">Comments</div>
                                <div class="col-md-8">
                                    <textarea rows="" cols="" class="form-control"
                                              id="order_comments"></textarea>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Payment</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
<div class='row seperation'>
<div class="col-md-12"><input type="radio" name="payment-mode" class="payment-mode" value="credit-card" checked />Payment through Credit Card</div>
<div class="col-md-12"><input type="radio" name="payment-mode" class="payment-mode" value="cash" />Payment through Cash</div>
</div>
                            <div class='row payment seperation'>
                                <div class="col-md-3">Card Number</div>
                                <div class="col-md-9 "><input class="form-control card-number"></div>

                            </div>
                            <div class='row payment seperation'>
                                <div class="col-md-3"></div>
                                <div class="col-md-3 "><input class="form-control card-cvc" placeholder="CVV"></div>
                                <div class="col-md-3">
                                    <select name="card-expiry-month" class="form-control card-expiry-month" id="card-expiry-month">
                                        <option value="" >Exp. Month</option>
                                        <option value="1" >1</option>
                                        <option value="2" >2</option>
                                        <option value="3" >3</option>
                                        <option value="4" >4</option>
                                        <option value="5" >5</option>
                                        <option value="6" >6</option>
                                        <option value="7" >7</option>
                                        <option value="8" >8</option>
                                        <option value="9" >9</option>
                                        <option value="10" >10</option>
                                        <option value="11" >11</option>
                                        <option value="12" >12</option>
                                    </select></div>
                                <div class="col-md-3 ">
                                    <select name="card-expiry-year" class="form-control card-expiry-year" id="card-expiry-year" >
                                        <option value="" >Exp. Year</option>
                                        <option value="2016" >2016</option>
                                        <option value="2017" >2017</option>
                                        <option value="2018" >2018</option>
                                        <option value="2019" >2019</option>
                                        <option value="2020" >2020</option>
                                        <option value="2021" >2021</option>
                                        <option value="2022" >2022</option>
                                        <option value="2023" >2023</option>
                                        <option value="2024" >2024</option>
                                        <option value="2025" >2025</option>
                                        <option value="2026" >2026</option>
                                        <option value="2027" >2027</option>
                                        <option value="2028" >2028</option>
                                        <option value="2029" >2029</option>
                                        <option value="2030" >2030</option>
                                        <option value="2031" >2031</option>
                                        <option value="2032" >2032</option>
                                        <option value="2033" >2033</option>
                                        <option value="2034" >2034</option>
                                        <option value="2035" >2035</option>
                                        <option value="2036" >2036</option>
                                        <option value="2037" >2037</option>
                                        <option value="2038" >2038</option>
                                        <option value="2039" >2039</option>
                                        <option value="2040" >2040</option>
                                    </select></div>

                            </div>

                            <button class="btn btn-primary" id="confirm-order">Confirm Order</button>
                        </div>
                    </div>


                </div>

            </div>


        </div>
    </div>
</div>
