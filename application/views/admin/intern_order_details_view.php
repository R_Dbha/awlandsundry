<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/admin/common.css' ?>" type="text/css" media="screen" />
        <!-- Colour Schemes
  
        Default colour scheme is green. Uncomment prefered stylesheet to use it.
        
        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
        
        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
 
        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.3.2.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/admin/common.js' ?>"></script>
        <script type="text/javascript">
            function validate()
            {
                if ($('#dropdown_vendor').val() === "option1") {
                    alert('Please select the vendor');
                    return false;
                }
                else {
                    return true;
                }
            }
            $(document).ready(function () {
                $('.refund').click(function () {
                    var element = $(this);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>index.php/admin/refund",
                        dataType: "json",
                        data: {
                            'orderid': '<?php echo $order_summary[0]['order_id']; ?>',
                            'invoiceno': '<?php echo $order_summary[0]['invoice_no']; ?>'
                        },
                        success: function (msg) {
                            window.location.href = '<?php echo base_url(); ?>' + 'index.php/admin/admin_view';
                        }
                    });
                });
            });

        </script>
        <!-- Internet Explorer .png-fix -->

        <!--[if IE 6]>
                <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('.png_bg, img, li');
                </script>
        <![endif]-->

    </head>

    <body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">Simpla Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src=<?php echo base_url() . "assets/img/logo.png" ?> alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a>
                        <br />
                        <!--a href="<?php echo base_url() . 'index.php' ?>" title="View the Site">View the Site</a--> 
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        


                    <?php $this->load->view('admin/intern_menu'); ?>

                    <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                        <h3>3 Messages</h3>

                        <p>
                            <strong>17th May 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br />
                            Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">

                            <h4>New Message</h4>

                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>

                            <fieldset>

                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>

                                <input class="button" type="submit" value="Send" />

                            </fieldset>

                        </form>

                    </div> <!-- End #messages -->

                </div></div> <!-- End #sidebar -->
            <?php
            $discountPerc = $order_summary[0]['discount_perc'];
            $disc = 0.00;
            $totalamt = 0;
            ?>
            <div id="main-content"> <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>

                <!-- Page Head -->
                <!--h2>Welcome</h2-->

                <!--p id="page-intro">What would you like to do?</p-->

                <ul class="shortcut-buttons-set">

                </ul><!-- End .shortcut-buttons-set -->

                <div class="clear"></div> <!-- End .clear -->
                <div class="content-box-header">
                    <h3>Order Details (Status : <?php echo $status[0]['status']; ?>)</h3>

                    <div class="clear"></div>
                </div> <!-- End .content-box-header -->

                <div class="content-box-content">					
                    <fieldset> 
                        <p>
                            <label>Order No : <?php echo $order_summary[0]['order_id']; ?></label>
                        </p>
                        <p>
                            <label>Invoice No : <?php echo $order_summary[0]['invoice_no']; ?></label>
                        </p>
                        <p>
                            <label><?php echo $status[0]['status_desc']; ?></label>
                        </p>
                    </fieldset>
                </div>
                <div style="clear:both;" ></div>
                <div id="address_table" style ="overflow: hidden;"> 
                    <table style = "width: 100%;" border-collapse: collapse; border-spacing: 0;>
                           <tbody style = "display: table-row-group; vertical-align: middle; border-color: inherit;">	
                            <tr style = "display: table-row; vertical-align: inherit; border-color: inherit;">
                                <th style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; 
                                    font-size: 21px; text-align: left; padding: 5px 0;">Billing to:</th>
                                <th style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; 
                                    font-size: 21px; text-align: left; padding: 5px 0;">Shipping to:</th>
                            </tr>
                            <tr>
                                <td style = "font-size: 20px;">

                                    <?php echo $billing[0]['first_name'] . ' ' . $billing[0]['last_name']; ?>
                                </td>
                                <td style = "font-size: 20px;">
                                    <?php echo $shipping[0]['firstname'] . ' ' . $shipping[0]['lastname']; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style = "font-size: 20px;">
                                    <?php echo ($billing[0]['address1'] == '' || $billing[0]['address1'] == '0') ? '' : $billing[0]['address1']; ?>
                                </td>
                                <td style = "font-size: 20px;">
                                    <?php echo $shipping[0]['address1'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td style = "font-size: 20px;">
                                    <?php
                                    echo (trim($billing[0]['address2']) == '' || $billing[0]['address2'] == '0') ? '' : $billing[0]['address2'] . ', ';
                                    echo (trim($billing[0]['city']) == '' || $billing[0]['city'] == '0') ? '' : $billing[0]['city'] . ', ';
                                    echo (trim($billing[0]['state']) == '' || $billing[0]['state'] == '0') ? '' : $billing[0]['state'] . ', ';
                                    echo (trim($billing[0]['country']) == '' || $billing[0]['country'] == '0') ? '' : $billing[0]['country'] . ', ';
                                    echo (trim($billing[0]['zipcode']) == '' || $billing[0]['zipcode'] == '0') ? '' : $billing[0]['zipcode'];
                                    ?>
                                </td>
                                <td style = "font-size: 20px;">
                                    <?php
                                    echo (trim($shipping[0]['address2']) == '' || $shipping[0]['address2'] == '0') ? '' : $shipping[0]['address2'] . ', ';
                                    echo $shipping[0]['city'] . ', ';
                                    echo (trim($shipping[0]['state']) == '' || $shipping[0]['state'] == '0') ? '' : $shipping[0]['state'] . ', ';
                                    echo (trim($shipping[0]['country']) == '' || $shipping[0]['country'] == '0') ? '' : $shipping[0]['country'] . ', ';
                                    echo $shipping[0]['zipcode'];
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style = "font-size: 20px;"></td>
                                <td style = "font-size: 20px;"><?php echo $shipping[0]['telephone']; ?></td>
                            </tr>
                            <tr>
                                <td style = "font-size: 20px;"></td>
                                <td style = "font-size: 20px;"><?php echo $shipping[0]['email']; ?></td>
                            </tr>
                        </tbody>	
                    </table>
                    <?php
                    if (trim($order_summary[0]['comments']) !== '') {
                        ?>
                        <div style = "font-size: 18px; padding-top: 10px;" >
                            <span>
                                Comments :
                            </span>
                            <b>
                                <?php
                                echo $order_summary[0]['comments'];
                                ?>
                            </b>
                        </div>
                        <?php
                    }
                    if (trim($remake_instruction) !== '') {
                        ?>
                        <div style = "font-size: 18px; padding-top: 10px;" >
                            <span>
                                Rework Instruction :
                            </span>
                            <b>
                                <?php
                                echo $remake_instruction;
                                ?>
                            </b>
                        </div>
                    <?php } ?>
                </div> 
                <div id="order_table" style = "overflow: hidden;">
                    <div id="tab">
                        <?php
                        $i = 1;
                        $class = "active";
                        foreach ($order['items'] as $item) {
                            ?>
                            <span class="<?php echo $class; ?>"><a href="#item<?php echo $i; ?>">Item #<?php echo $i; ?></a></span>
                            <?php
                            $class = "";
                            $i++;
                        }
                        ?>
                    </div>
                    <div id="orders">

                        <?php
                        $total = 0;
                        $class = "active";
                        $i = 1;
                        foreach ($order['items'] as $item) {
                            $class .= ' item' . $i;
                            $i++;
                            if (strtolower($item['item_name']) != 'gift card') {
                                $shoedesign = $item['shoedesign'];
                                $totalamt += $item['item_amt'];
                                ?>
                                <div class="order-items <?php echo $class; ?>">
                                    <table id="order_list"  style = " border-collapse: collapse; border-spacing: 0; display: table; border-color: gray; width: 100%;  font-weight: bold; font-family: gibsonsemibold, arial, sans-serif;">
                                        <thead style = "display: table-header-group; vertical-align: middle; border-color: inherit;">
                                            <tr>
                                                <th style="width:65%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
                                                    text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">DESCRIPTION</th>
                                                <th style="width:27%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
                                                    text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">QUANTITY</th>
                                                <th style="width:18%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
                                                    text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">PRICE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style = "border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">
                                                   <?php if($item['reference_order_no'] !== ''){
														echo 'Reference Order Number : '.$item['reference_order_no'].'<br/>';
													} ?>                                                   
                                                    Left shoe size : <?php echo $item['left_shoe']; ?> , Right shoe size : <?php echo $item['right_shoe']; ?>
                                                    <br/>
                                                    Left shoe Width : <?php echo @$item['left_width']; ?> , Right shoe size : <?php echo @$item['right_width']; ?>
                                                    <?php if($item['m_left_width'] !== '' && $item['m_left_width'] !== '0'){?>
              		                                    <br />
              		                                    <br />
              		                                    <u>Measurements:</u>
              		                                    <br />
              		                                    Left shoe size : <?php echo $item['m_left_size']; ?> , Right shoe size : <?php echo $item['m_right_size']; ?>
                    	                                <br />
                        	                            Left shoe Width : <?php echo @$item['m_left_width']; ?> , Right shoe width : <?php echo @$item['m_right_width']; ?>
                                                     <br />
                        	                            Left shoe Height : <?php echo @$item['m_lgirth']; ?> , Right shoe Height: <?php echo @$item['m_rgirth']; ?>
                        	                             <br />
                        	                            Left shoe Instep : <?php echo @$item['m_linstep']; ?> , Right shoe Instep : <?php echo @$item['m_rinstep']; ?>
                                                    <?php }?>
                                                   
                                                    <?php
                                                    if (isset($item['shop_images']) && is_array($item['shop_images'])) {
                                                        foreach ($item['shop_images'] as $key => $value) {
                                                            ?>
                                                            <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/' . $value['file_name']; ?>" />
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A0.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A1.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A2.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A3.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A4.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A5.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A6.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A7.png"; ?>" />
                                                    <?php } ?>
                                                    <?php if ($item['can_show_details']) { ?>
                                                        <p class="title">Design Details </p>
                                                        <div id="side_menu_content">
                                                            <p class="item-subtitle"><?php echo $shoedesign['last_name'] . "/" . $shoedesign['style_name'] . " (" . $shoedesign['manufacturer_code'] . ")"; ?></p>
                                                            <ul id="design_menu" class="level1">
                                                                <?php if (!$shoedesign['t_is_nothing']) { ?>
                                                                    <li>Toe  : <?php echo $shoedesign['toe_type']; ?> 
                                                                        <?php if ($shoedesign['t_is_material']) { ?>
                                                                            ,<?php echo $shoedesign['toe_material']; ?> ,<?php
                                                                            echo $shoedesign['toe_color'];
                                                                        }
                                                                        ?>
                                                                    </li>
                                                                    <ul class="level2">
                                                                    </ul>
                                                                <?php } if ($shoedesign['is_vamp_enabled']) { ?>
                                                                    <?php if (!$shoedesign['v_is_nothing']) { ?>
                                                                        <li>Vamp  : <?php echo $shoedesign['vamp_type']; ?> 
                                                                            <?php if ($shoedesign['v_is_material']) { ?>
                                                                                , <?php echo $shoedesign['vamp_material']; ?> , <?php
                                                                                echo $shoedesign['vamp_color'];
                                                                            }
                                                                            ?> 

                                                                        </li>
                                                                        <ul class="level2">
                                                                        </ul>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                                <li>Quarter  : <?php echo $shoedesign['quarter_material']; ?> , <?php echo $shoedesign['quarter_color']; ?></li>
                                                                <ul class="level2">
                                                                </ul>
                                                                <?php if (!$shoedesign['e_is_nothing']) { ?>
                                                                    <li>Eyestays : <?php echo $shoedesign['eyestay_type']; ?>
                                                                        <?php if ($shoedesign['e_is_material']) { ?>
                                                                            , <?php echo $shoedesign['eyestay_material']; ?> , <?php
                                                                            echo $shoedesign['eyestay_color'];
                                                                        }
                                                                        ?>

                                                                    </li>
                                                                    <ul class="level2">
                                                                    </ul>
                                                                <?php } if (!$shoedesign['f_is_nothing']) { ?>
                                                                    <li>Foxing  : <?php echo $shoedesign['foxing_type']; ?>
                                                                        <?php if ($shoedesign['f_is_material']) { ?> 
                                                                            , <?php echo $shoedesign['foxing_material']; ?> , <?php
                                                                            echo $shoedesign['foxing_color'];
                                                                        }
                                                                        ?></li>
                                                                    <ul class="level2">
                                                                    </ul>
                                                                <?php } ?>
                                                                <li>Stitching :  <?php echo $shoedesign['stitch_name']; ?></li>
                                                                <ul class="level2">
                                                                    <ul class="level3">
                                                                        <li style="background: <?php echo $shoedesign['stitch_color']; ?>"></li>
                                                                    </ul>
                                                                </ul>
                                                                <?php if ($shoedesign['is_lace_enabled']) { ?>
                                                                    <li>Laces  : <?php echo $shoedesign['lace_name']; ?></li>
                                                                    <ul class="level2">
                                                                        <ul class="level3">
                                                                            <li style="background: <?php echo $shoedesign['color_code']; ?>"></li>
                                                                        </ul>
                                                                    </ul>
                                                                    <?php
                                                                }
                                                                if ($shoedesign['monogram_id'] !== NULL && trim($shoedesign['monogram_text']) !== '') {
                                                                    $text = "";
                                                                    ?>
                                                                    <ul class="level2">
                                                                        <li>Monogram  : <?php echo $shoedesign['monogram_text']; ?></li>

                                                                        <?php
                                                                        if ($shoedesign['right_side'] > 0) {
                                                                            $text = "on right counter";
                                                                        }
                                                                        if ($shoedesign['left_side'] > 0) {
                                                                            $text = "on left counter";
                                                                        }
                                                                        if ($shoedesign['left_shoe'] > 0) {
                                                                            $text .= ", left shoe";
                                                                            if ($shoedesign['right_shoe'] > 0) {
                                                                                $text .= " and right shoe";
                                                                            }
                                                                        } elseif ($shoedesign['right_shoe'] > 0) {
                                                                            $text .= ", right shoe";
                                                                        }
                                                                        ?>
                                                                        <li><?php echo $text; ?></li>
                                                                    </ul>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    <?php } ?>
                                                </td>
                                                <td style="text-align:center; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;"><?php echo $item['quantity'] ?> PAIR</td>
                                                <td style="text-align:center; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">$<?php echo $item['item_amt'] ?></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                    <div>
                                        <?php
                                        if ($status[0]['status'] === 'order shipped' && $is_remake_request_sent[$i - 2]) {
                                            ?>
                                            <form id="remake" method="post" action="<?php echo base_url() . 'index.php/admin/remake_view'; ?>">
                                                <input type="submit" class="button submit" value="Remake this shoe" />
                                                <input type="hidden" name="shoe_design_id" value="<?php echo $shoedesign['shoe_design_id']; ?>" />
                                                <input type="hidden" name="order_id"  value="<?php echo $order_summary[0]['order_id']; ?>" />
                                            </form>
                                        <?php } ?>
                                    </div>
                                </div>

                                <?php
                                $class = '';
                            }
                        }
                        if ($discountPerc != 0) {
                            $disc = ($totalamt * $discountPerc) / 100;
                            $netamt = $totalamt - $disc;
                        } else {
                            $netamt = $totalamt;
                        }
                        ?>
                    </div>
                    <br/>
                    <table>
                        <tbody>
                            <tr style="border:1px solid #000; height: 40px;">
                                <td style="width:65%;"></td>

                                <td style="width:35%;" colspan="2">
                                    <div>
                                        <a href="javascript:window.print();" class="button">Print this page</a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>

                                </td>
                                <td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; text-transform: uppercase;">SUBTOTAL:</td>
                                <td style = "text-align: right; padding: 10px 30px 0px 0px;">$<?php echo $totalamt . '.00' ?>
                                </td>
                            </tr>
                            <tr>	
                                <td></td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; text-transform: uppercase;">SHIPPING COST:</td><td style = "text-align: right; padding: 10px 30px 0px 0px;">$0.00</td>
                            </tr>
                            <tr>	
                                <td></td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; text-transform: uppercase;">PROMOTION DISCOUNT:</td><td style = "text-align: right; padding: 10px 30px 0px 0px;">$<?php echo round($disc) . '.00' ?></td>
                            </tr>
                            <tr>	
                                <td></td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; text-transform: uppercase;">GRAND TOTAL:</td><td style = "text-align: right; padding: 10px 30px 0px 0px;">$<?php echo round($netamt) . '.00' ?></td>
                            </tr>
                        </tfoot>	
                    </table>
                </div>
                <div class="clear"></div>
                <!--a href="<?php echo base_url() . 'index.php/admin/pdf' ?>">Create Pdf</a-->
                <div class="clear"></div>
                <div id="footer">
                    <small>
                        &#169; Copyright 2013 | Powered by <a href="#"></a> | <a href="<?php echo base_url() . 'index.php/admin/admin_view' ?>">BACK</a>
                    </small>
                </div><!-- End #footer -->
            </div> <!-- End #main-content -->

    </body>

</html>
