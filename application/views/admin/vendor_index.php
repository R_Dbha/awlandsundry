<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>AWL&SUNDRY Vendor</title>
        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.3.2.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#grid tbody tr').off('click touchend').on('click touchend', function () {
                    window.location = $(this).attr('href');
                    return false;
                });
            });
            function downloadpdf() {
                window.open(ApplicationConfig::ROOT_BASE . 'files/PanelDesigns__renumbered_overview_2013-08-20.pdf');
            }
        </script>
    </head>
    <body>
        <div id="body-wrapper">
            <div id="sidebar">
                <div id="sidebar-wrapper">
                    <h1 id="sidebar-title">
                        <a href="#">AWL Vendor</a>
                    </h1>
                    <a href="#"><img id="logo" style="background-color: #fff; width: 217px;" src="<?php echo base_url() . "assets/img/logo.png" ?>" alt="Simpla Admin logo" /></a>
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a>
                        you have <a href="#" title="3 Messages"><?php echo $orders['cnt'] . ' ' . $orders['status'] ?> Orders</a><br />
                        <br /> <a href="<?php echo base_url(); ?>" title="View the Site"></a>
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>
                    <ul id="main-nav">
                        <li>
                            <a href="" class="nav-top-item <?php echo ($this->router->method == 'vendor' || $this->router->method == 'spring_vendor_view' || $this->router->method == 'kickstarter_vendor_view' || $this->router->method == 'b2b') ? 'current' : ''; ?>"> Orders </a>
                            <ul>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/vendor" class="<?php echo ($this->router->method == 'vendor') ? 'current' : ''; ?>"> MTO Hand Welt</a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/spring_vendor_view" class="<?php echo ($this->router->method == 'spring_vendor_view') ? 'current' : ''; ?>"> Spring </a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/kickstarter_vendor_view" class="<?php echo ($this->router->method == 'kickstarter_vendor_view') ? 'current' : ''; ?>"> Kick Starter </a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/b2b" class="<?php echo ($this->router->method == 'b2b') ? 'current' : ''; ?>"> B2B </a></li>
                            </ul>
                        </li>

                        <li><a href="<?php echo base_url() . 'index.php/admin/admin_changepass' ?>" class="nav-top-item no-submenu"> Change Password </a></li>
                        <li><a href="javascript:downloadpdf();" class="nav-top-item no-submenu"> Download Panel Designs </a></li>
                    </ul>

                    <div id="messages" style="display: none">
                        <h3>3 Messages</h3>
                        <p>
                            <strong>17th May 2009</strong> by Admin<br /> Lorem ipsum dolor
                            sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi
                            at felis aliquet congue. <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br /> Ut a est eget
                            ligula molestie gravida. Curabitur massa. Donec eleifend, libero
                            at sagittis mollis, tellus est malesuada tellus, at luctus turpis
                            elit sit amet quam. Vivamus pretium ornare est. <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br /> Lorem ipsum dolor
                            sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi
                            at felis aliquet congue. <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">
                            <h4>New Message</h4>
                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>
                            <fieldset>
                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>
                                <input class="button" type="submit" value="Send" />
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div id="main-content">
                <form action="<?php echo base_url() . 'index.php/admin/vendor' ?>" method="post">
                    <noscript>
                        <div class="notification error png_bg">
                            <div>
                                Javascript is disabled or is not supported by your browser.
                                Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or
                                <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript
                                to navigate the interface properly.
                            </div>
                        </div>
                    </noscript>
                    <h2>Welcome <?php echo $user['first_name']; ?></h2>
                    <div>
                        <fieldset>
                            <?php
                            $options = array('' => 'All', 'Manufacturing in progress' => 'Manufacturing in Progress', 'order shipped' => 'Order Shipped');
                            echo form_dropdown('dropdown_status', $options, $orders['status']);
                            ?>
                            <input class="button" type="submit" value="Show" />
                        </fieldset>
                    </div>
                    <ul class="shortcut-buttons-set"></ul>
                    <div class="clear"></div>
                    <div class="content-box">
                        <div class="content-box-header">
                            <h3>Order Summary</h3>
                            <div class="clear"></div>
                        </div>
                        <div class="content-box-content">
                            <div class="tab-content default-tab" id="tab1">
                                <div class="notification attention png_bg"></div>
                                <table id="grid">
                                    <thead>
                                        <tr>
                                            <th>Order No</th>
                                            <th>Invoice No</th>
                                            <th>Is Rework</th>
                                            <th>Order by</th>
                                            <th>Total no</th>
                                            <th>Ordered Date</th>
                                            <th>Ordered Status</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6"><div class="clear"></div></td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php $cnt = 0; ?>
                                        <?php foreach ($orders['rows'] as $order) { ?>
                                            <?php $cnt ++; ?>
                                            <?php if ($order['order_id'] != NULL) { ?>
                                                <tr style='cursor: pointer;'>
                                                    <td> <?php echo $order['order_id']; ?></td>
                                                    <td><a href="<?php echo base_url() . 'index.php/admin/second_stepdetails/' . $order['order_id'] . '/' . $orders['vendorId']; ?>" title="title"><?php echo $order['invoice_no']; ?></a></td>
                                                    <td><?php echo $order['is_rework']; ?></td>
                                                    <td><?php echo $order['first_name'] . ' ' . $order['last_name']; ?></td>
                                                    <td><?php echo $order['item_count']; ?></td>
                                                    <td><?php echo date("m/d/Y h:ia", strtotime($order['order_date'])); ?></td>
                                                    <td><?php echo $order['status']; ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <input type="hidden" name="hid_vendorId" value="<?php echo $orders['vendorId']; ?>" />
                            </div>
                            <div class="tab-content" id="tab2"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </body>
</html>