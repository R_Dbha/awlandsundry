<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>AWL&SUNDRY Admin</title>

        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <!-- Colour Schemes
  
        Default colour scheme is green. Uncomment prefered stylesheet to use it.
        
        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
        
        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
 
        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.3.2.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript">
            function countCheckBoxes() {
                if ($('.checkbox:checked').length)
                    return true
                alert('Select atleast one user to assign promocode');
                return false;
            }

            $(document).ready(function() {



                $('.selectall').click(function(event) {  //on click 

                    if ($(this).attr('checked')) { // check select status
                        $('.checkbox').each(function() { //loop through each checkbox
                            $(this).attr('checked', true);  //select all checkboxes with class "checkbox1"               
                        });
                    } else {
                        $('.checkbox').each(function() { //loop through each checkbox
                            $(this).attr('checked', false) //deselect all checkboxes with class "checkbox1"                       
                        });
                    }
                });


            });


        </script>
        <!-- Internet Explorer .png-fix -->

        <!--[if IE 6]>
                <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('.png_bg, img, li');
                </script>
        <![endif]-->

    </head>

    <body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">AWL Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src='<?php echo base_url() . "assets/img/logo.png" ?>' alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a> 

                        <br/>
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        

                    <?php
                    if ($user_type_id == 4) {
                        $this->load->view('admin/intern_menu');
                    } else {
                        $this->load->view('admin/menu');
                    }
                    ?>

                    <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                        <h3>3 Messages</h3>

                        <p>
                            <strong>17th May 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br />
                            Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">

                            <h4>New Message</h4>

                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>

                            <fieldset>

                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>

                                <input class="button" type="submit" value="Send" />

                            </fieldset>

                        </form>

                    </div> <!-- End #messages -->

                </div>
            </div> <!-- End #sidebar -->

            <div id="main-content"> <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>

                <!-- Page Head -->
                <h2>Welcome <?php echo $user['first_name']; ?></h2>
                <table style="font-size: 14px; ">
                    <tr>
                        <td width="250">Promocode:</td>
                        <td ><?php echo $promo['promocode']; ?></td>

                    </tr>
                    <tr>
                        <td>Discount Percentage:</td>
                        <td><?php echo $promo['discount_per']; ?></td>

                    </tr>
                    <tr>
                        <td>Expiry Date</td>
                        <td><?php echo date("m/d/Y", strtotime($promo['expiry_date'])); ?></td>

                    </tr>
                    <tr>
                        <td>Number of Assigned Users</td>
                        <td><?php echo $promo['assigned_users']; ?></td>

                    </tr>
                    <tr>
                        <td>Used</td>
                        <td><?php echo $promo['used_users']; ?></td>

                    </tr>
                    <tr>
                        <td>Available to all?</td>
                        <td><?php echo ($promo['used_users'] == 1) ? 'Yes' : 'No'; ?></td>

                    </tr>

                </table>
                <div style="padding: 10px 0px;">
                    <form action="" method ="post" >
                        <table style="font-size: 14px;">
                            <tr>
                                <td width="150">Enter user email </td>
                                <td width="150"><input type="email" required name="email"  ></td>
                                <td ><input type="submit" required name="submit" value="Assign to email"   /></td>

                            </tr>

                        </table>
                    </form>
                </div>
                <form action ="<?php echo base_url() . 'index.php/admin/assign' ?>" method ="post" onsubmit="return countCheckBoxes();">
                    <div>
                        <fieldset>


                            <input type="submit" class="button" value="Assign to Selected Users" />


                        </fieldset>
                    </div>
                    <!--p id="page-intro">What would you like to do?</p-->

                    <ul class="shortcut-buttons-set">


                    </ul><!-- End .shortcut-buttons-set -->

                    <div class="clear"></div> <!-- End .clear -->



                    <div class="content-box"><!-- Start Content Box -->

                        <div class="content-box-header">

                            <h3>Users</h3>



                            <div class="clear"></div>

                        </div> <!-- End .content-box-header -->

                        <div class="content-box-content">

                            <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                                <div class="notification attention png_bg">

                                </div>

                                <table id="grid">

                                    <thead>
                                        <tr>
                                            <!--th><input class="check-all" type="checkbox" /></th-->
                                            <th>#</th>
                                            <th><input class="selectall" type="checkbox" /></th>
                                            <th>Email</th>
                                            <th>Assigned</th>

                                            <th>Is Used</th>
                                            <th></th>
                                        </tr>

                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <td colspan="6">

                                                <div class="clear"></div>
                                            </td>
                                        </tr>
                                    </tfoot>

                                    <tbody>
                                        <?php
                                        $cnt = 0;
                                        foreach ($promousers as $row) {
                                            $cnt++;
                                            ?>
                                            <tr  style ='cursor:pointer;'>
                                                <td>
                                                    <?php echo $cnt ?>
                                                </td>
                                                <td>
                                                    <input type="checkbox" class="checkbox" <?php if ($row['is_used'] == 'Yes') echo 'disabled'; ?> name="userid[]" value="<?php echo $row['id']; ?>" />
                                                </td>
                                                <td>
                                                    <?php echo $row['email']; ?>
                                                </td>
                                                <td>
                                                    <?php echo ($row['is_assigned']) ? 'Yes' : 'No'; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['is_used']; ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>

                                </table>


                            </div> <!-- End #tab1 -->

                            <div class="tab-content" id="tab2">



                            </div> <!-- End #tab2 -->        

                        </div> <!-- End .content-box-content -->
                        <input type="hidden" name="promo_id" value="<?php echo $promo['promo_id']; ?>">
                            </form>
                    </div> 


                    <div class="clear"></div>



            </div> <!-- End #main-content -->


    </body>

</html>
