<?php
$this->load->view('includes/admin_header');
?>

<section class="content-header">
    <h1>
        Appointments <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Appointments</li>
    </ol>
</section>

<section id="content" class="content" style="background-color:#FFFFFF;">
    <div class="box box-primary">
        <div class="overlay"></div>
        <div class="loading-img"></div>
        <div class="box-header" data-toggle="tooltip" title="" data-original-title="Appointmens">
            <h3 class="box-title">Appointments</h3>
        </div>
        <div class="box-body">
            <table id="appointments" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>                       
                        <th>Appointment ID</th>
                        <th>Name</th>
                        <th>Email ID</th>
                        <th>Phone Number</th>
                        <th>Date Time</th>
                        <th>US Shoe Size</th>
                        <th>Notes</th>
                    </tr>
                </thead>               
                <tbody>
                    <?php
                    if ($appointments) {
                        foreach ($appointments as $appointment) {
                        	?>
                            <tr  style ='cursor:pointer;'>
                                <td><?php echo $appointment['appointment_id']; ?></td>
                                <td><?php echo $appointment['user_name']; ?></td>
                                <td><?php echo $appointment['email_id']; ?></td>
                                <td><?php echo $appointment['phone_number']; ?></td>
                                <td><?php echo $appointment['appointment_datetime']; ?></td>
                                <td><?php echo $appointment['us_shoe_size']; ?></td>
                                <td><?php echo $appointment['special_request']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php
$this->load->view('includes/admin_footer');
?>