<script type="text/javascript">
    $(document).ready(function () {
        $('.new-tab').click(function (e) {
            e.preventDefault();
            window.open('<?php echo base_url(); ?>orders/kso', '_blank');
            return false;
        });
        $('.new-tab1').click(function (e) {
            e.preventDefault();
            window.open('<?php echo base_url(); ?>orders/mto', '_blank');
            return false;
        });
    });
</script>
<ul id="main-nav">  <!-- Accordion Menu -->
    <li><a 
            class=" new-tab nav-top-item no-submenu"> Kick Starter Orders </a></li>

    <li>
    <li><a 
            class=" new-tab1 nav-top-item no-submenu"> Edit Orders </a></li>

    <li>
        <a href="<?php echo base_url(); ?>index.php/admin/intern" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Orders
        </a>       
    </li>
    <li>
        <a href="<?php echo base_url(); ?>promocode" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Promocode
        </a>       
    </li>
    <li>
        <a href="<?php echo base_url(); ?>giftcard/view" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Gift Card
        </a>       
    </li>
    <li>
        <a href="<?php echo base_url() . 'index.php/admin/admin_changepass' ?>" class="nav-top-item no-submenu"> 
            Change Password
        </a>       
    </li>
</ul> <!-- End #main-nav -->
