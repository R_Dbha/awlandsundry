<html><head></head>
		<body style = "margin: 0px; font-family: gibsonregular, arial, sans-serif; color: #414141; margin: 0px ;">
		
		<div>
				<div id="content" style = "width: 960px; margin: 0 auto;">
					<p style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; color: #414141;">Dear  <?php $user = $this->session->userdata('user_details'); echo $user['first_name'];?>,  </p>
					<div id="messsge_wrap" style = "margin: 45px 0; overflow: hidden;">
						<p style = "font-size: 20px; margin: 15px 0;">We are currently proccessing your order.</p>
						<p style = "font-size: 20px; margin: 15px 0;">Your made-to-order shoes will be delivered to your doorsteps within 4 weeks from today. We will
							give you a shout out with the tracking number as soon as your order is out the door. Please call us at
							<?php echo $this->config->item('customer_care_number'); ?> if you have any questions.</p>
						<br/>
						<p style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; margin: 15px 0;">YOUR ORDER SUMMERY</p>
						<p style = "font-size: 20px; margin: 15px 0;"><span style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px;">Order Date:</span><?php echo date("m/d/Y h:ia", strtotime($order_summary[0]['order_date'])) ?></p>
						<p style = "font-size: 20px; margin: 15px 0;"><span style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px;">Order Number:</span><?php echo $order_summary[0]['order_id'] ?></p>
						<br/>
						
                                                
                                                
                                                
                                                <div id="address_table" style ="overflow: hidden;">
							<div style = "width: 100%;">
								<div>	
									<div>
										<div>Shipping to:</div>
										<div>Billing to:</div>
									</div>
									<div>
										<div><?php echo $billing[0]['first_name']; ?></div>
										<div><?php echo $shipping[0]['firstname'] ?></div>
									</div>
									<div>
										<div><?php echo $billing[0]['address1'] ?></div>
										<div><?php echo $shipping[0]['address1'] ?></div>
									</div>
									<div>
										<div><?php echo $billing[0]['address2'].','.$billing[0]['city'].'-'.$billing[0]['zipcode']; ?></div>
										<div><?php echo $shipping[0]['address2'].','.$shipping[0]['city'].'-'.$shipping[0]['zipcode']; ?></div>
									</div>
								</div>	
							</div>
						</div>
						
                                                
                                                
                                                <div id="order_table" style = "overflow: hidden;">
							<div id="order_list"  style = " border-collapse: collapse; border-spacing: 0; display: table; border-color: gray; width: 100%; margin-top: 30px; font-weight: bold; font-family: gibsonsemibold, arial, sans-serif;">
								<div>
										<div style="width:65%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
										text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">DESCRIPTION</div>
										<div style="width:27%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
										text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">QUANTITY</div>
										<div style="width:18%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
										text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">PRICE</div>
                                                                </div>
                                                                    <?php $total = 0;  
							foreach($order['items'] as $item){
								
                                                            ?>
                                                                        <div>
                                                                                <div style = "border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">
											<?php echo $item['item_name'];?>
											<img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A0.png"; ?>" />
										</div>
										<div style="text-align:center; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">1 PAIR</div>
										<div style="text-align:center; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">$<?php echo $item['item_amt'] ?></div>
									</div>
                                                                        <?php } ?>
									<div style="border:1px solid #000; height: 40px;">
										
									</div>
									
						</div>
					</div>
				</div>	
		</div>			
	</body>
</html>