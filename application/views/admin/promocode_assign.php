<?php ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">×</button>
    <h4 class="modal-title">
        <i class="fa  fa-folder-open-o"></i> Assign PromoCode
    </h4>
</div>
<div class="modal-body">		
    <div class="row seperation">

        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <div class="col-md-12 no-padding">
                    <label>PromoCode</label>
                    <div><?php echo @$promocode['promocode']; ?></div>
                </div>

            </div>
        </div>
        <?php if ($promocode['discount_amount'] != 0) { ?>
            <div class="col-md-4">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <label>Discount Amount</label>
                        <div><?php echo @$promocode['discount_amount']; ?></div>
                    </div>

                </div>
            </div>
        <?php } ?>
        <?php if ($promocode['discount_per'] != 0) { ?>
            <div class="col-md-4">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <label>Discount Percentage</label>
                        <div><?php echo @$promocode['discount_per']; ?></div>
                    </div>

                </div>
            </div>
        <?php } ?>
        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <div class="col-md-12 no-padding">
                    <label>Expiry Date</label>
                    <div><?php echo @date('Y-m-d', strtotime($promocode['expiry_date'])); ?></div>
                </div>
            </div>
        </div>
    </div>


    <div class="row seperation" >

        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <label>Available To All</label>
                <div ><?php echo $promocode['is_common'] == 1 ? 'Yes' : 'No'; ?></div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <label>First Use Only</label>
                <div><?php echo $promocode['one_time'] == 1 ? 'Yes' : 'No'; ?></div>

            </div>
        </div>
        <div class="col-md-4">
            <div class="row" style="margin: 0px;">
                <label>Unlimited</label>
                <div ><?php echo $promocode['unlimited'] == 1 ? 'Yes' : 'No'; ?></div>

            </div>
        </div>
    </div>
    <br/>
    <div class="row seperation" >
        
        <div class="col-md-2 ">
            <label>Enter Email</label>
        </div>
        <div class="col-md-4 no-padding">
            <input type="text" id="promocode_email" class="form-control" />
        </div>
        <div class="col-md-4 ">
            <button type="button" class="btn btn-primary" id="assign_user" >Assign</button>
        </div>
    </div>
    <br/>
    <div class="row seperation" >
        <div class="col-md-12">
            <form id="promocode_users_form">
                <table id="promocode_users"  class="table table-striped table-bordered table-hover">
                    <thead>
                        <!--thead-->
                        <tr>
                            <th></th>
                            <th>Email</th>
                            <th>Is Assigned</th>
                        </tr>
                    <thead>
                </table>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer clearfix">

    <button type="button" class="btn btn-primary" id="assign_promocode" >
        <i class="fa fa-envelope"></i> Assign to Selected Users
        <input type="hidden" id="promocode-id" value="<?php echo @$promocode['promo_id']; ?>">
    </button>

    <button type="button" data-dismiss="modal"
            class="btn btn-warning pull-right">
        <i class="fa fa-times"></i> Close
    </button>
</div>
<?php ?>