<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>AWL&SUNDRY Admin</title>

        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/admin/common.css' ?>" type="text/css" media="screen" />
        <!-- Colour Schemes
  
        Default colour scheme is green. Uncomment prefered stylesheet to use it.
        
        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
        
        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
 
        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.3.2.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/admin/common.js' ?>"></script>

        <!-- Internet Explorer .png-fix -->

        <!--[if IE 6]>
                <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('.png_bg, img, li');
                </script>
        <![endif]-->

    </head>

    <body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">AWL Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src='<?php echo base_url() . "assets/img/logo.png" ?>' alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a> you have <a href="#" title=""><?php echo $orders['cnt'] . ' ' . $orders['status'] ?> Orders</a>
                        <br />
                        <!--a href="<?php echo base_url() . 'index.php' ?>" title="View the Site">View the Site</a--> 
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        

                    <?php $this->load->view('admin/menu'); ?>

                    <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                        <h3>3 Messages</h3>

                        <p>
                            <strong>17th May 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br />
                            Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">

                            <h4>New Message</h4>

                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>

                            <fieldset>

                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>

                                <input class="button" type="submit" value="Send" />

                            </fieldset>

                        </form>

                    </div> <!-- End #messages -->

                </div></div> <!-- End #sidebar -->

            <div id="main-content"> <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>

                <!-- Page Head -->
                <h2>Welcome <?php echo $user['first_name']; ?></h2>
                <div>

                    <div class="clear"></div>
                    <br/>

                    <form action="<?php echo base_url() . 'index.php/admin/spring_view'; ?>" method="post" id="filters">
                        <div class="bulk-actions align-left" style="float:left;margin: -10px 0 30px;">
                            <select name="order_status" id="order_status">
                                <?php echo $order_status; ?>
                            </select>
                            <input type='text' name="order_number" id='order_number' value='<?php echo set_value('order_number', ''); ?>'  placeholder="Order Number" />
                            <input type='text' name="invoice_number" id='invoice_number' value='<?php echo set_value('invoice_number', ''); ?>'  placeholder="Invoice Number" />
                            <input type='text' name="customer_name" id='customer_name' value='<?php echo set_value('customer_name', ''); ?>'  placeholder="Customer Name" />
                            <select name="style" id="style">
                                <option value="">Style</option>
                                <?php echo $style; ?>
                            </select>
                            <select name="last" id="last">
                                <option value="">Last</option>
                                <?php echo $last; ?>
                            </select>
                            <input type='submit' class='submit button'  value='Filter' />
                        </div>
                    </form>

                </div>
                <!--p id="page-intro">What would you like to do?</p-->

                <ul class="shortcut-buttons-set">

                </ul><!-- End .shortcut-buttons-set -->

                <div class="clear"></div> <!-- End .clear -->

                <div class="content-box"><!-- Start Content Box -->

                    <div class="content-box-header">

                        <h3>Order Summary</h3>

                        <!--ul class="content-box-tabs">
                                <li><a href="#tab1" class="default-tab">Table</a></li> 
                                <li><a href="#tab2">Forms</a></li>
                        </ul-->

                        <div class="clear"></div>

                    </div> <!-- End .content-box-header -->

                    <div class="content-box-content">

                        <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                            <div class="notification attention png_bg">
                                    <!--a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a-->
                                <!--div>
                                        This is a Content Box. You can put whatever you want in it. By the way, you can close this notification with the top-right cross.
                                </div-->
                            </div>

                            <table id="grid">

                                <thead>
                                    <tr>
                                        <!--th><input class="check-all" type="checkbox" /></th-->
                                        <th>Order No</th>
                                        <th>Is rework</th>
                                        <th>Order by</th>
                                        <th>Shoe Cost</th>
                                        <th>Total No</th>
                                        <th>Ordered Date</th>
                                        <th>Shipped Date</th>
                                        <th>Total Days</th>
                                        <th>Shipping Location</th>
                                        <th>Style</th>
                                        <th>Manufacturing Cost</th>
                                        <th>Shipping Cost</th>
                                        <th>Shipping Carrier</th>
                                        <th>Tracking Number</th>
                                        <th>Promo code</th>
                                        <th>Ordered Status</th>
                                    </tr>

                                </thead>

                                <tfoot>
                                    <tr>
                                        <td colspan="7">
                                            <div class="pagination">
                                                <?php echo $pagination; ?> 
                                            </div>
                                            <div class="clear"></div>
                                        </td>
                                    </tr>
                                </tfoot>

                                <tbody>
                                    <?php
                                    $cnt = 0;
                                    foreach ($orders['rows'] as $order) {
                                        $cnt++;
                                        if ($order['order_id'] != NULL) {
                                            if ($order['discount_perc'] !== 0) {
                                                $disc = ($order['total_amt'] * $order['discount_perc']) / 100;
                                                $order['gross_amt'] = $order['total_amt'] - $disc;
                                            }
                                            $shipping_carrier = '';
                                            $tracking_number = '';
                                            if ($order['status_desc'] !== '') {
                                                $shipping_carrier = get_string_inbetween($order['status_desc'], 'Carrier : ', ', TrackingNo:');
                                                $tracking_number = get_string_inbetween($order['status_desc'], 'TrackingNo:', ', Estimated Delivery date :');
                                            }
                                            $promocode = '';
                                            if ($order['promocode_id'] !== '0') {
                                                $promocode = $order['promocode'] .' ('. (($order['discount_amount'] == 0) ? $order['discount_per'].'%' : '$'.$order['discount_amount']) . ')';
                                            }
                                            ?>
                                            <tr  style ='cursor:pointer;'>
                                                <!--td><input type="checkbox" /></td-->
                                                <td> <a href="<?php echo base_url() . 'index.php/admin/detail_view/' . $order['order_id']; ?>" title="title"><?php echo $order['order_id']; ?></a></td>
                                                <td><?php echo $order['is_rework']; ?></a></td>
                                                <td><?php echo $order['first_name'] . ' ' . $order['last_name']; ?></td>
                                                <td><?php echo round($order['gross_amt'] + $order['tax_amount']); ?></td>
                                                <td><?php echo $order['item_count']; ?></td>
                                                <td><?php echo date("m/d/Y h:ia", strtotime($order['order_modified'])); ?></td>
                                                <td><?php echo $order['shipped_date'] != '' ? date("m/d/Y h:ia", strtotime($order['shipped_date'])) : ''; ?></td>
                                                <td><?php echo $order['day_diff']; ?></td>
                                                <td><?php echo $order['location']; ?></td>
                                                <td><?php echo $order['style_name']; ?></td>
                                                <td><?php echo $order['manufacturing_cost']; ?></td>
                                                <td><?php echo $order['shipping_cost']; ?></td>

                                                <td><?php echo $shipping_carrier; ?></td>
                                                <td><?php echo $tracking_number; ?></td>
                                                <td><?php echo $promocode; ?></td>
                                                <td><?php echo $order['status']; ?></td>

                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>

                            </table>

                        </div> <!-- End #tab1 -->

                        <div class="tab-content" id="tab2">



                        </div> <!-- End #tab2 -->        

                    </div> <!-- End .content-box-content -->
                </div>

            </div> <!-- End .content-box -->


            <div class="clear"></div>




        </div> <!-- End #main-content -->
    </body>

</html>
