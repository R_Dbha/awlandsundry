<div>
    <div class="clear"></div>
    <br/>
    <form action="<?php echo base_url() . 'index.php/admin/customers'; ?>" method="post" id="filters">
        <div class="bulk-actions align-left" style="float:left;margin: -10px 0 30px;">
            <select name="customer_category" id="customer_category">
                <?php echo $customer_category; ?>
            </select>
            <input type='text' name="name" id='name' value='<?php echo set_value('name', ''); ?>'  placeholder="Name" />
            <input type='text' name="email" id='email' value='<?php echo set_value('email', ''); ?>'  placeholder="Email" />
            <input type='submit' class='submit button'  value='Filter' />
        </div>
    </form>
</div>
<ul class="shortcut-buttons-set">
</ul><!-- End .shortcut-buttons-set -->
<div class="clear"></div> <!-- End .clear -->
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Customers</h3>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
            <div class="notification attention png_bg">                
            </div>
            <table id="grid">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Number Of Orders</th>
                        <th>Customer Category</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="7">
                            <div class="pagination">
                                <?php echo $pagination; ?> 
                            </div>
                            <div class="clear"></div>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    foreach ($customers['rows'] as $customer) {
                        ?>
                        <tr  style ='cursor:pointer;'>
                            <td> <a href="<?php echo base_url() . 'index.php/admin/viewcustomer/' . $customer['user_id']; ?>" title="Click To View">
                                <?php echo $customer['first_name']; ?>
                                </a>
                            </td>
                            <td><?php echo $customer['last_name']; ?></td>
                            <td><?php echo $customer['email']; ?></td>
                            <td><?php echo $customer['number_of_orders']; ?></td>
                            <td><?php echo $customer['customer_category']; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div> <!-- End #tab1 -->
        <div class="tab-content" id="tab2">
        </div> <!-- End #tab2 -->        
    </div> <!-- End .content-box-content -->
</div>