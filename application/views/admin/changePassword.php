<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>AWL&SUNDRY Admin</title>

        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.3.2.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript">
            function validate(){
                if($('#password').val().trim() === ''){
                    alert('Please enter new password!');return false;
                }
                else if($('#confirmpassword').val().trim() === ''){
                    alert('Please enter confirm password!');return false;
                }
                if($('#password').val().trim() !== $('#confirmpassword').val().trim()){
                    alert('Invalid confirm password!');
                    $('#password').val('');
                    $('#confirmpassword').val('');
                    return false;
                }
                return true;
            }
        </script>
       
    </head>

    <body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">AWL Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src="<?php echo base_url() . "assets/img/logo.png" ?>" alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a>
                        <br />
                        <!--a href="<?php echo base_url() . 'index.php' ?>" title="View the Site">View the Site</a--> 
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        
 

                    <ul id="main-nav">  <!-- Accordion Menu -->
                        <?php if($user['user_type_id'] < 2){ ?>
                        <li>
                            <a href="<?php echo base_url().'index.php/admin/admin_view' ?>" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                                Orders
                            </a>       
                        </li>
                         <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/giftcards" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                                Gift Cards
                            </a>       
                        </li>
                         <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/payments" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                                Payments
                            </a>       
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'index.php/admin/promocode' ?>" class="nav-top-item no-submenu"> 
                                PromoCode
                            </a>       
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'index.php/admin/customers' ?>" class="nav-top-item no-submenu"> 
                                Customers
                            </a>       
                        </li>
                        <?php } else {?>
                        <li>
                            <a href="<?php echo base_url().'index.php/admin/vendor' ?>" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                                Orders
                            </a>       
                        </li>
                        <?php }?>
                        <li>
                            <a href="#" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                                Change Password
                            </a>       
                        </li>

                    </ul> <!-- End #main-nav -->

                    
                </div></div>
            <div id="main-content"> <!-- Main Content Section with everything -->
                    <noscript> <!-- Show a notification if the user has disabled javascript -->
                        <div class="notification error png_bg">
                            <div>
                                Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                            </div>
                        </div>
                    </noscript>

                    <!-- Page Head -->
                    <h2></h2>
                    <div>
                      
                    </div>
                    <!--p id="page-intro">What would you like to do?</p-->

                    <ul class="shortcut-buttons-set">

                    </ul><!-- End .shortcut-buttons-set -->
 
                    <div class="clear"></div> <!-- End .clear -->
                    <?php if($message != ''){?>
                            <div> <?php echo $message; ?></div>
                            <?php
                        }
                        ?>
                    <div class="content-box"><!-- Start Content Box -->
                       
                        <div class="content-box-header">
                            <h3>Change Password</h3>
                        </div> <!-- End .content-box-header -->

                        <div id="divpassword" style="padding:10px;width:400px;"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->
                       
                        <form action="<?php echo base_url().'index.php/admin/admin_changepass'?>" method="post" onsubmit="return validate();">
                            <fieldset>
                                <span style="min-width: 105px;display:block;float:left;line-height:24px;">New Password </span>
                                <input type="password" name="password" id="password"/><p></p>
                                <span style="min-width: 105px;display:block;float:left;line-height:24px;">Confirm Password</span>
                                <input type="password" name="confirmpassword" id="confirmpassword"/>
                                <input type="hidden" name="hid_user_id" value="<?php echo $user['user_id']; ?>"/>
                            </fieldset><br />
                            <input class="button" name="submit" type="submit" value="Update" style="margin-left:220px;"/>

                        </form>
            </div> </div><!-- End .content-box -->
            <div class="clear"></div>
        </div> <!-- End #main-content -->

        </div></body>

</html>
