<?php
$this->load->view('includes/admin_header');
?>

<div class="modal fade promocode-modal" id="promocode" tabindex="-1"
     role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<section class="content-header">
    <h1>
        PromoCodes <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">PromoCodes</li>
    </ol>
</section>

<section id="content" class="content" style="background-color:#FFFFFF;">
    <div class="box box-primary">
        <div class="overlay"></div>
        <div class="loading-img"></div>
        <div class="box-header" data-toggle="tooltip" title="" data-original-title="PromoCodes">
            <h3 class="box-title">PromoCodes</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-primary btn-xs" id="create-promocode">
                    <i class="fa fa-pencil"> Create New PromoCode</i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table id="promocodes"  class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>PromoCode</th>
                        <th>Discount Percentage</th>
                        <th>Discount Amount</th>
                        <th>Expiry Date</th>
                        <th>Available to All?</th>
                        <th>No.of Assigned Users</th>
                        <th>Used</th>
                        <th>View Usage</th>
                        <th>Assign</th>
                        <th>Edit</th>
                    </tr>
                <thead>

            </table>
        </div>
    </div>
</section>

<?php
$this->load->view('includes/admin_footer');
?>
