<div>
    <div class="clear"></div>
    <br/>
    <!--form action="<?php echo base_url() . 'index.php/admin/customers'; ?>" method="post" id="filters">
        <div class="bulk-actions align-left" style="float:left;margin: -10px 0 30px;">
            <select name="customer_category" id="customer_category">
    <?php echo $customer_category; ?>
            </select>
            <input type='text' name="name" id='name' value='<?php echo set_value('name', ''); ?>'  placeholder="Name" />
            <input type='text' name="email" id='email' value='<?php echo set_value('email', ''); ?>'  placeholder="Email" />
            <input type='submit' class='submit button'  value='Filter' />
        </div>
    </form-->
</div>
<ul class="shortcut-buttons-set">
</ul><!-- End .shortcut-buttons-set -->
<div class="clear"></div> <!-- End .clear -->
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Feedback From Customers</h3>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
            <div class="notification  png_bg" style="padding: 10px 10px 0px 15px;">      
                <h3>Average Score : <?php echo $feedback_score['average']*10; ?></h3>
            </div>
            <table id="grid">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Score</th>
                        <th>Comments</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="7">
                            <div class="pagination">
                                <?php //echo $pagination; ?> 
                            </div>
                            <div class="clear"></div>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($feedbacks as $feedback) {
                        $i++;
                        ?>
                        <tr  style ='cursor:pointer;'>

                            <td><?php echo $i; ?></td>
                            <td><?php echo $feedback['score'] * 10; ?></td>
                            <td><?php echo $feedback['comments']; ?></td>
                            <td><?php echo $feedback['email']; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div> <!-- End #tab1 -->
        <div class="tab-content" id="tab2">
        </div> <!-- End #tab2 -->        
    </div> <!-- End .content-box-content -->
</div>