<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <?php echo $css; ?>
        <title><?php echo $title; ?></title>
        <!--[if lte IE 7]>
        <?php echo $ie7css; ?>
        <![endif]-->
        <?php echo $js; ?>
    </head>
    <body>
        <div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
            <div id="sidebar">
                <div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->
                    <h1 id="sidebar-title"><a href="#">AWL Admin</a></h1>
                    <a href="#">
                        <img id="logo" src='<?php echo base_url() . "assets/img/logo.png" ?>' alt="Awl and Sundry" />
                    </a>
                    <?php echo $this->load->view($profile_link, $profile_link_data); ?>
                    <?php echo $this->load->view($menu); ?>
                </div>
            </div> <!-- End #sidebar -->
            <div id="main-content"> <!-- Main Content Section with everything -->
                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please 
                            <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a>
                            your browser or 
                            <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a>
                            Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>
                <!-- Page Head -->
                <h2>Welcome <?php echo $user['first_name']; ?></h2>
                <div >
                    <?php echo $this->load->view($content, $content_data); ?>
                </div>
            </div> <!-- End .content-box -->
            <div class="clear"></div>
        </div> <!-- End #main-content -->
    </body>
</html>
