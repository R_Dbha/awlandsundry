<div>
    <div class="clear"></div>
    <br/>
    <form action="" method="post" id="filters">

    </form>
</div>
<ul class="shortcut-buttons-set">
</ul><!-- End .shortcut-buttons-set -->
<div class="clear"></div> <!-- End .clear -->
<div class="content-box"><!-- Start Content Box -->
    <div class="content-box-header">
        <h3>Dashboard</h3>
        <div class="clear"></div>
    </div> <!-- End .content-box-header -->
    <div class="content-box-content">
        <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
            <div class="notification attention png_bg">   
            </div>
            <div id="dashboard">
                <div class="items" >
                    <table>
                        <thead>
                            <tr>
                                <th>Month/ Year</th>
                            </tr>
                            <tr>
                                <th>Total customer orders</th>
                            </tr>
                            <tr>
                                <th>Revenue</th>
                            </tr>
                            <tr>
                                <th>COGS</th>
                            </tr>
                            <tr>
                                <th>Gross Margin</th>
                            </tr>
                            <tr>
                                <th>Other expenses</th>
                            </tr>
                            <tr>
                                <th>Net Profit/ loss</th>
                            </tr> 
                        </thead>
                    </table>
                </div>
                <div class="data"> 
                    <?php foreach ($monthly_report as $month => $report) { ?>
                        <div class="month">
                            <table>
                                <thead>
                                    <tr>
                                        <th><?php echo $month; ?></th>
                                    </tr>
                                    <tr>
                                        <th>
                                            <?php echo $report['Total_customer_orders']; ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            $<?php echo $report['Revenue']; ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            $<?php echo $report['COGS']; ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="<?php echo $report['profit_loss']; ?>" >
                                            $<?php echo $report['Gross_margin']; ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            $<?php echo $report['Other_expenses']; ?>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="<?php echo $report['profit_loss']; ?>" >
                                            $<?php echo $report['Net_profit_loss']; ?>
                                        </th>
                                    </tr> 
                                </thead>
                            </table>
                        </div>
                    <?php } ?>
                </div>
                <div class="clear"></div>
                <br/>
                <br/>
                <div class="notification attention png_bg">   
                </div>
                <div class="yearly" >
                    <table>
                        <thead>
                            <tr>
                                <th>
                                    YTD Revenue
                                </th> 
                                <th>
                                    <?php echo $YTD_Revenue; ?>
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    YTD COGS
                                </th>
                                <th>
                                    $<?php echo $YTD_COGS; ?>
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    YTD Gross margin
                                </th>
                                <th class="<?php echo $profit_loss; ?>" >
                                   $<?php echo $YTD_Gross_margin; ?>
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    YTD other expenses
                                </th>
                                <th>
                                   $<?php echo $YTD_other_expenses; ?>
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    YTD Net Profit/ loss
                                </th>
                                <th class="<?php echo $profit_loss; ?>" >
                                   $<?php echo $YTD_Net_Profit_loss; ?>
                                </th>
                            </tr>

                            <tr>
                                <th>YTD customer orders</th>
                                <th>
                                    <?php echo $YTD_customer_orders; ?>
                                </th>
                            </tr> 
                        </thead>
                    </table>
                </div>
            </div>
            <div class="clear"></div>

        </div> <!-- End #tab1 -->
        <div class="tab-content" id="tab2">
        </div> <!-- End #tab2 -->        
    </div> <!-- End .content-box-content -->
</div>
