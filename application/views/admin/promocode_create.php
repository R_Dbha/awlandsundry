<?php ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">×</button>
    <h4 class="modal-title">
        <i class="fa  fa-folder-open-o"></i> <?php echo $edit_mode ? 'Edit' : 'Create New'; ?> PromoCode
    </h4>
</div>
<div class="modal-body">	
    <div class="row seperation">
        <div class="col-md-3">
        </div>
        <div class="col-md-6 error">
        </div>
    </div>
    <form id="promocode-form" >
        <div class="row seperation">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <span>PromoCode</span>
                        <input  type="text" name="promocode"
                                class="form-control"
                                value="<?php echo @$promocode['promocode']; ?>" />
                    </div>

                </div>
            </div>
        </div>
        <div class="row seperation">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <span>Discount Criteria</span>
                        <select name="discount_criteria"
                                class="form-control discount_criteria"
                                >
                            <option <?php echo $discount_criteria == 'percentage' ? 'selected="selected"' : ''; ?> value="percentage">Percentage</option>
                            <option <?php echo $discount_criteria == 'amount' ? 'selected="selected"' : ''; ?>  value="amount">Amount</option>
                        </select>
                    </div>

                </div>
            </div>
        </div>
        <div class="row seperation discount_amount" >
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <span>Discount Amount</span>
                        <input  type="text" name="discount_amount"
                                class="form-control"
                                value="<?php echo @$promocode['discount_amount']; ?>" />
                    </div>

                </div>
            </div>
        </div>
        <div class="row seperation discount_per" >
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <span>Discount Percentage</span>
                        <input  type="text" name="discount_per"
                                class="form-control"
                                value="<?php echo @$promocode['discount_per']; ?>" />
                    </div>

                </div>
            </div>
        </div>
        <div class="row seperation" >
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="row" style="margin: 0px;">
                    <div class="col-md-12 no-padding">
                        <span>Expiry Date(yyyy/mm/dd)</span>
                        <input  type="text" id="expiry_date" name="expiry_date" 
                                class="form-control"
                                value="<?php echo (isset($promocode['expiry_date']) && $promocode['expiry_date'] != 0 ) ? date('Y-m-d', strtotime($promocode['expiry_date'])) : date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd"/>
                    </div>

                </div>
            </div>
        </div>
        <div class="row seperation" >
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="row" style="margin: 0px;">
                    <span>Available To All</span>
                    <div class="col-md-12 no-padding">

                        <input  type="checkbox" name="is_common" class="is_common"

                                value="1" <?php echo isset($promocode['is_common']) && $promocode['is_common'] == 1 ? 'checked="checked"' : ''; ?> />
                    </div>

                </div>
            </div>
        </div>
        <div class="row seperation one_time">
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="row" style="margin: 0px;">
                    <span>First Use Only</span>
                    <div class="col-md-12 no-padding">

                        <input  type="checkbox" name="one_time"

                                value="1" <?php echo isset($promocode['one_time']) && $promocode['one_time'] == 1 ? 'checked="checked"' : ''; ?> />
                    </div>

                </div>
            </div>
        </div>

        <div class="row seperation" >
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
                <div class="row" style="margin: 0px;">
                    <span>Unlimited</span>
                    <div class="col-md-12 no-padding">

                        <input  type="checkbox" name="unlimited"

                                value="1" <?php echo isset($promocode['unlimited']) && $promocode['unlimited'] == 1 ? 'checked="checked"' : ''; ?> />
                    </div>

                </div>
            </div>
        </div>
        <input type="hidden" name = "promo_id" value="<?php echo @$promocode['promo_id']; ?>">
    </form>

</div>
<div class="modal-footer clearfix">

    <button type="button" class="btn btn-primary" id="save_promocode" >
        <i class="fa fa-envelope"></i> Save
    </button>

    <button type="button" data-dismiss="modal"
            class="btn btn-warning pull-right">
        <i class="fa fa-times"></i> Close
    </button>
</div>

<?php ?>