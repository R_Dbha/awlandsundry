<ul class="shortcut-buttons-set">

</ul><!-- End .shortcut-buttons-set -->

<!-- End .clear -->
<div style="clear:both;" ></div>
<div id="order_table" style = "overflow: hidden;">
    <form action="<?php echo base_url() . 'index.php/admin/inspirationReview'; ?>" method="post" id="next-page">
        <select name="status" id="status">
            <option value="accepted" <?php if($status == 'accepted'){ ?> selected="selected" <?php } ?>>Accepted</option>
            <option value="not" <?php if($status == 'not'){ ?> selected="selected" <?php } ?>>Not Accepted</option>
        </select>
        <input type="hidden" name="page-no" />
    </form>
    <table>
        <thead>
            <tr  style=" height: 40px;">
                <th style="width: 50%; height: 40px; text-align: center">Shoe Details</th>
                <th style="width: 50%; height: 40px; text-align: center">Shoe Image</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="7">
                    <div class="pagination">
                        <?php echo $pagination; ?> 
                    </div>
                    <div class="clear"></div>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php if(isset($newShoe[0])){ foreach ($newShoe as $shoes) { ?>
                <tr style=" height: 40px;">
                    <td style="width: 30%; height: 40px;">
                        <table class="same-background">
                            <tr>
                                <td width="40%">Name :</td>
                                <td width="60%"><?php echo $shoes['first_name'] . ' ' . $shoes['last_name'] ?></td>
                            </tr>
                            <tr>
                                <td>Email :</td>
                                <td><?php echo $shoes['email'] ?></td>
                            </tr>
                            <tr>
                                <td>Public Name :</td>
                                <td><?php echo $shoes['public_name'] ?></td>
                            </tr>
                            <tr>
                                <td>Published date :</td>
                                <td><?php echo $shoes['date_created'] ?></td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 30%; height: 40px; vertical-align: middle;">
                        <form action="<?php echo base_url() . 'index.php/admin/inspired/'.$shoes['shoe_design_id'] ?>" method="post">
                            Accept: <input type="radio" class="accept" name="shoe_<?php echo $shoes['shoe_design_id']; ?>" <?php if(!$shoes['is_approved']){ ?> checked="checked" <?php } ?> value="Accept">
                            Reject: <input type="radio" class="accept" name="shoe_<?php echo $shoes['shoe_design_id']; ?>" <?php if($shoes['is_approved']){ ?> checked="checked" <?php } ?> value="Reject">
                            <input type="hidden" name="page_no" class="accept" value="<?php echo $page; ?>">
                            <input type="hidden" name="status" class="accept" value="<?php echo $status; ?>">
                            <input type="submit" class="accept" value="Submit">
                        </form>
                    </td>
                    <td style="width: 40%; height: 40px; vertical-align: middle;">
                        <div id="shoe-img-thumb">                           
                            <?php $i = 0; ?>
                            <?php while ($i < 8) { ?>
                                <a data-fancybox-group = "design<?php echo $shoes['name'] ?>" href="<?php  echo ApplicationConfig::ROOT_BASE. 'files/designs/' . $shoes['name'] . '_A'.$i.'.png'; ?>" class="fancybox" >
                                    <img class="inspire-shoes inspire-shoes-A<?php echo $i ?> item-img shoe-design-new" src="<?php echo ApplicationConfig::ROOT_BASE. 'files/designs/' . $shoes['name'] . '_A'.$i.'.png'; ?>">
                                </a>
                                <?php $i++; ?>
                            <?php } ?>
                        </div>
                    </td>
                </tr>
            <?php } } ?>
        </tbody>
    </table>
</div>
<div class="clear"></div>
<!--a href="<?php echo base_url() . 'index.php/admin/pdf' ?>">Create Pdf</a-->
<div class="clear"></div>

<div id="footer">
    <small>
        &#169; Copyright 2013 | Powered by <a href="#"></a> | <a href="<?php echo base_url() . 'index.php/admin/admin_view' ?>">BACK</a>
    </small>
</div><!-- End #footer -->
</div> <!-- End #main-content -->
<script>
    $(function() {
        $('.shoe-img-thumb').hide();
        $('.inspire-shoes').hide();
        $('.shoe-img-thumb').show();
        $('.inspire-shoes-A7').show();
        $('.pagination .page').bind('click', function(e) {
            e.preventDefault();
            var page_number = $(this).attr('data-page-number');
            var form = $('#next-page');
            form.attr('action', form.attr('action') + '/' + page_number);
            $('#next-page').submit();
        });
         $(".fancybox").fancybox();
         $('#status').on('change',function(){             
            $('#next-page').submit();
         });
        /*$('.shoe-design-new').on('click', function(){
            console.log($(this).parent().find('.design-id').val());
            var shoeName = $(this).parent().find('.design-id').val();
            //alert(shoeName);
            var url = "<?php //echo base_url() ?>index.php/admin/admin_inspiration_shoes/" + shoeName;
           $.ajax({
            type: "GET",
            url: url ,
            success: function(data) {
                console.log(data);
                $(".accounts_selection_popup").append(data);
            }
        }); 
        });*/
        //ShoeDesign.designDetails('', ''); 
    });
</script>
</body>

</html>

