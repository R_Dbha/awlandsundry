<div class="box">
    <div class="overlay"></div>
    <div class="loading-img"></div>
    <div class="nav-tabs-custom">
        <!-- Tabs within a box -->
        <ul class="nav nav-tabs  ui-sortable-handle">
            <li class="active order-summary"><a href="#order-summary" data-toggle="tab">Order Summary</a></li>
            <?php foreach ($order['items'] as $key => $item) { ?>
                <li class="items"><a href="#item-<?php echo $key + 1; ?>" data-toggle="tab">Item <?php echo $key + 1; ?><i title="Delete Item" class="delete-item fa fa-times pull-right"></i></a></li>
            <?php } ?>
            <li class="edit-btn" id="add-item-btn"><a href="#add-item" data-toggle="tab">Add Item</a></li>
        </ul>
        <div class="tab-content ">
            <div class="chart tab-pane active" id="order-summary">
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Order Details</h3>
                        </div>
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Order Id :</dt>
                                <dd id="order_id"><?php echo $order_summary[0]['order_id']; ?></dd>
                                <dt>Invoice Number :</dt>
                                <dd><?php echo $order_summary[0]['invoice_no']; ?></dd>
                                <div class="box-body">
                                    <div id="edit-status" class="not-editable">
                                        <div class="box-tools pull-right">
                                            <button id="btn-edit-status" class="btn btn-primary btn-xs edit-btn"><i class="fa fa-pencil"> Edit</i></button>
                                        </div>
                                        <dl class="dl-horizontal">
                                            <dt>Status :</dt><dd id="status"><?php echo $status[0]['status']; ?></dd>
                                        </dl>
                                        <?php if (isset($vendor) && $status[0]['status'] == "Manufacturing in progress") { ?>
                                            <dl class="dl-horizontal">
                                                <dt>Vendor :</dt><dd id="show-vendor"><?php echo $vendor['first_name'] . ' ' . $vendor['last_name']; ?></dd>
                                            </dl>
                                        <?php } ?>
                                    </div>
                                    <div id="show-status" class="editable">
                                        <div class="row seperation">
                                            <div class="col-md-4 right"><dt>Status :</dt></div>
                                            <div class="col-md-6">
                                                <select name="status-select" id="status-select" class="form-control">
                                                    <option selected="true" value="<?php echo $status[0]['status']; ?>"disabled  >Select Status</option>                                             
                                                    <option  value="Manufacturing in progress">Manufacturing in progress</option>
                                                    <option  value="Refund">Refund</option>
                                                    <option  value="Testing">Testing</option>
                                                    <option  value="Cancelled">Cancelled</option>
                                                    <option  value="order shipped">Order Shipped</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row seperation "id="vendor">
                                            <div class="col-md-4 "><dt>choose vendor :</dt></div>
                                            <div class="col-md-6">
                                                <select name="vendor-select" id="vendor-select" class="form-control">
                                                    <option disabled>choose vendor</option>
                                                    <?php foreach ($vendors as $vendor) { ?>
                                                        <option value="<?php echo $vendor['user_id'] ?>"><?php echo $vendor['first_name'] . ' ' . $vendor['last_name'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div> 
                                        <div id="shipped">
                                            <div class="row seperation">
                                                <div class="col-md-4 "><dt>Tracking No :</dt></div>
                                                <div class="col-md-6"><input type="text" name="trackingno" id="trackingno" class="form-control" ></div>
                                            </div>
                                            <div class="row seperation">
                                                <div class="col-md-4 "><dt>Carrier :</dt></div>
                                                <div class="col-md-6"><input type="text" name="carrier" id="carrier" class="form-control" ></div>
                                            </div>
                                            <div class="row seperation" data-provide="datepicker-inline">
                                                <div class="col-md-4 "><dt>Shipping Date :</dt></div>
                                                <div class="col-md-6"><input type="date" name="shipping_date" id="shippingDate" data-date-format="mm/dd/yyyy" class="form-control " ></div>
                                            </div>
                                            <div class="row seperation" data-provide="datepicker-inline">
                                                <div class="col-md-4 "><dt>Delivary Date :</dt></div>
                                                <div class="col-md-6"><input type="date" name="delivery_date" id="deliveryDate" class="form-control " data-date-format="mm/dd/yyyy" ></div>
                                            </div>
                                        </div>
                                        <div id="refund">
                                            <div class="row seperation">
                                                <div class="col-md-4 "><dt>Refund Amount :</dt></div>
                                                <div class="col-md-6"><input type="text" name="refundamount" id="refundamount" value="<?php echo ($total_amount - $disc); ?>" class="form-control" ></div>
                                            </div>
                                            <div class="row seperation">
                                                <div class="col-md-4 "><dt>Refund Reason :</dt></div>
                                                <div class="col-md-6"><input type="text" name="refundreason" id="refundreason" class="form-control" ></div>
                                            </div>
                                        </div>
                                        <div class="row seperation">
                                            <div class="col-md-4 "></div>
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-success" id="done-edit-status">Done</button>
                                                <button type="button" class="btn btn-reset cancel-edit">Cancel</button>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if ($status[0]['status'] == "order shipped") { ?>
                                        <div id="showtrackingNo" class="not-editable">
                                            <div class="box-tools pull-right">
                                                <button id="btn-edit-trackingNo" class="btn btn-primary btn-xs edit-btn">
                                                    <i class="fa fa-pencil"> Edit</i>
                                                </button>
                                            </div>
                                            <dl class="dl-horizontal">
                                                <dt>Tracking No :</dt>
                                                <?php $subject = $status[0]['status_desc']; ?>
                                                <?php $trackingNo = preg_split("/[\:,]+/", $subject); ?>
                                                <dd id="trackingNo"><?php print_r($trackingNo[5]); ?></dd>
                                            </dl>  
                                        </div>
                                    <?php } ?>
                                    <div id="edit_trackingNo" class="editable">
                                        <div class="row seperation">
                                            <div class="col-md-4 right"><dt>Tracking No :</dt></div>
                                            <div class="col-md-6"><input type="text" name="edittrackingno" id="edittrackingno" value="<?php print_r($trackingNo[5]); ?>" class="form-control" ></div>  
                                        </div>
                                        <div class="row seperation">
                                            <div class="col-md-4 "></div>
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-success" id="done-edit-trackingNo">Done</button>
                                                <button type="button" class="btn btn-reset cancel-edit">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <dt>Status Description :</dt>
                                <dd id="status_desc"><?php echo $status[0]['status_desc']; ?></dd>
                                <dt>Belt Needed :</dt>
                                <dd><?php echo $order_summary[0]['belt_needed'] == 1 ? 'Yes' : 'No'; ?></dd>
                                <dt>Shoe Horn :</dt>
                                <dd><?php echo $order_summary[0]['shoe_horn'] == 1 ? 'Yes' : 'No'; ?></dd>
                                <dt>Shoe Trees :</dt>
                                <dd><?php echo $order_summary[0]['shoe_trees'] == 1 ? 'Yes' : 'No'; ?></dd>
                            </dl>
                        </div>

                        <div class="box-header">
                            <i class="fa fa-dollar"></i> <h3 class="box-title">Payment Details</h3>
                        </div>

                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt>Subtotal :</dt>
                                <dd id="sub-total"><?php echo $total_amount; ?></dd>
                                <dt>Shipping Cost :</dt>
                                <dd><?php echo @$shipping_cost; ?></dd>
                                <dt><?php echo $discount_type; ?> Discount :</dt>
                                <dd id="discount"><?php echo $disc; ?></dd>
                                <dt>Tax :</dt>
                                <dd id="tax"><?php echo $tax; ?></dd>
                                <dt>Grand Total :</dt>
                                <dd id="grand-total"><?php echo $grand_total; ?></dd>
                            </dl>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-11 <?php echo $disc ? 'hidden' : ''; ?>" id = "apply">
                                    <div class="row seperation">
                                        <div class="col-md-2 no-padding">Promocode</div>
                                        <div class="col-md-4 no-padding"><input type="text" class="form-control promocode" ></div>
                                        <div class="col-md-2 ">
                                            <button id="btn-apply-promo" class="btn btn-primary btn-xs edit-btn">
                                                <i class="fa fa-pencil"> Apply Promocode</i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="row seperation">
                                        <div class="col-md-2 no-padding">Gift card</div>
                                        <div class="col-md-4 no-padding"><input type="text" class="form-control giftcard" ></div>
                                        <div class="col-md-2 ">
                                            <button id="btn-apply-giftcard" class="btn btn-primary btn-xs edit-btn">
                                                <i class="fa fa-pencil"> Apply giftcard</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button id="btn-remove-promo" class="btn btn-primary btn-xs edit-btn <?php echo $disc && $discount_type == 'PROMOTION' ? '' : 'hidden'; ?>">
                                <i class="fa fa-pencil"> Remove Promocode</i>
                            </button>

                            <button id="btn-remove-giftcard" class="btn btn-primary btn-xs edit-btn <?php echo $disc && $discount_type == 'GIFT CARD' ? '' : 'hidden'; ?>">
                                <i class="fa fa-pencil"> Remove giftcard </i>
                            </button>
                        </div>

                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">User Details</h3>
                            <div class="box-tools pull-right">
                                <button id="btn-edit-user" class="btn btn-primary btn-xs edit-btn">
                                    <i class="fa fa-pencil"> Edit</i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="user-details" class="not-editable">
                                <dl class="dl-horizontal">
                                    <dt>Name :</dt>
                                    <dd><?php echo $billing[0]['first_name'] . ' ' . $billing[0]['last_name']; ?></dd>
                                    <dt>Email :</dt>
                                    <dd><?php echo @$billing[0]['email']; ?></dd>
                                </dl>
                            </div>
                            <div id="edit-user-details" class="editable">
                                <div class="row seperation">
                                    <div class="col-md-4 right">First Name </div>
                                    <div class="col-md-6">
                                        <input type="text" id="user-first-name" class="form-control" value="<?php echo $billing[0]['first_name']; ?>" />
                                    </div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-md-4 right">Last Name</div>
                                    <div class="col-md-6">
                                        <input type="text" id="user-last-name" class="form-control" value="<?php echo $billing[0]['last_name']; ?>" />
                                    </div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-md-4 right">Email</div>
                                    <div class="col-md-6">
                                        <input type="text" id="user-email" class="form-control" value="<?php echo $billing[0]['email']; ?>" />
                                    </div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-md-4 "></div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-success" id="done-edit-user">Done</button>
                                        <button type="button" class="btn btn-reset cancel-edit">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="overlay"></div>
                        <div class="loading-img"></div>
                        <div class="box-header">
                            <i class="fa fa-envelope-o"></i>
                            <h3 class="box-title">Billing address</h3>
                            <div class="box-tools pull-right">
                                <button id="btn-edit-billing" class="btn btn-primary btn-xs edit-btn">
                                    <i class="fa fa-pencil"> Edit</i>
                                </button>
                            </div>
                        </div>

                        <div class="box-body">
                            <div id="billing-address" class="not-editable">
                                <?php $this->load->view('admin/order_billing_address_list', array('billing' => $billing)); ?>
                            </div>
                            <div id="edit-billing-address" class="editable">
                                <?php $this->load->view('admin/order_billing_address', array('billing' => $billing)); ?>
                                <div class="row seperation">
                                    <div class="col-md-4 "></div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-success" id="done-edit-billing">Done</button>
                                        <button type="button" class="btn btn-reset cancel-edit">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-solid">
                        <div class="overlay"></div>
                        <div class="loading-img"></div>
                        <div class="box-header">
                            <i class="fa  fa-envelope-o"></i>
                            <h3 class="box-title">Shipping address</h3>
                            <div class="box-tools pull-right">
                                <button id="btn-edit-shipping" class="btn btn-primary btn-xs edit-btn">
                                    <i class="fa fa-pencil"> Edit</i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="shipping-address" class="not-editable">
                                <?php $this->load->view('admin/order_shipping_address_list', array('shipping' => $shipping)); ?>
                            </div>

                            <div id="edit-shipping-address" class="editable">

                                <?php $this->load->view('admin/order_shipping_address', array('shipping' => $shipping)); ?>

                                <div class="row seperation">
                                    <div class="col-md-4 "></div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-success"
                                                id="done-edit-shipping">Done</button>
                                        <button type="button" class="btn btn-reset cancel-edit">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <div class="box box-solid">
                        <div class="overlay"></div>
                        <div class="loading-img"></div>
                        <div class="box-header">
                            <i class="fa  fa-envelope-o"></i>
                            <h3 class="box-title">Comments</h3>
                            <div class="box-tools pull-right">
                                <button id="btn-edit-comment" class="btn btn-primary btn-xs edit-btn">
                                    <i class="fa fa-pencil"> Edit</i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="comment" class="not-editable ">
                                <?php echo $order_summary[0]['comments']; ?>
                            </div>
                            <div id="edit-comment" class="editable">
                                <div class='row seperation'>
                                    <div class="col-md-10">
                                        <textarea class="form-control"><?php echo $order_summary[0]['comments']; ?></textarea>
                                    </div>
                                </div>
                                <div class='row seperation'>
                                    <div class="col-md-10">
                                        <button type="button" class="btn btn-success" id="done-edit-comment">Done</button>
                                        <button type="button" class="btn btn-reset cancel-edit">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view('admin/order_items', array('order' => $order)); ?>
            <div class="chart tab-pane add-edit" id="add-item">
                <?php
                $edit['key'] = $key;
                $this->load->view('admin/order_add_item', $edit);
                ?>
            </div>
        </div>
    </div>
</div>