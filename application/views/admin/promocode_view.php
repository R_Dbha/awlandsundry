<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>AWL&SUNDRY Admin</title>

        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/admin/common.css' ?>" type="text/css" media="screen" />
        <!-- Colour Schemes
  
        Default colour scheme is green. Uncomment prefered stylesheet to use it.
        
        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
        
        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
 
        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.3.2.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/admin/common.js' ?>"></script>
        <script type="text/javascript">

            $(document).ready(function() {
<?php
if ($saved) {
    echo 'alert("Promocode Saved Successfully");';
}
?>
//                $('#grid tbody tr').off('click touchend').on('click touchend', function() {
//                    window.location = $(this).attr('href');
//                    return false;
//                });
            });


        </script>
        <!-- Internet Explorer .png-fix -->

        <!--[if IE 6]>
                <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('.png_bg, img, li');
                </script>
        <![endif]-->

    </head>

    <body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">AWL Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src='<?php echo base_url() . "assets/img/logo.png" ?>' alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a> 

                        <br/>
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        

                    <?php
                    if ($user_type_id == 4) {
                        $this->load->view('admin/intern_menu');
                    } else {
                        $this->load->view('admin/menu');
                    }
                    ?>

                    <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                        <h3>3 Messages</h3>

                        <p>
                            <strong>17th May 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br />
                            Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">

                            <h4>New Message</h4>

                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>

                            <fieldset>

                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>

                                <input class="button" type="submit" value="Send" />

                            </fieldset>

                        </form>

                    </div> <!-- End #messages -->

                </div></div> <!-- End #sidebar -->

            <div id="main-content"> <!-- Main Content Section with everything -->


                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>

                <!-- Page Head -->
                <h2>Welcome <?php echo $user['first_name']; ?></h2>
                <div>
                    <fieldset>


                        <a class="button" style="padding:20px;" href="<?php echo base_url() . 'index.php/admin/promo' ?>" >
                            <span >Create New Promocode</span>
                        </a>

                    </fieldset>
                </div>

                <!--p id="page-intro">What would you like to do?</p-->

                <ul class="shortcut-buttons-set">


                </ul><!-- End .shortcut-buttons-set -->

                <div class="clear"></div> <!-- End .clear -->
                <form action="<?php echo base_url() . 'index.php/admin/promocode'; ?>" method="post" id="filters">
                    <div class="bulk-actions align-left" style="float:left;margin: -10px 0 30px;">
                        <input type='text' name="promocode" id='promocode' value='<?php echo set_value('promocode', ''); ?>'  placeholder="promocode" />
                        <input type='hidden' name="email" id='email' value='<?php //echo set_value('email', '');    ?>'  placeholder="Email" />
                        <input type='submit' class='button'  value='Filter'  />
                    </div>
                </form>
                <div class="clear"></div> <!-- End .clear -->
                <div class="content-box"><!-- Start Content Box -->

                    <div class="content-box-header">

                        <h3>PromoCode Details</h3>



                        <div class="clear"></div>

                    </div> <!-- End .content-box-header -->

                    <div class="content-box-content">

                        <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                            <div class="notification attention png_bg">

                            </div>

                            <table id="grid1">

                                <thead>
                                    <tr>
                                        <!--th><input class="check-all" type="checkbox" /></th-->
                                        <th>#</th>
                                        <th>PromoCode</th>
                                        <th>Discount Percentage</th>
                                        <th>Discount Amount</th>
                                        <th>Expiry Date</th>
                                        <th>Number of Assigned Users</th>
                                        <th>Used</th>
                                        <th>Available To all</th>
                                        <th></th>
                                    </tr>

                                </thead>

                                <tfoot>
                                    <tr>
                                        <td colspan="6">
                                            <div class="pagination">
<?php echo $pagination; ?> 
                                            </div>
                                            <div class="clear"></div>
                                        </td>
                                    </tr>
                                </tfoot>

                                <tbody>
                                    <?php
                                    $cnt = 0;
                                    $netcnt = $page + $cnt;
                                    foreach ($promo[$cnt] as $row) {
                                        $cnt++;
                                        ?>
                                        <tr  style ='cursor:pointer;'>

                                            <td><?php
                                                echo $netcnt + 1;
                                                $netcnt++;
                                                ?></td>
                                            <td>
                                                <a href="<?php echo base_url() . 'index.php/admin/assignusers/' . $row['promo_id']; ?>" title="title">
    <?php echo $row['promocode']; ?>
                                                </a>
                                            </td>
                                            <td><?php echo $row['discount_per']; ?></td>
                                            <td><?php echo $row['discount_amount']; ?></td>
                                            <td><?php echo date("m/d/Y", strtotime($row['expiry_date'])); ?></td>
                                            <td><?php echo $row['assigned_users']; ?></td>
                                            <td><?php echo $row['used_users']; ?></td>
                                            <td><?php echo ($row['is_common'] == 1) ? 'Yes' : 'No'; ?></td>
                                            <td><a href="<?php echo base_url() . 'index.php/admin/promo/' . $row['promo_id']; ?>" title="title">
                                                    Edit
                                                </a>
                                            </td>


                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>

                            </table>

                        </div> <!-- End #tab1 -->

                        <div class="tab-content" id="tab2">



                        </div> <!-- End #tab2 -->        

                    </div> <!-- End .content-box-content -->
                </div> 


                <div class="clear"></div>



            </div> <!-- End #main-content -->

        </div></body>

</html>
