<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>AWL&SUNDRY Admin</title>

        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/cupertino/jquery-ui-1.10.4.custom.min.css' ?>" type="text/css" media="screen" />
       
        <!-- Colour Schemes
  
        Default colour scheme is green. Uncomment prefered stylesheet to use it.
        
        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
        
        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
 
        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.10.2.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-ui-1.10.4.custom.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#expiryDate').datepicker({dateFormat: 'yy-mm-dd'});
                $('#discount_criteria').change(changeDiscountMode);
                changeDiscountMode();
                $('#is_common').change(is_common);
                is_common();
                function is_common(){
                    console.log($('#is_common').prop('checked'));
                    if($('#is_common').prop('checked')){
                        $('#one_time').parent('span').show();
                    }else{
                        $('#one_time').parent('span').hide();
                    }
                }
                function changeDiscountMode() {
                    if ($('#discount_criteria').val() === 'percentage') {
                        $('#discount_amount').hide();
                        $('#discount_per').show();
                    }else{
                        $('#discount_amount').show();
                        $('#discount_per').hide();
                    }
                }
            });
        </script>
        <!-- Internet Explorer .png-fix -->

        <!--[if IE 6]>
                <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('.png_bg, img, li');
                </script>
        <![endif]-->

    </head>

    <body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">AWL Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src='<?php echo base_url() . "assets/img/logo.png" ?>' alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a> 

                        <br/>
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        

                    <?php
                    if ($user_type_id == 4) {
                        $this->load->view('admin/intern_menu');
                    } else {
                        $this->load->view('admin/menu');
                    }
                    ?>

                    <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                        <h3>3 Messages</h3>

                        <p>
                            <strong>17th May 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br />
                            Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">

                            <h4>New Message</h4>

                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>

                            <fieldset>

                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>

                                <input class="button" type="submit" value="Send" />

                            </fieldset>

                        </form>

                    </div> <!-- End #messages -->

                </div></div> <!-- End #sidebar -->

            <div id="main-content"> <!-- Main Content Section with everything -->

                <noscript> <!-- Show a notification if the user has disabled javascript -->
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>

                <!-- Page Head -->
                <h2>Welcome <?php echo $user['first_name']; ?></h2>
                <div>

                </div>
                <!--p id="page-intro">What would you like to do?</p-->

                <ul class="shortcut-buttons-set">


                </ul><!-- End .shortcut-buttons-set -->

                <div class="clear"></div> <!-- End .clear -->

                <div class="content-box"><!-- Start Content Box -->

                    <div class="content-box-header">

                        <h3>Enter PromoCode Details</h3>



                        <div class="clear"></div>

                    </div> <!-- End .content-box-header -->

                    <div class="content-box-content">

                        <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                            <div class="notification attention png_bg">

                            </div>
                            <form method="POST" action="<?php echo base_url() . 'index.php/admin/promo' ?>" >
                                <table id="grid" >




                                    <tbody>
                                        <tr>
                                            <td width="150">
                                                Promo Code : 
                                            </td>
                                            <td><?php echo form_error('promocode'); ?>
                                                <input type ="text" class="" name="promocode" required value="<?php echo set_value('promocode', isset($promo['promocode']) ? $promo['promocode'] : ''); ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Discount criteria :  
                                            </td>
                                            <td>
                                                <?php echo form_error('discount_criteria'); ?>
                                                <select name="discount_criteria" id="discount_criteria" >
                                                    <option value="percentage" <?php if (set_value('discount_criteria', isset($promo['discount_criteria']) ? $promo['discount_criteria'] : '') === "percentage") { ?>selected = "selected" <?php } ?> >Percentage</option>
                                                    <option value="amount" <?php if (set_value('discount_criteria', isset($promo['discount_criteria']) ? $promo['discount_criteria'] : '') === "amount") { ?>selected = "selected" <?php } ?> >Amount</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr id="discount_per" <?php if (set_value('discount_criteria', 0) === "amount") { ?>style="display: none;"<?php } ?> >
                                            <td>
                                                Discount Percentage :  
                                            </td>
                                            <td>
                                                <?php echo form_error('discount_per'); ?>
                                                <input type ="text" class="" name="discount_per" value="<?php echo set_value('discount_per', isset($promo['discount_per']) ? $promo['discount_per'] : ''); ?>" />
                                            </td>
                                        </tr>
                                        <tr id="discount_amount" <?php if (set_value('discount_criteria', 0) === "percentage") { ?>style="display: none;"<?php } ?>>
                                            <td>
                                                Discount Amount :  
                                            </td>
                                            <td>
                                                <?php echo form_error('discount_amount'); ?>
                                                <input type ="text" class="" name="discount_amount"  value="<?php echo set_value('discount_amount', isset($promo['discount_amount']) ? $promo['discount_amount'] : ''); ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Expiry Date :
                                            </td>
                                            <td>
                                                <?php echo form_error('expiry_date'); ?>
                                                <input type ="text"  id = "expiryDate" name="expiry_date" autocomplete="off" placeholder="YYYY/MM/DD" required value="<?php echo set_value('expiry_date', isset($promo['expiry_date']) ? date('Y-m-d', strtotime($promo['expiry_date'])) : '' ); ?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Available to all? :
                                            </td>
                                            <td>
                                                <?php echo form_error('is_common'); ?>
                                                <input type ="checkbox" id="is_common"  name="is_common" <?php if ($is_common = set_value('is_common', isset($promo['is_common']) ? $promo['is_common'] : '0' )) {  echo 'checked';  } ?> value="1"  /> 
                                                <span style="display: none; display: inline-block;">
                                                    One time : 
                                                    <input type ="checkbox" id="one_time"  name="one_time"  <?php if ($one_time = set_value('one_time', isset($promo['one_time']) ? $promo['one_time'] : '0' )) {  echo 'checked';  } ?> value="1"  /> 
                                                </span>                                            
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tr>
                                        <td>

                                        </td>
                                        <td>
                                            <input type ="submit" class="" name="submit"  value="Submit" />
                                            <input type ="hidden" class="" name="promo_id"  value="<?php echo set_value('promo_id', isset($promo['promo_id']) ? $promo['promo_id'] : ''); ?>" />
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div> <!-- End #tab1 -->

                        <div class="tab-content" id="tab2">



                        </div> <!-- End #tab2 -->        

                    </div> <!-- End .content-box-content -->

                </div> 


                <div class="clear"></div>



            </div> <!-- End #main-content -->

        </div></body>

</html>
