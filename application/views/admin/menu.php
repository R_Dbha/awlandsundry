<ul id="main-nav">
    <!-- Accordion Menu -->
    <li><a href="<?php echo base_url(); ?>index.php/admin/index"
           class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Dashboard
        </a></li>

    <li><a 
            class=" new-tab nav-top-item no-submenu"> Kick Starter Orders </a></li>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.new-tab').click(function (e) {
                e.preventDefault();
                window.open('<?php echo base_url(); ?>orders/kso', '_blank');
                return false;
            });
        });
    </script>
    <li><a href="<?php echo base_url(); ?>index.php/orders/mto"
           class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Edit Orders
        </a></li>
    <li><a href=""
           class="nav-top-item <?php
           if ($this->router->method == 'admin_view' || $this->router->method == 'spring_view' || $this->router->method == 'parnter_view') {
               echo 'current';
           }
           ?>"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Orders
        </a>
        <ul>
            <li><a href="<?php echo base_url(); ?>index.php/admin/admin_view"
                   class="<?php
                   if ($this->router->method == 'admin_view') {
                       echo 'current';
                   }
                   ?>">MTO Hand Welt</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/admin/spring_view"
                   class="<?php
                   if ($this->router->method == 'spring_view') {
                       echo 'current';
                   }
                   ?>"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                    Spring
                </a></li>
            <li><a href="<?php echo base_url(); ?>index.php/admin/partner_view"
                   class="<?php
                   if ($this->router->method == 'partner_view') {
                       echo 'current';
                   }
                   ?>"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
                    Partner Orders
                </a></li>
        </ul></li>

    <!--li>
    <a href="<?php //echo base_url();    ?>index.php/admin/return_view" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
    <!-- Returns
</a>       
</li-->
    <li><a href=""
           class="nav-top-item <?php
           if ($this->router->method == 'reports' || $this->router->method == 'designs_vs_orders') {
               echo 'current';
           }
           ?>"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Reports
        </a>
        <ul>
            <li><a
                    href="<?php echo base_url(); ?>index.php/admin/designs_vs_orders"
                    class="<?php
                    if ($this->router->method == 'designs_vs_orders') {
                        echo 'current';
                    }
                    ?>">Designs Vs. Orders</a></li>
            <!--<li>
<a href="<?php echo base_url(); ?>index.php/admin/spring_view" class="<?php
            if ($this->router->method == 'spring_view') {
                echo 'current';
            }
            ?>">
Spring
</a>       
</li>-->
        </ul></li>
    <li><a href="<?php echo base_url(); ?>index.php/admin/feedback"
           class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            NPS
        </a></li>

    <li><a href="<?php echo base_url(); ?>giftcard/view"
           class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Gift Cards
        </a></li>
    <li><a href="<?php echo base_url(); ?>index.php/admin/payments"
           class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            Payments
        </a></li>
    <li><a href="<?php echo base_url(); ?>promocode"
           class="nav-top-item no-submenu"> PromoCode </a></li>
    <li><a href="<?php echo base_url(); ?>index.php/admin/customers"
           class="nav-top-item no-submenu"> Customers </a></li>
    <li><a
            href="<?php echo base_url(); ?>index.php/admin/inspirationReview"
            class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
            New Designs
        </a></li>
    <li><a
            href="<?php echo base_url() . 'index.php/admin/admin_changepass' ?>"
            class="nav-top-item no-submenu"> Change Password </a></li>
</ul>
<!-- End #main-nav -->
