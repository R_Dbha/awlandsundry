<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/cupertino/jquery-ui-1.10.4.custom.min.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/admin/common.min.css' ?>" type="text/css" media="screen" />

        <!-- jQuery -->
        <style>
            .bulk-actions.align-left>select {padding: 3px 5px;}
            .bulk-actions.align-left>input[type="text"], .bulk-actions.align-left>select {border: 1px solid #CCCCCC; border-radius: 5px 5px 5px 5px; margin-right: 5px; padding: 5px; }
            .bulk-actions.align-left>input[type="text"]:focus, .bulk-actions.align-left>select:focus { border-color: blue; }
        </style>

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.10.2.js' ?>"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-ui-1.10.4.custom.min.js' ?>"></script>

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>

        <script type="text/javascript" src="<?php echo base_url() . 'assets/js/admin/common.js' ?>"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#txtShipDate').datepicker({minDate: '-30', maxDate: '+10'});
                $('#txtDeliveryDate').datepicker({minDate: 0, maxDate: '+60'});

                $('#dropdown_status').change(function () {
                    if ($('#dropdown_status option:selected').text() === 'Order Shipped') {
                        $('#txtTrackNo').show();
                        $('#txtTrackNo').focus();
                        $('#txtCarrier').show();
                        $('#txtShipDate').show();
                        $('#txtDeliveryDate').show();
                        $('.submit').show();
                    } else {
                        $('#txtTrackNo').hide();
                        $('#txtCarrier').hide();
                        $('#txtShipDate').hide();
                        $('#txtDeliveryDate').hide();
                        $('.submit').hide();
                    }
                });
            });

            function validateCost() {
                if ($('#txtManufacturingCost').val().trim() == "") {
                    alert("Please enter the Manufacturing Cost.");
                    return false;
                }
                if ($('#txtShippingCost').val().trim() == "") {
                    alert("Please enter the Shipping Cost ");
                    return false;
                }
                return true;
            }
            function validate() {
                var a;
                if ($('#txtTrackNo').val().trim() !== "") {
                    a = '';
                } else {
                    alert("Please enter the correct Tracking number.");
                    return false;
                }
                if ($('#txtCarrier').val().trim() !== "") {
                    a = '';
                } else {
                    alert("Please enter carrier details ");
                    return false;
                }
                if (document.getElementById("txtShipDate").value === "") {
                    alert("Please enter the Date..!!");
                    $("#txtShipDate").focus();
                    return false;
                }
                if (document.getElementById("txtDeliveryDate").value === "") {
                    alert("Please enter the Delivery Date..!!");
                    $("#txtDeliveryDate").focus();
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <?php $tempSize = ["Regular" => "D", "Wide" => "E", "Extra Wide" => "EE", "Triple Wide" => "EEE"] ?>
        <div id="body-wrapper">
            <div id="sidebar">
                <div id="sidebar-wrapper">
                    <h1 id="sidebar-title"> <a href="#"></a> </h1>
                    <a href="#"><img id="logo" style="background-color: #fff; width: 217px;" src="<?php echo base_url() . "assets/img/logo.png" ?>" alt="Simpla Admin logo" /></a>
                    <div id="profile-links">
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>

                    <ul id="main-nav">
                        <li><a href="<?php echo base_url() . 'admin/vendor' ?>" class="nav-top-item no-submenu"> Orders </a></li>
                    </ul>
                    <div id="messages" style="display: none">
                        <h3>3 Messages</h3>
                        <p>
                            <strong>17th May 2009</strong> by Admin<br /> Lorem ipsum dolor
                            sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi
                            at felis aliquet congue. <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br /> Ut a est eget
                            ligula molestie gravida. Curabitur massa. Donec eleifend, libero
                            at sagittis mollis, tellus est malesuada tellus, at luctus turpis
                            elit sit amet quam. Vivamus pretium ornare est. <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br /> Lorem ipsum dolor
                            sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi
                            at felis aliquet congue. <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">
                            <h4>New Message</h4>
                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>
                            <fieldset>
                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>
                                <input class="button" type="submit" value="Send" />
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div id="main-content">
                <noscript>
                    <div class="notification error png_bg">
                        <div>
                            Javascript is disabled or is not supported by your browser. Please
                            <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                        </div>
                    </div>
                </noscript>
                <ul class="shortcut-buttons-set">
                </ul>
                <div class="clear"></div>
                <div class="content-box-header">
                    <h3>Order Details (Status : <?php echo $status[0]['status']; ?>)</h3>
                    <div class="clear"></div>
                </div>

                <?php if ($status[0]['status'] == "Manufacturing in progress") { ?>
                    <div class="content-box-content">
                        <fieldset>
                            <p> <label>Order No : <?php echo $order_summary[0]['order_id']; ?></label> </p>
                            <?php /* <p> <label>Invoice No : <?php echo $order_summary[0]['invoice_no']; ?></label> </p> */ ?>
                            <?php /* <p> <label><?php echo $status[0]['status_desc']; ?></label> </p> */ ?>
                            <?php /* <p> Belt Needed :<label><?php echo $status[0]['belt_needed'] == 1 ? 'Yes' : 'No'; ?></label> </p> */ ?>
                            <p> <?php vendorLang('Shoe Horn') ?> : <label><?php /* echo $status[0]['shoe_horn'] == 1 ? 'Yes' : 'No' */ vendorLang('Yes'); ?></label> </p>
                            <p> <?php vendorLang('Shoe Trees') ?> : <label><?php vendorLang($status[0]['shoe_trees'] == 1 ? 'Yes' : 'No'); ?></label> </p>
                        </fieldset>
                    </div>
                    <form action="<?php echo base_url() . 'admin/shipping/' . $order_summary[0]['order_id']; ?>" method="post">
                        <input type="hidden" name="hid_invoice" value="<?php echo $order_summary[0]['invoice_no']; ?>" />
                        <input type="hidden" name="hid_vendorId" value="<?php echo $vendors[0]['user_id']; ?>" />
                        <div class="bulk-actions align-left" style="float: left; margin: -10px 0 30px;">
                            <select name="dropdown_status" id="dropdown_status">
                                <option value="option1">Change Status</option>
                                <option value="1">Order Shipped</option>
                            </select>
                            <input type='text' name="trackNo" id='txtTrackNo' value='' style="display: none" placeholder="Tracking No" />
                            <input type='text' name="carrier" id='txtCarrier' value='' style="display: none" placeholder="Carrier" />
                            <input type='text' name="shipDate" id='txtShipDate' value='' style="display: none" placeholder="Shipping Date(MM/DD/YYYY)" ReadOnly />
                            <input type='text' name="deliveryDate" id='txtDeliveryDate' value='' style="display: none" placeholder="Delivery Date(MM/DD/YYYY)" ReadOnly />
                            <input type='submit' name="submit" class='submit button' onclick="return validate();" value='Move to Order Shipped' style="display: none" />
                        </div>
                    </form>
                    <br />
                    <div style="clear: both;">
                        <a style="font-size: 14px;" href="<?php echo base_url() . 'excel/index/' . $order_summary[0]['order_id']; ?>">Download Order details in Excel format</a> <br />
                    </div>
                <?php } ?>
                <br />
                <div id="address_table" style="overflow: hidden; clear: both">
                    <table style="width: 100%; border-collapse: collapse; border-spacing: 0;">
                        <tbody style="display: table-row-group; vertical-align: middle; border-color: inherit;">
                            <tr style="display: table-row; vertical-align: inherit; border-color: inherit;">
                                <th style="font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; text-align: left; padding: 5px 0;">Billing to:</th>
                                <th style="font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; text-align: left; padding: 5px 0;">Shipping to:</th>
                            </tr>
                            <tr>
                                <td style="font-size: 20px;"> <?php echo $billing[0]['first_name'] . ' ' . $billing[0]['last_name']; ?> </td>
                                <td style="font-size: 20px;"> <?php echo $shipping[0]['firstname'] . ' ' . $shipping[0]['lastname']; ?> </td>
                            </tr>
                            <tr>
                                <td style="font-size: 20px;"> <?php echo (trim($billing[0]['address1']) == '' || $billing[0]['address1'] == '0') ? '' : $billing[0]['address1']; ?> </td>
                                <td style="font-size: 20px;"> <?php echo $shipping[0]['address1'] ?> </td>
                            </tr>
                            <tr>
                                <td style="font-size: 20px;">
                                    <?php
                                    echo (trim($billing[0]['address2']) == '' || $billing[0]['address2'] == '0') ? '' : $billing[0]['address2'] . ', ';
                                    echo (trim($billing[0]['city']) == '' || $billing[0]['city'] == '0') ? '' : $billing[0]['city'] . ', ';
                                    echo (trim($billing[0]['state']) == '' || $billing[0]['state'] == '0') ? '' : $billing[0]['state'] . ', ';
                                    echo (trim($billing[0]['country']) == '' || $billing[0]['country'] == '0') ? '' : $billing[0]['country'] . ', ';
                                    echo (trim($billing[0]['zipcode']) == '' || $billing[0]['zipcode'] == '0') ? '' : $billing[0]['zipcode'];
                                    ?>
                                </td>
                                <td style="font-size: 20px;">
                                    <?php
                                    echo (trim($shipping[0]['address2']) == '' || $shipping[0]['address2'] == '0') ? '' : $shipping[0]['address2'] . ', ';
                                    echo $shipping[0]['city'] . ', ';
                                    echo (trim($shipping[0]['state']) == '' || $shipping[0]['state'] == '0') ? '' : $shipping[0]['state'] . ', ';
                                    echo (trim($shipping[0]['country']) == '' || $shipping[0]['country'] == '0') ? '' : $shipping[0]['country'] . ', ';
                                    echo $shipping[0]['zipcode'];
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 20px;"></td>
                                <td style="font-size: 20px;"><?php echo $shipping[0]['telephone']; ?></td>
                            </tr>
                            <tr>
                                <td style="font-size: 20px;"></td>
                                <td style="font-size: 20px;"><?php echo $shipping[0]['email']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php if (trim($order_summary[0]['comments']) !== '') { ?>
                        <div style="font-size: 18px; padding-top: 10px;">
                            <span> Comments : </span> <b> <?php echo $order_summary[0]['comments']; ?> </b>
                        </div>
                    <?php } ?>
                    <?php if (trim($remake_instruction) !== '') { ?>
                        <div style="font-size: 18px; padding-top: 10px;">
                            <span> Rework Instruction : </span> <b> <?php echo $remake_instruction; ?> </b>
                        </div>
                    <?php } ?>
                </div>
                <div id="order_table" style="overflow: hidden;">
                    <div id="tab">
                        <?php $i = 1; ?>
                        <?php $class = "active"; ?>
                        <?php foreach ($order['items'] as $item) { ?>
                            <span class="<?php echo $class; ?>"><a href="#item<?php echo $i; ?>">Item #<?php echo $i; ?></a></span>
                            <?php $class = ""; ?>
                            <?php $i ++; ?>
                        <?php } ?>
                    </div>
                    <div id="orders">
                        <?php $total = 0; ?>
                        <?php $class = "active"; ?>
                        <?php $i = 1; ?>
                        <?php $s = 0; ?>

                        <?php
                        foreach ($order['items'] as $item) {
                            $class .= ' item' . $i;
                            $i ++;
                            if (strtolower($item['item_name']) != 'gift card') {
                                $shoedesign = $item['shoedesign'];
                                $m = $item['measurement'];
                                ?>
                                <div class="order-items <?php echo $class; ?>">
                                    <div style="clear: both;"></div>
                                    <form action="<?php echo base_url() . 'admin/vendor_payment/' . $order_summary[0]['order_id']; ?>" method="post">
                                        <div class="" style="margin: -10px 0 30px;">
                                            <table style="font-size: 14px;">
                                                <tr>
                                                    <td width="140">Design ID :</td>
                                                    <td>
                                                        <?php
                                                        foreach ($payment_details as $key => $product) {
                                                            if ($product['design_id'] === $shoedesign['shoe_design_id']) {
                                                                $s = $key;
                                                            }
                                                        }
                                                        echo $shoedesign['shoe_design_id'];
                                                        ?>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td width="140">Manufacturing Cost:</td>
                                                    <td>
                                                        <?php echo form_error('manufacturing_cost'); ?>
                                                        <input type='text' name="manufacturing_cost" <?php echo isset($payment_details[$s]['manufacturing_cost']) && $payment_details[$s]['design_id'] == $shoedesign['shoe_design_id'] ? 'disabled' : ''; ?> value="<?php echo set_value('manufacturing_cost', isset($payment_details[$s]['manufacturing_cost']) && $payment_details[$s]['design_id'] == $shoedesign['shoe_design_id'] ? $payment_details[$s]['manufacturing_cost'] : ''); ?>" placeholder="Manufacturing Cost" />
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td width="140">Shipping Cost:</td>
                                                    <td colspan="2">
                                                        <?php echo form_error('shipping_cost'); ?>
                                                        <input type='text' name="shipping_cost" id='txtShippingCost' <?php echo isset($payment_details[$s]['id']) && $payment_details[$s]['design_id'] == $shoedesign['shoe_design_id'] ? 'disabled' : ''; ?> value="<?php echo set_value('shipping_cost', isset($payment_details[$s]['shipping_cost']) && $payment_details[$s]['design_id'] == $shoedesign['shoe_design_id'] ? $payment_details[$s]['shipping_cost'] : ''); ?>" placeholder="Shipping Cost" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="140" style="vertical-align: middle;">Comments:</td>
                                                    <td colspan="2"><textarea name="comments" id='comments' <?php echo isset($payment_details[$s]['id']) && $payment_details[$s]['design_id'] == $shoedesign['shoe_design_id'] ? 'disabled' : ''; ?> placeholder="comments"><?php echo set_value('comments', isset($payment_details[$s]['comments']) && $payment_details[$s]['design_id'] == $shoedesign['shoe_design_id'] ? $payment_details[$s]['comments'] : ''); ?></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td width="140"></td>
                                                    <td colspan="2">
                                                        <input type="hidden" name="hid_invoice" value="<?php echo $order_summary[0]['invoice_no']; ?>" />
                                                        <input type="hidden" name="hid_vendorId" value="<?php echo $vendors[0]['user_id']; ?>" />
                                                        <input type="hidden" name="hid_paymentId" value="<?php echo isset($payment_details[$s]['id']) && $payment_details[$s]['design_id'] == $shoedesign['shoe_design_id'] ? $payment_details[$s]['id'] : ''; ?>" />
                                                        <input type="hidden" name="hid_designId" value="<?php echo $shoedesign['shoe_design_id']; ?>" />
                                                        <input type='submit' name="submit" class='button' <?php echo isset($payment_details[$s]['id']) && $payment_details[$s]['design_id'] == $shoedesign['shoe_design_id'] ? 'disabled' : ''; ?> onclick="return validateCost();" value='Submit Comment' />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </form>
                                    <table id="order_list" style="border-collapse: collapse; border-spacing: 0; display: table; border-color: gray; width: 100%; margin-top: 30px; font-weight: bold; font-family: gibsonsemibold, arial, sans-serif;">
                                        <thead style="display: table-header-group; vertical-align: middle; border-color: inherit;">
                                            <tr>
                                                <th style="width: 65%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">DESCRIPTION</th>
                                                <th style="width: 27%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">QUANTITY</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">
                                                    <?php echo ($item['reference_order_no'] !== '') ? 'Reference Order Number : ' . $item['reference_order_no'] . '<br/>' : ''; ?>
                                                    Left shoe size : <?php echo $item['left_shoe']; ?>, Right shoe size : <?php echo $item['right_shoe']; ?>
                                                    <br /> Left shoe Width : <?php echo (isset($item['left_width']) ? (isset($tempSize[$item['left_width']]) ? $tempSize[$item['left_width']] : $item['left_width'] ) : '' ); ?>, Right shoe Width : <?php echo (isset($item['right_width']) ? (isset($tempSize[$item['right_width']]) ? $tempSize[$item['right_width']] : $item['right_width'] ) : '' ); ?>
                                                    <?php if ($item['m_left_width'] !== '' && $item['m_left_width'] !== '0') { ?>
                                                        <br /> <br /> <u>Measurements:</u>
                                                        <br /> Left shoe size : <?php echo $m['left']['size'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?> , Right shoe size : <?php echo $m['right']['size'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?>
                                                        <br /> Left shoe Width : <?php echo $m['left']['width'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?> , Right shoe width : <?php echo $m['right']['width'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?>
                                                        <br /> Left shoe Height : <?php echo $m['left']['girth'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?> , Right shoe Height: <?php echo $m['right']['girth'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?>
                                                        <br /> Left shoe Instep : <?php echo $m['left']['instep'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?> , Right shoe Instep : <?php echo $m['right']['instep'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?>
                                                        <br /> Left shoe Heel : <?php echo $m['left']['heel'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?> , Right shoe Heel : <?php echo $m['right']['heel'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?>
                                                        <br /> Left shoe Ankle : <?php echo $m['left']['ankle'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?> , Right shoe Ankle : <?php echo $m['right']['ankle'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?>
                                                    <?php } ?>
                                                    <?php if (isset($item['shop_images']) && is_array($item['shop_images'])) { ?>
                                                        <?php foreach ($item['shop_images'] as $key => $value) { ?>
                                                            <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/' . $value['file_name']; ?>" />
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A0.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A1.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A2.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A3.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A4.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A5.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A6.png"; ?>" />
                                                        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A7.png"; ?>" />
                                                    <?php } ?>
                                                    <?php if ($item['img1'] != '' && $item['img1'] != NULL) { ?>
                                                        <img style="width: 100%;" src="<?php echo $item['img1']; ?>" />
                                                    <?php } ?>
                                                    <?php if ($item['img2'] != '' && $item['img2'] != NULL) { ?>
                                                        <img style="width: 100%;" src="<?php echo $item['img2']; ?>" />
                                                    <?php } ?>
                                                    <?php if ($item['can_show_details']) { ?>
                                                        <p class="title">Design Details</p>
                                                        <div id="side_menu_content">
                                                            <p class="item-subtitle"><?php echo /* $shoedesign['last_name'] . "/" . */$shoedesign['style_name'] . " (" . $shoedesign['manufacturer_code'] . ")"; ?></p>
                                                            <ul id="design_menu" class="level1">
                                                                <?php if (!$shoedesign['t_is_nothing']) { ?>
                                                                    <li>
                                                                        <?php vendorLang('Toe'); ?> : <?php echo (str_replace('Toe', vendorLang('Toe', FALSE), $shoedesign['toe_type'])) . '' . (($shoedesign['t_is_material']) ? ', ' . vendorLang($shoedesign['toe_material'], FALSE) . ', ' . vendorLang($shoedesign['toe_color'], FALSE, $shoedesign['toe_material']) : '' ); ?>
                                                                    </li>
                                                                    <ul class="level2"></ul>
                                                                <?php } if ($shoedesign['is_vamp_enabled']) { ?>
                                                                    <?php if (!$shoedesign['v_is_nothing']) { ?>
                                                                        <li>
                                                                            <?php vendorLang('Vamp'); ?> : <?php echo (str_replace('Vamp', vendorLang('Vamp', FALSE), $shoedesign['vamp_type'])) . '' . (($shoedesign['v_is_material']) ? ', ' . vendorLang($shoedesign['vamp_material'], FALSE) . ', ' . vendorLang($shoedesign['vamp_color'], FALSE, $shoedesign['vamp_material']) : '' ); ?>
                                                                        </li>
                                                                        <ul class="level2"> </ul>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <li><?php vendorLang('Quarter'); ?> : <?php echo vendorLang($shoedesign['quarter_material'], FALSE) . ', ' . vendorLang($shoedesign['quarter_color'], FALSE, $shoedesign['quarter_material']); ?></li>
                                                                <ul class="level2"></ul>
                                                                <?php if (!$shoedesign['e_is_nothing']) { ?>
                                                                    <li>
                                                                        <?php vendorLang('Eyestays'); ?> : <?php echo (str_replace('Eyestay', vendorLang('Eyestay', FALSE), $shoedesign['eyestay_type'])) . '' . (($shoedesign['e_is_material']) ? ', ' . vendorLang($shoedesign['eyestay_material'], FALSE) . ', ' . vendorLang($shoedesign['eyestay_color'], FALSE, $shoedesign['eyestay_material']) : '' ); ?>
                                                                    </li>
                                                                    <ul class="level2"></ul>
                                                                <?php } if (!$shoedesign['f_is_nothing']) { ?>
                                                                    <li>
                                                                        <?php vendorLang('Foxing') ?> : <?php echo (str_replace('Foxing', vendorLang('Foxing', FALSE), $shoedesign['foxing_type'])) . '' . (($shoedesign['f_is_material']) ? ', ' . vendorLang($shoedesign['foxing_material'], FALSE) . ', ' . vendorLang($shoedesign['foxing_color'], FALSE, $shoedesign['foxing_material']) : '' ); ?>
                                                                    </li>
                                                                    <ul class="level2"></ul>
                                                                <?php } ?>
                                                                <?php /* <li><?php vendorLang('Stitching') ?> :  <?php vendorLang($shoedesign['stitch_name']); ?></li>
                                                                  <ul class="level2">
                                                                  <ul class="level3">
                                                                  <li style="background: <?php echo $shoedesign['stitch_color']; ?>"></li>
                                                                  </ul>
                                                                  </ul> */ ?>
                                                                <?php if ($shoedesign['is_lace_enabled']) { ?>
                                                                    <li><?php vendorLang('Laces') ?> :  <?php vendorLang($shoedesign['lace_name']); ?></li>
                                                                    <ul class="level2">
                                                                        <ul class="level3">
                                                                            <li style="background: <?php echo $shoedesign['color_code']; ?>"></li>
                                                                        </ul>
                                                                    </ul>
                                                                <?php } ?>
                                                                <?php if (isset($item['monogramText'])) { ?>
                                                                    <?php $text = ($item['monogramRightSide'] > 0) ? "on right counter" : (($item['monogramLeftSide'] > 0) ? "on left counter" : "" ); ?>
                                                                    <ul class="level2">
                                                                        <li><?php vendorLang('Monogram'); ?> : <?php echo $item['monogramText']; ?></li>
                                                                        <?php
                                                                        if ($item['monogramLeftShoe'] > 0) {
                                                                            $text .= ", left shoe";
                                                                            if ($item['monogramRightShoe'] > 0) {
                                                                                $text .= " and right shoe";
                                                                            }
                                                                        } elseif ($item['monogramRightShoe'] > 0) {
                                                                            $text .= ", right shoe";
                                                                        }
                                                                        ?>
                                                                        <li><?php vendorLang($text); ?></li>
                                                                    </ul>
                                                                <?php } ?>
                                                                <?php if ($item['soleId'] > 0) { ?>
                                                                    <li><?php vendorLang('Sole Type'); ?> : <?php vendorLang($item['soleName']); ?></li>
                                                                    <ul class="level2"></ul>
                                                                    <li><?php vendorLang('Sole Edge Color'); ?> : <?php vendorLang($item['soleColorName']); ?></li>
                                                                    <ul class="level2"></ul>
                                                                    <li><?php vendorLang('Shoe Construction'); ?> : <?php vendorLang($item['soleWelt']); ?></li>
                                                                    <ul class="level2"></ul>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (isset($item['is_readywear']) && $item['is_readywear'] == 1) { ?>
                                                        <p class="title">Design Details</p>
                                                        <div id="side_menu_content">
                                                            <p class="item-subtitle"><?php echo $item['last_name'] . " / " . $item['style_name'] . " (" . $item['shoe_name'] . ")"; ?></p>
                                                            <ul id="design_menu" class="level1">
                                                                <li>Color: <?php echo $item['color']; ?></li>
                                                            </ul>
                                                        </div>
                                                    <?php } ?>
                                                </td>
                                                <td style="text-align: center; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;"><?php echo $item['quantity'] ?> PAIR</td>
                                            </tr>
                                            <tr style="border: 1px solid #000; height: 40px;">
                                                <td></td>
                                                <td><div> <a href="javascript:window.print();" class="button">Print this page</a> </div></td>
                                            </tr>
                                        </tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            <?php } ?>
                            <?php $class = ''; ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="clear"></div>
                <div id="footer">
                    <small> &#169; Copyright 2013 | Powered by <a href="#">AWL&SUNDRY</a> | <a href="<?php echo base_url() . 'index.php/admin/vendor' ?>">BACK</a> </small>
                </div>
            </div>
        </div>
    </body>
</html>