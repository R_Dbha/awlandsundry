<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>AWL&SUNDRY Admin</title>

        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/cupertino/jquery-ui-1.10.4.custom.min.css' ?>" type="text/css" media="screen" />
        <!-- Colour Schemes
  
        Default colour scheme is green. Uncomment prefered stylesheet to use it.
        
        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
        
        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
 
        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.10.2.js' ?>"></script>

        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-ui-1.10.4.custom.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript">

            $(document).ready(function() {
//                $('#grid tbody tr').bind('click touchend', function() {
//                    window.location = $(this).find('a').attr('href');
//                    return false;
//                });
                $('#select_vendor').change(function() {
                    var vendor_id = $(this).val();
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url(); ?>index.php/admin/getAmountPayable',
                        data: {'vendor_id': vendor_id, },
                        success: function(response) {
                            $('#amountPayable').html('$ <span>' + response + '</span>');
                        }
                    });
                });
                $('#payment_date').datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: '-30',
                    maxDate: '+10',
                });
            });
            function validatePayment() {
                if ($('#select_vendor').val() === '') {
                    alert('Please select the vendor');
                    return false;
                }
                if ($('#amountPayable span').html() === '') {
                    alert('Please select the vendor');
                    return false;
                }
                if ($('#amount').val() === '') {
                    alert('Please enter the amount');
                    return false;
                }
//                if (parseInt($('#amount').val()) > parseInt($('#amountPayable span').html())) {
//                    alert('The amount is greater than the amount payable');
//                    return false;
//                }
                return true;
            }
        </script>
        <!-- Internet Explorer .png-fix -->

        <!--[if IE 6]>
                <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('.png_bg, img, li');
                </script>
        <![endif]-->

    </head>

    <body>
        <div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">AWL Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src='<?php echo base_url() . "assets/img/logo.png" ?>' alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a>
                        <br />
                        <!--a href="<?php echo base_url() . 'index.php' ?>" title="View the Site">View the Site</a--> 
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        

                    <?php $this->load->view('admin/menu'); ?>

                    <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                        <form action="" method="post">

                            <h4>New Message</h4>

                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>

                            <fieldset>

                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>

                                <input class="button" type="submit" value="Send" />

                            </fieldset>

                        </form>

                    </div> <!-- End #messages -->

                </div></div> <!-- End #sidebar -->

            <div id="main-content"> <!-- Main Content Section with everything -->
                <form action ="<?php echo base_url() . 'index.php/admin/submit_payment' ?>" method ="post" onsubmit="return validatePayment();" >
                    <noscript> <!-- Show a notification if the user has disabled javascript -->
                        <div class="notification error png_bg">
                            <div>
                                Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                            </div>
                        </div>
                    </noscript>

                    <!-- Page Head -->
                    <h2>Welcome <?php echo $user['first_name']; ?></h2>
                    <div>
                        <table style="font-size: 14px; ">
                            <tr>
                                <td width="140">Select Vendor:</td>
                                <td width="200">
                                    <select id="select_vendor" name="vendor_id">
                                        <option value="">Select Vendor</option>
                                        <?php echo $select_vendors; ?>
                                    </select>
                                </td>
                                <td width="140">Amount Payable:</td>
                                <td ><b><span id="amountPayable" readonly ></span></b></td>
                            </tr>
                            <tr>
                                <td>Amount:</td>
                                <td colspan="3"><input type="text" name ="amount" id="amount" value="" /></td>
                            </tr>
                            <tr>
                                <td>Payment Method:</td>
                                <td colspan="3"><input type="text" name ="payment_method" id="payment_method" value="" /></td>
                            </tr>
                            <tr>
                                <td>Reference no:</td>
                                <td colspan="3"><input type="text" name ="reference_no" id="reference_no" value="" /></td>
                            </tr>
                            <tr>
                                <td>Date:</td>
                                <td colspan="3"><input type="text" name ="date" id="payment_date" autocomplete='off'  value="" /></td>
                            </tr>
                            <tr>
                                <td>Comments:</td>
                                <td colspan="3"><textarea name ="comments"  id="comment" autocomplete='off'  ></textarea></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="3"><input type="submit" name ="submit" value="Submit Payment" /></td>
                            </tr>

                        </table>
                    </div>
                    <!--p id="page-intro">What would you like to do?</p-->

                    <ul class="shortcut-buttons-set">



                    </ul><!-- End .shortcut-buttons-set -->
                    <div class="content-box"><!-- Start Content Box -->

                        <div class="content-box-header">

                            <h3>Payment Summary</h3>

                            <!--ul class="content-box-tabs">
                                    <li><a href="#tab1" class="default-tab">Table</a></li> 
                                    <li><a href="#tab2">Forms</a></li>
                            </ul-->

                            <div class="clear"></div>

                        </div> <!-- End .content-box-header -->

                        <div class="content-box-content">

                            <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                                <div class="notification attention png_bg">
                                        <!--a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a-->
                                    <!--div>
                                            This is a Content Box. You can put whatever you want in it. By the way, you can close this notification with the top-right cross.
                                    </div-->
                                </div>

                                <table id="grid">

                                    <thead>
                                        <tr>
                                            <!--th><input class="check-all" type="checkbox" /></th-->
                                            <th>Date</th>
                                            <th>Vendor</th>
                                            <th>Amount</th>
                                            <th>Payment Method</th>
                                            <th>Reference No</th>
                                            <th>Advance</th>
                                            <th>Comments</th>

                                        </tr>

                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <td colspan="6">

                                                <div class="clear"></div>
                                            </td>
                                        </tr>
                                    </tfoot>

                                    <tbody>
                                        <?php
                                       
                                        foreach ($payments as $payment) {
                                           
                                            ?>
                                            <tr >
                                                <!--td><input type="checkbox" /></td-->
                                                <td><?php echo date("m/d/Y", strtotime($payment['payment_date'])); ?></td>
                                                <td style="width:200px; word-wrap: break-word; "><?php echo $payment['first_name']; ?></td>
                                                <td><?php echo $payment['amount']; ?></td>
                                                <td><?php echo $payment['payment_method']; ?></td>
                                                <td><?php echo $payment['reference_no']; ?></td>
                                                <td><?php echo $payment['advance']; ?></td>
                                                <td><?php echo $payment['comments']; ?></td>
                                              </tr>
                                        <?php }
                                        ?>
                                    </tbody>


                                </table>

                            </div> <!-- End #tab1 -->
                        </div> <!-- End .content-box-content -->
                    </div>
                    <div class="clear"></div> <!-- End .clear -->

                    <div class="content-box"><!-- Start Content Box -->

                        <div class="content-box-header">

                            <h3>Payment Distribution</h3>

                            <!--ul class="content-box-tabs">
                                    <li><a href="#tab1" class="default-tab">Table</a></li> 
                                    <li><a href="#tab2">Forms</a></li>
                            </ul-->

                            <div class="clear"></div>

                        </div> <!-- End .content-box-header -->

                        <div class="content-box-content">

                            <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                                <div class="notification attention png_bg">
                                        <!--a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a-->
                                    <!--div>
                                            This is a Content Box. You can put whatever you want in it. By the way, you can close this notification with the top-right cross.
                                    </div-->
                                </div>

                                <table id="grid">

                                    <thead>
                                        <tr>
                                            <!--th><input class="check-all" type="checkbox" /></th-->
                                            <th>Order No</th>
                                            <th>Invoice No</th>
                                            <th>Design ID</th>
                                            <th>Submitted Date</th>
                                            <th>Vendor</th>
                                            <th>Manufacturing cost</th>
                                            <th>Shipping cost</th>
                                            <th>Total cost</th>
                                            <th>Paid Amount</th>
                                            <th>Payment Status</th>
                                            <th>Comments</th>
                                            <th></th>

                                        </tr>

                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <td colspan="6">

                                                <div class="clear"></div>
                                            </td>
                                        </tr>
                                    </tfoot>

                                    <tbody>
                                        <?php
                                        $cnt = 0;
                                        foreach ($data as $payment_details) {
                                            $cnt++;

                                            if ($payment_details['discount_perc'] !== 0) {
                                                $disc = ($payment_details['gross_amt'] * $payment_details['discount_perc']) / 100;
                                                $payment_details['gross_amt'] = $payment_details['gross_amt'] - $disc;
                                            }
                                            ?>
                                            <tr  style ='cursor:pointer;'>
                                                <!--td><input type="checkbox" /></td-->
                                                <td><?php echo $payment_details['order_id']; ?></td>
                                                <td><?php echo $payment_details['invoice_no']; ?></td>
                                                <td><?php echo $payment_details['design_id']; ?></td>
                                                <td><?php echo date("m/d/Y", strtotime($payment_details['date'])); ?></td>
                                                <td style="width:200px; word-wrap: break-word; "><?php echo $payment_details['first_name']; ?></td>
                                                <td><?php echo $payment_details['manufacturing_cost']; ?></td>
                                                <td><?php echo $payment_details['shipping_cost']; ?></td>
                                                <td><?php echo $payment_details['shipping_cost'] + $payment_details['manufacturing_cost']; ?></td>
                                                <td><?php echo $payment_details['paid_amount']; ?></td>
                                                <td><?php echo $payment_details['payment_status']; ?></td>
                                                <td><?php echo $payment_details['comments']; ?></td>
                                                <td><?php if ($payment_details['payment_status'] == '') { ?><a href="<?php echo base_url() . 'index.php/admin/edit_payment/' . $payment_details['id']; ?>">Edit</a><?php } ?></td>
                                               



                                                <!--td>

                                                         <a href="#" title="Edit"><img src="resources/images/icons/pencil.png" alt="Edit" /></a>
                                                         <a href="#" title="Delete"><img src="resources/images/icons/cross.png" alt="Delete" /></a> 
                                                         <a href="#" title="Edit Meta"><img src="resources/images/icons/hammer_screwdriver.png" alt="Edit Meta" /></a>
                                                </td-->
                                            </tr>
                                        <?php }
                                        ?>
                                    </tbody>


                                </table>

                            </div> <!-- End #tab1 -->
                        </div> <!-- End .content-box-content -->
                    </div>
                </form>
            </div> <!-- End .content-box -->


            <div class="clear"></div>




        </div> <!-- End #main-content -->

    </body>

</html>
