<?php
$this->load->view('includes/admin_header');
?>

<div class="modal fade giftcard-modal" id="giftcard" tabindex="-1"
     role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<section class="content-header">
    <h1>
        Gift Cards <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gift Cards</li>
    </ol>
</section>

<section id="content" class="content" style="background-color:#FFFFFF;">
    <div class="box box-primary">
        <div class="overlay"></div>
        <div class="loading-img"></div>
        <div class="box-header" data-toggle="tooltip" title="" data-original-title="Gift Cards">
            <h3 class="box-title">Gift Cards</h3>
        </div>
        <div class="box-body">
            <table id="giftcards" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>                       
                        <th>Order No</th>
                        <th>Invoice No</th>
                        <th>Order by</th>
                        <th>Order Amount</th>
                        <th>Card Amount</th>
                        <th>Card Number</th>
                        <th>Recipient Email</th>                        
                        <th>Date</th>
                    </tr>
                </thead>               
                <tbody>
                    <?php
                    if ($giftcards) {
                        foreach ($giftcards as $giftcard) {
                            if ($giftcard['discount_perc'] !== 0) {
                                $disc = ($giftcard['item_amt'] * $giftcard['discount_perc']) / 100;
                                $giftcard['gross_amt'] = $giftcard['item_amt'] - $disc;
                            }
                            ?>
                            <tr  style ='cursor:pointer;'>
                                <td>
                                    <?php echo $giftcard['order_id']; ?>
                                    <input type="hidden" class="gift-card-id" value="<?php echo $giftcard['id']; ?>">
                                </td>
                                <td><?php echo $giftcard['invoice_no']; ?></td>
                                <td><?php echo $giftcard['first_name'] . ' ' . $giftcard['last_name']; ?></td>
                                <td><?php echo $giftcard['gross_amt']; ?></td>
                                <td><?php echo $giftcard['item_amt']; ?></td>
                                <td><?php echo $giftcard['card_number']; ?></td>
                                <td><?php echo $giftcard['recipient_email']; ?></td>
                                <td><?php echo date("m/d/Y h:ia", strtotime($giftcard['order_date'])); ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php
$this->load->view('includes/admin_footer');
?>