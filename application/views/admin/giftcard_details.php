<?php ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">×</button>
    <h4 class="modal-title">
        <i class="fa  fa-folder-open-o"></i> GiftCard
    </h4>
</div>
<div class="modal-body">		
    <div class="row seperation">

        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-text-width"></i>
                    <h3 class="box-title">GiftCard Details</h3>
                </div>
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Card Number :</dt>
                        <dd><?php echo $giftcard['card_number']; ?></dd>
                        <dt>Card Amount :</dt>
                        <dd><?php echo $giftcard['item_amt']; ?></dd>
                        <dt>Sender Email :</dt>
                        <dd><?php echo $giftcard['sender_email']; ?></dd>
                        <dt>Recipient Email :</dt>
                        <dd><?php echo $giftcard['recipient_email']; ?></dd>

                    </dl>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-envelope-o"></i>
                    <h3 class="box-title">Billing address</h3>
                </div>
                <div class="box-body">
                    <div id="billing-address" class="not-editable">
                        <?php $this->load->view('admin/order_billing_address_list', array('billing' => $billing)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row seperation" >
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header">
                    <i class="fa fa-envelope-o"></i>
                    <h3 class="box-title">Gift Card Usage</h3>
                </div>
                <div class="box-body">
                    <?php if ($usage) {
                        ?>
                        <label>The Gift card has been added to Store Credit</label>
                        <br/>
                        <?php if (!isset($usage['store_credit_id'])) {
                            ?>
                            <label>The purchases made are below</label>
                            <table id="giftcard-usage" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>                       
                                        <th>Order No</th>
                                        <th>Invoice No</th>
                                        <th>Order Amount</th>
                                        <th>Discount</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($usage as $key => $value) {
                                        ?>
                                        <tr>                       
                                            <td><?php echo $value['order_id'] ?></td>
                                            <td><?php echo $value['invoice_no'] ?></td>
                                            <td><?php echo $value['gross_amt'] ?></td>
                                            <td><?php echo $value['discount'] ?></td>
                                            <td><?php echo $value['payment_status'] ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        }
                        ?>
                    <?php }else{
                        ?>
                            <label>The Gift card has not been used</label>
                            <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <br/>
</div>
<div class="modal-footer clearfix">
    <button type="button" data-dismiss="modal"
            class="btn btn-warning pull-right">
        <i class="fa fa-times"></i> Close
    </button>
</div>
<?php ?>