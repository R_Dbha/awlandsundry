<?php
$this->load->view('includes/admin_header');
?>

<div class="adduser">
    <div id="add_user">
        <form id="register_form">
            <h3 class="heading"><?php echo (!isset($user)) ? 'Create' : 'Edit' ?> User</h3>
            <div class="registrationForm">
                <div>
                    <label>First Name</label>
                    <input type="text" name="fname" id="fname" value="<?php echo (isset($user)) ? $user['first_name'] : '' ?>" required/>
                </div>
                <div>
                    <label>Last Name</label>
                    <input type="text" name="lname" id="lname" value="<?php echo (isset($user)) ? $user['last_name'] : '' ?>" required/>
                </div>
                <div>
                    <label>Email address</label>
                    <input type="email" name="email" id="email" value="<?php echo (isset($user)) ? $user['email'] : '' ?>" required/>
                </div>

                <?php if (!isset($user)) { ?>
                    <div>
                        <label>Password</label>
                        <input type="password" name="password" id="password" required/>
                    </div>
                    <div>
                        <label>Confirm Password</label>
                        <input type="password" name="confirmpassword" id="confirmpassword" required/>
                    </div>
                <?php } else { ?>
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user['user_id'] ?>" />
                <?php } ?>
                <div>
                    <label>Select Role</label>
                    <select name="role" id="role">
                        <option value="admin" <?php echo (isset($user)) ? ($user['user_type_id'] == '1' ? 'selected' : '') : '' ?> >Admin</option>
                        <option value="webuser" <?php echo (isset($user)) ? ($user['user_type_id'] == '2' ? 'selected' : '') : '' ?>>Web User</option>
                        <option value="vendor" <?php echo (isset($user)) ? ($user['user_type_id'] == '3' ? 'selected' : '') : '' ?>>Vendor</option>
                        <option value="intern" <?php echo (isset($user)) ? ($user['user_type_id'] == '4' ? 'selected' : '') : '' ?>>Intern</option>
                    </select>
                </div>
                <div>
                    <span style="color:red" id="error"></span>
                </div>
                <div>
                    <span style="color:green" id="success"></span>
                </div>
                <div>
                    <button type="submit" name="addUser" id="btnAddUser">
                        Save User
                        <img src="../assets/img/add_user_loader.gif" hidden="true" id="loadIcon"/>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
$this->load->view('includes/admin_footer');
?>