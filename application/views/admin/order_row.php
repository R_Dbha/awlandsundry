<?php if (isset($orders) && sizeof($orders)) {?>
    <?php foreach ($orders as $key => $order) {?>
		<?php $order['total_amt'] = round($order['total_amt'] - ($order['total_amt'] * $order['discount_perc'] / 100), 0);?>

        <tr class="<?php echo strtolower(str_replace(' ', '-', $order['status'])); ?>" >
            <td>
                <?php echo $order['order_id']; ?>
                <?php $this->load->view('admin/order_quick_view', $order);?>
                <input type="hidden" value="<?php echo $order['order_id']; ?>" class="order_id">
            </td>
            <td><?php echo $order['invoice_no']; ?></td>
            <?php if ($kickstarter == 1) {?><td><?php echo $order['kickstarter_no']; ?></td><?php }?>
            <td><?php echo $order['first_name'] . ' ' . $order['last_name']; ?></td>
            <td><?php echo $order['item_count']; ?></td>
            <td><?php echo $order['total_amt']; ?></td>
            <td><?php echo date('m/d/Y H:ia', strtotime($order['order_date'])); ?></td>
            <td><?php echo $order['status']; ?></td>
        </tr>
        <?php }?>
<?php }?>