<div class="col-md-6">
    <div class="box box-solid">
        <div class="box-header">
            <i class="fa fa-text-width"></i>
            <h3 class="box-title">Selected Shoe</h3>
        </div>
        <div class="box-body">
            <ul class="new-item-images">
                <?php $file_path = ApplicationConfig::ROOT_BASE . 'files/'; ?>
                <?php if (isset($item['shop_images']) && is_array($item['shop_images'])) { ?>
                    <?php foreach ($item['shop_images'] as $key => $value) { ?>
                        <li><img src="<?php echo $file_path . $value['file_name']; ?>" /></li>
                    <?php } ?>
                <?php } else { ?>
                    <?php if ($item['hasPatina'] > 0) { ?>
                        <li><img src="<?php echo $file_path . 'designs/' . $item['patinaImg']; ?>" /></li>
                    <?php } ?>
                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A0.png"; ?>" /></li>
                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A1.png"; ?>" /></li>
                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A2.png"; ?>" /></li>
                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A3.png"; ?>" /></li>
                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A4.png"; ?>" /></li>
                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A5.png"; ?>" /></li>
                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A6.png"; ?>" /></li>
                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A7.png"; ?>" /></li>
                <?php } ?>
            </ul>
            <h5 class="center">
                $<span class="new-price"><?php echo $item['item_amt'] ?></span>
            </h5>
            <input type="hidden" class="new-design_id" value="<?php echo $item['shoe_design_id']; ?>"> 
            <input type="hidden" class="new-custom_collection_id" value="<?php echo @$item['custom_collection_id']; ?>">
            <input type="hidden" class="is_readywear" value="<?php echo @$item['is_readywear']; ?>">
        </div>
        <div class="box-footer clearfix">
            <button class="pull-left btn btn-warning back"><i class="fa fa-arrow-circle-left"></i> Back </button>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="box box-solid">
        <div class="box-header">
            <i class="fa fa-text-width"></i>
            <h3 class="box-title">Details</h3>
        </div>
        <div class="box-body">
            <dl class="dl-horizontal">
                <?php $shoedesign = $item; ?>
                <?php if ($item['can_show_details'] == 1) { ?>
                    <dt>Last/Style :</dt>
                    <dd><?php echo $shoedesign['last_name'] . "/" . $shoedesign['style_name'] . " (" . $shoedesign['manufacturer_code'] . ")"; ?></dd>

                    <dt>Quarter :</dt>
                    <dd><?php echo $shoedesign['quarter_material']; ?> , <?php echo $shoedesign['quarter_color']; ?></dd>

                    <?php if (!$shoedesign['t_is_nothing']) { ?>
                        <dt>Toe :</dt>
                        <dd><?php echo $shoedesign['toe_type'] . ($shoedesign['toe_material_id'] > 1 ? ', ' . $shoedesign['toe_material'] . ', ' . $shoedesign['toe_color'] : ''); ?></dd>
                    <?php } ?>

                    <?php if (!$shoedesign['v_is_nothing'] && $shoedesign['is_vamp_enabled']) { ?>
                        <dt>Vamp :</dt>
                        <dd><?php echo $shoedesign['vamp_type'] . ($shoedesign['vamp_material_id'] > 1 ? ', ' . $shoedesign['vamp_material'] . ', ' . $shoedesign['vamp_color'] : ''); ?></dd>
                    <?php } ?>

                    <?php if (!$shoedesign['e_is_nothing']) { ?>
                        <dt>Eyestays :</dt>
                        <dd><?php echo $shoedesign['eyestay_type'] . ($shoedesign['eyestay_material_id'] > 1 ? ', ' . $shoedesign['eyestay_material'] . ', ' . $shoedesign['eyestay_color'] : ''); ?></dd>
                    <?php } ?>

                    <?php if (!$shoedesign['f_is_nothing']) { ?>
                        <dt>Foxing :</dt>
                        <dd><?php echo $shoedesign['foxing_type'] . ($shoedesign['foxing_material_id'] > 1 ? ', ' . $shoedesign['foxing_material'] . ', ' . $shoedesign['foxing_color'] : ''); ?></dd>
                    <?php } ?>

                    <?php if ($item['hasPatina'] > 0) { ?>
                        <dt style="color: red;">Patina :</dt>
                        <dd style="color: red; font-weight: bold"><?php echo $item['patina_material']; ?> , <?php echo $item['patina_color']; ?></dd>
                    <?php } ?>

                    <?php if (isset($item['soleName']) && isset($item['soleColorName'])) { ?>
                        <dt>Sole :</dt>
                        <dd><?php echo $item['soleName'] . ' / ' . $item['soleColorName']; ?></dd>
                    <?php } ?>

                    <dt>Stitching :</dt>
                    <dd><?php echo $shoedesign['stitch_name']; ?>
                        <span class="color" style="background: <?php echo $shoedesign['stitch_color']; ?>"></span>
                    </dd>

                    <?php if ($shoedesign['is_lace_enabled']) { ?>
                        <dt>Laces :</dt>
                        <dd><?php echo $shoedesign['lace_name']; ?>
                            <span class="color" style="background: <?php echo $shoedesign['color_code']; ?>"></span>
                        </dd>
                    <?php } ?>

                <?php } ?>
                <?php if ($item['is_readywear'] == 1) { ?>
                    <dt>Last/Style :</dt>
                    <dd><?php echo $item['last_name'] . "/" . $item['style_name'] . " (" . $item['shoe_name'] . ")"; ?>
                    </dd>
                <?php } ?>
                <hr />

            </dl>
            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">Size</div>
            </div>
            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">
                    <select class="form-control left_shoe">
                        <option value="">Left Shoe size</option>
                        <?php echo $item['details']['left_shoe']; ?>
                    </select>
                </div>
                <div class="col-lg-4 no-padding">
                    <select class="form-control right_shoe">
                        <option value="">Right Shoe size</option>
                        <?php echo $item['details']['right_shoe']; ?>
                    </select>
                </div>
            </div>

            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">
                    <select class="form-control left_width">
                        <option value="">Left shoe width</option>
                        <?php echo $item['details']['left_width']; ?>
                    </select>
                </div>
                <div class="col-lg-4 no-padding">
                    <select class="form-control right_width">
                        <option value="">Right shoe width</option>
                        <?php echo $item['details']['right_width']; ?>
                    </select>
                </div>
            </div>
            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">Measurements</div>
            </div>

            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">Left Length</div>
                <div class="col-lg-4 no-padding">Right Length</div>
            </div>
            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control m_left_size" placeholder="Left Length (CM)" value="<?php echo $item['details']['m_left_size'] == '0' ? '' : $item['details']['m_left_size']; ?>" />
                </div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control m_right_size" placeholder="Right Length (CM)" value="<?php echo $item['details']['m_right_size'] == '0' ? '' : $item['details']['m_right_size']; ?>" />
                </div>
            </div>

            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">Left Width</div>
                <div class="col-lg-4 no-padding">Right Width</div>
            </div>

            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control m_left_width" placeholder="Left Width (CM)" value="<?php echo $item['details']['m_left_width'] == '0' ? '' : $item['details']['m_left_width']; ?>" />
                </div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control m_right_width" placeholder="Right Width (CM)" value="<?php echo $item['details']['m_right_width'] == '0' ? '' : $item['details']['m_right_width']; ?>" />
                </div>
            </div>
            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">Left Height</div>
                <div class="col-lg-4 no-padding">Right Height</div>
            </div>

            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control m_lgirth" placeholder="Left Height (CM)" value="<?php echo $item['details']['m_lgirth'] == '0' ? '' : $item['details']['m_lgirth']; ?>" />
                </div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control m_rgirth" placeholder="Right Height (CM)" value="<?php echo $item['details']['m_rgirth'] == '0' ? '' : $item['details']['m_rgirth']; ?>" />
                </div>
            </div>

            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">Left Instep</div>
                <div class="col-lg-4 no-padding">Right Instep</div>
            </div>

            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control m_linstep" placeholder="Left Instep(CM)" value="<?php echo $item['details']['m_linstep'] == '0' ? '' : $item['details']['m_linstep']; ?>" />
                </div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control m_rinstep" placeholder="Right Instep (CM)" value="<?php echo $item['details']['m_rinstep'] == '0' ? '' : $item['details']['m_rinstep']; ?>" />
                </div>
            </div>

            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">Quantity</div>
                <div class="col-lg-4 no-padding">Monogram</div>
            </div>
            <div class="row seperation">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control new-quantity" placeholder="Quantity" />
                </div>
                <div class="col-lg-4 no-padding">
                    <input type="text" class="form-control new-monogram" placeholder="Monogram" />
                </div>
            </div>
        </div>
        <div class="box-footer clearfix">
            <?php if ($edit) { ?>
                <button class="pull-right btn btn-primary" id="confirm-item"> <i class="fa fa-check-circle-o"></i> Done Edit </button>
            <?php } else { ?>
                <button class="pull-right btn btn-primary" id="confirm-item"> <i class="fa fa-check-circle-o"></i> Confirm Item </button>
            <?php } ?>
        </div>
    </div>
</div>