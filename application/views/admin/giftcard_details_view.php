<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


        <!--                       CSS                       -->

        <!-- Reset Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/reset.css ' ?>" type="text/css" media="screen" />

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/style.css' ?>" type="text/css" media="screen" />

        <!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/invalid.css' ?>" type="text/css" media="screen" />	
        <link rel="stylesheet" href="<?php echo base_url() . 'resources/css/blue.css' ?>" type="text/css" media="screen" />
        <!-- Colour Schemes
  
        Default colour scheme is green. Uncomment prefered stylesheet to use it.
        
        <link rel="stylesheet" href="resources/css/blue.css" type="text/css" media="screen" />
        
        <link rel="stylesheet" href="resources/css/red.css" type="text/css" media="screen" />  
 
        -->

        <!-- Internet Explorer Fixes Stylesheet -->

        <!--[if lte IE 7]>
                <link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
        <![endif]-->

        <!--                       Javascripts                       -->

        <!-- jQuery -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery-1.3.2.min.js' ?>"></script>

        <!-- jQuery Configuration -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/simpla.jquery.configuration.js' ?>"></script>

        <!-- Facebox jQuery Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/facebox.js' ?>"></script>

        <!-- jQuery WYSIWYG Plugin -->
        <script type="text/javascript" src="<?php echo base_url() . 'resources/scripts/jquery.wysiwyg.js' ?>"></script>
        <script type="text/javascript">

        </script>
        <!-- Internet Explorer .png-fix -->

        <!--[if IE 6]>
                <script type="text/javascript" src="resources/scripts/DD_belatedPNG_0.0.7a.js"></script>
                <script type="text/javascript">
                        DD_belatedPNG.fix('.png_bg, img, li');
                </script>
        <![endif]-->

    </head>

    <body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->

            <div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->

                    <h1 id="sidebar-title"><a href="#">Simpla Admin</a></h1>

                    <!-- Logo (221px wide) -->
                    <a href="#"><img id="logo" style="background-color: #fff;width: 217px;" src=<?php echo base_url() . "assets/img/logo.png" ?> alt="Simpla Admin logo" /></a>

                    <!-- Sidebar Profile links -->
                    <div id="profile-links">
                        <?php $user = $this->session->userdata('user_details'); ?>
                        Hello, <a href="#" title="Edit your profile"><?php echo $user['first_name'] ?></a>
                        <br />
                        <!--a href="<?php echo base_url() . 'index.php' ?>" title="View the Site">View the Site</a--> 
                        <a href="<?php echo base_url() . 'logout' ?>" title="Sign Out">Sign Out</a>
                    </div>        
   

                    <?php $this->load->view('admin/menu'); ?>

                    <div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->

                        <h3>3 Messages</h3>

                        <p>
                            <strong>17th May 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>2nd May 2009</strong> by Jane Doe<br />
                            Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <p>
                            <strong>25th April 2009</strong> by Admin<br />
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
                            <small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
                        </p>

                        <form action="" method="post">

                            <h4>New Message</h4>

                            <fieldset>
                                <textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
                            </fieldset>

                            <fieldset>

                                <select name="dropdown" class="small-input">
                                    <option value="option1">Send to...</option>
                                    <option value="option2">Everyone</option>
                                    <option value="option3">Admin</option>
                                    <option value="option4">Jane Doe</option>
                                </select>

                                <input class="button" type="submit" value="Send" />

                            </fieldset>

                        </form>

                    </div> <!-- End #messages -->

                </div></div> <!-- End #sidebar -->
            <?php
            ?>
            <div id="main-content"> <!-- Main Content Section with everything -->
                <form action="<?php echo base_url() ?>" onsubmit='return validate();' method="post">
                    <noscript> <!-- Show a notification if the user has disabled javascript -->
                        <div class="notification error png_bg">
                            <div>
                                Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
                            </div>
                        </div>
                    </noscript>

                    <!-- Page Head -->
                    <!--h2>Welcome</h2-->

                    <!--p id="page-intro">What would you like to do?</p-->

                    <ul class="shortcut-buttons-set">

                    </ul><!-- End .shortcut-buttons-set -->

                    <div class="clear"></div> <!-- End .clear -->
                    <div class="content-box-header">
                        <h3>Gift Card Details </h3>

                        <div class="clear"></div>
                    </div> <!-- End .content-box-header -->

                    <div class="content-box-content">					
                        <fieldset> 
                            <p>
                                <label>Order No : <?php echo $order_summary[0]['invoice_no']; ?></label>
                            </p>
                            <p>
                                <label></label>
                            </p>
                        </fieldset>
                    </div>

                    <div style="clear:both;" ></div>
                    <div id="address_table" style ="overflow: hidden;">
                        <table style = "width: 100%;" border-collapse: collapse; border-spacing: 0;>
                               <tbody style = "display: table-row-group; vertical-align: middle; border-color: inherit;">	
                                <tr style = "display: table-row; vertical-align: inherit; border-color: inherit;">
                                    <th style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; 
                                        font-size: 21px; text-align: left; padding: 5px 0;">Billing to:</th>
                                    <th style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; 
                                        font-size: 21px; text-align: left; padding: 5px 0;"></th>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;">

                                        <?php echo $billing[0]['first_name'] . ' ' . $billing[0]['last_name']; ?>
                                    </td>
                                    <td style = "font-size: 20px;">

                                    </td>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;">
                                        <?php echo ($billing[0]['address1'] == '' || $billing[0]['address1'] == '0') ? '' : $billing[0]['address1']; ?>
                                    </td>
                                    <td style = "font-size: 20px;">

                                    </td>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;">
                                        <?php
                                        echo (trim($billing[0]['address2']) == '' || $billing[0]['address2'] == '0') ? '' : $billing[0]['address2'] . ', ';
                                        echo (trim($billing[0]['city']) == '' || $billing[0]['city'] == '0') ? '' : $billing[0]['city'] . ', ';
                                        echo (trim($billing[0]['state']) == '' || $billing[0]['state'] == '0') ? '' : $billing[0]['state'] . ', ';
                                        echo (trim($billing[0]['zipcode']) == '' || $billing[0]['zipcode'] == '0') ? '' : 'Zip - ' . $billing[0]['zipcode'];
                                        ?>
                                    </td>
                                    <td style = "font-size: 20px;">

                                    </td>
                                </tr>

                            </tbody>	
                        </table>
                        <?php
                        if (trim($order_summary[0]['comments']) !== '') {
                            ?>
                            <div style = "font-size: 18px; padding-top: 10px;" >
                                <span>
                                    Comments :
                                </span>
                                <b>
                                    <?php
                                    echo $order_summary[0]['comments'];
                                    ?>
                                </b>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="content-box"><!-- Start Content Box -->

                        <div class="content-box-header">

                            <h3>Gift Cards</h3>

                            <!--ul class="content-box-tabs">
                                    <li><a href="#tab1" class="default-tab">Table</a></li> 
                                    <li><a href="#tab2">Forms</a></li>
                            </ul-->

                            <div class="clear"></div>

                        </div> <!-- End .content-box-header -->

                        <div class="content-box-content">

                            <div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->

                                <div class="notification attention png_bg">
                                        <!--a href="#" class="close"><img src="resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a-->
                                    <!--div>
                                            This is a Content Box. You can put whatever you want in it. By the way, you can close this notification with the top-right cross.
                                    </div-->
                                </div>
                                <table id="grid">

                                    <thead>
                                        <tr>
                                            <!--th><input class="check-all" type="checkbox" /></th-->
                                            <th>Card Number</th>
                                            <th>Price</th>
                                            <th>Used</th>
                                            <th>Sender Name</th>
                                            <th>Sender Email</th>
                                            <th>Receiver Name </th>
                                            <th>Receiver Email </th>
                                        </tr>

                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <td colspan="6">
                                                <div class="clear"></div>
                                            </td>
                                        </tr>
                                    </tfoot>

                                    <tbody>
                                        <?php
                                        foreach ($order['giftcard_details'] as $giftcard_details):
                                            ?>
                                            <tr  >
                                                <!--td><input type="checkbox" /></td-->
                                                <td><?php echo $giftcard_details['card_number']; ?></td>
                                                <td><?php echo $giftcard_details['amount']; ?></td>
                                                <td><?php echo $giftcard_details['used']; ?></td>
                                                <td><?php echo $giftcard_details['sender_name']; ?></td>
                                                <td><?php echo $giftcard_details['sender_email']; ?></td>
                                                <td><?php echo $giftcard_details['recipient_name']; ?></td>
                                                <td><?php echo $giftcard_details['recipient_email']; ?></td>

                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>
                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <!--a href="<?php echo base_url() . 'index.php/admin/pdf' ?>">Create Pdf</a-->
                    <div class="clear"></div>
                    <div id="footer">
                        <small>
                            &#169; Copyright 2013 | Powered by <a href="#"></a> | <a href="<?php echo base_url() . 'index.php/admin/admin_view' ?>">BACK</a>
                        </small>
                    </div><!-- End #footer -->
                </form>
            </div> <!-- End #main-content -->

    </body>

</html>
