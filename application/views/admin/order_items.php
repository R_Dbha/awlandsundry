<?php
foreach ($order['items'] as $key => $item) {
    if (strtolower($item['item_name']) != 'gift card') {
        $shoedesign = $item['shoedesign'];
        $m = $item['measurement'];
        ?>
        <div class="chart tab-pane " id="item-<?php echo $key + 1; ?>">
            <div class="details">
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="overlay"></div>
                        <div class="loading-img"></div>
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Item Details</h3>
                            <div class="box-tools pull-right">
                                <button class="btn-edit-sizing btn btn-primary btn-xs edit-btn">
                                    <i class="fa fa-pencil"> Edit</i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <div class="not-editable" id="sizing-<?php echo $item['design_id']; ?>">
                                    <dt>Price :</dt>
                                    <dd>$<?php echo $item['item_amt']; ?></dd>
                                    <dt>Quantity :</dt>
                                    <dd><?php echo $item['quantity'] ?> PAIR</dd>
                                    <hr />
                                    <dt>Left shoe size :</dt>
                                    <dd><?php echo $item['left_shoe'] . ' ' . $item['left_width']; ?></dd>
                                    <dt>Right shoe size :</dt>
                                    <dd><?php echo $item['right_shoe'] . ' ' . $item['right_width']; ?></dd>
                                    <?php if (isset($m['left']['size']) && trim($m['left']['size']) != '' && $m['left']['size'] > 0) { ?>
                                        <dt>Left measurement :</dt>
                                        <dd>Length : <?php echo $m['left']['size'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?> </dd>
                                        <dd>Width : <?php echo $m['left']['width'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dd>Height : <?php echo $m['left']['girth'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dd>Instep : <?php echo $m['left']['instep'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dd>Heel : <?php echo $m['left']['heel'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dd>Ankle : <?php echo $m['left']['ankle'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dt>Right measurement :</dt>
                                        <dd>Length : <?php echo $m['right']['size'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?> </dd>
                                        <dd>Width : <?php echo $m['right']['width'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dd>Height : <?php echo $m['right']['girth'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dd>Instep : <?php echo $m['right']['instep'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dd>Heel : <?php echo $m['right']['heel'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                        <dd>Ankle : <?php echo $m['right']['ankle'] . ' ' . (isset($m['size_type']) ? strtolower($m['size_type']) : 'cm'); ?></dd>
                                    <?php } ?>
                                </div>
                            </dl>
                            <div class="editable " id="edit-size-<?php echo $item['design_id']; ?>">
                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Price </div>
                                    <div class="col-lg-4 no-padding">Quantity </div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control item_amt" placeholder="Price" value="<?php echo $item['item_amt']; ?>" />
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control quantity" placeholder="Quantity" value="<?php echo $item['quantity'] ?>" />
                                    </div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Monogram </div>
                                    <div class="col-lg-4 no-padding"> </div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control item_monogram" placeholder="Monogram" value="<?php echo strtoupper($shoedesign['monogram_text']); ?>" />
                                    </div>
                                    <div class="col-lg-4 no-padding"></div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Size</div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <select class="form-control left_shoe">
                                            <option value="">Left Shoe size</option>
                                            <?php echo $item['details']['left_shoe']; ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <select class="form-control right_shoe">
                                            <option value="">Right Shoe size</option>
                                            <?php echo $item['details']['right_shoe']; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <select class="form-control left_width">
                                            <option value="">Left shoe width</option>
                                            <?php echo $item['details']['left_width']; ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <select class="form-control right_width">
                                            <option value="">Right shoe width</option>
                                            <?php echo $item['details']['right_width']; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Measurements</div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <select class="form-control sizeType">
                                            <option value="CM" <?php echo (isset($m['size_type'])) ? (($m['size_type'] == 'CM') ? 'selected' : '' ) : 'selected'; ?> >CM</option>
                                            <option value="IN" <?php echo (isset($m['size_type'])) ? (($m['size_type'] == 'IN') ? 'selected' : '' ) : ''; ?> >IN</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Left Length</div><div class="col-lg-4 no-padding">Right Length</div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_left_size" placeholder="Left Length" value="<?php echo $m['left']['size'] == '0' ? '' : $m['left']['size']; ?>" />
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_right_size" placeholder="Right Length" value="<?php echo $m['right']['size'] == '0' ? '' : $m['right']['size']; ?>" />
                                    </div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Left Width</div><div class="col-lg-4 no-padding">Right Width</div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_left_width" placeholder="Left Width" value="<?php echo $m['left']['width'] == '0' ? '' : $m['left']['width']; ?>" />
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_right_width" placeholder="Right Width" value="<?php echo $m['right']['width'] == '0' ? '' : $m['right']['width']; ?>" />
                                    </div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Left Height</div><div class="col-lg-4 no-padding">Right Height</div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_lgirth" placeholder="Left Height" value="<?php echo $m['left']['girth'] == '0' ? '' : $m['left']['girth']; ?>" />
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_rgirth" placeholder="Right Height" value="<?php echo $m['right']['girth'] == '0' ? '' : $m['right']['girth']; ?>" />
                                    </div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Left Instep</div><div class="col-lg-4 no-padding">Right Instep</div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_linstep" placeholder="Left Instep" value="<?php echo $m['left']['instep'] == '0' ? '' : $m['left']['instep']; ?>" />
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_rinstep" placeholder="Right Instep" value="<?php echo $m['right']['instep'] == '0' ? '' : $m['right']['instep']; ?>" />
                                    </div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Left Heel</div><div class="col-lg-4 no-padding">Right Heel</div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_lheel" placeholder="Left Heel" value="<?php echo $m['left']['heel'] == '0' ? '' : $m['left']['heel']; ?>" />
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_rheel" placeholder="Right Heel" value="<?php echo $m['right']['heel'] == '0' ? '' : $m['right']['heel']; ?>" />
                                    </div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">Left Ankle</div><div class="col-lg-4 no-padding">Right Ankle</div>
                                </div>

                                <div class="row seperation">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_lankle" placeholder="Left Ankle" value="<?php echo $m['left']['ankle'] == '0' ? '' : $m['left']['ankle']; ?>" />
                                    </div>
                                    <div class="col-lg-4 no-padding">
                                        <input type="text" class="form-control m_rankle" placeholder="Right Ankle" value="<?php echo $m['right']['ankle'] == '0' ? '' : $m['right']['ankle']; ?>" />
                                    </div>
                                </div>
                                <div class="row seperation">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-4 no-padding">
                                        <button type="button" class="btn btn-success done-edit-size">Done</button>
                                        <button type="button" class="btn btn-reset cancel-edit">Cancel</button>
                                        <input type="hidden" class="design_id" value="<?php echo $item['design_id']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-solid">
                        <div class="overlay"></div>
                        <div class="loading-img"></div>
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Design Details</h3>
                            <div class="box-tools pull-right">
                                <button class="btn-edit-shoe btn btn-primary btn-xs edit-btn">
                                    <i class="fa fa-pencil"> Edit</i>
                                </button>
                            </div>
                        </div>

                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <?php if ($item['can_show_details'] == 1 && $item['last_id'] != 6) { ?>
                                    <dt>Last/Style :</dt>
                                    <dd><?php echo $shoedesign['last_name'] . "/" . $shoedesign['style_name'] . " (" . $shoedesign['manufacturer_code'] . ")"; ?></dd>

                                    <dt>Quarter :</dt>
                                    <dd><?php echo $shoedesign['quarter_material']; ?> , <?php echo $shoedesign['quarter_color']; ?></dd>

                                    <?php if (!$shoedesign['t_is_nothing']) { ?>
                                        <dt>Toe :</dt>
                                        <dd><?php echo $shoedesign['toe_type'] . ($shoedesign['toe_material_id'] > 1 ? ', ' . $shoedesign['toe_material'] . ', ' . $shoedesign['toe_color'] : ''); ?></dd>
                                    <?php } ?>

                                    <?php if (!$shoedesign['v_is_nothing'] && $shoedesign['is_vamp_enabled']) { ?>
                                        <dt>Vamp :</dt>
                                        <dd><?php echo $shoedesign['vamp_type'] . ($shoedesign['vamp_material_id'] > 1 ? ', ' . $shoedesign['vamp_material'] . ', ' . $shoedesign['vamp_color'] : ''); ?></dd>
                                    <?php } ?>

                                    <?php if (!$shoedesign['e_is_nothing']) { ?>
                                        <dt>Eyestays :</dt>
                                        <dd><?php echo $shoedesign['eyestay_type'] . ($shoedesign['eyestay_material_id'] > 1 ? ', ' . $shoedesign['eyestay_material'] . ', ' . $shoedesign['eyestay_color'] : ''); ?></dd>
                                    <?php } ?>

                                    <?php if (!$shoedesign['f_is_nothing']) { ?>
                                        <dt>Foxing :</dt>
                                        <dd><?php echo $shoedesign['foxing_type'] . ($shoedesign['foxing_material_id'] > 1 ? ', ' . $shoedesign['foxing_material'] . ', ' . $shoedesign['foxing_color'] : ''); ?></dd>
                                    <?php } ?>

                                    <?php if ($item['hasPatina'] > 0) { ?>
                                        <dt style="color: red;">Patina :</dt>
                                        <dd style="color: red; font-weight: bold"><?php echo $item['patina_material']; ?> , <?php echo $item['patina_color']; ?></dd>
                                    <?php } ?>

                                    <?php if (isset($item['soleName']) && isset($item['soleColorName'])) { ?>
                                        <dt>Sole :</dt>
                                        <dd><?php echo $item['soleName'] . ' / ' . $item['soleColorName']; ?></dd>
                                    <?php } ?>

                                    <?php if (isset($item['soleWelt'])) { ?>
                                        <dt>Welt :</dt>
                                        <dd><?php echo $item['soleWelt']; ?></dd>
                                    <?php } ?>

                                    <?php /* <dt>Stitching :</dt>
                                      <dd><?php echo $shoedesign['stitch_name']; ?>
                                      <span class="color" style="background: <?php echo $shoedesign['stitch_color']; ?>"></span>
                                      </dd> */ ?>
                                    <?php if ($shoedesign['is_lace_enabled']) { ?>
                                        <dt>Laces :</dt>
                                        <dd><?php echo $shoedesign['lace_name']; ?>
                                            <span class="color" style="background: <?php echo $shoedesign['color_code']; ?>"></span>
                                        </dd>
                                    <?php } ?>

                                    <?php if (isset($item['shoetree'])) { ?>
                                        <?php if ($item['shoetree'] == 'true') { ?>
                                            <dt>Shoetree :</dt>
                                            <dd>YES</dd>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ($item['can_show_details'] == 0 && (isset($item['is_readywear']) && $item['is_readywear'] > 0 || $item['last_id'] == 6)) { ?>
                                    <dt>Last/Style :</dt>
                                    <dd><?php echo "The Ready To Wear/" . $item['style_name']; ?></dd>
                                <?php } ?>

                                <?php $class = ($shoedesign['monogram_id'] !== NULL && trim($shoedesign['monogram_text']) !== '') ? 'monogram_text' : 'hidden monogram_text'; ?>
                                <dt class="<?php echo $class; ?>">Monogram :</dt>
                                <dd class="<?php echo $class; ?>"><?php echo strtoupper($shoedesign['monogram_text']) . ' On Left Shoe Left Side'; ?></dd>
                                <?php if ($item['reference_order_no'] != '') { ?>
                                    <dt >Reference Order No.</dt>
                                    <dd ><?php echo $item['reference_order_no']; ?></dd>
                                <?php } ?>
                            </dl>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-solid">
                        <div class="box-header">
                            <i class="fa fa-text-width"></i>
                            <h3 class="box-title">Images</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <ul class="order_images">
                                <?php
                                $file_path = ApplicationConfig::ROOT_BASE . 'files/';
                                if (isset($item['shop_images']) && is_array($item['shop_images'])) {
                                    foreach ($item['shop_images'] as $k => $value) {
                                        ?>
                                        <li><img src="<?php echo $file_path . $value['file_name']; ?>" /></li>
                                    <?php } ?>
                                <?php } else { ?>
                                    <?php if ($item['hasPatina'] > 0) { ?>
                                        <li><img src="<?php echo $file_path . 'designs/' . $item['patinaImg']; ?>" /></li>
                                    <?php } ?>
                                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A0.png"; ?>" /></li>
                                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A1.png"; ?>" /></li>
                                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A2.png"; ?>" /></li>
                                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A3.png"; ?>" /></li>
                                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A4.png"; ?>" /></li>
                                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A5.png"; ?>" /></li>
                                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A6.png"; ?>" /></li>
                                    <li><img src="<?php echo $file_path . 'designs/' . $item['image_file'] . "_A7.png"; ?>" /></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php if ($status[0]['status'] == "order shipped" && !$item['is_remake_request_sent']) { ?>
                    <div class=" col-md-6">
                        <div class="box box-solid revokeshoe">
                            <div class="box-header">
                                <i class="fa fa-text-width"></i>
                                <h3 class="box-title">Remake Shoe</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn-revoke-shoe btn btn-primary btn-xs edit-btn">
                                        <i class="fa fa-pencil"> Remake Shoe</i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="editable" id="edit-size-<?php echo $item['design_id']; ?>">

                                    <div class="row seperation">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-4 no-padding">Order id :</div>
                                        <div class="col-lg-5 no-padding">
                                            <input type="text" class="form-control order_id" placeholder="Order ID" value="<?php echo $item['order_id'] ?>" readonly />
                                        </div>
                                    </div>
                                    <div class="row seperation">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-4 no-padding">Invoice No :</div>
                                        <div class="col-lg-5 no-padding">
                                            <input type="text" class="form-control invoice_no"
                                                   placeholder="Invoice No"
                                                   value="<?php echo $order_summary[0]['invoice_no']; ?>" readonly />
                                        </div>
                                    </div>
                                    <div class="row seperation">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-4 no-padding">Customer Name :</div>
                                        <div class="col-lg-5 no-padding">
                                            <input type="text" class="form-control customer_name"
                                                   placeholder="Customer Name"
                                                   value="<?php echo $billing[0]['first_name'] . ' ' . $billing[0]['last_name']; ?>" readonly />
                                            <input type="hidden" class="form-control customer_id"
                                                   placeholder="Customer Name"
                                                   value="<?php echo $order_summary[0]['user_id']; ?>" />
                                        </div>
                                    </div>
                                    <div class="row seperation">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-4 no-padding">Vendor Name :</div>
                                        <div class="col-lg-5 no-padding">
                                            <input type="text" class="form-control vendor_name" placeholder="Vendor Name" value="<?php echo $vendor['first_name'] . ' ' . $vendor['last_name'] ?>" readonly />
                                            <input type="hidden" class="form-control vendor_id" placeholder="Vendor Name" value="<?php echo $vendor['user_id']; ?>"  />
                                        </div>
                                    </div>
                                    <div class="row seperation">
                                        <div class="col-lg-2"></div>
                                        <div class="col-lg-4 no-padding">Comment :</div>
                                        <div class="col-lg-4 no-padding">
                                            <textarea class="form-control comment"
                                                      placeholder="Comment"
                                                      value="" ></textarea>
                                            <input type="hidden" class="design_id" value="<?php echo $item['design_id'] ?>"/>
                                        </div>
                                    </div>
                                    <div class="row seperation">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4 no-padding">
                                            <button type="button" class="btn btn-success done-revokeshoe">Done</button>
                                            <button type="button" class="btn btn-reset cancel-edit">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="add-edit" id="edit-item-<?php echo $key + 1; ?>" >
                <?php $this->load->view('admin/order_add_item', @$edit); ?>
            </div>
        </div>
    <?php } ?>
<?php } ?>