<?php require_once('includes/header.php'); ?>
<style type="text/css">
    form#register_form span {font-size: 13px !important; margin-top: 5px !important;}
</style>

<div id="registration-wrapper">
    <div id="register" class="container">
        <div id="reg_container">
            <form id="register_form" method="post" action="<?php echo base_url(); ?>signup">
                <h3 class="page-title t-left"></h3>
                <div class="reg_form_inner">
                    <p>
                        <label>First Name</label>
                        <input type="text" name="fname" value="<?php echo @$fname ?>" required/>
                        <span><?php echo @$errors['fname'] ?></span>
                    </p>
                    <p>
                        <label>Last Name</label>
                        <input type="text" name="lname" value="<?php echo @$lname ?>" required/>
                        <span><?php echo @$errors['lname'] ?></span>
                    </p>
                    <p>
                        <label>Email address</label>
                        <input type="email" name="email" value="<?php echo @$email ?>" required/>
                        <span style="color:red;"><?php echo @$errors['emailunique'] ?></span><span style="color:red;"><?php echo @$errors['emailvalid'] ?></span>
                    </p>
                    <p>
                        <label>Password</label>
                        <input type="password" name="password"  required/>
                        <span style="color:red;"><?php echo @$errors['passlength'] ?></span>
                    </p>
                    <p>
                        <label>Confirm Password</label>
                        <input type="password" name="conf_password"  required/>
                        <span style="color:red;"><?php echo @$errors['conf_password'] ?></span>
                    </p>
                    <div id="agree">
                        <h5 style="display:none;">AWL MEMBERSHIP</h5>
                        <div class="agree">
                            <p>By clicking on register, you agree to Awl &amp; Sundry's  <a href="<?php echo base_url(); ?>terms-and-conditions" style="color:#23A9CD;">Terms of use </a> and <a href="<?php echo base_url(); ?>privacy-policy" style="color:#23A9CD;"> Privacy Policy.</a></p>                       
                        </div>
                    </div>
                </div>

                <div class="bottomline"></div>
                <div class="left">
                    <a class="back" href="<?php echo base_url(); ?>login">Back</a>
                </div>
                <div class="right">
                    <input type="submit" value="Register" name="register" id="btnRegister"/>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require_once('includes/footer.php'); ?>