<div id="order_history_view" class="order_details" >
    <div class="dasboard-right-content address">
        <div class="dashboard-content-heading-wrapper">
            <div class="dashboard-content-heading left">
                <?php
                $shipping = $order['shipping'][0];
                $billing = $order['billing'][0];
                $orderstatus = $order['orderstatus'][0];
                ?>
                <h3><?php echo date('y/m/d', strtotime($orderstatus['order_date'])); ?> - <?php echo $orderstatus['status']; ?></h3>
            </div>
        </div>
        <div class="order_history_view_container">
            <div class="left order_history_view_address">
                <h3>Shipping Address</h3>
                <div>

                    <p><?php echo $shipping['firstname'] . ' ' . $shipping['lastname'] ?></p>
                    <p><?php echo $shipping['address1'] ?></p>
                    <p><?php echo $shipping['address2'] ?></p>
                    <p><?php echo $shipping['city'] . ', ' . $shipping['state'] . ', ' . $shipping['zipcode']; ?></p>
                    <p><?php echo $shipping['state']; ?></p>
                    <p>T: <?php echo $shipping['telephone']; ?></p>
                </div>
            </div>
            <div class="right order_history_view_address">
                <h3>Billing Address</h3>
                <div>
                    <p>
                        <?php
                        if ($billing['first_name'] != '' && $billing['first_name'] != '0') {
                            echo $billing['first_name'];
                        }
                        if($billing['last_name']!='' && $billing['last_name']!= '0'){
                          echo ' '.$billing['last_name'];  
                        } 
                        ?>
                    </p>
                    <p>
                        <?php
                        if ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') {
                            echo $billing['address1'];
                        }
                        ?>
                    </p>
                    <p>
                        <?php
                        if ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') {
                            echo $billing['address2'];
                        }
                        ?>
                    </p>
                    <p>
                        <?php
                        if ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') {
                            echo $billing['city'].',';
                        }
                        if ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') {
                            echo $billing['state'].',';
                        }
                        if ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') {
                            echo $billing['zipcode'];
                        }
                        ?>
                    </p>
                    <!--p>
                        <?php
                        if ($billing['state'] != '' && $billing['state'] != '0') {
                            echo $billing['state'];
                        }
                        ?>
                    </p-->

                </div>
            </div>
        </div>
        <div class="order_history_view_container">
            <?php
            if ($orderstatus['status'] == 'order shipped') {
                $status_desc = explode(",", $orderstatus['status_desc']);
                ?>
                <div class="left order_history_view_address">
                    <h3>Shipping Method  | <span>Track order</span></h3>
                    <div>
                        <p><?php echo $status_desc[1]; ?></p>
                        <p><?php echo $status_desc[2]; ?> </p>								
                    </div>
                </div>
<?php } else { ?>
                <div class="right order_history_view_address">
                </div>
<?php } ?>
        </div>
        <div class="order_history_view_container">
            <table width="100%">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $totalamt = 0;
                    $subtotal = 0;
                    $disc = 0;
                    foreach ($order['order']['items'] as $item) {
                        ?>
                        <tr>
                            <td><img style="width: 180px;" src="<?php echo CustomShoeConfig::FILE_BASE . 'designs/' . $item['image_file'] . "_A0.png"; ?>" >
                                <?php if ($item['monogram_text'] != NULL) { ?><span class="tleft">Monogram : <?php echo $item['monogram_text']; ?></span><?php } ?>    
                                <?php if ($item['stitch_name'] != NULL) { ?><span class="tleft">Stitching : <?php echo $item['stitch_name']; ?></span> <?php } ?>
    <?php if ($item['lace_name'] != NULL) { ?><span class="tleft" >Lacing : <?php echo $item['lace_name']; ?></span><?php } ?>

                                <span class="tleft">Shoe size : left - <?php echo $item['left_shoe']; ?>, right - <?php echo $item['right_shoe']; ?></span>
                                <span class="tleft">Shoe width: left - D, right - D</span>
                            </td>
                            <td>$<?php echo $item['item_amt'] ?></td>
                            <td>
                                <span>1</span>
                            </td>
                            <td>$<?php echo $item['item_amt'] ?></td>
                        </tr>

                        <?php
                        if (strtolower($item['item_name']) != 'gift card') {
                            $subtotal += $item['item_amt'];
                            $discountPerc = $item['discount_perc'];
                            if ($discountPerc !== 0) {
                                $disc = ($subtotal * $discountPerc) / 100;
                                $totalamt = $subtotal - $disc;
                            }
                        }
                    }
                    ?>
                    <tr class="total_amt">
                        <td></td>
                        <td colspan="2">
                            <span>Subtotal</span>
                            <span>Shipping & Handling</span>
                            <span>Tax</span>
                            <!--span>Store Credit</span-->
                            <span>Promo Code</span>
                            <span class="gtotal">Grand Total</span>
                        </td>
                        <td>
                            <span>$<?php echo $subtotal; ?></span>
                            <span>$0.00</span>
                            <span>$<?php echo '0.00' ?></span>
                            <span>$<?php echo $disc; ?></span>
                            <span class="gtotal">$<?php echo $totalamt; ?></span>
                        </td>
                    </tr>														
                </tbody>
            </table>
            <a href="#order_history" class="left">&#171; Back to my orders</a>
        </div>					
    </div>
</div>