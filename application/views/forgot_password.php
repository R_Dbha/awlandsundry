

<?php $this->load->view('includes/header'); ?>    
<div id="forgot_password_wrapper">
    <div id="forgot_password_container" class="center">
        <div id="login-wrapper">
            <div id="login" class="container">
                <h3 class="page-title">
                    <span>Forgot Password</span>
                </h3>
            </div>
        </div>    
        <form action="<?php echo base_url(); ?>forgot-password" method="post" name="awlsundry" class="sign_in" id="awlsundry">
            <input type="email" name="email" class="" id="email" placeholder="EMAIL" value="" required ><?php if ($error != NULL) { ?> <p style="color:red"> This email id is not registered with us.</p> <?php } ?>
            <input type="submit" name="submit" id="submitbutton" class="button blue" value="Submit">
        </form>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>   