<?php $this->load->view('includes/header'); ?>

<div id="pageWrapper">
    <h1> Experience AWL & SUNDRY in person. </h1>
    <h2 style="margin-top: 2em"> Get fitted for beautiful handcrafted shoes by one of our expert stylists. </h2>
    <!-- <h2 style="margin-top: 1em"> 18 West 23rd Street <br> 4th Floor <br> New York, NY 10010 </h2> -->
    <h2 style="margin-top: 1em"> 530 5th Ave,<br>New York, NY 10036 </h2>
    <a href="javascript:;" class="bookAppointmentA" data-modal-open="appointmentModal">MAKE AN APPOINTMENT</a>
</div>

<!-- <div class="appointmentModal" data-modal="appointmentModal">
    <div class="modal-inner">
        <div class="appointment_form_container centerAlign" id="form_div"> 
            <form id="appointment_form">                
                <h2>Please fill in your appointment details</h2>
                <p><input type="text"  name="name" id="name"  placeholder="*NAME" required/></p>
                <p><input type="email" name="email" id="email" placeholder="*EMAIL ADDRESS" required/></p>
                <p><input type="text"  name="phone" id="phone" placeholder="*PHONE NUMBER" required/></p>
                <p><input type="text"  name="appointment_date" id="appointment_date" placeholder="*APPOINTMENT DATE" required></p>
                <p><input type="text"  name="us_shoe_size" id="us_shoe_size" placeholder="U.S SHOE SIZE" required /></p>
                <p><input type="text"  name="special_request" id="special_request" placeholder="NOTES" /></p>
                <p><span class="errorSection" id="error"></span></p>
                <p style="margin: 10px 5px 20px 5px;"><label>* Required fields</label></p>
                <div>
                    <button type="submit" name="book_appointment" id="btnConfirm">
                        CONFIRM APPOINTMENT
                        <img src="../assets/img/add_user_loader.gif" hidden="true" id="loadIcon"/>
                    </button>
                </div>
            </form>
        </div>
        <div id="appoitnmentStatus">
            <div class="appoitnmentStatusError">
                <span style="color:red" id="statusError"></span>
            </div>
            <div class="appoitnmentStatusSuccess">
                <h2>THANK YOU!</h2>
                <hr align="center" width="90%" style="margin: 0px auto;">
                <p style="margin-top: 1rem;" id="appoint_message"></p>
                <p style="margin-bottom: 1rem;"> Our team will confirm your appointment by email shortly. </p>
                <hr align="center" width="90%" style="margin: 0px auto;">
            </div>
        </div>
        <a data-modal-close="appointmentModal" href="javascript:;" class="closeText">Close</a>
    </div>
</div> -->
<div class="appointmentModal" data-modal="appointmentModal" style="z-index:9999999">
    <div style="max-width:900px" class="modal-inner">
        <iframe src="https://app.acuityscheduling.com/schedule.php?owner=15520716&appointmentType=10500509&calendarID=3049459" width="100%" height="800" frameBorder="0"></iframe>
        <a data-modal-close="appointmentModal" href="javascript:;" class="closeText">Close</a>
    </div>
</div>

<div id="appointment_faq" class="centerAlign">
    <div style="margin-bottom: 20px;"><p class="faq-heading">FAQ</p></div>
    <div>
        <button class="accordion">What should I expect at my Awl & Sundry appointment?</button>
        <div class="panel"><p> A one of a kind custom fitting experience. One of our trained Awl & Sundry stylists will accommodate all your needs and guide you through the entire process. We will take your measurements, show you samples and make sure you leave with a smile on your face.</p></div>

        <button class="accordion">How do we respond to walk ins?</button>
        <div class="panel"><p>We appreciate scheduled appointments, if none of the available appointments work for you, please email us at wecare@awlandsundry.com and we'll do our best to accommodate your schedule.</p></div>

        <button class="accordion">What if I need to change or cancel my appointment?</button>
        <div class="panel"><p>You can change or cancel your appointment by emailing us at wecare@awlandsundry.com. A 24 hour advance notice would be appreciated.</p></div>

        <button class="accordion">How long does an appointment take?</button>
        <div class="panel"><p>Our appointments usually take about 20-30minutes, though it may vary based on your requirements.</p></div>

        <button class="accordion">Can I bring friends with me?</button>
        <div class="panel"><p>Certainly, feel free to bring your friends & family along to share the experience!</p></div>

        <button class="accordion">How long do I have to wait until I receive my order?</button>
        <div class="panel"><p>You can expect your order anywhere between 4-6 weeks, though we do make express deliveries.</p></div>
    </div>
</div>

<div class="mapIframe centerAlign">
    <!-- <iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJ-dCGdKRZwokRCB61Bc5iPdM&key=AIzaSyDE-oV3XZ3riCyUGeBysrMk1eE7iZL6aRM"></iframe> -->
        <iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.3097656911787!2d-73.98232198418253!3d40.75521117932728!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c258ffd2c075b3%3A0xdb9447516b60aa7d!2s530+5th+Ave%2C+New+York%2C+NY+10036%2C+USA!5e0!3m2!1sen!2sin!4v1561547048214!5m2!1sen!2sin"></iframe>
</div>

<script src="https://embed.acuityscheduling.com/js/embed.js" type="text/javascript"></script>
<?php $this->load->view('includes/footer'); ?>