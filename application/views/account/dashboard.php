<?php
$this->load->view('includes/header.php');
?>

<div id="dashboard-wrapper">
    <div id="dashboard" class="container dashboard-container">
        <h3 class="page-title">
            <!--span>ACCOUNT DASHBOARD</span-->
        </h3>

        <div id="dasboard-left-wrapper" class="left">
            <div id="dasboard-left-menu-wrapper">
                <ul id="dashboard-menu">
                    <li>
                        <a href="#account_dashboard" class="account-dashboard current">ACCOUNT DASHBOARD</a>
                    </li>
                    <li>
                        <a href="#account_info" class="accoun t -information">ACCOUNT INFORMATION</a>
                    </li>
                    <li>
                        <a href="#address_book" class="address-book">ADDRESS BOOK</a>
                    </li>
                    <li>
                        <a href="#order_history" class="order-history">ORDER HISTORY</a>
                    </li>
                    <li>
                        <a href="#saved_designs">SAVED DESIGNS</a>
                    </li>
                    <li>
                        <a href="#gift_certificate" class ="gift-certificate">GIFT CERTIFICATE</a>
                    </li>
                </ul>
            </div>	
        </div>	

        <div id="dashboard-right-wrapper" class="right">
            <div id="dasboard-right-content-wrapper">
                <div id="account-dashboard" class="hide">

                </div>
                <div id="account-information" class="hide none">
                    <h1 class="username">NIKIN RAWAT</h1>
                    <div class="dasboard-right-content profile">
                        <div class="dashboard-content-heading-wrapper">
                            <div class="dashboard-content-heading left">
                                <h3>PROFILE</h3>
                            </div>
                            <div class="dashboard-content-heading right">
                                <span><a>EDIT</a></span>
                            </div>
                        </div>
                        <div id="recent-orders-content">
                            <ul>
                                <li><label>FIRST NAME :</label><?php echo @$user['first_name']; ?></li>
                                <li><label>LAST NAME :</label><?php echo @$user['last_name'] ?></li>
                                <li><label>EMAIL ADDRESS :</label><?php echo @$user['email']; ?></li>
                                <li><label>PASSWORD :</label><a href="#">Reset Password</a></li>
                            </ul>
                        </div>
                    </div>	
                </div>
                <div id="add-account-information" class="hide none" style="display:none;">
                    <div class="dasboard-right-content address">
                        <div class="dashboard-content-heading-wrapper">
                            <h1 style="color:black;">CONTACT INFORMATION</h1>
                        </div>
                        <div class="contact-content">
                            <table>
                                <tr>
                                    <td width="30%">FIRST NAME </td>
                                    <td><input type="text" name="fname" id="fname" value="" required/></td>
                                </tr>
                                <tr>
                                    <td width="30%">LAST NAME </td>
                                    <td><input type="text" name="lname" id="lname" value="" required/></td>
                                </tr>
                                <tr>
                                    <td width="30%">COMAPNY </td>
                                    <td><input type="text" name="company" id="company" value=""/></td>
                                    <td>(optional)</td>
                                </tr>
                                <tr>
                                    <td width="30%">TELEPHONE </td>
                                    <td><input type="text" name="phone" id="phone" value="" required/></td>
                                </tr>
                                <tr>
                                    <td width="30%"	>FAX </td>
                                    <td><input type="text" name="fax" id="fax" value=""/></td>
                                    <td>(optional)</td>
                                </tr>
                            </table>

                        </div>
                    </div>	
                    <div class="dasboard-right-content address">
                        <div class="dashboard-content-heading-wrapper">
                            <h1 style="color:black;">ADDRESS</h1>
                        </div>
                        <div class="contact-content">
                            <table>
                                <tr>
                                    <td width="30%">STREET ADDRESS</td>
                                    <td><input type="text" name="address1" id="address1" value="" required/></td>
                                </tr>
                                <tr>
                                    <td width="30%"></td>
                                    <td><input type="text" name="address1" id="address2" value=""/></td>
                                    <td>(optional)</td>
                                </tr>
                                <tr>
                                    <td width="30%">CITY</td>
                                    <td><input type="text" name="city" value="" id="city" required/></td>

                                </tr>
                                <tr>
                                    <td width="30%">STATE / PROVINCE </td>
                                    <td><select>
                                            <option>Please select a region , state or province</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td width="30%"	>ZIP CODE </td>
                                    <td><input type="text" name="zipcode" id="zipcode" value=""  required/></td>
                                </tr>
                                <tr>
                                    <td width="30%"	>COUNTRY </td>
                                    <td><select>
                                            <option>United Sates</option>
                                        </select></td>
                                </tr>
                            </table>
                            <p class="billcheck"><input type="checkbox" name="chkBilling" id="chkBilling"/><span>Use as my default billing address</span></p>
                            <p class="billcheck"><input type="checkbox" name="chkShipping" id="chkShipping"/><span>Use as my default shipping address</span></p>

                            <p><input type="submit" value="SAVE ADDRESS" href="javascript:;" onClick="save_address();" /></p>
                        </div>
                    </div>
                </div>
                <div id="address-book" class="hide none" >
                    <div class="dasboard-right-content address">
                        <div class="dashboard-content-heading-wrapper">
                            <div class="dashboard-content-heading right">
                                <span><a class="add-account-information">ADD</a></span>
                            </div>
                        </div>
                        <div class="dashboard-content-heading-wrapper view-address">
                            <div class="dashboard-content-heading">
                                <h3 style="color: #262343;float:left;">Default Billing Address</h3>
                                <h3 style="color: #262343;float:right	;">Default Shipping Address</h3>
                            </div>
                        </div>
                        <div id="view-address-inner">
                            <div class="view-contact-contents left">
                                <p>Amit Haryan</p>
                                <p>303 7th Stree</p>
                                <p>Apt 2</p>
                                <p>Jersy City, New Jersy, 07302</p>
                                <p>United States</p>
                                <p>T: 555-555-5555</p>
                                <div class="dashboard-content-heading right">
                                    <span><a>EDIT</a></span>|<span><a>DELETE</a></span>
                                </div>					
                            </div>
                            <div class="view-contact-contents right">
                                <p>Amit Haryan</p>
                                <p>303 7th Stree</p>
                                <p>Apt 2</p>
                                <p>Jersy City, New Jersy, 07302</p>
                                <p>United States</p>
                                <p>T: 555-555-5555</p>	
                                <div class="dashboard-content-heading right">
                                    <span><a>EDIT</a></span>|<span><a>DELETE</a></span>
                                </div>			
                            </div>
                        </div>
                        <div class="dashboard-content-heading-wrapper view-address" style="clear:both;">
                            <div class="dashboard-content-heading left">
                                <h3 style="color: black;">Additional Address Entries</h3>
                            </div>
                        </div>
                        <div class="view-contact-contents left">
                            <p>Amit Haryan</p>
                            <p>303 7th Stree</p>
                            <p>Apt 2</p>
                            <p>Jersy City, New Jersy, 07302</p>
                            <p>United States</p>
                            <p>T: 555-555-5555</p>	
                            <div class="dashboard-content-heading right">
                                <span><a>EDIT</a></span>|<span><a>DELETE</a></span>
                            </div>				
                        </div>
                    </div>
                </div>
                <div id="order-history" class="hide none">
                    	
                </div>
                <div id="store-credits" class="hide none" >
                    <h3>Balance</h3>
                    <p class="description">Yout current balance is : $11.80</p>

                    </tr>
                    <tr>
                        <td width="30%">STATE / PROVINCE </td>
                        <td><select>
                                <option>Please selecta region , state or province</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td width="30%"	>ZIP CODE </td>
                        <td><input type="text" name="first" value="Nikunj"/></td>
                    </tr>
                    <tr>
                        <td width="30%"	>COUNTRY </td>
                        <td><select>
                                <option>United Sates</option>
                            </select></td>
                    </tr>
                    </table>
                    <p class="billcheck"><input type="checkbox"/><span>Use as my default billing address</span></p>
                    <p class="billcheck"><input type="checkbox"/><span>Use as my default shipping address</span></p>

                    <p><input type="submit" value="SAVE ADDRESS"/></p>
                </div>
            </div>	
        </div>
        <div id="address-book" class="hide none" >
            <div class="dasboard-right-content address">
                <div class="dashboard-content-heading-wrapper">
                    <div class="dashboard-content-heading right">
                        <span><a class="add-account-information">ADD</a></span>
                    </div>
                </div>
                <div class="dashboard-content-heading-wrapper view-address">
                    <div class="dashboard-content-heading">
                        <h3 style="color: #262343;float:left;">Default Billing Address</h3>
                        <h3 style="color: #262343;float:right	;">Default Shipping Address</h3>
                    </div>
                </div>
                <div id="view-address-inner">
                    <div class="view-contact-contents left">
                        <p>Amit Haryan</p>
                        <p>303 7th Stree</p>
                        <p>Apt 2</p>
                        <p>Jersy City, New Jersy, 07302</p>
                        <p>United States</p>
                        <p>T: 555-555-5555</p>
                        <div class="dashboard-content-heading right">
                            <span><a>EDIT</a></span>|<span><a>DELETE</a></span>
                        </div>					
                    </div>
                    <div class="view-contact-contents right">
                        <p>Amit Haryan</p>
                        <p>303 7th Stree</p>
                        <p>Apt 2</p>
                        <p>Jersy City, New Jersy, 07302</p>
                        <p>United States</p>
                        <p>T: 555-555-5555</p>	
                        <div class="dashboard-content-heading right">
                            <span><a>EDIT</a></span>|<span><a>DELETE</a></span>
                        </div>			
                    </div>
                </div>
                <div class="dashboard-content-heading-wrapper view-address" style="clear:both;">
                    <div class="dashboard-content-heading left">
                        <h3 style="color: black;">Additional Address Entries</h3>
                    </div>
                </div>
                <div class="view-contact-contents left">
                    <p>Amit Haryan</p>
                    <p>303 7th Stree</p>
                    <p>Apt 2</p>
                    <p>Jersy City, New Jersy, 07302</p>
                    <p>United States</p>
                    <p>T: 555-555-5555</p>	
                    <div class="dashboard-content-heading right">
                        <span><a>EDIT</a></span>|<span><a>DELETE</a></span>
                    </div>				
                </div>
            </div>
        </div>
        <div id="order-history" class="hide none">
            <div class="dasboard-right-content ">
                <div class="dashboard-content-heading-wrapper">
                    <div class="dashboard-content-heading left">
                        <h3 class="ordrnumcolor">Order Number : 100751214</h3>
                    </div>
                    <div class="dashboard-content-heading right">
                        <!--span><a>VIEW</a></span-->
                    </div>
                </div>
                <div class="order-history-content">
                    <div class="row1">
                        <div class="price left">
                            <div class="order-price">
                                <h2 class="left ordrnumcolor">6/12/14</h2><span class="cmpltcolor"> - Complete</span>
                            </div>
                            <div class="total-price">
                                <h3 class="ordrnumcolor">Total : $250.00</h3>
                            </div>
                        </div>
                        <div class="shoe-details left">
                            <div class="shoe-img">
                                <img src="css/images/order_img1.png">
                            </div>
                            <p class="shoe-name">Last the Eldrige <span>Oxford</span></p>
                            <p class="shoe-size">Size EE, EEE</p>
                            <p class="shoe-price ordrnumcolor">$250.00</p>
                        </div>
                        <div class="shoe-details left">
                            <div class="shoe-img">
                                <img src="css/images/order_img2.png">
                            </div>
                            <p class="shoe-name">Last the Eldrige <span>Oxford</span></p>
                            <p class="shoe-size">Size EE, EEE</p>
                            <p class="shoe-price ordrnumcolor">$250.00</p>
                        </div>
                    </div>
                    <div class="row2">
                        <input type="submit" value="TRACK YOUR ORDER"/>
                        <input type="submit" value="VIEW DETAILS"/>
                        <input type="submit" value="EXCHANGE ITEMS"/>
                    </div>
                </div>
            </div>	
            <div class="dasboard-right-content ">
                <div class="dashboard-content-heading-wrapper">
                    <div class="dashboard-content-heading left">
                        <h3 class="ordrnumcolor">Order Number : 100751214</h3>
                    </div>
                    <div class="dashboard-content-heading right">
                        <!--span><a>VIEW</a></span-->
                    </div>
                </div>
                <div class="order-history-content">
                    <div class="row1">
                        <div class="price left">
                            <div class="order-price">
                                <h2 class="left ordrnumcolor">6/12/14</h2><span class="cmpltcolor"> - Complete</span>
                            </div>
                            <div class="total-price">
                                <h3 class="ordrnumcolor">Total : $250.00</h3>
                            </div>
                        </div>
                        <div class="shoe-details left">
                            <div class="shoe-img">
                                <img src="css/images/order_img1.png">
                            </div>
                            <p class="shoe-name">Last the Eldrige <span>Oxford</span></p>
                            <p class="shoe-size">Size EE, EEE</p>
                            <p class="shoe-price ordrnumcolor">$250.00</p>
                        </div>
                        <div class="shoe-details left">
                            <div class="shoe-img">
                                <img src="css/images/order_img2.png">
                            </div>
                            <p class="shoe-name">Last the Eldrige <span>Oxford</span></p>
                            <p class="shoe-size">Size EE, EEE</p>
                            <p class="shoe-price ordrnumcolor">$250.00</p>
                        </div>
                    </div>
                    <div class="row2">
                        <input type="submit" value="TRACK YOUR ORDER"/>
                        <input type="submit" value="VIEW DETAILS"/>
                        <input type="submit" value="EXCHANGE ITEMS"/>
                    </div>
                </div>
            </div>	
        </div>
        <div id="store-credits" class="hide none" >
            <h3>Balance</h3>
            <p class="description">Yout current balance is : $11.80</p>

            <h3 style="margin-top:30px;">Redeem Gift Card</h3>
            <p class="description">Have gift card? Click <a href="#">here</a> to redeem it.</p>
            <div class=dasboard-right-content >
                <table width="100%">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Balance Change</th>
                            <th>Balance</th>
                            <th>Date</th>
                            <th>Comment</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Used</td>
                            <td>-$126.00</td>
                            <td>$11.80</td>
                            <td>6/12/14 12.30PM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>U[dated</td>
                            <td>$58.00</td>
                            <td>$137.80</td>
                            <td>3/6/14 4.30AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Used</td>
                            <td>-$126.00</td>
                            <td>$11.80</td>
                            <td>6/12/14 12.30PM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>U[dated</td>
                            <td>$58.00</td>
                            <td>$137.80</td>
                            <td>3/6/14 4.30AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Used</td>
                            <td>-$126.00</td>
                            <td>$11.80</td>
                            <td>6/12/14 12.30PM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>U[dated</td>
                            <td>$58.00</td>
                            <td>$137.80</td>
                            <td>3/6/14 4.30AM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Used</td>
                            <td>-$126.00</td>
                            <td>$11.80</td>
                            <td>6/12/14 12.30PM</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>U[dated</td>
                            <td>$58.00</td>
                            <td>$137.80</td>
                            <td>3/6/14 4.30AM</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>		
</div>
</div>	
</div>	
<script type="text/javascript">
    <?php $user = $this->session->userdata('user_details');
    ?>
        var userId = <?php echo $user['user_id']; ?>
</script>
<?php
$this->load->view('includes/footer.php');
?>
                            
