<?php if ($shoedesigns != NULL) { ?>
    <?php foreach ($shoedesigns as $shoedesign) { ?>
        <div class="item purchased"
             data-id="<?php echo $shoedesign['shoe_design_id']; ?>"
             data-mode="<?php echo $shoedesign['mode']; ?>" style="float: left">
            <div class="item_row_top">
                <span class="author"> <!--img src="<?php echo base_url(); ?>assets/css/images/icons/favicon.jpg" /-->
                    <span></span><span class="name"><?php //echo $shoedesign['first_name']; ?></span>
                </span>
            </div>
            <div class="img-wrapper saved_shoes">
                <?php
                $shoe = array();
                $shoe = json_decode($shoedesign["shoemodel"], TRUE);
                $base_url = CustomShoeConfig::IMG_BASE;
                $shoe['img_base'] = $shoe['style']['code'] . "_" . $shoe['toe']['type'] . $shoe['vamp']['type'] . $shoe['eyestay']['type'] . $shoe['foxing']['type'];
                ?>
                <div class="shoe">
                    <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['quarter']['matfolder'] . "/" . $shoe['style']['code'] . "_" . $shoe['quarter']['base'] . '_A1' . '.png'; ?>" />
                    <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['toe']['matfolder'] . "/" . $shoe['img_base'] . $shoe['toe']['material'] . $shoe['toe']['color'] . $shoe['toe']['part'] . '_A1' . '.png'; ?>" class="float-top" />
                    <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['eyestay']['matfolder'] . "/" . $shoe['img_base'] . $shoe['eyestay']['material'] . $shoe['eyestay']['color'] . $shoe['eyestay']['part'] . '_A1' . '.png'; ?>" class="float-top" />
                    <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['vamp']['matfolder'] . "/" . $shoe['img_base'] . $shoe['vamp']['material'] . $shoe['vamp']['color'] . $shoe['vamp']['part'] . '_A1' . '.png'; ?>" class="float-top" />
                    <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['foxing']['matfolder'] . "/" . $shoe['img_base'] . $shoe['foxing']['material'] . $shoe['foxing']['color'] . $shoe['foxing']['part'] . '_A1' . '.png'; ?>" class="float-top" />
                    <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['stitch']['folder'] . "/" . $shoe['img_base'] . $shoe['stitch']['code'] . '_A1' . '.png'; ?>" class="float-top" />
                    <img onerror="this.style.display = 'none';" src="<?php echo $base_url . $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['lace']['folder'] . "/" . $shoe['img_base'] . $shoe['lace']['code'] . '_A1' . '.png'; ?>" class="float-top" />
                </div>    
                <p>
                    <a href="javascript:void(0)" class="change_public_name" title="Click to Change"><?php echo ($shoedesign['public_name'] == null) ? 'No Name' : $shoedesign['public_name']; ?></a>
                    <input type="text" class="public_name" value="<?php echo $shoedesign['public_name']; ?>"> |
                    <a href="javascript:void(0)" ><?php echo date('Y-m-d H:iA', strtotime($shoedesign['date_created'])); ?></a>
                </p>
                <p><?php echo $shoedesign['last_name'] . '/' . $shoedesign['style_name']; ?></p>
                <p>
                    <a href="javascript:;" onclick="edit_savedshoe(<?php echo $shoedesign['shoe_design_id']; ?>);">EDIT</a> | 
                    <a href="javascript:;" onclick="purchase_savedshoe(<?php echo $shoedesign['shoe_design_id']; ?>);">ADD TO CART</a> | 
                    <a href="javascript:;" onclick="delete_savedshoe(<?php echo $shoedesign['shoe_design_id']; ?>);">DELETE</a>
                </p>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>
    <p style="text-align: center">No designs saved yet!</p>
<?php } ?>