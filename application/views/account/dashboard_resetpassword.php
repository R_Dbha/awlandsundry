<div class="reset_password">
    <div class="dashboard-content-heading-wrapper">
        <h1 style="color:black;">RESET PASSWORD</h1>
    </div>
    <div class="contact-content" style="color:brown">
        <form method="POST" onsubmit="return reset_password();">
            <table style="font-size: 12px;">
                <tr>
                    <td width="35%" style="">CURRENT PASSWORD</td>
                    <td><input style="height:25px;border:1px solid #ccc" type="password" name="pwd" id="pwd" value="" required/></td>
                    <td><span style="color:red;" id="pwd_message"><?php echo @$error['pwd']; ?></span></td>
                   <!--td><span id="fname_Err" class="hide">First Name Required</span></td-->
                </tr>
                <tr>
                    <td width="35%">NEW PASSWORD</td>
                    <td><input style="height:25px;border:1px solid #ccc"  type="password" name="new_pwd" id="new_pwd" value="" required pattern=".{6,10}" required title="6 to 10 characters"/></td>
                    <td><span id="new_pwd_message"><?php echo @$error['new_pwd']; ?></span></td>
                    <!--td><span id="lname_Err" class="hide">Last Name Required</span></td-->
                </tr>
                <tr>
                    <td width="35%">CONFIRM PASSWORD</td>
                    <td><input style="height:25px;border:1px solid #ccc"  type="password" name="confirm_pwd" id="confirm_pwd" value="" required minlength="6"/></td>
                    <td><span id="message"><?php echo @$error['confirm_pwd']; ?></span></td>
                </tr>
            </table><P></p>
            <input type="submit" style="float: right;margin-right: 183px;" name="submit" value="SAVE" href="javascript:;" />
            <!--a href="#account_info" >BACK</a-->
        </form>

    </div>
</div>