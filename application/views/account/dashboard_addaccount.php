<div class="add_account">
<form method="POST" onsubmit="return save_address();" >
    <div class="dasboard-right-content address">
    <div class="dashboard-content-heading-wrapper">
        <h1 style="color:black;">CONTACT INFORMATION</h1>
    </div>
    <input type="hidden" id="hid_from" value="<?php echo @$from ?>"/>
    <div class="contact-content">
        <input type="hidden" id="address_det_id" name="address_det_id" value="<?php echo @$data['address_details_id']; ?>" />
        <table>
            <tr>

                <td width="30%">FIRST NAME </td>
                <td><input type="text" name="fname" id="fname" class="<?php echo @$error_class['fname']; ?>" value="<?php echo @$data['first_name']; ?>" required="required"/></td>
            <label class="<?php echo @$error_class['fname']; ?>" ><?php echo @$errors['fname']; ?></label>
            </tr>
            <tr>
                <td width="30%">LAST NAME </td>
                <td><input type="text" name="lname" id="lname" value="<?php echo @$data['last_name']; ?>" required/></td>
                <!--td><span id="lname_Err" class="hide">Last Name Required</span></td-->
            </tr>
            <tr>
                <td width="30%">COMPANY </td>
                <td><input type="text" name="company" id="company" value="<?php echo @$data['company']; ?>"/></td>
                <td>(optional)</td>
            </tr>
            <tr>
                <td width="30%">TELEPHONE </td>
                <td><input type="text" name="phone" id="phone" value="<?php echo @$data['phone']; ?>" required/></td>
            </tr>
            <tr>
                <td width="30%"	>FAX </td>
                <td><input type="text" name="fax" id="fax" value="<?php echo @$data['fax']; ?>"/></td>
                <td>(optional)</td>
            </tr>
        </table>

    </div>
</div>	
<div class="dasboard-right-content address">
    <div class="dashboard-content-heading-wrapper">
        <h1 style="color:black;">ADDRESS</h1>
    </div>
    <div class="contact-content">
        <table>
            <tr>
                <td width="30%">STREET ADDRESS</td>
                <td><input type="text" name="address1" id="address1" value="<?php echo @$data['address1']; ?>" required/></td>
            </tr>
            <tr>
                <td width="30%"></td>
                <td><input type="text" name="address1" id="address2" value="<?php echo @$data['address2']; ?>"/></td>
                <td>(optional)</td>
            </tr>
            <tr>
                <td width="30%">CITY</td>
                <td><input type="text" name="city" value="<?php echo @$data['city']; ?>" id="city" required/></td>

            </tr>
            <tr>
                <td width="30%">STATE / PROVINCE </td>
                <td><select id="ddlState" value="<?php echo @$data['state']; ?>">
                        <!--option value="-1">Please select a region , state or province</option>
                        <option value="NJ">New JERCY</option-->
                        <?php foreach ($states as $s) {
                            if ($s['id'] == @$data['state']) {
                                ?>
                                <option selected value="<?php echo $s['id'] ?>"><?php echo $s['name'] ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $s['id'] ?>"><?php echo $s['name'] ?></option>
    <?php }
} ?>
                    </select></td>
            </tr>
            <tr>
                <td width="30%"	>ZIP CODE </td>
                <td><input type="text" name="zipcode" id="zipcode" value="<?php echo @$data['zipcode']; ?>"  required/></td>
            </tr>
            <tr>
                <td width="30%"	>COUNTRY </td>

                <td><select id="ddlCountry">
                        <?php foreach ($countries as $c) {
                            if ($c['id'] == @$data['country']) {
                                ?>
                                <option selected value="<?php echo $c['id'] ?>"><?php echo $c['name'] ?></option>
    <?php } else { ?>
                                <option value="<?php echo $c['id'] ?>"><?php echo $c['name'] ?></option>
    <?php }
} ?>

                    </select></td>
            </tr>
        </table>

        <p class="billcheck"><input type="checkbox" name="chkBilling" id="chkBilling"  <?php echo (@$data['is_billing'] == 1 ? 'checked' : ''); ?>/><span>Use as my default billing address</span></p>
        <p class="billcheck"><input type="checkbox" name="chkShipping" id="chkShipping"  <?php echo (@$data['is_shipping'] == 1 ? 'checked' : ''); ?>/><span>Use as my default shipping address</span></p>

        <p><input type="submit" value="SAVE ADDRESS" href="javascript:;"  /></p>
    </div>
</div></form>
</div>