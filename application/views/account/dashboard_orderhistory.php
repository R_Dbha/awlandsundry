<div class="dasboard-right-content ">
    <div class="overlay"></div>
    <div id="tracking-popup-wrapper" class="popup-wrapper"></div>
    <div id="index-popup-wrapper" class="" style="background:#373737">
        <div id="index-popup">
            <a onclick="javascript:return closePopUp();"><span class="close"></span></a>
            <div class="newletter-content">
                <h1>EXCHANGE ITEMS</h1>
            </div>
            <div class="newletter-content">
                <p>To exchange your Awl & Sundry pair, please send us an email at <?php echo $this->config->item('contact_email'); ?> with your order no. and reason for exchange.</p>
            </div>
            <div>
                <p Style="margin-top:20px;">
                    <a href="javascript:void(0);" value="Close" onclick="javascript:return closePopUp();" Style="color: #fff;font-size: 20px;">Close</a>
                </p>
            </div>
            <div class="newletter-content">
                <p class="none show"></p>
            </div>
        </div>
    </div>
    <?php if ($details != NULL) { ?> 
        <?php foreach ($details as $order) { ?>
            <div class="dashboard-content-heading-wrapper" style="clear:both;">
                <div class="dashboard-content-heading left">
                    <h3 class="ordrnumcolor">Order Number : <?php echo $order['invoice_no'] ?></h3>
                </div>
                <div></div>
            </div>
            <div class="order-history-content">
                <div class="row1">
                    <div class="price left">
                        <div class="order-price">
                            <?php $orderdate = date('d/m/y', strtotime($order['add_date'])); ?>
                            <h2 class="left ordrnumcolor"><?php echo $orderdate ?></h2>
                            <?php if ($order['status'] == 'order shipped') { ?>
                                <span class="cmpltcolor"> - Completed </span>
                            <?php } else if ($order['status'] == 'Manufacturing in progress' || $order['status'] == 'Order acknowledged') { ?>
                                <span class="cmpltcolor"> - In the works </span>
                            <?php } else { ?>
                                <span class="cmpltcolor" style="color:red;"> - <?php echo $order['status'] ?></span>
                            <?php } ?>
                        </div>
                        <div class="total-price">
                            <h3 class="left ordrnumcolor">Total : $<?php echo number_format(round($order['net_amt']), 2); ?></h3>
                        </div>
                    </div>
                    <div class="shoe-details-wrapper">
                        <?php foreach ($order['items'] as $item) { ?>
                            <div class="shoe-details left">
                                <div class="shoe-img">
                                    <?php if (isset($item['shop_images']) && is_array($item['shop_images'])) { ?>
                                        <img style="width: 180px;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/' . $item['shop_images'][0]['file_name']; ?>" />
                                    <?php } else { ?>
                                        <img style="width: 180px;" src="<?php echo CustomShoeConfig::FILE_BASE . 'designs/' . $item['image_file'] . "_A0.png"; ?>" >
                                    <?php } ?>
                                </div>
                                <p class="shoe-name"><?php echo $item['last_name'] ?> /<span><?php echo $item['style_name'] ?></span></p>
                                <p class="shoe-size">Size : left - <?php echo $item['left_shoe']; ?>, right - <?php echo $item['right_shoe']; ?></p>
                                <?php if ($item['left_width'] != NULL) { ?><p class="shoe-size">Width : left - <?php echo $item['left_width'] ?>, right - <?php echo $item['right_width'] ?></p> <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="row2">
                    <?php //if($order['status'] != "order shipped"){  ?>
                    <span>
                        <input type="submit" class="track_order" value="TRACK YOUR ORDER"/>
                        <input type="hidden" class="shipping_carrier" value="<?php echo @$order['shipping_carrier']; ?>"/>
                        <input type="hidden" class="tracking_number" value="<?php echo @$order['tracking_number']; ?>"/>
                    </span>
                    <?php //}  ?>
                    <input id="btn_order_view_details" type="submit" value="VIEW DETAILS" onclick="return btn_order_view_details('<?php echo $order['order_id'] ?>');"/>
                    <input type="submit" value="EXCHANGE ITEMS" onclick="javascript:showPopUpExchangeShoe(); return false;"/>
                </div>
            </div>
        <?php } ?>
    <?php } else { ?>
        <p style="text-align: center">No orders yet!</p>
    <?php } ?>	
</div>