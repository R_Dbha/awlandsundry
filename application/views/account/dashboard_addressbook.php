<div class="dasboard-right-content address">
	<div class="dashboard-content-heading-wrapper">
		<div class="dashboard-content-heading right">
			<span><a href="#add_account_info/address_book"
				class="add-account-information">ADD</a></span>
		</div>
	</div>
	<div class="dashboard-content-heading-wrapper view-address">
		<div class="dashboard-content-heading">
			<h3 style="color: #262343; float: left;">Default Billing Address</h3>
			<h3 style="color: #262343; float: right;">Default Shipping Address</h3>
		</div>
	</div>
	<div id="view-address-inner">
        <?php if($address_list != NULL){ foreach($address_list as $a){ ?>
       
            
               <?php
										
										if ($a ['is_billing'] == "1") { // var_dump($a);
											?>
                 <div class="view-contact-contents left">

			<p><?php echo $a['first_name'].' '.$a['last_name'] ?></p>
			<p><?php echo $a['address1'] ?></p>
			<p><?php echo $a['address2'] ?></p>
			<p><?php echo $a['city'].', '. $a['state'].', '.$a['zipcode'] ?></p>
			<p><?php echo $a['country'] ?></p>
			<p>T: <?php echo $a['phone'] ?></p>
			<div class="dashboard-content-heading">
				<span> <a id="<?php echo $a['address_details_id'] ?>"
					href="#add_account_info/address_book/<?php echo $a['address_details_id'] ?>">
						EDIT </a>
				</span> | <span> <a
					href="javascript:deleteAccount('<?php echo $a['address_details_id'] ?>','billing');">
						DELETE </a>
				</span>
			</div>
		</div>
            <?php } ?>
        
		
            <?php
										
										if ($a ['is_shipping'] == "1") { // var_dump($a);
											?>
              <div class="view-contact-contents right">

			<p><?php echo $a['first_name'].' '.$a['last_name'] ?></p>
			<p><?php echo $a['address1'] ?></p>
			<p><?php echo $a['address2'] ?></p>
			<p><?php echo $a['city'].', '. $a['state'].', '.$a['zipcode'] ?></p>
			<p><?php echo $a['country'] ?></p>
			<p>T: <?php echo $a['phone'] ?></p>
			<div class="dashboard-content-heading">
				<span> <a id="<?php echo $a['address_details_id'] ?>"
					href="#add_account_info/address_book/<?php echo $a['address_details_id'] ?>">
						EDIT </a></span>|<span><a
					href="javascript:deleteAccount('<?php echo $a['address_details_id'] ?>','shipping');">
						DELETE</a></span>
			</div>
		</div>
            <?php } ?>			
        
        <?php }}?>
    </div>
	<div class="dashboard-content-heading-wrapper view-address"
		style="clear: both;">
		<div class="dashboard-content-heading left">
			<h3 style="color: black;">Additional Address Entries</h3>
		</div>
	</div>
	<div class="additional-contact-wrap">    
    <?php if($address_list != NULL){foreach($address_list as $a){ ?>            
               <?php if($a['is_billing'] != "1" && $a['is_shipping'] != "1"){?>
                
    <div class="view-contact-contents left">
			<p><?php echo $a['first_name'].' '.$a['last_name'] ?></p>
			<p><?php echo $a['address1'] ?></p>
			<p><?php echo $a['address2'] ?></p>
			<p><?php echo $a['city'].', '. $a['state'].', '.$a['zipcode'] ?></p>
			<p><?php echo $a['country'] ?></p>
			<p>T: <?php echo $a['phone'] ?></p>
			<div class="dashboard-content-heading">
				<span><a id="<?php echo $a['address_details_id'] ?>"
					href="#add_account_info/address_book/<?php echo $a['address_details_id'] ?>">
						EDIT </a></span>|<span><a
					href="javascript:deleteAccount('<?php echo $a['address_details_id'] ?>','additional');">
						DELETE</a></span>
			</div>
		</div>
    
    <?php
						}
					}
				}
				?>
    </div>
</div>
