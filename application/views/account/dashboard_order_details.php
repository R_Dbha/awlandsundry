<div id="order_history_view" class="order_details" >
    <div class="dasboard-right-content address">
        <div class="dashboard-content-heading-wrapper">
            <div class="dashboard-content-heading left">
                <?php $shipping = $order['shipping'][0]; ?>
                <?php $billing = $order['billing'][0]; ?>
                <?php $orderstatus = $order['orderstatus'][0]; ?>
                <h3><?php echo date('y/m/d', strtotime($orderstatus['order_date'])); ?> - <?php echo $orderstatus['status']; ?></h3>
            </div>
        </div>
        <div class="order_history_view_container">
            <div class="left order_history_view_address">
                <h3>Shipping Address</h3>
                <div>
                    <p><?php echo $shipping['firstname'] . ' ' . $shipping['lastname'] ?></p>
                    <p><?php echo $shipping['address1'] ?></p>
                    <p><?php echo $shipping['address2'] ?></p>
                    <p><?php echo $shipping['city'] . ', ' . $shipping['state'] . ', ' . $shipping['zipcode']; ?></p>
                    <p><?php echo $shipping['state']; ?></p>
                    <p>T: <?php echo $shipping['telephone']; ?></p>
                </div>
            </div>
            <div class="right order_history_view_address">
                <h3>Billing Address</h3>
                <div>
                    <p>
                        <?php echo ($billing['first_name'] != '' && $billing['first_name'] != '0') ? $billing['first_name'] : ''; ?>
                        <?php echo ($billing['last_name'] != '' && $billing['last_name'] != '0') ? ' ' . $billing['last_name'] : ''; ?>
                    </p>
                    <p>
                        <?php echo ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') ? $billing['address1'] : ''; ?>
                    </p>
                    <p>
                        <?php echo ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') ? $billing['address2'] : ''; ?>
                    </p>
                    <p>
                        <?php echo ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') ? $billing['city'] . ',' : ''; ?>
                        <?php echo ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') ? $billing['state'] . ',' : ''; ?>
                        <?php echo ($billing['address1'] != '' && $billing['address1'] != '0' || $billing['state'] != '' && $billing['state'] != '0') ? $billing['zipcode'] : ''; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="order_history_view_container">
            <?php if ($orderstatus['status'] == 'order shipped') { ?>
                <?php $status_desc = explode(",", $orderstatus['status_desc']); ?>
                <div class="left order_history_view_address">
                    <h3>Shipping Method  | <span>Track order</span></h3>
                    <div><p><?php echo $status_desc[1]; ?></p><p><?php echo $status_desc[2]; ?> </p></div>
                </div>
            <?php } else { ?>
                <div class="right order_history_view_address"></div>
            <?php } ?>
        </div>
        <div class="order_history_view_container">
            <table id="order_details_table" width="100%">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $totalamt = 0; ?>
                    <?php $subtotal = 0; ?>
                    <?php $disc = 0; ?>
                    <?php foreach ($order['order']['items'] as $item) { ?>
                        <tr>
                            <td>
                                <div style="display: none"><?php print_r($item); ?></div>
                                <?php if (isset($item['shop_images']) && is_array($item['shop_images'])) { ?>
                                    <img style="width: 180px;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/' . $item['shop_images'][0]['file_name']; ?>" />
                                <?php } else { ?>
                                    <img style="width: 180px;" src="<?php echo CustomShoeConfig::FILE_BASE . 'designs/' . $item['image_file'] . "_A0.png"; ?>" > 
                                <?php } ?>
                                <?php if (isset($item['soleName']) && isset($item['soleColorName'])) { ?><span class="tleft">Sole : <?php echo $item['soleName'] . ' / ' . $item['soleColorName']; ?></span><?php } ?>
                                <?php if (isset($item['soleWelt'])) { ?><span class="tleft">Welt : <?php echo $item['soleWelt']; ?></span><?php } ?>
                                <?php if (isset($item['shoetree'])) { ?><span class="tleft">Shoetree : <?php echo ($item['shoetree'] == 'true') ? 'Yes' : 'No'; ?></span><?php } ?>
                                <?php if ($item['monogram_text'] != NULL) { ?><span class="tleft">Monogram : <?php echo $item['monogram_text']; ?></span><?php } ?>    
                                <?php /* if ($item['stitch_name'] != NULL) { ?><span class="tleft">Stitching : <?php echo $item['stitch_name']; ?></span> <?php } */ ?>
                                <?php if ($item['is_lace_enabled'] != 0) { ?>
                                    <?php if ($item['lace_name'] != NULL) { ?>
                                        <span class="tleft" >Lacing : <?php echo $item['lace_name']; ?></span>
                                    <?php } ?>
                                <?php } ?>
                                <span class="tleft">Shoe size : left - <?php echo $item['left_shoe']; ?>, right - <?php echo $item['right_shoe']; ?></span>
                                <?php if ($item['left_width'] != NULL) { ?> <span class="tleft">Shoe width: left - <?php echo $item['left_width'] ?>, right - <?php echo $item['right_width'] ?></span><?php } ?>                           
                            </td>
                            <td>$<?php echo $item['item_amt'] ?></td>
                            <td><span><?php echo $item['quantity'] ?></span></td>
                            <td>$<?php echo $item['item_amt'] ?></td>
                        </tr>
                        <?php
                        if (strtolower($item['item_name']) != 'gift card') {
                            $subtotal += $item['item_amt'];
                            $discountPerc = $item['discount_perc'];
                            if ($discountPerc !== 0) {
                                $disc = ($subtotal * $discountPerc) / 100;
                                $totalamt = $subtotal - $disc;
                            }
                        }
                    }
                    ?>
                    <?php foreach ($order['order']['ready_wear'] as $item) { ?>
                        <tr>
                            <td>
                                <?php if (isset($item['shop_images']) && is_array($item['shop_images'])) { ?>
                                    <img style="width: 180px;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/' . $item['shop_images'][0]['file_name']; ?>" />
                                <?php } else { ?>
                                    <img style="width: 180px;" src="<?php echo CustomShoeConfig::FILE_BASE . 'designs/' . $item['image_file'] . "_A0.png"; ?>" > 
                                <?php } ?>
                                <span class="tleft">Shoe size : left - <?php echo $item['left_shoe']; ?>, right - <?php echo $item['right_shoe']; ?></span>
                                <?php if ($item['left_width'] != NULL) { ?> <span class="tleft">Shoe width: left - <?php echo $item['left_width'] ?>, right - <?php echo $item['right_width'] ?></span><?php } ?>                           

                            </td>
                            <td>$<?php echo $item['item_amt'] ?></td>
                            <td><span><?php echo $item['quantity'] ?></span></td>
                            <td>$<?php echo $item['item_amt'] ?></td>
                        </tr>

                        <?php
                        $subtotal += $item['item_amt'];
                        $discountPerc = $item['discount_perc'];
                        if ($discountPerc !== 0) {
                            $disc = ($subtotal * $discountPerc) / 100;
                            $totalamt = $subtotal - $disc;
                        }
                    }
                    ?>
                    <tr class="total_amt">
                        <td></td>
                        <td colspan="2">
                            <span>Subtotal</span>
                            <span>Shipping & Handling</span>
                            <span>Tax</span>
                            <span>Promo Code</span>
                            <span class="gtotal">Grand Total</span>
                        </td>
                        <td>
                            <span>$<?php echo number_format(round($subtotal), 2); ?></span>
                            <span>$0.00</span>
                            <span>$<?php echo '0.00' ?></span>
                            <span>$<?php echo number_format(round($disc), 2); ?></span>
                            <span class="gtotal">$<?php echo number_format(round($totalamt), 2); ?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <a href="#order_history" class="left">&#171; Back to my orders</a>
        </div>
    </div>
</div>