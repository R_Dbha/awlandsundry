<?php
$this->load->view('includes/header');
?>
<div id="get-inspired-menu-wrapper">
    <div id="menu" class="container get_inspired">
        <ul id="menuitem">
            <?php
//            
            ?>
        </ul>
    </div>
</div>
<div id="get-inspired-social-wrapper">
    <div id="get-inspired-social">
        <span></span>
        <a target="_blank" class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo base_url() . 'common/shoe_design/' . $item['id']; ?>"></a>
        <a target="_blank" class="googleplus" href="https://plus.google.com/share?url=<?php echo base_url() . 'common/shoe_design/' . $item['id']; ?>"></a> 
        <a target="_blank" class="twitter" href="http://twitter.com/share?url=<?php echo base_url() . 'common/shoe_design/' . $item['id'] . '&text=Checkout this custom pair of shoes I just designed at Awlandsundry.com'; ?>" ></a>
        <a target="_blank" class="pinterest" href="http://pinterest.com/pin/create/button/?url=<?php echo base_url() . 'common/shoe_design/' . $item['id']; ?>&media=<?php echo $images[0]; ?>&description=Checkout this custom pair of shoes I just designed at Awlandsundry.com"></a>
        <a target="_blank" class="tumblr" href="https://www.tumblr.com/share/photo?source=<?php echo urlencode($images[0]); ?>&caption=<?php echo urlencode('Checkout this custom pair of shoes I just designed at Awlandsundry.com'); ?>&clickthru=<?php echo urlencode(base_url() . 'common/shoe_design/' . $item['id']); ?>"></a>
        <a target="_blank" class="email"></a>

    </div>
</div>
<div id="get_inspired-wrapper">
    <div id="get_inspired" class="container">
        <div id="get_inspired_slider">
            <ul class="get_inspired_slider">
                <?php
                if (is_array($images)) {
                    foreach ($images as $key => $value) {
                        ?>
                        <li><img src="<?php echo $value ?>" /></li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
        <div class="get_inspired_product_details">
            <h3 class="product-title"><?php echo @$item['shoe_name']; ?></h3>
            <h4 class="product-style"><?php echo @$item['last_name'] . '/' . $item['style_name']; ?></h4>
            <h5 class="product-price">$<?php echo @$item['price']; ?></h5>
        </div>
        <div class="get_inspired_product_details shoesize">
            <div class="sizes">
                <div id="select_size">
                    <label>SELECT SIZE (US)</label>
                    <select name="select_size" id="select_left_size" tabindex="1" class="details_sizes">
                        <option value="">L</option>
                        <option value="5">5 </option>
                        <option value="5.5">5.5 </option>
                        <option value="6">6 </option>
                        <option value="6.5">6.5 </option>
                        <option value="7">7 </option>
                        <option value="7.5">7.5 </option>
                        <option value="8">8 </option>
                        <option value="8.5">8.5 </option>
                        <option value="9">9 </option>
                        <option value="9.5">9.5 </option>
                        <option value="10">10 </option>
                        <option value="10.5">10.5 </option>
                        <option value="11">11 </option>
                        <option value="11.5">11.5 </option>
                        <option value="12">12 </option>
                        <option value="12.5">12.5 </option>
                        <option value="13">13 </option>
                        <option value="13.5">13.5 </option>
                        <option value="14">14 </option>
                    </select>
                    <select name="select_size" id="select_right_size" tabindex="1"  class="details_sizes">
                        <option value="">R</option>
                        <option value="5">5 </option>
                        <option value="5.5">5.5 </option>
                        <option value="6">6 </option>
                        <option value="6.5">6.5 </option>
                        <option value="7">7 </option>
                        <option value="7.5">7.5 </option>
                        <option value="8">8 </option>
                        <option value="8.5">8.5 </option>
                        <option value="9">9 </option>
                        <option value="9.5">9.5 </option>
                        <option value="10">10 </option>
                        <option value="10.5">10.5 </option>
                        <option value="11">11 </option>
                        <option value="11.5">11.5 </option>
                        <option value="12">12 </option>
                        <option value="12.5">12.5 </option>
                        <option value="13">13 </option>
                        <option value="13.5">13.5 </option>
                        <option value="14">14 </option>
                    </select>
                    <span id="s_s_det" class="help_button">?</span>
                </div>
                <div id="select_width">
                    <label>SELECT WIDTH (US)</label>
                    <select  id="left_width"  class="details_sizes">
                        <option value="">L</option>
                        <option value="D">D (Normal)</option>
                        <option value="E">E (Wide)</option>
                        <option value="EE">EE (Extra Wide)</option>
                        <option value="EEE">EEE (Extra Extra Wide)</option>
                    </select>
                    <select  id="right_width"  class="details_sizes">
                        <option value="">R</option>
                        <option value="D">D (Normal)</option>
                        <option value="E">E (Wide)</option>
                        <option value="EE">EE (Extra Wide)</option>
                        <option value="EEE">EEE (Extra Extra Wide)</option>
                    </select>
                    <span id="s_w_det" class="help_button">?</span>
                </div>
            </div>
            <a class="flat_button " id="edit-shoedesign" >Customize this shoe</a>
            <a class="flat_button" id="add-to-cart" >Add to Cart</a>
            <input type="hidden" id="shoe_design_id" value="<?php echo $item['id']; ?>">
        </div>



    </div>
    <!--div id="product-description" class="innergrid center">
        <h2 class="product-title-large"><?php echo @$item['shoe_name']; ?></h2>
        <h4 class="product-style"><?php echo @$item['last_name'] . '/' . $item['style_name']; ?></h4>
        <p><?php echo @$item['description']; ?></p>
    </div-->

</div>
<div id="bottom-slider-wrapper">
    <h3 class="product-title center">You might like these</h3>
    <div id="bottom_slider" style="background:url(<?php echo base_url(); ?>css/images/bottom_pattern.jpg)">
        <ul class="bottom_slider">
            <?php
                foreach ($relatedshoes as $related) {
                    ?>
                    <li>
                        <a href="<?php echo base_url() . 'create-a-custom-shoe/related/' . $related['shoe_design_id']; ?>" onclick="return CustomShoe.confirmChangeShoe();">
                            <img src="<?php echo CustomShoeConfig::DESIGN_BASE . $related['image_file'] . CustomShoeConfig::RELATED_DEF_ANGLE . CustomShoeConfig::EXTENTION; ?>"/>
                        </a>
                    </li>
                    <?php
                }
                ?>
        </ul>
    </div>
</div>
</div>
<div class="overlay"></div>
<div id="sizing-popup-wrapper" class="popup-wrapper">
    <div id="size-info">
        <span class="close"></span>
        <h1>Sizing Chart</h1>
        <p>Awl & Sundry shoes are true to fit. To get the right fit, please order the most frequently worn size.  For instance, if you wear a 10 for most of your shoes, we would recommend ordering a 10 with Awl & Sundry as well. 
            If you are unsure of your size, you can trace your foot on a blank piece of paper and send it to us at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a> and we will recommend you your size and last.</p>
        <table>
            <tr><th>US</th><th>CAN</th><th>UK</th><th>AUS</th></tr>
            <tr><td>5</td><td>5</td><td>4</td><td>4</td></tr>
            <tr><td>5&#189;</td><td>5&#189;</td><td>4&#189;</td><td>4&#189;</td></tr>
            <tr><td>6</td><td>6</td><td>5</td><td>5</td></tr>
            <tr><td>6&#189;</td><td>6&#189;</td><td>5&#189;</td><td>5&#189;</td></tr>
            <tr><td>7</td><td>7</td><td>6</td><td>6</td></tr>
            <tr><td>7&#189;</td><td>7&#189;</td><td>6&#189;</td><td>6&#189;</td></tr>
            <tr><td>8</td><td>8</td><td>7</td><td>7</td></tr>
            <tr><td>8&#189;</td><td>8&#189;</td><td>7&#189;</td><td>7&#189;</td></tr>
            <tr><td>9</td><td>9</td><td>8</td><td>8</td></tr>
            <tr><td>9&#189;</td><td>9&#189;</td><td>8&#189;</td><td>8&#189;</td></tr>
            <tr><td>10</td><td>10</td><td>9</td><td>9</td></tr>
            <tr><td>10&#189;</td><td>10&#189;</td><td>9&#189;</td><td>9&#189;</td></tr>
            <tr><td>11</td><td>11</td><td>10</td><td>10</td></tr>
            <tr><td>11&#189;</td><td>11&#189;</td><td>10&#189;</td><td>10&#189;</td></tr>
            <tr><td>12</td><td>12</td><td>11</td><td>11</td></tr>
            <tr><td>12&#189;</td><td>12&#189;</td><td>11&#189;</td><td>11&#189;</td></tr>
            <tr><td>13</td><td>13</td><td>12</td><td>12</td></tr>
            <tr><td>13&#189;</td><td>13&#189;</td><td>12&#189;</td><td>12&#189;</td></tr>
            <tr><td>14</td><td>14</td><td>13</td><td>13</td></tr>
        </table>

    </div>
</div>
<div id="width-popup-wrapper" class="popup-wrapper">
    <div id="width-info">
        <span class="close"></span>
        <h1>Width Chart</h1>
        <p>
            Awl & Sundry shoes are defaulted to width D. If you have a wider foot, we would strongly recommend tracing your foot on a blank piece of paper and send it to us at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a> and we will recommend you your width and last.
        </p>
        <table>
            <tr>
                <td>D</td>
                <td>Normal</td>
            </tr>
            <tr>
                <td>E</td>
                <td>Wide</td>
            </tr>
            <tr>
                <td>EE</td>
                <td>Extra Wide</td>
            </tr>
            <tr>
                <td>EEE</td>
                <td>Extra Extra Wide</td>
            </tr>
        </table>


    </div>
</div>
<div id="email-popup-wrapper" class="popup-wrapper" >
    <div id="email-info">
        <span class="close"></span>
        <h1>Email This Design</h1>
        <span id="errors"></span>
        <div class="content">
            <div class="half left">
                <div class="content">
                    <img src="<?php echo $images[0]; ?>" />
                </div>
            </div>
            <div class="half left">
                <div class="content">
                    <form action="">

                        <table>
                            <tr>
                                <td>
                                    <label>From</label>

                                    <input type="email" class="from_email" required placeholder="Your Email" />
                                    <span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>To</label>

                                    <input type="text" class="to_emails" required  placeholder="Your friends' Emails (Seperated by commas)"/>
                                    <span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Message</label>

                                    <textarea name="message" class="message" placeholder="Your Message (320 Characters limit)"></textarea>
                                    <span></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="button" value="Send it" id="send-email"/>
                                    <input type="hidden" value="<?php echo $item['id']; ?>" class="shoe_id"/>
                                    <input type="hidden" value="<?php echo $item['id']; ?>" class="collection"/>
                                    <input type="hidden" value="<?php echo $images[0]; ?>" class="image"/>
                                </td>
                            </tr>
                        </table>

                    </form>
                </div>

            </div>
            <div id="share_email_success" class="none">Message sent successfully</div>
        </div>
    </div>
</div>
<?php
$this->load->view('includes/footer');
?>
     
