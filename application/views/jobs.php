<?php
require_once('includes/header.php');
?>
<style type="text/css">
    @font-face {
        font-family: 'bebas_neueregular';
        src: url('assets/css/fonts/bebas/BebasNeue-webfont.eot');
        src: url('assets/css/fonts/bebas/BebasNeue-webfont.eot?#iefix') format('embedded-opentype'),
            url('assets/css/fonts/bebas/BebasNeue-webfont.woff2') format('woff2'),
            url('assets/css/fonts/bebas/BebasNeue-webfont.woff') format('woff'),
            url('assets/css/fonts/bebas/BebasNeue-webfont.ttf') format('truetype'),
            url('assets/css/fonts/bebas/BebasNeue-webfont.svg#bebas_neueregular') format('svg');
        font-weight: normal;
        font-style: normal;

    }  
</style>

<div id="jobs-wrapper">
    <div class="container">
        <div id="jobs-content">
            <div id="jobs_landing">
                <h2>Our team is growing- Join us!</h2>
                <span class="summary head">Does your work ethic, passion, and sense of humor belong at Awlandsundry?</span>
                <div id="position_wrapper">
                    <div class="position"><a id="jobs_marketing">
                            <img src="<?php echo base_url(); ?>assets/css/images/1.png" alt="" /></a>
                        <p><strong>VP of Marketing</strong><br><span class="summary"></span></p>
                    </div>   
                </div>
            </div>
            <div id="jobs_description">
                <h2>VP of Marketing</h2>
                <h4>About Awl &amp; Sundry:</h4>
                <p>Awl & Sundry provides high quality customized men's shoes at a revolutionary price point. Our mission is to 

                    bridge the growing divide between elite luxury and modest furnishing, as well as fine shoes and the men who 

                    love them.  We offer handmade shoes that are truly durable and personalized within a price range the 

                    everyday shoe hound can afford. </p>
                <h4>Vice President of Marketing:</h4>
                <p>Awl & Sundry is seeking an analytical, entrepreneurial VP of Marketing to grow new customer acquisition and

                    drive buyer retention. This person will play a key role in directing the Awl & Sundry business. Are you a 

                    strategic thinker who is a pro at tracking and measuring sales performance? Do you love men’s fashion and 

                    high-growth startups? If so, this position may be for you. </p>

                <h4>Responsibilities: </h4>
                <p>Reporting directly to the Founder and CEO, the VP of Marketing will be responsible for the creative ideation, 

                    execution and tracking of all Awl & Sundry marketing campaigns. This includes:
                </p>
                <ul>
                    <li>Spearheading marketing campaigns using various channels, including: AdWords, Facebook, display,  mail marketing, affiliate advertising, cross promotions, partnerships.</li>
                    <li>Executing, testing and measuring various marketing methods to widen customer reach</li>
                    <li>Overseeing all PR and social media strategies</li>
                    <li>Maintaining and establishing new customer relationships aligned with company sales objectives</li>
                    <li>Carefully crafting PR and social media strategies to engage a set of diverse audiences</li>
                    <li>Cultivating relationships with cultural influencers and brand ambassadors </li>
                </ul>

                <h4>About you: </h4>
                <p>You are a marketer by trade with a genuine interest in men’s fashion and the startup space. You have 

                    experience with online retail, e-commerce and global branding. You can design and execute a well-rounded, 

                    multi-channel marketing campaign with your eyes closed. You are data-driven, focused and committed. </p>
                <h4>Aside from these characteristics you have:</h4>
                <p></p>
                <ul>
                    <li>Significant experience in implementing numerous marketing strategies </li>
                    <li>Worked in a similar role at another online retailer, e-commerce company or agency </li>
                    <li>Excellent communication, leadership, organizational, and presentation skills</li>
                    <li>Strong conceptual planning and creative design skills</li>
                    <li>Ability to multi-task and shift priorities </li>
                    <li>Strong people and negotiation skills</li>
                </ul>

                <h4>What we offer:</h4>
                <p></p>
                <ul>
                    <li>Opportunity to get in on the ground of an early stage company </li>
                    <li>Competitive salary, sizeable equity and benefits</li>
                    <li>Chance to build personal portfolio and brand</li>
                </ul>
                <p>
                    Interested? Please send your CV and cover letter to: <a href="maito:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a>
                </p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    WebFontConfig = {
        google: {families: ['Open+Sans:400,600,700,300:latin']}
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();</script>
<?php
require_once('includes/footer.php');
?>

