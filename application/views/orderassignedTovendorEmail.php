<html>
    <head>
        <title>Awl & Sundry Order No.  <?php echo $order_summary[0]['invoice_no'] ?></title>
    </head>
    <body style = "margin: 0px; font-family: gibsonregular, arial, sans-serif; color: #414141; margin: 0px ;">
        <div id="header_wrapper" style = "background: url(<?php echo base_url() . 'assets/img/logo_inline.png' ?>) center 40px repeat-x; overflow: hidden;">
            <div id="header" style = "height: 140px; width: 960px; margin: 0 auto; overflow: hidden;">
                <h1 id = "logo" style = "text-align: center; position: absolute; margin: 0; top: 15px; left: 40%; font-size: 2em;">
                    <a href="<?php echo base_url(); ?>" style = "text-indent: -9999px; background: url(<?php echo base_url() . 'assets/img/logo.png' ?>) center bottom no-repeat; width:255px; height: 97px; display: inline-block; position: relative; z-index: 9999; margin-left: 9px; margin-top: 1px; overflow: hidden;"></a>
                </h1>
            </div>
        </div>
        <div id="content_wrapper" styl = "width: 100%; overflow: hidden; overflow: hidden;">
            <div id="content" style = "width: 960px; margin: 0 auto;">
                <p style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; color: #414141;">Dear <?php echo $vendor[0]['first_name']; ?>  </p>
                <div id="messsge_wrap" style = "margin: 45px 0; overflow: hidden;">
                    <p style = "font-size: 20px; margin: 15px 0;">We are currently assigned you order.</p>
                    <br/>
                    <p style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; margin: 15px 0;">ORDER SUMMARY</p>
                    <p style = "font-size: 20px; margin: 15px 0;"><span style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px;">Order Date:</span><?php echo date("m/d/Y h:ia", strtotime($order_summary[0]['order_modified'])) ?></p>
                    <p style = "font-size: 20px; margin: 15px 0;"><span style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px;">Order Number:</span><?php echo $order_summary[0]['invoice_no'] ?></p>
                    <br/>
                    <div id="address_table" style ="overflow: hidden;">
                        <table style = "width: 100%;" border-collapse: collapse; border-spacing: 0;>
                               <tbody style = "display: table-row-group; vertical-align: middle; border-color: inherit;">	
                                <tr style = "display: table-row; vertical-align: inherit; border-color: inherit;">
                                    <th style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; text-align: left; padding: 5px 0;">Billing to:</td>
                                    <th style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; text-align: left; padding: 5px 0;">Shipping to:</td>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;"><?php echo $billing[0]['first_name']; ?></td>
                                    <td style = "font-size: 20px;"><?php echo $shipping[0]['firstname'] ?></td>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;"><?php echo $billing[0]['address1'] ?></td>
                                    <td style = "font-size: 20px;"><?php echo $shipping[0]['address1'] ?></td>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;"><?php echo $billing[0]['address2'] . ',' . $billing[0]['city'] . '-' . $billing[0]['zipcode']; ?></td>
                                    <td style = "font-size: 20px;"><?php echo $shipping[0]['address2'] . ',' . $shipping[0]['city'] . '-' . $shipping[0]['zipcode']; ?></td>
                                </tr>
                            </tbody>	
                        </table>
                    </div>
                    <div id="order_table" style = "overflow: hidden;">
                        <table id="order_list"  style = " border-collapse: collapse; border-spacing: 0; display: table; border-color: gray; width: 100%; margin-top: 30px; font-weight: bold; font-family: gibsonsemibold, arial, sans-serif;">
                            <thead style = "display: table-header-group; vertical-align: middle; border-color: inherit;">
                                <tr>
                                    <th style="width:65%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">DESCRIPTION</th>
                                    <th style="width:27%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">QUANTITY</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total = 0; ?>
                                <?php foreach ($order['items'] as $item) { ?>
                                    <tr>
                                        <td style = "border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">
                                            <?php echo $item['item_name']; ?>
                                            <img style="width: 100%;" src="<?php echo (isset($item['shop_image_file'])) ? ApplicationConfig::ROOT_BASE . 'files/' . $item['shop_image_file']['file_name'] : ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A0.png"; ?>" />
                                        </td>
                                        <td style="text-align:center; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">1 PAIR</td>
                                    </tr>
                                <?php } ?>
                                <tr style="border:1px solid #000; height: 40px;"></tr>
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    </div>
                </div>
            </div>	
        </div>	
        <div id="footer_wrapper" style = "background: url(<?php echo base_url() . 'assets/img/foo_bg.png' ?>) repeat-x; width: 100%; overflow: hidden;">
            <div  id="footer" style = "background: url(<?php echo base_url() . 'assets/img/foo_bg.png' ?>) repeat-x; height: auto; min-height: 100px; padding: 30px 0; width: 960px; margin: 0 auto; overflow: hidden;">
                <div id="left" style = "float: left; overflow: hidden;">
                    <p style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; margin: 15px 0;">CUSTOMER SERVICE</p>
                    <P style = "font-size: 20px; margin: 15px 0;"><?php echo $this->config->item('contact_email'); ?></P>
                    <p style = "font-size: 20px; margin: 15px 0;"><?php echo $this->config->item('customer_care_number'); ?></p>
                </div>
                <div id="right" style = "float: right; overflow: hidden;">
                    <p style = "font-weight: bold; font-family: "gibsonsemibold", arial, sans-serif; font-size: 21px;">CONNECT WITH US</p>
                    <p style = "font-size: 20px; margin: 15px 0;">
                        <a href="#" id="facebook"></a>
                        <a href="#" id="twitter"></a>
                        <a href="#" id="pinterest"></a>
                        <a href="#" id="photo"></a>
                        <a href="#" id="bee"></a>
                    </p>
                </div>
                <div id="copyright" style = "text-align: center; clear: both; overflow: hidden;">&copy; Awl & Sundry. All Rights Reserved. 2012 - 2014</div>
            </div>
        </div>		
    </body>
</htm>