<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <?php
    $this->load->view('global/csscontainer');
    ?>
    <body class="<?php echo $body_class;?>">
        <div id="preloader"><div id="status">&nbsp;</div></div>
        <?php
        $headtemplate = "customshoe/template/header";
        if(isset($header)) {
            $headtemplate = $header;
        }
        $this->load->view($headtemplate);
        ?>
        <section id="middle_container">    
            <?php
                $this->load->view($maincontent);
            ?>
        </section>
        <?php
        $this->load->view('global/jscontainer');
        ?>
    </body>
</html>