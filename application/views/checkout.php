<?php $this->load->view('includes/header'); ?>

<div id="checkout-wrapper">
    <div id="checkout" class="container">
        <form action="" name="checkout"  id="checkout-form" method="POST">
            <div id="shipping-address" class="checkout-container left">
                <?php $this->load->view('shipping_details', @$shipping); ?>
            </div>
            <div id="billing-address" class="checkout-container left ">
                <?php $this->load->view('billing_details', @$billing); ?>
            </div>	
            <div id="review" class="checkout-container left">
                <div class="checkout-overlay <?php echo @$review_overlay; ?>" ></div>
                <div class="form" >
                    <h3 class="page-title"><span>REVIEW</span></h3>
                    <div id="item-total">
                        <label>Subtotal (<?php echo @$number_of_items; ?> items)</label>
                        <label id="sub_total">$<?php echo @$sub_total; ?></label>
                        <label>Tax</label>
                        <label id="tax_total">$<?php echo @$tax; ?></label>
                        <label>Shipping</label>
                        <label id="shipping_cost">$<?php echo @$shipping_cost; ?></label>
                    </div>
                    <div id="item-total-amt">
                        <label>Total</label>
                        <label id="net_total">$<?php echo @$net_total; ?></label>
                    </div>
                    <div id="comment_wrap">
                        <p>Add your comments here </p>
                        <textarea name="comment" id="txtComment"></textarea>
                    </div>
                    <div class="form_submit">
                        <input type="submit" name="review_submit" value="CHECKOUT"/>
                    </div>
                </div>
            </div>	
        </form>
    </div>
</div>

<?php $this->load->view('includes/footer'); ?>
