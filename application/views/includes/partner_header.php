<?php
/**
 *
 * @author Ajith Vp <ajith@kraftlabs.com>
 * @copyright Copyright (c) 2015, Ajith Vp <ajith@kraftlabs.com>
 *            @date 28-Apr-2015
 */
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $this->config->item('app_title'); ?></title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="<?php echo base_url(); ?>assets/css/ionicons.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap-3.2.0.min.css"
	rel="stylesheet" type="text/css" />
<link
	href="<?php echo base_url(); ?>assets/css/datatables/dataTables.bootstrap.css"
	rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="<?php echo base_url(); ?>assets/css/AdminLTE.css"
	rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/partner.css"
	rel="stylesheet" type="text/css" />

<link href="<?php echo base_url(); ?>assets/css/admin/common.css"
	rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/css/fileuploader.css"
	rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
	<!-- header logo: style can be found in header.less -->
	<header class="header">

		<a href="<?php echo base_url(); ?>admin" class="logo"> <!-- Add the class icon to your logo image or logo icon to add the margining -->
               <?php if($partner_image !== ''){ ?><img
			src="<?php echo base_url().$partner_image; ?>"
			class="" alt="User Image" /> <?php } ?><span><?php echo $this->config->item('app_title'); ?></span>
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top" role="navigation">
			<!-- Sidebar toggle button-->
			<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas"
				role="button"> <span class="sr-only">Toggle navigation</span> <span
				class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
			</a>
			<div class="navbar-right">
				<ul class="nav navbar-nav">
					<!-- Messages: style can be found in dropdown.less-->
					<li class="dropdown user user-menu"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <i
							class="glyphicon glyphicon-user"></i> <span><?php echo @$user['first_name'];  ?><i
								class="caret"></i></span>
					</a>
						<ul class="dropdown-menu">
							<!-- User image -->

							<!-- Menu Body -->

							<!-- Menu Footer-->
							<li class="user-footer">

								<div class="pull-right">
									<a href="<?php echo base_url(); ?>partners/logout"
										class="btn btn-default btn-flat">Sign out</a>
								</div>
							</li>
						</ul></li>
				</ul>
			</div>
		</nav>
	</header>
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image"></div>
					<div class="pull-left info">
						<p>Hello, <?php echo @$user['first_name']; ?></p>
					</div>
				</div>
				<!-- search form -->

				<!-- /.search form -->
				<?php
				
if($this->router->class === 'partners'){
					$this->load->view('includes/partner_menu');
				}else{
					$this->load->view('includes/admin_menu');
				}
				?>
							</section>
			<!-- /.sidebar -->
		</aside>
		<div id="log"></div>

		<!-- Right side column. Contains the navbar and content of the page -->
		<aside id="right-content" class="right-side">