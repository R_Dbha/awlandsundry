
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li class="active"><a href="<?php echo base_url(); ?>admin/index">
            <i class="fa fa-dashboard"></i> 
            <span>Dashboard</span>
        </a>
    </li>
    <li ><a href="<?php echo base_url(); ?>orders/mto"> 
            <i class="fa fa-bar-chart-o"></i> 
            <span>Orders</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
    <li ><a href="<?php echo base_url(); ?>promocode"> 
            <i class="fa fa-bar-chart-o"></i> 
            <span>Promocodes</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
    <li ><a href="<?php echo base_url(); ?>giftcard/view"> 
            <i class="fa fa-bar-chart-o"></i> 
            <span>Gift Cards</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
    <li ><a href="<?php echo base_url(); ?>admin/users"> 
            <i class="fa fa-user"></i> 
            <span>Users</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
    <li ><a href="<?php echo base_url(); ?>admin/appointments"> 
            <i class="fa fa-bar-chart-o"></i> 
            <span>Appointments</span>
            <i class="fa fa-angle-left pull-right"></i>
        </a>
    </li>
    <li class="active"><a href="<?php echo base_url(); ?>admin/index">
            <i class="fa fa-dashboard"></i> 
            <span>Old Admin</span>
        </a>
    </li>
</ul>
