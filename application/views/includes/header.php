<!DOCTYPE html>
<html lang="en" <?php echo isset($props) ? $props : ''; ?>>
    <head>
        <title><?php echo (isset($page_title)) ? $page_title : 'Awl & Sundry'; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
        <?php echo isset($meta) ? $meta : ''; ?>
        <link href="https://plus.google.com/+Awlandsundrycustomshoes" Rel="Publisher" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/css/images/icons/favicon.jpg" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/stacktable.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slicknav.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.bxslider.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/supersized.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.min.css">
        <link href="https://fonts.googleapis.com/css?family=Buenard:400,700" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">

        <?php if (isset($additional_css)) { ?>
            <?php if (is_array($additional_css)) { ?>
                <?php foreach ($additional_css as $css) { ?>
                    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' . $css; ?>.css" > 
                <?php } ?>
            <?php } ?>
            <?php echo (!is_array($additional_css)) ? (($additional_css != '') ? '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/' . $additional_css . '.css" >' : '' ) : '' ?>
        <?php } ?>

        <script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
        </script>

        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-TTQHZ8');</script>
        <!-- End Google Tag Manager -->

        <!-- Facebook Pixel Code -->
        <script>
            var dataLayer = [];
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '314559972062391');
            fbq('track', 'PageView');
        </script>
        <!-- End Facebook Pixel Code -->

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o), m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-40200583-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- Begin Setster Widget -->
<!--        <script type="text/javascript" charset="utf-8">
            var is_ssl = ("https:" == document.location.protocol);
            var setsHost = is_ssl ? "https://www.setster.com/widget/" : "https://www.setster.com/widget/";
            document.write(unescape("%3Cscript src='<?php echo base_url(); ?>assets/js/setster_over.js' type='text/javascript'%3E%3C/script%3E"));
        </script>

        <script type="text/javascript" charset="utf-8">
            var setster_widget_options = {};

            setster_widget_options.display = "overlay";
            setster_widget_options.uri = "awlandsundry";
            setster_widget_options.placement = "left";
            setster_widget_options.buttonName = "Appointment";
            setster_widget_options.setsterURL = setsHost;
            var setster_widget = new Setster.setster_widget(setster_widget_options);
        </script>-->
        <!-- End Setster Widget -->
    </head>

    <body class="<?php echo isset($bodyclass) ? $bodyclass : ''; ?>" >
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TTQHZ8" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        <!-- Facebook Pixel (noscript) -->
        <noscript><img height="1" width="1" src="https://www.facebook.com/tr?id=314559972062391&ev=PageView&noscript=1"/></noscript>
        <!-- End Facebook Pixel (noscript) -->

        <style type="text/css">
            .secondary-links li .subMenu{
                padding: 10px 20px !important;
                margin-left: -60px !important;
                text-align: left !important;
            }
            .secondary-links li .subMenu li{
                white-space: nowrap !important;
            }
            .secondary-links li .subMenu li a{
                font-size: 0.875rem !important;
                line-height: 1.175rem !important;
                font-weight: 300 !important;
                text-transform: none !important;
                font-family: sans-serif !important;
            }
            .secondary-links li .subMenu li a:first-child{
                color: grey !important;
            }
            .secondary-links li .subMenuLogin{
                white-space: nowrap !important;
                margin-left: -135px !important;
            }
            .secondary-links li .subMenuLogin .login-inputs{
                text-transform: none;
                border: 1px solid #ccc;
                background-color: #ccc;
            }

            .secondary-links li .subMenuLogin .with-button{
                /*width: 200px;*/
            }
        </style>

        <div id="wrapper">
            <div id="header-wrapper" class="header-wrapper hide-appointment">
                <?php $hideHeader='true'; ?>
                <?php if (!isset($hideHeader)) { ?>
                    <div id="appointmentHeader">
                        <p><a href="<?php echo base_url(); ?>appointment">IN NYC? Schedule an Appointment to Visit Our Showroom</a></p>
                        <a href="javascript:;" class="closeAppointment">X</a>
                    </div>
                <?php } ?>

                <div id="header" class="container">
                    <div id="primary-menu">
                        <div class="left menu-left" id="menu-left">
                            <ul class="leftMenu">
                                <li class="main-link"><a class="shop">SHOP <span>|</span></a>
                                    <ul>
                                        <li class="main-link"><a href="<?php echo base_url(); ?>shop" class="cm-main-link" ><span class="container"><strong>Inspirations</strong> <img src="<?php echo base_url(); ?>/assets/css/images/custom-made-icon.png" width="17" height="21" /> HANDCRAFTED / FROM $495</span></a></li>
                                        <li class="main-link"><a href="<?php echo base_url(); ?>shop/the-blacklist-collection" class="ready-main-link" ><span class="container"><strong>The Blacklist Collection</strong> <img src="<?php echo base_url(); ?>/assets/css/images/wear-icon.png" width="16" height="21" /> HANDCRAFTED / FROM $375</span></a></li>
                                    </ul>
                                </li>	
                                <li class="main-link customMenu"><a href="<?php echo base_url(); ?>create-a-custom-shoe">Custom</a>
                                    <!-- <ul>
                                        <li class="main-link custom-main-link"><a href="<?php echo base_url(); ?>create-a-custom-shoe" class="" ><span class="container"><strong>CREATE AN ORIGINAL</strong> <img src="<?php echo base_url(); ?>/assets/css/images/original-icon.png" width="14" height="21" /> EXPRESS YOURSELF / FROM $495</span></a></li>
                                        <li class="main-link custom-main-link"><a href="<?php echo base_url(); ?>shop" class="ins-main-link" ><span class="container"><strong>GET INSPIRATION</strong> <img src="<?php echo base_url(); ?>/assets/css/images/inspiration-icon.png" width="20" height="21" /> VIEW THE CUSTOM CREATIONS OF OTHERS</span></a></li>
                                    </ul> -->
                                </li>
                                <li><a href="<?php echo base_url(); ?>appointment">FITTINGS</a></li>
                                <li id="mob_login"><a href="<?php echo base_url(); ?>login">LOGIN</a></li>
                            </ul>
                        </div>

                        <div class="logo"><a href="<?php echo base_url(); ?>"></a></div>

                        <div class="right menu-right">
                            <ul class="secondary-links">
                                <li><a href="<?php echo base_url(); ?>about-us">ABOUT US</a></li>

                                <?php $user = $this->session->userdata('user_details'); ?> 
                                <?php if ($user != FALSE) { ?> 
                                    <li id="divUser"><a  href="<?php echo base_url(); ?>my-account"><span>MY ACCOUNT</span></a>
                                        <div class="subMenu">
                                            <ul>
                                                <?php if ($user['user_type_id'] === '1') { ?>
                                                    <li><a href="<?php echo base_url(); ?>admin/index" class="custom">ADMIN </a></li>
                                                <?php } if ($user['user_type_id'] === '3') { ?>
                                                    <li><a href="<?php echo base_url(); ?>admin/vendor" class="custom">ADMIN </a></li>
                                                <?php } ?>

                                                <li><a href="javascript:;" class="custom" style="color : grey !important">Hi, <?php echo @$user['first_name']; ?>!</a></li>
                                                <li><a href="<?php echo base_url(); ?>my-account#account_info" class="custom">Account Information </a></li>
                                                <li><a href="<?php echo base_url(); ?>my-account#order_history" class="custom">Order History </a></li>
                                                <li><a href="<?php echo base_url(); ?>my-account#saved_designs" class="custom">Saved Designs </a></li>
                                                <li><a href="<?php echo base_url(); ?>my-account#gift_certificate" class="custom">Gift Certificate </a></li>
                                                <li><a href="<?php echo base_url(); ?>logout?t=<?php echo time(); ?>" class="custom">Logout </a></li>
                                            </ul>
                                        </div>
                                    </li>
                                <?php } else { ?>
                                    <!--<li id="replace"><a href="<?php echo base_url(); ?>login">LOGIN</a></li>-->
                                                                        <li id="replace"><a href="javascript:;">LOGIN</a>
                                                                                <div class="subMenu subMenuLogin" style="width: 299px;">
                                                                                    <div id="signin-form" style="text-align: center; height: auto">
                                                                                        <div>
                                                                                            <p>ALREADY HAVE AN ACCOUNT?</p>
                                                                                            <form  id="login_form" method="post" action="<?php echo base_url(); ?>login">
                                                                                                <p>
                                                                                                    <input autocomplete="off" value="" class="login-inputs" type="email" id="email" name="email" required placeholder="Email Address" />
                                                                                                </p>
                                                                                                <p>
                                                                                                    <input autocomplete="off" value="" class="login-inputs with-button" type="password" name="password" required placeholder="Password" />
                                                                                                </p>
                                                                                                <p>
                                                                                                    <input type="submit" name="login" value="LOGIN" />
                                                                                                </p>
                                                                                                <a href="<?php echo base_url(); ?>forgot-password" style="font-weight: 400; color: #3574c0 !important; font-size: 0.75rem; font-family: 'Gotham SSm A','Gotham A',sans-serif !important;">FORGOT PASSWORD?</a>
                                                                                                <p style="margin: 0">OR</p>
                                                                                                <p style="margin-top: 5px">
                                                                                                    <a href="<?php echo base_url(); ?>facebook/signup" id="fb_login" class="fbsignup"><img src="<?php echo base_url(); ?>assets/css/images/Facebook_Login.png"></a>
                                                                                                </p>
                                                                                                <p style="margin-top: 5px">
                                                                                                    <a href="<?php echo base_url(); ?>googlelogin/signup" class="fbsignup"><img src="<?php echo base_url(); ?>assets/css/images/Google_Login.png"></a>
                                                                                                </p>
                                                                                                <hr style="border-color: #ccc; border-style: solid; border-width: 1px 0 0; margin: 10px 0; height: 0;">
                                                                                                <a href="<?php echo base_url(); ?>signup" style="font-weight: 400; color: #3574c0 !important; font-size: 0.75rem; font-family: 'Gotham SSm A','Gotham A',sans-serif !important;">CREATE AN ACCOUNT</a>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                <?php } ?>
                                <li class="cart_desktop"><a href="<?php echo base_url(); ?>cart">CART(<?php echo sizeof($this->cart->contents()); ?>)</a></li>
                                <li class="cart_mobile"><a href="<?php echo base_url(); ?>cart">(<?php echo sizeof($this->cart->contents()); ?>)</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="slick-menu">
                        <div id="slick"></div>
                    </div>
                    <div id="mobile_menu_wrap">
                        <ul id="mobile_menu">
                            <li class="mobile-shop"><a href="" onclick="return false;">+ SHOP</a></li>
                            <li class="mobile-dropdown"><a href="<?php echo base_url(); ?>shop" class="" ><strong>INSPIRATIONS</strong> </a></li>
                            <li class="mobile-dropdown"><a href="<?php echo base_url(); ?>shop/the-blacklist-collection" class="" ><strong>THE BLACKLIST COLLECTION</strong></a></li>

                            <li><a href="<?php echo base_url(); ?>create-a-custom-shoe" >• DESIGN YOUR OWN PAIR</a></li>
                            <li><a href="<?php echo base_url(); ?>appointment">• FITTINGS</a></li>
<!--                            <li class="mobile-dropdown-1"><a href="<?php echo base_url(); ?>create-a-custom-shoe" class="" ><strong>CREATE AN ORIGINAL</strong> </a></li>
                            <li class="mobile-dropdown-1"><a href="<?php echo base_url(); ?>shop" class="" ><strong>GET INSPIRATION</strong></a></li>-->
                            <li><a href="<?php echo base_url(); ?>about-us">• OUR STORY</a></li>
                            <?php if ($user === FALSE) { ?>
                                <li id="mob_login1"><a href="<?php echo base_url(); ?>login">• LOGIN</a></li>
                            <?php } else { ?>
                                <li><a href="<?php echo base_url(); ?>my-account">• MY ACCOUNT</a></li>
                                <li id="mob_login"><a href="<?php echo base_url(); ?>logout">• LOGOUT</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="content-wrapper">
                <div class="portrait-support">
                    <img src="<?php echo base_url(); ?>/assets/css/images/portrait-rotation.png" alt="portrait-rotation">
                    <p>Use your phone in Landscape mode for best experience</p>
                </div>
                <div id="content">