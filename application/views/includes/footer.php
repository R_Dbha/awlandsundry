</div>
</div>

<div id="footer-menu">
    <div class="container">  
        <ul class="footer">
            <li><a href="https://medium.com/@AwlandSundry" target="_blank">BLOG</a></li>
            <li><a href="<?php echo base_url(); ?>faq#product">FAQ</a></li>
            <li><a href="<?php echo base_url(); ?>appointment">APPOINTMENT</a></li>
            <li><a href="<?php echo base_url(); ?>gift-card">GIFT CARD</a></li>
            <li><a href="<?php echo base_url(); ?>press">PRESS</a></li>
            <li><a href="<?php echo base_url(); ?>faq">CONTACT US</a></li>
            <li class="mob"><a href="<?php echo base_url(); ?>privacy-policy">PRIVACY POLICY</a></li>
            <li><a href="<?php echo base_url(); ?>terms-and-conditions">TERMS OF USE</a></li>
        </ul>
    </div>
</div>

<div id="footer-wrapper">
    <div id="footer" class="container">
        <div id="footer-logo" class="footer">
            <a href="<?php echo base_url(); ?>"></a>
        </div>
        <div id="footer-copyright">
            <span class="footer">&copy; AWL &AMP; SUNDRY.</span>
            <span class="footer">ALL RIGHTS RESERVED. 2012-<?php echo date('Y'); ?></span>
        </div>
    </div>
</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/build/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ddscrollspy.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.sticky.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.slicknav.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/prefixfree.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/afterbefore.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.min.js"></script>

<script>
    var baseUrl = '<?php echo base_url(); ?>';
</script>

<?php
if (isset($additional_js)) {
    if (is_array($additional_js)) {
        foreach ($additional_js as $js) {
            if (strpos($js, 'http') === FALSE) {
                $js = base_url() . 'assets/js/' . $js . '.js';
            }
            ?>
            <script type="text/javascript" src="<?php echo $js; ?>"></script>
            <?php
        }
    } elseif ($additional_js !== '') {
        if (strpos($additional_js, 'http') === FALSE) {
            $js = base_url() . 'assets/js/' . $additional_js . '.js';
        }
        ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $additional_js; ?>.js"></script>
    <?php } ?>
<?php } ?>

<script>
    $(function () {
        $('#mobile_menu').slicknav({prependTo: '#primary-menu'});
    });

    $(document).ready(function () {
        $("#slick").sticky({topSpacing: 50});
        $("#menu").sticky({topSpacing: 100});
        $("#header-wrapper").sticky({topSpacing: 0});
    });
</script>

<noscript >
<img height = "1" width = "1" alt = "" style = "display:none" src = "https://www.facebook.com/tr?ev=6017866457827&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" />
</noscript>

<?php if ($this->router->method === 'order_confirmation' && $this->router->class === 'common') { ?>
    <div style="display:inline;"> 
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/966287318/?label=gaBcCJ-Wp1gQ1r_hzAM&amp;guid=ON&amp;script=0"/> 
    </div>
<?php } ?>

<!-- Hotjar Tracking Code for www.awlandsundry.com -->
<script>
    (function (h, o, t, j, a, r) {
        h.hj = h.hj || function () {
            (h.hj.q = h.hj.q || []).push(arguments)
        };
        h._hjSettings = {hjid: 41912, hjsv: 6};
        a = o.getElementsByTagName('head')[0];
        r = o.createElement('script');
        r.async = 1;
        r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
        a.appendChild(r);
    })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
</script>

<script type="text/javascript">
    var __lc = {};
    __lc.license = 6098381;

    (function () {
        var lc = document.createElement('script');
        lc.type = 'text/javascript';
        lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(lc, s);
    })();
</script>

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
    var google_conversion_id = 966287318;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
</script>
<div style="display: none">
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
</div>
<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/966287318/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>