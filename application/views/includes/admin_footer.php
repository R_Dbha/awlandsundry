<?php
/**
 * @author Ajith Vp <ajith@kraftlabs.com>
 * @copyright Copyright (c) 2015, Ajith Vp <ajith@kraftlabs.com>
 * @date 28-Apr-2015
 */
?>
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/js/AdminLTE/app.js" type="text/javascript"></script>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var baseUrl = '<?php echo base_url(); ?>';
    var app_title = '<?php echo $this->config->item('app_title'); ?>';
    var image_base = '<?php echo CustomShoeConfig::IMG_BASE ; ?>';
    var imageBase = '<?php echo CustomShoeConfig::IMG_BASE; ?>';
    var svgPath = "<?php echo CustomShoeConfig::FILE_BASE . 'SVG/'; ?>";
    var designBase = "<?php echo CustomShoeConfig::FILE_BASE . 'designs/'; ?>";
    var thumbBase = "<?php echo CustomShoeConfig::FILE_BASE . 'thump/'; ?>";
</script>
<?php
if (isset($additional_js)) {
    if (is_array($additional_js)) {
        foreach ($additional_js as $js) {
            if (strpos($js, 'http') === FALSE) {
                $js = base_url() . 'assets/js/' . $js . '.js';
            }
            ?>
            <script type="text/javascript" src="<?php echo $js; ?>"></script>
            <?php
        }
    } elseif ($additional_js !== '') {
        if (strpos($additional_js, 'http') === FALSE) {
            $additional_js = base_url() . 'assets/js/' . $additional_js . '.js';
        }
        ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $additional_js; ?>.js"></script>
        <?php
    }
    ?>

<?php } ?>
</body>
</html>