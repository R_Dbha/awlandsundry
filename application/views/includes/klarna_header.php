<!DOCTYPE html>
<html lang="en" <?php echo isset($props) ? $props : ''; ?>>
    <head>
        <title><?php echo (isset($page_title)) ? $page_title : 'Awl & Sundry'; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=yes">
        <?php echo isset($meta) ? $meta : ''; ?>
        <link href="https://plus.google.com/+Awlandsundrycustomshoes" Rel="Publisher" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/css/images/icons/favicon.jpg" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/stacktable.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slicknav.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.bxslider.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/supersized.css">

        <?php if (isset($additional_css)) { ?>
            <?php if (is_array($additional_css)) { ?>
                <?php foreach ($additional_css as $css) { ?>
                    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' . $css; ?>.css" > 
                <?php } ?>
            <?php } ?>
            <?php echo (!is_array($additional_css)) ? (($additional_css != '') ? '<link rel="stylesheet" type="text/css" href="' . base_url() . 'assets/' . $additional_css . '.css" >' : '' ) : '' ?>
        <?php } ?>

        <script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
        </script>
    </head>
    <body class = "<?php echo isset($bodyclass) ? $bodyclass : ''; ?>">
        <div id="wrapper">
            <div id="header-wrapper" class="header-wrapper">
                <div id="header" class="container">
                    <div>
                        <div class="logo">
                            <a href="<?php echo base_url(); ?>"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content-wrapper">
                <div id="content">