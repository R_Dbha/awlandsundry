<html >
    <head>
        <title>Awl &amp; Sundry</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=yes">
        <style>
            @import url(https://fonts.googleapis.com/css?family=Raleway:400,500,600);
        </style>
    </head>
    <body style="line-height: 25px;background:url('<?php echo base_url(); ?>assets/css/images/email_bg.png');margin:0;padding:0;font-family: 'Raleway', sans-serif; color: #414141;">
        <div id="wrapper">
            <div id="container" style="width: 80%; margin:0 auto;">
            <div id="header-wrapper" style="min-height: 110px;">
                <div id="header" class="container">
                    <div class="logo" style="width: 200px;padding-top: 10px; min-height: 110px; margin: 0px auto;" >
                        <a href="<?php echo base_url(); ?>"><img style="margin: 15px auto; width: 200px; "src="<?php echo base_url() . 'assets/css/images/mob-logo.png'; ?>"></a>
                    </div>
                </div>
            </div>



