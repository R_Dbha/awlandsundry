<!DOCTYPE html>
<html lang="en" <?php echo isset($props) ? $props : '' ; ?>>
    <head>
        
        <title>
            <?php
            if (isset($page_title)) {
                echo $page_title;
            } else {
                echo 'Awl & Sundry';
            }
            ?>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=yes">
        <?php echo isset($meta) ? $meta : ''; ?>
         <link href="https://plus.google.com/+Awlandsundrycustomshoes" Rel="Publisher" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/css/images/icons/favicon.jpg" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/stacktable.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slicknav.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.bxslider.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/supersized.css">
        <?php
        if (isset($additional_css)) {
            if (is_array($additional_css)) {
                foreach ($additional_css as $css) {
                    ?>
                    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' . $css; ?>.css" > 
                    <?php
                }
            } elseif ($additional_css !== '') {
                ?>
                <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/' . $additional_css; ?>.css" > 
                <?php
            }
        }
        ?>
        <script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
        </script>
    </head>
    <body class = "<?php echo isset($bodyclass) ? $bodyclass : ''; ?>">

        <div id="wrapper">
            <div id="header-wrapper">
                <div id="header" class="container">
                    <div id="primary-menu">
                        <div class="logo">
                            <a href="<?php echo base_url(); ?>"></a>
                        </div>
                        <div class="right menu-right">
                            <ul class="secondary-links">
                                <li><a href="<?php echo base_url(); ?>faq">HELP</a></li>
                                <?php
                                $user = $this->session->userdata('user_details');
                                if ($user != FALSE) {
                                    ?> 
                                                                                                <!--li><a href="<?php echo base_url(); ?>my-account">MY ACCOUNT</a></li-->
                                <li id="divUser"><a  href="javascript:void(0)"><span>WELCOME,</span> <?php echo strtoupper($user['first_name']) ?></a>
                                        <div class="subMenu">
                                            <ul>
                                                <li><a href="<?php echo base_url(); ?>my-account">MY ACCOUNT</a></li>
                                                <?php
                                                if ($user['user_type_id'] === '1') {
                                                    ?>
                                                    <li><a href="<?php echo base_url(); ?>admin/index">ADMIN </a></li>
                                                    <?php
                                                }

                                                if ($user['user_type_id'] === '3') {
                                                    ?>
                                                    <li><a href="<?php echo base_url(); ?>admin/vendor">ADMIN </a></li>
                                                    <?php
                                                }
                                                ?>
                                                <li><a href="<?php echo base_url(); ?>logout?t=<?php echo time(); ?>" >LOGOUT </a></li>
                                            </ul>
                                        </div>
                                    </li>


                                <?php } else {
                                    ?>
                                    <li><a href="<?php echo base_url(); ?>login">LOGIN</a></li>
                                <?php } ?>
                                <li><a href="<?php echo base_url(); ?>cart">CART(<?php echo sizeof($this->cart->contents()); ?>)</a></li>
                            </ul>
                        </div>
                    </div>
                    <div id="slick-menu">
                        <div id="slick">
                        </div>
                    </div>
                    <div class="left menu-left" id="menu-left">
                        <ul class="primary-links">
                            <li class="main-link"><a class="shop">SHOP <span>|</span></a> 
                                <ul >
                                    <!--li class="sub-link"><a href="#" >FALL COLLECTION</a></li-->
                                    <li class="sub-link main-link"><a href="#" class="custom" >CUSTOM COLLECTIONS</a>
                                        <ul >
                                            <li class="sub-link"><a href="<?php echo base_url(); ?>products/oxford" >OXFORD</a></li>
                                            <li class="sub-link"><a href="<?php echo base_url(); ?>products/derby" >DERBY</a></li>
                                            <li class="sub-link"><a href="<?php echo base_url(); ?>products/monkstrap" >MONKSTRAP</a></li>
                                            <li class="sub-link"><a href="<?php echo base_url(); ?>products/loafer" >LOAFER</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo base_url(); ?>special/valentine">VALENTINE'S COLLECTION</a></li>
                                    <li><a href="<?php echo base_url(); ?>gift-card">GIFT CARD</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo base_url(); ?>create-a-custom-shoe">DESIGN</a></li>
                            
                            <!--li id="kickstarter"><a href="http://bit.ly/awlandsundry" target="_blank">KICKSTARTER</a></li-->
                            <li id="mob_login"><a href="<?php echo base_url(); ?>login">LOGIN</a></li>
                        </ul>
                    </div>

                    <div id="mobile_menu_wrap">
                        <ul id="mobile_menu">
                            <li class="main-link"><a>CUSTOM COLLECTIONS</a> 
                                <ul >
                                    <li class="sub-link"><a href="<?php echo base_url(); ?>products/oxford" >OXFORD</a></li>
                                    <li class="sub-link"><a href="<?php echo base_url(); ?>products/derby" >DERBY</a></li>
                                    <li class="sub-link"><a href="<?php echo base_url(); ?>products/monkstrap" >MONKSTRAP</a></li>
                                    <li class="sub-link"><a href="<?php echo base_url(); ?>products/loafer" >LOAFER</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo base_url(); ?>special/valentine">VALENTINE'S COLLECTION</a></li>
                            <li><a href="<?php echo base_url(); ?>create-a-custom-shoe">DESIGN</a></li>
                            <li><a style="font-weight: 500;" href="<?php echo base_url(); ?>gift-card">GIFT CARD</a></li>
                            <!--li><a href="http://bit.ly/awlandsundry" target="_blank">KICKSTARTER</a></li-->
                            <?php if ($user === FALSE) {
                                ?>
                                <li id="mob_login1"><a href="<?php echo base_url(); ?>login">LOGIN</a></li>
                                <?php
                            } else {
                                ?>
                                <li><a href="<?php echo base_url(); ?>my-account">MY ACCOUNT</a></li>
                                <li id="mob_login"><a href="<?php echo base_url(); ?>logout">LOGOUT</a></li>
                            <?php }
                            ?>

                        </ul>
                    </div>

                </div>

            </div>
            <div id="content-wrapper">
                <div id="content">

