</div>	
</div>	

<div id="footer-menu">
    <ul class="footer">
        <li><a href="https://medium.com/@AwlandSundry" target="_blank">BLOG</a></li>
        <li><a href="<?php echo base_url(); ?>faq#product">FAQ</a></li>
        <li><a href="<?php echo base_url(); ?>appointment">APPOINTMENT</a></li>
        <li><a href="<?php echo base_url(); ?>gift-card">GIFT CARD</a></li>
        <li><a href="<?php echo base_url(); ?>press">PRESS</a></li>
        <li><a href="<?php echo base_url(); ?>faq">CONTACT US</a></li>
        <li class="mob"><a href="<?php echo base_url(); ?>privacy-policy">PRIVACY POLICY</a></li>
        <li><a href="<?php echo base_url(); ?>terms-and-conditions">TERMS OF USE</a></li>
    </ul>
</div>
<div id="footer-wrapper">
    <div id="footer" class="container">
        <div id="footer-logo" class="footer">
            <a href="<?php echo base_url(); ?>"></a>
        </div>
        <div id="footer-copyright">
            <span class="footer">&copy; AWL &AMP; SUNDRY.</span>
            <span class="footer">ALL RIGHTS RESERVED. 2012-<?php echo date('Y'); ?></span>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/build/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ddscrollspy.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/prefixfree.min.js"></script>

<script>
    var baseUrl = '<?php echo base_url(); ?>';
</script>

<?php
if (isset($additional_js)) {
    if (is_array($additional_js)) {
        foreach ($additional_js as $js) {
            if (strpos($js, 'http') === FALSE) {
                $js = base_url() . 'assets/js/' . $js . '.js';
            }
            ?>
            <script type="text/javascript" src="<?php echo $js; ?>"></script>
            <?php
        }
    } elseif ($additional_js !== '') {
        if (strpos($additional_js, 'http') === FALSE) {
            $js = base_url() . 'assets/js/' . $additional_js . '.js';
        }
        ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $additional_js; ?>.js"></script>
    <?php } ?>
<?php } ?>


<script>
    $(function () {
        $('#mobile_menu').slicknav({prependTo: '#primary-menu'});
    });

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-40200583-1', 'auto');
    ga('send', 'pageview');
</script>

<noscript >
<img height = "1" width = "1" alt = "" style = "display:none" src = "https://www.facebook.com/tr?ev=6017866457827&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" />
</noscript>

<!-- Google Tag Manager -->
<noscript>
<iframe src="//www.googletagmanager.com/ns.html?id=GTM-TTQHZ8" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>

<script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-TTQHZ8');
</script>
<!-- End Google Tag Manager -->

<?php if ($this->router->method === 'order_confirmation' && $this->router->class === 'common') { ?>
    <div style="display:inline;"> 
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/966287318/?label=gaBcCJ-Wp1gQ1r_hzAM&amp;guid=ON&amp;script=0"/> 
    </div>
<?php } ?>

<!-- Hotjar Tracking Code for www.awlandsundry.com -->
<script>
    (function (h, o, t, j, a, r) {
        h.hj = h.hj || function () {
            (h.hj.q = h.hj.q || []).push(arguments)
        };
        h._hjSettings = {hjid: 41912, hjsv: 6};
        a = o.getElementsByTagName('head')[0];
        r = o.createElement('script');
        r.async = 1;
        r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
        a.appendChild(r);
    })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
</script>

<script type="text/javascript">
    var __lc = {};
    __lc.license = 6098381;

    (function () {
        var lc = document.createElement('script');
        lc.type = 'text/javascript';
        lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(lc, s);
    })();
</script>

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
    var google_conversion_id = 966287318;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/966287318/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>