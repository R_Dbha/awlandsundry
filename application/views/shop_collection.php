<?php if (isset($collections) && sizeof($collections)) { ?>
    <?php foreach ($collections as $key => $item) { ?>
        <li>
            <div class="image-set">
                <a href="<?php echo base_url() . 'products/' . $item['style_name'] . '/' . $item['name'] . '/' . $item['shoe_name']; ?>">
                    <div class="" style=" z-index: 1;"><img src="<?php echo $item['image'][1]; ?>" /></div>
                    <div class=""><img src="<?php echo $item['image'][0]; ?>" /></div>
                </a>
            </div>
            <div class="shop-product-details">
                <a href="<?php echo base_url() . 'products/' . $item['style_name'] . '/' . $item['name'] . '/' . $item['shoe_name']; ?>">
                    <h4 class="product-title ins-product-title"><?php echo $item['shoe_name']; ?></h4>
                    <h4 class="product-style ins-product-style"><?php echo $item['last_name'] . '/' . $item['style_name']; ?></h4>
                    <h5 class="product-price ins-product-price">$<?php echo $item['price']; ?>
                        <div class="hidden">
                            <?php
                            echo '<br/>quarter:' . $item['quarter_material'] . '-' . $item['quarter_color'];
                            echo '<br/>toe:' . $item['toe_material'] . '-' . $item['toe_color'];
                            echo '<br/>vamp:' . $item['vamp_material'] . '-' . $item['vamp_color'];
                            echo '<br/>eyestay:' . $item['eyestay_material'] . '-' . $item['eyestay_color'];
                            echo '<br/>foxing:' . $item['foxing_material'] . '-' . $item['foxing_color'];
                            ?>
                        </div>
                    </h5>
                    <!--<h6 class="product-deliver ins-product-deliver">DELIVERED IN 4-6 WEEKS</h6>-->
                </a>
            </div>
        </li>
    <?php } ?>
<?php } ?>