<div class="main_view">
    <h1 class="username">Hello, <?php echo @$user['first_name']; ?>!</h1>
    <p class="description">From your My Account Dashboard you have the ability to view a snapshot of your recent account activity
        and update your account information. Select a link below to view or edit information.</p>

    <div class="dasboard-right-content recent-orders">
        <div class="dashboard-content-heading-wrapper">
            <div class="dashboard-content-heading left">
                <h3>Recent Orders</h3>
            </div>
            <div class="dashboard-content-heading right">
                <span><!--a>VIEW</a--></span>
            </div>
        </div>
        <div id="recent-orders-content">
            <table width="100%">
                <thead>
                    <tr>
                        <th>ORDER#</th>
                        <th>DATE</th>
                        <th>SHIP TO</th>
                        <th>TOTAL</th>
                        <th>STATUS</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($orders != NULL) {
                        foreach ($orders as $order) {
                            $orderdate = date('d-m-y', strtotime($order['order_date']));
                            //<td>6/12/14</td> 
                            ?>
                            <tr>
                                <td><?php echo $order['invoice_no']; ?></td>
                                <td><?php echo $orderdate; ?></td>
                                <td><?php echo $order['name']; ?></td>
                                <td>$<?php echo $order['net_amt']; ?></td>
                                <td><span><?php echo $order['status']; ?></span></td>
                                <td><span><a href="#view_order_details/<?php echo $order['order_id']; ?>">View Order</a></span>|<span><a href="#">Track</a></span>|<span><a href="#">Exchange</a></span></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
    <div class=dasboard-right-content account-info>
        <div class="dashboard-content-heading-wrapper">
            <div class="dashboard-content-heading left">
                <h3>Account Information</h3>
            </div>
            <div class="dashboard-content-heading right">
                <span><a href="#edit_AccountInfo/main">EDIT</a></span><!--span><a>VIEW</a></span-->
            </div>
        </div>
        <div id="account-info-content">
            <p><?php echo @$user['first_name']; ?></p>
            <p><?php echo @$user['email']; ?></p>
        </div>
    </div>
    <div class=dasboard-right-content account-info>
        <div class="dashboard-content-heading-wrapper">
            <div class="dashboard-content-heading right">
                <!--span><a>EDIT</a></span><!--span><a>VIEW</a></span-->
            </div>
        </div>
        <div id="info-content">
            <div id="billing-address" class="left sbaddress">
                <h3>Default Billing Address</h3>

                <?php
                $address_billing_id = -1;
                ;
                if ($address_list != NULL) {
                    foreach ($address_list as $a) {
                        if ($a['is_billing'] == "1") {
                            $address_billing_id = $a['address_details_id'];
                            ?>
                            <p><?php echo $a['first_name'] . ' ' . $a['last_name'] ?></p>
                            <p><?php echo $a['address1'] ?></p>
                            <p><?php echo $a['address1'] ?></p>
                            <p><?php echo $a['city'] . ', ' . $a['state'] . ', ' . $a['zipcode'] ?></p>
                            <p><?php echo $a['country'] ?></p>
                            <p>T: <?php echo $a['phone'] ?></p>
                            <span><a style='color:#C72733' id="<?php echo $address_billing_id ?>" href="#add_account_info/dashboard/<?php echo $address_billing_id ?>"> EDIT </a></span>|<span><a style='color:#C72733' href="javascript:deleteAccount('<?php echo $address_billing_id ?>');"> DELETE</a></span>

                            <?php
                        }
                    }
                }
                ?>
            </div>
            <div id="shipping-address" class="right sbaddress">
                <h3>Default Shipping Address</h3>
                <?php
                $address_shipping_id = -1;
                if ($address_list != NULL) {
                    foreach ($address_list as $a) {
                        if ($a['is_shipping'] == "1") {
                            $address_shipping_id = $a['address_details_id'];
                            ?>
                            <p><?php echo $a['first_name'] . ' ' . $a['last_name'] ?></p>
                            <p><?php echo $a['address1'] ?></p>
                            <p><?php echo $a['address1'] ?></p>
                            <p><?php echo $a['city'] . ', ' . $a['state'] . ', ' . $a['zipcode'] ?></p>
                            <p><?php echo $a['country'] ?></p>
                            <p>T: <?php echo $a['phone'] ?></p>
                            <span><a style='color:#C72733' id="<?php echo $address_shipping_id ?>" href="#add_account_info/dashboard/<?php echo $address_shipping_id ?>"> EDIT </a></span>|<span><a style='color:#C72733' href="javascript:deleteAccount('<?php echo $address_shipping_id ?>');"> DELETE</a></span>

                            <?php
                        }
                    }
                }
                ?>
            </div>

        </div>
    </div>	
</div>
