<?php
$this->load->view('includes/header');
?>
<div id="details_wrapper">
    <div style="" id="details_section" class="container">
        <div class="spinner" id="loader" style="display: none;"></div>
        <div class="ready_product_details">
            <div class="image_thumbs">
                <div class="image_thumbs_fixed">
                    <ul id="nav">
                        <?php
                        if(is_array($images)){
                            $i = 1;
                            foreach ($images as $key => $value) {
                                ?>
                                <li class="product-gallery__thumb <?php if($i == 5){?>
                                    last 
                                <?php } ?>
                                ">
                                    <a href="#img<?php echo $i ?>"><img class="img-responsive" src="<?php echo $value ?>" alt="<?php echo @$alt; ?>"/></a>
                                </li>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
<!--            <div class="ready_image_container">-->

            <div class="ready_image_section">
                <ul>
                    <?php
                    if (is_array($images)) {
                        $i=1;
                        foreach ($images as $key => $value) {
                            ?>
                        <li id="img<?php echo $i ?>" <?php if($i==5){?>class="last"<?php }?>>
                            <div class="ready_prod_image_container"><img class="ready_prod_image" src="<?php echo $value ?>" alt="<?php echo @$alt; ?>"  /></div>
                        </li>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="ready-owl-carousel">
                <div id="ready_image_mobile">
                    <div class="row">
                        <div class="large-12 columns">
                            <div class="owl-carousel owl-theme">
                                <?php
                                if (is_array($images)) {
                                    foreach ($images as $key => $value) {
                                        ?>
                                  <img src="<?php echo $value ?>" title=" "  alt=" <?php echo @$alt; ?>" class="img-responsive">
                        <?php   }
                                    }
                                    ?>
                              </div>    
                        </div>
                    </div>
                </div>
            </div>
<!--</div>-->
            <div class="ready_details_section">
                <div class="details_fixed">
                    <div class="details_child">
                        <h1><?php echo @$item['shoe_name']; ?></h1>
                        <h3>Price:$<?php echo @$item['price']; ?></h3>
                        <!--<h5 class="trad-retail-price">Traditional Price:$<?php // echo @$item['price']; ?></h5>-->
                       <?php /*?> <p class="product-color"><?php echo ucwords($item['color']); ?></p>
                        <?php $color1 = preg_replace('/[\s-]+/', '-', $item['similar_shoes'][0]['color']);
                            $color2 = preg_replace('/[\s-]+/', '-',  $item['similar_shoes'][1]['color']);?>
                        <div class="product-available-colors">
                            <ul class="colors-list-ul">
                                <li>
                                    <a href="<?php echo base_url() . 'shop/products/'.$color1.'/'.$item['shoe_name'];?>" class="custom-color" id="<?php echo $item['shoe_name']; ?>-color-1">
                                        <p class="color-overlay <?php echo $item['similar_shoes'][0]['colorClass'];?>"><span class="color-dot <?php echo $color1;?>">&nbsp;</p>
                                        <p class="color-dot-text"><?php echo ucwords($item['similar_shoes'][0]['color']);?></p>                                            
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() . 'shop/products/'.$color2.'/'.$item['shoe_name'];?>" class="custom-color" id="<?php echo $item['shoe_name']; ?>-color-2">
                                        <p class="color-overlay <?php echo $item['similar_shoes'][1]['colorClass'];?>"><span class="color-dot <?php echo $color2;?>">&nbsp;</p>
                                        <p class="color-dot-text"><?php echo ucwords($item['similar_shoes'][1]['color']); ?></p>
                                    </a>
                                </li>
                            </ul>
                        </div> <?php */?>
                        <div class="details_section_options">
                            <div class="size_options">
                                <label class='select_size_options'>Select Size</label>
                                <span class="standard_width"> Standard D Width</span>
                            </div>
                            <div class="size_options_container">
                                <div>
                                    <div class="size_options_selector"><span>5</span></div>
                                    <div class="size_options_selector"><span>5.5</span></div>
                                    <div class="size_options_selector"><span>6</span></div>
                                    <div class="size_options_selector"><span>6.5</span></div>
                                    <div class="size_options_selector"><span>7</span></div>
                                    <div class="size_options_selector"><span>7.5</span></div>
                                </div>
                            </div>
                            <div class="size_options_container">
                                <div>
                                    <div class="size_options_selector"><span>8</span></div>
                                    <div class="size_options_selector"><span>8.5</span></div>
                                    <div class="size_options_selector"><span>9</span></div>
                                    <div class="size_options_selector"><span>9.5</span></div>
                                    <div class="size_options_selector"><span>10</span></div>
                                    <div class="size_options_selector"><span>10.5</span></div>
                                </div>
                            </div>
                            <div class="size_options_container">
                                <div>
                                    <div class="size_options_selector"><span>11</span></div>
                                    <div class="size_options_selector"><span>11.5</span></div>
                                    <div class="size_options_selector"><span>12</span></div>
                                    <div class="size_options_selector"><span>12.5</span></div>
                                    <div class="size_options_selector"><span>13</span></div>
                                    <div class="size_options_selector"><span>13.5</span></div>
                                    <div class="size_options_selector"><span>14</span></div>
                                </div>
                            </div>
<!--                            <div class="us-shoe-size">Select your regular US shoe size</div>
                            <div class="half-size-select">If between sizes, select half a size up</div>-->
                            <div>
                                <a class="flat_button  " id="add-readywear-item-to-cart" >Add to Cart</a>
                                <input type="hidden" id="ready_wear_id" value="<?php echo $item['id']; ?>">
                            </div>
                            <div class="free-shipping-return">Ships in 3 weeks</div>
                            <div class="shoe_details_section">
                                <?php /*<div>
                                    <label class="title">DETAILS</label>
                                </div>
                                <div class="shoe_details_section_list">
                                    <br/>
                                    <?php
                                    if ($item['details'] !== "") {
                                        ?>
                                        <ul>
                                            <?php
                                            foreach (explode(',', $item['details']) as $value) {
                                                ?><li><?php echo str_replace("'", '', $value); ?></li><?php
                                            }
                                            ?> </ul><?php
                                        }
                                        ?>
                                </div> */?>
                            </div>
                             <div class="shoe_details_section">
                                <label class="title">DESCRIPTION</label>
                                <div class="shoe_details_section_list">
                                    <label><?php echo @$item['description']; ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('includes/footer');
?>