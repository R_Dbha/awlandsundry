<?php
$stl = "";
$lgndisp = "block";
$regdisp = "none";
if (isset($registersubmit)) {
    $stl = 'style="height: 386px;"';
    $lgndisp = "none";
    $regdisp = "block";
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <head>
        <?php $this->load->view('common/template/begin'); ?>
    <body class="page cart checkout login">

        <div id="popupOverlay" >

        </div>
        <div id="signin_popup" class="messageBox messageBoxBlue loginDetails" >
            <h1 id="checkoutpagelogo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
            <div class="order billing clearfix" >
                <div class="column" >
                    <div id="login" style="display:<?php echo $lgndisp; ?>;">    
                        <h4>Please sign in</h4>
                        <div class="invalid_errors">
                            <?php
                            if (isset($errors['username'])) {
                                ?>
                                <div class="invalid invalid_uname"><span>* </span>PLEASE ENTER EMAIL</div>
                                <?php
                            }
                            if (isset($errors['password'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="invalid invalid_pword"><span>* </span>PLEASE ENTER PASSWORD</div>
                                <?php
                            }
                            if (isset($errors['invalid'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="invalid invalid_uname"><span>* </span>INVALID USERNAME/PASSWORD</div>
                            <?php } ?>
                        </div>
                        <form action="<?php echo base_url(); ?>login" method="post" name="awlsundry" class="sign_in" id="signin">
                            <input type="hidden" name="id" value="">
                            <div class="" id="invalid_user" style="color: #ED5224; display: none;"><span>* </span>INVALID USERNAME/PASSWORD</div>
                            <input type="text" name="username" class="lowercase-email" id="username" placeholder="EMAIL" value="">
                            <input type="password" name="password" placeholder="PASSWORD" id="password" value="">
                            <div class="greyBtnWrap">
                                <input type="submit" name="loginsubmit" id="submitbutton" class="button blue" value="Submit">
                            </div>
                        </form>

                        <a href="<?php echo base_url() . 'forgot-password' ?>" class="link forget_pword">Forgot your password?</a>      
                        <h4 class="orTxt">~ Or ~</h4>
                        <div class="greyBtnWrap">
                            <a href="javascript:;" class="button grey" id="signupform">Create a new account</a>
                        </div>
                        <a href="javascript:;" class="fb_loginBtn">&nbsp;</a>
                    </div>
                    <div id="signup" style="display:<?php echo $regdisp; ?>;">
                        <h4>Create a new account</h4>
                        <span class="grey">Enter your login details</span>
                        <form action="<?php echo base_url(); ?>login" method="post" name="awlsundry" class="create_new_acc" id="register_submit">
                            <input type="hidden" name="id" value="">
                            <input type="text" name="firstname" id="firstname" placeholder="FIRST NAME" maxlength="15" value="">
                            <input type="text" name="lastname" id="lastname" placeholder="LAST NAME" maxlength="15" value="">
                            <input type="text" name="email" id="email" placeholder="EMAIL ADDRESS" class="lowercase-email" value="">
                            <input type="password" name="password" id="reg_password" placeholder="PASSWORD (6-15 CHARACTERS)" maxlength="15" value="">
                            <!--input type="text" name="birth_date" id="birth_date" placeholder="DATE OF BIRTH (MM/DD/YYYY)" value="<?php echo isset($_POST['birth_date']) ? $_POST['birth_date'] : '' ?>" /--->
                            <div class="greyBtnWrap" >
                                <input type="submit" name="registersubmit" id="submitbutton" class="button submit" value="Submit">
                            </div>
                            <div class="greyBtnWrap" >
                                <input type="button" id="returnbutton" class="button submit" value="Already have an account?">
                            </div>
                        </form>                                    
                        <div class="invalid_errors">
                            <!-- USERNAME NOT FOUND / PASSWORD INCORRECT  #ED5224 -->
                            <?php
                            if (isset($errors['firstname'])) {
                                ?>
                                <div class="error-check"><span>* </span>PLEASE ENTER FIRSTNAME</div>
                                <?php
                            }
                            if (isset($errors['lastname'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="error-check"><span>* </span>PLEASE ENTER LASTNAME</div>
                                <?php
                            }
                            if (isset($errors['email'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="error-check"><span>* </span>PLEASE ENTER EMAIL</div>
                                <?php
                            }
                            if (isset($errors['spassword'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="error-check"><span>* </span>PLEASE ENTER PASSWORD</div>
                                <?php
                            }
                            if (isset($errors['fnamelenght'])) {
                                ?>
                                <div class="error-check"><span>* </span>FIRSTNAME MUST BE 4-15 CHARACTERS</div>
                                <?php
                            }
                            if (isset($errors['lnamelenght'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="error-check"><span>* </span>LASTNAME MUST BE 4-15 CHARACTERS</div>
                                <?php
                            }
                            if (isset($errors['emailvalid'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="error-check"><span>* </span>PLEASE ENTER A VALID EMAIL</div>
                                <?php
                            }
                            if (isset($errors['passlength'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="error-check"><span>* </span>PASSWORD MUST BE 6-15 CHARACTERS</div>
                                <?php
                            }
                            if (isset($errors['emailunique'])) {
                                ?>
                                <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                                <div class="error-check"><span>* </span>THIS EMAIL IS ALREADY REGISTERED</div>
                                <?php
                            }
                            if (isset($errors['birth_date'])) {
                                ?>
                                <div class="error-check"><span>* </span>PLEASE ENTER YOUR DATE OF BIRTH</div>
                                <?php
                            } else {
                                ?>
                                <div class="error-check"></div>
                                <?php
                            }
                            if (isset($errors['iv_birth_date'])) {
                                ?>
                                <div class="error-check"><span>* </span>PLEASE ENTER YOUR DATE OF BIRTH(MM/DD/YYYY)</div>
                                <?php
                            } else {
                                ?>
                                <div class="error-check"></div>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <div id="emailtoVerify" style="display:none">
                        <p>YOUR ACCOUNT HAS BEEN SUCCESSFULLY CREATED.PLEASE CHECK YOUR EMAIL FOR VERIFICATION LINK AND ACCOUNT DETAILS</p>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript">

            $(document).ready(function() {
                $("#popupOverlay").show();
                $('body').css('overflow', 'hidden');
                var $selectionPopup = $('#signin_popup');
                // $selectionPopup.html(msg);
                $selectionPopup.css("opacity", "0");
                $selectionPopup.css({
                    'display': 'block',
                    'margin-left': document.body.clientWidth / 2 - $selectionPopup.width() / 2 - 30,
                });
                $selectionPopup.css("opacity", "1");
                $selectionPopup.animate({"height": "470", "top": '95'}, 1200);

                $(".fb_loginBtn").unbind().bind('click', function() {
                    window.open(baseUrl + 'facebooklogin', 'FB', 'width=400,height=300');

                });

                $("#signupform").on('click', function() {
                    $('#login').hide();
                    $('#emailtoVerify').hide();
                    $('#signup').show();
                });
                $("#returnbutton").on('click', function() {
                    $('#login').show();
                    $('#emailtoVerify').hide();
                    $('#signup').hide();
                });
                $('#signin').submit(function() {
                    var username = $('#username').val();
                    var password = $('#password').val();
                    $.ajax({
                        type: "POST",
                        url: baseUrl + "customshoe/login",
                        data: {
                            'username': username,
                            'password': password,
                            'loginsubmit': 'submit',
                        },
                        dataType: 'html',
                        success: function(msg) {
                            if (msg === 'success') {
                                window.location =baseUrl + "custom-shoe/checkout";
                            } else if (msg === 'pendingVerification') {
                                var $selectionPopup = $('#signin_popup');
                                $('#login').hide();
                                $('#emailtoVerify').show();
                                $('#signup').hide();
                            } else if (msg === 'Invalid Login') {
                               $('#invalid_user').show();
                              
                            }
                        }

                    });
                    return false;
                });
                $('#register_submit').submit(function() {
                    var fname = $('#firstname').val();
                    var lname = $('#lastname').val();
                    var email = $('#email').val();
                    var pwd = $('#reg_password').val();
                    var dob = $('#birth_date').val();
                    $.ajax({
                        type: "POST",
                        url: baseUrl + "customshoe/login",
                        data: {
                            'firstname': fname,
                            'lastname': lname,
                            'email': email,
                            'password': pwd,
                            'birth_date': dob,
                            'registersubmit': 'registersubmit'
                        },
                        dataType: 'html',
                        success: function(msg) {
                            if (msg === 'success') {
                                window.location = baseUrl + "common/login";
                            } else {
                                var $selectionPopup = $('#signin_popup');
                                $selectionPopup.html(msg);
                                //$('#login').hide();
                                //$('#signup').show();
                            }
                        }

                    });
                    return false;
                });

            });
        </script>
    </body>
</html>    