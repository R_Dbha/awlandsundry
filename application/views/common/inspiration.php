<!DOCTYPE html> 
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php $this->load->view('common/template/begin',@$data); ?>
    <body class="page inspiration">
        <header id="header">
            <section id="toolbar" class="">	
                <div class="toolbarInner clearfix">
                    <div class="wrapper content">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Get Inspired</span>
                        </div>

                        <div class="right"> 
                            <ul id="headerNavLinks">

                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li> 
                                <?php $this->load->view('common/login_status'); ?>
                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&Sundry</a>
            </h1>
            <div class="wrapper shopping_cart_pop clearfix">
                <section class="shopping_cart_mini"></section>
            </div>
        </header>
        <section id="middle_container">     
            <div class="wrapper clearfix quote">
                <h3>Shoe genius is 1% inspiration and 99% homage...</h3>
            </div>

            <section id="main" class="page wrapper clearfix">
                <div class="wrapper clearfix inspirationInnerTop clearfix">
                    <div id="inspiration_filter" class="wrapper clearfix">

                        <div>
                            <span>Filter by </span>
                            <a href="javascript:;" id="clearall">[ Clear All ]</a>
                        </div>

                        <form id="filter">
                            <div>
                                <label class="last" for="last">Base last</label>
                                <select name="last" id="last">
                                    <option value="" selected="selected">All</option>
                                    <?php
                                    foreach ($laststyles as $laststyle) {
                                        ?>
                                        <option value="<?php echo $laststyle['last_item_def_id'] ?>"><?php echo $laststyle['last_name'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div>
                                <label class="pattern" for="pattern">Style</label>
                                <select name="base" id="base">
                                    <option value="" selected="selected">All</option>
                                    <?php
                                    foreach ($defstyles as $style) {
                                        ?>
                                        <option data-byname="true" value="<?php echo $style['style_name']; ?>">
                                            <?php echo $style['style_name']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>

                            <div>
                                <label class="material" for="material">Material</label>
                                <select name="material" id="leather">
                                    <option value="" selected="selected">All</option>
                                    <?php
                                    foreach ($materialcolors as $matcol) {
                                        ?>
                                        <option value="<?php echo $matcol['material_code']; ?>">
                                            <?php echo $matcol['material_name']; ?>
                                        </option>
                                        <?php
                                    }
                                    ?> 
                                </select>
                            </div>
                            <div>
                                <label class="color" for="color">Color</label>
                                <select name="color" id="color">
                                    <option value="" selected="selected">All</option>
                                </select>
                            </div>

                        </form>

                        <div id="search_username">
                            <label for="username" id="lblusername">search by Designer name</label>
                            <input type="text" name="username" id="username" placeholder="ENTER DESIGNER NAME"/>
                            <a href="javascript:;" class="submituser">click</a>
                        </div>
                    </div>
                    <div class="inspirationLinks">
                        <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style"><span>Create a custom shoe</span></a>
                    </div>
                </div>
            </section>
            <section id="content" class="clearfix">
                <div class="row clearfix" style="background-position-y: 0;">
                    <div class="row_wrapper clearfix" id="shedesigns">

                    </div>
                    <div id='loader'><div >&nbsp;</div></div>
                </div>
            </section>	
            <div id="popupOverlay"></div>
            <div id="accounts_selection_popup" class="popup"></div>
            <div id="shared"></div>
        </section>
        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>

        <script type="text/javascript">
            var materialColors = [];
            var lastStyles = [];
            var defaultStylenames = [];
<?php
foreach ($materialcolors as $matcol) {
    ?>
                materialColors['<?php echo $matcol['material_code']; ?>'] = {"name": '<?php echo $matcol['material_name']; ?>', colors: <?php echo json_encode($matcol['colors']); ?>};
    <?php
}
foreach ($laststyles as $laststyle) {
    ?>
                lastStyles['<?php echo $laststyle['last_item_def_id']; ?>'] = {"name": '<?php echo $laststyle['last_name']; ?>', styles: <?php echo json_encode($laststyle['styles']); ?>};
    <?php
}
foreach ($defstyles as $style) {
    ?>
                defaultStylenames.push('<?php echo $style['style_name']; ?>');
    <?php
}
?>
            $(window).load(function() {
                ShoeDesign.loadinspiration();
<?php if (isset($design_id)) { ?>
                    ShoeDesign.designDetails('<?php echo $design_id; ?>', '');
<?php } ?>
            });
            /*$(document).ready(function() {
             $('.styledCheckbox').change(function() {
             $type = $('#designtype').val();
             var purchasecheck = $('.styledCheckbox input#purchased').is(':checked');
             var savecheck = $('.styledCheckbox input#purchased').is(':checked');
             var favcheck = $('.styledCheckbox input#purchased').is(':checked');
             //getdesigns('', $type, purchasecheck, savecheck, favcheck);
             });
             });*/
            $(window).scroll(function() {
                if ($(window).scrollTop() + $('#footer').height() >= $(document).height() - $(window).height()) {
                    if (ShoeDesign.isEnd || ShoeDesign.requestSent) {
                        return;
                    }
                    ShoeDesign.requestSent = true;
                    ShoeDesign.loadinspiration();
                }
                return;
            });

            $(document).ready(function() {
                var isLikeorFav = '';
<?php
$isLikeFavAfterLogin = $this->session->userdata('likeFavAfterLogin');
if ($isLikeFavAfterLogin != NULL) {
    $likeOrfav = $isLikeFavAfterLogin['favOrlike'];
    echo "isLikeorFav = '" . $likeOrfav . "';";
    $this->session->unset_userdata('likeFavAfterLogin');
}
?>
                if (isLikeorFav == 'Like')
                    alert('You have liked this shoe.');
                else if (isLikeorFav == 'Favorite') {
                    alert('This shoe has been added to your favorites.');
                }
                else if (isLikeorFav == 'existsLike') {
                    alert('You have already liked this shoe.');
                }
                else if (isLikeorFav == 'existsFav') {
                    alert('You have already added this shoe to your favorites');
                }
                /*
                 Common.loadShareView(baseUrl, 'A wise sage once said, "you can judge a man by his shoes". Checkout this custom made pair I just designed at Awl & Sundry.', 'https://awlandsundry.com/files/designs/739305_A0.png', function(response){
                 $('#shared').append(response);
                 }); */
                fillAllcolors();

                jQuery('#filter select').dropkick({
                    //   theme: 'black',
                    //  change: function(value, label) {
                    //    console.log(value + label);
                    // $(this).dropkick('theme', value);
                    //     return false;
                    // }


                });
                $('#base').unbind('change');
                $('#base').change(function() {
                    searchInspiration();
                    return false;
                });

                $(".submituser").on('click , touchend', function() {
                    searchInspiration();
                });
                $("#username").keypress(function(e) {
                    if (e.charCode == "13" || e.keyCode == "13") {
                        searchInspiration();
                    }
                });

                $('#leather').unbind('change');
                $('#leather').change(function() {
                    $("#dk_container_color .dk_label").text("All");
                    $("#dk_container_color .dk_options li").eq(0).addClass("dk_option_current");
                    $("#dk_container_color .dk_options li").eq(0).nextAll().remove();
                    $("#color option").eq(0).nextAll().remove();
                    $("#color option").eq(0).attr("selected", "selected");
                    if ($("#leather").val() !== "") {
                        $.each(materialColors[$("#leather").val()]['colors'], function(i, val) {
                            $li = $("<li>");
                            $option = $("<option>");
                            $option.attr("value", val.color_code);
                            $option.text(val.color_name);
                            $a = $("<a>");
                            $a.attr("data-dk-dropdown-value", val.color_code);
                            $a.text(val.color_name);
                            $li.append($a);
                            $("#color").append($option);
                            $("#dk_container_color .dk_options .dk_options_inner").append($li);
                        });
                        searchInspiration();
                    } else {
                        fillAllcolors();
                        searchInspiration();
                    }
                });
                $('#color').unbind('change');
                $('#color').change(function() {
                    searchInspiration();
                });


                $('#last').off('change').blur().on('change', function(event) {
                    event.stopPropagation();
                    event.preventDefault();

                    $("#dk_container_base .dk_label").text("All");
                    $("#dk_container_base .dk_options li").eq(0).addClass("dk_option_current");
                    $("#dk_container_base .dk_options li").eq(0).nextAll().remove();
                    $("#base option").eq(0).nextAll().remove();
                    $("#base option").eq(0).attr("selected", "selected");
                    if ($("#last").val() !== "") {
                        $.each(lastStyles[$("#last").val()]['styles'], function(i, val) {
                            $li = $("<li>");
                            $option = $("<option>");
                            $option.attr("value", val.last_style_id);
                            $option.text(val.style_name);
                            $a = $("<a>");
                            $a.attr("data-dk-dropdown-value", val.last_style_id);
                            $a.text(val.style_name);
                            $li.append($a);
                            $("#base").append($option);
                            $("#dk_container_base .dk_options .dk_options_inner").append($li);
                        });
                    } else {
                        $.each(defaultStylenames, function(i, val) {
                            $li = $("<li>");
                            $option = $("<option data-byname='true'>");
                            $option.attr("value", val);
                            $option.text(val);
                            $a = $("<a>");
                            $a.attr("data-dk-dropdown-value", val);
                            $a.text(val);
                            $li.append($a);
                            $("#base").append($option);
                            $("#dk_container_base .dk_options .dk_options_inner").append($li);
                        });
                    }
                    searchInspiration();
                    return false;
                });

                $('#clearall').on("click", function() {
                    $('#filter .dk_label').text('All');
                    $('#filter .dk_label').val('');
                    $(".dk_options_inner li.dk_option_current").removeClass('dk_option_current');
                    $(".dk_options_inner li a[data-dk-dropdown-value='']").parent().addClass('dk_option_current');
                    $(".dk_options_inner li a[data-dk-dropdown-value='']").val('');
                    $('#username').val("");
                    $('#username').removeClass("uname_focused");
                    $("#username").val("");
                    $("#last").val("");
                    $("#base").val("");
                    $("#leather").val("");
                    $("#color").val("");
                    ShoeDesign.isEnd = false;
                    searchInspiration();
                    return false;
                });

            });
            function searchInspiration() {
                ShoeDesign.offset = 0;
                ShoeDesign.limit = 12;
                ShoeDesign.isFirst = true;
                //$(window.opera ? 'html' : 'html, body').animate({
                $(window.opera ? 'html' : 'html').animate({
                    scrollTop: 0
                }, 500, function() {
                    $("#shedesigns").html('');
                    ShoeDesign.loadinspiration();
                    return false;
                });
                return false;
            }
            function fillAllcolors() {
                var clrs = [];
                var clr;
                $.each(Object.keys(materialColors), function(i, material) {
                    $.each(materialColors[material].colors, function(j, clr) {
                        if ($.inArray(clr.color_name, clrs) === -1) {
                            clrs.push(clr.color_name);

                            $li = $("<li>");
                            $option = $("<option>");
                            $option.attr("value", clr.color_name);
                            $option.text(clr.color_name);
                            $a = $("<a>");
                            $a.attr("data-dk-dropdown-value", clr.color_name);
                            $a.text(clr.color_name);
                            $li.append($a);
                            $("#color").append($option);
                            $("#dk_container_color .dk_options .dk_options_inner").append($li);
                        }
                        //console.log(clrs['color_name']);

                    });
                });
                //clrs.sort();
                // $.each(clrs,function(a,b){console.log(b)});
            }
            /*function getinspirations() {
             $('#search_username #username').addClass('active');
             $last = $("#last option:selected").val();
             $leather = $("#leather option:selected").val();
             $color = $("#color option:selected").val();
             $base = $("#base option:selected").val();
             $username = $('#username').val();
             //console.log($username);
             
             $.ajax({
             type: "GET",
             url: "http://awlandsundry.com/~awlsundr/public" + "/home/getinspirations",
             data: {
             userid: '',
             last: $last,
             base: $base,
             leather: $leather,
             color: $color,
             username: $username
             },
             success: function(msg) {
             //console.log(msg);
             $('#content').html(msg.html);
             }
             });
             }*/




            /*function clearall() {
             $('#base').val("");
             $('#last').val("");
             $('#leather').val("");
             $('#color').val("");
             $('#username').val("");
             location.reload();
             //getinspirations();
             }*/


        </script>   
        <style type="text/css" media="screen">
            #shared {
                position: absolute;
                left: 0;
                top: 40px;
            }

        </style>
    </body>
</html>        
