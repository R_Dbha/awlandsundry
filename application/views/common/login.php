<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <head>
        <?php $this->load->view('common/template/begin'); ?>
    <body class="page login">
        <header id="header">
            <section id="toolbar" class="clearfix">
                <div class="toolbarInner clearfix">
                    <div class="wrapper clearfix">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Login</span>
                        </div>
                        <div class="right"> 
                            <ul>
                                <li class="login">
                                    <a href="<?php echo base_url(); ?>signup">Sign up</a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
        </header>
        <section id="middle_container">              
            <section class="wrapper clearfix main existing_ulogin">
                <div class="loginBoxWrap">
                    <h4>Please sign in</h4>
                    <form action="<?php echo base_url(); ?>login" method="post" name="awlsundry" class="sign_in" id="awlsundry">
                        <input type="hidden" name="id" value="">
                        <input type="text" name="username" class="lowercase-email" id="username" placeholder="EMAIL" value="">
                        <input type="password" name="password" placeholder="PASSWORD" id="password" value="">
                        <input type="submit" name="submit" id="submitbutton" class="button blue" value="Submit">
                    </form>
                    <div class="invalid_errors">
                        <?php
                        if (isset($errors['username'])) {
                            ?>
                            <div class="invalid invalid_uname"><span>* </span>PLEASE ENTER EMAIL</div>
                            <?php
                        }
                        if (isset($errors['password'])) {
                            ?>
                            <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                            <div class="invalid invalid_pword"><span>* </span>PLEASE ENTER PASSWORD</div>
                            <?php
                        }
                        if (isset($errors['invalid'])) {
                            ?>
                            <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                            <div class="invalid invalid_uname"><span>* </span>INVALID USERNAME/PASSWORD</div>
                        <?php } 
                             if (isset($errors['unverified'])) {
                            ?>
                            <!-- USERNAME NOT FOUND / PASSWORD INCORRECT-->
                            <div class="invalid invalid_uname"><span>* </span>PLEASE CHECK YOUR EMAIL FIRST</div>
                        <?php } ?>
    <!--<h2 class="invalid invalid_uname"><span>* </span>
    </h2> -->
                    </div>
                    <a href="<?php echo base_url(); ?>forgot-password" class="link forget_pword">Forgot your password?</a>
                    <h4 class="orTxt">~ Or ~</h4>
                    <div class="greyBtnWrap">
                        <a href="<?php echo base_url(); ?>signup" class="button grey">Create a new account</a>
                    </div>
                    <a href="javascript:;" class="fb_loginBtn">&nbsp;</a>
                    <a href="<?php echo base_url(); ?>" class="cancel_sign">Cancel sign in</a>
                </div>
            </section>
        </section>       
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
        <script type="text/javascript">
            $(".fb_loginBtn").off('click touchend').on('click touchend',function() {
                window.open(baseUrl + 'facebooklogin','FB','width=400,height=300');
            });
            $('input#username').keyup(function() {
                $('.incorrect_uname').hide();
                $('input#username').removeClass('invalid_box');
            });
        </script>            

        <?php $this->load->view('common/template/footer'); ?>
    </body>
</html>        

