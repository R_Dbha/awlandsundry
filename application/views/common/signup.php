<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->


    <?php $this->load->view('common/template/begin'); ?>    
    <body class="page login">
        <header id="header">
            <section id="toolbar" class="clearfix">
                <div class="toolbarInner clearfix">
                    <div class="wrapper clearfix">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Sign up</span>
                        </div>
                        <div class="right"> 
                            <ul>
                                <!--<li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>common/story">Our Story</a>
                                </li> -->
                                <li class="login active">
                                    <a href="<?php echo base_url(); ?>login">Log in</a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
            <style>
                .invalid_errors{
                    margin-top: 100px;
                    width: 138px;
                    font-size: 9px;
                    line-height: 25px;
                    color: red;
                }

            </style>
        </header>

        <section class="wrapper clearfix main create_new">
            <div class="loginBoxWrap" id="email_signup" <?php if(sizeof($errors)) echo 'style="display:block;" '; ?> >
                <h4>Create a new account</h4>
                <span class="grey">Enter your login details</span>
                <form action="<?php echo base_url(); ?>signup" method="post" name="awlsundry" class="create_new_acc" id="awlsundry">
                    <input type="hidden" name="id" value="" />
                    <input type="text" name="firstname" placeholder="FIRST NAME" value="<?php echo isset($_POST['firstname']) ? $_POST['firstname'] : '' ?>" />
                    <input type="text" name="lastname" placeholder="LAST NAME" value="<?php echo isset($_POST['lastname']) ? $_POST['lastname'] : '' ?>">
                    <input type="text" name="email" placeholder="EMAIL ADDRESS" class="lowercase-email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>" />
                    <input type="password" name="password" placeholder="PASSWORD (6-15 CHARACTERS)" value="<?php echo isset($_POST['password']) ? $_POST['password'] : '' ?>" />
                    <!--input type="text" name="birth_date" placeholder="DATE OF BIRTH (MM/DD/YYYY)" value="<?php echo isset($_POST['birth_date']) ? $_POST['birth_date'] : '' ?>" /-->
                    <input type="submit" name="submit" id="submitbutton" class="button submit" value="Submit" />
                    <!-- <a href="javascript:;" class="fbsignup">&nbsp;</a>-->
                </form>
                <!-- <form class="create_new_acc">
                        <input type="text" name="username" placeholder="USER NAME (4-15 CHARACTERS)" required="required" />
                        <input type="text" name="password" placeholder="PASSWORD (6-15 CHARACTERS)" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" />
                        <input type="email" name="email" placeholder="EMAIL ADDRESS" />
                        <input type="email" name="email_re" placeholder="RE-ENTER EMAIL ADDRESS" />
                        <input class="button submit" type="submit" value="Create your account" />
                </form>  -->
                <a href="<?php echo base_url(); ?>" class="cancel_sign">Cancel sign in</a>


                <div class="invalid_errors ">
                    <table>
                        <tr>
                            <td><div class="error-check">
                                <?php
                                if (isset($errors['firstname'])) {
                                    ?><span>* </span>PLEASE ENTER FIRST NAME<?php
                                }elseif (isset($errors['fnamelenght'])) {
                                    ?><span>* </span>FIRST NAME MUST BE 4-15 CHARACTERS<?php
                                }
                                ?>
                                </div>
                            </td> 
                        </tr>
                        <tr>
                            <td>
                                <div class="error-check">
                                <?php
                                if (isset($errors['lastname'])) {
                                    ?><span>* </span>PLEASE ENTER LAST NAME</div><?php
                                }
                                if (isset($errors['lnamelenght'])) {
                                    ?><span>* </span>LASTNAME MUST BE 4-15 CHARACTERS<?php
                                }
                                ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="error-check">
                                <?php
                                if (isset($errors['email'])) {
                                    ?><span>* </span>PLEASE ENTER EMAIL<?php
                                }
                                if (isset($errors['emailunique'])) {
                                    ?><span>* </span>THIS EMAIL IS ALREADY REGISTERED<?php
                                }
                                if (isset($errors['emailvalid'])) {
                                    ?><span>* </span>PLEASE ENTER A VALID EMAIL<?php
                                }
                                ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="error-check">
                                <?php
                                if (isset($errors['spassword'])) {
                                    ?><span>* </span>PLEASE ENTER PASSWORD<?php
                                }
                                if (isset($errors['passlength'])) {
                                    ?><span>* </span>PASSWORD MUST BE 6-15 CHARACTERS<?php
                                }
                                ?>
                                </div>
                            </td>
                        </tr>
                        <!--tr>
                            <td><div class="error-check">
                                <?php
                                if (isset($errors['birth_date'])) {
                                    ?><span>* </span>PLEASE ENTER YOUR DATE OF BIRTH<?php
                                }
                                if (isset($errors['iv_birth_date'])) {
                                    ?><span>* </span>PLEASE ENTER YOUR DATE OF BIRTH(MM/DD/YYYY)<?php
                                }
                                ?></div>
                            </td>
                        </tr--->
                    </table>
                </div>

            </div>
            <div id="login_button_wrap" <?php if(sizeof($errors)) echo 'style="display:none;" '; ?> >
                <span class="grey">Sign up using email</span>
                <a href="javascript:;" class="fbsignup">&nbsp;</a>    
            </div>

            <div class="signup_text"><p>By clicking on Sign up, you agree to Awl &amp; Sundry's 
                    <a href="<?php echo base_url(); ?>terms-and-conditions">terms of use</a> and 
                    <a href="<?php echo base_url(); ?>privacy-policy">privacy policy</a>. </p>
            </div>
        </section>	            

        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
        <script type="text/javascript">
                                $(".fbsignup").off('click touchend').on('click touchend',function() {
                                    window.open(baseUrl + 'facebooklogin', 'FB', 'width=400,height=300');
                                });
        </script>

    </body>
</html>        

