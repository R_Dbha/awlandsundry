<?php $this->load->view('common/template/begin'); ?>

    <body class="page about">
        <header id="header">
            <section id="toolbar">
                <div class="toolbarInner clearfix">	
                    <div class="wrapper content clearfix">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Our story</span>
                        </div>

                        <div class="right"> 
                            <ul id="headerNavLinks">

                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li> 
                                <?php $this->load->view('common/login_status'); ?>
                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
            <div class="wrapper shopping_cart_pop clearfix">
                <section class="shopping_cart_mini">
                    <h4>ORDER CART</h4>
                    <h2 class="emptyCart">Cart is Empty.</h2>
                </section>
            </div>
            <div class="patterned-rule-blue"  ></div>
        </header>
        <section id="middle_container"  >
            <div class="wrapper clearfix quote">
                <h3 style="margin: 51px 0 56px 0;" class="align-left">Awl & Sundry in the media</h3>
                <!--<h4>we founded awl & sundry to bring attentively handcrafted men’s shoes within reach.</h4>-->
            </div>
            <section class="wrapper clearfix main">
                <section id="sidebar" class="sidebar">
                    <ul>
                        <li style="margin-bottom: 25px;"><a href="<?php echo base_url(); ?>press"><img style="width:100px;" src="../../assets/img/press/TMF logo.jpg" /></a></li>
                        <li class="active" style="margin-bottom: 25px;"><a href="<?php echo base_url(); ?>common/AandH"><img style="width:100px;" src="../../assets/img/press/logo.png" /></a></li>
                        <li style="margin-bottom: 25px;"><a href="<?php echo base_url(); ?>common/fyg"><img style="width:100px;" src="../../assets/img/press/tfyg.jpg" /></a></li>
                    </ul>
                </section>
                <section class="content">
                    <div class="img-container">
                        <img style="width: 100%;" src="../../assets/img/press/A&H.jpg" />
                    </div>
                    <hr class="last"/>
                </section>    
            </section>   
        </section>
        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>

    </body>
</html>        

  
    

