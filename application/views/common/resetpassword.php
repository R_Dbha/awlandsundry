<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <head>
        <?php $this->load->view('common/template/begin'); ?>    

    	<body class="page login">
        <header id="header">
        	<section id="toolbar" class="clearfix">
            <div class="toolbarInner clearfix">
            	<div class="wrapper clearfix">
                    <div class="right"> 
                        <ul>
                            <li class="login active">
                                <a href="javascript:;">Log in</a>
                            </li> 
                        </ul>
                    </div>
                </div>
                </div>
            </section>
	        <h1 id="logo">
		        <a href="<?php echo base_url();?>">Awl&amp;Sundry</a>
	        </h1>
        </header>
  <section id="middle_container">              
        <section class="wrapper clearfix main existing_ulogin">
        	<div class="loginBoxWrap">
				<h4>Reset Password</h4>
								<!-- <form class="sign_in">
					<input type="text" name="username" placeholder="USER NAME" required="required" />
					<input type="text" name="password" placeholder="PASSWORD" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" />
					  
					<input class="button blue" type="submit" value="Submit" />
				</form>
				 -->
                                
				<form action="<?php echo base_url();?>common/resetPassword/<?php echo $token; ?>" method="post" name="awlsundry" class="sign_in" id="awlsundry">
                                    <input type="password" name="newPwd" class="" id="newPwd" placeholder="PASSWORD" value="">
                                    <input type="password" name="confirmPwd" class="" id="confirmPwd" placeholder="RE ENTER PASSWORD" value="">
                                    <input type="submit" name="submit" onClick="return validate();" id="submitbutton" class="button blue" value="Submit">
                                    <input type="hidden" name="hid_token" id="hid_userId" value="<?php echo $token?>">
                                </form>               
                                
                 <!--<h2 class="invalid invalid_uname"><span>* </span>
                                    </h2> -->
                <div class="invalid_errors">
                                                 
                </div>
                
                                                                 
				<a href="javascript:;" class="link forget_pword"></a> 
				
                                
                                <h4 class="orTxt"></h4>
                                
	        	<a href="<?php echo base_url();?>login" class="cancel_sign">Login</a>
        	</div>
        </section>
        
 </section>       
        <script type="text/javascript">
		$('input#username').keyup(function() {
			$('.incorrect_uname').hide();
			$('input#username').removeClass('invalid_box');
		});
                function validate(){
                    if($('#newPwd').val().trim() == ""){
                         alert('You must enter a password!!!');
                            return false;
                    }
                    if($('#confirmPwd').val().trim() == ""){
                         alert('You must confirm the password!!!');
                            return false;
                    }
                    if($('#confirmPwd').val() != $('#newPwd').val())
                        {
                            alert('Invalid confirm password!!!');
                            $('#confirmPwd').val("");
                            return false;
                        }
                    }
		</script>            
                
        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
    </body>
</html>        
   
