

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <?php $this->load->view('common/template/begin'); ?>
    <body class="page contact">
        <header id="header">
            <section id="toolbar" class="clearfix">	
                <div class="toolbarInner clearfix">
                    <div class="wrapper content">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Contact / faq</span>
                        </div>

                        <div class="right"> 
                            <ul id="headerNavLinks">

                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li> 
                                <?php $this->load->view('common/login_status'); ?> 
                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&Sundry</a>
            </h1>
            <div class="wrapper shopping_cart_pop clearfix">
                <section class="shopping_cart_mini"></section>
            </div>
            <div class="wrapper clearfix quote">
                <h3>In case you have any questions</h3>
                <div id="contact">
                    <div class="phone">
                        <p>Reach us by phone</p>
                        <span><?php echo $this->config->item('customer_care_number'); ?></span>
                    </div>
                    <div class="laptop">
                        <p>Reach us by email</p>
                        <a href="mailto:<?php echo $this->config->item('contact_email'); ?>"><?php echo $this->config->item('contact_email'); ?></a>
                    </div>
                </div>
            </div>
            <div id="contact_top">
                <div class="wrapper clearfix contact_top">
                    <nav id="faq">
                        <a  style="cursor: pointer;" onclick="faqScroll('#payment');" >PAYMENT</a>
                        <a  style="cursor: pointer;" onclick="faqScroll('#shipping');" >SHIPPING</a>
                        <a  style="cursor: pointer;" onclick="faqScroll('#sizing');" >SIZING</a>
                        <a  style="cursor: pointer;" onclick="faqScroll('#returns');" >EXCHANGES</a>
                        <a  style="cursor: pointer;" onclick="faqScroll('#product');" >PRODUCT</a>
                    </nav>
                </div>
            </div>

        </header>


        <section id="middle_container">
            <section id="main" class="wrapper clearfix">

                <section id="payment" class="faq">
                    <header><h3>Payment</h3></header>
                    <ul>
                        <li class="question" >What forms of payment do you accept?</li>
                        <li class="answer payment">We accept all major forms of credit and debit cards. This includes Visa, Mastercard, American Express and Discover. We also accept PayPal.</li>
                    </ul>	
                    <ul>
                        <li id="whatisincluded" class="question">What is included in the prices of the shoes?</li>
                        <li class="answer price" >We love our shoes so much we want them to last a long time and that's why we have included a shoe tree pair along with a shoe horn free of charge.</li>
                    </ul>			
                </section>

                <section id="shipping" class="faq">
                    <header><h3>Shipping</h3></header>
                    <ul>
                        <li class="question">Where do we ship our shoes?</li>
                        <li class="answer">Currently we only ship our shoes in the contiguous States. We are working very hard to open orders & shipping to other international geographies outside of the US.</li>
                    </ul>
                    <ul>
                        <li class="question">How long will it take for the shoes to arrive?</li>
                        <li class="answer">Your customized handcrafted shoes will be delivered to your doorsteps within 4 weeks from the date of the order.</li>
                    </ul>			
                    <ul>
                        <li class="question">Do we offer free shipping?</li>
                        <li class="answer"><span>Yes,</span> we provide free shipping for all of our orders.</li>
                    </ul>
                    <ul>
                        <li class="question">Do you accept international orders?</li>
                        <li class="answer">At present we are only accepting orders in the US. We are working at a great speed to open our products to other international geographies outside of the US. Please sign up to our subscription list to stay informed.</li>
                    </ul>

                </section>

                <section id="sizing" class="faq">
                    <header><h3>Sizing</h3></header>
                    <ul>
                        <li class="question">What sizes do we offer?</li>
                        <li class="answer">We offer US sizes 5 – 14.</li>
                    </ul>
                    <ul>
                        <li class="question">How do I know which size to select or that you’ll get the size right?</li>
                        <li class="answer">We would recommend you use your normal size, the size that you wear most often. You can also review your size using our <a href="<?php echo base_url(); ?>files/131123_A&S_size_guide.pdf" target="_blank">shoe guide</a>.</li>
                    </ul>			
                    <ul>
                        <li class="question">Do you have size conversion chart?</li>
                        <li class="answer"><span>Yes,</span> please see <a href="javascript:void(0);" onclick="return showSizeChart();">here</a> the international shoe conversion chart. You can also download the shoe size guide  <a href="<?php echo base_url(); ?>files/131123_A&S_size_guide.pdf" target="_blank">here</a>.
                            <div id="shape_selection_popup" class="top_slide_bg sizingChart shape_popup shape_selection_popup" style="display: none;">
                                <div class="topSliderInner clearfix">
                                    <div class="size_chartDetail">

                                        <div class="top_title"><span>INTERNATIONAL SIZE CONVERSIONS</span></div>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                            <tr class="sizefirstRow">
                                                <th scope="row"><div>US</div></th>
                                            <td><div>5</div></td>
                                            <td><div>5&frac12;</div></td>
                                            <td><div>6</div></td>
                                            <td><div>6&frac12;</div></td>
                                            <td><div>7</div></td>
                                            <td><div>7&frac12;</div></td>
                                            <td><div>8</div></td>
                                            <td><div>8&frac12;</div></td>
                                            <td><div>9</div></td>
                                            <td><div>9&frac12;</div></td>
                                            <td><div>10</div></td>
                                            <td><div>10&frac12;</div></td>
                                            <td><div>11</div></td>
                                            <td><div>11&frac12;</div></td>
                                            <td><div>12</div></td>
                                            <td><div>12&frac12;</div></td>
                                            <td><div>13</div></td>
                                            <td><div>13&frac12;</div></td>
                                            <td><div>14</div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><div>CAN</div></th>
                                            <td><div>5</div></td>
                                            <td><div>5&frac12;</div></td>
                                            <td><div>6</div></td>
                                            <td><div>6&frac12;</div></td>
                                            <td><div>7</div></td>
                                            <td><div>7&frac12;</div></td>
                                            <td><div>8</div></td>
                                            <td><div>8&frac12;</div></td>
                                            <td><div>9</div></td>
                                            <td><div>9&frac12;</div></td>
                                            <td><div>10</div></td>
                                            <td><div>10&frac12;</div></td>
                                            <td><div>11</div></td>
                                            <td><div>11&frac12;</div></td>
                                            <td><div>12</div></td>
                                            <td><div>12&frac12;</div></td>
                                            <td><div>13</div></td>
                                            <td><div>13&frac12;</div></td>
                                            <td><div>14</div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><div>UK</div></th>
                                            <td><div>4&frac12;</div></td>
                                            <td><div>5</div></td>
                                            <td><div>5&frac12;</div></td>
                                            <td><div>6</div></td>
                                            <td><div>6&frac12;</div></td>
                                            <td><div>7</div></td>
                                            <td><div>7&frac12;</div></td>
                                            <td><div>8</div></td>
                                            <td><div>8&frac12;</div></td>
                                            <td><div>9</div></td>
                                            <td><div>9&frac12;</div></td>
                                            <td><div>10</div></td>
                                            <td><div>10&frac12;</div></td>
                                            <td><div>11</div></td>
                                            <td><div>11&frac12;</div></td>
                                            <td><div>12</div></td>
                                            <td><div>12&frac12;</div></td>
                                            <td><div>13</div></td>
                                            <td><div>13&frac12;</div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><div>EU</div></th>
                                            <td><div>37</div></td>
                                            <td><div>37.5</div></td>
                                            <td><div>38</div></td>
                                            <td><div>38.5</div></td>
                                            <td><div>39</div></td>
                                            <td><div>39.5</div></td>
                                            <td><div>40</div></td>
                                            <td><div>40.5</div></td>
                                            <td><div>41</div></td>
                                            <td><div>41.5</div></td>
                                            <td><div>42</div></td>
                                            <td><div>42.5</div></td>
                                            <td><div>43</div></td>
                                            <td><div>43.5</div></td>
                                            <td><div>44</div></td>
                                            <td><div>44.5</div></td>
                                            <td><div>45</div></td>
                                            <td><div>45.5</div></td>
                                            <td><div>46</div></td>
                                            </tr>
                                            <tr>
                                                <th scope="row"><div>AUS</div></th>
                                            <td><div>4&frac12;</div></td>
                                            <td><div>5</div></td>
                                            <td><div>5&frac12;</div></td>
                                            <td><div>6</div></td>
                                            <td><div>6&frac12;</div></td>
                                            <td><div>7</div></td>
                                            <td><div>7&frac12;</div></td>
                                            <td><div>8</div></td>
                                            <td><div>8&frac12;</div></td>
                                            <td><div>9</div></td>
                                            <td><div>9&frac12;</div></td>
                                            <td><div>10</div></td>
                                            <td><div>10&frac12;</div></td>
                                            <td><div>11</div></td>
                                            <td><div>11&frac12;</div></td>
                                            <td><div>12</div></td>
                                            <td><div>12&frac12;</div></td>
                                            <td><div>13</div></td>
                                            <td><div>13&frac12;</div></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul>
                        <li class="question">Do you offer different sizes for each shoe?</li>
                        <li class="answer">Absolutely, we allow our customers to order different sizes for each shoe. Please provide the right size during the checkout process.</li>
                    </ul>			
                </section>

                <section id="returns" class="faq">
                    <header><h3>EXCHANGES</h3></header>
                    <ul>
                        <li class="question">What is our exchange policy?</li>
                        <li class="answer">We want our customers to be absolutely happy with their product. For some reason if they are unhappy, we will exchange your shoe pair with a different one as long as the shoe is in unworn conditions and we are informed about it within 30 days from the delivery date.  Unfortunately due to the nature of our business, we cannot provide refunds on our products.</li>
                    </ul>
                </section>

                <section id="product" class="faq">
                    <header><h3>Product</h3></header>
                    <ul>
                        <li class="question">How do we make our shoes?</li>
                        <li class="answer">All of our shoes are meticulously handcrafted by a group of experienced artisanal craftsman. Each shoe goes through hundreds of different steps from clicking, sewing, hand skiving, hand drafting, Toe lasting and many more. You can learn more about the process <a href="<?php echo base_url(); ?>our-artisanal-process">here</a></li>
                    </ul>
                    <ul>
                        <li class="question">What kind of leather types we offer?</li>
                        <li class="answer">We offer five different kinds of leather. They are calf skin plain, calf skin suede, calf skin grained, ostrich, and alligator.</li>
                    </ul>			
                    <ul>
                        <li class="question">How many shoe styles do we provide?</li>
                        <li class="answer">We provide four different shoe styles for each of our lasts. They are oxford, derby, monk strap, and loafer.</li> 
                    </ul> 
                    <ul>
                        <li class="question">What is the difference between Oxford, Derby, Monks and Loafer?</li>   
                        <li class="answer">
                            <span>The Oxford:</span>
                            The Oxford also know as Balmorals is to recognized by its closed lacing. The laces are threaded through five pairs of eyelets and fasten up the shoe so perfectly that only the upper edge of the tongue can be seen. This lends itself to a sleeker, more elegant appearance that takes your style up a notch and is the perfect finishing touch to a sharp tailored suit. This type of shoe looks particularly good on narrow feet with a low instep. <br/><br/><br/>

                            <span>The Derby:</span>
                            The Derby  is characterized by open lacing. It also known as "Bluchers" after the Prussian field marshal Gebhard Leberecht von Blücher, Duke of Wahlstady (1742 - 1819), who joined forces with Wellington to defeat Napoleon at the Battle of Waterloo in 1815 and ordered laced shoes of this type to be made for his soldiers. The Derby offers pure comfort to 
                            shoe-lovers who have a wide foot or an unusually high instep. The open lacing mean that it is easier to slip into this shoe than an Oxford, and the distance between the two quarters is easier to adjust. <br/><br/><br/>

                            <span>The Monk:</span>
                            The Monk (so called because it is reminiscent of the sandals worn by monks) is an independent style of shoe. It consists of a vamp and quarters, like the Oxford and the Derby, but differs from them markedly in that the two quarters are fastened together with a distinctive buckle. The advantage of the Monk is the simplicity of its buckle fastening. A Monk can be classically elegant or casual and relaxed. <br/><br/><br/>

                            <span>The Loafer:</span>
                            The Loafer also know as Slipper was originally introduced by Raymond Lewis Wildsmith for King George VI as a casual house shoe. The vamp, the quarters, and the tongue are made of a single piece of leather. They go perfectly with a casual look.

                        </li>                                                            
                    </ul>	
                    <ul>
                        <li class="question">What is a shoe last?</li>
                        <li class="answer">A shoe last is a wooden mold that gives the shoe its shape.</li>
                    </ul>			
                    <ul>
                        <li class="question">What is Goodyear welt process?</li>
                        <li class="answer">Goodyear welt is a shoe making process where the leather sole is stitched to the upper of the sole. This methodology is considered one of the best since it allows customers to re-sole their shoes. This flexibility increases the durability of the shoes. You can learn more about this <a href="<?php echo base_url(); ?>our-artisanal-process">here</a></li>
                    </ul>			
                </section>
            </section>
        </section>
        <script type="text/javascript">
                            function showSizeChart() {
                                $("#shape_selection_popup").slideDown(1000);
                                return false;
                            }
        </script>            

        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
    </body>
</html>        

