<?php
if ($shoedesigns != NULL) {
    foreach ($shoedesigns as $shoedesign) {
        ?>
        <div class="item purchased" data-id="<?php echo $shoedesign['shoe_design_id']; ?>" data-mode="<?php echo $shoedesign['mode']; ?>"  style="float:left">
            <div class="item_row_top">
                <!--<span class="filter-desc">your purchased item</span>--><!---->
                <span class="author">
                    <img src="<?php echo base_url(); ?>assets/img/icons/bird.png" />
                    <span>
                        by
                    </span>
                    <span class="name">
                        <?php echo $shoedesign['first_name']; ?>
                    </span>
                </span>
            </div>
            <img class="item-img" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoedesign['image_file'] . "_A0.png"; ?>" width="249">
            <img class="item-img selected" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoedesign['image_file'] . "_A7.png"; ?>" width="249">
            <div class="item_row_bottom">
                <p class="item_row_title">
                    <!--span class="item-title"><?php echo $shoedesign['mode']; ?>&nbsp;</span-->
                    <span class="item-title"><?php echo ucwords(strtolower($shoedesign['public_name'])); ?>&nbsp;</span>
                    <span class="item-subtitle"><?php echo $shoedesign['last_name']; ?> /<?php echo $shoedesign['style_name']; ?></span>
                </p>
                <?php if($shoedesign['LikeCount'] > 0 || $shoedesign['ReworkCount'] >0 || $shoedesign['FavoriteCount'] > 0) {?> 
                <p class="meta">

                    <?php if ($shoedesign['LikeCount'] == 1) { ?>
                        <span> <?php echo $shoedesign['LikeCount'] ?> Like</span>
                    <?php } else if ($shoedesign['LikeCount'] > 1) { ?>
                        <span><?php echo $shoedesign['LikeCount'] ?> Likes</span>
                    <?php } else { ?>
                        <span></span>
                    <?php } if ($shoedesign['ReworkCount'] == 1) { ?>
                        <span>1 Redesign</span>
                    <?php } if ($shoedesign['ReworkCount'] > 1) { ?>
                        <span><?php echo $shoedesign['ReworkCount']; ?> Redesigns</span>
                    <?php } else { ?>
                        <span></span>
                    <?php }
                    if ($shoedesign['FavoriteCount'] == 1) {
                        ?>
                        <span>1 Favorite </span>
                    <?php } if ($shoedesign['FavoriteCount'] > 1) { ?>
                        <span><?php echo $shoedesign['FavoriteCount']; ?> Favorites</span>
                    <?php } else { ?>
                        <span></span>
                    <?php } ?>
                    <?php if ($shoedesign['mode'] != "") {
                        if ($shoedesign['mode'] == "fav") {
                            ?>
                            <span>Favourite Shoe</span>
                        <?php } else if ($shoedesign['mode'] == "save") {
                            ?>
                            <span>Saved Shoe</span> 
                        <?php } else {
                            ?>
                            <span>Purchased Shoe</span> 
            <?php }
        } ?>

                </p>
                <?php } else {?>
                <p></p>
                <?php } ?>
            </div>
        </div>
        <?php
    }
}?>