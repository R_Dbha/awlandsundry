<h4>ORDER CART</h4>
<?php
$i = 1;
$angle = "_A1";
$exte = ".png";
$baseurl = ApplicationConfig::IMG_BASE;
if (sizeof($this->cart->contents()) > 0) {
    foreach ($this->cart->contents() as $items):
        if (strtolower($items['name']) == 'gift card') {
            ?>
            <div class="cart-item">
                <div class="ico-img">
                    <div class="card">
                    </div>
                </div>
                <div class="shoppingCartRightDet">
                    <span> Gift card</span><br/>                			    
                    QTY: <small><?php echo $items['qty']; ?></small>
                </div>
                <span class="price">Price:<span><?php echo $items['price']; ?></span></span>
                <div class="remove"><span class="ico"></span></div>
                <input type="hidden" class="cartId" value="<?php echo $items['rowid']; ?>" />
            </div>
            <?php
        } else {
            $options = $this->cart->product_options($items['rowid']);
            $shoe = json_decode($options['shoe'], TRUE);
            $lastStyle = $this->CustomShoeModel->get_last_style_details($shoe['style']['id'], $shoe['last']['id']);
            $imgBase = $shoe['style']['code'] . "_" . $shoe['toe']['type'] . $shoe['vamp']['type'] . $shoe['eyestay']['type'] . $shoe['foxing']['type'];
            $base = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['quarter']['matfolder'] . "/" . $shoe['style']['code'] . "_" . $shoe['quarter']['base'] . $angle . $exte;
            $toe = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['toe']['matfolder'] . "/" . $imgBase . $shoe['toe']['material'] . $shoe['toe']['color'] . $shoe['toe']['part'] . $angle . $exte;
            $eyestay = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['eyestay']['matfolder'] . "/" . $imgBase . $shoe['eyestay']['material'] . $shoe['eyestay']['color'] . $shoe['eyestay']['part'] . $angle . $exte;
            $foxing = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['foxing']['matfolder'] . "/" . $imgBase . $shoe['foxing']['material'] . $shoe['foxing']['color'] . $shoe['foxing']['part'] . $angle . $exte;
            $stitch = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['stitch']['folder'] . "/" . $imgBase . $shoe['stitch']['code'] . $angle . $exte;
            $lace = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['lace']['folder'] . "/" . $imgBase . $shoe['lace']['code'] . $angle . $exte;
            ?>
            <div class="cart-item">
                <div class="ico-img">
                    <img src="<?php echo $baseurl . $base; ?>" class="float-top" />
                    <img src="<?php echo $baseurl . $toe; ?>" class="float-top" />
                    <img src="<?php echo $baseurl . $eyestay; ?>" class="float-top" />
                    <img src="<?php echo $baseurl . $foxing; ?>" class="float-top" />
                    <img src="<?php echo $baseurl . $stitch; ?>" class="float-top" />
                    <img src="<?php echo $baseurl . $lace; ?>" class="float-top" />
                </div>
                <div class="shoppingCartRightDet">
                    <span>Last <?php echo $lastStyle['last_name'] ?></span><?php echo $lastStyle['style_name']; ?><br/>
                    Size:<small><?php echo $shoe['size']['left']['text'] . '/' . $shoe['size']['right']['text']; ?></small><br />				    
                    QTY: <small><?php echo $items['qty']; ?></small>
                </div>
                <span class="price">Price:<span><?php echo $items['price']; ?></span></span>
                <div class="remove"><span class="ico"></span></div>
                <input type="hidden" class="cartId" value="<?php echo $items['rowid']; ?>" />
            </div>
            <?php
        }
    endforeach;
    ?>
    <p class="total">Subtotal<span id="total"><?php echo $this->cart->format_number($this->cart->total()); ?></span></p>
    <a href="<?php echo base_url(); ?>custom-shoe/order" class="button">Checkout</a>
<?php } else { ?>
    <h2 class="emptyCart">Cart is Empty</h2>
    <?php
}?>