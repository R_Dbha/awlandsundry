<section class="createNewContainer" id="main">
    <div id="stepMain">
        <div class="wrapper clearfix">
            <ul class="createNewNav clearfix">
                <li class="createStep1"><a href="javascript:;" ><span>Style</span></a><div class="progressBar"></div></li>
                <li class="createStepLast"><a href="javascript:void(0);"  ><span>Last</span></a></li>
                <li class="createStep2"><a class="active" href="javascript:;" href="javascript:void(0);" class="no-bg"><span>Design features</span></a></li>
                <li class="createStep3"><a href="javascript:void(0);" class="no-bg"><span>Materials &amp; colors</span></a></li>
                <li class="createStep5"><a href="javascript:void(0);" class="no-bg"><span>Monogram</span></a></li>
                <li class="createStep4"><a href="javascript:void(0);" class="no-bg"><span>Sizing</span></a></li>
            </ul>
            <div class="stepFlowBar" id="stepFlowBar"></div>
            <div class="headline">
                <p><span>Step 3</span>Click on any part of the shoe to add design features</p>
            </div>
        </div>
    </div>
    <div class="stepContent wrapper clearfix">
        <div id="shape_visualization" class="shape_visualization">
            <!--<div class="save_d">Save your design</div>-->
            <div class="visualImgWrap">
                <img id="error" src="<?php echo ApplicationConfig::IMG_BASE ?>none.png" />
                <img id="base" src="" class="float-top" />
                <img id="toe" src="" class="float-top" />
                <img id="eyestay" src="" class="float-top" />
                <img id="foxing" src="" class="float-top" />
                <img id="vamp" src="" class="float-top" />
                <img id="lace" src="" class="float-top" />
                <img id="stitch" src="" class="float-top" />
                <div id="style_path"></div>
            </div>
            <div style="display:none" id="save-share">
                <a href="javascript:;" onclick="CustomShoe.saveAndShare();" class="save-share button blue">Save And Share</a>
            </div>
            <div id="last_name">
                <span><?php echo $model['last']['last_name'] . ' / ' . $model['style']['style_name']; ?></span>
            </div>
            <div id="vis_buttons">                    
                <a class="rotate left" href="javascript:;"></a>
                <a class="rotate right" href="javascript:;" style=""></a>
                <span class="topview_link">
                    <a class="top" href="javascript:;">Top view</a>
                </span>
            </div>
            <div id="price_div">
                <span class="symbol">$</span>
                <span class="price"></span>
                <span class="shipping">W / free shipping</span>
            </div>
        </div>
        <div id="design_main_links">
            <ul class="top" style="display:none;">
                <li class="save"><a href="javascript:;" onClick="messagepopup('save');"><span>Save your design</span></a></li>
                <li class="create"><a href="javascript:;" id="stepthreesubmit"><span>Select & continue...</span></a></li>
            </ul>
            <div class="action">
                <div class="default"></div>
                <div class="changable hidden save save_two"><p><strong>Save</strong> this design to your personal account and return to it later</p></div>
                <div class="changable hidden create select_two"><p><strong>Proceed</strong> to the next step to select the leather texture and colors</p></div>
            </div>
        </div>
    </div>
    <div id="shape_selection_popup" class="top_slide_bg toe_styling shape_selection_popup shape_popup">
        <div class="topSliderInner clearfix">
            <div class="topSliderLeft">
                <ul class="styleList" id="style_types">
                    <li>
                        <a id="design_toe" href="javascript:;"><span>Toe</span></a>
                    </li>
                    <?php
                    if ($model != NULL && $model['style']['isVampEnabled']) {
                        ?>
                        <li>
                            <a id="design_vamp" href="javascript:;"><span>Vamp</span></a>
                        </li>
                    <?php } ?>
                    <li>
                        <a id="design_eyestays" href="javascript:;"><span>Eyestays</span></a>
                    </li>
                    <li>
                        <a id="design_foxing" href="javascript:;"><span>Foxing</span></a>
                    </li>
                </ul>
            </div>
            <div class="topSliderRight">
                <ul id="toe_styling" class="jHcarousel"></ul>
            </div>
            <?php
            if ($model != NULL && $model['style']['isVampEnabled']) {
                ?>
                <div class="topSliderRight" style="display: none;">
                    <ul id="vamp_styling" class="img-style jHcarousel"></ul>
                </div>
            <?php } ?>
            <div class="topSliderRight" style="display: none;">
                <ul id="eyestays_styling" class="img-style jHcarousel"></ul>
            </div>
            <div class="topSliderRight" style="display: none;">
                <ul id="foxing_styling" class="img-style jHcarousel"></ul>
            </div>
        </div>
        <div class="popup_pattern_bar"><a class="shape_popup_close" href="javascript:;" onClick="shape_popup_close();">&nbsp;</a></div>   
    </div>
</section>
<div id="popupOverlay"></div>
<!-- previous step popups begins-->
<div class="messageBox messageBoxRed messageBox_step1">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div> 
</div>
<div class="messageBox messageBoxGreen messageBox_save">
    <div class="msgTop"><div class="msgHead">You must be signed in to do that.</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Saving a shoe to your scrapbook requires an account.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>login">Sign In</a></div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>signup">Create an Account</a></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">Not now,</a> <span>do not save.</span></div>
    </div> 
</div>
<div class="messageBox messageBoxRed messageBox_step_last">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-last">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div> 
</div>
<div id="popupOverlay"></div>
<div id="accounts_selection_popup" class="popup"></div>

<div class="messageBox messageBoxRed messageBox_revise">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design and take you back to step 1</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a>
            <span>start over.</span>
        </div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>login" id="savedesign">Yes,</a>
            <span>but first save my current design to my account page (account required).</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and return to my current design.</span>
        </div>
    </div>
</div>


<!-- previous step popups ends-->
<div class="controls">
    <a id="prev" class="control" href="javascript:;" ></a>
    <a id="next" class="control" href="javascript:;" ></a>
</div>
<!--<style>
    .jcarousel-item img {
        width: 100%;
    }
    #shape_visualization .visualImgWrap img.float-top {
        position: absolute;
        top: 0;
        left: 0;
        margin-top: 60px;
    }
</style>-->
<?php $this->load->view('common/template/jscontainer'); ?>
<script type="text/javascript">
    function shape_popup_close() {
        $('.designStep .shape_popup').animate({height: "0"}, 1500, function() {
            $('.designStep .shape_popup').hide();
            $('.designStep .shape_popup').removeClass("popup_alive");
            //$('.designStep #stepMain').css({'position':'fixed','top':'50'});
        });
    }
    $(document).ready(function() {
        CustomShoe.stepTwo.init(<?php echo json_encode($model); ?>);
        $('#next').off('click touchend').on('click touchend', function() {
            CustomShoe.stepTwo.nextStep();
        });
        $('#prev').off('click touchend').on('click touchend', function() {
            messagepopup('step_last');
        });
    });
</script>