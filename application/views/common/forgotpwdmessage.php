<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <?php $this->load->view('common/template/begin'); ?>
    <body class="page login"> 
        <header id="header">
            <section id="toolbar">
                <div class="toolbarInner clearfix">	
                    <div class="wrapper content clearfix">
                        <div class="left">

                        </div>

                        <div class="right"> 
                            <!--<ul id="headerNavLinks">-->
                            <ul>
                                <li class="login active">
                                    <a href="<?php echo base_url(); ?>login">Log in</a>
                                </li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
        </header>
        <section id="middle_container">              
            <section class="wrapper clearfix main">
                <div class="welcome">
                    <h1></h1>
                    <h3>TO RESET THE PASSWORD <BR />
                        PLEASE CHECK YOUR EMAIL FOR VERIFICATION LINK </h3>

                    <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style" class="button blue">Start Creating</a>
                </div>
            </section>  


            <div id="popupOverlay"></div>
            <div class="messageBox messageBoxGreen messageBox_mail">
                <div class="msgTop"><div class="msgHead">Please check your email</div></div>
                <div class="msgBottom">
                    <div class="msgDesc">Please check your email for our welcome message, locate the 5-digit confirmation code and paste that number here to continue.</div>
                    <div class="codeboxwrap"><input type="text" value="" placeholder="enter code here" id="digitcode" name="digitcode"></div>
                    <div class="msgOption"><a href="javascript:;">Submit and activate my account</a></div>
                    <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">Not now,</a> <span>close window and finish creating my accont later.</span></div>
                </div> 
            </div>

        </section>    


        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
    </body>
</html>