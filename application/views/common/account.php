<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('common/template/begin'); ?>
    <?php $this->load->view('common/template/jscontainer'); ?>
    <body class="page accounts">
        <header id="header" class="headerTop">
            <section id="toolbar" class="toolbarScroll">
                <div class="toolbarInner clearfix"> 
                    <div class="wrapper content clearfix">
                        <div class="left">
                            <a >
                                <span>Free shipping &amp; 30 Day Exchange</span>
                            </a>
                        </div>
                        <div class="right"> 
                            <ul id="headerNavLinks">
                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li> 
                                <li class="login">
                                    <a href="<?php echo base_url(); ?>logout" title="Logout" >Logout
                                    </a>
                                </li>


                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:;">Cart 
                                        <span class="number" id="cartcount">&nbsp;</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <h1 id="logo">
                    <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
                </h1>
                <div class="wrapper shopping_cart_pop clearfix">
                    <section class="shopping_cart_mini">
                        <h4>ORDER CART</h4>
                        <h2 class="emptyCart">Cart is Empty.</h2>
                    </section>
                </div>
            </section>
        </header>



        <section id="middle_container">              
            <section id="main" class="page wrapper clearfix">
                <div id="main_slider" class="clearfix">
                    <div class="camera_wra camera_emboss pattern_1" id="large-image">
                        <div data-src="https://www.awlandsundry.com/images/slider/slider3.jpg" data-fx="scrollHorz" rel="partial">
                            <div class="camera_caption ">
                                <div class="hs-image-container">
                                    <a class="hs-image-page-link" href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">&nbsp;</a>
                                    <div class="part">
                                        CREATE AN <br>ORIGINAL PAIR
                                    </div>
                                    <div class="part sub" style="margin-top:6px;">COLLABORATE WITH US AND<br/>
                                        LET&rsquo;S DESIGN A CUSTOM SHOE<br/>
                                        THAT&rsquo;S TAILOR-MADE FOR YOU
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-src="https://www.awlandsundry.com/images/image6.jpg" data-fx="scrollHorz" rel="partial">
                            <div class="camera_caption ">
                                <div class="hs-image-container">
                                    <a class="hs-image-page-link" href="<?php echo base_url(); ?>our-artisanal-process">&nbsp;</a>
                                    <div class="part">
                                        GOODYEAR WELT<br/>
                                        CONSTRUCTION
                                    </div>
                                    <div class="part sub" style="margin-top:6px;">MACHINES? NONSENSE! <br/>
                                        THIS IS THE PROCESS<br/>
                                        BY WHICH A PROPER MEN&rsquo;S <br/>
                                        DRESS SHOE IS BUILT
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-src="https://www.awlandsundry.com/images/slider/slider1.jpg" data-fx="scrollHorz" rel="partial">
                            <div class="camera_caption ">
                                <div class="hs-image-container">
                                    <a class="hs-image-page-link" href="<?php echo base_url(); ?>get-inspired">&nbsp;</a>
                                    <div class="part">
                                        GET INSPIRED<br/>
                                    </div>
                                    <div class="part sub" style="margin-top:6px;">UNORIGINAL? THAT&rsquo;S OK TOO.<br/>
                                        YOU&rsquo;LL BE ABLE TO PICK FROM <br/>
                                        THE MOST POPULAR<br/>
                                        COLLABORATIONS
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div data-src="https://www.awlandsundry.com/assets/img/gallery/slider3.jpg" data-fx="scrollHorz" rel="partial">
                            <div class="camera_caption ">
                                <div class="hs-image-container">
                                    <a class="hs-image-page-link" href="<?php echo base_url(); ?>faq#whatisincluded/price">&nbsp;</a>
                                    <div class="part">
                                        ALL RELIGIONS, ARTS AND SCIENCES <br/>
                                        ARE BRANCHES OF THE SAME TREE.<br/>

                                    </div>
                                    <div class="part sub" style="margin-top:6px;">
                                        THAT&rsquo;S WHY EVERY PAIR COMES <br>
                                        WITH TREES... AND HORNS. <br>
                                        WE GIVE YOU THE TOTAL PACKAGE
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="account" class="wrapper clearfix">
                    <?php $user = $this->session->userdata('user_details'); ?>
                    <h3>Welcome <?php echo $user['first_name']; ?></h3>
                </div>

            </section>

            <section id="secondary" class="page wrapper clearfix">

                <div id="links" class="wrapper clearfix">
                    <ul class="top clearfix">
                        <li class="account"><a href="javascript:;"><span>Your account info</span></a></li>
                        <li class="inspire"><a href="<?php echo base_url(); ?>get-inspired"><span>Get Inspired</span></a></li>
                        <li class="create"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style"><span>Create a custom shoe</span></a></li>
                        <li class="giftcard"><a href="javascript:;" ><span>Gift Card</span></a></li>
                    </ul>
                </div>
                <div id="account_content" class="wrapper clearfix">
                    <div class="action">
                        <div class="default clearfix account">
                            <div class="youHaveLeft">
                                <span>You have :</span>
                            </div>
                            <div class="youHaveRight">
                                <ul class="myaccLinks clearfix">
                                    <li>
                                        <a class="order order_processing" href="javascript:;"  data-id="order_processing" >
                                            <span class="number" id="ordered"><?php echo @$ordered; ?></span> 
                                            order processing
                                        </a>
                                    </li>
                                    <li>
                                        <a class="order purchases" href="javascript:;"  data-id="order_processing" >
                                            <span class="number" id="purchased"><?php echo @$purchased; ?></span>
                                            purchases
                                        </a>
                                    </li>
                                    <li>
                                        <a class="order returns" href="javascript:;"  data-id="order_processing" >
                                            <span class="number"><?php echo @$returns; ?></span>
                                            EXCHANGES
                                        </a>
                                    </li>
                                    <li class="b_crumb">
                                        <a class="acc_settings" href="javascript:;" data-id="acc_settings" >
                                            ACCOUNT SETTINGS
                                        </a>
                                    </li> 
                                    <li class="b_crumb clearfix checkOrderFormWrap">
                                        <p>check Order status</p>
                                        <div class="checkOrderForm">
                                            <input name="check_order_number" id="check_order_number" onKeyDown="if (event.keyCode == 13)
                                                        check_order_submit();" type="text" />
                                            <!--<input type="submit" value="submit" />-->
                                            <a href="javascript:;" id="check_order_submit" class="check_order_submit" onClick="check_order_submit();">&nbsp;</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="changable hidden giftcard">

                            <div class="">
                                <ul class="myaccLinks clearfix">
                                    <li>
                                        <div id="giftcard_details">
                                            <?php
                                            if ($giftcards) {
                                                ?>
                                                <div>Card Number</div>
                                                <div>Price</div>
                                                <div>Used</div>

                                                <?php
                                                foreach ($giftcards as $giftcard) {
                                                    ?>
                                                    <div><?php echo $giftcard['card_number']; ?></div>
                                                    <div>$ <?php echo $giftcard['amount']; ?></div>
                                                    <div>$ <?php echo $giftcard['balance']; ?></div>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </li>
                                    <li>
                                        <div id="gift_some_one" >
                                            Want to gift someone? 
                                            <a  href="<?php echo base_url(); ?>gift-card"   >Click here</a>
                                            to purchase a gift card</div>
                                    </li>


                                </ul>
                            </div>
                        </div>
                        <div class="changable hidden inspire"><p><strong>view</strong> the shoe creations of others, save them for inspiration, buy them or rework them with your own ideas</p></div>
                        <div class="changable hidden create"><p><strong>choose</strong> a basic style, then specify design details, broguing patterns, leathers, colors, stitching and lacing</p></div>
                    </div>
                </div>

                <div id="giftcard_detail" class="sliderContent giftcardDetails_slider wrapper clearfix" style="display:none;">
                    <a class="cancel_sign" href="javascript:;" onClick="accountSliderClose();">&nbsp;</a>
                    <div class="sliderContentInner giftcardDetails_sliderInner" style="display:none;">

                        <div class="accDetHead"><span class="number" >Card </span> Value</div>
                        <div class="accDetHead"><span class="number" >Card </span> Value</div>
                    </div>
                </div>

                <div id="order_processing_detail" class="sliderContent accDet_slider wrapper clearfix" style="display:none;">
                    <a class="cancel_sign" href="javascript:;" onClick="accountSliderClose();">&nbsp;</a>
                    <div class="sliderContentInner accDet_sliderInner" style="display:none;">

                        <div class="accDetHead">
                            <span class="number" id="order_processing"><?php echo @$ordered; ?></span>
                            order processing
                            <div <?php if ($ordered == 0) echo "style='display:none;'" ?> >
                                <?php
                                if ($ordered !== 0)
                                    foreach ($ordered_items as $item) {
                                        ?>
                                        <span class="small">#<?php echo @$item['invoice_no']; ?></span>
                                        <span class="small bold"><?php echo @$item['public_name']; ?></span>
                                        <span class="medium">last : <?php echo @$item['last']; ?></span>
                                        <span class="small" >style : <?php echo @$item['style_name']; ?></span>
                                        <span class="large">submitted : <?php echo date('m/d/Y', strtotime($item['order_date'])); ?></span>
                                        <span class="large">est.delivery : <?php
                                            echo date('m/d/Y', strtotime(date("Y-m-d", strtotime($item['order_date'])) . " +2 week"));
                                            ?></span>
                                        <span class="large">status : 
                                            <span class="rose">
                                                <?php if ($item['status'] == 'Order acknowledged')
                                                    echo 'acknowledged';
                                                else
                                                    echo 'in progress';
                                                ?>
                                            </span>
                                        </span>
                                        <div></div>
        <?php
    }
?>

                            </div>
                        </div>
                        <div class="accDetHead" id="orderedDetails"></div>
                        <div class="accDetHead">
                            <span class="number" id="order_purchased">
                                <?php echo @$purchased; ?></span>
                            purchases
                            <div <?php if ($purchased == 0) echo "style='display:none;'" ?> >
                                <?php
                                if ($purchased !== 0)
                                    foreach ($purchased_items as $item) {
                                        ?>
                                        <span class="small">#<?php echo @$item['invoice_no']; ?></span>
                                        <span class="small bold"><?php echo @$item['public_name']; ?></span>
                                        <span class="medium">last : <?php echo @$item['last']; ?></span>
                                        <span class="small" >style : <?php echo @$item['style_name']; ?></span>
                                        <span class="large">submitted : <?php echo date('m/d/Y', strtotime($item['order_date'])); ?></span>
                                        <span class="large" >shipped : 
                                        <?php echo date('m/d/Y', strtotime($item['shipped_date'])); ?>
                                        </span>
                                        <span class="large">status : <span class="blue">delivered</span></span>
                                        <div></div>
        <?php
    }
?>

                            </div>
                        </div>
                        <div class="accDetHead" id="purchasedDetails"></div>
                        <div class="accDetHead"><span class="number"><?php echo @$returns; ?></span> Exchanges</div>
                        <p class="accDetHead">Please contact our customer service to exchange your product. You can reach them at <?php echo $this->config->item('contact_email'); ?> 
                            or <?php echo $this->config->item('customer_care_number'); ?>.</p>
                    </div>
                </div>


                <div id="account_settings_detail" class="sliderContent accSetting_slider wrapper clearfix" style="display:none;">
                    <a class="cancel_sign" href="javascript:;" onClick="accountSliderClose();">&nbsp;</a>
                    <div class="sliderContentInner accSetting_sliderInner clearfix" style="display:none;">

                        <div class="acc_cre account_credentials" id="account_credentials">
                            <h2><?php echo $user['first_name']; ?></h2>
                            <div class="userInfo_line">EMAIL:
                                <input type="text" disabled="disabled" name="acc_email" id="acc_email" value="<?php echo $user['email']; ?>" readonly />
                                <input type="hidden" disabled="disabled" name="hid_acc_email" id="hid_acc_email" value="<?php echo $user['email']; ?>" readonly />
                                <a class="change_email" id="change_email" href="javascript:;" onClick="change_userInfo('email');">[CHANGE]</a>
                            </div>

                            <div class="userInfo_line">PASSWORD:
                                <input type="password" disabled="disabled" name="acc_password" id="acc_password" value= "" readonly />
                                <!--<a href="javascript:;" onClick="showpassword();" id="show_password">{SHOW}</a> -->
                                <a class="change_password" id="change_password" href="javascript:;" onClick="change_userInfo('password');">[CHANGE]</a>
                            </div>

                            <div id="errorInfo" class=""></div>


                        </div>    


                        <div class="acc_cre billing_credentials" id="billing_credentials">
                            <h2>BILLING ADDRESS</h2>
                            <div class="addr_line" id ="bil_name"><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></div>
                            <div class="addr_line" id="bil_addr1"></div>
                            <div class="addr_line" id="bil_addr2"></div>
                            <div class="addr_line" id="bil_city"></div>
                            <div class="addr_line" id="bil_state"></div>
                            <div class="addr_line" id="bil_zip"></div>
                            <div class="edit_addr_wrap"><a class="edit_addr" id="edit_billing_addr" href="javascript:;" onClick="edit_address('billing');"><span>Edit</span></a></div>

                        </div>
                        <input type ="hidden" id="hid_billing_id" />
                        <input type ="hidden" id="hid_shipping_id" />
                        <input type ="hidden" id="hid_user_id" value="<?php echo $user['user_id']; ?>" />

                        <div class="acc_cre shipping_credentials" id="shipping_credentials">
                            <h2>SHIPPING ADDRESS</h2> <a class="sameBillingAddr" id="sameBillingAddr" href="javascript:;">SAME AS BILLING</a>
                            <div class="addr_line" id="shipping_name"><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></div>
                            <div class="addr_line" id="shipping_address1"></div>
                            <div class="addr_line" id="shipping_address2"></div>
                            <div class="addr_line"  id="shipping_city"> </div>                       <div class="addr_line"  id="shipping_state"> </div>
                            <div class="addr_line"  id="shipping_zipcode"> </div>
                            <div class="addr_line"  id="shipping_phone"> </div>

                            <div class="edit_addr_wrap"><a class="edit_addr" id="edit_shipping_addr" href="javascript:;" onClick="edit_address('shipping');"><span>Edit</span></a></div>
                        </div> 


                        <div class="edit_address_slider" id="billing_credentials_slider">
                            <div class="edit_address_slider_inner">
                                <div>Billing Address</div>
                                <div class="edit_address_fields">
                                    <input type="text" name="edit_firstname" id="edit_firstname" placeholder="firstname" value="<?php echo $user['first_name']; ?>" />
                                    <input type="text" name="edit_lastname" id="edit_lastname" placeholder="lastname" value="<?php echo $user['last_name']; ?>" />
                                    <input type="text" name="edit_address1" id="edit_address1" placeholder="address1" value="" />
                                    <input type="text" name="edit_address2" id="edit_address2" placeholder="address2" value="" />
                                    <input type="text" name="edit_city" id="edit_city" placeholder="city" value="" />
                                    <input type="text" name="edit_state" id="edit_state" placeholder="state" value="" />
                                    <input type="text" name="edit_zipcode" id="edit_zipcode" placeholder="zipcode" value="0" />
                                </div>
                                <div class="edit_addr_slider_btn clearfix">
                                    <div class="edit_addr_slider_btn_wrap">
                                        <a class="save_addr" href="javascript:;" onClick="save_address('billing');"><span>Save</span></a>
                                    </div>
                                    <div class="edit_addr_slider_btn_wrap">
                                        <a class="cancel_addr" id="cancel_shipping_addr" href="javascript:;" onClick="cancel_address('billing');"><span>Cancel</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="edit_address_slider" id="shipping_credentials_slider">
                            <div class="edit_address_slider_inner">
                                <div>Shipping Address</div>
                                <div class="edit_address_fields">
                                    <input type="text" name="edit_firstname" id="edit_sfirstname" placeholder="firstname" value="Aniyan" />
                                    <input type="text" name="edit_lastname" id="edit_slastname" placeholder="lastname"  value="P N" />
                                    <input type="text" name="edit_address1" id="edit_saddress1" placeholder="address1" value="" />
                                    <input type="text" name="edit_address2" id="edit_saddress2" placeholder="address2" value="" />
                                    <input type="text" name="edit_city" id="edit_scity" placeholder="city" value="" />
                                    <input type="text" name="edit_state" id="edit_sstate" placeholder="state" value="" />
                                    <input type="text" name="edit_zipcode" id="edit_szipcode" placeholder="zipcode" value="0" />
                                    <input type="text" name="edit_phone" id="edit_phone" placeholder="phone number" value="" />
                                </div>
                                <div class="edit_addr_slider_btn clearfix">
                                    <div class="edit_addr_slider_btn_wrap">
                                        <a class="save_addr" href="javascript:;" onClick="save_address('shipping');"><span>Save</span></a>
                                    </div>
                                    <div class="edit_addr_slider_btn_wrap">
                                        <a class="cancel_addr" id="cancel_shipping_addr" href="javascript:;" onClick="cancel_address('shipping');"><span>Cancel</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>

                <div id="balance_detail" class="sliderContent balance_slider wrapper clearfix" style="display:none;">
                    <a class="cancel_sign" href="javascript:;" onClick="accountSliderClose();">&nbsp;</a>
                    <div class="sliderContentInner balance_sliderInner" style="display:none;">
                        <div id="balanceContent">

                        </div>
                    </div>
                </div>

                <div id="check_order_detail" class="sliderContent checkOrder_slider wrapper clearfix" style="display:none;">
                    <a class="cancel_sign" href="javascript:;" onClick="accountSliderClose();">&nbsp;</a>
                    <div class="sliderContentInner checkOrder_sliderInner" style="display:none;">
                        <div id="checkOrderContent">
                            <div class="accDetHead">YOUR ORDER # 76896K</div>
                            <ul class="accDetList">
                                <li>
                                    <span class="acc_order_no"># 76896K</span>
                                    <span class="acc_order_name"><big>pimptastic</big></span>
                                    <span class="acc_order_last">LAST: MARTIN</span>
                                    <span class="acc_order_style">STYLE: MONKSTRAP</span>
                                    <span class="acc_order_submitted">SUBMITTED: 01 / 20 / 14</span>
                                    <span class="acc_order_delivery acc_order_delivery_est">EST. DELIVERY: 02/16/14</span>
                                    <span class="acc_order_status">STATUS: <big>IN PRODUCTION</big></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div id="scrapbook-tab" class="wrapper clearfix">
                    <ul class="designsList clearfix">
                        <!--<li class="featured_design_tab"><a href="javascript:;" onClick="getdesigns('88', 'featured', 'true', 'true', 'true');"><span>Featured designs</span></a></li>-->
                        <li class="my_design_tab"><a href="javascript:;" ><span>My designs</span></a></li>
                    </ul>

                    <div id="scrapbook-filter">
                        <span>Show:</span>
                        <form>              
                            <input type="hidden" id="designtype" name="designtype" value="" />  
                            <input type="checkbox" name="purchases" checked="checked" id="purchased" data-sdb-image="url('../../assets/img/icons/checkbox_blue.png')"/>
                            <label for="purchase">Purchases</label> 
                            <input type="checkbox" checked="checked" name="saved_designs" id="saved" data-sdb-image="url('../../assets/img/icons/checkbox_green.png')" />
                            <label for="purchase">Saved Designs</label>
                            <input type="checkbox" checked="checked" name="favorites" id="favorited" data-sdb-image="url('../../assets/img/icons/checkbox_purple.png')" /> 
                            <label for="purchase">Favorites</label>
                        </form>
                    </div>

                </div>

            </section>
            <!--From here-->
            <section id="content" class="clearfix">
                <div class="row clearfix" style="background-position-y: 0;">
                    <div class="row_wrapper clearfix" id="account_designs">

                    </div>
                </div>
            </section>
            <section id="content" class="clearfix"></section>       
            <section id="content" class="clearfix"><div class="row clearfix">
                    <div class="row_wrapper clearfix">




                        <div id="popupOverlay"></div>
                        <div id="accounts_selection_popup" class="popup"></div>
                        </section>  
                        </section>              
                        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.screwdefaultbuttonsV2.min.js"></script>
                        <script type="text/javascript">


                                                jQuery('#large-image').camera({
                                                    height: 'auto',
                                                    loader: 'none',
                                                    pagination: true,
                                                    thumbnails: false,
                                                    hover: false,
                                                    navigation: false,
                                                    playPause: false,
                                                    pauseOnClick: false,
                                                    overlayer: false,
                                                    opacityOnGrid: false,
                                                    imagePath: 'https://www.awlandsundry.com/images',
                                                    partialDelay: 1500,
                                                    time: 4000
                                                });
                                                function edit_address(type) {
                                                    fillEditAddress(type);
                                                    $('.edit_address_slider').show();
                                                    $('#edit_' + type + '_addr').addClass('active');
                                                    $('#' + type + '_credentials_slider').animate({"top": "0"}, 500, function() {
                                                        $('#edit_' + type + '_addr span').text("Save");
                                                        var onclickfn = 'save_address("' + type + '")';
                                                        $('#edit_' + type + '_addr').attr("onclick", onclickfn);

                                                    });

                                                }
                                                function fillEditAddress(type)
                                                {
                                                    if (type === 'billing') {
                                                        //$('#edit_firstname').val($('#'));
                                                        //$lastname = $('#edit_lastname').val();
                                                        $('#edit_address1').val($('#bil_addr1').text());
                                                        $('#edit_address2').val($('#bil_addr2').text());
                                                        $('#edit_city').val($('#bil_city').text());
                                                        $('#edit_state').val($('#bil_state').text());
                                                        $('#edit_zipcode').val($('#bil_zip').text());
                                                    } else if (type === 'shipping') {
                                                        $('#edit_sfirstname').val($('#shipping_name').text().split(' ')[0]);
                                                        $('#edit_slastname').val($('#shipping_name').text().split(' ')[1]);
                                                        $('#edit_saddress1').val($('#shipping_address1').text());
                                                        $('#edit_saddress2').val($('#shipping_address2').text());
                                                        $('#edit_scity').val($('#shipping_city').text());
                                                        $('#edit_sstate').val($('#shipping_state').text());
                                                        $('#edit_szipcode').val($('#shipping_zipcode').text());
                                                        $('#edit_phone').val($('#shipping_phone').text());
                                                    }
                                                }
                                                function cancel_address(type)
                                                {
                                                    $('#edit_' + type + '_addr').removeClass('active');
                                                    $('#' + type + '_credentials_slider').animate({"top": "-300px"}, 500, function() {
                                                        $('#edit_' + type + '_addr span').text("Edit");
                                                        var onclickfn = 'edit_address("' + type + '")';
                                                        $('#edit_' + type + '_addr').attr("onclick", onclickfn);
                                                        $('.edit_address_slider').hide();
                                                    });
                                                    $check = $(".sameBillingAddr.checked").length;
                                                    if ($check === 1) {
                                                        $(".sameBillingAddr").prop("checked", false);
                                                        $(".sameBillingAddr").removeClass("checked");
                                                    }
                                                }

                                                function save_address(type) {
                                                    $('#edit_' + type + '_addr').removeClass('active');
                                                    if (type === 'billing') {
                                                        $firstname = $('#edit_firstname').val();
                                                        $lastname = $('#edit_lastname').val();
                                                        $address1 = $('#edit_address1').val();
                                                        $address2 = $('#edit_address2').val();
                                                        $city = $('#edit_city').val();
                                                        $state = $('#edit_state').val();
                                                        $zipcode = $('#edit_zipcode').val();
                                                    } else if (type === 'shipping') {
                                                        $firstname = $('#edit_sfirstname').val();
                                                        $lastname = $('#edit_slastname').val();
                                                        $address1 = $('#edit_saddress1').val();
                                                        $address2 = $('#edit_saddress2').val();
                                                        $city = $('#edit_scity').val();
                                                        $state = $('#edit_sstate').val();
                                                        $zipcode = $('#edit_szipcode').val();
                                                        $phone = $('#edit_phone').val();
                                                    }
                                                    if ($firstname.trim() == "" || $lastname.trim() == "") {
                                                        alert("First Name & Last Name should't be blank.");
                                                        return;
                                                    }
                                                    $('#' + type + '_credentials_slider').animate({"top": "-300px"}, 500, function() {
                                                        $('#edit_' + type + '_addr span').text("Edit");
                                                        var onclickfn = 'edit_address("' + type + '")';
                                                        $('#edit_' + type + '_addr').attr("onclick", onclickfn);
                                                    });
                                                    var updateUrl;
                                                    var userid;
                                                    var updatingData = {};
                                                    if (type === "billing")
                                                    {
                                                        //alert('ok');
                                                        updateUrl = "<?php echo base_url(); ?>common/updateprimarybillingdata";
<?php $userid = $user['user_id']; ?>;
                                                        userId = <?php echo $userid; ?>;
                                                        updatingData = {
                                                            fname: $firstname,
                                                            lname: $lastname,
                                                            address1: $address1,
                                                            address2: $address2,
                                                            city: $city,
                                                            state: $state,
                                                            zipcode: $zipcode,
                                                            type: type,
                                                            user_id: userId
                                                        };

                                                    }
                                                    else if (type === "shipping")
                                                    {
                                                        //alert('ok');
                                                        updateUrl = "<?php echo base_url(); ?>common/updateprimaryshippingdata";
<?php $userid = $user['user_id']; ?>;
                                                        userId = <?php echo $userid; ?>;
                                                        updatingData = {
                                                            firstname: $firstname,
                                                            lastname: $lastname,
                                                            address1: $address1,
                                                            address2: $address2,
                                                            city: $city,
                                                            state: $state,
                                                            zipcode: $zipcode,
                                                            phone: $phone,
                                                            type: type,
                                                            user_id: userId
                                                        };

                                                    }
                                                    $.ajax({
                                                        type: "POST",
                                                        url: updateUrl,
                                                        dataType: "json",
                                                        data: updatingData,
                                                        success: function(msg) {
                                                            $check = $(".sameBillingAddr.checked").length;
                                                            if ($check === 1) {
                                                                $(".sameBillingAddr").prop("checked", false);
                                                                $(".sameBillingAddr").removeClass("checked");
                                                            }
                                                            fillaccountDetail();
                                                        }
                                                    });
                                                }

                                                function check_giftcard_balance() {
                                                    alert('vdf');
                                                    card_number = $('#giftcard_number').val();
                                                    $('.balance_slider').show();
                                                    $('.balance_slider .balance_sliderInner').slideDown();
                                                    $('#balanceContent').html('');
                                                    $('#balanceContent').html('balance is 64');
                                                }
                                                function check_order_submit() {
                                                    $order_number = $('#check_order_number').val();
<?php $userid = $user['user_id']; ?>;
                                                    var userId = <?php echo $userid; ?>;
                                                    if ($order_number != '') {
                                                        $('.checkOrderForm input#check_order_number').addClass('active');
                                                        //$('#check_order_number').val("");
                                                        $('.myaccLinks li a').removeClass('active');
                                                        $('.accDet_slider').slideUp();
                                                        $('.accDet_slider .accDet_sliderInner').slideUp();
                                                        $('.accSetting_slider').slideUp();
                                                        $('.accSetting_slider .accSetting_sliderInner').slideUp();

                                                        var slideDivDisplay = $('.checkOrder_slider').css('display');

                                                        $.ajax({
                                                            type: "POST",
                                                            url: "<?php echo base_url(); ?>common/searchorder",
                                                            dataType: "json",
                                                            data: {
                                                                userid: userId,
                                                                refno: $order_number
                                                            },
                                                            success: function(msg) {
                                                                if (slideDivDisplay == 'none') {
                                                                    $('.checkOrder_slider').show();
                                                                    $('.checkOrder_slider .checkOrder_sliderInner').slideDown();

                                                                } else {
                                                                    $('.myaccLinks li a').removeClass('active');
                                                                    $('.checkOrder_slider').slideDown();
                                                                    $('.checkOrder_slider .checkOrder_sliderInner').slideDown();
                                                                }
                                                                var message = '';
                                                                if (msg.status !== '') {
                                                                    message = 'Invoice no: ' + msg.invoice_no + ', Status: ' + msg.status;
                                                                    +', Descriptiion : ' + msg.status_desc;

                                                                } else {
                                                                    message = "There is no order with the given order id";
                                                                }
                                                                $('#checkOrderContent').html(message);
                                                                //item_popup_detail(designid);
                                                            }
                                                        });


                                                    }
                                                }


                                                jQuery(function() {
                                                    jQuery('#scrapbook-filter input:checkbox').screwDefaultButtons({
                                                        image: 'url("' + baseHosturl + '/img/icons/checkbox_blue.png")',
                                                        width: 9,
                                                        height: 9
                                                    });
                                                });

                                                function accountSliderClose() {
                                                    $('.sliderContent').slideUp();
                                                    $('.sliderContentInner').slideUp();
                                                    $('.myaccLinks li a').removeClass('active');
                                                    $('.checkOrderForm input#check_order_number').removeClass('active');
                                                    $('#check_order_number').val("");
                                                }

                                                function orderStatus(id)
                                                {
                                                    $('.myaccLinks li a').removeClass('active');
                                                    $('.checkOrderForm input#check_order_number').removeClass('active');
                                                    $('#check_order_number').val("");
                                                    $('.' + id).toggleClass('active');
                                                    $('.accSetting_slider').slideUp();
                                                    $('.accSetting_slider .accSetting_sliderInner').slideUp();
                                                    $('.checkOrder_slider').slideUp();
                                                    $('.checkOrder_slider .checkOrder_sliderInner').slideUp();
                                                    var slideDivDisplay = $('.accDet_slider').css('display');
                                                    if (slideDivDisplay == 'none') {
                                                        $('.accDet_slider').show();
                                                        $('.accDet_slider .accDet_sliderInner').slideToggle();
                                                    } else
                                                    {
                                                        $('.myaccLinks li a').removeClass('active');
                                                        $('.accDet_slider').slideToggle();
                                                        $('.accDet_slider .accDet_sliderInner').slideToggle();
                                                    }
                                                }

                                                function accSetting(id)
                                                {
                                                    $('.myaccLinks li a').removeClass('active');
                                                    $('.checkOrderForm input#check_order_number').removeClass('active');
                                                    $('#check_order_number').val("");
                                                    $('.' + id).toggleClass('active');
                                                    $('.accDet_slider').slideUp();
                                                    $('.accDet_slider .accDet_sliderInner').slideUp();
                                                    $('.checkOrder_slider').slideUp();
                                                    $('.checkOrder_slider .checkOrder_sliderInner').slideUp();

                                                    var slideDivDisplay = $('.accSetting_slider').css('display');
                                                    if (slideDivDisplay == 'none') {
                                                        $('.accSetting_slider').show();
                                                        $('.accSetting_slider .accSetting_sliderInner').slideToggle();
                                                    } else
                                                    {
                                                        $('.myaccLinks li a').removeClass('active');
                                                        $('.accSetting_slider').slideToggle();
                                                        $('.accSetting_slider .accSetting_sliderInner').slideToggle();
                                                    }
                                                    fillaccountDetail();
                                                }
                                                function fillaccountDetail() {
                                                    var userId = $('#hid_user_id').val();
                                                    var url = "<?php echo base_url(); ?>common/getPrimaryUserDetails/" + userId;
                                                    $.ajax({
                                                        type: "GET",
                                                        url: url,
                                                        dataType: "json",
                                                        success: function(test) {
                                                            var msg = test;
                                                            //alert(msg);
                                                            if (msg.billing !== null) {
                                                                $('#bil_name').text(msg.billing.first_name + ' ' + msg.billing.last_name);
                                                                $('#bil_addr1').text(msg.billing.address1);
                                                                $('#bil_addr2').text(msg.billing.address2);
                                                                $('#bil_city').text(msg.billing.city);
                                                                $('#bil_state').text(msg.billing.state);
                                                                $('#bil_zip').text(msg.billing.zipcode);
                                                                $('#acc_email').val(msg.billing.email);
                                                                $('#hid_acc_email').val(msg.billing.email);
                                                            }
                                                            if (msg.shipping !== null) {
                                                                //$('#bil_name').text(msg.userInfo.billing);
                                                                $('#shipping_name').text(msg.shipping.firstname + ' ' + msg.shipping.lastname);
                                                                $('#shipping_address1').text(msg.shipping.address1);
                                                                $('#shipping_address2').text(msg.shipping.address2);
                                                                $('#shipping_city').text(msg.shipping.city);
                                                                $('#shipping_state').text(msg.shipping.state);
                                                                $('#shipping_zipcode').text(msg.shipping.zipcode);
                                                                $('#shipping_phone').text(msg.shipping.telephone);
                                                            }

                                                        }
                                                    });
                                                }




                                                function change_userInfo(type)
                                                {
                                                    $('#acc_' + type).focus();
                                                    var onclickfn = 'save_userInfo("' + type + '")';
                                                    $('#change_' + type).attr("onclick", onclickfn);
                                                    $('#change_' + type).text("[SAVE]");
                                                    $('input#acc_' + type).addClass('change_input');
                                                    $('input#acc_' + type).attr("disabled", false);
                                                    $('input#acc_' + type).attr("readonly", false);
                                                }

                                                function showpassword()
                                                {
                                                    var onclickfn = 'hidepassword()';
                                                    $('#show_password').attr("onclick", onclickfn);
                                                    $('#show_password').text("[HIDE]");
                                                    $('input#acc_password').attr("type", "text");
                                                }

                                                function hidepassword()
                                                {
                                                    var onclickfn = 'showpassword()';
                                                    $('#show_password').attr("onclick", onclickfn);
                                                    $('#show_password').text("[SHOW]");
                                                    $('input#acc_password').attr("type", "password");
                                                    save_userInfo('password');
                                                }
                                                function save_userInfo(type)
                                                {
                                                    var onclickfn = 'change_userInfo("' + type + '")';
                                                    $('#change_' + type).attr("onclick", onclickfn);
                                                    $('#change_' + type).text("[CHANGE]");
                                                    $('input#acc_' + type).removeClass('change_input');
                                                    $('input#acc_' + type).attr("disabled", true);
                                                    $('input#acc_' + type).attr("readonly", true);


                                                    var newval;
                                                    newval = $('#acc_' + type).val();
                                                    if (type === 'password' || type === 'email') {
                                                        if (type === 'email') {
                                                            if (validateEmail($('#acc_' + type).val())) {
                                                                newval = encodeURIComponent($('#acc_' + type).val());
                                                            } else {
                                                                alert('Invalid email id');
                                                                return false;
                                                            }
                                                        }
                                                        else {
                                                            if ($('#acc_' + type).val().trim() == '') {
                                                                return false;
                                                            }
                                                        }
                                                        var userId = <?php echo $user['user_id']; ?>;
                                                        var url = "<?php echo base_url(); ?>common/changeSettings/" + type + "/" + newval + "/" + userId;
                                                        $.ajax({
                                                            type: "GET",
                                                            url: url,
                                                            success: function(msg) {
                                                                if (!msg) {
                                                                    if ($('#acc_email').val() !== $('#hid_acc_email').val()) {
                                                                        alert('Email has already registered');
                                                                        fillaccountDetail();
                                                                    }
                                                                }
                                                                else if (type === 'email') {
                                                                    alert('Email has changed successfully');
                                                                }
                                                                else if (type === 'password') {
                                                                    alert('Password has changed successfully');
                                                                }
                                                                if (msg['status'] === "true") {
                                                                    $('#errorInfo').removeClass('invalid');
                                                                    $('#errorInfo').html(msg.error);
                                                                } else {
                                                                    $('#errorInfo').addClass('invalid');
                                                                    $('#errorInfo').html(msg.error);
                                                                }
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        $.ajax({
                                                            type: "GET",
                                                            url: "<?php echo base_url(); ?>home/updateprofile",
                                                            dataType: "json",
                                                            data: {
                                                                value: value,
                                                                type: type,
                                                                userid: '88'
                                                            },
                                                            success: function(msg) {
                                                                if (msg.status == "true") {
                                                                    $('#errorInfo').removeClass('invalid');
                                                                    $('#errorInfo').html(msg.error);
                                                                } else {
                                                                    $('#errorInfo').addClass('invalid');
                                                                    $('#errorInfo').html(msg.error);
                                                                }
                                                            }
                                                        });
                                                    }

                                                }
                                                function validateEmail($email) {
                                                    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                                                    if (!emailReg.test($email)) {
                                                        return false;
                                                    } else {
                                                        return true;
                                                    }
                                                }
                                                function getdesigns(userid, type, purchase, save, fav) {

                                                    $('#designtype').val(type);

                                                    if (purchase === "") {
                                                        purchase = $('.styledCheckbox input#purchased').is(':checked');
                                                    }

                                                    if (save === "") {
                                                        save = $('.styledCheckbox input#saved').is(':checked');
                                                    }

                                                    if (fav === "") {
                                                        fav = $('.styledCheckbox input#favorited').is(':checked');
                                                    }

                                                    $('.designsList a').removeClass('active');
                                                    $('.' + type + '_design_tab a').addClass('active');
                                                    $.ajax({
                                                        type: "GET",
                                                        url: "<?php echo base_url(); ?>home/getdesigns",
                                                        data: {
                                                            userid: userid,
                                                            type: type,
                                                            purchase: purchase,
                                                            save: save,
                                                            fav: fav
                                                        },
                                                        success: function(msg) {
                                                            $('#content').html(msg.html);
                                                        }
                                                    });
                                                }


                                                $(document).ready(function() {
                                                    $('#giftcards').on('click touchend', function() {
                                                        $('.giftcardDetails_slider').show();
                                                        $('.giftcardDetails_slider .giftcardDetails_sliderInner').slideToggle();
                                                    });
                                                    $('.scrollOpen').on('click touchend', function() {
                                                        faqScroll($(this).attr('data-id'));
                                                    });
                                                    $('.myaccLinks li a.order').on('click touchend', function() {
                                                        orderStatus($(this).attr('data-id'));
                                                        return false;
                                                    });
                                                    $('.myaccLinks li a.acc_settings').off('click touchend').on('click touchend', function() {
                                                        accSetting($(this).attr('data-id'));
                                                    });
                                                    getPurchaseCounts();
                                                    designcheckchange();

                                                    $('.styledCheckbox').on('click touchend', function() {
                                                        designcheckchange();
                                                    });

                                                });
                                                function designcheckchange() {

                                                    // $('#scrapbook-filter input[type = "checkbox"]').change(function(){alert('a')})
                                                    $type = $('#designtype').val();
                                                    var purchasecheck = $('.styledCheckbox input#purchased').is(':checked') ? 1 : 0;
                                                    var savecheck = $('.styledCheckbox input#saved').is(':checked') ? 1 : 0;
                                                    var favcheck = $('.styledCheckbox input#favorited').is(':checked') ? 1 : 0;
                                                    $.ajax({
                                                        url: "<?php echo base_url(); ?>common/getUserDesigns/" + purchasecheck + "/" + savecheck + "/" + favcheck,
                                                        success: function(msg) {
                                                            $("#account_designs").html(msg);
                                                            $('#account_designs .item').off(is_touch_device() ? 'tap' : 'click').on(is_touch_device() ? 'tap' : 'click', function() {
                                                                var designId = $(this).attr('data-id');
                                                                var mode = $(this).attr('data-mode');
                                                                ShoeDesign.designDetails(designId, mode);
                                                            });
                                                        }
                                                    });
                                                }
                                                function getPurchaseCounts() {
                                                    var userId = <?php echo $user['user_id']; ?>;
                                                    $.ajax({
                                                        type: "POST",
                                                        url: "<?php echo base_url(); ?>common/getPurchaseCounts/" + userId,
                                                        dataType: "json",
                                                        data: {
                                                            userid: userId
                                                        },
                                                        success: function(msg) {
                                                            // $('#ordered').text(msg.ordered); 
                                                            // $('#order_processing').text(msg.ordered);
                                                            // $('#purchased').text(msg.purchased); 
                                                            //  $('#order_purchased').text(msg.purchased); 
                                                            // $("#account_designs").html(msg);
                                                        }
                                                    });
                                                }

                                                function item_popup_detail(designid)
                                                {
                                                    //$('body').addClass('noscroll');
                                                    $('#popupOverlay').css("display", "block");
                                                    $('#design_menu li:first').addClass('expanded');
                                                    $('#design_menu li:first').next('.level2').show();
                                                    var $selectionPopup = $(".popup");
                                                    $('#design_menu li:first').addClass('expanded');
                                                    $('#design_menu li:first').next('.level2').show();
                                                    $selectionPopup.css("opacity", "0");
                                                    $selectionPopup.css({
                                                        'display': 'block',
                                                        'top': $(window).height() / 2 + $(window).scrollTop(),
                                                        'left': $(window).width() / 2,
                                                        'margin-left': '-' + ($selectionPopup.width() / 2) + 'px',
                                                        'margin-top': '-' + (545 / 2) + 'px'
                                                    });
                                                    $selectionPopup.css("opacity", "1");
                                                    $selectionPopup.animate({height: "565"}, 1200);
                                                    return false;
                                                }

                                                $('#sameBillingAddr').off('click touchend').on('click touchend', function() {
                                                    var type = 'shipping';
                                                    //console.log($('#selectedBillingStateId').val());
                                                    $(this).toggleClass('checked');
                                                    $check = $(".sameBillingAddr.checked").length;
                                                    if ($check === 1) {
                                                        $('.edit_address_slider').show();
                                                        $('#edit_' + type + '_addr').addClass('active');
                                                        $('#' + type + '_credentials_slider').animate({"top": "0"}, 500, function() {
                                                            $('#edit_' + type + '_addr span').text("Save");
                                                            var onclickfn = 'save_address("' + type + '")';
                                                            $('#edit_' + type + '_addr').attr("onclick", onclickfn);

                                                        });
                                                        $('#edit_sfirstname').val($('#bil_name').text().split(' ')[0]);
                                                        $('#edit_slastname').val($('#bil_name').text().split(' ')[1]);
                                                        $('#edit_saddress1').val($('#bil_addr1').text());
                                                        $('#edit_saddress2').val($('#bil_addr2').text());
                                                        $('#edit_scity').val($('#bil_city').text());
                                                        $('#edit_sstate').val($('#bil_state').text());
                                                        $('#edit_szipcode').val($('#bil_zip').text());
                                                        $('#edit_phone').val("");

                                                    }


                                                });


                        </script>            

<?php $this->load->view('common/template/footer'); ?>


                        </body>
                        </html>        


