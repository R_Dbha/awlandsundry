<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<?php $this->load->view('common/template/begin'); ?>

<body class="home">
    <?php if($error != NULL) { ?>
    <p> <?php echo $error; ?></p>
    <?php } else {?>
    <p>Some error has occurred due to invalid data. </p>
    <?php } ?>
    <br />
    <a href="<?php echo base_url();?>custom-shoe/checkout">Click here to go back.</a>
</body>

</html>