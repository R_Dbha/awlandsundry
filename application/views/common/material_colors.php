<section class="createNewContainer" id="main">
    <div id="stepMain">
        <div class="wrapper clearfix">
            <ul class="createNewNav clearfix">
                <li class="createStep1"><a href="javascript:;" ><span>Style</span></a></li>
                <li class="createStepLast"><a href="javascript:void(0);"  ><span>Last</span></a></li>
                <li class="createStep2"><a href="javascript:;" ><span>Design features</span></a></li>
                <li class="createStep3"><a class="active" href="javascript:void(0);" class="no-bg"><span>Materials &amp; colors</span></a></li>
                <li class="createStep5"><a href="javascript:;" class="no-bg"><span>Monogram</span></a></li>
                <li class="createStep4"><a href="javascript:void(0);" class="no-bg"><span>Sizing</span></a></li>
                <div class="progressBar"></div>
            </ul>
            <div class="stepFlowBar" id="stepFlowBar"></div>
        </div>
    </div> 
    <div class="stepContent wrapper clearfix">
        <div class="headline">
            <p><span>Step 4</span>Click on any part of the shoe to add materials and colors</p>
        </div>
        <div id="shape_visualization" class="shape_visualization">
            <!--<div class="save_d">Save your design</div>-->
            <div class="visualImgWrap">
                <img id="error" src="<?php echo ApplicationConfig::IMG_BASE ?>none.png" />
                <img id="base" src="" class="float-top" />
                <img id="toe" src="" class="float-top" />
                <img id="vamp" src="" class="float-top" />
                <img id="eyestay" src="" class="float-top" />
                <img id="foxing" src="" class="float-top" />
                <img id="lace" src="" class="float-top" />
                <img id="stitch" src="" class="float-top" />
                <div id="style_path"></div>
            </div>            
            <div style="display:none" id="save-share">
                <a href="javascript:;" onclick="CustomShoe.saveAndShare();" class="save-share button blue">Save And Share</a>
            </div>
            <!--div id="vis_buttons">
                <span>Top view</span>
                <a class="rotate top" href="javascript:;"></a>
                <span>Rotate view</span>
                <a class="rotate left" href="javascript:;"></a>
                <a class="rotate right" href="javascript:;"></a>
            </div-->
            <div id="last_name">
                <span><?php echo $model['last']['last_name'] . ' / ' . $model['style']['style_name']; ?></span>
            </div>
            <div id="vis_buttons">                   
                <a class="rotate left" href="javascript:;"></a>
                <a class="rotate right" href="javascript:;" style=""></a>
                <span class="topview_link">
                    <a class="top" href="javascript:;">Top view</a>
                </span>
            </div>
            <div id="price_div">
                <span class="symbol">$</span>
                <span class="price"></span>
                <span class="shipping">W / free shipping</span>
            </div>
        </div>
        <div id="design_main_links">
            <ul class="top">
                <li class="save"><a href="javascript:;" onClick="messagepopup('save');"><span>Save your design</span></a></li>
                <li class="create"><a href="javascript:;" id="steptwosubmit"><span>Select & continue...</span></a></li>
            </ul>
            <div class="action">
                <div class="default"></div>
                <div class="changable hidden save save_two"><p><strong>Save</strong> this design to your personal account and return to it later</p></div>
                <div class="changable hidden create select_two"><p><strong>Proceed</strong> to the next step to give your shoes the proper fit</p></div>
            </div>
        </div>
    </div>
    <div id="color_selection_popup" class="colors_popup shape_selection_popup">
        <div class="colorsMenuWrap clearfix"> 
            <div id="stitching" class="color_menu">
                <a>Stitching color</a>
                <ul id="stitching_color" class="clearfix">
                </ul>
            </div>
            <div id="lacing" class="color_menu">
                <a>Lacing color</a>
                <ul id="lacing_color" class="clearfix">
                </ul>
            </div>
        </div>        
    </div>
    <div id="shape_selection_popup" class="top_slide_bg toe_styling shape_popup shape_selection_popup">
        <div class="topSliderInner clearfix" id="quarter_matco">
            <div class="topSliderLeft">
                <ul class="styleList" id="quarter_materials">
                </ul>
            </div>
            <!--div class="topSliderRight">
                <ul id="quarter_colors" class="img-style jHcarousel"></ul>
            </div-->
        </div>
        <div class="topSliderInner clearfix" id="toe_matco">
            <div class="topSliderLeft">
                <ul class="styleList" id="toe_materials">
                </ul>
            </div>
            <!--div class="topSliderRight">
                <ul id="toe_colors" class="img-style jHcarousel"></ul>
            </div-->
        </div>
        <?php
        $stepdata = $this->session->userdata('step_data');
        $isvamp = $stepdata['style']['isVampEnabled'];
        if ($stepdata != NULL && $isvamp) {
            ?>
            <div class="topSliderInner clearfix" id="vamp_matco">
                <div class="topSliderLeft">
                    <ul class="styleList" id="vamp_materials">
                    </ul>
                </div>
                <!--div class="topSliderRight">
                    <ul id="vamp_colors" class="img-style jHcarousel"></ul>
                </div-->
            </div>
        <?php } ?>
        <div class="topSliderInner clearfix" id="eyestay_matco">
            <div class="topSliderLeft">
                <ul class="styleList" id="eyestay_materials">
                </ul>
            </div>
            <!--div class="topSliderRight">
                <ul id="eyestay_colors" class="img-style jHcarousel"></ul>
            </div-->
        </div>
        <div class="topSliderInner clearfix" id="foxing_matco">
            <div class="topSliderLeft">
                <ul class="styleList" id="foxing_materials">
                </ul>
            </div>
            <!--div class="topSliderRight">
                <ul id="foxing_colors" class="img-style jHcarousel"></ul>
            </div-->
        </div>
        <div class="popup_pattern_bar"><a class="shape_popup_close" href="javascript:;" onClick="CustomShoe.stepThree.closePopup();">&nbsp;</a></div>   
    </div>
</section>
<canvas id="democan"  width="779" height="438" style="display:none;"></canvas>
<div class="transparent_div" id="transparent_div"></div> 
<div id="materials">
    <div class="a material hidden">
        <div id="img_wrap">

        </div>
        <span><span class="bold"></span> <span class="slash">/</span><span class="mat-name"></span></span>
        <a class="closeItemOver" href="javascript:;">&nbsp;</a>
    </div>
</div>

<div id="popupOverlay"></div>
<!-- previous step popups begins-->
<div class="messageBox messageBoxRed messageBox_step1">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div> 
</div>


<div class="messageBox messageBoxRed messageBox_step2">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will default your leather texture to calf plain black.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/design-features">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div> 
</div>

<div id="popupOverlay"></div>
<div id="accounts_selection_popup" class="popup"></div>

<!-- previous step popups ends-->
<div class="messageBox messageBoxGreen messageBox_save">
    <div class="msgTop"><div class="msgHead">You must be signed in to do that.</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Saving a shoe to your scrapbook requires an account.</div>
       <div class="msgOption"><a href="<?php echo base_url(); ?>login">Sign In</a></div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>signup">Create an Account</a></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">Not now,</a> <span>do not save.</span></div>
    </div> 
</div>
<div class="messageBox messageBoxRed messageBox_step_last">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-last">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div> 
</div>
<div class="messageBox messageBoxRed messageBox_revise">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design and take you back to step 1</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a>
            <span>start over.</span>
        </div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>login" id="savedesign">Yes,</a>
            <span>but first save my current design to my account page (account required).</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and return to my current design.</span>
        </div>
    </div>
</div>
<div class="controls">
    <a id="prev" class="control" href="javascript:;" ></a>
    <a id="next" class="control" href="javascript:;" ></a>
</div>
<?php $this->load->view('common/template/jscontainer'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        CustomShoe.stepThree.init(<?php echo json_encode($this->session->userdata('step_data')); ?>);
        $('#next').off('click touchend').on('click touchend', function() {
            CustomShoe.stepThree.nextStep();
        });
        $('#prev').off('click touchend').on('click touchend', function() {
            CustomShoe.stepThree.prevStep();
        });
    });
</script>