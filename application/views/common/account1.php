<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<?php $this->load->view('common/template/begin_accounts'); ?>
       <?php $this->load->view('common/template/jscontainer'); ?>
       <body class="page accounts">
       <header id="header">
           <section id="toolbar">
               <div class="toolbarInner clearfix"> 
                   <div class="wrapper content clearfix">
                       <div class="left">
                           <a href="<?php echo base_url();?>index.php/common/shipping"><span>Free shipping &amp; 30 Day Exchange</span></a>
                       </div>
                       
               <div class="right"> 
                       <ul id="headerNavLinks">
                           <li class="top_our_story">
                           <a href="<?php echo base_url();?>index.php/common/story">Our Story</a>
                       </li> 
                   <li class="login"><a href="<?php echo base_url();?>index.php/common/logout" title="Logout" >Logout</a></li>
                   
                   
                                   <li class="cart">
                       <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                   </li>
               </ul>
               
               </div>

</div>
               </div>
           </section>
           <h1 id="logo">
               <a href="<?php echo base_url();?>index.php">Awl&amp;Sundry</a>
           </h1>
           <div class="wrapper shopping_cart_pop clearfix">
               <section class="shopping_cart_mini"></section>
           </div>
       </header>
 <section id="middle_container">              
       <section id="main" class="page wrapper clearfix">
            <div id="main_slider" class="clearfix">
                <div id="main_slider_content" class="hoverslider">

                    <div class="hs-inner">
                        <div class="hs-image-container">
                            <a class="hs-image-page-link" href="<?php echo base_url();?>index.php/common/story">&nbsp;</a>
                            <div class="hs-image" style="background: url('<?php echo base_url(); ?>assets/img/gallery/1.jpg')"></div>
                            <div class="hs-html-container">
                                <h1 class="hs-title">A REVOLUTION<br/>IS AN IDEA<br/>TAKEN UP BY SHOEMAKERS</h1>
                                <div class="hs-html hs-right">
                                    <div class="hs-html-content">
                                        <p>We’re bringing handcrafted shoes down from the top shelves of luxury and putting them within reach. 
                                        </p>
                                        <a href="<?php echo base_url();?>index.php/common/story">Learn more</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="hs-image-container">
                            <a class="hs-image-page-link" href="<?php echo base_url();?>index.php/customshoe/index">&nbsp;</a>
                            <div class="hs-image" style="background: url('<?php echo base_url(); ?>assets/img/gallery/2.jpg')"></div>
                            <div class="hs-html-container">
                                <h1 class="hs-title">Without tools<br/>man is nothing.<br/>WIth tools<br/>he is a shoemaker.</h1>
                                <div class="hs-html hs-right">
                                    <div class="hs-html-content">
                                        <p>
                                            Our creative online design tools put you, the one with the actual feet, back into the shoemaking process. 
                                        </p>
                                        <a href="<?php echo base_url();?>index.php/customshoe/index">Create a shoe</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hs-image-container">
                            <a class="hs-image-page-link" href="<?php echo base_url();?>index.php/common/inspiration">&nbsp;</a>
                            <div class="hs-image" style="background: url('<?php echo base_url(); ?>assets/img/gallery/3.jpg')"></div>
                            <div class="hs-html-container">
                                <h1 class="hs-title">THE TREE OF LIBERTY<br/>MUST FROM TIME TO TIME<br/>BE A SHOE TREE</h1>
                                <div class="hs-html hs-right">
                                    <div class="hs-html-content">
                                        <p>
                                            Free yourself from ready-made mens’ shoes. Free at last! Free at last! (We have five lasts to choose from!)
                                        </p>
                                        <a href="<?php echo base_url();?>index.php/common/inspiration">Get Inspired</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
           
           <div id="account" class="wrapper clearfix">
               <?php $user =  $this->session->userdata('user_details'); ?>
               <h3>Welcome <?php echo $user['first_name']; ?></h3>
           </div>
           
       </section>
       
       <section id="secondary" class="page wrapper clearfix">
           
           <div id="links" class="wrapper clearfix">
               <ul class="top clearfix">
                   <li class="account"><a href="javascript:;"><span>Your account info</span></a></li>
                   <li class="inspire"><a href="<?php echo base_url();?>index.php/common/inspiration"><span>Get Inspired</span></a></li>
                   <li class="create"><a href="<?php echo base_url();?>index.php/customshoe/index"><span>Create a custom shoe</span></a></li>
               </ul>
           </div>
           <div id="account_content" class="wrapper clearfix">
               <div class="action">
                   <div class="default clearfix">
                       <div class="youHaveLeft">
                           <span>You have :</span>
                       </div>
                       <div class="youHaveRight">
                           <ul class="myaccLinks clearfix">
                               <li><a class="order_processing" href="javascript:;" onClick="orderStatus('order_processing');"><span class="number">0</span> order processing</li>
                               <li><a class="purchases" href="javascript:;" onClick="orderStatus('order_processing');"><span class="number">0</span> purchases</a></li>
                               <li><a class="returns" href="javascript:;" onClick="orderStatus('order_processing');"><span class="number">0</span> EXCHANGES</a></li>
                               <li class="b_crumb"><a class="acc_settings" href="javascript:;" onClick="accSetting('acc_settings');">ACCOUNT SETTINGS</a></li> 
                               <li class="b_crumb clearfix checkOrderFormWrap">
                                   <p>check Order status</p>
                                    <div class="checkOrderForm">
                                       <input name="check_order_number" id="check_order_number" onKeyDown="if(event.keyCode == 13) check_order_submit();" type="text" />
                                       <!--<input type="submit" value="submit" />-->
                                       <a href="javascript:;" id="check_order_submit" class="check_order_submit" onClick="check_order_submit();">&nbsp;</a>
                                    </div>
                               </li>
                            </ul>
                       </div>
                       
                       
                   </div>
                   
                   
                   <div class="changable hidden inspire"><p><strong>view</strong> the shoe creations of others, save them for inspiration, buy them or rework them with your own ideas</p></div>
                   <div class="changable hidden create"><p><strong>choose</strong> a basic style, then specify design details, broguing patterns, leathers, colors, stitching and lacing</p></div>
                   

               </div>
           </div>
           
           <div id="order_processing_detail" class="sliderContent accDet_slider wrapper clearfix" style="display:none;">
               <a class="cancel_sign" href="javascript:;" onClick="accountSliderClose();">&nbsp;</a>
               <div class="sliderContentInner accDet_sliderInner" style="display:none;">
                   
                   <div class="accDetHead"><span class="number">0</span> order processing</div>
                                    
                   <div class="accDetHead"><span class="number">0</span> purchases</div>
                                        <div class="accDetHead"><span class="number">0</span> Exchanges</div>
                                        <p class="accDetHead">Please contact our customer service to exchange your product. You can reach them at <?php echo $this->config->item('contact_email'); ?> or (917) 825 9404.</p>
               </div>
           </div>
           
           
           <div id="account_settings_detail" class="sliderContent accSetting_slider wrapper clearfix" style="display:none;">
               <a class="cancel_sign" href="ja
                  vascript:;" onClick="accountSliderClose();">&nbsp;</a>
               <div class="sliderContentInner accSetting_sliderInner clearfix" style="display:none;">
                    
                   <div class="acc_cre account_credentials" id="account_credentials">
                           <h2><?php echo $user['first_name']; ?></h2>
                           <div class="userInfo_line">EMAIL:
                              <input type="text" disabled="disabled" name="acc_email" id="acc_email" value="<?php echo $user['email']; ?>" readonly />
                              <input type="hidden" disabled="disabled" name="hid_acc_email" id="hid_acc_email" value="<?php echo $user['email']; ?>" readonly />
                              <a class="change_email" id="change_email" href="javascript:;" onClick="change_userInfo('email');">[CHANGE]</a>
                           </div>
                           
                           <div class="userInfo_line">PASSWORD:
                              <input type="password" disabled="disabled" name="acc_password" id="acc_password" value= "" readonly />
                               <!--<a href="javascript:;" onClick="showpassword();" id="show_password">{SHOW}</a> -->
                               <a class="change_password" id="change_password" href="javascript:;" onClick="change_userInfo('password');">[CHANGE]</a>
                           </div>
                           
                           <div id="errorInfo" class=""></div>
                           
                           
                  </div>    
                   
                   
                   <div class="acc_cre billing_credentials" id="billing_credentials">
                       <h2>BILLING ADDRESS</h2>
                       <div class="addr_line" id ="bil_name"><?php echo $user['first_name'].' '.$user['last_name']; ?></div>
                       <div class="addr_line" id="bil_addr1"></div>
                       <div class="addr_line" id="bil_addr2"></div>
                       <div class="addr_line" id="bil_city"></div>
                       <div class="addr_line" id="bil_state"></div>
                       <div class="addr_line" id="bil_zip"></div>
                       <div class="edit_addr_wrap"><a class="edit_addr" id="edit_billing_addr" href="javascript:;" onClick="edit_address('billing');"><span>Edit</span></a></div>
                       
                   </div>
                   <input type ="hidden" id="hid_billing_id" />
                   <input type ="hidden" id="hid_shipping_id" />
                   <input type ="hidden" id="hid_user_id" value="<?php echo $user['user_id']; ?>" />
                   
                   <div class="acc_cre shipping_credentials" id="shipping_credentials">
                       <h2>SHIPPING ADDRESS</h2> <a class="sameBillingAddr" id="sameBillingAddr" href="javascript:;">SAME AS BILLING</a>
                       <div class="addr_line" id="shipping_name"><?php echo $user['first_name'].' '.$user['last_name']; ?></div>
                       <div class="addr_line" id="shipping_address1"></div>
                       <div class="addr_line" id="shipping_address2"></div>
                       <div class="addr_line"  id="shipping_city"> </div>                       <div class="addr_line"  id="shipping_state"> </div>
                       <div class="addr_line"  id="shipping_zipcode"> </div>
                       <div class="addr_line"  id="shipping_phone"> </div>
                       
                       <div class="edit_addr_wrap"><a class="edit_addr" id="edit_shipping_addr" href="javascript:;" onClick="edit_address('shipping');"><span>Edit</span></a></div>
                   </div>
                   
                   
                   <div class="edit_address_slider" id="billing_credentials_slider">
                       <div class="edit_address_slider_inner">
                           <div>Billing Address</div>
                           <div class="edit_address_fields">
                           <input type="text" name="edit_firstname" id="edit_firstname" placeholder="firstname" value="<?php echo $user['first_name']; ?>" />
                           <input type="text" name="edit_lastname" id="edit_lastname" placeholder="lastname" value="<?php echo $user['last_name']; ?>" />
                           <input type="text" name="edit_address1" id="edit_address1" placeholder="address1" value="" />
                           <input type="text" name="edit_address2" id="edit_address2" placeholder="address2" value="" />
                           <input type="text" name="edit_city" id="edit_city" placeholder="city" value="" />
                           <input type="text" name="edit_state" id="edit_state" placeholder="state" value="" />
                           <input type="text" name="edit_zipcode" id="edit_zipcode" placeholder="zipcode" value="0" />
                           </div>
                           <div class="edit_addr_slider_btn clearfix">
                               <div class="edit_addr_slider_btn_wrap">
                                   <a class="save_addr" href="javascript:;" onClick="save_address('billing');"><span>Save</span></a>
                               </div>
                               <div class="edit_addr_slider_btn_wrap">
                                   <a class="cancel_addr" id="cancel_shipping_addr" href="javascript:;" onClick="cancel_address('billing');"><span>Cancel</span></a>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="edit_address_slider" id="shipping_credentials_slider">
                       <div class="edit_address_slider_inner">
                           <div>Shipping Address</div>
                           <div class="edit_address_fields">
                           <input type="text" name="edit_firstname" id="edit_sfirstname" placeholder="firstname" value="Aniyan" />
                           <input type="text" name="edit_lastname" id="edit_slastname" placeholder="lastname"  value="P N" />
                           <input type="text" name="edit_address1" id="edit_saddress1" placeholder="address1" value="" />
                           <input type="text" name="edit_address2" id="edit_saddress2" placeholder="address2" value="" />
                           <input type="text" name="edit_city" id="edit_scity" placeholder="city" value="" />
                           <input type="text" name="edit_state" id="edit_sstate" placeholder="state" value="" />
                           <input type="text" name="edit_zipcode" id="edit_szipcode" placeholder="zipcode" value="0" />
                           <input type="text" name="edit_phone" id="edit_phone" placeholder="phone number" value="" />
                           </div>
                           <div class="edit_addr_slider_btn clearfix">
                               <div class="edit_addr_slider_btn_wrap">
                                   <a class="save_addr" href="javascript:;" onClick="save_address('shipping');"><span>Save</span></a>
                               </div>
                               <div class="edit_addr_slider_btn_wrap">
                                   <a class="cancel_addr" id="cancel_shipping_addr" href="javascript:;" onClick="cancel_address('shipping');"><span>Cancel</span></a>
                               </div>
                           </div>
                       </div>
                   </div>
                   
                   
                   
               </div>
           </div>
           
           <div id="check_order_detail" class="sliderContent checkOrder_slider wrapper clearfix" style="display:none;">
               <a class="cancel_sign" href="javascript:;" onClick="accountSliderClose();">&nbsp;</a>
               <div class="sliderContentInner checkOrder_sliderInner" style="display:none;">
                   <div id="checkOrderContent">
                       <div class="accDetHead">YOUR ORDER # 76896K</div>
                       <ul class="accDetList">
                           <li>
                               <span class="acc_order_no"># 76896K</span>
                               <span class="acc_order_name"><big>pimptastic</big></span>
                               <span class="acc_order_last">LAST: MARTIN</span>
                               <span class="acc_order_style">STYLE: MONKSTRAP</span>
                               <span class="acc_order_submitted">SUBMITTED: 01 / 20 / 14</span>
                               <span class="acc_order_delivery acc_order_delivery_est">EST. DELIVERY: 02/16/14</span>
                               <span class="acc_order_status">STATUS: <big>IN PRODUCTION</big></span>
                           </li>
                       </ul>
                   </div>
               </div>
           </div>
           
           
           <div id="scrapbook-tab" class="wrapper clearfix">
               <ul class="designsList clearfix">
                   <!--<li class="featured_design_tab"><a href="javascript:;" onClick="getdesigns('88', 'featured', 'true', 'true', 'true');"><span>Featured designs</span></a></li>-->
                   <li class="my_design_tab"><a href="javascript:;" onClick="getdesigns('88', 'my', 'true', 'true', 'true');"><span>My designs</span></a></li>
               </ul>
               	
               <div id="scrapbook-filter">
                   <span>Show:</span>
                   <form>              
                       <input type="hidden" id="designtype" name="designtype" value="" />  
                       <input type="checkbox" name="purchases" checked="checked" id="purchased" data-sdb-image="url('http://108.163.198.226/~awlsundr/public/img/icons/checkbox_blue.png')"/>
                       <label for="purchase">Purchases</label> 
                       <input type="checkbox" checked="checked" name="saved_designs" id="saved" data-sdb-image="url('http://108.163.198.226/~awlsundr/public/img/icons/checkbox_green.png')" />
                       <label for="purchase">Saved Designs</label>
                       <input type="checkbox" checked="checked" name="favorites" id="favorited" data-sdb-image="url('http://108.163.198.226/~awlsundr/public/img/icons/checkbox_purple.png')" /> 
                       <label for="purchase">Favorites</label>
                   </form>
               </div>
           
           </div>
           
       </section>
       <!--From here-->
               <section id="content" class="clearfix">
                <div class="row clearfix" style="background-position-y: 0;">
                    <div class="row_wrapper clearfix" id="account_designs">
                       
                    </div>
                </div>
            </section>
   <section id="content" class="clearfix"></section>       
        <section id="content" class="clearfix"><div class="row clearfix">
                              <div class="row_wrapper clearfix">


           
            
            <div id="popupOverlay"></div>
            <div id="accounts_selection_popup" class="popup"></div>
        </section>  
 </section>              
               
<script type="text/javascript" src="http://108.163.198.226/~awlsundr/public/js/jquery.screwdefaultbuttonsV2.min.js"></script>
<script type="text/javascript" src="http://108.163.198.226/~awlsundr/public/js/plugins.js"></script>
    <script type="text/javascript">

function edit_address(type) {
    fillEditAddress(type);
   $('.edit_address_slider').show();
   $('#edit_'+type+'_addr').addClass('active');
   $('#'+type+'_credentials_slider').animate({"top": "0"},500, function(){
       $('#edit_'+type+'_addr span').text("Save");
       var onclickfn = 'save_address("'+type+'")';
       $('#edit_'+type+'_addr').attr("onclick",onclickfn);
        
   });
        //Rework Again Below after commenting above/// sn// 
        /*var Id,mode;
        if(type == 'billing'){
            Id = $('hid_billing_id').val();
        }
        else
            Id = $('hid_shipping_id').val();
        alert(type + $('hid_billing_id').val());
        $.ajax({
           url: "<?php echo base_url(); ?>index.php/common/billingShippingUpdate/"+type+"/"+Id,
           success: function(msg) {
                $("#account_designs").html(msg);
            }
        });*/ 
        }
function fillEditAddress(type)
{
    if(type === 'billing') {
           //$('#edit_firstname').val($('#'));
           //$lastname = $('#edit_lastname').val();
           $('#edit_address1').val($('#bil_addr1').text());
           $('#edit_address2').val($('#bil_addr2').text());
           $('#edit_city').val($('#bil_city').text());
           $('#edit_state').val($('#bil_state').text());
           $('#edit_zipcode').val($('#bil_zip').text());
       } else if(type === 'shipping') {
           $('#edit_sfirstname').val($('#shipping_name').text().split(' ')[0]);
           $('#edit_slastname').val($('#shipping_name').text().split(' ')[1]);
           $('#edit_saddress1').val($('#shipping_address1').text());
           $('#edit_saddress2').val($('#shipping_address2').text());
           $('#edit_scity').val($('#shipping_city').text());
           $('#edit_sstate').val($('#shipping_state').text());
           $('#edit_szipcode').val($('#shipping_zipcode').text());
           $('#edit_phone').val($('#shipping_phone').text());
       }
}
    function cancel_address(type)
{
   $('#edit_'+type+'_addr').removeClass('active');
   $('#'+type+'_credentials_slider').animate({"top": "-300px"},500, function(){
       $('#edit_'+type+'_addr span').text("Edit");
       var onclickfn = 'edit_address("'+type+'")';
       $('#edit_'+type+'_addr').attr("onclick",onclickfn);
       $('.edit_address_slider').hide();
   });
    $check = $(".sameBillingAddr.checked").length;
    if($check === 1){
        $(".sameBillingAddr").prop("checked",false);
        $(".sameBillingAddr").removeClass("checked");
    }
}

function save_address(type) {
   $('#edit_'+type+'_addr').removeClass('active');
       if(type === 'billing') {
           $firstname = $('#edit_firstname').val();
           $lastname = $('#edit_lastname').val();
           $address1 = $('#edit_address1').val();
           $address2 = $('#edit_address2').val();
           $city = $('#edit_city').val();
           $state = $('#edit_state').val();
           $zipcode = $('#edit_zipcode').val();
       } else if(type === 'shipping') {
           $firstname = $('#edit_sfirstname').val();
           $lastname = $('#edit_slastname').val();
           $address1 = $('#edit_saddress1').val();
           $address2 = $('#edit_saddress2').val();
           $city = $('#edit_scity').val();
           $state = $('#edit_sstate').val();
           $zipcode = $('#edit_szipcode').val();
           $phone = $('#edit_phone').val();
       }
       if($firstname.trim() == "" || $lastname.trim() == "") {
          alert("First Name & Last Name should't be blank.");
          return;
       }
       $('#'+type+'_credentials_slider').animate({"top": "-300px"},500, function(){
           $('#edit_'+type+'_addr span').text("Edit");
           var onclickfn = 'edit_address("'+type+'")';
           $('#edit_'+type+'_addr').attr("onclick",onclickfn);
       });
       var updateUrl;
       var userid;
       var updatingData = {};
	   if(type === "billing")
	   {
               //alert('ok');
               updateUrl = "<?php echo base_url();?>index.php/common/updateprimarybillingdata";
               <?php $userid = $user['user_id']; ?>;
               userId = <?php echo $userid; ?>;
               updatingData =  {
                fname : $firstname,
                lname : $lastname,
               address1 : $address1,
               address2 : $address2,
               city : $city,
               state : $state,
               zipcode : $zipcode,
               type : type,
               user_id : userId
           };
			
	   }
            else if(type === "shipping")
	   {
               //alert('ok');
			updateUrl = "<?php echo base_url();?>index.php/common/updateprimaryshippingdata";
			<?php $userid = $user['user_id']; ?>;
			userId = <?php echo $userid; ?>;
			updatingData =  {
                firstname : $firstname,
                lastname : $lastname,
                address1 : $address1,
                address2 : $address2,
                city : $city,
                state : $state,
                zipcode : $zipcode,
                phone : $phone,
                type : type,
                user_id : userId
           };
			
	   }
       $.ajax({
           type: "POST",
           url: updateUrl, 
           dataType: "json",
           data: updatingData,
           success: function(msg) {
               //console.log(msg.type);
               //location.reload();
               //alert(msg);
                $check = $(".sameBillingAddr.checked").length;
                if($check === 1){
                    $(".sameBillingAddr").prop("checked",false);
                    $(".sameBillingAddr").removeClass("checked");
                }
               fillaccountDetail();
               /*if(type == 'billing') {
                   
                       $('#edit_firstname').val(post.firstname);
                       $('#edit_lastname').val(post.lastname);
                       $('#edit_address1').val(post.address1);
                       $('#edit_address2').val(post.address2);
                       $('#edit_city').val(post.city);
                       $('#edit_state').val(post.state);
                       $('#edit_zipcode').val(post.zipcode);
                    } else {
                   
                       $('#edit_firstname').val(post.firstname);
                       $('#edit_lastname').val(post.lastname);
                       $('#edit_saddress1').val(post.address1);
                       $('#edit_saddress2').val(post.address2);
                       $('#edit_scity').val(post.city);
                       $('#edit_sstate').val(post.state);
                       $('#edit_szipcode').val(post.zipcode);
                   
               }*/
           }
       });
}

function popup_design(designid) {
   
   $.ajax({
       type: "GET",
       url: "http://108.163.198.226/~awlsundr/public"+"/home/getdesignpopup", 
       dataType: "json",
       data: {
           designid : designid,
           userid : '88'
       },
       success: function(msg) {
           //console.log(msg.html);
           $('#accounts_selection_popup').html(msg.html);
           item_popup_detail(designid);
       }
    });
}

function check_order_submit() {
   $order_number = $('#check_order_number').val();
   if($order_number!='') {
       $('.checkOrderForm input#check_order_number').addClass('active');
       //$('#check_order_number').val("");
       $('.myaccLinks li a').removeClass('active');
       $('.accDet_slider').slideUp();
       $('.accDet_slider .accDet_sliderInner').slideUp();
       $('.accSetting_slider').slideUp();
       $('.accSetting_slider .accSetting_sliderInner').slideUp();
       
       var slideDivDisplay = $('.checkOrder_slider').css('display');
       if(slideDivDisplay=='none'){
                   $('.checkOrder_slider').show();
                   $('.checkOrder_slider .checkOrder_sliderInner').slideDown();
                   $('#checkOrderContent').html('');
                   $.ajax({
                       type: "GET",
                       url: "http://108.163.198.226/~awlsundr/public"+"/home/searchorder", 
                       dataType: "json",
                       data: {
                           userid : '88',
                           refno : $order_number
                       },
                       success: function(msg) {
                           $('#checkOrderContent').html(msg.html);
                           //item_popup_detail(designid);
                       }
                   });
               }
               else
       {
                   $('.myaccLinks li a').removeClass('active');
                   $('.checkOrder_slider').slideDown();
                   $('.checkOrder_slider .checkOrder_sliderInner').slideDown();
       }
   
   }
}


jQuery(function(){
   jQuery('#scrapbook-filter input:checkbox').screwDefaultButtons({
           image: 'url("'+baseHosturl+'/img/icons/checkbox_blue.png")',
           width: 9,
           height: 9
   });
});

function accountSliderClose() {
   $('.sliderContent').slideUp();
   $('.sliderContentInner').slideUp();
   $('.myaccLinks li a').removeClass('active');
   $('.checkOrderForm input#check_order_number').removeClass('active');
   $('#check_order_number').val("");
}

function orderStatus(id)
{
   $('.myaccLinks li a').removeClass('active');
   $('.checkOrderForm input#check_order_number').removeClass('active');
   $('#check_order_number').val("");
   $('.'+id).toggleClass('active');
   $('.accSetting_slider').slideUp();
   $('.accSetting_slider .accSetting_sliderInner').slideUp();
   $('.checkOrder_slider').slideUp();
   $('.checkOrder_slider .checkOrder_sliderInner').slideUp();
   var slideDivDisplay = $('.accDet_slider').css('display');
   if(slideDivDisplay=='none'){
           $('.accDet_slider').show();
           $('.accDet_slider .accDet_sliderInner').slideToggle();
   }else
   {
           $('.myaccLinks li a').removeClass('active');
           $('.accDet_slider').slideToggle();
           $('.accDet_slider .accDet_sliderInner').slideToggle();
   }
}

function accSetting(id)
{
   $('.myaccLinks li a').removeClass('active');
   $('.checkOrderForm input#check_order_number').removeClass('active');
   $('#check_order_number').val("");
   $('.'+id).toggleClass('active');
   $('.accDet_slider').slideUp();
   $('.accDet_slider .accDet_sliderInner').slideUp();
   $('.checkOrder_slider').slideUp();
   $('.checkOrder_slider .checkOrder_sliderInner').slideUp();

   var slideDivDisplay = $('.accSetting_slider').css('display');
   if(slideDivDisplay=='none'){
           $('.accSetting_slider').show();
           $('.accSetting_slider .accSetting_sliderInner').slideToggle();
   }else
   {
           $('.myaccLinks li a').removeClass('active');
           $('.accSetting_slider').slideToggle();
           $('.accSetting_slider .accSetting_sliderInner').slideToggle();
   }
   fillaccountDetail();
}
function fillaccountDetail(){
    var userId = $('#hid_user_id').val();
    var url = "<?php echo base_url(); ?>index.php/common/getPrimaryUserDetails/"+ userId;
    $.ajax({
       type: "GET",
       url: url, 
       dataType:"json",
       success: function(test) {
           var msg = test;
           //alert(msg);
           if(msg.billing !== null){
               $('#bil_name').text(msg.billing.first_name+' '+msg.billing.last_name);
               $('#bil_addr1').text(msg.billing.address1);
               $('#bil_addr2').text(msg.billing.address2);
               $('#bil_city').text(msg.billing.city);
               $('#bil_state').text(msg.billing.state);
               $('#bil_zip').text(msg.billing.zipcode);
               $('#acc_email').val(msg.billing.email);
               $('#hid_acc_email').val(msg.billing.email);
           }
            if(msg.shipping !== null){
               //$('#bil_name').text(msg.userInfo.billing);
               $('#shipping_name').text(msg.shipping.firstname+' '+msg.shipping.lastname);
               $('#shipping_address1').text(msg.shipping.address1);
               $('#shipping_address2').text(msg.shipping.address2);
               $('#shipping_city').text(msg.shipping.city);
               $('#shipping_state').text(msg.shipping.state);
               $('#shipping_zipcode').text(msg.shipping.zipcode);
               $('#shipping_phone').text(msg.shipping.telephone);
           }
          
       }
    });
}

                   
                   
                   
function change_userInfo(type)
{
   $('#acc_'+type).focus();
   var onclickfn = 'save_userInfo("'+type+'")';
   $('#change_'+type).attr("onclick",onclickfn);
   $('#change_'+type).text("[SAVE]");
   $('input#acc_'+type).addClass('change_input');
   $('input#acc_'+type).attr("disabled",false);
   $('input#acc_'+type).attr("readonly",false);
}

function showpassword()
{
   var onclickfn = 'hidepassword()';
   $('#show_password').attr("onclick",onclickfn);
   $('#show_password').text("[HIDE]");
   $('input#acc_password').attr("type","text");
}

function hidepassword()
{
   var onclickfn = 'showpassword()';
   $('#show_password').attr("onclick",onclickfn);
   $('#show_password').text("[SHOW]");
   $('input#acc_password').attr("type","password");
   save_userInfo('password');
}
function save_userInfo(type)
{
   var onclickfn = 'change_userInfo("'+type+'")';
   $('#change_'+type).attr("onclick",onclickfn);
   $('#change_'+type).text("[CHANGE]");
   $('input#acc_'+type).removeClass('change_input');
   $('input#acc_'+type).attr("disabled",true);
   $('input#acc_'+type).attr("readonly",true);
   
   
    var newval;
    newval = $('#acc_'+type).val();
    if(type ==='password' || type === 'email'){
    if(type === 'email')
        newval = encodeURIComponent($('#acc_'+type).val());
    var userId = <?php echo $user['user_id']; ?>;
    var url = "<?php echo base_url(); ?>index.php/common/changeSettings/"+type+"/"+newval+"/"+ userId;
   $.ajax({
       type: "GET",
       url: url, 
       success: function(msg) {
           if(!msg){
               if($('#acc_email').val() !== $('#hid_acc_email').val()){
                    alert('Email has already registered');
                    fillaccountDetail();
                }
            }
            else if(type === 'email'){
                alert('Email has changed successfully');
            }
            else if(type === 'password'){
                alert('Password has changed successfully');
            }
           if(msg['status'] === "true") {
               $('#errorInfo').removeClass('invalid');
               $('#errorInfo').html(msg.error);
           } else {
               $('#errorInfo').addClass('invalid');
               $('#errorInfo').html(msg.error);
           }
       }
    });
    }
    else{
   $.ajax({
       type: "GET",
       url: "http://108.163.198.226/~awlsundr/public"+"/home/updateprofile", 
       dataType: "json",
       data: {
           value : value,
           type : type,
           userid : '88'
       },
       success: function(msg) {
           if(msg.status == "true") {
               $('#errorInfo').removeClass('invalid');
               $('#errorInfo').html(msg.error);
           } else {
               $('#errorInfo').addClass('invalid');
               $('#errorInfo').html(msg.error);
           }
           //console.log(msg);
           //$('#content').html('');
           //$('#content').html(msg.html);
           //$(this).parent().remove();
           //location.reload();
       }
    });
    }
    
}  

function getdesigns(userid, type, purchase, save, fav) {
   
   $('#designtype').val(type);
   
   if(purchase === "") {
       purchase = $('.styledCheckbox input#purchased').is(':checked');
   }
   
   if(save === "") {
       save = $('.styledCheckbox input#saved').is(':checked');
   }
   
   if(fav === "") {
       fav = $('.styledCheckbox input#favorited').is(':checked');
   }
   
   $('.designsList a').removeClass('active');
   $('.'+type+'_design_tab a').addClass('active');
   //alert(this.id); // id of clicked li by directly accessing DOMElement property
   $.ajax({
       type: "GET",
       url: "http://108.163.198.226/~awlsundr/public"+"/home/getdesigns", 
       data: {
           userid : userid,
           type : type,
           purchase : purchase,
           save : save,
           fav : fav
       },
       success: function(msg) {
           //console.log(msg);
           //$('#content').html('');
           $('#content').html(msg.html);
           //$(this).parent().remove();
           //location.reload();
           // $('#shape_selection_popup').html(msg.html);
       }
    });
}

$(window).load(function(){
   getdesigns('88', 'my', 'true', 'true', 'true');
});

$(document).ready(function(){
    
   designcheckchange();

$('.styledCheckbox').click(function() {
    designcheckchange();
});

});
function designcheckchange(){

      // $('#scrapbook-filter input[type = "checkbox"]').change(function(){alert('a')})
      //alert('a');
       $type = $('#designtype').val();
       var purchasecheck = $('.styledCheckbox input#purchased').is(':checked') ? 1 : 0;
       var savecheck = $('.styledCheckbox input#saved').is(':checked') ? 1 : 0;
       var favcheck = $('.styledCheckbox input#favorited').is(':checked') ? 1 : 0;
       //alert(purchasecheck+" "+savecheck+" "+favcheck);
        $.ajax({
           url: "<?php echo base_url(); ?>index.php/common/getUserDesigns/"+purchasecheck+"/"+savecheck+"/"+favcheck,
           success: function(msg) {
                $("#account_designs").html(msg);
            }
        }); 
       //getdesigns('88', $type, purchasecheck, savecheck, favcheck);
  }
// get design popup
function popup_design(designid) {
   $.ajax({
       type: "GET",
       url: "http://108.163.198.226/~awlsundr/public"+"/home/getdesignpopup", 
       dataType: "json",
       data: {
           designid : designid,
           userid : '88'
       },
       success: function(msg) {
           console.log(msg.html);
           $('#accounts_selection_popup').html(msg.html);
           item_popup_detail(designid);
       }
    });
}

function item_popup_detail(designid)
{
   //$('body').addClass('noscroll');
 $('#popupOverlay').css("display", "block");
  $('#design_menu li:first').addClass('expanded');
  $('#design_menu li:first').next('.level2').show();  
  var $selectionPopup = $(".popup");
 $('#design_menu li:first').addClass('expanded');
 $('#design_menu li:first').next('.level2').show();  
 $selectionPopup.css("opacity", "0"); 
 $selectionPopup.css({
          'display': 'block',
          'top': $(window).height()/2 + $(window).scrollTop(),
          'left': $(window).width()/2,
          'margin-left': '-' + ($selectionPopup.width() / 2) + 'px',
          'margin-top': '-' + (545 / 2) + 'px'
    });
 $selectionPopup.css("opacity", "1"); 
 $selectionPopup.animate({height: "565"}, 1200);
 
 /*$('#selection_popup_content').animate({right: "210px"}, 1200, function(){
  $('.popup #community').animate({"right": "770px"}, 1);
 });
 $('#side_menu').animate({right: "15px"}, 1200);*/
 
 //popup_design(designid);
 
 return false;
}

$('#sameBillingAddr').click(function() {
    var type = 'shipping';
   //console.log($('#selectedBillingStateId').val());
        $(this).toggleClass('checked');
        $check = $(".sameBillingAddr.checked").length;
        if($check === 1) {
            $('.edit_address_slider').show();
        $('#edit_'+type+'_addr').addClass('active');
        $('#'+type+'_credentials_slider').animate({"top": "0"},500, function(){
            $('#edit_'+type+'_addr span').text("Save");
            var onclickfn = 'save_address("'+type+'")';
            $('#edit_'+type+'_addr').attr("onclick",onclickfn);

        });
      $('#edit_sfirstname').val($('#bil_name').text().split(' ')[0]);
        $('#edit_slastname').val($('#bil_name').text().split(' ')[1]);
        $('#edit_saddress1').val($('#bil_addr1').text());
        $('#edit_saddress2').val($('#bil_addr2').text());
        $('#edit_scity').val($('#bil_city').text());
        $('#edit_sstate').val($('#bil_state').text());
        $('#edit_szipcode').val($('#bil_zip').text());
        $('#edit_phone').val("");
      /* $.ajax({
           type: "GET",
           url: "http://108.163.198.226/~awlsundr/public"+"/home/updateuseraddress", 
           dataType: "json",
           data: {
               type : 'shipping'
           },
           success: function(msg) {
               location.reload();
               //console.log(msg.designsarr);
               //console.log(msg.designsarr[0].shipping_address1);
               //$('#shipping_address1').val(msg.designsarr[0].shipping_address1);
               
           }
       });
       
       /*
       $('#billing_shipping').val('Y');
       $('#shipping_street_1').val($('#billing_street_1').val());
       $('#shipping_street_2').val($('#billing_street_2').val());
       $('#shipping_first_name').val($('#userfirstname').val());
       $('#shipping_last_name').val($('#userlastname').val());
       $('#shipping_email').val($('#useremail').val());
       $('#shipping_city').val($('#billing_city').val());
       $('#shipping_zipcode').val($('#billing_zipcode').val());
       if($('#selectedBillingStateId').val()!=''){
           $('#dk_container_shipping_state .dk_label').text($('#selectedBillingStateId').val());
           $('#dk_container_shipping_state').removeClass('invalid_box');
           $('#selectedShippingStateId').val($('#selectedBillingStateId').val());
           $('#shipping_state option[value="'+$('#selectedShippingStateId').val()+'"]').attr('selected', true);
           $('#shipping_state').text($('#selectedShippingStateId').val());
       }*/
   }
   else {
       /*$('#billing_shipping').val('N');
       $('#shipping_street_1').val('');
       $('#shipping_street_2').val('');
       $('#shipping_city').val('');
       $('#shipping_state').val('');
       $('#shipping_zipcode').val('');
       $('#shipping_first_name').val('');
       $('#shipping_last_name').val('');
       $('#shipping_email').val('');

       if($('#selectedShippingStateId').val() != '') {
           $('#dk_container_shipping_state .dk_label').text($('#selectedShippingStateId').val());
           $('#shipping_state option[value="'+$('#selectedShippingStateId').val()+'"]').attr('selected', true);
           $('#dk_container_shipping_state').removeClass('invalid_box');

       } else {
           $('#dk_container_shipping_state .dk_label').text("State");
           $('#dk_container_shipping_state').addClass('invalid_box');
       }*/
   }

});

               
</script>            
               
        <footer id="footer">
           <div class="wrapper">
               <section class="clearfix">
                   <span class="close"></span>
                   <div class="row">
                       <h3>Handmade shoes</h3>
                       <ul>
                           <li><a href="<?php echo base_url();?>index.php/common/inspiration">Get inspired</a></li>
                           <li><a href="<?php echo base_url();?>index.php/customshoe/index">Create a custom shoe</a></li>
                       </ul>
                   </div>
                   <div class="row">
                       <h3>About us</h3>
                       <ul>
                           <li><a href="<?php echo base_url();?>index.php/common/story">Our story</a></li>
                           <li><a href="<?php echo base_url();?>index.php/common/process">our artisanal process</a></li>
                           <li><a target="_blank" href="javascript:void(0);">Blog</a></li>
                       </ul>
                   </div>
                   <div class="row">
                       <h3>Customer service</h3>
                       <ul>
                           <li><a href="<?php echo base_url();?>index.php/common/shipping/faq#shipping" onclick="faqScroll('#shipping');">Shipping</a></li>
                           <li><a href="<?php echo base_url();?>index.php/common/shipping/faq#returns" onclick="faqScroll('#returns');">Exchange</a></li>
                           <li><a href="<?php echo base_url();?>index.php/common/shipping/faq">FAQs</a></li>
                           <li><a href="<?php echo base_url();?>index.php/common/shipping/faq">Contact us</a></li>
                       </ul>
                   </div>
                   <div class="row newsletter">
                       <h3>Keep in touch</h3>
                       <div class="keeptouchform">
                           <input type="email" class="email" id="email" onkeydown="if (event.keyCode == 13)
                                   keepEmail();" />
                           <input type="submit" class="submit" onclick="keepEmail();" />
                           <div class="thanksforsignup"><h2>Thanks for Signing up!</h2></div>
                       </div>
                       <ul>
                           <li><span>Connect with us:</span>                   
                               <div id="socials">
                                   <a href="https://www.facebook.com/AwlandSundry" class="facebook" target="_blank">Facebook</a>
                                   <a href="https://twitter.com/AwlandSundry" class="twitter" target="_blank">Twitter</a>
                                   <a href="http://www.pinterest.com/awlandsundry/" class="pinterest" target="_blank">Pinterest</a>
                                   <a href="http://instagram.com/awlandsundry?ref=badge" target="_blank" title="Instagram" class="instagram">Instagram</a>
                               </div>
                           </li>
                       </ul>
                   </div>              
               </section>
               <div class="bottom clearfix">
                   <p class="float-left">&copy; Awl &amp; Sundry. All Rights Reserved. 2012 - 2013</p>
                   <a class="float-right" href="<?php echo base_url();?>index.php/common/privacy">Privacy Policy</a> 
                   <a class="float-right" href="<?php echo base_url();?>index.php/common/terms">Terms of Use</a>
               </div>
           </div>
       </footer>

       <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
       <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.screwdefaultbuttonsV2.min.js"></script>
   </body>
</html>        
  

