<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h4>SHIPPING</h4>
<div id="shippingDetailsNonEditable" class="<?php echo $nonEditable; ?>" >
    <ul>
        <li><?php echo set_value('shipping_first_name', '') ?></li>
        <li><?php echo set_value('shipping_last_name', '') ?></li>		
        <li><?php echo set_value('shipping_email', '') ?></li>	
        <li><?php echo set_value('shipping_phone', '') ?></li>	

        <li><?php echo set_value('shipping_street_1', '') ?></li>	
        <li><?php echo set_value('shipping_street_2', '') ?></li>					       
        <li>
            <?php echo set_value('shipping_city', '') . ',' . set_value('shipping_state', '') . ', ' . set_value('shipping_country', '') . ',' . set_value('shipping_zipcode', '') ?>
        </li>
    </ul>

    <a href="javascript:;" tabindex="12" class="button continue" onClick="editDetails('shipping');">Edit</a>
</div>
<?php
if ($shippingDet != NULL) {
    $user = $this->session->userdata['user_details'];
    ?>
    <div id="shippingDetailsEditable" class="<?php echo $editable; ?>">
        <input type="hidden" name="shippingstateid" tabindex="13" id="shippingstateid" value="<?php echo $shippingstateid; ?>">
        <input type="hidden" name="billing_shipping" tabindex="14" id="billing_shipping" value="N" />
        <input type="text" name="shipping_first_name" tabindex="15" placeholder="First name" class=" req<?php echo isset($errors['shipping_first_name']) ? ' invalid_box ' : ''; ?>" id="shipping_first_name" value="<?php echo set_value('shipping_first_name', $shippingDet['firstname']); ?>">
        <input type="text" name="shipping_last_name" tabindex="16" placeholder="Last name" class="" id="shipping_last_name" value="<?php echo set_value('shipping_last_name', $shippingDet['lastname']); ?>">
        <input type="text" name="shipping_email" tabindex="17" placeholder="Email Address" class="req<?php echo isset($errors['shipping_email']) ? ' invalid_box ' : ''; ?>" id="shipping_email" value="<?php echo set_value('shipping_email', $user['email']); ?>" >
        <input type="text" name="shipping_phone" maxlength ="15" tabindex="18" placeholder="Telephone" class="<?php echo isset($errors['shipping_phone']) ? ' invalid_box ' : ''; ?>" value="<?php echo set_value('shipping_phone', $shippingDet['telephone']); ?>">
        <input type="text" name="shipping_street_1" tabindex="19" placeholder="Address line 1" class="alphaNumericOnly req<?php echo isset($errors['shipping_street_1']) ? ' invalid_box ' : ''; ?>" id="shipping_street_1" value="<?php echo set_value('shipping_street_1', $shippingDet['address1']); ?>" >
        <input type="text" name="shipping_street_2" tabindex="20" placeholder="Address line 2" class="alphaNumericOnly" id="shipping_street_2" value="<?php echo set_value('shipping_street_2', $shippingDet['address2']); ?>">
        <input type="text" name="shipping_zipcode" maxlength ="12" placeholder="Zipcode" tabindex="23" class="zipcode  req<?php echo isset($errors['shipping_zipcode']) ? ' invalid_box ' : ''; ?>" id="shipping_zipcode"  value="<?php echo set_value('shipping_zipcode', $shippingDet['zipcode']); ?>" >
        <input type="text" name="shipping_city" tabindex="21" placeholder="City" class=" req<?php echo isset($errors['shipping_city']) ? ' invalid_box ' : ''; ?>" id="shipping_city" value="<?php echo set_value('shipping_city', $shippingDet['city']); ?>" >

        <select name="shipping_country" id="shipping_country"  tabindex="12" class="shipping_country req <?php echo $this->form_validation->error_exists('shipping_state') ? 'invalid_box' : ''; ?>">
            <option value="">Country</option>
            <?php echo $shipping_country; ?>    
        </select> 
        <select name="shipping_state" class="shipping_state req" tabindex="22" id="shipping_state" >
            <?php echo $shipping_state; ?>
        </select>


    <?php } else { ?>
        <div id="shippingDetailsEditable" class="<?php echo $editable; ?>">
            <input type="hidden" name="shippingstateid" tabindex="13" id="shippingstateid" value="<?php echo $shippingstateid; ?>">
            <input type="hidden" name="billing_shipping" tabindex="14" id="billing_shipping" value="N" />
            <input type="text" name="shipping_first_name" tabindex="15" placeholder="First name" class=" req<?php echo isset($errors['shipping_first_name']) ? ' invalid_box ' : ''; ?>" id="shipping_first_name" value="<?php echo set_value('shipping_first_name', ''); ?>">
            <input type="text" name="shipping_last_name" tabindex="16" placeholder="Last name" class="" id="shipping_last_name" value="<?php echo set_value('shipping_last_name', ''); ?>" id="shipping_last_name">
            <input type="text" name="shipping_email" tabindex="17" placeholder="Email Address" class="req<?php echo isset($errors['shipping_email']) ? ' invalid_box ' : ''; ?>" id="shipping_email" value="<?php echo set_value('shipping_email', ''); ?>" >
            <input type="text" name="shipping_phone" maxlength ="15" tabindex="18" placeholder="Telephone" class="<?php echo isset($errors['shipping_phone']) ? ' invalid_box ' : ''; ?>" value="<?php echo set_value('shipping_phone', ''); ?>">
            <input type="text" name="shipping_street_1" tabindex="19" placeholder="Address line 1" class="alphaNumericOnly req<?php echo isset($errors['shipping_street_1']) ? ' invalid_box ' : ''; ?>" id="shipping_street_1" value="<?php echo set_value('shipping_street_1', ''); ?>" >
            <input type="text" name="shipping_street_2" tabindex="20" placeholder="Address line 2" class="alphaNumericOnly" id="shipping_street_2" value="<?php echo set_value('shipping_street_2', ''); ?>">
            <input type="text" name="shipping_zipcode" placeholder="Zipcode" tabindex="23" class="zipcode  req<?php echo isset($errors['shipping_zipcode']) ? ' invalid_box ' : ''; ?>" id="shipping_zipcode" maxlength="12"  value="<?php echo set_value('shipping_zipcode', ''); ?>" />
            <input type="text" name="shipping_city" tabindex="21" placeholder="City" class=" req<?php echo isset($errors['shipping_city']) ? ' invalid_box ' : ''; ?>" id="shipping_city" value="<?php echo set_value('shipping_city', ''); ?>" >

            <select name="shipping_country" id="shipping_country" tabindex="12" class="shipping_country req <?php echo $this->form_validation->error_exists('shipping_state') ? 'invalid_box' : ''; ?>">
                <option value="">Country</option>
                <?php echo $shipping_country; ?>    
            </select> 
            <select name="shipping_state" class="shipping_state req" tabindex="22" id="shipping_state" >

                <?php echo $shipping_state; ?>
            </select> 




        <?php } if ($shippingDet != NULL) { ?>
            <input type="hidden" name="selectedShippingStateId" id="selectedShippingStateId"   value="<?php echo set_value('selectedShippingStateId', $shippingDet['state']); ?>">
        <?php } else { ?>
            <input type="hidden" name="selectedShippingStateId" id="selectedShippingStateId" value="<?php echo set_value('selectedShippingStateId', ''); ?>" />
        <?php } ?>
        <div class="invalid_errors">
            <div class="invalid invalid_required">
                <?php if (isset($errors['shipping'])) {
                    echo $errors_list;
                } ?>
            </div>
            <a href="javascript:;" class="button continue" tabindex="24" onclick="validateShippingDetails();">Continue</a>

        </div>
    </div>