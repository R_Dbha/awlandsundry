<a href="javascript:;" class="popup_close" onclick="closeSelectionPopup();">&nbsp;</a>
<div id="selection_popup_content">
    <div class="top">
        <a href="javascript:;" class="close" onclick="closeSelectionPopup();">CLOSE</a>
        <div id="popup_socials">

            <a class="facebook" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo base_url(); ?>get-inspired/<?php echo $shoedesign['shoe_design_id']; ?>">Facebook</a>
            <a class="twitter" target="_blank" href="http://twitter.com/share?url=<?php echo base_url(); ?>get-inspired/<?php echo $shoedesign['shoe_design_id']; ?>&text=Checkout this custom pair of shoes I just designed at Awlandsundry.com" >Twitter</a>
            <!--<a href="#" class="pinterest">Pinterest</a> -->
            <a class="pinterest" target="_blank" href="//www.pinterest.com/pin/create/button/?media=<?php echo base_url(); ?>files/designs/' . $shoedesign['image_file'] . "_A0.png")) . "&description=" . htmlentities(urlencode('A wise sage once said, "you can judge a man by his shoes". Checkout this custom made pair I just designed at Awl & Sundry.')); ?>" data-pin-do="buttonPin" data-pin-config="above">Pinterest</a>

            <a href="http://instagram.com/awlandsundry" target="_blank" title="Instagram" class="instagram">Instagram</a>
        </div> 
    </div>
    <div id="visualization">
        <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoedesign['image_file'] . "_A0.png"; ?>" />
        <!--div id="vis_buttons">
            <span>Top view</span>
            <a class="rotate top" href="javascript:;" onclick="ShoeDesign.topView();" ></a>
            <span>Rotate view</span>
            <a class="rotate left" href="javascript:;" onclick="ShoeDesign.rotateLeft();"></a>
            <a class="rotate right" href="javascript:;" onclick="ShoeDesign.rotateRight();"></a>
        </div-->
        <div id="vis_buttons">                     
            <a class="rotate left" href="javascript:;" onclick="ShoeDesign.rotateLeft();"></a>
            <a class="rotate right" href="javascript:;" onclick="ShoeDesign.rotateRight();" style=""></a>

        </div>
        <span class="topview_link">
            <span>
                <a class="top" href="javascript:;" onclick="ShoeDesign.topView();">Top view</a>
            </span>
        </span>
    </div>
    <div id="popup_main_links">
        <ul>
            <?php
            if ($mode != NULL) {
                if ($mode == "fav" && $shoedesign['design_favs'] > 0) {
                    ?>
                    <li><a class="delete" id= "addTofavId" href="javascript:removeFromFav('Favorite');">
                            <span class="top">Unfavorite</span><span class="bottom"><strong>remove</strong> this shoe from your favorites</span></a>
                    </li> 
                    <?php
                } else {
                    if ($shoedesign['IsUserfavs'] > 0) {
                        ?>
                        <li><a class="delete" id= "addTofavId" href="javascript:alert('You have already added this shoe to your favorites');">
                                <span class="top">Add to Favorite</span><span class="bottom"><strong>add</strong> this shoe to your favorites</span></a>
                        </li>  
                    <?php } else { ?>
                        <li><a class="delete" id= "addTofavId" href="javascript:addToLikeOrFav('Favorite');">
                                <span class="top">Add to Favorite</span><span class="bottom"><strong>add</strong> this shoe to your favorites</span></a>
                        </li>  
                        <?php
                    }
                }
            } else {
                if ($shoedesign['IsUserfavs'] > 0) {
                    ?>
                    <li><a class="delete" id= "addTofavId" href="javascript:alert('You have already added this shoe to your favorites');">
                            <span class="top">Add to Favorite</span><span class="bottom"><strong>add</strong> this shoe to your favorites</span></a>
                    </li>  
                <?php } else { ?>
                    <li><a class="delete" id= "addTofavId" href="javascript:addToLikeOrFav('Favorite');">
                            <span class="top">Add to Favorite</span><span class="bottom"><strong>add</strong> this shoe to your favorites</span></a>
                    </li>  
                    <?php
                }
            }
            ?>


            <li><a class="rework" href="javascript:;" onclick="ShoeDesign.reworkShoe(<?php echo $shoedesign['shoe_design_id']; ?>);"><span class="top">Rework this shoe</span><span class="bottom"><strong>create</strong> a new design<br/>by altering this one</span></a></li>
            <li><a class="add_to_cart" href="javascript:;" onclick="ShoeCart.addDesignToStepFour(<?php echo $shoedesign['shoe_design_id']; ?>);"><span class="top">Own it</span><span class="bottom"><strong>purchase</strong> this shoe</span></a></li>
        </ul>
    </div>
    <div class="bottom">

        <?php if ($shoedesign['rework_count'] == 1) { ?>
            <span><span class="number"><?php echo $shoedesign['rework_count']; ?></span>Rework</span>
        <?php } else if ($shoedesign['rework_count'] > 1) { ?>
            <span><span class="number"><?php echo $shoedesign['rework_count']; ?></span>Reworks</span>
        <?php } else { ?>
            <span><span class="number"></span></span>
        <?php } if ($shoedesign['design_favs'] == 1) { ?>
            <span id="favmsg"><span class="number" id="favcount"><?php echo $shoedesign['design_favs']; ?> </span>Favorite</span>
        <?php } else if ($shoedesign['design_favs'] > 1) { ?>
            <span  id="favmsg"><span class="number" id="favcount"><?php echo $shoedesign['design_favs']; ?> </span>Favorites</span>
        <?php } else { ?>
            <span  id="favmsg"><span class="number" id="favcount"> </span></span>
        <?php } if ($shoedesign['design_likes'] == 1) { ?>
            <span  id="likemsg"><span class="number" id="likecount"><?php echo $shoedesign['design_likes']; ?></span>Like</span>
        <?php } else if ($shoedesign['design_likes'] > 1) { ?>
            <span  id="likemsg"><span class="number" id="likecount"><?php echo $shoedesign['design_likes']; ?></span>Likes</span>
        <?php } else { ?>
            <span  id="likemsg"><span class="number" id="likecount"></span></span>
        <?php } if ($shoedesign['IsUserlikes'] > 0) { ?>
            <a id="likeorliked">You have liked this shoe</a>
        <?php } else { ?>
            <a id="likeorliked" href="javascript:addToLikeOrFav('Like');">Like this</a>
            <?php
        } if ($mode != NULL) {
            if ($mode == "save") {
                ?>
                <a href="javascript:DeleteSavedDesign();">Delete this design</a>
                <?php
            }
        }

        if ($user['user_type_id'] == 1 && $shoedesign['is_public']) {
            ?>
            <a  id='unpublish' href='javascript:unPublish();'>Unpublish this shoe</a>
        <?php }
        ?>
    </div>
</div>
<div id="side_menu">
    <p class="title">Design Details </p>
    <div id="side_menu_content">
        <p class="item-title">
            <span ><?php echo $shoedesign['public_name']; ?></span>
            <input type='text' id='input-title' value='<?php echo $shoedesign['public_name']; ?>' />
        </p>
        <p class="item-subtitle"><?php echo $shoedesign['last_name'] . "/" . $shoedesign['style_name']; ?></p>
        <ul id="design_menu" class="level1">
            <?php if (!$shoedesign['t_is_nothing']) { ?>
                <li>Toe</li>
                <ul class="level2">
                    <li class="pattern"><?php echo $shoedesign['toe_type']; ?></li>
                    <?php if ($shoedesign['t_is_material']) { ?>
                        <li class="leather"><?php echo $shoedesign['toe_material']; ?></li>
                        <li class="color"><?php echo $shoedesign['toe_color']; ?></li>
                    <?php } else { ?>
                        <li class="leather"><?php echo $shoedesign['quarter_material']; ?></li>
                        <li class="color"><?php echo $shoedesign['quarter_color']; ?></li>
                    <?php } ?>

                </ul>
            <?php }      
            if ($shoedesign['is_vamp_enabled']) {
                ?>
                <?php if (!$shoedesign['v_is_nothing']) { ?>
                    <li>Vamp</li>
                    <ul class="level2">
                        <li class="pattern"><?php echo $shoedesign['vamp_type']; ?></li>
                        <?php if ($shoedesign['v_is_material']=="1") { ?>
                            <li class="leather"><?php echo $shoedesign['vamp_material']; ?></li>
                            <li class="color"><?php echo $shoedesign['vamp_color']; ?></li>
                        <?php } else { ?>
                            <li class="leather"><?php echo $shoedesign['quarter_material']; ?></li>
                            <li class="color"><?php echo $shoedesign['quarter_color']; ?></li>
                        <?php } ?>

                    </ul>
                    <?php
                }
            }
            if (!$shoedesign['e_is_nothing']) {
                ?>
                <li>Eyestays</li>
                <ul class="level2">
                    <li class="pattern"><?php echo $shoedesign['eyestay_type']; ?></li>
                    <?php if ($shoedesign['e_is_material']) { ?>
                        <li class="leather"><?php echo $shoedesign['eyestay_material']; ?></li>
                        <li class="color"><?php echo $shoedesign['eyestay_color']; ?></li>
                    <?php } else { ?>
                        <li class="leather"><?php echo $shoedesign['quarter_material']; ?></li>
                        <li class="color"><?php echo $shoedesign['quarter_color']; ?></li>
                    <?php } ?>

                </ul>
            <?php } ?>
            <li>Quarter</li>
            <ul class="level2">
                <li class="leather"><?php echo $shoedesign['quarter_material']; ?></li>
                <li class="color"><?php echo $shoedesign['quarter_color']; ?></li>

            </ul>
            <?php if (!$shoedesign['f_is_nothing']) { ?>
                <li>Foxing</li>
                <ul class="level2">
                    <li class="pattern"><?php echo $shoedesign['foxing_type']; ?></li>
                    <?php if ($shoedesign['f_is_material']) { ?>
                        <li class="leather"><?php echo $shoedesign['foxing_material']; ?></li>
                        <li class="color"><?php echo $shoedesign['foxing_color']; ?></li>
                    <?php } else { ?>
                        <li class="leather"><?php echo $shoedesign['quarter_material']; ?></li>
                        <li class="color"><?php echo $shoedesign['quarter_color']; ?></li>
                    <?php } ?>

                </ul>
                <?php
            }
            if ($shoedesign['is_lace_enabled']) {
                ?>
                <li>Laces</li>
                <ul class="level2">
                    <li class="color"><?php echo $shoedesign['lace_name']; ?></li>
                    <!-- <ul class="level3">
                         <li style="background: <?php // echo $shoedesign['color_code'];       ?>"></li>
                     </ul>-->
                </ul>
            <?php } ?>
            <li>Stitching</li>
            <ul class="level2">
                <li class="color"><?php echo $shoedesign['stitch_name']; ?></li>
                <!-- <ul class="level3">
                     <li style="background: <?php // echo $shoedesign['stitch_color'];       ?>"></li>
                 </ul>-->
            </ul>
        </ul>
        <div id="price_div">
            <!--<p>Handmade custom shoe</p>-->
            <span class="symbol">$</span>
            <span class="price"> <?php echo $price ?> </span>
            <span class="shipping">W / free shipping</span>
        </div>
    </div>
</div>      
<style>

</style>
<script type="text/javascript">
<?php if ($user['user_type_id'] == 1) { ?>
        $('.item-title').on('click', function() {


            $(this).children('span').hide();
            $(this).children('input#input-title').show();
        });
        $('input#input-title').on('blur', function() {
            var input_title = $(this);
            var shoe_name = $(input_title).val();

    <?php
    echo "designid = '" . $shoedesign['shoe_design_id'] . "'; ";
    ?>
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>common/renameShoe",
                dataType: "json",
                data: {
                    designid: designid,
                    shoe_name: shoe_name,
                },
                success: function(msg) {
                    shoe_name = shoe_name.replace(/^(.)|\s(.)/g, function($1) {
                        return $1.toUpperCase( );
                    })
                    $('.item-title').children('span').html(shoe_name);
                    $(input_title).hide();
                    $('.item-title').children('span').show();
                    $("#shedesigns").find('div.item[data-id=' + designid + ']').find('span.item-title').html(shoe_name);
                }
            });


        });
<?php } ?>
    $('#design_menu > li').off('click touchend').on('click touchend', function() {
        $(this).parent().find('.expanded').next('.level2').hide();
        $(this).parent().find('.expanded').removeClass();
        $(this).next('.level2').toggle('fast');
        $(this).toggleClass('expanded');
    });
    $("#popup_main_links").mouseover(function() {
        $('#selection_popup_content').css({"z-index": "9999999"});
    });
    $("#popup_main_links").mouseleave(function() {
        $('#selection_popup_content').css({"z-index": "999999"});
    });
    function unPublish() {
        if (confirm("Are you sure you want to unpublish this design?")) {

            var userId;
            var designid;
<?php
echo "designid = '" . $shoedesign['shoe_design_id'] . "'; ";
if ($user != NULL) {
    echo "userId = '" . $user['user_id'] . "';";
}
?>
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>common/unPublish",
                dataType: "json",
                data: {
                    designid: designid,
                    uid: userId,
                },
                success: function(msg) {
                    $('#unpublish').remove();
                }
            });
        }
    }
    function addToLikeOrFav(type) {
        var userId;
        var designid;
<?php
echo "designid = '" . $shoedesign['shoe_design_id'] . "'; ";
if ($user != NULL) {
    echo "userId = '" . $user['user_id'] . "';";
}
?>
        if (typeof userId !== "undefined")
        {
            // alert(user);return;
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>common/addToFavourite",
                dataType: "json",
                data: {
                    designid: designid,
                    uid: userId,
                    type: type
                },
                success: function(msg) {
                    if (type === "Favorite") {
                        alert('Your shoe was successfully added to favorites.');
                        $('#addTofavId').removeAttr('href');
                        //$('#addTofavId').attr('href',alert('You have already added this shoe to your favorites'));

                    }
                    else {
                        $('#likeorliked').text('You have liked this shoe');
                        $('#likeorliked').removeAttr('href');
                    }
                    $("#likemsg").html("");
                    $("#favmsg").html("");
                    var $counter = '';
                    var $likecounter = '';
                    if (msg.fav.cnt > 0) {
                        $counter = $("<span class='number' id='favcount'>").text(msg.fav.cnt);
                    }
                    if (msg.like.cnt > 0) {
                        $likecounter = $("<span class='number' id='likecount'>").text(msg.like.cnt);
                    }
                    $("#favmsg").html($counter);
                    $("#likemsg").html($likecounter);
                    if (msg.fav.cnt == 1) {
                        $("#favmsg").append(" Favorite");
                    } else if (msg.fav.cnt > 1) {
                        $("#favmsg").append(" Favorites");
                    }
                    if (msg.like.cnt == 1) {
                        $("#likemsg").append(" Like");
                    } else if (msg.like.cnt > 1) {
                        $("#likemsg").append(" Likes");
                    }
                }
            });
        }
        else
        {
            Common.setRedirect(baseUrl + 'common/inspiration/' + designid , function() {
                window.location.href = baseUrl + 'common/login';
            });
        }
    }
    function DeleteSavedDesign() {
        if (confirm("Are you sure you want to delete this design? This cannot be undone.")) {
            var userId;
            var designid;
<?php
$users = $this->session->userdata('user_details');
if ($user != NULL) {
    echo "userId = '" . $users['user_id'] . "';";
    echo "designid = '" . $shoedesign['shoe_design_id'] . "'; ";
}
?>

            if (typeof userId !== "undefined")
            {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>common/DeleteSavedDesign",
                    data: {
                        designid: designid,
                        uid: userId
                    },
                    success: function(msg) {
                        if (msg == 1) {
                            closeSelectionPopup();
                            $('#account_designs').find('div.item[data-id=' + designid + ']').remove();
                        } else {
                            alert('Purchased Shoe cannot be deleted!');
                        }
                    }
                });
            }
        }
    }
    function removeFromFav(type) {
        var userId;
        var designid;
<?php
$userr = $this->session->userdata('user_details');
if ($user != NULL) {
    echo "userId = '" . $userr['user_id'] . "';";
    echo "designid = '" . $shoedesign['shoe_design_id'] . "'; ";
}
?>

        if (typeof userId !== "undefined")
        {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>common/RemoveFromFav",
                data: {
                    designid: designid,
                    uid: userId
                },
                success: function(msg) {
                    ShoeDesign.loadinspiration();
                    closeSelectionPopup();
                    window.location.reload();
                }
            });
        }
    }

    document.onkeydown = function(ev) {
        switch (ev.keyCode) {
            case 37://left
                ShoeDesign.rotateLeft();
                break;
            case 38://up
                break;
            case 39://right
                ShoeDesign.rotateRight();
                break;
            case 40://down
                break;
        }
    };
</script>
