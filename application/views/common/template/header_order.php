<header id="header">
    <section id="toolbar" class="clearfix">
        <div class="toolbarInner clearfix">	
            <div class="wrapper content">
                <div id="breadcrumb" class="left">
                    <a href="<?php echo base_url();?>">Home</a>
                    <span>CREATE A NEW SHOE</span>
                </div>
                <div class="right"> 
                    <ul id="headerNavLinks">

                        <li class="top_our_story">
                            <a href="<?php echo base_url();?>our-story">Our Story</a>
                        </li> 
                        <?php $this->load->view('common/login_status');?>
                        <li class="cart">
                            <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </section>
    <h1 id="logo">
        <a href="<?php echo base_url();?>">Awl&Sundry</a>
    </h1>
    <div class="wrapper shopping_cart_pop clearfix">
        <section class="shopping_cart_mini"></section>
    </div>
</header>