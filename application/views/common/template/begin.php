<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php if(isset($title_text)) echo $title_text; else echo 'Awl &amp; Sundry'; ?></title>
<meta name="description" content="<?php echo @$desc; ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=yes">
    <?php
    if (isset($meta)) {
        echo $meta;
    }
    ?>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--[if lte IE 9]>
        <link href="<?php echo base_url(); ?>assets/css/ie.css"  media="screen" rel="stylesheet" type="text/css">
    <![endif]-->
    <link href="<?php echo base_url(); ?>assets/css/layout.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/main.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/js/mediaelementjs/mediaelementplayer.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/camera.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/img/favicon.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <!--[if lt IE 9]>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/html5.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var baseUrl = '<?php echo base_url(); ?>';
    </script>
    <script>(function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', '678040932249205']);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=678040932249205&amp;ev=NoScript" /></noscript>
</head>