<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Awl &amp; Sundry</title>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=yes">
        <?php
        if(isset($meta)){
            echo $meta;
        }

    ?>
   <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--[if lte IE 9]>
        <link href="<?php echo base_url(); ?>assets/css/ie.css"  media="screen" rel="stylesheet" type="text/css">
    <![endif]-->
    <link href="<?php echo base_url(); ?>assets/css/style.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/main.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/layout.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/js/mediaelementjs/mediaelementplayer.min.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/responsive.css" media="screen" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/img/favicon.jpg" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <!--[if lt IE 9]>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/html5.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var baseUrl = '<?php echo base_url(); ?>';
    </script>
</head>