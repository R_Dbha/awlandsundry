<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.10.2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery-easing-1.3.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.easydropdown.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.dropkick-1.0.0.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.sticky.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/tooltipsy.min.js"></script> 

<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/hoverslider.kreaturamedia.jquery.js"></script> -->

<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.8.3.min.js"></script> -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/camera-mymini.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.bxslider.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/mediaelement-and-player.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.tap.min.js"></script>

<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/plugins_slider_video.js"></script>-->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/shoecart.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/customshoe.js"></script>

<!--[if IE]>       

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery.placeholder.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/selectivizr-min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/jquery.corner.js"></script>

<script src="<?php echo base_url(); ?>assets/js/vendor/plugins-ie.js"></script>

<![endif]-->

<!-- Piwik -->

<script type="text/javascript">
    var _paq = _paq || [];
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u = (("https:" == document.location.protocol) ? "https" : "http") + "://awlandsundry.com/piwik/";
        _paq.push(['setTrackerUrl', u + 'piwik.php']);
        _paq.push(['setSiteId', 1]);
        var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.defer = true;
        g.async = true;
        g.src = u + 'piwik.js';
        s.parentNode.insertBefore(g, s);
    })();

    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-40200583-1', 'awlandsundry.com');
    ga('send', 'pageview');

</script>
<script type='text/javascript'>
    window.__wtw_lucky_site_id = 20446;

    (function() {
        var wa = document.createElement('script');
        wa.type = 'text/javascript';
        wa.async = true;
        wa.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://ca20446') + '.luckyorange.com/w.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wa, s);
    })();
</script> 

<noscript><p><img src="http://awlandsundry.com/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>

<!-- End Piwik Code -->

<?php
if (isset($scripts)) {
    foreach ($scripts as $script) {
        ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>.js"></script>
        <?php
    }
}
?>
<!-- Quantcast Tag -->
<script type="text/javascript">
    var _qevents = _qevents || [];

    (function() {
        var elem = document.createElement('script');
        elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
        elem.async = true;
        elem.type = "text/javascript";
        var scpt = document.getElementsByTagName('script')[0];
        scpt.parentNode.insertBefore(elem, scpt);
    })();

    _qevents.push({
        qacct: "p-m_BVuPKPX136j"
    });
</script>

<noscript>
<div style="display:none;">
    <img src="//pixel.quantserve.com/pixel/p-m_BVuPKPX136j.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!-- End Quantcast tag -->

