<div id="stepMain">
    <div class="wrapper clearfix">
        <ul class="createNewNav clearfix">
            <li class="createStep1"><a href="javascript:void(0);" class="active" ><span>Style</span></a><div class="progressBar"></div></li>
            <li class="createStep1"><a href="javascript:void(0);" ><span>Last</span></a></li>
            <li class="createStep2"><a href="javascript:void(0);" class="no-bg"><span>Design features</span></a></li>
            <li class="createStep3"><a href="javascript:void(0);" class="no-bg"><span>Materials &amp; colors</span></a></li>
            <li class="createStep4"><a href="javascript:void(0);" class="no-bg"><span>Monogram</span></a></li>
            <li class="createStep4"><a href="javascript:void(0);" class="no-bg"><span>Sizing</span></a></li>
        </ul>
        <div class="stepFlowBar" id="stepFlowBar"></div>
        <div class="headline">
            <p id="style_head"> <span> Step 1</span> Pick your preferred style to begin with</p>
       </div>
    </div>
</div>   