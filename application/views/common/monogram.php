<section id="main" class="createNewContainer page wrapper clearfix">
    <div id="stepMain">
        <div class="wrapper clearfix">
            <ul class="createNewNav clearfix">
                <li class="createStep1"><a href="javascript:;" ><span>Style</span></a></li>
                <li class="createStepLast"><a href="javascript:void(0);"  ><span>Last</span></a></li>
                <li class="createStep2"><a href="javascript:;" ><span>Design features</span></a></li>
                <!--<li class="createStep3"><a href="javascript:;" onClick="messagepopup('step3');"><span>Materials & colors</span></a></li>-->
                <li class="createStep3"><a  href="javascript:;"><span>Materials & colors</span></a></li>
                <li class="createStep5"><a class="active" href="javascript:;"><span>Monogram</span></a></li>
                <li class="createStep4"><a href="javascript:;" class="no-bg"><span>Sizing</span></a></li>

                <div class="progressBar"></div>
            </ul>
            <div class="stepFlowBar" id="stepFlowBar"></div>
        </div>
    </div>
    <div class="stepContent wrapper clearfix">
        <div class="headline">
            <p><span>Step 5</span>Add monogram on your shoe</p>
        </div>
        <div id="menu_left" style="text-align: left; width:205px;">
            <div class="add_monogram">
                <p>Type your initials here</p>
                <span>upto 3 characters</span>
                <p>
                    <input type="text" id="size_box" maxlength="3"/>
                    <!--<a id="initial_submit" href="javascript:;" onclick="CustomShoe.stepFive.addText();" class="mbtn blue_btn">DONE</a>-->
                </p>
                <!--<p><a class="mbtn black_btn" href="javascript:;" onclick="CustomShoe.stepFive.showPopup('monogram');">BACK</a></p>-->
                <p><!--<a class="mbtn black_btn" href="javascript:;" onclick="messagepopup('monogram');">BACK</a>--></p>

            </div>
            <div class="remove_monogram">
                <p>DON'T LIKE IT?</p>
                <span>Click here to remove your monogram.</span>
                <p><a href="javascript:;"  id="mg_remove" class="mbtn blue_btn">REMOVE</a></p>
                <p><a href="javascript:;"  id="mg_back" class="mbtn black_btn">EDIT</a></p>
            </div>
        </div>
        <div id="shape_visualization" class="shape_visualization">

            <div class="visualImgWrap">
                <img id="error" src="<?php echo ApplicationConfig::IMG_BASE; ?>none.png" />
                <img id="base" src="" class="float-top" />
                <img id="toe" src="" class="float-top" />
                <img id="vamp" src="" class="float-top" />
                <img id="eyestay" src="" class="float-top" />
                <img id="foxing" src="" class="float-top" />
                <img id="lace" src="" class="float-top" />
                <img id="stitch" src="" class="float-top" />
                <div class="monogram" style="display: none;">
                    <span class="monotext"></span>
                </div>
            </div>
            <div style="display:none" id="save-share">
                <a href="javascript:;" onclick="CustomShoe.saveAndShare();" class="save-share button blue">Save And Share</a>
            </div>
            <!--div id="vis_buttons">
                <span>Top view</span>
                <a class="rotate top" href="javascript:;"></a>
                <span>Rotate view</span>
                <a class="rotate left" href="javascript:;"></a>
                <a class="rotate right" href="javascript:;"></a>
            </div-->
            <div id="last_name">
                <span><?php echo $model['last']['last_name'] . ' / ' . $model['style']['style_name']; ?></span>
            </div>
            <div id="vis_buttons">
                <a class="rotate left" href="javascript:;"></a>
                <a class="rotate right" href="javascript:;" style=""></a>
                <span class="topview_link">
                    <a class="top" href="javascript:;">Top view</a>
                </span>
            </div>
            <div id="price_div">
                <span class="symbol">$</span>
                <span class="price"></span>
                <span class="shipping">W / free shipping</span>
            </div>
        </div>
        <div id="design_main_links" style="display:none !important;">
            <ul class="top">
                <li class="save"><a href="javascript:;"  class="sfoursave" id=""><span>Save your design</span></a></li>
                <li class="purchase"><a href="javascript:;" id="ordersubmit"><span>Purchase</span></a></li>
            </ul>
            <div class="action">
                <div class="default"></div>
                <div class="changable hidden save"><p><strong>save</strong> this design to your personal account scrapbook and return to it later </p></div>
                <div class="changable hidden purchase"><p><strong>submit</strong> your custom handmade shoe order and receive them in about 4 weeks</p></div>
            </div>
        </div>
        <div id="shoe_details" style="display:none!important;">
            <div class="shoe_details_inner" style="text-align: left;">
                <p class="item-title">The Samuel</p>
                <p class="item-subtitle">Derby</p>
                <p class="item-type">oxford lace-up</p>
                <p class="item-desc">Semi-round toe with a roomier forepart. Ideal for the short and wide foot. Full leather sole featuring hand-sewn goodyear welt construction.</p>
                <div id="price_div">
                    <p>Handmade as shown</p>
                    <span class="symbol">$</span>
                    <span class="price">350</span>
                    <span class="shipping">w/ free shipping<br><big>&amp; 30 DAY EXCHANGE</big></span>
                </div>
            </div>
        </div>
    </div>
    <div id="shape_selection_popup" class="top_slide_bg sizingChart shape_popup shape_selection_popup">
        <div class="topSliderInner clearfix">
            <div id="chartTop">
                <a href="javascript:;"  class="top_close"><span>CLOSE SIZING CHART</span></a>
            </div>
            <div class="size_chartDetail">
                <div class="top_title"><span>INTERNATIONAL SIZE CONVERSIONS</span></div>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="sizefirstRow">
                        <th scope="row"><div>US</div></th>
                    <td><div>5</div></td>
                    <td><div>5&frac12;</div></td>
                    <td><div>6</div></td>
                    <td><div>6&frac12;</div></td>
                    <td><div>7</div></td>
                    <td><div>7&frac12;</div></td>
                    <td><div>8</div></td>
                    <td><div>8&frac12;</div></td>
                    <td><div>9</div></td>
                    <td><div>9&frac12;</div></td>
                    <td><div>10</div></td>
                    <td><div>10&frac12;</div></td>
                    <td><div>11</div></td>
                    <td><div>11&frac12;</div></td>
                    <td><div>12</div></td>
                    <td><div>12&frac12;</div></td>
                    <td><div>13</div></td>
                    <td><div>13&frac12;</div></td>
                    <td><div>14</div></td>
                    </tr>
                    <tr>
                        <th scope="row"><div>CAN</div></th>
                    <td><div>5</div></td>
                    <td><div>5&frac12;</div></td>
                    <td><div>6</div></td>
                    <td><div>6&frac12;</div></td>
                    <td><div>7</div></td>
                    <td><div>7&frac12;</div></td>
                    <td><div>8</div></td>
                    <td><div>8&frac12;</div></td>
                    <td><div>9</div></td>
                    <td><div>9&frac12;</div></td>
                    <td><div>10</div></td>
                    <td><div>10&frac12;</div></td>
                    <td><div>11</div></td>
                    <td><div>11&frac12;</div></td>
                    <td><div>12</div></td>
                    <td><div>12&frac12;</div></td>
                    <td><div>13</div></td>
                    <td><div>13&frac12;</div></td>
                    <td><div>14</div></td>
                    </tr>
                    <tr>
                        <th scope="row"><div>UK</div></th>
                    <td><div>4&frac12;</div></td>
                    <td><div>5</div></td>
                    <td><div>5&frac12;</div></td>
                    <td><div>6</div></td>
                    <td><div>6&frac12;</div></td>
                    <td><div>7</div></td>
                    <td><div>7&frac12;</div></td>
                    <td><div>8</div></td>
                    <td><div>8&frac12;</div></td>
                    <td><div>9</div></td>
                    <td><div>9&frac12;</div></td>
                    <td><div>10</div></td>
                    <td><div>10&frac12;</div></td>
                    <td><div>11</div></td>
                    <td><div>11&frac12;</div></td>
                    <td><div>12</div></td>
                    <td><div>12&frac12;</div></td>
                    <td><div>13</div></td>
                    <td><div>13&frac12;</div></td>
                    </tr>
                    <tr>
                        <th scope="row"><div>EU</div></th>
                    <td><div>37</div></td>
                    <td><div>37.5</div></td>
                    <td><div>38</div></td>
                    <td><div>38.5</div></td>
                    <td><div>39</div></td>
                    <td><div>39.5</div></td>
                    <td><div>40</div></td>
                    <td><div>40.5</div></td>
                    <td><div>41</div></td>
                    <td><div>41.5</div></td>
                    <td><div>42</div></td>
                    <td><div>42.5</div></td>
                    <td><div>43</div></td>
                    <td><div>43.5</div></td>
                    <td><div>44</div></td>
                    <td><div>44.5</div></td>
                    <td><div>45</div></td>
                    <td><div>45.5</div></td>
                    <td><div>46</div></td>
                    </tr>
                    <tr>
                        <th scope="row"><div>AUS</div></th>
                    <td><div>4&frac12;</div></td>
                    <td><div>5</div></td>
                    <td><div>5&frac12;</div></td>
                    <td><div>6</div></td>
                    <td><div>6&frac12;</div></td>
                    <td><div>7</div></td>
                    <td><div>7&frac12;</div></td>
                    <td><div>8</div></td>
                    <td><div>8&frac12;</div></td>
                    <td><div>9</div></td>
                    <td><div>9&frac12;</div></td>
                    <td><div>10</div></td>
                    <td><div>10&frac12;</div></td>
                    <td><div>11</div></td>
                    <td><div>11&frac12;</div></td>
                    <td><div>12</div></td>
                    <td><div>12&frac12;</div></td>
                    <td><div>13</div></td>
                    <td><div>13&frac12;</div></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="popup_pattern_bar"><a class="shape_popup_close" href="javascript:;" >&nbsp;</a></div>
    </div>
</section>
<div id="popupOverlay"></div>
<!-- previous step popups begins-->
<div class="messageBox messageBoxRed messageBox_step1">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div>
</div>


<div class="messageBox messageBoxRed messageBox_step2">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will default your leather texture to calf plain black.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/design-features">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div>
</div>


<div class="messageBox messageBoxRed messageBox_step3">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current shoe sizes.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/materials-and-colors">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div>
</div>


<div class="messageBox messageBoxGreen messageBox_monogram">
    <div class="msgTop">
        <div class="msgHead" id="msgHead1">Would you like to add a monogram?</div>
        <div class="msgHead" id="msgHead2" style="display:none;">Select a shoe to add your monogram.</div>
        <div class="msgHead" id="msgHead3" style="display:none;">Select a side of your shoe to add your monogram.</div>
    </div>
    <div class="msgBottom" >
        <div class="msgOptionWrap" id="msgOption1">
            <div class="msgOption"><a href="javascript:;" onclick="CustomShoe.stepFive.nextSelection('#msgOption1', '#msgHead1');" >Yes,</a> <span> Continue.</span></div>
            <div class="msgOption"><a href="javascript:;" onclick="CustomShoe.stepFive.removeMonoNext();">No,</a> <span>  skip adding monogram and proceed to sizing.</span></div>
<!--            <div class="msgOption"><a href="javascript:;" >No,</a> <span> skip adding monogram and save my current design to my account page (account required).</span></div>-->
        </div>
        <div class="msgOptionWrap" id="msgOption2" style="display:none;">
            <div class="bottmLeft">
                <div class="checkbox" id="side">
                    <input id="right" type="radio" name="check" value="<?php echo ApplicationConfig::RIGHT_SHOE; ?>">
                    <label for="right">Right</label>
                    <br>
                    <input id="left" type="radio" name="check" value="<?php echo ApplicationConfig::LEFT_SHOE; ?>">
                    <label for="left">Left</label>
                </div>
            </div>
            <div class="bottomRight">
                <a class="nextBtn" onClick="CustomShoe.stepFive.showPopup();">PREV</a>
                <a class="nextBtn" onClick="CustomShoe.stepFive.selectShoe();">NEXT</a>
            </div>
        </div>
        <div class="msgOptionWrap" id="msgOption3" style="display:none; ">
            <div class="bottmLeft"  >
                <div class="checkbox" id="counter">
                    <div>
                        <input id="rightheel" type="radio" name="check" value="<?php echo ApplicationConfig::RIGHT_SIDE; ?>">
                        <label for="rightheel">Right Counter</label>
                        <img src="<?php echo base_url(); ?>assets/img/icons/help.png" title="" class="q_symbol">
                        <div class="counter first" >
                            <img height="150" src ="<?php echo base_url(); ?>/assets/img/counter.jpg"/>
                        </div>
                    </div>
                    <div>
                        <input id="leftheel" type="radio" name="check" value="<?php echo ApplicationConfig::LEFT_SIDE; ?>">
                        <label for="leftheel">Left Counter</label>
                        <img src="<?php echo base_url(); ?>assets/img/icons/help.png" title="" class="q_symbol">
                        <div class="counter second" >
                            <img height="150" src ="<?php echo base_url(); ?>/assets/img/counter.jpg"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottomRight">
                <a class="nextBtn" onClick="CustomShoe.stepFive.prevSelection('#msgOption3', '#msgHead3');">PREV</a>
                <a class="nextBtn" onClick="CustomShoe.stepFive.selectSide();">NEXT</a>
            </div>
        </div>
    </div>
</div>


<!-- previous step popups ends-->
<div class="messageBox messageBoxRed messageBox_error">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">It appears you have selected different sizes for your shoe</div>
        <div class="msgOption"><a href="javascript:;" onClick="CustomShoe.stepFour.nextStep();">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span>close window and edit your sizes.</span></div>
    </div>
</div>

<div class="messageBox messageBoxRed messageBox_revise">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design and take you back to step 1</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a> <span> start over.</span></div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>login" id="savedesign">Yes,</a> <span> but first save my current design to my account page (account required).</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div>
</div>
<div class="messageBox messageBoxGreen messageBox_save">
    <div class="msgTop"><div class="msgHead">You must be signed in to do that.</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Saving a shoe to your scrapbook requires an account.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>login">Sign In</a></div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>signup">Create an Account</a></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">Not now,</a> <span>do not save.</span></div>
    </div>
</div>

<div class="messageBox messageBoxRed messageBox_revise">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design and take you back to step 1</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a>
            <span>start over.</span>
        </div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>login" id="savedesign">Yes,</a>
            <span>but first save my current design to my account page (account required).</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and return to my current design.</span>
        </div>
    </div>
</div>
<div id="accounts_selection_popup" class="popup"></div>

<div class="messageBox messageBoxRed messageBox_step_last">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-last">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div>
</div>

<div class="messageBox messageBoxBlue messageBox_rename">
    <div class="header_blue"><a class="shape_popup_close" href="javascript:;" onclick="messagepopupclose();">&nbsp;</a></div>
    <div class="popup_content">
        <div class="popup_shoe_image">
            <img src="<?php echo base_url(); ?>files/images/OX_L2_T01V00E00F00M0C0_A0.png"/>
        </div>
        <div class="popup_shoe_desc">
            <div class="shoe_rename_box">
                <h1 class="shoe_share_title">Rename</h1>
                <p class="shoe_share_summary">Get creative! Give your shoe a personality by giving it an unique name and share it with your friends.</p>
                <div id="shoe_edit_box">
                    <div class="edit_first">
                        <span class="shoe_title">NAME YOUR SHOE:</span>
                        <h2 class="shoe_name">Shoe Name</h2>
                        <a href="javascript:;" class="common_button" id="edit_name">EDIT</a>
                    </div>
                    <div class="edit_last">
                        <span class="shoe_title">UPTO 10 CHARACTERS</span>
                        <input type="text" id="shoe_name_input" />
                        <a href="javascript:;" class="common_button" id="save_name">SAVE</a>
                    </div>
                </div>
                <a href="javascript:;" id="save_share">SAVE &amp; SHARE</a>
            </div>
            <div class="shoe_share_box">
                <h1 class="shoe_share_title">Saved!</h1>
                <p class="shoe_share_summary">Announce the official birth of your shoes by sharing it to the world.</p>
                <p class="social_links">

                    <a target="_blank" href="http://twitter.com/share?url=<?php echo ApplicationConfig::ROOT_BASE; ?>files/designs/658468_A0.png" class="social" id="twitter"></a>
                    <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo ApplicationConfig::ROOT_BASE; ?>files/designs/658468_A0.png" class="social" id="fb"></a>
                    <a target="_blank" href="//www.pinterest.com/pin/create/button/?url=<?php echo ApplicationConfig::ROOT_BASE; ?>files/designs/658468_A0.png" class="social" id="pinterest"></a>
                    <a href="javascript:;" class="social" id="instagram"></a>
                </p>
                <ul>
                    <li class="create"><a href="javascript:;"  onclick="messagepopupclose();"><span>Continue</span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="popup_pattern_bar"></div>
</div>
<div class="controls">
    <a id="prev" class="control" href="javascript:;"></a>
    <a id="next" class="control" href="javascript:;" ></a>
</div>
<style>


    .jcarousel-item img {
        width: 162px;
    }

    .color-elem {
        height: 25px;
        width: 29px;
        border: 1px solid #ccc;
    }
</style>
<?php $this->load->view('common/template/jscontainer'); ?>
<script type="text/javascript">
    var MonogramConfig = {
        leftShoe: '<?php echo ApplicationConfig::LEFT_SHOE; ?>',
        rightShoe: '<?php echo ApplicationConfig::RIGHT_SHOE; ?>',
        leftSide: '<?php echo ApplicationConfig::LEFT_SIDE; ?>',
        rightSide: '<?php echo ApplicationConfig::RIGHT_SIDE; ?>'
    };
    function showNextStep(content, title) {
        $(content).hide();
        $(title).hide();
        $(content).next().show();
        $(title).next().show();
    }



    function popupshow(name) {
        messagepopup(name);
    }
    function popuphide(name) {
        jQuery(".messageBox").removeClass('popupbounce');
        jQuery('.messageBox').animate({"height": "0", "top": "0"}, 500, function() {
            jQuery('.messageBox').css("display", "none");
            //jQuery('#popupOverlay').fadeOut();
            popupshow(name);

        });

    }
    $(document).ready(function() {

        CustomShoe.stepFive.init(<?php echo json_encode($this->session->userdata('step_data')) . ",'" . $redirectUrl . "'"; ?>);

        $('.top_close').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.closePopup();
        });
        $('.shape_popup_close').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.closePopup();
        });
        $('#mg_remove').on('click touchend', function() {
            CustomShoe.stepFive.removeMonoNext();
        });
        $('#mg_back').on('click touchend', function() {
            CustomShoe.stepFive.addedBack();
        });

        $('#next').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFive.nextStep();
        });
        $('#prev').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFive.nextStep('materialcolor');
        });
        $("#size_box").keyup(CustomShoe.stepFive.addText);

        $('#edit_name').off('click touchend').on('click touchend', function() {
            $('.edit_first').hide();
            $('.edit_last').show();

        });


        $('#save_name').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.updateName('no');
        });

        $('#save_share').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.updateName('yes');
        });
    });

</script>