<section id="main" class="createNewContainer page wrapper clearfix">
    <div id="stepMain">
        <div class="wrapper clearfix">
            <ul class="createNewNav clearfix">
                <li class="createStep1">
                    <a href="javascript:;" >
                        <span>Style</span>
                    </a>
                </li>
                <li class="createStepLast">
                    <a href="javascript:void(0);"  >
                        <span>Last</span>
                    </a>
                </li>
                <li class="createStep2">
                    <a href="javascript:;" >
                        <span>Design features</span>
                    </a>
                </li>
                <!--<li class="createStep3"><a href="javascript:;" onClick="messagepopup('step3');"><span>Materials & colors</span></a></li>-->
                <li class="createStep3">
                    <a href="javascript:;" >
                        <span>Materials & colors</span>
                    </a>
                </li>
                <li class="createStep5">
                    <a href="javascript:;" >
                        <span>Monogram</span>
                    </a>
                </li>
                <li class="createStep4">
                    <a class="active" href="javascript:;">
                        <span>Sizing</span>
                    </a>
                </li>
                <div class="progressBar"></div>
            </ul>
            <div class="stepFlowBar" id="stepFlowBar"></div>
        </div>
    </div>
    <div class="stepContent wrapper clearfix">
        <div class="headline">
            <p>
                <span>Step 6</span>Select your sizes</p>
        </div>
        <div id="menu_left">
            <ul id="side_links">
                <li>
                    <a class="view" href="javascript:;">Size Chart</a>
                </li>
                <li>
                    <a class="proper" href="<?php echo ApplicationConfig::ROOT_BASE; ?>files/131123_A&S_size_guide.pdf" target="_blank">Size Guide</a>
                </li>
                <!--<li><a class="rename" href="javascript:;" onClick="CustomShoe.stepFour.renameShare();">Rename &amp; Share</a></li>-->
                <!--<li><a class="revise" href="javascript:;" onClick="messagepopup('revise');">Revise design</a></li> -->
            </ul>
        </div>
        <div id="button_right">
            <ul id="button_links">
                <li><a class="rename" href="javascript:;" >Name &AMP; Save</a></li>
                <li><a class="save_d sfoursave" id="" href="javascript:;">Own it</a></li>

            </ul>
        </div>
        <!--<div id="menu_right">
            <div id="top">
                <span class="title">Sizing charts</span>
                <a href="javascript:;" class="close">Close sizing charts</a>
            </div>
            
            <div id="menu_right_content">
                <p>(Sizing chart)</p>
            </div>
        </div>-->
        <div id="shape_visualization" class="shape_visualization">
            <?php if (strtolower($model['style']['style_name']) === 'loafer') { ?><p class="tip">We recommend ordering half size smaller for all of our loafer models</p> <?php } ?>
            <!--<div class="save_d" id="sfoursave">Save your design</div>-->
            <div class="shoeSizeBoxWrap">
                <form id="right_shoe_form" class="clearfix" method="post" >
                    <div id="right_shoe" class="shoe_wrap">
                        <label for="right_length">Right</label>
                        <select name="right_length" id="rightlength" class="dropdown">
                            <option value="all"  selected="selected" >Select:</option>
                            <option value="5">5 </option>
                            <option value="5.5">5.5 </option>
                            <option value="6">6 </option>
                            <option value="6.5">6.5 </option>
                            <option value="7">7 </option>
                            <option value="7.5">7.5 </option>
                            <option value="8">8 </option>
                            <option value="8.5">8.5 </option>
                            <option value="9">9 </option>
                            <option value="9.5">9.5 </option>
                            <option value="10">10 </option>
                            <option value="10.5">10.5 </option>
                            <option value="11">11 </option>
                            <option value="11.5">11.5 </option>
                            <option value="12">12 </option>
                            <option value="12.5">12.5 </option>
                            <option value="13">13 </option>
                            <option value="13.5">13.5 </option>
                            <option value="14">14 </option>
                        </select>
                    </div>
                    <div id="left_shoe" class="shoe_wrap">
                        <label for="left_length">Left</label>
                        <select name="left_length" id="leftlength" class="dropdown"> 
                            <option value="all"  selected="selected" >select:</option>
                            <option value="5">5 </option>
                            <option value="5.5">5.5 </option>
                            <option value="6">6 </option>
                            <option value="6.5">6.5 </option>
                            <option value="7">7 </option>
                            <option value="7.5">7.5 </option>
                            <option value="8">8 </option>
                            <option value="8.5">8.5 </option>
                            <option value="9">9 </option>
                            <option value="9.5">9.5 </option>
                            <option value="10">10 </option>
                            <option value="10.5">10.5 </option>
                            <option value="11">11 </option>
                            <option value="11.5">11.5 </option>
                            <option value="12">12 </option>
                            <option value="12.5">12.5 </option>
                            <option value="13">13 </option>
                            <option value="13.5">13.5 </option>
                            <option value="14">14 </option>
                        </select>
                    </div>                        
                </form>
            </div>
            <div class="visualImgWrap">
                <img id="error" src="<?php echo ApplicationConfig::IMG_BASE; ?>none.png" />
                <img id="base" src="" class="float-top" />
                <img id="toe" src="" class="float-top" />
                <img id="vamp" src="" class="float-top" />
                <img id="eyestay" src="" class="float-top" />
                <img id="foxing" src="" class="float-top" />
                <img id="lace" src="" class="float-top" />
                <img id="stitch" src="" class="float-top" />
                <div class="monogram" style="display: none;">
                    <span class="monotext"></span>
                </div>
            </div>
            <div id="save-share">
                <a href="javascript:;" onclick="CustomShoe.saveAndShare();" class="save-share button blue">Save And Share</a>
            </div>
            <div id="last_name">
                <span><?php echo $model['last']['last_name'] . ' / ' . $model['style']['style_name']; ?></span>
            </div>
            <div id="vis_buttons">                     
                <a class="rotate left" href="javascript:;"></a>
                <a class="rotate right" href="javascript:;" style=""></a>
                <span class="topview_link">
                    <a class="top" href="javascript:;">Top view</a>
                </span>
            </div>
            <div id="price_div">
                <span class="symbol">$</span>
                <span class="price"></span>
                <span class="shipping">W / free shipping</span>
            </div>
            <!--div id="vis_buttons">
               <span>Rotate view</span>                        
               <a class="rotate left" href="javascript:;"></a>
               <a class="rotate right" href="javascript:;"></a>
               <a class="top" href="javascript:;">Top view</span>
            <!--a class="rotate top" href="javascript:;"></a-->

        </div>
    </div>
    <div id="design_main_links">
        <ul class="top">
            <li class="save">
                <a href="javascript:;" class="sfoursave" id="">
                    <span>Save your design</span>
                </a>
            </li>
            <li class="purchase">
                <a href="javascript:;" id="ordersubmit">
                    <span>Purchase</span>
                </a>
            </li>
        </ul>
        <div class="action">
            <div class="default"></div>
            <div class="changable hidden save">
                <p>
                    <strong>save</strong>this design to your personal account scrapbook and return to it later</p>
            </div>
            <div class="changable hidden purchase">
                <p>
                    <strong>submit</strong>your custom handmade shoe order and receive them in about 4 weeks</p>
            </div>
        </div>
    </div>
    <div id="shoe_details" style="disply:block;">
        <div class="shoe_details_inner">
            <p class="item-title">The Samuel</p>
            <p class="item-subtitle">Derby</p>
            <!--p class="item-type">oxford lace-up</p-->
            <!--<p class="item-desc">Semi-round toe with a roomier forepart. Ideal for the short and wide foot. Full leather sole featuring hand-sewn goodyear welt construction.</p>-->
            <div id="price_div">
                <!--p>Handmade as shown</p-->
                <span class="symbol">$</span>
                <span class="price">350</span>
                <span class="shipping">w/ free shipping
                    <br>
                    <big>&amp; 30 DAY EXCHANGE</big>
                </span>
            </div>
        </div>
    </div>
</div>
<div id="shape_selection_popup" class="top_slide_bg sizingChart shape_popup shape_selection_popup">
    <div class="topSliderInner clearfix">
        <div id="chartTop">
            <a href="javascript:;" onClick="CustomShoe.stepFour.closePopup();" class="top_close">
                <span>CLOSE SIZING CHART</span>
            </a>
        </div>
        <div class="size_chartDetail">
            <div class="top_title">
                <span>INTERNATIONAL SIZE CONVERSIONS</span>
            </div>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr class="sizefirstRow">
                    <th scope="row">
                <div>US</div>
                </th>
                <td>
                    <div>5</div>
                </td>
                <td>
                    <div>5&frac12;</div>
                </td>
                <td>
                    <div>6</div>
                </td>
                <td>
                    <div>6&frac12;</div>
                </td>
                <td>
                    <div>7</div>
                </td>
                <td>
                    <div>7&frac12;</div>
                </td>
                <td>
                    <div>8</div>
                </td>
                <td>
                    <div>8&frac12;</div>
                </td>
                <td>
                    <div>9</div>
                </td>
                <td>
                    <div>9&frac12;</div>
                </td>
                <td>
                    <div>10</div>
                </td>
                <td>
                    <div>10&frac12;</div>
                </td>
                <td>
                    <div>11</div>
                </td>
                <td>
                    <div>11&frac12;</div>
                </td>
                <td>
                    <div>12</div>
                </td>
                <td>
                    <div>12&frac12;</div>
                </td>
                <td>
                    <div>13</div>
                </td>
                <td>
                    <div>13&frac12;</div>
                </td>
                <td>
                    <div>14</div>
                </td>
                </tr>
                <tr>
                    <th scope="row">
                <div>CAN</div>
                </th>
                <td>
                    <div>5</div>
                </td>
                <td>
                    <div>5&frac12;</div>
                </td>
                <td>
                    <div>6</div>
                </td>
                <td>
                    <div>6&frac12;</div>
                </td>
                <td>
                    <div>7</div>
                </td>
                <td>
                    <div>7&frac12;</div>
                </td>
                <td>
                    <div>8</div>
                </td>
                <td>
                    <div>8&frac12;</div>
                </td>
                <td>
                    <div>9</div>
                </td>
                <td>
                    <div>9&frac12;</div>
                </td>
                <td>
                    <div>10</div>
                </td>
                <td>
                    <div>10&frac12;</div>
                </td>
                <td>
                    <div>11</div>
                </td>
                <td>
                    <div>11&frac12;</div>
                </td>
                <td>
                    <div>12</div>
                </td>
                <td>
                    <div>12&frac12;</div>
                </td>
                <td>
                    <div>13</div>
                </td>
                <td>
                    <div>13&frac12;</div>
                </td>
                <td>
                    <div>14</div>
                </td>
                </tr>
                <tr>
                    <th scope="row">
                <div>UK</div>
                </th>
                <td>
                    <div>4</div>
                </td>
                <td>
                    <div>4&frac12;</div>
                </td>
                <td>
                    <div>5</div>
                </td>
                <td>
                    <div>5&frac12;</div>
                </td>
                <td>
                    <div>6</div>
                </td>
                <td>
                    <div>6&frac12;</div>
                </td>
                <td>
                    <div>7</div>
                </td>
                <td>
                    <div>7&frac12;</div>
                </td>
                <td>
                    <div>8</div>
                </td>
                <td>
                    <div>8&frac12;</div>
                </td>
                <td>
                    <div>9</div>
                </td>
                <td>
                    <div>9&frac12;</div>
                </td>
                <td>
                    <div>10</div>
                </td>
                <td>
                    <div>10&frac12;</div>
                </td>
                <td>
                    <div>11</div>
                </td>
                <td>
                    <div>11&frac12;</div>
                </td>
                <td>
                    <div>12</div>
                </td>
                <td>
                    <div>12&frac12;</div>
                </td>
                <td>
                    <div>13</div>
                </td>
                </tr>
                <tr>
                    <th scope="row">
                <div>EU</div>
                </th>
                <td>
                    <div>37</div>
                </td>
                <td>
                    <div>37.5</div>
                </td>
                <td>
                    <div>38</div>
                </td>
                <td>
                    <div>38.5-39</div>
                </td>
                <td>
                    <div>39.5</div>
                </td>
                <td>
                    <div>40</div>
                </td>
                <td>
                    <div>40.5</div>
                </td>
                <td>
                    <div>41-41.5</div>
                </td>
                <td>
                    <div>42</div>
                </td>
                <td>
                    <div>42.5</div>
                </td>
                <td>
                    <div>43-43.5</div>
                </td>
                <td>
                    <div>44</div>
                </td>
                <td>
                    <div>44.5</div>
                </td>
                <td>
                    <div>45</div>
                </td>
                <td>
                    <div>45.5-46</div>
                </td>
                <td>
                    <div>46.5</div>
                </td>
                <td>
                    <div>47</div>
                </td>
                <td>
                    <div>47.5</div>
                </td>
                <td>
                    <div>48</div>
                </td>
                </tr>
                <tr>
                    <th scope="row">
                <div>AUS</div>
                </th>
                <td>
                    <div>4</div>
                </td>
                <td>
                    <div>4&frac12;</div>
                </td>
                <td>
                    <div>5</div>
                </td>
                <td>
                    <div>5&frac12;</div>
                </td>
                <td>
                    <div>6</div>
                </td>
                <td>
                    <div>6&frac12;</div>
                </td>
                <td>
                    <div>7</div>
                </td>
                <td>
                    <div>7&frac12;</div>
                </td>
                <td>
                    <div>8</div>
                </td>
                <td>
                    <div>8&frac12;</div>
                </td>
                <td>
                    <div>9</div>
                </td>
                <td>
                    <div>9&frac12;</div>
                </td>
                <td>
                    <div>10</div>
                </td>
                <td>
                    <div>10&frac12;</div>
                </td>
                <td>
                    <div>11</div>
                </td>
                <td>
                    <div>11&frac12;</div>
                </td>
                <td>
                    <div>12</div>
                </td>
                <td>
                    <div>12&frac12;</div>
                </td>
                <td>
                    <div>13</div>
                </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="popup_pattern_bar">
        <a class="shape_popup_close" href="javascript:;" onClick="CustomShoe.stepFour.closePopup();">&nbsp;</a>
    </div>
</div>
</section>
<div id="popupOverlay"></div>
<!-- previous step popups begins-->
<div class="messageBox messageBoxRed messageBox_step1">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design.</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a>
            <span>Continue.</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and return to my current design.</span>
        </div>
    </div>
</div>


<div class="messageBox messageBoxRed messageBox_step2">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will default your leather texture to calf plain black.</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>create-a-custom-shoe/design-features">Yes,</a>
            <span>Continue.</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and return to my current design.</span>
        </div>
    </div>
</div>


<div class="messageBox messageBoxRed messageBox_step3">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current shoe sizes.</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>create-a-custom-shoe/materials-and-colors">Yes,</a>
            <span>Continue.</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and return to my current design.</span>
        </div>
    </div>
</div>
<div class="messageBox messageBoxRed messageBox_step_last">
    <div class="msgTop"><div class="msgHead">Are you sure you want to do that?</div></div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design.</div>
        <div class="msgOption"><a href="<?php echo base_url(); ?>create-a-custom-shoe/select-last">Yes,</a> <span> Continue.</span></div>
        <div class="msgOption msgOptionClose"><a href="javascript:;" onClick="messagepopupclose();">No,</a> <span> close window and return to my current design.</span></div>
    </div> 
</div>


<!-- previous step popups ends-->
<div class="messageBox messageBoxRed messageBox_error">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">It appears you have selected different sizes for your shoe</div>
        <div class="msgOption">
            <a href="javascript:;" onClick="CustomShoe.stepFour.nextStep();">Yes,</a>
            <span>Continue.</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and edit your sizes.</span>
        </div>
    </div>
</div>

<div class="messageBox messageBoxRed messageBox_revise">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design and take you back to step 1</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a>
            <span>start over.</span>
        </div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>login" id="savedesign">Yes,</a>
            <span>but first save my current design to my account page (account required).</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and return to my current design.</span>
        </div>
    </div>
</div>

<div class="messageBox messageBoxRed messageBox_revise">
    <div class="msgTop">
        <div class="msgHead">Are you sure you want to do that?</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Selecting this option will delete your current design and take you back to step 1</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">Yes,</a>
            <span>start over.</span>
        </div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>login" id="savedesign">Yes,</a>
            <span>but first save my current design to my account page (account required).</span>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">No,</a>
            <span>close window and return to my current design.</span>
        </div>
    </div>
</div>

<div id="popupOverlay"></div>
<div id="accounts_selection_popup" class="popup"></div>

<div class="messageBox messageBoxGreen messageBox_save">
    <div class="msgTop">
        <div class="msgHead">You must be signed in to do that.</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">
            Saving a shoe to your scrapbook requires an account.</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>login">Sign In</a>
        </div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>signup">Create an Account</a>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">Not now </a>
            <img src="<?php echo base_url(); ?>assets/img/icons/help.png" title="" class="q_symbol">
            <div class="desc" >
                <span>
                    In order to name your shoe and <br/>for the community to view your design, <br/>you need to create an account
                </span>
            </div>
        </div>
    </div>
</div>
<div class="messageBox messageBoxGreen messageBox_share">
    <div class="msgTop">
        <div class="msgHead">You must be signed in to do that.</div>
    </div>
    <div class="msgBottom">
        <div class="msgDesc">Renaming and sharing your shoe require an account.</div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>login">Sign In</a>
        </div>
        <div class="msgOption">
            <a href="<?php echo base_url(); ?>signup">Create an Account</a>
        </div>
        <div class="msgOption msgOptionClose">
            <a href="javascript:;" onClick="messagepopupclose();">Not now,</a>
            <span>do not rename and share.</span>
        </div>
    </div>
</div>
<div class="messageBox messageBoxBlue messageBox_rename">
    <div class="header_blue">
        <a class="shape_popup_close" href="javascript:;" onclick="messagepopupclose();">&nbsp;</a>
    </div>
    <div class="popup_content">
        <div class="popup_shoe_image">
            <img src="<?php echo ApplicationConfig::ROOT_BASE; ?>files/images/OX_L2_T01V00E00F00M0C0_A0.png" />
        </div>
        <div class="popup_shoe_desc">
            <div class="shoe_rename_box">
                <h1 class="shoe_share_title">Rename</h1>
                <p class="shoe_share_summary">Get creative! Give your shoe a personality by giving it an unique name and share it with your friends.</p>
                <div id="shoe_edit_box">
                    <div class="edit_first">
                        <span class="shoe_title">NAME YOUR SHOE:</span>
                        <h2 class="shoe_name">Shoe Name</h2>
                        <a href="javascript:;" class="common_button" id="edit_name">EDIT</a>
                    </div>
                    <div class="edit_last">
                        <span class="shoe_title">UPTO 15 CHARACTERS</span>
                        <input type="text" id="shoe_name_input" maxlength="15"/>
                        <!--a href="javascript:;" class="common_button" id="save_name">SAVE</a--> 
                    </div>
                </div>
                <a href="javascript:;" id="save_share">SAVE &amp; SHARE</a>
            </div>
            <div class="shoe_share_box">
                <h1 class="shoe_share_title">Saved!</h1>
                <p class="shoe_share_summary">Announce the official birth of your shoes by sharing it to the world.</p>
                <p class="social_links">
                    <a  target="_blank" href="http://twitter.com/share?url=<?php echo base_url(); ?>get-inspired/##&text=Checkout this custom pair of shoes I just designed at Awlandsundry.com" class="social" id="twitter"></a>
                    <a  target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo base_url(); ?>get-inspired/##" class="social" id="fb"></a>
                    <a  target="_blank" href="http://www.pinterest.com/pin/create/button/?url=<?php echo ApplicationConfig::ROOT_BASE; ?>files/designs/##_A0.png" class="social" id="pinterest"></a>
                    <a href="javascript:;" class="social" id="instagram"></a>
                </p>

            </div>
        </div>
    </div>
    <div class="popup_pattern_bar"></div>
</div>
<div class="controls">
    <a id="prev" class="control" href="javascript:;" ></a>
    <a id="next" class="control" href="javascript:;" ></a>
</div>
<style>
    .jcarousel-item img {
        width: 162px;
    }
    #shape_visualization .visualImgWrap img.float-top {
        position: absolute;
        top: 0;
        left: 0;
        margin-top: 60px;
    }
    #shape_visualization div.monogram_A4 {
        height: 19px;
        left: 580px;
        position: absolute;
        top: 200px;
        z-index: 2;
        min-width: 100px;
    }
    #shape_visualization div.monogram_A0 {
        height: 19px;
        left: 155px;
        position: absolute;
        top: 200px;
        z-index: 2;
        min-width: 100px;
    }
    #shape_visualization .monogram span.monotext {
        color: rgba(0, 0, 0, 0);
        font-size: 25px;
        font-weight: bold;
        opacity: 0.5;
        text-shadow: 1px 1px rgba(175, 175, 175, 0.2), -1px -1px #060606;
        text-transform: uppercase;
    }

    #shape_visualization .monogram img {
        float: left;
        height: 100% !important;
        margin-right: 2px;
        width: auto !important;
    }
    .color-elem {
        height: 25px;
        width: 29px;
        border: 1px solid #ccc;
    }
</style>
<?php $this->load->view('common/template/jscontainer'); ?>
<script type="text/javascript">
    $(document).ready(function() {

        CustomShoe.stepFour.init(<?php echo json_encode($this->session->userdata('step_data')); ?>);
        $('.edit_first').hide();
        $('.edit_last').show();
        $('#edit_name').off('click touchend').on('click touchend', function() {
            $('.edit_first').hide();
            $('.edit_last').show();
        });
        $('#next').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.validate();
        });
        $('#prev').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.nextStep('monogram');
        });

        $('#save_name').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.updateName('no');
        });
        $('.rename').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.renameShare();
        });

        $('#save_share').off('click touchend').on('click touchend', function() {
            CustomShoe.stepFour.updateName('yes');
        });

        $('#rightlength').change(function() {
            var dropdown, rightSelected;
            if ($('#leftlength option:selected').val() === 'all' || $('#leftlength option:selected').val() === undefined) {

                //if(ShoeModel.size.left.value == 'all'){
                rightSelected = $('#rightlength option:selected').val();
                $('#leftlength').val(rightSelected);
                dropdown = $('#leftlength').parents('.dropdown');
                dropdown.find('li').removeClass('active');
                dropdown.find('li:contains(' + rightSelected + ')').addClass('active');
                dropdown.find('span.selected').text(rightSelected);
                // $('#dk_container_leftlength .dk_options_inner li').removeClass('dk_option_current');
                // $('#dk_container_leftlength .dk_options_inner li a[data-dk-dropdown-value="' +  $('#rightlength option:selected').text() + '"]').parent().addClass('dk_option_current');
                //  $('#dk_container_leftlength .dk_toggle .dk_label').text($('#rightlength option:selected').text());
            }
            if (ShoeModel.size.left.value === 'all') {

                rightSelected = $('#rightlength option:selected').val();
                $('#leftlength').val(rightSelected);
                dropdown = $('#leftlength').parents('.dropdown');
                dropdown.find('li').removeClass('active');
                dropdown.find('li:contains(' + rightSelected + ')').addClass('active');
                dropdown.find('span.selected').text(rightSelected);
            }
        });

        $('#leftlength').change(function() {
            var dropdown, leftSelected;
            if ($('#rightlength option:selected').val() === 'all' || $('#rightlength option:selected').val() === undefined) {
                leftSelected = $('#leftlength option:selected').val();
                $('#rightlength').val(leftSelected);
                dropdown = $('#rightlength').parents('.dropdown');
                dropdown.find('li').removeClass('active');
                dropdown.find('li:contains(' + leftSelected + ')').addClass('active');
                dropdown.find('span.selected').text(leftSelected);
                //if(ShoeModel.size.right.value == 'all'){
                //  $('#rightlength').val($('#leftlength option:selected').val());
                //  $('#dk_container_rightlength .dk_options_inner li').removeClass('dk_option_current');
                //   $('#dk_container_rightlength .dk_options_inner li a[data-dk-dropdown-value="' + $('#leftlength option:selected').text() + '"]').parent().addClass('dk_option_current');
                // $('#dk_container_rightlength .dk_toggle .dk_label').text($('#leftlength option:selected').text());
            }
            else if (ShoeModel.size.right.value === 'all') {
                leftSelected = $('#rightlength option:selected').val();
                $('#rightlength').val(leftSelected);
                dropdown = $('#rightlength').parents('.dropdown');
                dropdown.find('li').removeClass('active');
                dropdown.find('li:contains(' + leftSelected + ')').addClass('active');
                dropdown.find('span.selected').text(leftSelected);
            }

        });
    });
</script>