<section class="main">

    <div class="wrapper clearfix">
        <h2>shopping cart</h2>
        <section class="shopping_cart clearfix">
            <?php
            foreach ($this->cart->contents() as $items):
                if (strtolower($items['name']) == 'gift card') {
                    continue;
                } else {
                    ?>
                    <div class="headlines">
                        <span class="shoe">Shoe</span>
                        <span class="shoe_style">Style</span>
                        <span class="sizes">Sizes</span>
                        <span class="quantity">Quantity</span>
                        <span class="price">Price</span>
                        <span class="remove"></span>
                    </div>
                    <?php
                    break;
                }
            endforeach;
            $angle = "_A1";
            $exte = ".png";
            $baseurl = ApplicationConfig::IMG_BASE;
            $i = 0;
            foreach ($this->cart->contents() as $items):
                if (strtolower($items['name']) == 'gift card') {
                    continue;
                }
                $i++;
                $options = $this->cart->product_options($items['rowid']);
                $shoe = json_decode($options['shoe'], TRUE);
                $lastStyle = $this->CustomShoeModel->get_last_style_details($shoe['style']['id']);
                $imgBase = $shoe['style']['code'] . "_" . $shoe['toe']['type'] . $shoe['vamp']['type'] . $shoe['eyestay']['type'] . $shoe['foxing']['type'];
                $base = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['quarter']['matfolder'] . "/" . $shoe['style']['code'] . "_" . $shoe['quarter']['base'] . $angle . $exte;
                $toe = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['toe']['matfolder'] . "/" . $imgBase . $shoe['toe']['material'] . $shoe['toe']['color'] . $shoe['toe']['part'] . $angle . $exte;
                $eyestay = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['eyestay']['matfolder'] . "/" . $imgBase . $shoe['eyestay']['material'] . $shoe['eyestay']['color'] . $shoe['eyestay']['part'] . $angle . $exte;
                $foxing = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['foxing']['matfolder'] . "/" . $imgBase . $shoe['foxing']['material'] . $shoe['foxing']['color'] . $shoe['foxing']['part'] . $angle . $exte;
                $stitch = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['stitch']['folder'] . "/" . $imgBase . $shoe['stitch']['code'] . $angle . $exte;
                $lace = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['lace']['folder'] . "/" . $imgBase . $shoe['lace']['code'] . $angle . $exte;
                ?>
                <div class="cart-item">
                    <div class="shoe">
                        <img onerror="this.style.display = 'none';" src="<?php echo $baseurl . $base; ?>" />
                        <img onerror="this.style.display = 'none';" src="<?php echo $baseurl . $toe; ?>" class="float-top" />
                        <img onerror="this.style.display = 'none';" src="<?php echo $baseurl . $eyestay; ?>" class="float-top" />
                        <img onerror="this.style.display = 'none';" src="<?php echo $baseurl . $foxing; ?>" class="float-top" />
                        <img onerror="this.style.display = 'none';" src="<?php echo $baseurl . $stitch; ?>" class="float-top" />
                        <img onerror="this.style.display = 'none';" src="<?php echo $baseurl . $lace; ?>" class="float-top" />
                    </div>
                    <div class="shoe_style"><span>Last <?php echo $lastStyle['last_name'] ?></span><br/><?php echo $lastStyle['style_name']; ?><br/></div>
                    <div class="sizes">LEFT SHOE: <span><?php echo $shoe['size']['left']['text']; ?></span><br/>RIGHT SHOE: <span><?php echo $shoe['size']['right']['text']; ?></span></div>
                    <div class="quantity"><?php echo $items['qty']; ?> PAIR</div>
                    <div class="price">$ <?php echo $items['price'] . '.00' ?></div>
                    <div class="edit"><span class="text">Edit</span><span class="ico"></span></div>
                    <div class="remove"><span class="text">Remove</span><span class="ico"></span></div>
                    <input type="hidden" class="cartId" value="<?php echo $items['rowid']; ?>" />
                </div>
                <?php
            endforeach;
            if ($i < sizeof($this->session->userdata('cart_contents')) - 2) {
                ?>
                <div class="headlines">
                    <span class="shoe">Item</span>
                    <span class="shoe_style">From</span>
                    <span class="sizes">To</span>
                    <span class="quantity">Quantity</span>
                    <span class="price">Price</span>
                    <span class="remove"></span>
                </div>
                <?php
                foreach ($this->cart->contents() as $items):
                    if (strtolower($items['name']) != 'gift card') {
                        continue;
                    }
                    $options = $this->cart->product_options($items['rowid']);
                    ?>
                    <div class="cart-item">
                        <div class="card">
                            <img  src="<?php echo base_url() . 'assets/img/logo.png'; ?>" width="100" class="float-top" />
                        </div>
                        <div class="shoe_style"><span><?php echo $options['sender_email']; ?></span></div>
                        <div class="sizes"><span><?php echo $options['recipient_email']; ?></span></div>
                        <div class="quantity"><?php echo $items['qty']; ?></div>
                        <div class="price">$ <?php echo $items['price'] . '.00' ?></div>
                        <div class="edit"><span class="text">Edit</span><span class="ico"></span></div>
                        <div class="remove"><span class="text">Remove</span><span class="ico"></span></div>
                        <input type="hidden" class="cartId" value="<?php echo $items['rowid']; ?>" />
                    </div>
                    <?php
                endforeach;
            }
            ?>
            <div class="couponBox clearfix">
                <input type="text" name="promocode" id="promocode" value="" placeholder="Promocode" class="zipcode">
                <a href="javascript:;" onclick="ShoeCart.validatePromo();" id="applyCode">Apply Code</a>

                <!--a href="#" id="applyCode">Apply Code</a-->
                <div class="invalid_errors clearfix">
                    <div class="invalid invalid_required"></div>
                </div>
                <div id="divdiscount" style="float: right;width: 260px;">
                    <label id="lblDiscount"></label>
                </div>

                <?php if ($userdetails != FALSE) { ?>
                    <input type="text" name="giftcard" id="giftcard" value="" placeholder="Gift Card Number" class="zipcode">
                    <a href="javascript:;" onclick="ShoeCart.validateGiftCard();" id="applyGiftCard">Redeem Gift Card</a>
                <?php } else { ?>
                    <div class="redeem">Got a gift card? Login to redeem it.</div>
                <?php } ?>
            </div>

            <div class="total" >
                <p>Subtotal<span id="lblCartTotal">$<?php echo $this->cart->format_number($this->cart->total()); ?></span>
                </p>
                <input type="hidden" id="hid_net_total" value="<?php echo $this->cart->format_number($this->cart->total()); ?>" />
            </div>
        </section>
        <div class="bottom_links_wrap">
            <div class="links wrapper clearfix">
                <ul class="top">
                    <!--<li class="homepage"><a href="<?php //echo base_url();       ?>index.php/customshoe/index"><span>Create a new shoe</span></a></li>-->
                    <li class="checkout"><a href="<?php echo base_url(); ?>custom-shoe/checkout"><span>Checkout</span></a></li>
                </ul>
            </div>
            <div class="action">
                <div class="default"></div>
                <div class="changable homepage hidden"><p><strong>return</strong> to the design process to create yet another original shoe</p></div>
                <div class="changable checkout hidden"><p><strong>finalize</strong> your order by specifying your delivery address and purchase method.</p></div>
            </div>	  
        </div>   
    </div>		        
</section>
<div id="cartpopupOverlay" style="display: none;">

</div>
<div id="selection_popup" class="popup">
    <div id="selection_popup_content">
        <div class="top">
            <a href="#" class="close">Close window / return to cart</a>
            <div id="popup_socials">
                <a id = "fb" class="share_btns facebook" href="http://www.facebook.com/sharer.php?u=<?php echo ApplicationConfig::ROOT_BASE . basename(FCPATH); ?>get-inspired/##" target="_blank"  >Facebook</a>
                <a id = "twitter" class="share_btns twitter" href="http://www.twitter.com/share?url=<?php echo ApplicationConfig::ROOT_BASE . basename(FCPATH); ?>get-inspired/##&text=Checkout this custom pair of shoes I just designed at Awlandsundry.com" target="_blank" >Twitter</a>
                <a id = "pinterest" class="share_btns pinterest" href="http://www.pinterest.com/pin/create/button/?url=<?php echo ApplicationConfig::ROOT_BASE; ?>files/designs/##_A0.png" target="_blank" >Pinterest</a>
            </div>
        </div>
        <div id="visualization">
            <!--img src="img/visualizations/shoe1.png" /-->
            <!--div id="vis_buttons">
                <span>Rotate view</span>
                <a class="rotate left" href="#"></a>
                <a class="rotate right" href="#"></a>
                <a class="zoom" href="#"></a>
            </div-->
        </div>
        <div id="popup_main_links">
            <ul>
                <li><a class="rework edit" href="javascript:;"><span class="top">Rework this shoe</span><span class="bottom"><strong>revise</strong> this shoe before<br/>placing your order</span></a></li>
                <li><a class="add_to_cart close" href="#"><span class="top">Return to cart</span><span class="bottom"><strong>approve</strong> this shoe as-is</span></a></li>
                <input type="hidden" class="cartId" id="cart_item_detail_id" />
            </ul>
        </div>
    </div>
</div>
<?php $this->load->view('common/template/jscontainer'); ?>
<style>
    .shoe img {
        width: 125px;
    }
    .shoe .float-top {
        position: absolute;
        margin-left: -125px;
    }
</style>