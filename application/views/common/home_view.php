<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <?php $this->load->view('common/template/begin',@$data); ?>
    <?php $this->load->view('common/template/jscontainer'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/preloadjs-0.4.0.min.js"></script>
    <body class="home">
        <header id="header" class="headerTop">
            <section id="toolbar" class="toolbarScroll">
                <div class="toolbarInner clearfix">
                    <div class="wrapper clearfix">
                        <div class="left">
                            <a >
                                <span>Free shipping &amp; 30 Day Exchange</span>
                            </a>
                        </div>
                        <div class="right">
                            <ul id="headerNavLinks">
                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li>
                                <?php $this->load->view('common/login_status'); ?>
                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:void(0);">Cart
                                        <span class="number" id="cartcount">&nbsp;</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>

            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
            <div class="wrapper shopping_cart_pop clearfix">
                <section class="shopping_cart_mini">
                    <h4>ORDER CART</h4>
                    <h2 class="emptyCart">Cart is Empty.</h2>
                </section>
            </div>
        </header>


        <section id="middle_container">
            <section id="main" class="wrapper clearfix">
                <div id="main_slider" class="clearfix">

                    <div class="camera_wra camera_emboss pattern_1" id="large-image">
                        <div data-src="<?php echo ApplicationConfig::ROOT_BASE; ?>images/slider/slider3.jpg" data-fx="scrollHorz" rel="partial">
                            <div class="camera_caption ">
                                <div class="hs-image-container">
                                    <a class="hs-image-page-link" href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">&nbsp;</a>
                                    <div class="part">
                                        CREATE AN <br>ORIGINAL PAIR
                                    </div>
                                    <div class="part sub" style="margin-top:6px;">COLLABORATE WITH US AND<br/>
                                        LET&rsquo;S DESIGN A CUSTOM SHOE<br/>
                                        THAT&rsquo;S TAILOR-MADE FOR YOU
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-src="<?php echo ApplicationConfig::ROOT_BASE; ?>images/image6.jpg" data-fx="scrollHorz" rel="partial">
                            <div class="camera_caption ">
                                <div class="hs-image-container">
                                    <a class="hs-image-page-link" href="<?php echo base_url(); ?>our-artisanal-process">&nbsp;</a>
                                    <div class="part">
                                        HAND WELT<br/>
                                        CONSTRUCTION
                                    </div>
                                    <div class="part sub" style="margin-top:6px;">MACHINES? NONSENSE! <br/>
                                        THIS IS THE PROCESS<br/>
                                        BY WHICH A PROPER MEN&rsquo;S <br/>
                                        DRESS SHOE IS BUILT
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-src="<?php echo ApplicationConfig::ROOT_BASE; ?>images/slider/slider1.jpg" data-fx="scrollHorz" rel="partial">
                            <div class="camera_caption ">
                                <div class="hs-image-container">
                                    <a class="hs-image-page-link" href="<?php echo base_url(); ?>get-inspired">&nbsp;</a>
                                    <div class="part">
                                        GET INSPIRED<br/>
                                    </div>
                                    <div class="part sub" style="margin-top:6px;">UNORIGINAL? THAT&rsquo;S OK TOO.<br/>
                                        YOU&rsquo;LL BE ABLE TO PICK FROM <br/>
                                        THE MOST POPULAR<br/>
                                        COLLABORATIONS
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-src="<?php echo ApplicationConfig::ROOT_BASE; ?>assets/img/gallery/slider3.jpg" data-fx="scrollHorz" rel="partial">
                            <div class="camera_caption ">
                                <div class="hs-image-container">
                                    <a class="hs-image-page-link" href="<?php echo base_url(); ?>faq#whatisincluded/price">&nbsp;</a>
                                    <div class="part">
                                        ALL RELIGIONS, ARTS AND SCIENCES <br/>
                                        ARE BRANCHES OF THE SAME TREE.<br/>

                                    </div>
                                    <div class="part sub" style="margin-top:6px;">
                                        THAT&rsquo;S WHY EVERY PAIR COMES <br>
                                        WITH TREES... AND HORNS. <br>
                                        WE GIVE YOU THE TOTAL PACKAGE
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="links" class="wrapper">
                    <ul class="top clearfix hBoxLinks">
                        <li class="inspire">
                            <a href="<?php echo base_url(); ?>get-inspired">
                                <div id="button_wrapper">
                                    <div id="button_img">
                                    </div>
                                    <div id="button_text">
                                        <h1>Get inspired</h1>
                                    </div>
                                </div>
                                <span id="arrow"></span>
                            </a>
                        </li>
                        <li class="create">
                            <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">
                                <div id="button_wrapper">
                                    <div id="button_img">
                                    </div>
                                    <div id="button_text">
                                        <h1>Create a custom shoe</h1>
                                    </div>
                                </div>
                                <span id="arrow"></span>
                            </a>
                        </li>



                    </ul>
                </div>
                <div class="action">
                    <p class="default">Design a truly
                        <a href="<?php echo base_url(); ?>create-a-custom-shoe/select-style">one-of-a-kind</a> pair of handcrafted shoes online.</p>
                    <p class="changable hidden inspire">view the creation’s of others; buy them, use them for inspiration or rework them to your liking</p>
                    <p class="changable hidden create">choose from one of our classic styles, then specify design details, leathers, colors, stitching and lacing</p>
                </div>
            </section>
        </section>
        <?php $this->load->view('common/template/footer'); ?>

        <div id="loader_bg">
            <span id="progress">
                <img src="<?php echo base_url(); ?>assets/img/logo.png"/><br>
                <span></span>
            </span>
        </div>
        <div id="preload">
            <div id="preload_inner">
                <span><img src="<?php echo base_url(); ?>assets/img/logo.png"/></span>
                <p>We are working our tails off to create a mobile optimized experience. In the meantime, please register and get 10% off your first purchase via Desktop/ Laptop/ Ipad. That is just one of the ways to thank you for visiting us from your mobile device!</p>  
                <div>
                    <input id="promo_email" type="text"   >
                    <button class="" onclick="saveEmail();">Submit</button>
                </div>
            </div>
        </div>
        <div id="promo_message">
            <div id="promo_inner">
                <span><img src="<?php echo base_url(); ?>assets/img/logo.png"/></span>
                <hr/>
                <p>Use the following discount code at Awl &amp; Sundry to get 10% off all products! 
                    Make sure that you are using this email for signing up at Awl &amp; Sundry, otherwise it will not work.
                    <br/>
                    <br/>
                    <b id="promo_code"></b>
                </p>  

            </div>
        </div>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/camera-mymini.js"></script>
        <script type="text/javascript">
        $(document).ready(function() {
            $('#logo a').removeAttr('href');
            jQuery('#large-image').camera({
                height: 'auto',
                loader: 'none',
                pagination: true,
                thumbnails: false,
                hover: false,
                navigation: false,
                playPause: false,
                pauseOnClick: false,
                overlayer: false,
                opacityOnGrid: false,
                imagePath: '<?php echo base_url(); ?>images',
                partialDelay: 1500,
                time: 4000
            });
        });
        </script>
    </body>

</html>
