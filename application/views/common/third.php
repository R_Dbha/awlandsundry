<?php $this->load->view('common/template/begin'); ?>

<body class="page about">
    <header id="header">
        <section id="toolbar">
            <div class="toolbarInner clearfix">	
                <div class="wrapper content clearfix">
                    <div id="breadcrumb" class="left">
                        <a href="<?php echo base_url(); ?>">Home</a>
                        <span>Press</span>
                    </div>

                    <div class="right"> 
                        <ul id="headerNavLinks">

                            <li class="top_our_story">
                                <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                            </li> 
                            <?php $this->load->view('common/login_status'); ?>
                            <li class="cart">
                                <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
        <h1 id="logo">
            <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
        </h1>
        <div class="wrapper shopping_cart_pop clearfix">
            <section class="shopping_cart_mini">
                <h4>ORDER CART</h4>
                <h2 class="emptyCart">Cart is Empty.</h2>
            </section>
        </div>
        <div class="patterned-rule-blue"  ></div>
    </header>
    <section id="middle_container"  >
        <div class="wrapper clearfix quote">
            <h3 style="margin: 51px 0 56px 0;" class="align-left">Awl & Sundry in the media</h3>
            <!--<h4>we founded awl & sundry to bring attentively handcrafted men’s shoes within reach.</h4>-->
        </div>
        <section class="wrapper clearfix main">
            <section class="press" >
                
                <div class="paging_press">
                    <div class="items">
                    <div class="item-inner" >
                        <div class="logo UD" ></div>
                        <div class="thumb UD" ></div>
                    </div>
                </div>
                <div class="full" >
                    <div class='UD'></div>
                    <div class='collapse'><div>Collapse</div></div>
                </div>
                 <div class="items">
                    <div class="item-inner" >
                        <div class="logo snob" ></div>
                        <div class="thumb snob" ></div>
                    </div>
                </div>
                <div class="full" >
                    <div class='snob'></div>
                    <div class='collapse'><div>Collapse</div></div>
                </div>
                 <div class="items">
                    <div class="item-inner" >
                        <div class="logo ND" ></div>
                        <div class="thumb ND" ></div>
                    </div>
                </div>
                <div class="full" >
                    <div class='ND'></div>
                    <div class='collapse'><div>Collapse</div></div>
                </div>
                 <div class="items">
                    <div class="item-inner" >
                        <div class="logo BI" ></div>
                        <div class="thumb BI" ></div>
                    </div>
                </div>
                <div class="full" >
                    <div class='BI'></div>
                    <div class='collapse'><div>Collapse</div></div>
                </div>
                 <div class="items">
                    <div class="item-inner" >
                        <div class="logo WD" ></div>
                        <div class="thumb WD" ></div>
                    </div>
                </div>
                <div class="full" >
                    <div class='WD'></div>
                    <div class='collapse'><div>Collapse</div></div>
                </div>
                </div>
                
                <div class="show_more">
                    <a <?php if($page_position != 'press'){ ?> href="<?php echo base_url(); ?>common/press" <?php } ?>>1</a>
                    <a <?php if($page_position != 'second'){ ?> href="<?php echo base_url(); ?>common/nextpress/second" <?php } ?>>2</a>
                    <a <?php if($page_position != 'third'){ ?> href="<?php echo base_url(); ?>common/nextpress/third" <?php } ?>>3</a>
                </div>
                </div>
            </section>

        </section>   
    </section>
    <?php $this->load->view('common/template/footer'); ?>
    <?php $this->load->view('common/template/jscontainer'); ?>
    <script>
        $(document).ready(function() {
            $('.logo , .thumb').click(function() {
                $(this).parents('.items').next('.full').slideToggle();
            });
            $('.collapse').click(function() {
                $(this).parents('.full').slideUp();
            });
        });
    </script>

</body>
</html>        




