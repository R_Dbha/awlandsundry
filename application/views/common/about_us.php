<?php
$this->load->view('includes/header');
?>
<div class="aboutUs">
        <div id="aboutMenu-wrapper" class="aboutMenu-wrapper hide-appointment">
            <div id="aboutMenu" class="aboutMenu">
                <div class="container">
                    <ul>
                        <li><a  href="#ourstory">OUR STORY</a></li>
                        <li><a  href="#thetruth">THE TRUTH</a></li>
                        <li><a  href="#theteam">THE TEAM</a></li>
                        <li><a  href="#theworkshop">THE WORKSHOP</a></li>
                    </ul>
                </div>
            </div>
            <div id="aboutMenu" class="aboutMenu aboutMenu-xs">
                <div class="container">
                    <ul>
                        <li><a  href="#ourstory">ABOUT US</a></li>
                    </ul>
                </div>
            </div>
        </div>
	<div class="ourStory" id="ourstory">
        <div class="ourStoryBg "><h2>OUR STORY</h2></div>
        <div class="container">
            <div class="ourStoryCont">
            	<p>Awl & Sundry was founded with a simple mission: to democratize the luxury of bespoke footwear.</p>
            	<p>Back in 2012, Nikunj, our founder, was faced with a dilemma many men share: he couldn't find a sanely priced pair of dress shoes that really fit! After a month and a half of searching, he found he had to choose between comfort, style and affordability. Finding two of the three quality criteria was easy enough but all three... almost impossible! Awl & Sundry was born of the frustration of not being able to find the perfect pair of shoes at a fair price.</p>
                <p>Nikunj decided to take matters into his own hands and began canvassing the planet with one goal in mind: to find an artisanal cobbler who could produce a well-made, custom pair of shoes at a reasonable price. </p>
                <p>During his search, he visited some of the factories that produce shoes for the big global brands and was shocked to discover a well-kept secret. Many of these brands mass-manufacture shoes for $15-$20 per pair and then mark them up to $150-$200. That's right, a whooping 1000% markup!!! </p>
                <p>The worst part: often the quality of the materials and the shoe construction process was not only harmful to our health but also to our environment.</p>
                <p>But the search proved worthy of the effort when he came upon a group of distinguished craftsmen who are still employing the age-old process of handcrafting shoes. These shoes were beautiful, stylish, comfortable and durable - all at a production cost significantly below what a custom shoemaker in the U.S. would charge. In 2013, Nikunj decided to share this find with the legions of men forced to walk through life in uncomfortable, mass-produced  shoes and soon thereafter, Awl & Sundry was born!</p>
        	</div>
            <div class="ourStoryRight">Nikunj, our <br>founder, was <br>faced with a <br>dilemma many <br>men share: <br>he couldn't <br>find a sanely <br>priced pair of <br>dress shoes <br>that really fit!</div>
        </div>
    </div>
    
    <div class="theTruth" id="thetruth">
        <h2>THE TRUTH</h2>
        <div class="container">
        	<img src="<?php echo base_url(); ?>assets/css/images/truth-model.jpg" data-original="<?php echo base_url(); ?>assets/css/images/pyramid_animation.gif" data-as="true" data-as-animation="anime-end-1" class="img-responsive anime-start-1" />
        </div>
    </div>
    
    <div class="theTeam" id="theteam">
        <h2>THE TEAM</h2>
        <div class="theTeamBg">
            <div class="container">
                <div class="teamList">
                	<article>
                        <div class="team-img-1">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/css/images/nikunj-img.png" width="251" height="305" />
                        </div>
                        <p class="authorName  author-xs"><strong>Nikunj Marvania</strong> / Founder/CEO </p>
                        <p>	Born in Mumbai, India, Nikunj moved to the United States at the age of seventeen to pursue college. After studying finance, he worked on Wall Street in sales and trading. It was then that the idea for Awl & Sundry came to him. He launched Awl & Sundry in March 2014 in pursuit of his passion to offer high quality comfortable and stylish shoes at a revolutionary price. In addition to shoes, Nikunj is equally passionate about spirituality and meditation. He loves reading in his free time and enjoys spending time in nature.</p>
                    </article>
                    <p class="authorName"><strong>Nikunj Marvania</strong> / Founder/CEO </p>
                </div>
                <div class="teamList">
                	<article>
                        <div class="team-img-2">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/css/images/mario-img.png" width="251" height="305" />
                        </div>
                        <p class="authorName author-xs"><strong>Mario Lanzarotti</strong> / Chief Operating Officer </p>
                	    <p>Mario Lanzarotti is a German/Italian entrepreneur and speaker with a background in fashion and marketing. A true Italian at heart, he has a deep sense of fashion and an impeccable eye for well-crafted shoes. With a degree in Luxury Fashion and a multi-cultural experience that spans from Berlin to Cape Town to New York, he has organized events in collaboration with Fashion Week Berlin & The Russian Embassy.</p>
                    </article>
                    <p class="authorName"><strong>Mario Lanzarotti</strong> / Chief Operating Officer </p>
                </div>
            </div>
        </div>
    </div>
    <div class="theWorkshop" id="theworkshop">
        <h2>THE WORKSHOP</h2>
        <div id="thevideo">
            <video onclick="this.play();" playsinline autoplay muted loop preload="auto" >
                <source src="<?php echo base_url(); ?>assets/video/Aamp_S_for_vimeo.mp4" type="video/mp4"/>
            </video>
        </div>        
        <?php /*<div class="container">
        	<div class="theWorkshopCont">
            	<p>We've been making shoes in Bergamo town of Italy. Initially, we explored outsourcing our manufacturing to another workshop but we soon realized that, for Awl & Sundry to offer the best quality shoes and customer satisfaction we had to achieve complete control over the manufacturing process. Therefore, in September of 2014, we acquired some old-world machines, leased a workshop, hired third-generation shoemakers and began a journey to produce the best quality, traditionally-made custom footwear at the best possible price. </p>
                <p><strong>Let's break shoemaking down into various components that drive value and cost:</strong></p>
                <p><strong>Leather:</strong> The material used in the construction of the shoe plays a fundamental role in determining the cost of production. There are various types and grades of leather that determine the quality of a shoe. Awl & Sundry custom shoes are handcrafted using the finest full grain leather sourced from European tanneries.</p>
                <p><strong>Craftsmanship:</strong> There are a number of ways to construct a shoe and the cost associated with each process varies significantly. Awl & Sundry employs two of most renowned construction processes: hand welt and blake construction. These are the methods used by the most revered luxury footwear brands in the world.</p>
                <p><strong>Labor and Geography:</strong> The last and the most overlooked variable is cost-of-labor: where the shoes are made. Since most high quality shoes are handcrafted by seasoned artisans, location plays an important role in determining the cost of the shoes. For example, the labor cost for craftsmen in Northampton, England is relatively higher than craftsmen in Guangzhou, China despite similar methods and a similar old-world dedication to craftsmanship. The location of our workshop allows us to offer very high quality products at a remarkable price. </p>
                <p><strong>Flexibility:</strong> Our founder, Nikunj, visited with factories in England, India, Italy, Mexico, Spain and many other countries and came to realize that, although some of these factories provided the flexibility to make a one-of-a-kind custom shoe, none could do it at a price that was comparable to good quality ready-to-wear brands. Guangzhou provided us the flexibility in terms of a solid supply chain and relatively lower labor costs that allow us to provide a one-of-a-kind shoe at a truly revolutionary price.</p>
        	</div>
            <div class="theWorkshopRight">Awl & Sundry employs two of most renowned construction processes: hand welt and blake construction. These are the methods used by the most revered luxury footwear brands in the world.</div>
        </div>*/ ?>
    </div> 

</div>

<?php
$this->load->view('includes/footer.php');
?>
<script>

$(document).ready(function() {

$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 50
        }, 1000);
        return false;
      }
    }
  });
});
});
//$(window).scroll(function() {
//    if($(".aboutMenu-wrapper").hasClass("is-sticky")){
//        var headerHeight = $('#header-wrapper-sticky-wrapper').outerHeight();
//        $("#aboutMenu-wrapper").css('top', headerHeight);
//    };
//}
$(document).ready(function() {
        $('.videoImg').click(function() {
            $('.videoImg').fadeOut('fast', function() {
                $("#thevideo").css("display","block");
                $("video").click();
             });
        });
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/7.2.0/lazyload.transpiled.min.js"></script>
<script>
		new LazyLoad();
	</script>
