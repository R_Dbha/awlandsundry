<?php
if ($shoedesigns != NULL) {
    foreach ($shoedesigns as $shoedesign) {
        ?>
        <div class="item purchased" onclick="popup_design('<?php echo $shoedesign['shoe_design_id']; ?>');" style="float:left">
            <div class="item_row_top">
                <!--<span class="filter-desc">your purchased item</span>--><!---->
                <span class="author"><img src="<?php echo base_url(); ?>assets/img/author_avatar_bob.png">by <span class="name"><?php echo $shoedesign['first_name']; ?></span></span>
            </div>
            <img class="item-img" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoedesign['image_file'] . "_A0.png"; ?>" width="249">
            <img class="item-img selected" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoedesign['image_file'] . "_A7.png"; ?>" width="249">
            <div class="item_row_bottom">
                <p class="item_row_title">
                    <span class="item-title"><?php echo $shoedesign['public_name']; ?>&nbsp;</span>
                    <span class="item-subtitle"><?php echo $shoedesign['last_name']; ?> /<?php echo $shoedesign['style_name']; ?></span>
                </p>
                <p class="meta">
                    <span>0 Likes</span>
                    <span>1 Redesigns</span>
                </p>
            </div>
        </div>
    <?php
    }
}
?>
