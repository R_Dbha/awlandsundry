<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <?php $this->load->view('common/template/begin'); ?>   
    <body class="page cart checkout">
        <style>
            .invalid_errors .error-check {
                color: #ED5224;
                font-size: 10px;
            } 
        </style>
        <header id="header">
            <section id="toolbar" class="clearfix"><div class="toolbarInner clearfix"></div></section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
        </header>
        <section id="middle_container">
            <section class="main">
                <div class="wrapper clearfix">

                    <?php
                    $stl = "";
                    $shipping = 0;
                    $tax = 0;
                    $billingdisabled = "hiddenColumnBox";
                    $billinghidden = "block";
                    if (isset($registersubmit)) {
                        $stl = 'style="height: 386px;"';
                    }
                    ?>
                    <div class="order billing  clearfix"  >
                        <!--<div class="column">-->
                        <?php //if (!isset($userdetails['username'])) {  ?>

                        <?php
                        // } else {
                        //   $billingdisabled = "";
                        //    $billinghidden = "none";
                        ?>
                        <!--<h4>You are signed in.</h4> 
                        <h1>Welcome <?php // echo $userdetails['first_name'];                                                                                                        ?></h1>
                        <a class="diffUserIn" href="javascript:;">SIGN IN AS ANOTHER USER</a>-->
                        <?php // }  ?>
                        <!--</div>-->
                        <div class="billingShippingColumn <?php //echo $billingdisabled;                                                                                 ?>" >

                            <form action="<?php echo base_url() ?>custom-shoe/checkout" method="post" name="awlsundry" class="create_new_acc" id="checkoutform">


                                <div class="column shippingColumn" id="shippingDetails">
                                    <?php
                                    $this->load->view('common/checkout_shipping_details');
                                    ?>
                                </div>
                                <div class="column billingColumn" id="billingDetails">
                                    <?php
                                    if ($total != 0) {
                                        $this->load->view('common/checkout_billing_details');
                                    } else {
                                        echo '<div>&nbsp;</div>';
                                    }
                                    ?>
                                </div>
                                <div class="column order_summary" id="orderDetails">
                                    <div class="form-overlay" style="height: 100%; top:0px;" ></div>
                                    <div class="form-overlay" style="height: 55px; top:370px;"></div>
                                    <h4>Review Your Order</h4>
                                    <ul>
                                        <?php
                                        foreach ($items as $key => $value) {
                                            ?>
                                            <li>
                                                <?php echo $value['name']; ?>
                                                <span class="amount"  id="gross_amount">
                                                    <?php echo $value['price']; ?>
                                                </span>
                                            </li>
                                            <?php
                                        }
                                        if ($disc > 0) {
                                            ?>
                                            <li >
                                                Discount:
                                                <span class="amount">
                                                    <?php echo round($disc, 2); ?>
                                                </span>
                                            </li>  
                                        <?php } ?>
                                        <li >
                                            Shipping:
                                            <span class="amount">
                                                <?php echo $shipping; ?>
                                            </span>
                                        </li>
                                        <li >
                                            Tax:
                                            <span class="amount" id="tax_amount">
                                                <?php echo round($tax, 2); ?>
                                            </span>
                                        </li>
                                        <li class="total" >
                                            Order total
                                            <span class="amount" id="net_amount">
                                                <?php echo round($total, 2); ?>
                                            </span>
                                        </li>
                                    </ul>
                                    <br/>
                                    <div id='comments'>
                                        <span>Add your comments here:</span>
                                        <textarea name='comment' tabindex="25" ></textarea>

                                    </div>

                                </div>
                                <button type="submit" name="submit-button" class="button continue" >Place Order</button>
                                <input type="hidden" id="userfirstname" name="userfirstname" value="" />    
                                <input type="hidden" id="userlastname" name="userlastname" value="" />    
                                <input type="hidden" id="useremail" name="useremail" value="" />    
                                <input type="hidden" id="sameasbilling" name="sameasbilling" value="" />
                                <input type="hidden" value="submit" name="checksubmit" />                                
                            </form>  

                            <!--a href="javascript:;" id="checkoutsubmit" tabindex="26" class="button continue" >Place Order</a-->
                        </div>      
                    </div>

                </div>              
            </section>
        </section>
        <div id="popupOverlay" style="display: none;">

        </div>
        <div id="signin_popup" class="messageBox messageBoxBlue loginDetails"></div>
        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.autosize.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.formance.min.js"></script>
        <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v1/"></script>

        <script type="text/javascript">
            var billingsubmit = '<?php echo $checksubmit != NULL ? $checksubmit : ''; ?>'
            jQuery.expr[":"].contains = jQuery.expr.createPseudo(function(arg) {
                return function(elem) {
                    return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
                };
            });
            Stripe.setPublishableKey('<?php echo $this->config->item('stripe_public_key'); ?>');
            $(document).ready(function() {
<?php if ($total != 0) { ?>
                    $("#checkoutform").validate({
                        submitHandler: submit,
                        rules: {
                            "card-cvc": {
                                cardCVC: true,
                                required: true
                            },
                            "card-number": {
                                cardNumber: true,
                                required: true
                            },
                            // "card-expiry-year": "cardExpiry" // we don't validate month separately
                        },
                        invalidHandler: function(event, validator) {
                            if ($('#payment_method').val() === "Paypal") {
                                removeInputNames();
                                form.submit();
                                return false;
                            }
                        },
                    });
<?php } ?>


                // add custom rules for credit card validating
                jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
                jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
                jQuery.validator.addMethod("cardExpiry", function() {
                    return Stripe.validateExpiry($("select.card-expiry-month").val(),
                            $("select.card-expiry-year").val())
                }, "Please enter a valid expiration");
                addInputNames();



                $('.dk_container').off('keyup').on('keyup', dk_onkeyup);
                $('#comments').autosize();

<?php if (!isset($userdetails['username'])) { ?>
                    login();
<?php } ?>
                if (!$('#billingDetailsNonEditable').length || !$('#billingDetailsEditable').length) {
                    $('#shippingDetails .form-overlay').hide();
                    $('.sameBillingAddr').hide();
                }

                $(".fb_loginBtn").off('click touchend').on('click touchend', function() {
                    window.open(baseUrl + 'facebooklogin', 'FB', 'width=400,height=300');

                });
                if ($('.card_state option[value="' + $('#selectedBillingStateId').val() + '"]').length > 0) {
                    $('.card_state option[value="' + $('#selectedBillingStateId').val() + '"]').attr('selected', true);
                    if ($("#dk_container_card_state").length > 0 && $('#selectedBillingStateId').val() !== "") {
                        $("#dk_container_card_state .dk_options_inner li.dk_option_current").removeClass('dk_option_current');
                        $("#dk_container_card_state .dk_options_inner li a[data-dk-dropdown-value='" + $('#selectedBillingStateId').val() + "']").parent().addClass('dk_option_current');
                        $("#dk_container_card_state .dk_label").text($('#selectedBillingStateId').val());
                        $('#dk_container_card_state').removeClass('invalid_box');
                        $('#dk_container_card_state').removeClass('dk_theme_black');
                    }
                }
                if ($('#shipping_state option[value="' + $('#selectedShippingStateId').val() + '"]').length > 0) {
                    $('#shipping_state option[value="' + $('#selectedShippingStateId').val() + '"]').attr('selected', true);
                    if ($("#dk_container_shipping_state").length > 0 && $('#selectedShippingStateId').val() !== "") {
                        $("#dk_container_shipping_state .dk_options_inner li.dk_option_current").removeClass('dk_option_current');
                        $("#dk_container_shipping_state .dk_options_inner li a[data-dk-dropdown-value='" + $('#selectedShippingStateId').val() + "']").parent().addClass('dk_option_current');
                        $("#dk_container_shipping_state .dk_label").text($('#selectedShippingStateId').val());
                        $('#dk_container_shipping_state').removeClass('invalid_box');
                        $('#dk_container_shipping_state').removeClass('dk_theme_black');
                    }
                }
                $('#payment_method').off('change', payment_method).on('change', payment_method);
                change_payment_method();
                shipping_country_change();
                billing_country_change();
                $('#billing_country').change(billing_country_change);
                $('#shipping_country').change(shipping_country_change);
                $('.shipping_state').change(state_change);
                $('.card_state').change(card_state_change);


                $("select:invalid").each(function() {
                    $("#dk_container_" + $(this).attr("name")).addClass("invalid_box");
                });

                $("#checkoutsubmit").on('click touchend', function() {

                    // submit($("#checkoutform"));
                });

                $('.sameBillingAddr').off('click touchend').on('click touchend', function() {
                    //console.log($('#selectedBillingStateId').val());
                    $(this).toggleClass('checked');
                    $check = $(".sameBillingAddr.checked").length;
                    if ($check == 1) {
                        $('#sameasbilling').val('1');
                        $('#billing_shipping').val('Y');
                        $('#shipping_street_1').val($('#billing_street_1').val());
                        $('#shipping_street_2').val($('#billing_street_2').val());
                        $('#shipping_city').val($('#billing_city').val());
                        $('#shipping_state').val($('#billing_state').val());
                        $('#shipping_zipcode').val($('#billing_zipcode').val());
                        if ($('#selectedBillingStateId').val() !== '') {
                            $('#dk_container_shipping_state .dk_label').text($('#selectedBillingStateId').val());
                            $('#dk_container_shipping_state').removeClass('invalid_box');
                            $('#dk_container_shipping_state').removeClass('dk_theme_black');
                            $('#dk_container_shipping_state').removeClass('dk_theme_');
                            $('#selectedShippingStateId').val($('#selectedBillingStateId').val());
                            $('#shipping_state option[value="' + $('#selectedShippingStateId').val() + '"]').attr('selected', true);
                            //$('#shipping_state').text($('#selectedShippingStateId').val());
                            //console.log($('#dk_container_shipping_state').attr('class'));
                        }
                    }
                    else {
                        $('#sameasbilling').val('0');
                        $('#billing_shipping').val('N');
                        $('#shipping_street_1').val('');
                        $('#shipping_street_2').val('');
                        $('#shipping_city').val('');
                        $('#shipping_state').val('');
                        $('#shipping_zipcode').val('');
                        $('#shipping_first_name').val('');
                        $('#shipping_last_name').val('');
                        $('#shipping_email').val('');

                        if ($('#selectedShippingStateId').val() != '') {
                            $('#dk_container_shipping_state .dk_label').text($('#selectedShippingStateId').val());
                            $('#shipping_state option[value="' + $('#selectedShippingStateId').val() + '"]').attr('selected', true);
                            $('#dk_container_shipping_state').removeClass('invalid_box');

                        } else {
                            $('#dk_container_shipping_state .dk_label').text("State");
                            $('#dk_container_shipping_state').addClass('invalid_box');
                        }
                    }

                });
                if (billingsubmit !== "") {
                    $("input.req").each(function() {
                        if ($(this).val().trim() === "") {
                            $(this).attr("required", "required");
                        }
                    });
                    $("select.req").each(function() {
                        if ($(this).val() === "") {
                            $("#dk_container_" + $(this).attr("name")).addClass("invalid_box");
                        }
                    });
                }
                $('.checkout').on('blur', '#txtCardNumber', GetCardType);
                // $('.checkout').on('blur', '.zipcode', zipcode_search);
                $('#txtCardNumber').formance('format_credit_card_number');

            });
            function addInputNames() {
                // Not ideal, but jQuery's validate plugin requires fields to have names
                // so we add them at the last possible minute, in case any javascript 
                // exceptions have caused other parts of the script to fail.
                $(".card-number").attr("name", "card-number")
                $(".card-cvc").attr("name", "card-cvc")
                $("select.card-expiry-year").attr("name", "card-expiry-year")
            }

            function removeInputNames() {
                $(".card-number").removeAttr("name")
                $(".card-cvc").removeAttr("name")
                $("select.card-expiry-year").removeAttr("name")
            }

            function submit(form) {
                removeInputNames(); // THIS IS IMPORTANT!
                $(form['submit-button']).attr("disabled", "disabled")
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month')[1].value,
                    exp_year: $('.card-expiry-year')[1].value,
                }, function(status, response) {
                    if (response.error) {
                        $(form['submit-button']).removeAttr("disabled");
                        $(".payment-errors").html(response.error.message);
                        addInputNames();
                    } else {
                        var token = response['id'];
                        var input = $("<input name='stripeToken' value='" + token + "' style='display:none;' >");
                        $(form['submit-button']).removeAttr("disabled");
                        form.appendChild(input[0]);
                        form.submit();
                    }
                });
                return false;
            }

            function payment_method(e) {
                if ($(e.currentTarget).val() === "Paypal") {
                    $("#redirecttopaypal").prevAll().not("#dk_container_payment_method, h4").hide();
                    $('#redirecttopaypal').show();
                    $('.sameBillingAddr').hide();
                    $('.invalid_required').hide();
                }
                else
                {
                    $('#txtCardNumber').addClass($(e.currentTarget).val());
                    //  formatCardNumber();

                    $("#redirecttopaypal").prevAll().not("select").show();
                    $('#redirecttopaypal').hide();
                    if ($('#billingDetailsNonEditable').length || $('#billingDetailsEditable').length) {
                        $('.sameBillingAddr').show();
                    }
                }
            }

            function formatCardNumber() {
                var element = $('#txtCardNumber');
                var split = [4, 4, 4, 4];
                if ($(element).hasClass('AMEX')) {
                    split = [4, 6, 5];
                }
                $(element).val(function(i, v) {
                    var re, p = 0, temp = 0;
                    $.each(split, function(j) {
                        p += split[j] + temp;
                        temp = 1;
                        if (v.toString().length === p + 1) {
                            re = new RegExp(".{1," + p + "}", "g");
                        }
                    });
                    if (typeof re !== 'undefined') {
                        v = v.replace(/[^\d^\s]/g, '').match(re);
                        return v.join(' ');
                    }
                    return v;
                });
            }

            function card_state_change(e) {
                $('#selectedBillingStateId').val($(e.currentTarget).val());
            }

            function state_change(e) {
                if ($(e.currentTarget).val() === "New Jersey") {
                    $('#selectedShippingStateId').val('New Jersey');
                }
                else {
                    $('#selectedShippingStateId').val($(e.currentTarget).val());
                    //alert($('#selectedShippingStateId').val());
                }
            }

            function change_payment_method() {
                if ($('#payment_method').val() === "Paypal") {
                    $("#redirecttopaypal").prevAll().not("#dk_container_payment_method, h4").hide();
                    $('#redirecttopaypal').show();
                    $('.sameBillingAddr').hide();
                }
                else
                {
                    $("#redirecttopaypal").prevAll().not("select").show();
                    $('#redirecttopaypal').hide();
                    if ($('#billingDetailsNonEditable').length || $('#billingDetailsEditable').length) {
                        $('.sameBillingAddr').show();
                    }
                }
            }
            function validateBillingDetails() {
                var card_number = $('.card-number').val();
                var card_cvc = $('.card-cvc').val();
                var card_exp_month = $('.card-expiry-month')[1].value;
                var card_exp_year = $('.card-expiry-year')[1].value;
                var error = false;
                if (card_number === '') {
                    $('.card-number').addClass('invalid_box');
                    error = true;
                }
                if (card_cvc === '') {
                    $('.card-cvc').addClass('invalid_box');
                    error = true;
                }
                if (card_exp_month === '') {
                    $('#dk_container_exp_date_month').addClass('invalid_box');
                    error = true;
                }
                if (card_exp_year === '') {
                    $('#dk_container_exp_date_year').addClass('invalid_box');
                    error = true;
                }
                if ($('#payment_method').val() !== "Paypal") {
                    if (error) {
                        $('#billingDetailsEditable .invalid_required').html(' * Required fields can not be empty<br>');
                        return false;
                    }
                } else {
                    removeInputNames();
                }
                $.ajax({
                    type: 'POST',
                    url: baseUrl + "customshoe/validate_billing_details",
                    data: $("#checkoutform").serialize(),
                    success: function(response) {
                        $('#billingDetails').html(response);
                        $('.card-expiry-month').val(card_exp_month);
                        $('.card-expiry-year').val(card_exp_year);
                        $('#billingDetails select').dropkick({
                            theme: 'black',
                            change: function(value, label) {
                                $(this).dropkick('theme', value);
                            }
                        });
                        addInputNames();
                        $('.card-number').val(card_number);
                        $('.card-cvc').val(card_cvc);
                        $('.card-expiry-month')[1].value = card_exp_month;
                        $('.card-expiry-year')[1].value = card_exp_year;
                        $('#payment_method').off('change', payment_method).on('change', payment_method);
                        change_payment_method();
                        validate_checkout_page();
                        $('.card_state').change(card_state_change);
                        $('#billing_country').change(billing_country_change);
                        billing_country_change();
                        $('#txtCardNumber').formance('format_credit_card_number');
                        // $('#selectedBillingStateId').val($('.card_state').val());
                        jQuery('.cart form input').change(function() {
                            $(this).removeClass('invalid_box');
                        });

                        if ($('#billingDetails #billingDetailsNonEditable').hasClass('hidden') || $('#billingDetails #billingDetailsEditable').hasClass('hidden') || $('#billingDetailsEditable').length === 0) {
                            $('#billingDetails .form-overlay').hide();
                            if ($('#shippingDetails #shippingDetailsEditable').hasClass('hidden') && $('#billingDetails #billingDetailsEditable').hasClass('hidden')) {
                                $('#orderDetails .form-overlay').hide();
                            }
                        }

                        $('.dk_container').off('keyup').on('keyup', dk_onkeyup);
                        $('.dk_container').each(function() {
                            if ($(this).find('.dk_label').html() !== $(this).find('.dk_options_inner').find('a').eq(0).html())
                                $(this).removeClass('dk_theme_black dk_theme_').addClass('dk_theme_' + $(this).find('.dk_label').html());
                        });

                    },
                });
            }

            function validateShippingDetails() {
                removeInputNames();
                $.ajax({
                    type: 'POST',
                    url: baseUrl + "customshoe/validate_shipping_details",
                    data: $("#checkoutform").serialize(),
                    dataType: 'json',
                    success: function(response) {
                        $('#shippingDetails').html(response.view);
                        $('#tax_amount').text(response.tax);
                        $('#net_amount').text(response.netAmount);
                        $('#payment_method').off('change', payment_method).on('change', payment_method);
                        jQuery('.cart form select').dropkick({
                            theme: 'black',
                            change: function(value, label) {
                                $(this).dropkick('theme', value);
                            }
                        });
                        addInputNames();
                        validate_checkout_page_shipping();
                        change_payment_method();
                        $('#shipping_country').change(shipping_country_change);
                        shipping_country_change();
                        $('.shipping_state').change(state_change);
                        jQuery('.cart form input').change(function() {
                            $(this).removeClass('invalid_box');
                        });
                        if ($('#shipping_state').val() === '') {
                            $('#dk_container_shipping_state').addClass('invalid_box');
                        }
                        $('.dk_container').off('keyup').on('keyup', dk_onkeyup);
                        $('.dk_container').each(function() {
                            if ($(this).find('.dk_label').html() !== $(this).find('.dk_options_inner').find('a').eq(0).html())
                                $(this).removeClass('dk_theme_black dk_theme_').addClass('dk_theme_' + $(this).find('.dk_label').html());
                        });
                        if ($('#shippingDetails #shippingDetailsEditable').hasClass('hidden')) {
                            if ($('#billing_country').length && $('#billing_country').is(':visible')) {
                            $('#billingDetails .form-overlay').hide();

                            $('#txtCardOwner').val($('#shipping_first_name').val() + ' ' + $('#shipping_last_name').val());
                            $('#billing_street_1').val($('#shipping_street_1').val());
                            $('#billing_street_2').val($('#shipping_street_2').val());
                            $('#billing_city').val($('#shipping_city').val());
                            $('#billing_zipcode').val($('#shipping_zipcode').val());

                            $('#dk_container_billing_country .dk_label').text($('#shipping_country').val());
                            $('#dk_container_billing_country').removeClass('invalid_box');
                            $('#dk_container_billing_country').removeClass('dk_theme_black');
                            $('#dk_container_billing_country').removeClass('dk_theme_');
                            $('#selectedBillingStateId').val($('#selectedShippingStateId').val());
                            $('#billing_country option[value="' + $('#shipping_country').val() + '"]').attr('selected', true);
                            
                                billing_country_change('', $('#shipping_state').val());
                            }

                            if (!$('#billingDetailsEditable').length || $('#billingDetails #billingDetailsEditable').hasClass('hidden')) {
                                $('#orderDetails .form-overlay').hide();
                            }
                        }
                    },
                });
            }
            function shipping_country_change(e, state) {
                var country = $('#shipping_country').val();
                if (country === '') {
                    if ($('#shipping_country').hasClass('invalid_box')) {
                        $('#dk_container_shipping_country').addClass('invalid_box');
                    }
                    if (typeof e !== "undefined") {
                        $select = $("#shipping_state");
                        $select.removeData("dropkick");
                        $("#dk_container_shipping_state").remove();
                        $select.html('<option >State/Province</option>');
                        $('#dk_container_shipping_country').after($select);
                        $select.dropkick({
                            theme: 'black',
                            change: function(value, label) {
                                $(this).dropkick('theme', value);
                            }
                        });
                    }
                } else {
                    $('#dk_container_shipping_country').removeClass('dk_theme_black');
                    $('#dk_container_shipping_country').removeClass('dk_theme_');
                    if (typeof e !== "undefined") {
                        $('#dk_container_shipping_country').removeClass('invalid_box');
                        $.ajax({
                            type: 'POST',
                            url: baseUrl + "customshoe/get_states_ajax/" + country,
                            success: function(response) {
                                $select = $("#shipping_state");
                                $select.removeData("dropkick");
                                $("#dk_container_shipping_state").remove();
                                $select.html(response);
                                if (typeof state !== "undefined") {
                                    $select.find('option[value="' + state + '"]').attr('selected', true);
                                }
                                $('#dk_container_shipping_country').after($select);
                                $select.dropkick({
                                    theme: 'black',
                                    change: function(value, label) {
                                        $(this).dropkick('theme', value);
                                    }
                                });
                                $('#dk_container_shipping_state').removeClass('invalid_box');
                                $('#dk_container_shipping_state').removeClass('dk_theme_black');
                                $('#dk_container_shipping_state').removeClass('dk_theme_');
                                $select.bind('change', state_change);
                            },
                        });
                    }
                }
            }
            function billing_country_change(e, state) {
                var country = $('#billing_country').val();
                if (country === '') {
                    if ($('#billing_country').hasClass('invalid_box')) {
                        $('#dk_container_billing_country').addClass('invalid_box');
                    }
                    if (typeof e !== "undefined") {
                        $select = $("#card_state");
                        $select.removeData("dropkick");
                        $("#dk_container_card_state").remove();
                        $select.html('<option >State/Province</option>');
                        $('#dk_container_billing_country').after($select);
                        $select.dropkick({
                            theme: 'black',
                            change: function(value, label) {
                                $(this).dropkick('theme', value);
                            }
                        });
                    }
                } else {
                    $('#dk_container_billing_country').removeClass('dk_theme_black');
                    $('#dk_container_billing_country').removeClass('dk_theme_');
                    if (typeof e !== "undefined") {
                        $('#dk_container_billing_country').removeClass('invalid_box');
                        $.ajax({
                            type: 'POST',
                            url: baseUrl + "customshoe/get_states_ajax/" + country,
                            success: function(response) {
                                $select = $("#card_state");
                                $select.removeData("dropkick");
                                $("#dk_container_card_state").remove();
                                $select.html(response);
                                if (typeof state !== "undefined") {
                                    $select.find('option[value="' + state + '"]').attr('selected', true);
                                }
                                $('#dk_container_billing_country').after($select);
                                $select.dropkick({
                                    theme: 'black',
                                    change: function(value, label) {
                                        $(this).dropkick('theme', value);
                                    }
                                });
                                $('#dk_container_card_state').removeClass('invalid_box');
                                $('#dk_container_card_state').removeClass('dk_theme_black');
                                $('#dk_container_card_state').removeClass('dk_theme_');
                                // $select.bind('change',state_change);
                            },
                        });
                    }
                }
            }
            function editDetails(type) {
                $('#' + type + 'DetailsNonEditable').addClass('hidden');
                $('#' + type + 'DetailsEditable').removeClass('hidden');
            }
            function zipcode_search() {
                var zipcode = $(this).val();
                var id = $(this).attr('id');
                if (zipcode === '') {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: baseUrl + "customshoe/zipcode_search",
                    data: {zipcode: zipcode},
                    dataType: 'json',
                    success: function(response) {
                        var postalcode_data = response.postalCodes;
                        if (!postalcode_data.length) {
                            return;
                        }

                        postalcode_data = postalcode_data[0];
                        if (postalcode_data.country === '') {
                            return;
                        }
                        if (id === 'shipping_zipcode') {
                            $('#shipping_city').val((typeof postalcode_data.adminName2 === 'undefined') ? postalcode_data.adminName2 : postalcode_data.placeName);
                            $('#dk_container_shipping_country .dk_label').text(postalcode_data.country);
                            $('#dk_container_shipping_country').removeClass('invalid_box');
                            $('#dk_container_shipping_country').removeClass('dk_theme_black');
                            $('#dk_container_shipping_country').removeClass('dk_theme_');
                            $('#selectedShippingStateId').val(postalcode_data.country);
                            $('#shipping_country option[value="' + postalcode_data.country + '"]').attr('selected', true);
                            shipping_country_change('', postalcode_data.adminName1);
                        } else {
                            $('#billing_city').val((typeof postalcode_data.adminName2 === 'undefined') ? postalcode_data.adminName2 : postalcode_data.placeName);
                            $('#dk_container_billing_country .dk_label').text(postalcode_data.country);
                            $('#dk_container_billing_country').removeClass('invalid_box');
                            $('#dk_container_billing_country').removeClass('dk_theme_black');
                            $('#dk_container_billing_country').removeClass('dk_theme_');
                            $('#selectedBillingStateId').val(postalcode_data.country);
                            $('#billing_country option[value="' + postalcode_data.country + '"]').attr('selected', true);
                            billing_country_change('', postalcode_data.adminName1);
                        }


                    }
                });

            }
            function GetCardType() {
                var number = $(this).val();
                if (number === '') {
                    $('#cardtype').removeClass();
                    return;
                }
                var regex = {'Mastercard': '^5[1-5]',
                    'Visa': '^4',
                    'AMEX': '^(34|37)',
                    'Discover': '^6011'
                };
                var re;
                $.each(regex, function(key, value) {
                    re = new RegExp(value);
                    if (number.match(re) !== null) {
                        $('#dk_container_payment_method .dk_label').text('Credit card');
                        $('#dk_container_payment_method').removeClass('invalid_box');
                        $('#dk_container_payment_method').removeClass('dk_theme_black');
                        $('#dk_container_payment_method').removeClass('dk_theme_');
                        $('#payment_method option[value="Credit card"]').attr('selected', true);
                        $('#cardtype').removeClass().addClass(key);
                    }
                });

            }
            function login() {
                $("#popupOverlay").show();
                $('body').css('overflow', 'hidden');
                $.ajax({
                    type: "GET",
                    url: baseUrl + "customshoe/login",
                    success: function(msg) {
                        var $selectionPopup = $('#signin_popup');
                        $selectionPopup.html(msg);
                        $selectionPopup.css("opacity", "0");
                        $selectionPopup.css({
                            'display': 'block',
                            'margin-left': document.body.clientWidth / 2 - $selectionPopup.width() / 2 - 30,
                        });
                        $selectionPopup.css("opacity", "1");
                        $selectionPopup.animate({"height": "470", "top": '95'}, 1200);

                        $(".fb_loginBtn").unbind().bind('click', function() {
                            window.open(baseUrl + 'facebooklogin', 'FB', 'width=400,height=300');

                        });
                    }
                });
            }
        </script>            
    </body>
</html>        
