<html>
    <head>
        <title>order_confirmation</title>
        <link rel="stylesheet" href="<?php echo base_url() . '/assets/css/style.css' ?>">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/css/responsive.css' ?>">
        <script type="text/javascript">

            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-40200583-1', 'awlandsundry.com');
            ga('send', 'pageview');

            ga('require', 'ecommerce', 'ecommerce.js');
<?php $total = $order_summary[0]['gross_amt'] - ($order_summary[0]['gross_amt'] * $order_summary[0]['discount_perc']) / 100; ?>
            ga('ecommerce:addTransaction', {
                'id': '<?php echo $order_summary[0]['order_id']; ?>', // transaction ID - required
                'affiliation': 'Awl and Sundry', // affiliation or store name
                'revenue': '<?php echo $total; ?>', // total - required
                'shipping': '<?php echo 0; ?>',
                'tax': '<?php echo $order_summary[0]['tax_amount']; ?>', // tax
            });

            // add item might be called for every item in the shopping cart
            // where your ecommerce engine loops through each item in the cart and
            // prints out _addItem for each
<?php foreach ($order['items'] as $item) { ?>
                ga('ecommerce:addItem', {
                    'id': '<?php echo $order_summary[0]['order_id']; ?>', // transaction ID - required
                    'sku': '<?php echo $item['item_id']; ?>', // SKU/code - required
                    'name': '<?php echo $item['item_name']; ?>', // product name
                    'price': '<?php echo $item['item_amt']; ?>', // unit price - required
                    'quantity': '1'               // quantity - required
                });
<?php } ?>
            ga('ecommerce:send'); //submits transaction to the Analytics servers



        </script>
        <!-- Facebook Conversion Code for MTO shoes checkout -->
        <script>
            (function() {
                var _fbq = window._fbq || (window._fbq = []);
                if (!_fbq.loaded) {
                    var fbds = document.createElement('script');
                    fbds.async = true;
                    fbds.src = '//connect.facebook.net/en_US/fbds.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(fbds, s);
                    _fbq.loaded = true;
                }
            })();
            window._fbq = window._fbq || [];
            window._fbq.push(['track', '6016476967927', {'value': '<?php echo $total; ?>', 'currency': 'USD'}]);
        </script>
        <noscript>
    <img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6016476967927&amp;cd[value]=<?php echo $total; ?>&amp;cd[currency]=USD&amp;noscript=1" />
    </noscript>
</head>
<body class="page cart" cz-shortcut-listen="true">

    <header id="header">
        <section id="toolbar" class="clearfix"><div class="toolbarInner clearfix"></div></section>
        <h1 id="logo">
            <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
        </h1>
    </header>

    <section class="main" style="margin-top:100px;">        
        <div class="wrapper clearfix">
            <div id="undefined-sticky-wrapper" class="sticky-wrapper" style="height: 51px;"><h2 style="">Order confirmation</h2></div>
            <div class="order clearfix">
                <div class="column">
                    <h3>Your Order summary</h3>
                    <ul class="order_summary">

                        <?php
                        $totalamt = 0;
                        $disc = 0;
                        foreach ($order['items'] as $item) {
                            $totalamt += $item['item_amt'];
                            $discountPerc = $item['discount_perc'];
                            ?>



                            <li><?php echo $item['item_name'] ?><span class="amount"><?php echo $item['item_amt'] ?>.00</span></li>
                            <?php
                        }
                        if ($discountPerc !== 0) {
                            $disc = ($totalamt * $discountPerc) / 100;
                            $totalamt = $totalamt - $disc;
                        }
                        ?>
                        <?php if ($disc - floor($disc) > 0) { ?>
                            <li>PROMO DISCOUNT<span class="amount"><?php echo $disc; ?></span></li>
                        <?php } else { ?>
                            <li>PROMO DISCOUNT<span class="amount"><?php echo $disc; ?>.00</span></li>
                        <?php } ?>
                        <li>SHIPPING<span class="amount">0.00</span></li>
                        <?php if ($order_summary[0]['tax_amount'] - floor($order_summary[0]['tax_amount']) > 0) { ?>
                            <li>TAXES<span class="amount"><?php echo $order_summary[0]['tax_amount']; ?></span></li>
                        <?php } else { ?>
                            <li>TAXES<span class="amount"><?php echo $order_summary[0]['tax_amount']; ?>.00</span></li>
                        <?php } ?>
                        <?php if ($totalamt + $order_summary[0]['tax_amount'] - floor($totalamt + $order_summary[0]['tax_amount']) > 0) { ?>
                            <li class="total">Order total<span class="amount"><?php echo $totalamt + $order_summary[0]['tax_amount'] ?></span></li>
                        <?php } else { ?>
                            <li class="total">Order total<span class="amount"><?php echo $totalamt + $order_summary[0]['tax_amount'] ?>.00</span></li>
                        <?php } ?>

                    </ul>
                </div>


                <div class="right">
                    <h3>THANK YOU! YOUR SHOE ORDER HAS BEEN FORWARDED TO OUR CUSTOM SHOP.</h3>
                    <p>
                        YOUR ORDER REFERENCE NUMBER IS  <strong><?php echo $order_summary[0]['invoice_no']; ?></strong>.<br>
                        YOU MAY USE THIS NUMBER TO CHECK YOUR ORDER STATUS VIA YOUR PERSONAL ACCOUNT PAGE.<br>
                        EXTIMATED DELIVERY TIME IS 4 WEEKS.<br>
                        PLEASE CHECK YOUR EMAIL FOR A PURCHASE ORDER CONFIRMATION AND RECEIPT.<br>
                        FEEL FREE TO <a href="<?php echo base_url() . 'contact-us' ?>">CONTACT US</a> WITH ANY QUESTIONS.<br>
                        THANK YOU FOR JOINING OUR SHOEMAKING REVOLUTION!<br>
                    </p>
                </div>

                <div class="absolute">
                    <a href="javascript:window.print();" class="button">Print this page</a>
                    <!--a href="#" class="button">Save this page as a pdf</a-->
                </div>

            </div>
            <div class="links wrapper clearfix">
                <ul class="top">
                    <li class="homepage"><a href="<?php echo base_url() . 'my-account' ?>"><span>Go to my homepage</span></a></li>		 
                </ul>
            </div>
            <div class="action">
                <div class="default"></div>
                <div class="changable homepage hidden"><p><strong>return</strong> to your personal home page to create yet another original shoe.</p></div>
            </div>	     
        </div>		        
    </section>

    <footer id="footer" style="display: none">
        <div class="wrapper">
            <section class="clearfix">
                <span class="close"></span>
                <div class="row">
                    <h3>Custom footwear</h3>
                    <ul>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">View existing designs</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Create a shoe</a></li>
                    </ul>
                </div>
                <div class="row">
                    <h3>Awl &amp; sundry</h3>
                    <ul>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Our story</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Faq's</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Return policy</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Shipping</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Privacy policy</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Our blog</a></li>
                    </ul>
                </div>
                <div class="row">
                    <h3>Personal service</h3>
                    <ul>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">My account page</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Account settings</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Billing info</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Order status</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Contact us</a></li>
                    </ul>
                </div>
                <div class="row newsletter">
                    <h3>Get on our list</h3>
                    <form method="post">
                        <input type="email" class="email">
                        <input type="submit" class="submit">
                    </form>
                    <ul>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Privacy policy</a></li>
                        <li><a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#">Terms of use</a></li>
                        <li><span>Connect with us:</span>					
                            <div id="socials">
                                <a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#" class="facebook">Facebook</a>
                                <a href="http://robocza3.linuxpl.info/awl/order_confirmation.html#" class="twitter">Twitter</a>
                            </div>
                        </li>
                    </ul>
                </div>				
            </section>
        </div>
    </footer>
    <div id="popupOverlay"></div>	

    <?php
    $total_amt = $order_summary[0]['gross_amt'] - ($order_summary[0]['gross_amt'] * $order_summary[0]['discount_perc']) / 100 + $order_summary[0]['tax_amount'];
    $now = time();
    $referal_candy_secret_key = 'f0fe09c160f1a8b8cdadf89d847a0215';
    $code = $shipping[0]['email'] . ',' . $shipping[0]['firstname'] . ',' . $total_amt . ',' . $now . ',' . $referal_candy_secret_key;
    ?>

    <div
        id="refcandy-popsicle"
        data-app-id="fg1fq5o0wzswo7ubrnoydmrxd"
        data-fname="<?php echo $shipping[0]['firstname']; ?>"
        data-lname="<?php echo $shipping[0]['lastname']; ?>"
        data-email="<?php echo $shipping[0]['email']; ?>"
        data-amount="<?php echo $total_amt; ?>"
        data-currency="USD"
        data-timestamp="<?php echo $now; ?>"
        data-signature="<?php echo MD5($code); ?>"
        ></div>
    <script>(function(e) {
            var t, n, r, i, s, o, u, a, f, l, c, h, p, d, v;
            f = "script";
            l = "refcandy-purchase-js";
            c = "refcandy-popsicle";
            p = "go.referralcandy.com/purchase/";
            t = "data-app-id";
            r = {email: "a", fname: "b", lname: "c", amount: "d", currency: "e", "accepts-marketing": "f", timestamp: "g", "referral-code": "h", locale: "i", signature: "ab"};
            i = e.getElementsByTagName(f)[0];
            s = function(e, t) {
                if (t) {
                    return"" + e + "=" + encodeURIComponent(t)
                } else {
                    return""
                }
            };
            d = function(e) {
                return"" + p + h.getAttribute(t) + ".js?lightbox=1&aa=75&"
            };
            if (!e.getElementById(l)) {
                h = e.getElementById(c);
                if (h) {
                    o = e.createElement(f);
                    o.id = l;
                    a = function() {
                        var e;
                        e = [];
                        for (n in r) {
                            u = r[n];
                            v = h.getAttribute("data-" + n);
                            e.push(s(u, v))
                        }
                        return e
                    }();
                    o.src = "" + e.location.protocol + "//" + d(h.getAttribute(t)) + a.join("&");
                    return i.parentNode.insertBefore(o, i)
                }
            }
        })(document);</script>
    <!-- Quantcast Tag -->
    <script type="text/javascript">
        var _qevents = _qevents || [];

        (function() {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
        })();

        _qevents.push({
            qacct: "p-m_BVuPKPX136j"
        });
    </script>

    <noscript>
    <div style="display:none;">
        <img src="//pixel.quantserve.com/pixel/p-m_BVuPKPX136j.gif" border="0" height="1" width="1" alt="Quantcast"/>
    </div>
    </noscript>
    <!-- End Quantcast tag -->
</body></html>