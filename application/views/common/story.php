

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <?php $this->load->view('common/template/begin',@$data); ?>
    <body class="page about">
        <header id="header">
            <section id="toolbar">
                <div class="toolbarInner clearfix">	
                    <div class="wrapper content clearfix">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Our story</span>
                        </div>

                        <div class="right"> 
                            <ul id="headerNavLinks">

                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li> 
                                <?php $this->load->view('common/login_status'); ?>
                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
            <div class="wrapper shopping_cart_pop clearfix">
                <section class="shopping_cart_mini">
                    <h4>ORDER CART</h4>
                    <h2 class="emptyCart">Cart is Empty.</h2>
                </section>
            </div>
            <div class="patterned-rule-blue"  ></div>
        </header>
        <section id="middle_container"  >

            <div class="wrapper clearfix quote">
                <h1 class="align-left">We believe a fine pair of shoes<br/>shouldn’t be a luxury...</h1>
                <!--<h4>we founded awl & sundry to bring attentively handcrafted men’s shoes within reach.</h4>-->
            </div>

            <section class="wrapper clearfix main">
                <section id="sidebar" class="sidebar">
                    <ul>
                        <li class="active"><a href="<?php echo base_url(); ?>our-story">Our story</a></li>
                        <li><a href="<?php echo base_url(); ?>our-artisanal-process">our artisanal process</a></li>
                    </ul>
                </section>
                <section class="content">
                    <div class="img-container">
                        <img src="<?php echo base_url(); ?>assets/img/content/about1.png" />
                    </div>
                    <p>We at Awl & Sundry mean to bridge the growing divide between price and quality, between elite luxury and modest furnishing, between fine shoes and the men who love them. Because that is who we are.</p>
                    <h1>Why do we do what we do?</h1>
                    <p>We are a group of creatively inspired shoe hounds who have witnessed 

a disturbing disconnection in the market place. On the one hand the 

bejeweled selection of luxury brands crafting shoes with a high degree 

of skill and devotion. A construct of traditional craftsmanship and fine 

quality resulting in a shoe that is meant to last. These products are well 

beyond the practical realm of most men. On the other hand an expanding 

legion of mass-produced shoes offering customers a moderate price and 

larger quantity. Unfortunately these products often lack the attention 

for detailing. Generally imitating but missing the essential character and 

durability of their handmade counterparts. Priced for the average man and 

marketed as high quality footwear.</p>
                    <hr/>
                    <div class="width50">
                        <h2>Clearly, a revolution was in order.</h2>
                        <p>We set out to find a way to offer shoes of handmade quality, true durability and personalized distinction within a price range the everyday shoe hound like us could afford. In doing so we agreed that we would not compromise on the quality of the materials or the old-world methodology, experience and artisanal skill of the shoemakers themselves.</p>
                    </div>
                    <div class="width50">
                        <h2>A revolutionary idea taken up by shoemakers.</h2>
                        <p>The key to our success lies within a select group of seasoned artisans who take meticulous care in creating every shoe, adhering to proven methods of handmade construction including the enduring goodyear welting method of sewing the uppers to the soles still used by the best shoemaking shops and considered the gold standard in ensuring a long life against the elements.</p>
                    </div>
                    <div class="width50">
                        <h2>We looked to the past, we looked to the future.</h2>
                        <p>Ironically, we found our solution within the latest possibilites of internet technology: we developed a proprietary, high-fidelity online shoe design interface that allows our customers to become an integral part of the shoemaking process, the way they used to be, creating personalized footwear by choosing from a variety of shoe lasts, patterns, colors and materials.</p>
                    </div>
                    <div class="width50">
                        <h2>We’re bringing handmade shoes to the people!</h2>
                        <p>And lastly, because our shoes are all made-to-order, without need of retail stores, inventory, middlemen and the costly overhead that goes with all of that, we enable you to submit your custom orders directly to our shop. This is how we can offer you a luxury-class product for a price that, you might say, steps on the toes of the traditional luxury market.</p>
                    </div>
                    <hr class="last"/>

                </section>
            </section>	


        </section>            

        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>

    </body>
</html>        

