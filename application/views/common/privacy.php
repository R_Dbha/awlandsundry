<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <head>
    <?php $this->load->view('common/template/begin'); ?>       
    	<body class="page about privacy">
    <header id="header">
        <section id="toolbar" class="clearfix">	
            <div class="toolbarInner clearfix">
                <div class="wrapper content">
                    <div id="breadcrumb" class="left">
                        <a href="<?php echo base_url();?>">Home</a>
                        <span>Privacy Policy</span>
                    </div>
                    
				<div class="right"> 
                        <ul id="headerNavLinks">
                        
        		                	<li class="top_our_story">
                        <a href="<?php echo base_url();?>our-story">Our Story</a>
                    </li> 
	        		<li class="login">
        				<a href="javascript:void(0);">Log in</a>
        			</li> 
	        	        			<li class="cart">
        				<a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
        			</li>
        		</ul>
		        
        		</div>
</div>
            </div>
        </section>
        <h1 id="logo">
            <a href="<?php echo base_url();?>">Awl&amp;Sundry</a>
        </h1>
        <div class="wrapper shopping_cart_pop clearfix">
            <section class="shopping_cart_mini"></section>
        </div>
        <div class="patterned-rule-blue"></div>
    </header>
    <section id="middle_container"> 
        <div class="wrapper clearfix quote">
            <h1>Privacy Policy</h1>
        </div>

        <section class="wrapper clearfix main">

            <section class="content">
                <p>Welcome to Awl & Sundry. Awl & Sundry LLC (“Awl & Sundry”, “we”, “our”, or “us”) is the owner and operator of awlandsundry.com (“Website”). Our goal is to provide shoppers and those who purchase from us (“Customer”, “you”, or “your”) with access to quality handmade and custom footwear using our unique online shoe design interface. The following is our Privacy Policy (“Policy”) which explains our online information practices and the choices you can make about the way your information is collected and used. This Policy covers personally identifiable information (“Personal Data”), anonymous data collection and aggregate reporting. Please carefully read these this Policy before using or obtaining any information, products, or services through our Website. By accessing or using our Website, you expressly agree to be bound by the provisions of this Policy, as well as our Terms and Conditions of Use, which create a legal and enforceable agreement whether or not you purchase from us. If you do not agree to all of the provisions of this Policy, as well as our Terms and Conditions of Use, do not browse or use our Website.</p>
                <p><b style="font-weight: bold;">By browsing or using our Website or allowing someone to do so on your behalf, you are consenting to our collection, use, disclosure, transfer and storage in accordance with this Privacy Policy of any Personal Data or other information received by us as a result of such use. </b></p>

                <p>We reserve the right to change or modify our information collection, use and disclosure practices set forth in this Policy. We will use and disclose Personal Data in accordance with the Privacy Policy that was in effect when such information was collected.</p>

                <h2>1. Information We Collect.</h2>

                <ul class="char">
                    <li><span>(a)</span>&nbsp;&nbsp;&nbsp;&nbsp;Personal Data is collected from you when you create an account with our Website or transact with us. Personal Data means any information that may be used to identify an individual, including but not limited to, your name, address, telephone number, e-mail address, shipping address, and billing information regarding your payment method (credit card information).</li>
                    <li><span>(b)</span>&nbsp;&nbsp;&nbsp;&nbsp;When you browse our Website, you do so anonymously, unless you have previously created an account with us. However, we may log your IP address (the Internet address of your computer) to give us an idea of which part of our Website you visit and how long you spend there. However, we do not link your IP address to any Personal Data unless you have logged into our Website. Like many other commercial websites, our Website may use a standard technology called a “cookie” to collect information about how you interact with our Website. Please see “Use of Cookies” below for more information. </li>
                </ul>

                <h2>2. How We Use It.</h2>
                <h3 class="subline">We use, allow access to, or disclose your Personal Data to third parties to:</h3>
                <ul class="dot">
                    <li>Allow for secure payment online;</li>
                    <li>Fill your order and send you an order confirmation;</li>
                    <li>Provide you with products you purchase from us;</li>
                    <li>Notify you of new products, upgrades, and special offers provided by us;</li>
                    <li>Increase the usability of our Website;</li>
                    <li>Give you the opportunity to participate in promotional offers;</li>
                    <li>Respond to requests for assistance from our customer support team.</li>
                </ul>


                <h2>3. Who We Share It With.</h2>
                <ul class="char">
                    <li><span>(a)</span>&nbsp;&nbsp;&nbsp;&nbsp;We never sell or rent your Personal Data. We will use or disclose Personal Data to the extent necessary, in our sole discretion, to provide products and/or services to you (for example, but not limited to payment processing). We also use Personal Data to carry on our business generally. </li>
                    <li><span>(b)</span>&nbsp;&nbsp;&nbsp;&nbsp;We may disclose Personal Data to the extent required by law or by court order.</li>
                    <li><span>(c)</span>&nbsp;&nbsp;&nbsp;&nbsp;We may disclose Personal Data to protect our rights and property, to prevent fraudulent activity or other deceptive practices, or to prevent a likely threat of physical harm to others.</li>
                    <li><span>(d)</span>&nbsp;&nbsp;&nbsp;&nbsp;We may transfer Personal Data if Awl & Sundry is acquired by (or merged with) another company, or if the assets of Awl & Sundry are sold to another company. In all of these circumstances, you understand and agree that our Terms and Conditions of Use and this Policy will be assigned and delegated to the other company.</li>
                    <li><span>(e)</span>&nbsp;&nbsp;&nbsp;&nbsp;With appropriate confidentiality agreements in place, Personal Data or other information may be shared with our agents and contractors to perform services for us.</li>
                    <li><span>(f)</span>&nbsp;&nbsp;&nbsp;&nbsp;When you create an account with us, we will not share your Personal Data with third parties without your permission, other than for the limited exceptions already listed and it will only be used for the purposes stated above. </li> 
                </ul>

                <h2>4. Internet Commerce.</h2>
                <p>The process of creating an account with us is designed to give you options concerning the privacy of your credit card information, name, address, e-mail address and any other information you provide to us. We are committed to data security with respect to information collected through our Website and/or services.</p>


                <h2>5. Security of Your Personal Information.</h2> 
                <ul class="char">
                    <li><span>(a)</span>&nbsp;&nbsp;&nbsp;&nbsp;We exercise ordinary care and prudence in protecting the security of Personal Data provided by you. We carefully protect your data from loss, misuse, unauthorized access or disclosure, alteration, or destruction. Specifically, we use the Secured Socket Layer (SSL) encryption when collecting or transferring sensitive data. Personal Data is stored in password-controlled servers with limited access. </li>
                    <li><span>(b)</span>&nbsp;&nbsp;&nbsp;&nbsp;You also have a significant role in account security. No one can see or edit your Personal Data without knowing your user name and password, so do not share these with others.</li>
                </ul>


                <h2>6. Access to Your Personal Information.</h2> 
                <p>We will provide you with the means to ensure that your Personal Data is correct and current. Customers may review and update this information by logging into your account. To protect your privacy and security, we will also take reasonable steps to verify your identity, such as password and user name, before granting access to any Personal Data.</p>

                <h2>7.  Use of Cookies.</h2> 
                <p>We and third parties with whom we may partner may use cookies, clear GIFs (also known as ‘web beacons’), or local shared objects (sometimes called ‘flash cookies’) to help you personalize our Website. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a Web server in the domain that issued the cookie to you. A clear GIF is typically a transparent graphic image (usually 1 pixel by 1 pixel in size) that is used in conjunction with our Website, which allows us to measure the actions of Customers who open pages that contain the clear gif. We use clear GIFs to measure traffic and related browsing behavior, and to improve your experience when using our Website. We may also use customized links or other similar technologies to track hyperlinks that you click, and associate that information with your Personal Data in order to provide you with more focused communications. You have the ability to accept or decline cookies and Clear GIFs may be unusable if you elect to reject cookies. Most Web browsers automatically accept cookies, but you can usually modify the browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of our Website.</p>

                <h2>8. Compliance with the Children’s Online Policy Protection Act.</h2> 
                <p>Protecting the privacy young people is especially important. For that reason, we never collect or maintain information through our Website from those we actually know are under thirteen (13), and no part of our Website is structured to attract anyone under thirteen (13). </p>

                <h2>9. Affiliated Businesses We Do Not Control.</h2> 
                <p>We may use third-party companies and individuals to perform functions on our behalf. Examples include fulfilling orders, processing payments, hosting, data storage, sending postal mail and e-mail, removing repetitive information from customer lists, analyzing data, providing marketing assistance, providing search results and links (including paid listings and links), processing credit card payments, and providing customer service. We will provide such entities with access to certain information needed to perform their functions, but will take measures to ensure that they may not use it for other purposes. We share information only as described above and with third parties that are either subject to this Policy or to their own privacy policy that is at least as protective as this Policy.</p>

                <h2>10. Questions or Comments Regarding this Policy.</h2>
                <p>We value your comments and opinions. If you have questions, comments or a complaint about compliance with this Policy, you may send a written notice to us at: <a href="mailto:wecare@awlandsundry.com">wecare@awlandsundry.com</a></p>



            </section>
        </section>		

    </section>            
                
        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
        </body>
</html>        
   
