

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <?php $this->load->view('common/template/begin'); ?>    
    <body class="page about">
        <header id="header">
            <section id="toolbar" class="clearfix">	
                <div class="toolbarInner clearfix">
                    <div class="wrapper content">
                        <div id="breadcrumb" class="left">
                            <a href="<?php echo base_url(); ?>">Home</a>
                            <span>Our story</span>
                        </div>

                        <div class="right"> 
                            <ul id="headerNavLinks">

                                <li class="top_our_story">
                                    <a href="<?php echo base_url(); ?>our-story">Our Story</a>
                                </li> 
                                <?php $this->load->view('common/login_status'); ?>
                                <li class="cart">
                                    <a id="shopping_cart_mini" href="javascript:;">Cart <span class="number" id="cartcount">&nbsp;</span></a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&amp;Sundry</a>
            </h1>
            <div class="wrapper shopping_cart_pop clearfix">
                <section class="shopping_cart_mini">
                    <h4>ORDER CART</h4>
                    <h2 class="emptyCart">Cart is Empty.</h2>
                </section>
            </div>
            <div class="patterned-rule-blue"></div>
        </header>
        <section id="middle_container">

            <div class="wrapper clearfix quote">
                <h1>Our artisanal shoemaking process.</h1>
            </div>

            <section class="wrapper clearfix main">
                <section id="sidebar" class="sidebar">
                    <ul>
                        <li ><a href="<?php echo base_url(); ?>our-story">Our story</a></li>
                        <li class="active"><a href="<?php echo base_url(); ?>our-artisanal-process">our artisanal process</a></li>
                    </ul>
                </section>
                <section class="content">
                    <!--<video id="video-player" width="732" height="416" preload="" controls>
                            <source src="<?php echo base_url(); ?>files/videos/A&S (new).mov" />
                    </video> -->
                    <!--<video id="video-player" src="<?php echo base_url(); ?>files/videos/A&S (new).mov" width="732" height="416" preload="" controls>				</video>-->


                    <!--<video id="video-player" width="732" height="416" preload="" controls>
                            <source src="<?php echo base_url(); ?>files/videos/A&S (new).mp4" type="video/mp4"/>
                            <source src="<?php echo base_url(); ?>files/videos/A&S (new).mov" type="video/mov"/>
                    </video> -->

                    <video width="732" height="416" poster="<?php echo base_url(); ?>files/videos/A&S_(new).jpg" controls="controls" preload="">
                        <!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
                        <source type="video/mp4" src="<?php echo base_url(); ?>files/videos/A&S_(new).m4v" />
                        <!-- WebM/VP8 for Firefox4, Opera, and Chrome -->
                        <source type="video/webm" src="<?php echo base_url(); ?>files/videos/A&S_(new).webm" />
                        <!-- Ogg/Vorbis for older Firefox and Opera versions -->
                        <source type="video/ogg" src="<?php echo base_url(); ?>files/videos/A&S_(new).ogv" />
                        <!-- Flash fallback for non-HTML5 browsers without JavaScript -->
                        <object width="732" height="416" type="application/x-shockwave-flash" data="flashmediaelement.swf">
                            <param name="movie" value="flashmediaelement.swf" />
                            <param name="flashvars" value="controls=true&file=myvideo.mp4" />
                            <!-- Image as a last resort -->
                            <img src="<?php echo base_url(); ?>files/videos/A&S_(new).jpg" width="732" height="416" title="No video playback capabilities" />
                        </object>
                    </video>

                    <h1>They say the shoes make the man but we like it the other way around.</h1>
                    <p>Each shoe we produce is meticulously crafted by hand. In order to ensure highest quality and performance we believe in the necessity of a production process over several weeks.  Just like the art of cooking requires impeccable ingredients, fine shoes require a very specific choice of quality materials. Our only choice is full-grainleather.</p>
                    <p>Unlike synthetic or corrected leathers, the fullgrain leather from which we build every shoe is durable. This ensures a long-lasting product that will outperform those made from manufactured substitutes.  Furthermore our high-quality leather maintains its shine longer and is naturally porous allowing your pair to breathe and providing a well-balanced temperature within. Finally, due to the flexibility of the high-grade material the shoe can stretch and mold, ultimately leading to the creation of a truly custom-made experience.</p>
                    <hr/>
                    <div class="thumbs min">
                        <img src="http://awlandsundry.com/~awlsundr/public/img/content/about9.png" />
                        <img src="http://awlandsundry.com/~awlsundr/public/img/content/about10.png" />
                        <img src="http://awlandsundry.com/~awlsundr/public/img/content/about11.png" />
                    </div>
                    <h2>Hand-lasted, all leather construction.</h2>
                    <p class="two-columns">Our handmade process begins with a specific wooden ‘last’: the form which gives the shoe its unique shape. Leathers are then cut into panels, expertly stitched to patterns and stretched and formed using traditional handtooling methods. These uppers are then sewn into genuine leather soles using the goodyear welt process, adding hand-nailed, stacked leather heels (containing none of the synthetic materials of lesser shoes). The process is slow and labor-intensive but it yields a shoe that, well-cared-for, will retain its magic for many years.</p>
                    <hr/>
                    <img class="right" src="http://awlandsundry.com/~awlsundr/public/img/content/about-process.png" />
                    <h2>Hand-sewn goodyear welting on every shoe.</h2>
                    <p class="width50">Goodyear welting (named after the inventor Charles Goodyear) is an age-old process that has nothing to do with the tire brand but everything to do with the idea of tires. When your car tires wear out you don’t throw out the car, you change the tires. Similarly, shoes that use the Goodyear welting method can easily be resoled over and over again. The process, which uses two seperate rows of stitching, also provides excellent protection of the insoles against the elements. This is why the world’s most esteemed shoemakers still apply this expensive, time-intensive method in affixing their soles. No, we didn’t invent it, we just made it less of a luxury.</p>
                    <hr/>
                </section>
            </section>	

        </section>


        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>

    </body>
</html>        

