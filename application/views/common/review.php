<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <?php $this->load->view('common/template/begin'); ?>   
    </head>

    <body class="page cart">
        <header id="header">
            <section id="toolbar" class="clearfix"><div class="toolbarInner clearfix"></div></section>
            <h1 id="logo">
                <a href="<?php echo base_url(); ?>">Awl&Sundry</a>
            </h1>
        </header>

        <section class="main">
            <div class="wrapper clearfix">
                <h2>REVIEW & PLACE YOUR ORDER</h2>
                <div class="order reviewOrder clearfix">
                    <div class="column order_summary">
                        <h3>Your Order</h3>

                        <ul>
                            <?php
                            $total = 0;
                            foreach ($order['items'] as $item) {
                                ?>
                                <li>
                                    <?php echo $item['item_name'] ?>
                                    <span class="amount">
                                        <?php echo $item['item_amt'] ?>
                                    </span>
                                </li>
                                <?php
                                $total += $item['item_amt'];
                                $discountPerc = $item['discount_perc'];
                                if ($discountPerc !== 0) {
                                    $total = $total - (($total * $discountPerc) / 100);
                                }
                            }
                            ?>
                            <li class="total">
                                Order total
                                <span class="amount">
                                    <?php echo $total; ?>
                                </span>
                            </li>
                        </ul>
                        <a href="javascript:;"  id="orderedit"></a>
                    </div>					

                    <div class="column" id="billingdet">						<h3>YOUR BILLING DETAILS</h3>
                        <input type="hidden" id="hidden_billing_id" value="<?php echo $billing[0]['billing_details_id'] ?>" />
                        <input type="hidden" id="hidden_shipping_id" value="<?php echo $shipping[0]['shipping_details_id'] ?>" />
                        <input type="hidden" id="hidden_order_id" value="<?php echo $billing[0]['order_id'] ?>" />
                        <ul>
                            <!--li>CARD TYPE : Paypal</li>
                            <li>CARD # : 22222</li-->		
                            <li>STREET ADDRESS 1 : <?php echo $billing[0]['address1'] ?></li>	
                            <li>STREET ADDRESS 2 : <?php echo $billing[0]['address2'] ?></li>	
                            <li>CITY, STATE  ZIPCODE : <?php echo $billing[0]['city'] . ',' . $billing[0]['state'] . ',' . $billing[0]['zipcode'] ?></li>
                        </ul>

                        <div class="err" style="width: 231px; font-size: 12px; display: inline-block; color: red;"></div>

                        <a href="javascript:;" class="button edit" onClick="editYourOrder('billing');">Edit</a>
                    </div>



                    <div class="column" id="shippingdet">
                        <h3>YOUR SHIPPING DETAILS</h3>

                        <ul>
                            <li>FIRST NAME : <?php echo $shipping[0]['firstname'] ?></li>
                            <li>LAST NAME : <?php echo $shipping[0]['lastname'] ?> </li>		
                            <li>EMAIL ADDRESS : <?php echo $shipping[0]['email'] ?></li>	
                            <li>TELEPHONE : <?php echo $shipping[0]['telephone'] ?></li>	

                            <li>ADDRESS LINE 1 : <?php echo $shipping[0]['address1'] ?></li>	
                            <li>ADDRESS LINE 2 : <?php echo $shipping[0]['address2'] ?></li>					       
                            <li>CITY, STATE  ZIPCODE :  <?php echo $shipping[0]['city'] . ',' . $shipping[0]['state'] . ',' . $shipping[0]['zipcode'] ?></li>
                        </ul>

                        <a href="javascript:;" class="button edit" onClick="editYourOrder('shipping');">Edit</a>
                    </div>

                </div>
                <div class="links wrapper clearfix">
                    <ul class="top">
                        <li class="return"><a href="/~awlsundr/public/home/checkout"><span>Return to details</span></a></li>		 

                        <li class="purchase"><a href="<?php echo base_url(); ?>welcome/payByCard/<?php echo $billing[0]['order_id'] ?>" ><span>Complete purchase</span></a></li>		 
                    </ul>
                </div>
                <div class="action">
                    <div class="default"></div>
                    <div class="changable return hidden"><p><strong>return</strong> to the previous step to edit your billing / shipping details.</p></div>
                    <div class="changable purchase hidden"><p><strong>Click</strong> this button to complete your purchase and submit your custom shoe order</p></div>
                </div>	     
            </div>	        
        </section>

        <div id="popupOverlay"></div>

        <div class="messageBox editOrderDetails" id="billing_details">
            <div class="editOrderDetailsInner order billing">
                <a onclick="editYourOrderClose();" href="javascript:;" class="cancel_sign">&nbsp;</a>
                <h2>Edit Billing Details</h2>
                <div class="edit_address_fields">
                    <input type="text" name="billing_street_1" id="billing_street_1" placeholder="Street address 1" class="alphaNumericOnly" value="asd" />

                    <input type="text" name="billing_street_2" class="alphaNumericOnly" placeholder="Street address 2" id="billing_street_2" value="asd" />

                    <input type="text" name="billing_city" id="billing_city" placeholder="City" value="asd" />

                    <select name="card_state" id="card_state">
                        <option value="AL" selected='selected'>AL</option>
                        <option value="AK" >AK</option>
                        <option value="AZ" >AZ</option>
                        <option value="AR" >AR</option>
                        <option value="CA" >CA</option>
                        <option value="CO" >CO</option>
                        <option value="CT" >CT</option>
                        <option value="DE" >DE</option>
                        <option value="DC" >DCoption>
                        <option value="FL" >FL</option>
                        <option value="GA" >GA</option>
                        <option value="HI" >HI</option>
                        <option value="ID" >ID</option>
                        <option value="IL" >IL</option>
                        <option value="IN" >IN</option>
                        <option value="IA" >IA</option>
                        <option value="KS" >KS</option>
                        <option value="KY" >KY</option>
                        <option value="LA" >LA</option>
                        <option value="ME" >ME</option>
                        <option value="MD" >MD</option>
                        <option value="MA" >MA</option>
                        <option value="MI" >MI</option>
                        <option value="MN" >MN</option>
                        <option value="MS" >MS</option>
                        <option value="MO" >MO</option>
                        <option value="MT" >MT</option>
                        <option value="NE" >NE</option>
                        <option value="NV" >NV</option>
                        <option value="NH" >NH</option>
                        <option value="NJ" >NJ</option>
                        <option value="NM" >NM</option>
                        <option value="NY" >NY</option>
                        <option value="NC" >NC</option>
                        <option value="ND" >ND</option>
                        <option value="OH" >OH</option>
                        <option value="OK" >OK</option>
                        <option value="OR" >OR</option>
                        <option value="PA" >PA</option>
                        <option value="RI" >RI</option>
                        <option value="SC" >SC</option>
                        <option value="SD" >SD</option>
                        <option value="TN" >TN</option>
                        <option value="TX" >TX</option>
                        <option value="UT" >UT</option>
                        <option value="VT" >VT</option>
                        <option value="VA" >VA</option>
                        <option value="WA" >WA</option>
                        <option value="WV" >WV</option>
                        <option value="WI" >WI</option>
                        <option value="WY" >WY</option>
                    </select>
                    <input type="text" name="billing_zipcode"  placeholder="Zipcode" pattern=".{5,5}" maxlength="5" id="billing_zipcode" class="zipcode numericOnly" value="32121" />

                    <div class="buttonWrap"><a href="javascript:updatebilling();" class="button edit_submit" id="billingsubmit">Submit</a></div>

                </div>

                <div class="invalid_errors">
                    <div class="invalid invalid_required"></div>
                </div>


            </div>
        </div>

        <div class="messageBox editOrderDetails" id="shipping_details">
            <div class="editOrderDetailsInner order billing">
                <a onclick="editYourOrderClose();" href="javascript:;" class="cancel_sign">&nbsp;</a>
                <h2>Edit Shipping Details</h2>
                <div class="edit_address_fields">
                    <input type="text" id="firstname" name="firstname" placeholder="First name" value="sad" />
                    <input type="text" id="lastname" name="lastname" placeholder="Last name" value="as" />
                    <input type="text" name="email" id="email" placeholder="Email Address" value="sad@dd.com" /><br />
                    <input type="text" name="telephone" id="telephone" placeholder="Telephone" value="" />
                    <input type="text" name="sstreet1" id="sstreet1" class="alphaNumericOnly" placeholder="Street address 1" value="sasd" />
                    <input type="text" name="sstreet2" id="sstreet2" class="alphaNumericOnly" placeholder="Street address 2" value="sasdas" />
                    <input type="text" name="scity" id="scity" placeholder="City" value="asd" /> 

                    <select name="shipping_state" id="shipping_state">
                        <option value="AL" selected='selected'>AL</option>
                        <option value="AK" >AK</option>
                        <option value="AZ" >AZ</option>
                        <option value="AR" >AR</option>
                        <option value="CA" >CA</option>
                        <option value="CO" >CO</option>
                        <option value="CT" >CT</option>
                        <option value="DE" >DE</option>
                        <option value="DC" >DC</option>
                        <option value="FL" >FL</option>
                        <option value="GA" >GA</option>
                        <option value="HI" >HI</option>
                        <option value="ID" >ID</option>
                        <option value="IL" >IL</option>
                        <option value="IN" >IN</option>
                        <option value="IA" >IA</option>
                        <option value="KS" >KS</option>
                        <option value="KY" >KY</option>
                        <option value="LA" >LA</option>
                        <option value="ME" >ME</option>
                        <option value="MD" >MD</option>
                        <option value="MA" >MA</option>
                        <option value="MI" >MI</option>
                        <option value="MN" >MN</option>
                        <option value="MS" >MS</option>
                        <option value="MO" >MO</option>
                        <option value="MT" >MT</option>
                        <option value="NE" >NE</option>
                        <option value="NV" >NV</option>
                        <option value="NH" >NH</option>
                        <option value="NJ" >NJ</option>
                        <option value="NM" >NM</option>
                        <option value="NY" >NY</option>
                        <option value="NC" >NC</option>
                        <option value="ND" >ND</option>
                        <option value="OH" >OH</option>
                        <option value="OK" >OK</option>
                        <option value="OR" >OR</option>
                        <option value="PA" >PA</option>
                        <option value="RI" >RI</option>
                        <option value="SC" >SC</option>
                        <option value="SD" >SD</option>
                        <option value="TN" >TN</option>
                        <option value="TX" >TX</option>
                        <option value="UT" >UT</option>
                        <option value="VT" >VT</option>
                        <option value="VA" >VA</option>
                        <option value="WA" >WA</option>
                        <option value="WV" >WV</option>
                        <option value="WI" >WI</option>
                        <option value="WY" >WY</option>
                    </select>

                    <input type="text" name="shipping_zipcode" id="shipping_zipcode" class="zipcode numericOnly" placeholder="Zipcode" value="23333" />
                    <div class="buttonWrap"><a href="javascript:updateshipping();" class="button edit_submit" id="shippingsubmit">Submit</a></div>


                </div>

                <div class="invalid_errors">
                    <div class="invalid invalid_required"></div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
                            function editYourOrder(type) {
                                jQuery('#popupOverlay').fadeIn();
                                var $selectionPopup = $("#" + type + "_details");
                                if (type === "billing")
                                {
                                    setUpbillingform();
                                }
                                else if (type === "shipping")
                                {
                                    setUpshippingform();
                                }
                                //$selectionPopup.addClass('popupbounce');
                                $selectionPopup.css("opacity", "0");
                                $selectionPopup.css({
                                    'display': 'block',
                                    //'top': $(window).height()/2 + $(window).scrollTop(),
                                    'left': jQuery(window).width() / 2,
                                    'margin-left': '-' + ($selectionPopup.width() / 2) + 'px',
                                    'margin-top': '-' + (405 / 2) + 'px'
                                });
                                $selectionPopup.css("opacity", "1");
                                $selectionPopup.animate({"height": "425", "top": jQuery(window).height() / 2 + jQuery(window).scrollTop()}, 1200);

                                return false;
                            }



                            function setUpbillingform()
                            {
                                arr = new Array();
                                $('#billingdet').children().children().each(function(index) {
                                    arr[index] = ($(this).text().split(':')[1]);
                                });

                                $('#billing_street_1').val(arr[0]);
                                $('#billing_street_2').val(arr[1]);
                                $('#billing_city').val(arr[2].split(',')[0]);
                                $('#card_state').val(arr[2].split(',')[1]);
                                $('#billing_zipcode').val(arr[2].split(',')[2]);
                            }
                            function setUpshippingform()
                            {
                                arr = new Array();
                                $('#shippingdet').children().children().each(function(index) {
                                    arr[index] = ($(this).text().split(':')[1]);
                                });
                                $('#firstname').val(arr[0]);
                                $('#lastname').val(arr[1]);
                                $('#email').val(arr[2]);
                                $('#telephone').val(arr[3]);
                                $('#sstreet1').val(arr[4]);
                                $('#sstreet2').val(arr[5]);
                                $('#scity').val(arr[6].split(',')[0]);
                                $('#shipping_state option').removeAttr('selected');
                                $('#shipping_state option[value ="' + arr[6].split(',')[1] + '"]').attr('selected', true);

                                $('#dk_container_shipping_state .dk_label').text(arr[6].split(',')[1]);
                                //$('#shipping_state option[value ="'+$('#selectedShippingStateId').val()+'"]').attr('selected','selected');
                                $('#shipping_zipcode').val(arr[6].split(',')[2]);

                            }

                            function updatebilling()
                            {
                                $address1 = $('#billing_street_1').val();
                                $address2 = $('#billing_street_2').val();
                                $city = $('#billing_city').val();
                                $state = $('#card_state').val();
                                $zipcode = $('#billing_zipcode').val();
                                $order_id = $('#hidden_billing_id').val();
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>customshoe/updatebilling/" + $address1 + "/" + $address2 + "/" + $city + "/" + $state + "/" + $zipcode + "/" + $order_id,
                                    success: function(msg) {
                                        editYourOrderClose();
                                        if (msg)
                                        {
                                            alert('Business details Updated Successfully');
                                            if ($('#hidden_order_id').val()) {
                                                window.location.href = "<?php echo base_url(); ?>common/review/" + $('#hidden_order_id').val() + "";
                                            }
                                        }
                                        else
                                        {
                                            alert('Updation failed');
                                        }
                                    }
                                });
                            }
                            function updateshipping()
                            {
                                $('#shipping_zipcode').val(arr[6].split(',')[2]);
                                $firstname = $('#firstname').val();
                                $lastname = $('#lastname').val();
                                $email = encodeURIComponent($('#email').val());
                                $telephone = $('#telephone').val();
                                $address1 = $('#sstreet1').val();
                                $address2 = $('#sstreet2').val();
                                $city = $('#scity').val();
                                $state = $('#shipping_state').val();
                                $zipcode = $('#shipping_zipcode').val();
                                $orderid = $('#hidden_shipping_id').val();
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>customshoe/updateshipping/" + $firstname + "/" + $lastname + "/" + $email + "/" + $telephone + "/" + $address1 + "/" + $address2 + "/" + $city + "/" + $state + "/" + $zipcode + "/" + $orderid,
                                    success: function(msg) {
                                        editYourOrderClose();
                                        if (msg)
                                        {
                                            alert('Shipping details Updated Successfully');
                                            if ($('#hidden_order_id').val()) {
                                                window.location.href = "<?php echo base_url(); ?>common/review/" + $('#hidden_order_id').val() + "";
                                            }
                                        }
                                        else
                                        {
                                            alert('Updation failed');
                                        }
                                    }
                                });
                            }
                            $('#orderedit').click(function() {

                            });

                            $('#billingsubmit').click(function() {
                                //alert('a');
                                /* $cardtype = $('#payment_method').val();
                                 $cardno = $('#card_number').val();
                                 $cardowner = $('#card_owner').val();
                                 $expmonth = $('#exp_date_month').val();
                                 $expyear = $('#exp_date_year').val();
                                 $cvvcode = $('#card_cvv_code').val();*/
                                $address1 = $('#billing_street_1').val();
                                $address2 = $('#billing_street_2').val();
                                $city = $('#billing_city').val();
                                $state = $('#card_state').val();
                                $zipcode = $('#billing_zipcode').val();

                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>/costomshoe/updatebilling",
                                    dataType: "json",
                                    data: {
                                        address1: $address1,
                                        address2: $address2,
                                        city: $city,
                                        state: $state,
                                        zipcode: $zipcode,
                                        type: 'billing',
                                        order_id: 1
                                    },
                                    success: function(msg) {
                                        if (msg.response == 'true') {
                                            location.reload();
                                        } else {
                                            $('.invalid_required').html(msg.errormessage);
                                        }
                                    }
                                });
                            });

                            $('#shippingsubmit').click(function() {


                            });

        </script>


        <?php $this->load->view('common/template/footer'); ?>
        <?php $this->load->view('common/template/jscontainer'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/plugins_slider_video.js"></script>
    </body>
</html>        
