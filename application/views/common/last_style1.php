<section class="createNewContainer" style="position: relative;">
    <?php
    $this->load->view('common/template/step_container');
    ?>
    <div class="basicStyleContainer clearfix pick_your_last last" style="position: relative; top:0px; left:0px; ">
        <div class="basicStyleBox styles <?php //echo $adlClass;    ?>" style="margin:0;">

            <ul class="shapeTypeList">
                <?php
                foreach ($styles as $style) {
                    ?>
                    <li class="last-item" id="<?php echo $style['style_name']; ?>" > 
                        <h2><?php echo $style['style_name']; ?>
                            <img src="https://awlandsundry.com/members/assets/img/icons/help.png" title="<?php echo $style['style_description']; ?>" class="q_symbol">
                        </h2>
                        <div class="imgShap" title="<?php echo $style['style_description']; ?>" >
                            <img src="<?php echo ApplicationConfig::IMG_BASE . 'thumb/' . $style['img_file'] . '_A0.png'; ?>">
                            <img class="active" src="<?php echo ApplicationConfig::IMG_BASE . 'thumb/' . $style['img_file'] . '_A7.png'; ?>">
                        </div>
                        <span><?php ?></span> 
                    </li>
                    <?php
                } 
                ?>
            </ul>
        </div>
    </div>
    <?php
    foreach ($lasts as $key => $last) {
        ?>
        <div class="basicStyleContainer clearfix pick_your_last style <?php echo $key; ?>" style="position: relative; top:0px; left:0px;">

            <!--div style="float: left; width: 50px; height: 450px; display: block;padding:0 20px; ">
                <a href="javascript:void();"  class="control-back"  title="Load previous last">
                </a>
            </div!-->
            <div class="controls">
                <a href="javascript:;" class="control-back control" id="prev"></a>
            </div>
            <div class="basicStyleBox" style="margin:0;">

                <ul class="shapeTypeList">
    <?php
    foreach ($last as $style) {
        ?>
                        <li class="last-style " > 
                            <input type="hidden" class="ls_style_id" name="ls_style_id" value="<?php echo $style['last_style_id']; ?>" />
                            <div class="imgShap" title="<?php echo $style['style_description']; ?>">
                                <img src="<?php echo ApplicationConfig::IMG_BASE . 'thumb/' . $style['img_file'] . '_A7.png'; ?>">
                                <img class="active" src="<?php echo ApplicationConfig::IMG_BASE . 'thumb/' . $style['img_file'] . '_A7.png'; ?>">
                            </div>
                            <span><?php echo $style['last_name']; ?>
                                <img src="https://awlandsundry.com/members/assets/img/icons/help.png" title="<?php echo $style['description']; ?>" class="q_symbol">
                            </span> 
                        </li>
        <?php
    }
    ?>
                </ul>
            </div>

        </div>

<?php } ?>

    <div class="wrapper clearfix">
        <div id="design_main_links">
            <ul class="top">
                <li class="inspiration"><a href="<?php echo base_url(); ?>index.php/common/inspiration"><span>Get Inspired</span></a></li>
                <li class="create"><a href="javascript:;" id="steptwosubmit"><span>Select &amp; continue...</span></a></li>
            </ul>
            <div class="action">
                <div class="default"></div>
                <div class="changable hidden inspiration"><p><strong>view</strong> the creation’s of others; buy them, use them for inspiration or rework them to your liking</p></div>
                <div class="changable hidden create select_one"><p><strong>Proceed</strong> with this classic last and style to the next step to specify design features, materials and colors</p></div>
            </div>

        </div>
    </div>
</section>
<div id="shape_selection_popup" class="shape_popup step_one_popup shape_selection_popup" style="margin-top: 109px;">
    <div class="choseShapeDetailInner">

        <div id="shape_visualization" class="shape_visualization">
            <div class="headline">
                <!--p><span>Step 1</span>You have selected </p-->
                <p class="stepOneHeading"><span>Step 1</span>You have selected this shoe</p>
            </div>
            <div class="visualImgWrap"><img src=""></div>
            <div id="thumb_wrapper">
                <a id="prev" href="javascript:CustomShoe.stepOne.loadNextLast('prev');" class="control" title="Load previous last"></a>
                <div class="thumbs">
                    <ul class="thumbs_list">
                    </ul>
                </div>
                <a id="next" href="javascript:CustomShoe.stepOne.loadNextLast('next');" class="control" title="Load next last"></a>
            </div>
            <div id="vis_buttons">
                <span>Rotate view</span>
                <a class="rotate left" href="javascript:;"></a>
                <a class="rotate right" href="javascript:;"></a>
            </div>
        </div>
        <div class="choseShapeDetailRight">
            <!--<a class="return_head" href="javascript:;" onclick="shape_popup_close();"><span>RETURN TO SHOE MENU</span></a>-->
            <div id="shoe_details" class="shoe_details">
                <p class="item-title">The Ben</p>
                <p class="item-type">BLUCHER LACE-UP</p>
                <p class="item-subtitle"></p>
                <p class="item-desc">Slightly elongated profile with a square toe for a more European look. Full leather sole featuring hand-sewn goodyear welt construction.</p>                
                <div id="price_div">
                    <p style="visibility:hidden;">Handmade as shown</p>
                    <span class="symbol">$</span>
                    <span class="price">350</span>
                    <span class="shipping">w/ free shipping<br><big>&amp; 30 DAY RETURNS</big></span>
                </div>
            </div>
            <a id="continue_btn" onclick="CustomShoe.stepOne.nextStep();"/>Continue</a>

        </div>
    </div>
    <div class="popup_pattern_bar"><a class="shape_popup_close" href="javascript:;" onclick="CustomShoe.stepOne.closeStyleDetails();">&nbsp;</a></div>
</div>
<!--
<div class="controls">
    <a id="prev" class="control"></a>
    <a id="next" class="control" onClick="CustomShoe.stepOne.nextStep();"></a>
</div> -->
<input type="hidden" name="lastval" id="lastval">
<?php $this->load->view('common/template/jscontainer'); ?>
<script type="text/javascript">

                $(document).ready(function() {
                    CustomShoe.stepOne.init();
                    test();
                    //$('.nextLast').click(function(){
                    //    CustomShoe.stepOne.loadNextLast();
                    // });
                    $.each($('.thumbs_list').find('li'), function() {
                        alert($(this).html());
                        $(this).find('img').click(function() {
                            var styleId = $(this).find('input').val();
                            //alert(styleId);

                            CustomShoe.stepOne.getStyleDetails(styleId);
                        });
                    });
                });
                $('.visualImgWrap img').attr('ondragstart', "return false");
                $('.visualImgWrap img').attr('ondrop', "return false");
                function test() {
                    var $body = $('body');
                    var last_position = {},
                            $output = $('.visualImgWrap');
                    $output.on('mousedown', function(evt) {
                        $output.on('mouseup mousemove', function handler(evt) {
                            if (evt.type === 'mouseup') {
                            } else {
                                if (typeof(last_position.x) != 'undefined') {

                                    //get the change from last position to this position
                                    var deltaX = last_position.x - evt.clientX,
                                            deltaY = last_position.y - evt.clientY;

                                    //check which direction had the highest amplitude and then figure out direction by checking if the value is greater or less than zero
                                    if (Math.abs(deltaX) > Math.abs(deltaY) && deltaX > 0) {
                                        var elem = $('.visualImgWrap img').attr('src');
                                        var code = elem[elem.length - 5];
                                        var code = parseInt(code);
                                        var a = code;
                                        if (code === 7) {
                                            a = 0;
                                        } else {
                                            a++;
                                        }
                                        var src = elem.replace(code + ".png", a + ".png");
                                        $('.visualImgWrap img').attr('src', src);

                                    } else if (Math.abs(deltaX) > Math.abs(deltaY) && deltaX < 0) {
                                        var elem = $('.visualImgWrap img').attr('src');
                                        var code = elem[elem.length - 5];
                                        var code = parseInt(code);
                                        var a = code;
                                        if (code === 0) {
                                            a = 7;
                                        } else {
                                            a--;
                                        }
                                        var src = elem.replace(code + ".png", a + ".png");
                                        $('.visualImgWrap img').attr('src', src);

                                    } else if (Math.abs(deltaY) > Math.abs(deltaX) && deltaY > 0) {
                                        //up
                                    } else if (Math.abs(deltaY) > Math.abs(deltaX) && deltaY < 0) {
                                        //down
                                    }
                                }

                                //set the new last position to the current for next time

                            }
                            $output.off('mouseup mousemove', handler);
                        });
                        last_position = {
                            x: evt.clientX,
                            y: evt.clientY
                        };

                    });
                }
</script>