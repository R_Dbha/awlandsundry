<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h4>BILLING</h4>
<div class="form-overlay" style="height: 100%; top:0px;" ></div>
<div class="form-overlay" style="height: 55px; top:370px;"></div>
<div id="billingDetailsNonEditable" class="<?php echo $nonEditable; ?>" >
    <?php if (trim($this->input->post('payment_method')) == "Paypal") { ?>
        <div  style="font-size: 13px; padding: 5px 10px;">
            You will be taken to Paypal once you place the order
        </div>
    <?php } else { ?>
        <ul>		
            <li><?php echo set_value('billing_street_1', '') ?></li>	
            <li><?php echo set_value('billing_street_2', '') ?></li>	
            <li><?php
                echo set_value('billing_city', '');
                echo set_value('billing_city', '') != '' ? ', ' : ' ';
                echo set_value('card_state', '');
                echo set_value('card_state', '') != '' ? ', ' : ' ';
                echo set_value('billing_country', '');
                echo set_value('billing_country', '') != '' ? ', ' : ' ';
                echo set_value('billing_zipcode', '');
                ?>
            </li>
                <!--li><?php //echo set_value('billing_city', '') .', ' . set_value('card_state', '') .', ' . set_value('billing_zipcode', '');          ?></li-->
        </ul><?php } ?>
    <a href="javascript:;" class="button continue" onclick="editDetails('billing');">Edit</a>
</div>
<div id="billingDetailsEditable" class="<?php echo $editable; ?>">
    <input type="hidden" name="paymentid" id="paymentid" value="<?php echo $paymentId; ?>">
    <input type="hidden" name="expmonthid" id="expmonthid" value="<?php echo $expmonthid; ?>">
    <input type="hidden" name="expyearid" id="expyearid" value="<?php echo $expyearid; ?>">
    <input type="hidden" name="billingstateid" id="billingstateid" value="<?php echo $billingstateid; ?>">

    <select name="payment_method" class="payment_method  req" id="payment_method" tabindex="1">
        <option value="">Choose Payment Method</option>
        <option value="Credit card" <?php if (set_value('payment_method', '') == 'Credit card') echo "selected"; ?> >Credit card</option>
        <option value="Paypal" <?php if (set_value('payment_method', '') == 'Paypal') echo "selected"; ?> >Paypal</option>
    </select>
    <input type="text" id="txtCardOwner" name="card_owner" placeholder="Name on card" tabindex="2" value="<?php echo set_value('card_owner', ''); ?>" class="<?php echo $ispaypal; ?><?php echo isset($errors['card_owner']) ? ' invalid_box ' : ''; ?> ">
    <input type="text" id ="txtCardNumber" name="card_number" tabindex="3" placeholder="Credit Card number" class="card-number stripe-sensitive <?php echo $ispaypal; ?><?php echo isset($errors['card_number']) ? ' invalid_box ' : ''; ?>" id="card_number" maxlength="20" value="<?php echo set_value('card_number', ''); ?>">
    <span id="cardtype" >

    </span>
    <span style="display: inline-block;width: 231px;">
        <label>Exp date</label>
        <select name="exp_date_month" tabindex="4"  class="card-expiry-month exp_date_month <?php echo $ispaypal; ?>">
            <option value="">--</option>
            <?php echo $exp_date_month; ?>
        </select>
        <span class="blue">/</span>
        <select name="exp_date_year" tabindex="5"  class="card-expiry-year exp_date_year <?php echo $ispaypal; ?>">
            <option value="">--</option>
            <?php echo $exp_date_year; ?>
        </select>
        <label>CVV Code</label>
        <input type="text" id="card_cvv_code" name="card_cvv_code" tabindex="6" placeholder="---" class="card-cvc card_cvv_code numericOnly <?php echo $ispaypal; ?><?php echo isset($errors['card_cvv_code']) ? ' invalid_box ' : ''; ?>" maxlength="4" value="<?php echo set_value('card_cvv_code', ''); ?>">
    </span>
    <?php if ($billingDet != NULL) { ?> 
        <input type="text" name="billing_street_1" tabindex="7" placeholder="Street address 1" id="billing_street_1" class="alphaNumericOnly req<?php echo isset($errors['billing_street_1']) ? ' invalid_box ' : ''; ?>" value="<?php echo set_value('billing_street_1', $billingDet['address1']); ?>" >
        <input type="text" name="billing_street_2" tabindex="8" placeholder="Street address 2" id="billing_street_2" class="alphaNumericOnly" value="<?php echo set_value('billing_street_2', $billingDet['address2']); ?>" >
        <input type="text" name="billing_zipcode" tabindex="11" placeholder="Zipcode" class="zipcode req<?php echo isset($errors['billing_zipcode']) ? ' invalid_box ' : ''; ?>" id="billing_zipcode" maxlength="12"  value="<?php echo set_value('billing_zipcode', $billingDet['zipcode']); ?>" >
        <input type="text" name="billing_city" tabindex="9" placeholder="City" id="billing_city" value="<?php echo set_value('billing_city', $billingDet['city']); ?>" class=" req<?php echo isset($errors['billing_city']) ? ' invalid_box ' : ''; ?>" >
        <select name="billing_country" id="billing_country" tabindex="12" class="billing_country req <?php echo $this->form_validation->error_exists('card_state') ? 'invalid_box' : ''; ?>">
            <option value="">Country</option> 
            <?php echo $billing_country; ?>
        </select> 
        <select name="card_state" tabindex="10" id="card_state" class="card_state req">

            <?php echo $card_state; ?>
        </select>


    <?php } else { ?>
        <input type="text" name="billing_street_1" tabindex="7" placeholder="Street address 1" id="billing_street_1" class="alphaNumericOnly req<?php echo isset($errors['billing_street_1']) ? ' invalid_box ' : ''; ?>" value="<?php echo set_value('billing_street_1', ''); ?>" >
        <input type="text" name="billing_street_2" tabindex="8" placeholder="Street address 2" id="billing_street_2" class="alphaNumericOnly" value="<?php echo set_value('billing_street_2', ''); ?>" >
        <input type="text" name="billing_zipcode" tabindex="11" placeholder="Zipcode" class="zipcode req<?php echo isset($errors['billing_zipcode']) ? ' invalid_box ' : ''; ?>" id="billing_zipcode" maxlength="12"  value="<?php echo set_value('billing_zipcode', ''); ?>" />
        <input type="text" name="billing_city" tabindex="9" placeholder="City" id="billing_city" value="<?php echo set_value('billing_city', ''); ?>" class=" req<?php echo isset($errors['billing_city']) ? ' invalid_box ' : ''; ?>" >
        <select name="billing_country" id="billing_country" tabindex="12" class="billing_country req <?php echo $this->form_validation->error_exists('card_state') ? 'invalid_box' : ''; ?>" >
            <option value="">Country</option> 
            <?php echo $billing_country; ?>
        </select>
        <select name="card_state" tabindex="10" id="card_state" class="card_state req">

            <?php echo $card_state; ?>
        </select>


    <?php } ?>
    <div id="redirecttopaypal" style="font-size: 13px; padding: 5px 10px;">
        You will be taken to Paypal once you place the order
    </div>
    <?php if ($billingDet != NULL) { ?>
        <input type="hidden" name="selectedBillingStateId" id="selectedBillingStateId" value="<?php echo set_value('card_state', $billingDet['state']); ?>" >
    <?php } else { ?>

        <input type="hidden" name="selectedBillingStateId" id="selectedBillingStateId" value="<?php echo set_value('card_state', '') ?>" />
    <?php } ?>
    <div class="invalid_errors">
        <div class="invalid invalid_required">
            <?php
            if (isset($errors['billing'])) {
                echo $errors_list;
            }
            ?>
        </div>
    </div>
    <a href="javascript:;" class="button continue" tabindex="12" onclick="validateBillingDetails();">Continue</a>
</div>