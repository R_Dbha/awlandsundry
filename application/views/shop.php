<?php require_once( 'includes/header.php'); ?>
<div id="shop-wrapper">    
    <div class="shop-subtitle-container">
        <!--<div class="ins-title">INSPIRATION</div>-->
        <div class="left-sidebar-titlebar">
            <h2 class="shop-subtitle" style="text-transform: capitalize;">OWNING AN ORIGINAL <br>IS WELL WORTH THE WAIT</h2>            
        </div>
        <div class="right-sidebar-titlebar">
            <h2 class="shop-subtitle">Cheating is allowed.</h2>            
            <p class="hidden-sm">Here You Can Peek Freely At The Custom Work Of Others</p>
            <p style="display: none;" class="visible-sm">Here You Can Peek Freely </br>At The Custom Work Of Others</p>
        </div>
    </div>
    <div id="shop" class="container">
        <div id="shop-filter" class="filter hide-appointment">
            <div class="shop-filter-container">
                <div class="products-filter-heading">
                    FILTER <span class="products-filter-clear" type="button">clear all</span>
                </div>
                <ul class="filter-container">
                    <li class="filter-heading">Shoe Name</li>
                    <li class="filter-option">
                        <input type="text" class="shoe-name" style="padding: 7px 0px 7px 8px;"/>
                    </li>
                </ul>
                <ul class="filter-container">
                    <li class="filter-heading">Style</li>
                    <?php if (isset($styles) && sizeof($styles)) { ?>
                        <?php foreach ($styles as $key => $style) { ?>
                            <li class="filter-option" data-category="style"><span class="icon-filtering icon-work"><input type="checkbox" /></span>
                                <p><?php echo $style['style_name'] ?></p></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
                <ul class="filter-container">
                    <li class="filter-heading">Last</li>
                    <?php if (isset($lasts) && sizeof($lasts)) { ?>
                        <?php foreach ($lasts as $key => $last) { ?>
                            <li class="filter-option" data-category="last">
                                <span class="icon-filtering icon-work">
                                    <input type="checkbox" /></span>
                                <p><?php echo $last['last_name'] ?></p></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
                <ul class="filter-container">
                    <li class="filter-heading">Material</li>
                    <?php if (isset($materials) && sizeof($materials)) { ?>
                        <?php foreach ($materials as $key => $material) { ?>
                            <li class="filter-option" data-category="material">
                                <span class="icon-filtering icon-work">
                                    <input type="checkbox" />
                                </span>
                                <p><?php echo $material['material_name'] ?></p>
                                <ul class="filter-container">
                                    <?php if (isset($material['colors']) && sizeof($material['colors'])) { ?>
                                        <?php foreach ($material['colors'] as $k => $color) { ?>
                                            <li class="filter-option-inner" data-category="color">
                                                <span class="icon-filtering icon-work color">
                                                    <div style="background-color: #<?php echo $color['color_code'] ?>">
                                                        <img src="<?php echo ApplicationConfig::IMG_BASE . 'materials/' . $color['material_folder'] . '/' . $color['color_image'] ?>" />
                                                    </div>
                                                </span>
                                                <p><?php echo $color['color_name']; ?></p>
                                                <input type="hidden" value="<?php echo $color['material_color_id'] ?>" />
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
                <div class="desktop_filter_space">
                    <br/><br/><br/>
                </div>
            </div>
            <div class="product-filter-mobile-close">Close</div>
        </div>
        <div class="filter-ghost"></div>
        <div class="section-container product-container">
            <div class="spinner" id="loader" ></div>
            <ul id="product-list" class="product-list">
                <?php if (isset($collections) && sizeof($collections)) { ?>
                    <?php foreach ($collections as $key => $item) { ?>
                        <li> 
                            <div class="image-set">
                                <a href="<?php echo base_url() . 'products/' . $item['style_name'] . '/' . $item['name'] . '/' . $item['shoe_name']; ?>">
                                    <div class="" style=" z-index: 1;"><img src="<?php echo $item['image'][1]; ?>" /></div>
                                    <div class=""><img src="<?php echo $item['image'][0]; ?>" /></div>
                                </a>
                            </div>
                            <div class="shop-product-details">
                                <a href="<?php echo base_url() . 'products/' . $item['style_name'] . '/' . $item['name'] . '/' . $item['shoe_name']; ?>">
                                    <h4 class="product-title ins-product-title"><?php echo $item['shoe_name']; ?></h4>
                                    <h4 class="product-style ins-product-style"><?php echo $item['last_name'] . '/' . $item['style_name']; ?></h4>
                                    <h5 class="product-price ins-product-price">$<?php echo $item['price']; ?>
                                        <div class="hidden">
                                            <?php
                                            echo '<br/>quarter:' . $item['quarter_material'] . '-' . $item['quarter_color'];
                                            echo '<br/>toe:' . $item['toe_material'] . '-' . $item['toe_color'];
                                            echo '<br/>vamp:' . $item['vamp_material'] . '-' . $item['vamp_color'];
                                            echo '<br/>eyestay:' . $item['eyestay_material'] . '-' . $item['eyestay_color'];
                                            echo '<br/>foxing:' . $item['foxing_material'] . '-' . $item['foxing_color'];
                                            ?>
                                        </div>
                                    </h5>
                                    <!--<h6 class="ins-product-deliver product-deliver">DELIVERED IN 4-6 WEEKS</h6>-->
                                </a>
                            </div>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<?php require_once( 'includes/footer.php'); ?>