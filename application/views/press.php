<?php
require_once('includes/header.php');
?>
<div id="press-page-wrapper">
    <div id="press-page" class="container">
        <h3 class="page-title">
            <span>FEATURED PRESS</span>
        </h3>
        <div class="press-container">
            <div class="press-logos">
                <a href="http://plasticloafer.com/2014/10/15/awl-sundry-custom-luxury-shoes/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/plastic.jpg"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.mensfashionmagazine.com/brand-spotlight-awl-sundry" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/mensfashion.png"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.startupdaily.com.au/2014/09/awl-sundry-takes-aim-designer-labels-bespoke-shoes-men/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/startup.jpg"></a>
            </div>     
            <div class="press-logos">
                <a href="http://www.theshoemakerworld.com/blog/awl-sundry-part-1" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/shoemakerworld.jpg"></a>
            </div>  
            <!--div class="press-logos">
                <a href="http://www.supercompressor.com/gear/awl-sundry-custom-handmade-dress-shoes-you-ll-wear-for-life" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/super.jpg"></a>
            </div-->
            <div class="press-logos">
                <a href="http://yourstory.com/2014/08/awl-sundry-shoes/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/YourStory_Media-Logo.jpg"></a>
            </div>
            <div class="press-logos">
                <a href="http://tsbmen.com/51278/its-a-custom-world-made-to-order-monkstraps/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/tsb.jpg"></a>
            </div>
        </div>

        <div class="press-container">
            
            <div class="press-logos">
                <a href="http://evigo.com/15607-awl-sundry-evigo-customized-shoes-future-e-commerce/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/evigo.jpg"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.menswearstyle.co.uk/2014/08/18/interview-with-nikunj-marvania-of-awl-and-sundry/1254" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/menswearstyle.jpg"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.mensjournal.com/style/shoes/design-your-own-dress-shoes-online-from-heel-to-toe-20140730" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/mj.jpg"></a>
            </div>
            <div class="press-logos">
                <a href="http://network.details.com/post/awl-sundry-handmade-custom-footwear" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/network.jpg"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.wwd.com/footwear-news/retail/made-to-order-7839191" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/fn.jpg"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.urbandaddy.com/ntl/style/31680/Awl_Sundry_Meet_Your_Shoe_Guys_They_re_in_the_Internet_National_NTL_Clothing" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/urb.jpg"></a>
            </div> 
        </div>

        <div class="press-container">
            
            <div class="press-logos">
                <a href="http://www.businessinsider.com/best-websites-for-gentlemen-2014-7" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/bi.jpg"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.theshoesnobblog.com/2014/07/awl-sundry-customization-made-easy.html" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/snob.jpg"></a>
            </div> 
            <div class="press-logos">
                <a href="http://www.thefineyounggentleman.com/shoes/awl-sundry-custom-shoe-review/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/fyg.jpg"></a>
            </div>
             <div class="press-logos">
                <a href="http://www.aandhmag.com/make-it-your-own-awl-sundry/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/ah.jpg""></a>
            </div>
            
             <div class="press-logos">
                <a href="http://www.teachingmensfashion.com/product-review/awl-sundry-shoe-review-its-here " target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/tmf.jpg"></a>
            </div>
             <div class="press-logos">
                <a href="http://mensstylepro.com/2014/05/01/shoe-brands-to-watch-awl-sundry-online-custom-footwear/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/mens.jpg"></a>
            </div>
            
        </div>
        <div class="press-container">
           
          
            
            <div class="press-logos">
                <a href="http://mpd.me/launching-awl-sundry-the-future-of-mens-dress-shoes/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/mpd.png"></a>
            </div>
            <div class="press-logos">
                <a href="http://genteelflair.com/2014/04/07/your-shoes-your-way-awl-sundry-feature/" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/genteel.jpg"></a>
            </div> 
            <div class="press-logos">
                <a href="http://www.teachingmensfashion.com/product-review/review-on-the-customization-of-my-new-awl-sundry-shoes" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/tmf.jpg"></a>
            </div>
                       
            
            
        </div>
    </div>
</div>		
<?php
require_once('includes/footer.php');
?>