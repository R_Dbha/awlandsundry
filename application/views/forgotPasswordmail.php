<html>
    <head>
        <title>AwlandSundry Reset password validation</title>
    </head>
	<body style = "margin: 0px; font-family: gibsonregular, arial, sans-serif; color: #414141; margin: 0px ;">
            <div style = "background: url(<?php echo base_url() . 'assets/img/logo_inline.png' ?>) center 40px repeat-x; overflow: hidden;">
                <div style = "height: 135px; width: 760px; margin: 0 auto; overflow: hidden;">
                    <h1 style = "text-align: center; position: absolute; margin: 0; top: 15px; left: 40%; font-size: 2em;">
                        <a href="#" style = "text-indent: -9999px; background: url(<?php echo base_url() . 'assets/img/logo.png' ?>) center bottom no-repeat; width:255px; height: 97px; display: inline-block; position: relative; z-index: 9999; margin-left: 9px; 
                        margin-top: 1px; overflow: hidden;"></a>
                    </h1>
                </div>
            </div>
            <div style = "width: 100%; overflow: hidden;">
                <div style = "width: 760px; margin: 0 auto; padding-bottom: 15px;">
                    <p style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; color: #414141;">Hi <?php echo $emaildetails['fname'] ?> ,</p>
                    <div style = "margin: 5px 0; overflow: hidden;">
                        <p style = "font-size: 18px; margin: 15px 0;"></p>
                        <p style = "font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; margin: 15px 0;">To change your password please click on the button below:</p>
                        <p><a name="validate" id="validate" href="<?php echo $emaildetails['link'] ?>" style = "text-decoration:none;background: #00ADD3;clear:both;display:block;width:80px;
                    color: #FFF !important; text-transform: uppercase; border: 1px solid #00ADD3; padding: 7px 14px; border-radius: 5px;
                    -webkit-border-radius: 5px; outline: none; font-weight: bold; cursor: pointer; line-height: normal; font-family: inherit; font-size: 100%; margin: 0;">Click Here</a></p>
                    </div>
                    <p style = "font-size: 18px; margin: 10px 0;">Thank You.</p>
                <p style = "font-size: 18px; margin: 10px 0;">Awl &amp; Sundry team</p>
                </div>
            </div>
           <div id="footer_wrapper" style = "background: url(<?php echo base_url() . 'assets/img/foo_bg.png' ?>) center 0 repeat-x; width: 100%; overflow: hidden;">
                <div  id="footer" style = "background: url(<?php echo base_url() . 'assets/img/foo_bg.png' ?>) center 0 repeat-x; height: auto; min-height: 100px; 
                padding: 30px 0; width: 760px; margin: 0 auto; overflow: hidden;">
                    <div id="left" style = "float: left; overflow: hidden;">
                        <p style = "font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; font-size: 16px; margin: 15px 0;">CUSTOMER SERVICE</p>
                        <P style = "font-size: 16px; color:#414141;  margin: 15px 0;"><?php echo $this->config->item('contact_email'); ?></P>
                        <p style = "font-size: 16px; margin: 15px 0;"><?php echo $this->config->item('customer_care_number'); ?></p>
                    </div>
                    <div id="right" style = "float: right; overflow: hidden;">
                        <p style = "font-weight: bold; font-family: 'gibsonsemibold', Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; font-size: 16px;">CONNECT WITH US</p>
                        <p style = "font-size: 16px; margin: 0; float:right;">
                            <a href="https://www.facebook.com/AwlandSundry" class="facebook" target="_blank" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() .'assets/img/social.png' ?>) 0px 0px no-repeat">Facebook</a>
                            <a href="https://twitter.com/AwlandSundry" class="twitter" target="_blank" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() .'assets/img/social.png' ?>) -46px 0px no-repeat">Twitter</a>
                            <a href="http://www.pinterest.com/awlandsundry/" class="pinterest" target="_blank" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() .'assets/img/social.png' ?>) -92px 0px no-repeat">Pinterest</a>
                            <a href="http://instagram.com/awlandsundry"target="_blank" title="Instagram" class="instagram" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() .'assets/img/social.png' ?>) -138px 0 no-repeat">Instagram</a>
                        <a href="http://awl-and-sundry.tumblr.com/"target="_blank" title="Tumblr" class="tumblr" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() .'assets/img/social.png' ?>) -230px 0 no-repeat">Tumblr</a>
                        <a href="https://plus.google.com/111459378470264472967" target="_blank" rel="publisher" title="google plus" class="google-plus" style="width: 33px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() .'assets/img/social.png' ?>) -276px 0 no-repeat">Google+</a>
                        </p>
                    </div>
                    <div id="copyright" style = "font-size: 16px;text-align: center; clear: both; overflow: hidden;">&copy; Awl & Sundry. All Rights Reserved. 2012 - 2014</div>
                </div>
            </div>
	</body>
</html>
