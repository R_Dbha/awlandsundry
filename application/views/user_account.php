<?php
	require_once('includes/header.php');
?>
<div id="account-wrapper">
	<div id="account" class="container">
		<h3 class="page-title">
			<span>MY ACCOUNT</span>
		</h3>
		<div id="account-menu" class="left ">
			<ul class="slicknav">
				<li><a href="#">ACCOUNT DASHBOARD</a></li>
				<li><a href="#" class="active">ACCOUNT INFORMATION</a></li>
				<li><a href="#">ADDRESS BOOK</a></li>
				<li><a href="#">ORDER HISTORY</a></li>
				<li><a href="#">STORE CREDITS</a></li>
				<li><a href="#">GIFT CERTIFICATE</a></li>
			</ul>
		</div>
		<div class="account_details right">
			<h2>NIKIN RAWAT <span class="acc"><a href="#">LOG OUT</a></span></h2>
			<div class="account-section-title"><span>PROFILE</span><span class="acc"<a href="#">EDIT</a></span></div>
			<div class="account-inner">
				<ul>
					<li><label>FIRST NAME:</label>Nikin</li>
					<li><label>LAST NAME:</label>Rawat</li>
					<li><label>EMAIL ADDRESS:</label>test@mail.com</li>
					<li><label>PASSWORD:</label><a href="#">Reset Password</a></li>
				</ul>
			</div>
			<div class="account-section-title"><span>ADDRESS</span><span class="acc"><a href="#">ADD NEW</a></span></div>
			<div class="account-inner">
				<ul>
					<li>no saved address</li>
				</ul>
			</div>	
		</div>
	</div>
</div>
<?php
	require_once('includes/footer.php');
?>