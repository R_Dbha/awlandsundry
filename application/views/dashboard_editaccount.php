<div class="edit_account">
    <div class="dashboard-content-heading-wrapper">
        <h1 style="color:black;">EDIT ACCOUNT INFORMATION</h1>
    </div>
    <div class="contact-content">
        <form method="POST" name="edit_account" onsubmit="return update_userinfo();">
            <table style="font-size: 12px;">
                <tr>
                    <td width="35%">FIRST NAME </td>
                    <td><input style="height:25px;" type="text" name="fname" id="fname" value="<?php echo @$user['first_name']; ?>" required="required"/></td>
                    <!--td><span id="fname_Err" class="hide">First Name Required</span></td-->
                </tr>
                <tr>
                    <td width="35%">LAST NAME </td>
                    <td><input style="height:25px;" type="text" name="lname" id="lname" value="<?php echo @$user['last_name']; ?>" required/></td>
                    <!--td><span id="lname_Err" class="hide">Last Name Required</span></td-->
                </tr>
                <tr>
                    <td width="35%">EMAIL ADDRESS </td>
                    <td><input style="height:25px;border:1px solid #CCC" type="email" name="email" id="email" value="<?php echo @$user['email']; ?>" required/></td>

                </tr>
            </table><P></p>
            <input type='hidden' name="hid_from" id="hid_from" value="<?php echo $from; ?>" />
            <input style="margin-left: 118px;" type="submit" value="SAVE" />
        </form>
        <!--a href="#account_info" >BACK</a-->

    </div>
</div>