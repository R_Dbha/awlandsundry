<?php
$this->load->view('includes/header');
?>
<div id="get_inspired_2_wrapper">
    <!--div id="get_inspired_2_banner_wrapper" class="container kam">
        <div id="get_inspired_2_banner">
            <h2><?php echo $style['style_name']; ?></h2>
            <p><?php echo $style['style_description']; ?></p>
        </div>
    </div>
    <div id="menu" class="container get_inspired">
        <ul id="menuitem">
            <?php
            if (is_array($types)) {
                foreach ($types as $key => $value) {
                    ?>
                    <li ><a href="#"><?php echo $value['name']; ?></a></li>
                    <li>|</li>
                    <?php
                }
            }
            ?>
        </ul>
    </div-->
    <?php
    if (is_array($collections)) {
        $i = 0;
        foreach ($collections as $key => $collection) {
           $i++;
            ?>
            <div class="container get_inspired_slider_container">
                <h3 class="product-title small">
                    <span><?php echo $key; ?></span>
                </h3>
                <div class="get_inspired_slider_content">
                    <ul class="slider_classic" id="slider<?php echo $i; ?>" >
                        <?php
                        if (is_array($collection)) {
                            foreach ($collection as $item) {
                                ?>
                                <li>
                                    <div class="slide_img">
                                        <a href="<?php echo base_url() . 'products/' . $item['style_name'] . '/' . $item['name'] . '/' . $item['shoe_name']; ?>">
                                            <img src="<?php echo @$item['image'][0]; ?>" alt="<?php echo @$item['alt']; ?>" />
                                            <p><?php echo @$item['shoe_name']; ?></p>
                                            <h5 class="product-price">$<?php echo @$item['price']; ?></h5>
                                        </a>
                                        
                                    </div>
                                </li>        	
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>	
<?php
$this->load->view('includes/footer');
?>
                            