<?php $this->load->view('includes/header'); ?>
<div id="faq-wrapper">
    <div id="faq-contact" class="container">
        <h3 class="page-title">
            <span>CONTACT US</span>
        </h3>
        <div id="contact-description">
            <!-- <p class="center">Get in touch with us</p> -->
        </div>
        <div id="contact-wrapper">
            <div class="contact-info address">
                <h4>OFFICE ADDRESS</h4>
                <!-- <p>18 W 23rd Street, 4th Floor,</p>
                <p>New York, NY 10010 </p> -->
                <p>530 5th Ave,</p>
                <p>New York, NY 10036</p>
            </div>
            <div class="contact-info">
                <h4>PHONE</h4>
                <p><?php echo $this->config->item('contact_phone'); ?></p>
            </div>
            <div class="contact-info">
                <h4>EMAIL</h4>
                <p><a href='mailto:<?php echo $this->config->item('contact_email'); ?>'><?php echo $this->config->item('contact_email'); ?></a></p>
            </div>
            <div style="text-align:right;margin: 90px 11% 0 0;font-size: 11px;font-weight: bold;">
                <span>* By Appointment only</span>
            </div>
        </div>
    </div>
    <div id="faq-menu-wrapper" class="hide-appointment">
        <div id="menu" class="container faq">
            <ul id="menuitem">
                <!--                <li><a href="#exchange">EXCHANGE</a></li>
                                <li>|</li>-->
                <li><a href="#product">PRODUCT</a></li>
                <li>|</li>
                <li><a href="#shipping">SHIPPING</a></li>
                <li>|</li>
                <li><a href="#sizing">SIZING</a></li>
            </ul>
        </div>
    </div>
    <div id="faq" class="center">
        <!--        <div id="exchange" class="container">
                    <h3 class="bgline section-title">
                        <div class="line"></div>
                        <span>EXCHANGE</span>
                    </h3>
                    <h4><span class="question">What is the exchange policy &#63;</span></h4>
                    <p class="none answer">You can exchange your Awl & Sundry shoes within 30 days of receipt if they don't fit right or if they are faulty. Your shoes are considered faulty if they are different from the design you initially ordered. This does not apply to colors and leather texture since your actual pair of shoes may look slightly different from the colors you see on the screen.<br><br>When exchanging, you will be responsible for one way return postage to our NYC office. Please ensure the shoes are in an unworn condition. We recommend trying them on carpeted surface.<br><br>Please email <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a> to exchange your shoe.</p>
                </div>-->

        <div id="product" class="container">
            <h3 class="bgline section-title">
                <div class="line"></div>
                <span>PRODUCT</span>
            </h3>

            <h4><span class="question">How are your shoes made ?</span></h4>
            <p class="none answer">All our shoes are handmade by highly skilled craftsmen. We can use two types of constructions for our shoes. The Blake Stitch, which creates a beautifully flexible shoe, and sleek outsole appearance. This is highly popular in places like Italy, and France. Next, is the Sacchetto construction, which is unique a specific region in Tuscany. One of the most comfortable and flexible types of construction that requires intensive manual labor and can be resoled at any point. Due to the nature of Sacchetto shoes, bigger changes to the fit are only applicable on the Blake Stitch.</p>

            <h4><span class="question">Where is the leather sourced from ?</span></h4>
            <p class="none answer">All our leather is sourced carefully from traditional tanneries in Tuscany. Quality is our mantra and that's why we opt for the highest quality that maintains the shoes for many years to come. Furthermore our leather is vegetable tanned and therefore uses only natural ingredients in the tanning process. Not only is this less harmful for the environment but also for the people that handle the leather. You can view our tannery right <a href="http://www.conceria800.it/Azienda.htm" target="_blank" style="text-decoration: underline; color: blue;">here</a>. More info on vegetable tanned leather <a href="https://en.wikipedia.org/wiki/Leather" target="_blank" style="text-decoration: underline; color: blue;">here</a>.</p>

            <h4><span class="question">What is the difference between Oxford and Derby ?</span></h4>
            <p class="none answer">In short, the Oxford uses a closed lacing construction that fits more snugly over the instep, but provides a more formal appearance. People with narrow feet, and a lower instep tend to like the Oxford. While the Derby features an open lacing construction that provides more width through the top of the shoe. The Derby works beautifully for both formal and casual attires depending on the colors you use. Those with wide feet or high insteps will feel more comfortable with the extra room the derby provides. While it is possible for almost any person to wear both shoes, we strongly recommend going for a derby if you have a higher instep and wider feet.</p>

            <h4><span class="question">Where are your shoes made ?</span></h4>
            <p class="none answer">Italy is known in the fashion world as one of the greats, which is why we use craftsmen from Italy to handcraft our shoes. We pride ourselves with creating a high quality product, and would settle for nothing less. Who doesn't love a product that is handmade in Tuscany?</p>

            <h4><span class="question">How does Awl & Sundry compare to their competitors ?</span></h4>
            <p class="none answer">While those makers are very good at creating ready to wear shoes, their factory process leaves much to be desired. This is why customers who have shopped with these companies eventually move to a higher-end product that gives them more control over the experience. This is where we come in. Awl & Sundry is nestled in this beautiful middle ground between those shoemakers, and luxury companies like John Lobb or Berluti. We allow our customers more control over their experience, use better materials, and construction than factory-made shoes at a price that is far below many of our competitors.</p>

            <h4><span class="question">What are the different kind of outsoles you offer ?</span></h4>
            <p class="none answer">We offer a leather outsole that is made of the sturdy hide located on the back portion of the animal. This creates a perfect traditional formal dress shoe. We also offer a rubber outsole. This outsole would be perfect for any fall/winter shoe to provide more traction on slick surfaces. Lastly, we offer a half rubber sole, which adds rubber only on the walking portion of the outsole. This would make a good year round shoe!</p>

            <h4><span class="question">Do you offer bespoke shoes ?</span></h4>
            <p class="none answer">Absolutely! We will try our best to create or recreate any shoe design our customers desire. We also provide measuring instructions on our website for all people who live outside of the New York area, so all of our customers can receive a perfectly fitting shoe. Also, we do offer in house consultations by appointment only, so you can meet the team, and buy mesmerizing shoes in the process. Just send an email to <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a>, and we will gladly assist you in any way we can. For more information see Bespoke Process below.</p>

            <h4><span class="question">Do you offer boots ?</span></h4>
            <p class="none answer">Boots are awesome, and Awl & Sundry is awesome, so of course we offer boots! Just send images of a design you'd like to <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a>, and we will determine whether it is possible to make them. We promise to do our very best! Prices can vary depending on the type of boot including materials.</p>

            <h4><span class="question">What's included in my order ?</span></h4>
            <p class="none answer">Every custom order comes with a shoe horn, soft protective shoe bags, and obviously your custom shoes. If you would like wooden shoe trees included they will be $30, and are recommended for proper shoe care.</p>

            <h4><span class="question">Do you offer vegan shoes ?</span></h4>
            <p class="none answer">Currently, we do not offer this service. However, we understand how important the environment is, and are researching how to offer this option to our customers in the near future!</p>

            <h4><span class="question">Do you offer resoling ?</span></h4>
            <p class="none answer">We do not offer a resoling service as of now. We recommend to take your shoes to a trained cobbler who can resole your shoes for you. Do not try to do the process yourself.</p>

            <h4><span class="question">Bespoke Process</span></h4>
            <p class="none answer">
                <span style="display: block; width: 50%; text-align: left !important; margin-left: 30%; margin-bottom: 10px; margin-top:10px;">
                    What in the world is bespoke? <br>
                    Bespoke means making something for you from scratch. <br>
                    <b>The following things are included in the bespoke process:</b><br>
                    &nbsp;&nbsp;&nbsp;&nbsp; - Your personal last if your feet are too narrow or too wide <br>
                    &nbsp;&nbsp;&nbsp;&nbsp; - A new pattern that we do not offer on the website. For instance if you would like us to recreate one of your existing shoes and can't find the pattern in the custom process <br>
                    &nbsp;&nbsp;&nbsp;&nbsp; - Certain types of boots that require us to create a new pattern (design) <br>
                    &nbsp;&nbsp;&nbsp;&nbsp; - Orthopedic insoles<br>
                    Pricing: $450 one-time investment. <br>
                    Orthopedic insoles may require a higher investment and need to be examined by our team in NYC.
                </span>
            </p>

            <h4><span class="question">Refunds & Exchanges</span></h4>
            <p class="none answer">Due to the nature of custom-made products, we are not able to offer refunds. If your shoes don’t fit, they are faulty or we made a mistake with your order (for example wrong color, sole type or style choice), we will remake the shoes as fast as possible. Please keep in mind that all our shoes are handmade and small blemishes can happen. We offer a 30-day exchange policy from the day your order has been delivered.</p>
        </div>

        <div id="shipping" class="container">
            <h3 class="bgline section-title">
                <div class="line"></div>
                <span>SHIPPING</span>
            </h3>
            <h4><span class="question">How do you ship the shoes ?</span></h4>
            <p class="none answer">Our shoes will ship DHL Express, which is a quick, and secure process. This takes about 3-5 business days, and we will email you the tracking information once they ship. Also, a signature is required upon delivery, so make sure someone is present to sign.</p>

            <h4><span class="question">Do you offer express delivery option ?</span></h4>
            <p class="none answer">The nature of our handmade process is time consuming, but it is necessary for creating a high quality product. Our craftsmen can only measure, cut, and stitch so fast. So given this nature, we are unable to offer express delivery options at this time, so ensure that you give yourself at least 4-6 weeks before delivery. <b>In some cases we are able to deliver within 3 weeks but this needs to be confirmed BEFORE placing the order (once production begins we are not able to refund or make changes).</b></p>

            <h4><span class="question">How long will my shoes to get delivered ?</span></h4>
            <p class="none answer">Your shoes will take 4-6 weeks from purchase to be delivered to your door.</p>

            <h4><span class="question">How do I track my order ?</span></h4>
            <p class="none answer">Once your shoes ship, we will supply you with all the tracking information necessary.</p>

            <h4><span class="question">How much does shipping cost ?</span></h4>
            <p class="none answer">Shipping is complimentary for all US + EU orders. There will be a $35 charge for all international orders.</p>

            <h4><span class="question">Do I need to be home to sign for my shoes ?</span></h4>
            <p class="none answer">Yes, there must be somebody present to sign for your shoes upon delivery or they will be shipped back. We recommend shipping to an office address if you are unsure if somebody will be able to sign at your dwelling. <b>Please keep in mind that we are not responsible for lost packages once the shipment is on the way to you.</b></p>
        </div>

        <div id="sizing" class="container">
            <h3 class="bgline section-title">
                <div class="line"></div>
                <span>SIZING</span>
            </h3>
            <h4><span class="question">What sizes do you offer ?</span></h4>
            <p class="none answer">We offer U.S. sizes 5 to 14 including half sizes. Outside of these sizes, please email <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a>. Just select your normal shoe size or the size you wear most often once you reach checkout. If you're really stuck and don't know which to choose, please email us at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a> and let us know what sizes you usually wear and how you find them to fit. Then we'll get in touch to suggest the most appropriate size for the style you've designed.</p>

            <h4><span class="question">How do I know which size to select or that I'll get the size right ?</span></h4>
            <p class="none answer">After lots of experimentation, we've found that making your shoes in your normal size, or the size you wear most often, is the best way for us to get sizing right for you. Different shoe brands use different sizing systems, but we've had a great success rate with this method. If you're not sure which size to select during checkout, let us know in the comment section and we'll get in touch to confirm the best size to try. We can't promise to get sizing right the first time, but we'll certainly do our best and so far we've been right the vast majority of the time. If your shoes don't fit we'll remake them at no cost to you.</p>

            <h4><span class="question">Do you offer shoes for wide feet ?</span></h4>
            <p class="none answer">Yes, our custom measuring process allows for a variety of different widths! During the design process on our <a href="<?php echo base_url('create-a-custom-shoe'); ?>" target="_blank" style="text-decoration: underline; color: blue;">custom page</a> we will show you how to measure your own feet. <b>If you have very wide feet, please email us first because we may have to create a new last for you that comes at an additional cost.</b> Depending on your measurements we will determine if we have to create a last just for you! You can also email our customer care team at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a> for additional details.</p>

            <h4><span class="question">Do you offer shoes for narrow feet ?</span></h4>
            <p class="none answer">Absolutely! However, the process requires us to create your own personal last (see bespoke last). Please follow the instructions for measuring during the design process on our <a href="<?php echo base_url('create-a-custom-shoe'); ?>" target="_blank" style="text-decoration: underline; color: blue;">custom page</a>. You will then email us the measurements at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a> so we can determine if your personal last is necessary.</p>

            <h4><span class="question">Can I select two different sizes for the same pair ?</span></h4>
            <p class="none answer">Yes, we at Awl & Sundry understand the complexities of the human foot, and allow you to input your unique measurements independently. We do this to ensure you receive the best fit for your new custom shoes, so you can walk in comfort & style!</p>

            <h4><span class="question">Will you keep my sizing on file for future orders ?</span></h4>
            <p class="none answer">The measurement process can be slightly time consuming, and that is why we save our customers unique measurements for future reference. We urge that you create an account during the buying process, so your measurements are saved. This way, your next purchase will move much more quickly. The only time new measurements might be needed is if your foot changes significantly. Like after an operation or an accident.</p>

            <h4><span class="question">How do I know my size ?</span></h4>
            <p class="none answer">All Awl & Sundry shoes are true-to-fit, To get the right fit, please order the most frequently worn size. For instance, if you wear a 10 for most of your dress shoes, we would recommend ordering a 10 with Awl & Sundry as well. If you are unsure of your size, please follow the measuring process in the <a href="<?php echo base_url('create-a-custom-shoe'); ?>" target="_blank" style="text-decoration: underline; color: blue;">design process</a> and send the measurements to <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a>.</p>

            <h4><span class="question">Do you offer custom fit ?</span></h4>
            <p class="none answer">Yes, we do! During our custom process we will ask for 6 different measurements. These measurements tell us what type of foot you have, and allows us to make a shoe to those exact measurements. Then, after you receive your custom shoes, you can bask in the glory of finely furnished footwear! To view the custom page, and see what we have to offer <a href="<?php echo base_url('create-a-custom-shoe'); ?>" target="_blank" style="text-decoration: underline; color: blue;">Click here!</a></p>

            <h4><span class="question">Can you accommodate custom orthotics or insoles into your custom shoes ?</span></h4>
            <p class="none answer">We handle this on a case-by-case basis to ensure that we can assist you properly. Email our care team at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a> to start a discussion about your unique footwear requirements.</p>

            <h4><span class="question">What if the shoes don't fit ?</span></h4>
            <p class="none answer">We understand the unique aspects behind shoe sizing, and the "break in" period of high-end shoes. However, if the shoes are complete off we offer a risk free 30 day exchange policy. Contact our care team at <a href="mailto:<?php echo $this->config->item('contact_email'); ?>" style="text-decoration: underline; color: blue;"><?php echo $this->config->item('contact_email'); ?></a>, and we will facilitate an exchange or provide you with all the information you need about your new shoes, so they break in properly. <b>Please keep in mind that we do not offer refunds, only exchanges within the 30 day frame!</b></p>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer.php'); ?>