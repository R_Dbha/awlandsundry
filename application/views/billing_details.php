<div class="checkout-overlay <?php echo @$billing_overlay; ?>" ></div>
<div class="form <?php echo @$billing_form; ?>">
    <h3 class="page-title"><span>BILLING</span></h3>
    <div id="payment-method">
        <span class='error'><?php echo @$errors['billing_details']['payment_method']; ?></span>
        <div>
            <label>PAYMENT METHOD</label>
            <select class="payment-method" id="payment_method" name="billing_details[payment_method]" >
                <option value="">Choose</option>
                <?php echo @$billing_details['payment_method']; ?>
            </select>
        </div>
    </div>
    <div class="method-paypal <?php echo @$method_paypal; ?>">
        You will be taken to Paypal once you place the order
    </div>
    <div class="method-credit-card <?php echo @$method_credit_card; ?>">
        <span class='error'><?php echo @$errors['billing_details']['card']; ?></span>
        <div id="card" >
            <p class="cardname"><input type="text" name="card-name" class="card-name" id="card-name" placeholder="NAME ON CARD" maxlength="100" /></p>
            <p class="cardnmbr" ><input type="text" name="card-number" class="card-number" id="card-number" placeholder="CREDIT CARD NUMBER" /></p>
            <p class="carddetail">
                <span class="left">
                    <label>Exp date</label>
                    <select name="card-expiry-month" class="card-expiry-month" id="card-expiry-month">
                        <option value="1" >1</option>
                        <option value="2" >2</option>
                        <option value="3" >3</option>
                        <option value="4" >4</option>
                        <option value="5" >5</option>
                        <option value="6" >6</option>
                        <option value="7" >7</option>
                        <option value="8" >8</option>
                        <option value="9" >9</option>
                        <option value="10" >10</option>
                        <option value="11" >11</option>
                        <option value="12" >12</option>
                    </select>
                </span>
                <span class="left">
                    <label>/</label>
                    <select name="card-expiry-year" class="card-expiry-year" id="card-expiry-year" >
                        <option value="2015" >2015</option>
                        <option value="2016" >2016</option>
                        <option value="2017" >2017</option>
                        <option value="2018" >2018</option>
                        <option value="2019" >2019</option>
                        <option value="2020" >2020</option>
                        <option value="2021" >2021</option>
                        <option value="2022" >2022</option>
                        <option value="2023" >2023</option>
                        <option value="2024" >2024</option>
                        <option value="2025" >2025</option>
                        <option value="2026" >2026</option>
                        <option value="2027" >2027</option>
                        <option value="2028" >2028</option>
                        <option value="2029" >2029</option>
                        <option value="2030" >2030</option>
                        <option value="2031" >2031</option>
                        <option value="2032" >2032</option>
                        <option value="2033" >2033</option>
                        <option value="2034" >2034</option>
                        <option value="2035" >2035</option>
                        <option value="2036" >2036</option>
                        <option value="2037" >2037</option>
                        <option value="2038" >2038</option>
                        <option value="2039" >2039</option>
                        <option value="2040" >2040</option>
                    </select>
                </span>
                <span><input type="text" name="card-cvc" class="card-cvc" id="card-cvc" placeholder="CVV"  maxlength="4" /></span>
            </p>
        </div>
        <table>
            <tr>
                <td>FIRST NAME</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['first_name']; ?></span>
                    <input class="first_name" type="text" name="billing_details[first_name]" value="<?php echo @$billing_details['first_name']; ?>" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>LAST NAME</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['last_name']; ?></span>
                    <input class="last_name" type="text" name="billing_details[last_name]" value="<?php echo @$billing_details['last_name']; ?>" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>ADDRESS LINE 1</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['address1']; ?></span>
                    <input class="address1" type="text" name="billing_details[address1]" value="<?php echo @$billing_details['address1']; ?>" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>ADDRESS LINE 2</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['address2']; ?></span>
                    <input class="address2" type="text" name="billing_details[address2]" value="<?php echo @$billing_details['address2']; ?>" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>CITY</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['city']; ?></span>
                    <input class="city" type="text" name="billing_details[city]" value="<?php echo @$billing_details['city']; ?>" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>COUNTRY</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['country']; ?></span>
                    <select class="country" name="billing_details[country]"  id="billing_country">
                        <option>select</option>
                        <?php echo @$billing_country; ?>
                    </select>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>STATE</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['state']; ?></span>
                    <?php if (isset($billing_state_isText) && $billing_state_isText < 1) { ?>
                        <select class="state" name="billing_details[state]" id="billing_state">
                            <?php echo @$billing_state; ?>
                        </select>
                    <?php } else { ?>
                        <input id="billing_state" class="state" type="text" name="billing_details[state]" value="<?php echo @$billing_details['state']; ?>" />
                    <?php } ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>ZIP CODE</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['zipcode']; ?></span>
                    <input class="zipcode" type="text" name="billing_details[zipcode]" value="<?php echo @$billing_details['zipcode']; ?>" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>PHONE</td>
                <td>
                    <span class='error'><?php echo @$errors['billing_details']['phone']; ?></span>
                    <input class="phone" type="text" name="billing_details[phone]" value="<?php echo @$billing_details['phone']; ?>" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="form_submit">
        <button id="billing_next">NEXT</button>
        <input type="hidden" id="billing_error" value="<?php echo @$billing_error; ?>">
        <input type="hidden" id="billing_total" value="<?php echo @$billing_total; ?>">
    </div>
</div>