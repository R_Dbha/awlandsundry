<?php require_once( 'includes/header.php'); ?>
<style type="text/css" media="screen">
    #supersized {
        display: block;
    }
</style>
<div id="slider-wrapper">
    <div id="slider" class="container">
        <div id="banner_button_wrap">
            <a href="<?php echo base_url(); ?>products/derby/casual/Mario" class="flat_button">Shop Now</a> 
<!--            <a href="http://bit.ly/awlandsundry" class="flat_button">Preview</a> -->
            <a href="" class="flat_button">Send an e-gift card</a>
        </div>

<!--        <a id="prevslide" class="load-item"></a>
        <a id="nextslide" class="load-item"></a>-->
        <div id="slidecaption"></div>
        <ul id="slide-list"></ul>
    </div>
    <div id="menu" class="container homepage">
        <ul id="menuitem">
            <li style="display:none;">
                <a href="#slider-wrapper"></a>
            </li>
            <li><a href="#about-us">ABOUT US</a><span> | </span>
            </li>
            <li><a href="#featured">FEATURED</a><span> | </span>
            </li>
            <li><a href="#workshop">WORKSHOP</a><span> | </span>
            </li>
            <li><a href="#testimonials">TESTIMONIALS</a><span> | </span>
            </li>
            <li><a href="#connect">CONNECT</a>
            </li>
        </ul>
    </div>
</div>
<div id="about-us" class="container home-row">
    <h3 class="section-title">
        <span>CUSTOM HANDCRAFTED SHOES</span>

    </h3>
    <p>At Awl &amp; Sundry, we are determined to bridge the growing divide between price and premium quality in the men's shoe industry. Our driving mission is to democratize bespoke luxury by ensuring that custom crafted shoes are within reach for the modern man.</p>
</div>
<div id="featured" class="container home-row">
    <h3 class="section-title center">
        <span>FEATURED</span>
    </h3>
    <div id="featured-slider">
        <ul class="featured-slider">
            <li>
                <a href="<?php echo base_url(); ?>products/oxford/casual/Frank" >
                    <img src="<?php echo base_url(); ?>assets/css/images/featured/oxford_1.jpg">
                    <h4 class="product-title">FRANK</h4>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>products/oxford/classics/Preston" >
                    <img src="<?php echo base_url(); ?>assets/css/images/featured/oxford_2.jpg">
                    <h4 class="product-title">PRESTON</h4>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>products/derby/classics/Watson">
                    <img src="<?php echo base_url(); ?>assets/css/images/featured/derby_1.jpg">
                    <h4 class="product-title">WATSON</h4>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>products/derby/casual/Christopher">
                    <img src="https://www.awlandsundry.com/files/custom/Derby/Casual/4/2.jpg">
                    <h4 class="product-title">CHRISTOPHER</h4>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>products/monk/casual/Lawrence">
                    <img src="<?php echo base_url(); ?>assets/css/images/featured/monk_1.jpg">
                    <h4 class="product-title">LAWRENCE</h4>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>products/monk/street/Oliver">
                    <img src="<?php echo base_url(); ?>assets/css/images/featured/monk_2.jpg">
                    <h4 class="product-title">OLIVER</h4>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>products/loafer/casual/Greg">
                    <img src="<?php echo base_url(); ?>assets/css/images/featured/loafer_1.jpg">
                    <h4 class="product-title">GREG</h4>
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>products/loafer/classics/Columbus">
                    <img src="<?php echo base_url(); ?>assets/css/images/featured/loafer_2.jpg">
                    <h4 class="product-title">COLUMBUS</h4>
                </a>
            </li>
        </ul>
    </div>
</div>
<div id="workshop-wrapper">
    <video id="process" src="<?php echo base_url(); ?>assets/video/A&S_aspect ratio_02.mp4"></video>
    <div id="workshop" class="container home-row">
        <h3 class="section-title center">
            <span>WORKSHOP</span>
        </h3>
        <div class="workshop">
            <span class="play" title="Watch Our Artisanal Shoemaking Process"></span>
            WATCH OUR ARTISANAL SHOEMAKING PROCESS
        </div>
    </div>
</div>
<div id="testimonials-wrapper">
    <div id="testimonials" class="container home-row">
        <h3 class="section-title center">
            <span>TESTIMONIALS</span>
        </h3>
        <div class="testimonial-container">
            <div class="testimonial left half">

                <div class="desc">
                    <p>No one else offers this level of customization in goodyear welted shoes at this price point - no one.</p>
                    <h4>David Leibensperger</h4>
                    <h5>Attorney</h5>
                    <h5>Baltimore, MD</h5>
                </div>
            </div>
            <div class="testimonial left half">

                <div class="desc">
                    <p>Awl and Sundry literally restored my faith in the fact that the art of shoemaking still existed.</p>
                    <h4>Sam Zane</h4>
                    <h5>Director of Channel Marketing</h5>
                    <h5>Orange, CA</h5>
                </div>
            </div>
            <div class="testimonial left half">

                <div class="desc">
                    <p>From the beautiful box with individual cloth bags and wooden forms for each shoe, to the impeccably made shoes themselves, everything was absolutely top notch.</p>
                    <h4>Rick Starbuck</h4>
                    <h5>Digital Banking Design Leader</h5>
                    <h5>San Francisco, California</h5>

                </div>
            </div>
            <div class="testimonial left half">

                <div class="desc">
                    <p>I created a design that is exactly what I was looking for. I get so many compliments. I never aspired to be a shoe designer, but this was so easy.</p>
                    <h4>Laird Elhert</h4>
                    <h5>Photographer</h5>
                    <h5>Jersey City, NJ</h5>

                </div>
            </div>            
        </div>
    </div>
</div>
<div id="connect-wrapper">
    <div id="connect" class="container home-row">
        <h3 class="section-title center">
            <span>CONNECT</span>
        </h3>
        <h4 class="text">JOIN THE CONVERSATION</h4>
        <div class="social">
            <a href="https://www.facebook.com/AwlandSundry" target="_blank" class="facebook"></a>
            <a href="https://twitter.com/AwlandSundry" target="_blank" class="twitter"></a>
            <a href="http://www.pinterest.com/awlandsundry/" target="_blank" class="pinterest"></a>
            <a href="http://instagram.com/awlandsundry" target="_blank" class="instagram"></a>
            <a href="http://awlandsundry.tumblr.com/" target="_blank" class="tumblr"></a>
            <a href="https://plus.google.com/111459378470264472967" target="_blank" class="googleplus"></a>
        </div>
        <div class="newsletter">
            <span class="text">NEWSLETTER SIGNUP</span>
            <div id="nl_wr">
                <input type="text" id="newsletter_email" placeholder="ENTER YOUR EMAIL ADDRESS">    
                <p class="none show keepmail_msg">YOU ARE SIGNED UP ! THANK YOU !</p>
            </div>
            <input type="button" value="SUBMIT" id="newsletter_footer-submit" onclick="javascript:return keepEmail('footer');">
            <!--p class="none show">ERROR</p-->
        </div>
    </div>
</div>
<div id="press-wrapper">
    <div id="press" class="container home-row">
        <div class="press-container">
            <div class="press-logos">
                <a href="http://www.businessinsider.com/best-websites-for-gentlemen-2014-7?IR=T" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/bi.jpg" target="_blank"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.wwd.com/footwear-news/retail/made-to-order-7839191" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/fn.jpg" target="_blank"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.mensjournal.com/style/shoes/design-your-own-dress-shoes-online-from-heel-to-toe-20140730" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/mj.jpg" target="_blank"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.theshoesnobblog.com/2014/07/awl-sundry-customization-made-easy.html" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/snob.jpg" target="_blank"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.urbandaddy.com/ntl/style/31680/Awl_Sundry_Meet_Your_Shoe_Guys_They_re_in_the_Internet_National_NTL_Clothing" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/urb.jpg" target="_blank"></a>
            </div>
            <div class="press-logos">
                <a href="http://www.mensfashionmagazine.com/brand-spotlight-awl-sundry" target="_blank"><img src="<?php echo base_url(); ?>assets/css/images/mensfashion.png" target="_blank"></a>
            </div>
        </div>
    </div>
</div>
<div class="overlay">
    <div id="index-popup-wrapper" class="">
        <div id="index-popup">
            <span class="close"></span>
            <div class="newletter-content">
                <h1>For the shoe aficionado </h1>
            </div>
            <div class="newletter-content">
                <p> Sign up now to get premium access to new collections, product launches, events and the latest in men's style. </p>
            </div>
            <div class="newletter-content">
                <form class="hide" method="POST">
                    <p class="">
                        <input type="email" id="newsletter-email" name="newsletter-email" placeholder="ENTER YOUR EMAIL ADDRESS" />
                        <input type="button" value="SUBMIT" id="newsletter-submit" onclick="javascript:return keepEmail('popup');" required>
                    </p>
                </form>
                <p class="none show">YOU ARE SIGNED UP ! THANK YOU !</p>
            </div>
        </div>
    </div>
</div>

<div id="spinner_overlay">
    <h1 id="message">For optimal experience, Please use the landscape mode.</h1>
    <div class="spinner"></div>
</div>
<div id="feedback-tab"></div>
<div class="modal-dialog-sop-theme" id="feedback-form" tabindex="0" role="dialog">
    <div class="modal-dialog-sop-theme-title modal-dialog-sop-theme-title-draggable">
        <span class="modal-dialog-sop-theme-title-text">Feedback</span>
        <span class="modal-dialog-sop-theme-title-close"></span>
    </div>
    <div class="modal-dialog-sop-theme-content">
        <div>
            <form id="feedback-form-popup" onsubmit="return send_feedback();">
                <p>How likely are you to recommend Awl &amp; Sundry to a friend?</p>
                <table class="feedback-score-table">
                    <tbody>
                        <tr>
                            <th rowspan="2">Not at all
                                <br>likely</th>
                            <td>
                                <input type="radio" name="score" value="0">
                            </td>
                            <td>
                                <input type="radio" name="score" value="1">
                            </td>
                            <td>
                                <input type="radio" name="score" value="2">
                            </td>
                            <td>
                                <input type="radio" name="score" value="3">
                            </td>
                            <td>
                                <input type="radio" name="score" value="4">
                            </td>
                            <td>
                                <input type="radio" name="score" value="5">
                            </td>
                            <td>
                                <input type="radio" name="score" value="6">
                            </td>
                            <td>
                                <input type="radio" name="score" value="7">
                            </td>
                            <td>
                                <input type="radio" name="score" value="8">
                            </td>
                            <td>
                                <input type="radio" name="score" value="9">
                            </td>
                            <td>
                                <input type="radio" name="score" value="10">
                            </td>
                            <th rowspan="2">Extremely
                                <br>likely</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>0</td>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                            <td>5</td>
                            <td>6</td>
                            <td>7</td>
                            <td>8</td>
                            <td>9</td>
                            <td>10</td>
                        </tr>
                    </tbody>
                </table>
                <p class="feedback-instructions">How could we improve the design process so that it's easier for you to find and create a shoe you love?</p>
                <textarea name="comments" id="feedback-comments"></textarea>
                <br>
                <table class="feedback-email-table">
                    <tbody>
                        <tr>
                            <td nowrap="">Your email (optional)</td>
                            <td>
                                <input type="email" name="email" id="feedback-email">
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="modal-dialog-sop-theme-buttons">
                    <button name="cancel" class="cancel">No thanks</button>
                    <input type="submit" name="submit" class="send-feedback goog-buttonset-default" value="Send feedback" />
                </div>
            </form>
            <div id="feedback-thankyou" class=" feedback-share-popup">
                <p>
                    Thanks for sharing your feedback with us - we really appreciate it! Now spread the good word and share with your friends by clicking one of the icons below.
                </p>
                <div class="feedback-share-icons">
                    <a href="https://www.facebook.com/AwlandSundry" target="_blank">
                        <div class="feedback-icons feedback-facebook"></div>
                    </a>
                    <a href="https://twitter.com/AwlandSundry" target="_blank">
                        <div class="feedback-icons feedback-twitter"></div>
                    </a>
                    <a href="http://instagram.com/awlandsundry" target="_blank">
                        <div class="feedback-icons feedback-instagram"></div>
                    </a>
                    <a href="http://www.pinterest.com/awlandsundry/" target="_blank">
                        <div class="feedback-icons feedback-pinterest"></div>
                    </a>
                    <br>
                </div>
                <p>
                <div class="modal-dialog-sop-theme-buttons">

                    <button name="submit" class="finish goog-buttonset-default">Finished</button>
                </div>
                </p>
            </div>
        </div>
    </div>

</div>
<?php require_once( 'includes/footer.php'); ?>
