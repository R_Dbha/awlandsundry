<html>
    <head>
        <title>Order Confirm</title>
    </head>
    <body style = "margin: 0px; font-family: gibsonregular, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; color: #414141; margin: 0px ;">
        <div id="header_wrapper" style = "background: url(<?php echo base_url() . 'assets/img/logo_inline.png' ?>) center 40px repeat-x; overflow: hidden;">
            <div id="header" style = "height: 140px; width: 760px; margin: 0 auto; overflow: hidden;">
                <h1 id = "logo" style = "text-align: center; position: absolute; margin: 0; top: 15px; left: 40%; font-size: 2em;">
                    <a href="<?php echo base_url(); ?>" style = "text-indent: -9999px; background: url(<?php echo base_url() . 'assets/img/logo.png' ?>) center bottom no-repeat; width:255px; height: 97px; display: inline-block; position: relative; z-index: 9999; margin-left: 9px; 
                       margin-top: 1px; overflow: hidden;"></a>
                </h1>
            </div>
        </div>
        <div id="banner_wrapper" style = "background: url(<?php echo base_url() . 'assets/img/main_slider_border_top.png' ?>) 8px 0 repeat-x,url(<?php echo base_url() . 'assets/img/gallery/2.jpg' ?>) 0 0 no-repeat; border-bottom: 10px solid #00ADD3; width: 100%; padding: 5px;">
            <div id="banner" style = "height: 350px; width: 760px; margin: 0 auto;">
                <h1 style = "text-align: center; color: #FFF; font-size: 55px; width: 580px; display: block; margin: 175px auto 0 auto;">THANK YOU FOR YOUR ORDER.</h1>	
            </div>
        </div>	
        <div id="content_wrapper" styl = "width: 100%; overflow: hidden; overflow: hidden;">
            <div id="content" style = "width: 760px; margin: 0 auto;">
                <p style = "font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; font-size: 21px; color: #414141;">Dear  <?php $user = $this->session->userdata('user_details');
echo $user['first_name']; ?>,  </p>
                <div id="messsge_wrap" style = "margin: 45px 0; overflow: hidden;">
                    <p style = "font-size: 20px; margin: 15px 0;">We are currently proccessing your order.</p>
                    <p style = "font-size: 20px; margin: 15px 0; text-align:justify;">Your made-to-order shoes will be delivered to your doorsteps within 4 weeks from today. We will
                        give you a shout out with the tracking number as soon as your order is out the door. Please call us at
                        <?php echo $this->config->item('customer_care_number'); ?> if you have any questions.</p>
                    <br/>
                    <p style = "font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; font-size: 21px; margin: 15px 0;">YOUR ORDER SUMMERY</p>
                    <p style = "font-size: 20px; margin: 15px 0;"><span style = "font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; font-size: 21px;">Order Date:</span><?php echo date("m/d/Y h:ia", strtotime($order_summary[0]['order_date'])) ?></p>
                    <p style = "font-size: 20px; margin: 15px 0;"><span style = "font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; font-size: 21px;">Order Number:</span><?php echo $order_summary[0]['order_id'] ?></p>
                    <br/>
                    <div id="address_table" style ="overflow: hidden;">
                        <table style = "width: 100%;" border-collapse: collapse; border-spacing: 0;>
                               <tbody style = "display: table-row-group; vertical-align: middle; border-color: inherit;">	
                                <tr style = "display: table-row; vertical-align: inherit; border-color: inherit;">
                                    <th style = "font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; 
                                        font-size: 21px; text-align: left; padding: 5px 0;">Shipping to:</td>
                                    <th style = "font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; 
                                        font-size: 21px; text-align: left; padding: 5px 0;">Billing to:</td>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;"><?php echo $billing[0]['first_name']; ?></td>
                                    <td style = "font-size: 20px;"><?php echo $shipping[0]['firstname'] ?></td>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;"><?php echo $billing[0]['address1'] ?></td>
                                    <td style = "font-size: 20px;"><?php echo $shipping[0]['address1'] ?></td>
                                </tr>
                                <tr>
                                    <td style = "font-size: 20px;"><?php echo $billing[0]['address2'] . ',' . $billing[0]['city'] . '-' . $billing[0]['zipcode']; ?></td>
                                    <td style = "font-size: 20px;"><?php echo $shipping[0]['address2'] . ',' . $shipping[0]['city'] . '-' . $shipping[0]['zipcode']; ?></td>
                                </tr>
                            </tbody>	
                        </table>
                    </div>
                    <div id="order_table" style = "overflow: hidden;">
                        <table id="order_list"  style = " border-collapse: collapse; border-spacing: 0; display: table; border-color: gray; width: 100%; margin-top: 30px; font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif;">
                            <thead style = "display: table-header-group; vertical-align: middle; border-color: inherit;">
                                <tr>
                                    <th style="width:65%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
                                        text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">DESCRIPTION</th>
                                    <th style="width:27%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
                                        text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">QUANTITY</th>
                                    <th style="width:18%; border: 1px solid #000; padding: 10px 30px; font-size: 20px; 
                                        text-transform: uppercase; border-collapse: collapse; border-spacing: 0;">PRICE</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $totalamt = 0;
                                $disc = 0;
                                foreach ($order['items'] as $item) {
                                    $totalamt += $item['item_amt'];
                                    $discountPerc = $item['discount_perc'];
                                    if ($discountPerc !== 0) {
                                        $disc = ($totalamt * $discountPerc) / 100;
                                        $totalamt = $totalamt - $disc;
                                    }
                                    ?>

                                    <tr>
                                        <td style = "border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">
    <?php echo $item['item_name']; ?>
                                            <img style="width: 100%;" src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $item['image_file'] . "_A0.png"; ?>" />
                                        </td>
                                        <td style="text-align:center; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">1 PAIR</td>
                                        <td style="text-align:center; border: 1px solid #000; padding: 10px 30px; font-size: 20px; text-transform: uppercase; font-weight: normal;">$<?php echo $item['item_amt'] ?></td>
                                    </tr>
<?php } ?>
                                <tr style="border:1px solid #000; height: 40px;">

                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; text-transform: uppercase;">SUBTOTAL:</td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; font-weight: normal;">$<?php echo $order_summary[0]['gross_amt']; ?> </td>
                                </tr>
                                <tr>	
                                    <td></td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; text-transform: uppercase;">SHIPPING COST:</td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; font-weight: normal;">$0.00</td>
                                </tr>
                                <tr>	
                                    <td></td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; text-transform: uppercase;">PROMOTION CODE:</td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; font-weight: normal;"><?php echo $disc ?></td>
                                </tr>
                                <tr>	
                                    <td></td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; text-transform: uppercase;">GRAND TOTAL:</td><td style = "text-align: right; padding: 10px 30px 0px 0px; font-size: 20px; font-weight: normal;"><?php echo $totalamt; ?></td>
                                </tr>
                            </tfoot>	
                        </table>
                    </div>
                </div>
            </div>	
        </div>	
        <div id="footer_wrapper" style = "background: url(<?php echo base_url() . 'assets/img/foo_bg.png' ?>) center 0 repeat-x; width: 100%; overflow: hidden;">
            <div  id="footer" style = "background: url(<?php echo base_url() . 'assets/img/foo_bg.png' ?>) center 0 repeat-x; height: auto; min-height: 100px; 
                  padding: 30px 0; width: 760px; margin: 0 auto; overflow: hidden;">
                <div id="left" style = "float: left; overflow: hidden;">
                    <p style = "font-weight: bold; font-family: gibsonsemibold, Helvetica Neue,Helvetica,Helvetica,Arial,sans-serif; font-size: 21px; margin: 15px 0;">CUSTOMER SERVICE</p>
                    <P style = "font-size: 20px; margin: 15px 0;"><?php echo $this->config->item('contact_email'); ?></P>
                    <p style = "font-size: 20px; margin: 15px 0;"><?php echo $this->config->item('customer_care_number'); ?></p>
                </div>
                <div id="right" style = "float: right; overflow: hidden;">
                    <p style = "font-weight: bold; font-family: gibsonsemibold, aHelvetica Neue,Helvetica,Helvetica,Arial,sans-serif; font-size: 21px;">CONNECT WITH US</p>
                    <p style = "font-size: 20px; margin: 0; float:right;">
                        <a href="https://www.facebook.com/AwlandSundry" class="facebook" target="_blank" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() . 'assets/img/social.png' ?>) 0px 0px no-repeat">Facebook</a>
                        <a href="https://twitter.com/AwlandSundry" class="twitter" target="_blank" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() . 'assets/img/social.png' ?>) -46px 0px no-repeat">Twitter</a>
                        <a href="http://www.pinterest.com/awlandsundry/" class="pinterest" target="_blank" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() . 'assets/img/social.png' ?>) -92px 0px no-repeat">Pinterest</a>
                        <a href="http://instagram.com/awlandsundry"target="_blank" title="Instagram" class="instagram" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() . 'assets/img/social.png' ?>) -138px 0 no-repeat">Instagram</a>
                        <a href="http://awl-and-sundry.tumblr.com/"target="_blank" title="Tumblr" class="tumblr" style="width: 32px; height: 32px; display: block; float: left; margin-right: 8px; text-indent: -99999px; color: rgba(0, 0, 0, 0); background: url(<?php echo base_url() .'assets/img/social.png' ?>) -230px 0 no-repeat">Tumblr</a>
                    </p>
                </div>
                <div id="copyright" style = "text-align: center; clear: both; overflow: hidden;">&copy; Awl & Sundry. All Rights Reserved. 2012 - 2014</div>
            </div>
        </div>
    </body>
</html>