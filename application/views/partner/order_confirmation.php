<div class="col-md-12">
	<div class="box box-solid">
		<div class="box-body">
			<h2>ORDER CONFIRMATION</h2>
			<h3>Thank you, your shoe order has been forwarded to our custom shop</h3>
			<p>Your order reference number is <?php echo @$order_details['invoice_no']; ?></p>
		</div>
	</div>

	<div class="box box-solid">
		<div class="box-body">
			<dl class="dl-horizontal">
<?php

foreach($items as $key => $item){
	
	?>	
		<dt>ITEM <?php echo $key+1; ?> :</dt>
				<dd>$ <?php echo number_format(round($item['item_amt'], 2), 2); ?>
					</dd>
	<?php
}

?>	
<dt>ORDER TOTAL :</dt>
				<dd>$ <?php echo number_format(round($order_details['order_total']), 2); ?>
					</dd>
				<dt>AMOUNT PAID :</dt>
				<dd>$ <?php echo number_format(round($order_details['amount_paid']), 2); ?>
					</dd>
				<dt>AMOUNT PENDING :</dt>
				<dd>$ <?php echo number_format(round($order_details['pending']), 2); ?>
					</dd>
			</dl>
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-md-12">
			<button type="button" onclick="window.location = window.location;"
				class="btn btn-primary pull-right">
				<i class="fa fa-envelope"></i> Close
			</button>
		</div>
	</div>
</div>