<form method="post" id="order-form">
<span class="error"></span>
	<div class="row">
		<div class="col-md-4">Customer Name:</div>
		<div class="col-md-8">
			<input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="CUSTOMER NAME"/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">Customer Email:</div>
		<div class="col-md-8">
			<input type="text" class="form-control" id="customer_email" name="customer_email" placeholder="CUSTOMER EMAIL" />
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">Cell Number:</div>
		<div class="col-md-8">
			<input type="text" class="form-control" id="cell_number" name="cell_number" placeholder="CELL NUMBER"/>
		</div>
	</div>
	<div class="seperation"></div>
	<div class="row">
		<div class="col-md-4">Amount Paying:</div>
		<div class="col-md-8">
			<input type="text" class="form-control" id="amount_paying" name="amount_paying" placeholder="AMOUNT PAYING" />
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">Card Name:</div>
		<div class="col-md-8">
			<input type="text" name="card-name" class="form-control card-name"
				id="card-name" placeholder="NAME ON CARD" maxlength="100" />
		</div>
	</div>
	<div class="seperation"></div>
	<div class="row">
		<div class="col-md-4">Card Number:</div>
		<div class="col-md-8">
			<input type="text" name="card-number"
				class="form-control card-number" id="card-number"
				placeholder="CREDIT CARD NUMBER" />
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">Expiry Date:</div>

		<div class="col-md-8">
			

				
					<select name="card-expiry-month" style="width:50%; float:left;"
						class="form-control card-expiry-month" id="card-expiry-month">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
						<option value="8">8</option>
						<option value="9">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
					</select>
				
					<select name="card-expiry-year" style="width:50%; "
						class="form-control card-expiry-year" id="card-expiry-year">
						<option value="2015">2015</option>
						<option value="2016">2016</option>
						<option value="2017">2017</option>
						<option value="2018">2018</option>
						<option value="2019">2019</option>
						<option value="2020">2020</option>
						<option value="2021">2021</option>
						<option value="2022">2022</option>
						<option value="2023">2023</option>
						<option value="2024">2024</option>
						<option value="2025">2025</option>
						<option value="2026">2026</option>
						<option value="2027">2027</option>
						<option value="2028">2028</option>
						<option value="2029">2029</option>
						<option value="2030">2030</option>
						<option value="2031">2031</option>
						<option value="2032">2032</option>
						<option value="2033">2033</option>
						<option value="2034">2034</option>
						<option value="2035">2035</option>
						<option value="2036">2036</option>
						<option value="2037">2037</option>
						<option value="2038">2038</option>
						<option value="2039">2039</option>
						<option value="2040">2040</option>
					</select>
				
		
		</div>


	</div>
	<div class="seperation"></div>
	<div class="row">
		<div class="col-md-4">CVV Code:</div>
		<div class="col-md-8">
			<input type="text" name="card-cvc" class="form-control card-cvc"
				id="card-cvc" placeholder="CVV" maxlength="4" />
		</div>
	</div>
	<div class="seperation"></div>
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-8">
			<input class="btn btn-success" id="confirm-submit" name="review_submit" type="submit" value="Confirm Order" />
		</div>
	</div>


</form>