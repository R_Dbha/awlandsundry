<div class="col-md-12">
<?php
if(is_array($items)){
	foreach($items as $key => $item){
		$shoe = $item['shoe'];
		$shoedesign = $item['shoedesign'];
		?>

	<div class="row cart-item">
		<div class="col-md-12">
			<div class=" row ">
				<div class="col-md-6 shoe">
					<img onerror="this.style.display = 'none';"
						src="<?php echo $base_url . $shoe['base']; ?>" /> <img
						onerror="this.style.display = 'none';"
						src="<?php echo $base_url . $shoe['toe']; ?>" class="float-top" />
					<img onerror="this.style.display = 'none';"
						src="<?php echo $base_url . $shoe['eyestay']; ?>"
						class="float-top" /> <img onerror="this.style.display = 'none';"
						src="<?php echo $base_url . $shoe['vamp']; ?>" class="float-top" />
					<img onerror="this.style.display = 'none';"
						src="<?php echo $base_url . $shoe['foxing']; ?>" class="float-top" />
					<img onerror="this.style.display = 'none';"
						src="<?php echo $base_url . $shoe['stitch']; ?>" class="float-top" />
					<img onerror="this.style.display = 'none';"
						src="<?php echo $base_url . $shoe['lace']; ?>" class="float-top" />
				</div>
				<div class="col-md-6 pricing">

					<div class="box-tools pull-right">

						<input type="hidden" class="cart-id"
							value="<?php echo $item['row_id']; ?>" />
						<button title="Remove Item"
							class="btn btn-danger btn-sm remove-cart-item"
							data-widget="remove">
							<i class="fa fa-times"></i>
						</button>
					</div>
					<div class="clearfix"></div>
					<div class="seperation"></div>
					<dl class="dl-horizontal">
						<dt>Quantity :</dt>
						<dd>
							<input type="text" value="<?php echo $item['quantity']; ?>"
								class="form-control quantity" />
						</dd>
						<div class="seperation"></div>
						<dt>Price :</dt>
						<dd>
							<input type="text" value="<?php echo $item['price']; ?>"
								class="form-control price" disabled="disabled" />
						</dd>
						<div class="seperation"></div>

						<dt>Subtotal :</dt>
						<dd>
							<input type="text" value="<?php echo $item['subtotal']; ?>"
								class="form-control subtotal" disabled="disabled" />
						</dd>
					</dl>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row ">
				<div class="col-md-6 details">
					<dl class="dl-horizontal">
						<dt>Last/Style :</dt>
						<dd><?php echo $shoedesign['style_name'] . "/ (" . $shoedesign['manufacturer_code'] . ")"; ?>
					</dd>
								
					<?php if (!$shoedesign['t_is_nothing']) { ?>
					<dt>Toe :</dt>
						<dd><?php
			echo $shoedesign['toe_type'];
			if($shoedesign['t_is_material']){
				echo ', ' . $shoedesign['toe_material'] . ', ' . $shoedesign['toe_color'];
			}
			?>
					</dd>
					<?php } ?>
					<?php if ($shoedesign['is_vamp_enabled']) { ?>
                    <?php if (!$shoedesign['v_is_nothing']) { ?>                                       
                    <dt>Vamp :</dt>
						<dd><?php echo $shoedesign['vamp_type']; ?>
                    <?php
				if($shoedesign['v_is_material']){
					echo ', ' . $shoedesign['vamp_material'] . ', ' . $shoedesign['vamp_color'];
				}
				?></dd>
				    <?php } } ?> 
                    </dd>
						<dt>Quarter :</dt>
						<dd><?php echo $shoedesign['quarter_material']; ?> , <?php echo $shoedesign['quarter_color']; ?></dd>
					<?php if (!$shoedesign['e_is_nothing']) { ?>
                    <dt>Eyestays :</dt>
						<dd><?php echo $shoedesign['eyestay_type']; ?>
                    <?php if ($shoedesign['e_is_material']) { ?>,
                    <?php echo $shoedesign['eyestay_material']; ?>,
                    <?php echo $shoedesign['eyestay_color']; } ?></dd>
                    <?php } ?>
                    <?php  if (!$shoedesign['f_is_nothing']) { ?>
                    <dt>Foxing :</dt>
						<dd>
                    <?php echo $shoedesign['foxing_type']; ?>
                    <?php if ($shoedesign['f_is_material']) { ?>,
                    <?php echo $shoedesign['foxing_material']; ?>,
                    <?php echo $shoedesign['foxing_color']; } ?>
                     </dd>
                     <?php } ?>
				<dt>Stitching :</dt>
						<dd><?php echo $shoedesign['stitch_name']; ?>
                	<span class="color" style="background: <?php echo $shoedesign['stitch_color']; ?>"></span>
						</dd>
				<?php if ($shoedesign['is_lace_enabled']) { ?>
                <dt>Laces :</dt>
						<dd><?php echo $shoedesign['lace_name']; ?>
                <span class="color" style="background: <?php echo $shoedesign['color_code']; ?>"></span>
						</dd> <?php } ?>
	
	</dl>
				</div>
				<div class="col-md-3 sizing ">
					Sizing Left
					<dl class="dl-horizontal">
						<dt>Size :</dt>
						<dd><?php echo $shoe['size']['left']['value'] .' - '.$shoe['size']['left']['width'] ; ?>
					</dd>
						<dt>Length :</dt>
						<dd><?php echo $shoe['measurement']['left']['size']; ?>cm
					</dd>
						<dt>Width :</dt>
						<dd><?php echo $shoe['measurement']['left']['width']; ?>cm
					</dd>
						<dt>Height :</dt>
						<dd><?php echo $shoe['measurement']['left']['girth']; ?>cm
					</dd>
						<dt>Instep :</dt>
						<dd><?php echo $shoe['measurement']['left']['instep']; ?>cm
					</dd>

					</dl>
				</div>
				<div class="col-md-3 sizing">
					Sizing Right
					<dl class="dl-horizontal">
						<dt>Size :</dt>
						<dd><?php echo $shoe['size']['right']['value'] .' - '.$shoe['size']['right']['width'] ; ?>
					</dd>
						<dt>Length :</dt>
						<dd><?php echo $shoe['measurement']['right']['size']; ?>cm
					</dd>
						<dt>Width :</dt>
						<dd><?php echo $shoe['measurement']['right']['width']; ?>cm
					</dd>
						<dt>Height :</dt>
						<dd><?php echo $shoe['measurement']['right']['girth']; ?>cm
					</dd>
						<dt>Instep :</dt>
						<dd><?php echo $shoe['measurement']['right']['instep']; ?>cm
					</dd>

					</dl>
				</div>

			</div>
			<hr />
		</div>

	</div>
	
<?php
	}
}
?>
</div>