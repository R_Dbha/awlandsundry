<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $this->config->item('app_title'); ?></title>
<link
	href="<?php echo base_url(); ?>assets/css/bootstrap/bootstrap-3.2.0.min.css"
	rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="<?php echo base_url(); ?>assets/css/AdminLTE.css"
	rel="stylesheet" type="text/css" />
</head>
<body class="skin-blue fixed">
	<header class="header">
		<a href="<?php echo base_url(); ?>admin" class="logo"
			style="width: 100%;"> <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Custom Shoe Portal
            </a>
	</header>
	<div>
		<aside>
			<section id="content" class="content" style="margin-top: 200px;">
				<div class="row">
					<div class="col-xs-4"></div>
					<div class="col-xs-4">
						<div class="box box-primary">
							<div class="box-header">

								<div class="col-xs-8">
									<h3 class="box-title">Login</h3>
								</div>

							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive">

								<form name="userData" method="post"
									action="<?php echo base_url(); ?>partners/login">
									<div class="row">
										<div class="col-xs-4"></div>
										<div class="col-xs-8">
											<span class="error"><?php echo @$error['invalid_login']; ?></span>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="row">
												<div class="col-xs-1"></div>
												<div class="col-xs-3">Email</div>
												<div class="col-xs-7">
													<input type="text" class="required form-control"
														name="email" value="<?php echo @$form_data['email']; ?>" />
													<span class="error"><?php echo @$error['email']; ?></span>
												</div>

											</div>
											<br />
											<div class="row">
												<div class="col-xs-1"></div>
												<div class="col-xs-3">Password</div>
												<div class="col-xs-7">
													<input type="password" name="password" class="form-control"
														value="<?php echo @$form_data['password']; ?>" /> <span
														class="error"><?php echo @$error['password']; ?></span>
												</div>

											</div>
											<br />
											<div class="row">
												<div class="col-xs-1"></div>
												<div class="col-xs-3"></div>
												<div class="col-xs-7">
													<input type="submit" class="btn btn-primary" Value="Login" />
													<input type="reset" class="btn btn-warning" Value="Reset" />
												</div>
											</div>
											<br />
										</div>
									</div>
								</form>
							</div>
							<!--tablecontent-->
						</div>
						<!--subcontent-->
					</div>
				</div>
			</section>

</body>
</html>