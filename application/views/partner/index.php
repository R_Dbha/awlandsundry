<?php
$this->load->view('includes/partner_header');
?>

<div class="modal fade order-modal partner-modal" id="order-create"
	tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-x-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">×</button>
				<h4 class="modal-title">
					<i class="fa  fa-folder-open-o"></i> Create New Order
				</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer clearfix">

				<!-- button type="button" class="btn btn-danger" data-dismiss="modal">
						<i class="fa fa-times"></i> OK
					</button-->

				<button type="button" data-dismiss="modal"
					class="btn btn-primary pull-right">
					<i class="fa fa-envelope"></i> Close
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade order-modal" id="order-details" tabindex="-1"
	role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">×</button>
				<h4 class="modal-title">
					<i class="fa  fa-folder-open-o"></i> Order Details
				</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer clearfix">

				<!-- button type="button" class="btn btn-danger" data-dismiss="modal">
						<i class="fa fa-times"></i> OK
					</button-->

				<button type="button" data-dismiss="modal"
					class="btn btn-primary pull-right">
					<i class="fa fa-envelope"></i> Close
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<section class="content-header">
	<h1>
		Orders <small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Orders</li>
	</ol>
</section>

<!-- Main content -->
<section id="content" class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box collapsed-box">

				<div class="box-header" data-toggle="tooltip" title=""
					data-original-title="Filters">
					<h3 class="box-title">Filters</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-primary btn-xs" data-widget="collapse">
							<i class="fa fa-plus"></i>
						</button>
					</div>
				</div>
				<div class="box-body" style="display: none;"></div>
			</div>
			<!--subcontent-->
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="overlay"></div>
				<div class="loading-img"></div>
				<div class="box-header" data-toggle="tooltip" title=""
					data-original-title="Orders">
					<h3 class="box-title">Orders</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-primary btn-xs" id="create-order">
							<i class="fa fa-pencil"> Create New Order</i>
						</button>
					</div>
				</div>
				<div class="box-body">

					<table id="orders"
						class="table table-striped table-bordered table-hover">
						<thead>
							<!--thead-->
							<tr>
								<th>Order No</th>

								<th>Customer Name</th>
								<th>No.of Items</th>
								<th>No.of Shoes</th>
								<th>Shoe Cost</th>
								<th>Order Date</th>
								<th>Order Status</th>
							</tr>
						
						
						<thead>
						
						
						<tbody>

						<?php
						if(isset($orders) && sizeof($orders)){
							foreach($orders as $key => $order){
								
								?>
								
							<tr
								class="<?php echo strtolower(str_replace(' ','-', $order['status'])); ?>">
								<td>
								
								<?php
								echo $order['order_id'];
								$this->load->view('admin/order_quick_view', $order);
								?>
								<input type="hidden" value="<?php echo $order['order_id'];?>"
									class="order_id">
								</td>

								<td><?php echo $order['first_name'];?></td>
								<td><?php echo $order['number_of_items'];?></td>
								<td><?php echo $order['item_count'];?></td>
								<td><?php echo $order['total_amt'];?></td>
								<td><?php echo date('d/m/Y H:ia',strtotime($order['order_date']));?></td>
								<td><?php echo $order['status'];?></td>
							</tr>
								
								<?php
							}
						}
						?>
                        
                        </tbody>
					</table>

				</div>
			</div>
			<!--subcontent-->
		</div>
	</div>
</section>

<script type="text/javascript">
    
</script>


<?php
$this->load->view('includes/admin_footer');
?>
