<?php
?>
<div class="box">
	<div class="overlay"></div>
	<div class="loading-img"></div>
	<div class="nav-tabs-custom">
		<!-- Tabs within a box -->
		<ul class="nav nav-tabs  ui-sortable-handle partner-nav-tabs">
			<li class="active" id="add-item-btn"><a href="#add-item"
				data-toggle="tab">Add Item</a></li>
			<li class=""><a href="#order-review" data-toggle="tab">Order Review </a></li>
		</ul>
		<div class="tab-content ">
			<!-- Morris chart - Sales -->
			<div class="chart tab-pane active" id="add-item">
				<div class="col-md-8 no-padding">
					<div class="box box-solid">
						<div class="overlay"></div>
						<div class="loading-img"></div>
						<div class="box-body">
							<div class="row create-shoe">
								<div class="col-md-1 ">Style</div>
								<div class="col-md-2 no-padding ">
									<select class="form-control change-style">
									<?php echo $style; ?>
									</select>
								</div>
								<div class="col-md-1">Last</div>
								<div class="col-md-2 no-padding ">
									<select class="form-control change-last" disabled="disabled">
									<?php echo $last; ?>
									</select>
								</div>
								<div class="col-md-3">Design Number</div>
								<div class="col-md-2">
									<input class="design form-control" readonly />
								</div>

							</div>
							<div class="row select-shop-shoe ">
								<div class="col-md-1 hide">Style</div>
								<div class="col-md-2 hide no-padding ">
									<select class="form-control hide change-style-1">
									<?php echo $style; ?>
									</select>
								</div>
								<div class="col-md-1 hide">Last</div>
								<div class="col-md-2 no-padding hide">
									<select class="form-control change-last-1" disabled="disabled">
									<?php echo $last; ?>
									</select>
								</div>
								<div class="col-md-3">Enter Design Number</div>
								<div class="col-md-2">
									<input class="design form-control design-number" />
								</div>
								<div class="col-md-1">
									<button class="btn btn-default select-design-number">SELECT</button>
								</div>


							</div>


							<div id="steps">
								<div id="step0">
									<div class="slide_container">
										<div class="row step0_main_slider">
											<div class="col-md-2"></div>
											<div class="col-md-9">
												<button class="btn btn-primary btn-main create-new-shoe">Design
													A New Shoe</button>
												<button
													class="btn btn-primary btn-main select-existing-shoe">Select
													Or Shop An Existing Design</button>
											</div>
											<div class="col-md-1"></div>
										</div>

									</div>
								</div>
								<div id="step1">
									<h4 class="center head">
										Select Style
										</h3>
										<div class="slide_container">
											<ul class="step1_main_slider">
        <?php
								foreach($styles as $style){
									?>
            <li><img class="main-shoe"
													src="<?php echo CustomShoeConfig::IMG_BASE . $style['location'] . '_A7.png'; ?>"
													alt="<?php echo $style['style_name']; ?>">
													<h3 class="main-shoe"><?php echo $style['style_name']; ?></h3>
													<p><?php echo $style['style_description']; ?></p></li>
            <?php
								}
								?>
    </ul>
										</div>
										<div class="center">
											<button class="btn btn-default select-main-back">Back</button>
											<button class="btn btn-default select-style">Next</button>
										</div>
								
								</div>
								<div id="step2">
									<h4 class="center head">Select Last</h4>
									<div class="slide_container"></div>
									<div class="center">
										<button class="btn btn-default select-style-back">Back</button>
										<button class="btn btn-default select-last">Next</button>
									</div>
								</div>
								<div id="step3">
									<h4 class="center head">Select Design Template</h4>
									<div class="slide_container"></div>
									<div class="center">
										<button class="btn btn-default toggle select-last-back">Back</button>
										<button class="btn btn-default select-design">Next</button>
									</div>
								</div>
								<div id="step4">
									<div class="slide_container">
										<div class="step4_main_slider"></div>
									</div>
									<div class="center">
										<h3 class="center" id="custom-price">
											<span class="price"></span>
										</h3>
										<button class="btn btn-default select-design-back">Back</button>
										<button class="btn btn-default confirm-design">Finish Order</button>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="seperation"></div>
					<div class="box box-solid" id="sizing">
						<div class="overlay"></div>
						<div class="loading-img"></div>
						<div class="box-body">
							<div class="row">

								<div class="col-md-12 ">
									Sizing Left
									<div class="row">
										<div class="col-md-2 ">
											<select class="form-control lsize">
											<?php echo $size; ?>
											</select>
										</div>
										<div class="col-md-2 ">
											<select class="form-control lwidth">
											<?php echo $width; ?>
											</select>
										</div>
										<div class="col-md-2 ">
											<input type="text" class="form-control m_lsize"
												placeholder="LENGTH (CM)" />
										</div>
										<div class="col-md-2 ">
											<input type="text" class="form-control m_lwidth"
												placeholder="WIDTH (CM)" />
										</div>
										<div class="col-md-2 ">
											<input type="text" class="form-control m_lgirth"
												placeholder="HEIGHT (CM)" />
										</div>
										<div class="col-md-2 ">
											<input type="text" class="form-control m_linstep"
												placeholder="INSTEP (CM)" />
										</div>
									</div>

								</div>
								<div class="col-md-12 ">
									Sizing Right
									<div class="row">
										<div class="col-md-2 ">
											<select class="form-control rsize">
											<?php echo $size; ?>
											</select>
										</div>
										<div class="col-md-2 ">
											<select class="form-control rwidth">
											<?php echo $width; ?>
											</select>
										</div>
										<div class="col-md-2 ">
											<input type="text" class="form-control m_rsize"
												placeholder="LENGTH (CM)" />
										</div>
										<div class="col-md-2 ">
											<input type="text" class="form-control m_rwidth"
												placeholder="WIDTH (CM)" />
										</div>
										<div class="col-md-2 ">
											<input type="text" class="form-control m_rgirth"
                                                                                               placeholder="HEIGHT (CM)" />
										</div>
										<div class="col-md-2 ">
											<input type="text" class="form-control m_rinstep"
												placeholder="INSTEP (CM)" />
										</div>
									</div>

								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4 no-padding">
					<div class="box box-solid" id="details">
						<div class="overlay"></div>
						<div class="loading-img"></div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">

									<div class="nav-tabs-custom material-select">
										<!-- Tabs within a box -->
										<ul class="nav nav-tabs  ui-sortable-handle">

											<li class="tab-toe"><a href="#tab-toe" data-toggle="tab">Toe</a></li>
											<li class="tab-vamp"><a href="#tab-vamp" data-toggle="tab">Vamp</a></li>
											<li class="tab-eyestay"><a href="#tab-eyestay"
												data-toggle="tab"> Eyestay</a></li>
											<li class="tab-quarter active"><a href="#tab-quarter"
												data-toggle="tab">Quarter</a></li>
											<li class="tab-foxing"><a href="#tab-foxing"
												data-toggle="tab">Foxing</a></li>
										</ul>
										<div class="tab-content tab-part">
											<div class="chart tab-pane " id="tab-toe">
												<div class="col-md-12 no-padding">
													Select Material <select class="leather-types form-control"></select>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-12 no-padding">
													Select Color
													<div class="box box-solid">
														<div class="box-body leather-colors">
															<span class="preview"> <img /> <span class="pull-left"></span>
																<span class="pull-right"></span>
															</span>
															<div class="clearfix leather-color"></div>
														</div>
													</div>
												</div>
											</div>
											<div class="chart tab-pane " id="tab-vamp">
												<div class="col-md-12 no-padding">
													Select Material <select class="leather-types form-control"></select>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-12 no-padding">
													Select Color
													<div class="box box-solid">
														<div class="box-body leather-colors">
															<span class="preview"> <img /> <span class="pull-left"></span>
																<span class="pull-right"></span>
															</span>
															<div class="clearfix leather-color"></div>
														</div>
													</div>
												</div>
											</div>
											<div class="chart tab-pane " id="tab-eyestay">
												<div class="col-md-12 no-padding">
													Select Material <select class="leather-types form-control"></select>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-12 no-padding">
													Select Color
													<div class="box box-solid">
														<div class="box-body leather-colors">
															<span class="preview"> <img /> <span class="pull-left"></span>
																<span class="pull-right"></span>
															</span>
															<div class="clearfix leather-color"></div>
														</div>
													</div>
												</div>
											</div>
											<div class="chart tab-pane active" id="tab-quarter">
												<div class="col-md-12 no-padding">
													Select Material <select class="leather-types form-control"></select>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-12 no-padding">
													Select Color
													<div class="box box-solid">
														<div class="box-body leather-colors">
															<span class="preview"> <img /> <span class="pull-left"></span>
																<span class="pull-right"></span>
															</span>
															<div class="clearfix leather-color"></div>
														</div>
													</div>
												</div>
											</div>

											<div class="chart tab-pane " id="tab-foxing">
												<div class="col-md-12 no-padding">
													Select Material <select class="leather-types form-control"></select>
												</div>
												<div class="clearfix"></div>
												<div class="col-md-12 no-padding">
													Select Color
													<div class="box box-solid">
														<div class="box-body leather-colors">
															<span class="preview"> <img /> <span class="pull-left"></span>
																<span class="pull-right"></span>
															</span>
															<div class="clearfix leather-color"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">

									<div class="nav-tabs-custom details">
										<!-- Tabs within a box -->
										<ul class="nav nav-tabs  ui-sortable-handle">
											<li class="tab-stitching active"><a href="#tab-stitch"
												data-toggle="tab">Stitching</a></li>
											<li class="tab-lacing "><a href="#tab-lace" data-toggle="tab">Lacing</a></li>
											<li class="tab-monogram"><a href="#tab-monogram"
												data-toggle="tab">Monogram</a></li>
										</ul>
									</div>
									<div class="tab-content tab-part">
										<div class="chart tab-pane active" id="tab-stitch">
											<div class="col-md-12 no-padding">
												<div class="box box-solid">
													<div class="box-body stitch-lace-color stitches">
														<span class="preview">
															<div class="preview-color"></div> <span class="pull-left"></span>
															<span class="pull-right"></span>
														</span>
														<div class="box box-solid">
															<div class="box-body stitching-colors">
																<div class="clearfix stitching-color"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="chart tab-pane " id="tab-lace">
											<div class="col-md-12 no-padding">
												<div class="box box-solid">
													<div class="box-body stitch-lace-color laces">
														<span class="preview">
															<div class="preview-color"></div> <span class="pull-left"></span>
															<span class="pull-right"></span>
														</span>
														<div class="box box-solid">
															<div class="box-body lacing-colors">
																<div class="clearfix lacing-color"></div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="chart tab-pane " id="tab-monogram">
											<div class="col-md-12 no-padding">
												<div class="box box-solid">
													<div class="box-body monograms">

														<div class="box box-solid">
															<div class="box-body lacing-colors">
																<div class="clearfix monogram">
																	<input type="text" class="form-control monogram-text"
																		maxlength="3" />
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="box box-solid">
						<div class="box-body">
							<button class="btn btn-success confirm-design">Finish Your Order</button>
						</div>
					</div>
				</div>
			</div>

			<div class="chart tab-pane" id="order-review">
				<div class="col-md-12 order-review">
					<div calss="row">
						<div class="col-md-8 no-padding">
							<div class="box box-solid">
								<div class="overlay"></div>
								<div class="loading-img"></div>
								<div class="box-body">
									<div class="row cart-items"></div>
								</div>
							</div>
						</div>
						<div class="col-md-4 no-padding">

							<div class="box box-solid">
								<div class="overlay"></div>
								<div class="loading-img"></div>
								<div class="box-body">
									<?php $this->load->view('partner/order_form');?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 order-confirmation" style="display: none;"  >
					<div class="row"></div>
				</div>
			</div>
		</div>