<?php $this->load->view('includes/email_header'); ?>

<div style = "width: 100%; overflow: hidden;">
    <div style = "width: 650px; margin: 0 auto; padding-bottom: 15px;">
        <p style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 18px; color: #414141;">Hi <?php echo $data['firstname']; ?>,</p>
        <div style = "margin: 5px 0; ">
            <p style = "font-family: gibsonsemibold, arial, sans-serif; font-size: 18px; ">
            Can you please confirm the shipping address along with the phone number. Please note you won't
             be able to change your shipping address once you have confirmed it. Also, you or someone will 
             need to sign for the package once it's delivered. </p>
            <p style = "font-family: gibsonsemibold, arial, sans-serif; font-size: 18px;color: #414141; " ><?php echo $data['firstname']. ' '. $data['lastname']; ?><br/><?php echo $data['address1']; ?><br/><?php echo $data['country']; ?><br/><?php echo $data['telephone']; ?></p>
        </div>
        <p style = "font-family: gibsonsemibold, arial, sans-serif; font-size: 18px;color: #414141; ">Thank You. Please note you will receive an email 
        along with the tracking details once your shoes has been shipped. </p><br/>
        <p style = "font-size: 18px; margin: 10px 0; color: #414141;">Regards, <br/>Nikunj</p>
    </div>
</div>

<?php $this->load->view('includes/email_footer'); ?>

