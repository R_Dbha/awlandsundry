<?php $this->load->view('includes/email_header'); ?>

<div style = "width: 100%; overflow: hidden;">
    <div style = "width: 760px; margin: 0 auto; padding-bottom: 15px;">
        <p style = "font-weight: bold; font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; color: #414141;">Hi <?php echo $emaildetails['fname'] ?> ,</p>
        <div style = "margin: 5px 0; overflow: hidden;">
            <p style = "font-size: 18px; margin: 15px 0;"></p>
            <p style = "font-family: gibsonsemibold, arial, sans-serif; font-size: 21px; margin: 15px 0;">To change your password please click on the button below:</p>
            <p><a name="validate" id="validate" href="<?php echo $emaildetails['link'] ?>" style = "text-decoration:none;background: #00ADD3;clear:both;display:block;width:80px;
                  color: #FFF !important; text-transform: uppercase; border: 1px solid #00ADD3; padding: 7px 14px; border-radius: 5px;
                  -webkit-border-radius: 5px; outline: none; font-weight: bold; cursor: pointer; line-height: normal; font-family: inherit; font-size: 100%; margin: 0;">Click Here</a></p>
        </div>
        <p style = "font-size: 18px; margin: 10px 0;">Thank You.</p>
        <p style = "font-size: 18px; margin: 10px 0;">Awl &amp; Sundry team</p>
    </div>
</div>

<?php $this->load->view('includes/email_footer'); ?>

