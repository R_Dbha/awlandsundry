<?php $this->load->view('includes/email_header'); ?>

<div class="content" style="width:80%;
     margin: auto;
     text-align: center
     float: none;
     clear: both;
     border-radius: 5px;
     -moz-border-radius: 5px;
     -webkit-border-radius: 5px;">
    <div style="text-align: center;">
        <h2 style=" text-align: center;">Your Appointment Details</h2>
        <img style="width: 80%;" src="<?php echo base_url() . 'assets/css/images/appointment_email.gif'; ?>"/>
    </div>
    <div class="description" style="width:80%; margin:15px auto 15px;
         padding:10px 0px;
         text-align: left;">
        <div>
            Dear <?php echo $name; ?>
            <br>
            <br>
            Woot woot! We are beyond excited to meet you in person for your custom fitting and help you design your perfect pair of custom shoes.
            <br>
            <br>
            Your appointment is confirmed for <?php echo $appointment_date; ?>  at<!-- 18 West 23rd Street, 4th Floor (between 5th and 6th avenue). --> 530 5th Ave, New York, NY 10036. Please check in at the front desk and come up to the CONVENE offices on the 9th floor. We will meet you there.
            <!-- Please buzz "Lucky Guy" when you get here. -->
            <br>
            <br>
            If you are having any issues finding us or running late please give us a call at <!-- (347) 671 4789. -->(844) 785-9692.
            <br>
            See you shortly! 
            <br>
            <br>
            Best,
            <br>
            Awl & Sundry Team
        </div>
    </div>
</div>

<?php $this->load->view('includes/email_footer'); ?>