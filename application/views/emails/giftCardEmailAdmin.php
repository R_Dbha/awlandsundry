<?php $this->load->view('includes/email_header'); ?>

<div class="content" style="width:500px;
     margin: auto;
     float: none;
     clear: both;
     border-radius: 5px;
     -moz-border-radius: 5px;
     -webkit-border-radius: 5px;
     padding: 30px;">
    <h2 style=" text-align: center;">E-Gift Card Details</h2>
    <div id="giftcard">
        <div class="card" style="background: white;
             border-radius: 8px;
             -moz-border-radius: 8px;
             -webkit-border-radius: 8px; 
             width: 450px;
             height: 300px;
             display: block;
             margin: auto;">
            <div  style="padding: 20px 0px 0px 0px; "></div>
            <div class="number" style=" width:50%;float: left;">
                <div style="padding:0px 0px 0px 20px;">SENDER NAME</div>
                <div style="padding:0px 0px 0px 20px;">SENDER EMAIL ID</div>
                <div style="padding:0px 0px 0px 20px;">RECEIVER'S NAME</div>
                <div style="padding:0px 0px 0px 20px;">RECEIVER'S EMAIL ID</div>
                <div style="padding:0px 0px 0px 20px;">CARD NUMBER</div>
                <div  style="padding:0px 0px 0px 20px;">GIFT CARD VALUE</div>
            </div>
            <div class="value" style="width:50%; float: left;">
                <div style="padding:0px 0px 0px 20px;font-size: 13px;font-weight: bold;" >
                    <?php echo $sender_name ?></div>
                <div style="padding:0px 0px 0px 20px;font-size: 13px;font-weight: bold;" >
                    <?php echo $sender_email ?></div>
                <div style="padding:0px 0px 0px 20px;font-size: 13px;font-weight: bold;" >
                    <?php echo $recipient_name ?></div>
                <div style="padding:0px 0px 0px 20px;font-size: 13px;font-weight: bold;" >
                    <?php echo $recipient_email ?></div>
                <div style="padding:0px 0px 0px 20px;font-size: 13px;font-weight: bold;" >
                    <?php echo $card_number; ?></div>
                <div style="padding:0px 0px 0px 20px;font-size: 13px;font-weight: bold;">$<?php echo $giftcard_price; ?></div>
            </div>
        </div>
         <?php if($message!="") { ?>
        <div class="description" style="margin:15px auto 15px;
             padding:10px 0px;
             text-align: center;">
            <h3 style=" text-align: center;">Gift Card Message</h3>
            <?php echo $message; ?>
        </div>
         <?php } ?>
    </div>
</div>

<?php $this->load->view('includes/email_footer'); ?>

