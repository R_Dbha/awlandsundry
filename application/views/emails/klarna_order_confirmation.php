<?php
$this->load->view('includes/email_header');
?>
<div id="content">
    <p>Dear <?php echo $user['first_name'] . ' ' . $user['last_name']; ?></p>

    <p>Thank you for your recent purchase. We have sent your order to our
        shop. Your handcrafted shoes will be delivered to your doorstep within
        4-6 weeks. We will be in touch with you during this time to keep you
        informed about your shoes.</p>

    <p>In the mean time, please feel free to reach out to us at <?php echo $this->config->item('contact_phone'); ?>
	 if you have any questions.</p>

    <div>
        <h3>Order Summary</h3>
        <p style="margin: 0;">Order Date : <?php echo date('Y-m-d', strtotime($order_summary[0]['order_date'])); ?></p>
        <p style="margin: 0;">Order Number : <?php echo $order_summary[0]['invoice_no']; ?></p>
    </div>
    <div style="overflow: hidden;">
        <div style="float: left;">
            <h3>Shipping Details</h3>
            <p style="margin: 0;"><?php echo $shipping[0]['firstname'] . ' ' . $shipping[0]['lastname']; ?></p>
            <p style="margin: 0;"><?php echo $shipping[0]['address1']; ?></p>
            <p style="margin: 0;"><?php echo $shipping[0]['city'] . ', ' . $shipping[0]['state'] . ', ' . $shipping[0]['country']; ?></p>
            <p style="margin: 0;"><?php echo $shipping[0]['telephone']; ?></p>
        </div>
        <div style="float: right; text-align: right;">
            <h3>Billing Details</h3>
            <p style="margin: 0;"><?php echo $billing[0]['first_name'] . ' ' . $billing[0]['last_name']; ?></p>
            <p style="margin: 0;"><?php echo ($billing[0]['address1'] !== "0") ? $billing[0]['address1'] : ""; ?></p>
            <p style="margin: 0;"><?php echo ($billing[0]['city'] !== "0") ? $billing[0]['city'] . ', ' . $billing[0]['state'] . ', ' . $billing[0]['country'] : ""; ?></p>
            <p style="margin: 0;"></p>
        </div>

    </div>
    <div style="clear: both; margin-top: 30px;">
        <table style="width: 100%;">
            <thead>
                <tr>
                    <td><h3>Shoe Details</h3></td>
                    <td style="text-align: center;"><h3>Quantity</h3></td>
                    <td><h3 style="text-align: right;">Price</h3></td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($shoedesign as $key => $design) {
                    if($design['type']=="Custom shoe"){
                    ?>
                    <tr>
                        <td>
                            <div style="width: 200px; height: 115px;">
                                <?php
                                if (isset($design['shop_images']) && is_array($design['shop_images'])) {
                                    ?>
                                    <img
                                        style="width: 100%;"
                                        src="<?php echo ApplicationConfig::ROOT_BASE . 'files/' . $design['shop_images'][0]['file_name']; ?>" />
                                        <?php
                                    } else {
                                        ?>
                                    <img style="width: 100%;"
                                         src="<?php echo ApplicationConfig::ROOT_BASE . 'files/designs/' . $design['image_file'] . '_A0.png'; ?>" />
                                     <?php } ?>
                            </div>
                        </td>
                        <td style="vertical-align: middle; text-align: center;"><h3><?php echo $summary[$key]['quantity']; ?></h3></td>
                        <td style="text-align: right; vertical-align: middle;"><h3>$<?php echo number_format($summary[$key]['price'], 2); ?></h3></td>


                    <tr>
                        <td>
                            <div>
                                <div>Last : <?php echo $design['last_name']; ?></div>
                                <div>Style : <?php echo $design['style_name']; ?></div>
                            </div>
                            <?php if ($design['can_show_details'] == 1) { ?>
                                <div>
                                    <p>Leather texture/color :</p>
                                    <ul style="list-style: none;">
                                        <?php if (!$design['t_is_nothing'] && $design['t_is_material']) { ?>
                                            <li>Toe : <?php echo $design['toe_material'] . '/' . $design['toe_color']; ?></li>
                                            <?php
                                        }
                                        if ($design['is_vamp_enabled'] && !$design['v_is_nothing'] && $design['v_is_material']) {
                                            ?>
                                            <li>Vamp : <?php echo $design['vamp_material'] . '/' . $design['vamp_color']; ?></li>
                                            <?php
                                        }
                                        if (!$design['e_is_nothing'] && $design['e_is_material']) {
                                            ?>
                                            <li>Eyestays : <?php echo $design['eyestay_material'] . '/' . $design['eyestay_color']; ?></li>
                                        <?php } ?>
                                        <li>Quarter : <?php echo $design['quarter_material'] . '/' . $design['quarter_color']; ?></li>
                                        <?php if (!$design['f_is_nothing'] && $design['f_is_material']) { ?>
                                            <li>Foxing : <?php echo $design['foxing_material'] . '/' . $design['foxing_color']; ?></li>
                                        <?php } ?>
                                    </ul>
                                </div>

                                <div>
                                    <?php if ($design['is_lace_enabled']) { ?>
                                        <p>Lace Color : <?php echo $design['lace_name']; ?></p>
                                    <?php } ?>
                                    <p>Stitching Color : <?php echo $design['stitch_name']; ?></p>
                                    <?php if ($design['monogram_text'] != NULL) { ?><div>Monogram : <?php echo $design['monogram_text']; ?></div> <?php } ?>
                                </div>
                            <?php } ?>
                            <div>
                                <p>Sizing :</p>
                                <ul style="list-style: none;">
                                    <li>Left : <?php echo $summary[$key]['left_size'] . $summary[$key]['left_width']; ?></li>
                                    <li>Right : <?php echo $summary[$key]['right_size'] . $summary[$key]['right_width']; ?></li>
                                </ul>
                            </div>
                        </td>
                        <td></td>
                        <td></td>


                    </tr>
                    <?php } else { ?>
                        <tr>
                            <td><div style="width: 200px; height: 115px;"><img  style="width: 80%;" src="<?php echo ApplicationConfig::ROOT_BASE .'assets/css/images/logo.png'; ?>" ></div></td>
                            <td style="vertical-align: middle; text-align: center;"><h3><?php echo $shoedesign[$key]['quantity']; ?></h3></td>
                            <td style="text-align: right; vertical-align: middle;"><h3>$<?php echo number_format($shoedesign[$key]['giftcard_price'], 2); ?></h3></td>
                        </tr>
                    <?php }
                } ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <table style="width: 100%;">
                            <?php if ($discount > 0) { ?>
                                <tr>
                                    <td>PROMO DISCOUNT</td>
                                    <td style="text-align: right;">$ <?php echo number_format($discount, 2); ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td>SHIPPING</td>
                                <td style="text-align: right;">$ <?php echo number_format(0, 2); ?></td>
                            </tr>
                            <tr>
                                <td>TAXES</td>
                                <td style="text-align: right;">$ <?php echo number_format($tax_amount, 2); ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <h3>ORDER TOTAL</h3>
                                </td>
                                <td style="text-align: right;">
                                    <h3>$ <?php echo number_format(round($total), 2); ?></h3>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td colspan="3"><br /> Thank you,
                        <h4 style="margin: 5px 0px;">Awl & Sundry team</h4></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php
$this->load->view('includes/email_footer');
?>

