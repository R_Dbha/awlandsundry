<?php $this->load->view('includes/email_header'); ?>
<div id="content">
    <p>Hello !,</p>

    <p>Welcome to Awl & Sundry</p>

    <p>Congrats--you are now a member of the Awl & Sundry family.we will bring you special promotions, offers and of course the latest in men's style.</p> 

    <p>As a token of our appreciation we bring to you a promo code for 10% off for your first purchase - <?php echo $promocode; ?></p>

    <p>Redeem your promo code and design your Awl & Sundry pair today.</p>


    <p>Thank you, </p>

    <div id="regards">
        <p style="margin:0;">Awl & Sundry Team</p>
    </div>
</div>
<?php $this->load->view('includes/email_footer'); ?>

