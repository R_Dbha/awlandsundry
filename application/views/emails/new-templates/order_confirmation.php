<?php 

$this->load->view('includes/email_header'); 

?>
<div id="content">
    <p>Dear</p>

    <p>Thank you for your recent purchase.We have sent you order to our shop.Your handcrafted shoes will be delivered to your desktop within -4 weeks.We will be in touch with you during this time to keep you informed about your shoes.</p>

    <p>In the mean time,please feel free to reach out us at(917)825 9404 if you have any questions.</p> 

    <div>
        <h3>Order Summary</h3>
        <p style="margin: 0;">Order Date :</p>
        <p style="margin: 0;">Order Number :</p>
    </div>
    <div style="overflow:hidden;">
        <div style="float:left;">
            <h3>Shipping Details</h3>
            <p style="margin: 0;">Nikunj Marvania</p>
            <p style="margin: 0;">216 1st street, Apt 1</p>   
            <p style="margin: 0;">Jersy City, NJ 07302</p>
            <p style="margin: 0;">(917)825 9404</p>
        </div>
        <div style="float:right;text-align: right;">
            <h3>Billing Details</h3>
            <p style="margin: 0;">Nikunj Marvania</p>
            <p style="margin: 0;">216 1st street, Apt 1</p>   
            <p style="margin: 0;">Jersy City, NJ 07302</p>
            <p style="margin: 0;">(917)825 9404</p>
        </div>

    </div>
    <div style="clear:both;margin-top: 30px;">
        <table style="width: 100%;">
            <thead>
                <tr>
                    <td><h3>Shoe Details</h3></td>
                    <td style="text-align: center;"><h3>Quantity</h3></td>
                    <td><h3 style="text-align: right;">Price</h3></td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div style="width: 200px;height: 115px;">
                            <img style="width:100%;" src="https://www.awlandsundry.com/files/designs/491598_A7.png" />
                        </div>

                        <div>
                            <p>Last : Harvey</p>
                            <p>Style : Oxford</p>
                        </div>

                        <div>
                            <p>Leather texture/color : </p>
                            <ul style="list-style-type: none;">
                                <li>Toe : Calf Plain/Black</li>
                                <li>Vamp : Calf Plain/Black</li>
                                <li>Eyestays : Calf Plain/Black</li>
                                <li>Quarter : Calf Plain/Black</li>
                                <li>Foxing : Calf Plain/Black</li>
                            </ul>
                        </div>

                        <div>
                            <p>Lace Color : Red</p>
                            <p>Stitching Color : Red</p>
                            <p>Monogram : NMD</p>
                        </div>

                        <div>
                            <p>Sizing : </p>
                            <ul style="list-style-type: none;">
                                <li>Left : 8D</li>
                                <li>Right : 8D</li>
                            </ul>

                        </div>
                    </td>
                    
                    <td style="vertical-align: top;text-align: center;"><h3>1</h3></td>
                    <td style="text-align: right;vertical-align: top;"><h3>$350</h3></td>
                </tr>
            </tbody>
        </table>
    </div>
    

</div>

</body>
</html>

