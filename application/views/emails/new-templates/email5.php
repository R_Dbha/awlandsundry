<?php $this->load->view('includes/email_header'); ?>
<div id="content">
    <p>Hi XXXX,</p>

    <p>It's time to pop a bottle of your choicest Champagne as you open the attachment below to see what you own!  We appreciate your patience and trust in us as we have carefully handcrafted your custom pair with love and passion. We hope you love them as much as we do! </p>

    <p>We have shipped them out to you and you can track them here. If it's not too much trouble, can we please request you to send us few pictures of you rocking them? We'd love to feature you on our website! </p> 

    <p>We would love to hear from you about your experience shopping with us and more importantly, how we can improve. We take your feedback very seriously. </p>
    
    <p>Lastly, we truly want to thank you for your support. We would not be here if it wasn't for you and for that, we are grateful. </p>
    
    <p>Please reach out to us if you have any questions, queries or insights.  We look forward hearing from you. </p>


    <p>Yours cordially, </p>

    <div id="regards">
        <p style="margin:0;">Mario Lanzarotti</p>
        <p style="margin:0;">Chief Happiness Officer</p>
    </div>
</div>

<?php $this->load->view('includes/email_footer'); ?>
