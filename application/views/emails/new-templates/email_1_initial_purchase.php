<?php $this->load->view('includes/email_header'); ?>
<div id="content">
    <p>Dear ____,</p>

    <p>Thank you for your recent purchase with Awl & Sundry. I would like to welcome you to the Awl & Sundry family.</p>

    <p>We at Awl & Sundry believe in enabling you to create your own masterpiece - your own design brought to life. Each shoe is a work of art and a product of traditional craftsmanship.</p> 

    <p>Providing exceptional customer service is hardwired in our DNA. And as a testimony to this, we will keep you updated, every week, on the progress of your order. Once your shoes are ready, we will send you the pictures of the finished product. </p>

    <p>In the meantime, please feel free to get in touch with us if you have any questions. We look forward to serving you.</p> 


    <p>Thank you,</p>

    <div id="regards">
        <p style="margin:0;">Mario Lanzarotti</p>
        <p style="margin:0;">Chief Happiness Officer</p>
    </div>
</div>

<?php $this->load->view('includes/email_footer'); ?>
