<?php $this->load->view('includes/email_header'); ?>
<div id="content">
    <p>Hi XXXX,</p>

    <p>It's been almost 2 weeks! We are halfway through the process of crafting your shoes.  We are so excited and can't wait to ship them out to you soon! Your shoes are an investment. It's well worth your time and effort to invest in a good pair and take care of them. While you wait, we wanted to send you a quick note on the best practices to care for your beauties.  </p>
    <ul style="list-style-type: none;">
        <li style="margin: 10px 0px;">a) <span>Shoe trees</span> - Your Awl & Sundry shoes will come with high quality wood shoe trees. Put them to use from day one. We recommend you stick your shoe trees into the shoes when not being used. They draw out the inner moisture and help the leather maintain its form. This prevents the leather from shrinking and creasing. This will increase the life of your shoes. </li>
        <li style="margin: 10px 0px;">b) <span>Shoe horn</span> - Shoe horns have been around for centuries but it's the most unused shoe accessory. It helps you slide your foot easily into your well-fitted shoe without damaging the heel. Use your Awl & Sundry shoe horn everyday and pack it with you when you travel</li>
        <li style="margin: 10px 0px;">c) <span>Shoe bags</span> - Your Awl & Sundry shoes come with protective bags. We recommend you put your shoes in bags when not in use. This will save your shoes from accumulating dust. </li>
        <li style="margin: 10px 0px;">d) <span>Shoe polish</span> - 	Clean your shoes regularly to remove dirt, stains, and layers of built-up polish. Remember leather is a skin just like yours, and requires nourishment and special care. We recommend using Saphir polish due to its natural and organic ingredients. </li>
    </ul>
    <p>Take care of your shoes, and they will take care of you when you walk out in the sun! </p> 

    <p>Please reach out to us if you have additional questions. </p>


    <p>Yours cordially, </p>

    <div id="regards">
        <p style="margin:0;">Mario Lanzarotti</p>
        <p style="margin:0;">Chief Happiness Officer</p>
    </div>
</div>

<?php $this->load->view('includes/email_footer'); ?>
