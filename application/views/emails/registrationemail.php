<?php $this->load->view('includes/email_header'); ?>
<div id="content">
    <p>Hi <?php echo $first_name; ?>,</p>

    <p>Now that you have taken the leap towards becoming a shoe designer with the purchase of your Awl & Sundry pair, it's time to up the ante and leave an indelible impression on every person who passes you by. </p>

    <p>They say, "Good things are worth waiting for" and we couldn't agree more! While you wait for your shoes to be crafted, here are a few style recommendations for your oxford/ derby/ monks/ loafers. </p> 

    <p>Keep yourself updated through our daily recommendations by joining us on our social media outlets. </p>

    
    <p>Yours cordially, </p>

    <div id="regards">
        <p style="margin:0;">Mario Lanzarotti</p>
        <p style="margin:0;">Chief Happiness Officer</p>
    </div>
</div>

<?php $this->load->view('includes/email_footer'); ?>
