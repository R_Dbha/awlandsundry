<?php $this->load->view('includes/email_header'); ?>
<div id="content">
    <p style="font-family: 'Raleway', sans-serif;">Hello!</p>

    <p style="font-family: 'Raleway', sans-serif;">Welcome to Awl &amp; Sundry!</p>

    <p style="font-family: 'Raleway', sans-serif;">Congrats-you are now a member of the Awl & Sundry family. We will bring you special promotions, offers and of course the latest in men's style.</p> 

    <p style="font-family: 'Raleway', sans-serif;">As a token of our appreciation we bring to you a promo code for 10% off for your first purchase - <a href="<?php echo base_url(); ?>" style=" font-family: 'Raleway', sans-serif;text-transform: uppercase;text-decoration: none;color: #000;font-weight: 600;padding: 0px 10px;border: 1px solid #000;line-height: 42px;cursor:pointer;display: block;margin: 20px auto;width: 140px;text-align: center;"><?php echo $promocode; ?></a></p>

    <p style="font-family: 'Raleway', sans-serif;">Redeem your promo code and design your Awl & Sundry pair today.</p>


    <p style="font-family: 'Raleway', sans-serif; margin:0;">Thank you, </p>
    <div id="regards">
        <p style="font-family: 'Raleway', sans-serif; margin:0;">Awl & Sundry Team</p>
    </div>
</div>
<?php $this->load->view('includes/email_footer'); ?>

 