<?php
/**
 * @author ajith
 * @date 25 Mar, 2015
 */
$this->load->view('includes/email_header');
?>
<div id="content">
    <p style="font-family: 'Raleway', sans-serif;">Hello!</p>

    <p style="font-family: 'Raleway', sans-serif;">The order <?php echo @$invoice_no; ?> has been shipped</p>

    <p style="font-family: 'Raleway', sans-serif;">Below  are the details</p> 

    <h4 style="font-family: 'Raleway', sans-serif;"></h4>

    <table >
        <tr>
            <td>Order No: </td>
            <td><?php echo @$order_id; ?></td>
        </tr>
        <tr>
            <td>Invoice No:</td>
            <td><?php echo @$invoice_no; ?></td>
        </tr>
        <tr>
            <td>Shipped Date</td>
            <td><?php echo @$shipped_date; ?></td>
        </tr>
        <tr>
            <td>Carrier</td>
            <td><?php echo @$carrier; ?></td>
        </tr>
        <tr>
            <td>Tracking No:</td>
            <td><?php echo @$tracking_no; ?></td>
        </tr>
        <tr>
            <td>Estimated Delivery Date </td>
            <td><?php echo @$delivery_date; ?></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width="50%">
                <h4 style="font-family: 'Raleway', sans-serif;">Billing Address</h4>
            </td>
            <td width="50%">
                <h4 style="font-family: 'Raleway', sans-serif;">Shipping Address</h4>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" >
                    <tr>
                        <td width="30%">Name: </td>
                        <td width="70%"><?php echo @$billing_address['first_name'] . ' ' . $billing_address['last_name']; ?></td>
                    </tr>
                    <tr>
                        <td>Address Line 1:</td>
                        <td><?php echo isset($billing_address['address1']) && $billing_address['address1'] != '0' ? $billing_address['address1'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td>Address Line 2</td>
                        <td><?php echo isset($billing_address['address2']) && $billing_address['address2'] != '0' ? $billing_address['address2'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><?php echo isset($billing_address['city']) && $billing_address['city'] != '0' ? $billing_address['city'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><?php echo isset($billing_address['state']) && $billing_address['state'] != '0' ? $billing_address['state'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td>Country </td>
                        <td><?php echo isset($billing_address['country']) && $billing_address['country'] != '0' ? $billing_address['country'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td>Zip code</td>
                        <td><?php echo isset($billing_address['zipcode']) && $billing_address['zipcode'] != '0' ? $billing_address['zipcode'] : ''; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td></td>
                    </tr>

                </table>
            </td>
            <td>
                <table >
                    <tr>
                        <td width="30%">Name: </td>
                        <td width="70%"><?php echo @$shipping_address['firstname'] . ' ' . $shipping_address['lastname']; ?></td>
                    </tr>
                    <tr>
                        <td>Address Line 1:</td>
                        <td><?php echo @$shipping_address['address1']; ?></td>
                    </tr>
                    <tr>
                        <td>Address Line 2</td>
                        <td><?php echo @$shipping_address['address2']; ?></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><?php echo @$shipping_address['city']; ?></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><?php echo @$shipping_address['state']; ?></td>
                    </tr>
                    <tr>
                        <td>Country </td>
                        <td><?php echo @$shipping_address['country']; ?></td>
                    </tr>
                    <tr>
                        <td>Zip code</td>
                        <td><?php echo @$shipping_address['zipcode']; ?></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td><?php echo @$shipping_address['phone']; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <p style="font-family: 'Raleway', sans-serif; margin:0;">Thank you, </p>
    <div id="regards">
        <p style="font-family: 'Raleway', sans-serif; margin:0;">Awl & Sundry Team</p>
    </div>
</div>
<?php $this->load->view('includes/email_footer'); ?>