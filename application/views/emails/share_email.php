<?php
/**
 * @author Ajith Vp <ajith@kraftlabs.com>
 * @copyright Copyright (c) 2014, Ajith Vp <ajith@kraftlabs.com>
 * @date 04-Dec-2014
 */
?>
<?php $this->load->view('includes/email_header'); ?>
<div>
    <h2 style="text-align: center;">
        The World at Your Feet!
    </h2>
    <h2 style="text-align: center;">Buy your custom, handmade pair from over 2 billion design variations today!</h2>
    <img style="width:100%;" src="<?php echo $image; ?>" />
    <a style="text-decoration: none;display: block;background: #ccc;text-align: center;width: 145px;border-radius: 3px;color: #fff;font-weight: bold;font-size: 14px;
       margin: 10px auto;" href="<?php echo $url; ?>">View this design</a>
    <h4 style="text-align: center;"><?php echo $message; ?></h4>
</div>
<?php $this->load->view('includes/email_footer'); ?>