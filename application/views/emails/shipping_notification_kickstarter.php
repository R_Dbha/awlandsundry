<?php
/**
 *
 * @author ajith
 *         @date 25 Mar, 2015
 */
$this->load->view('includes/email_header');
?>
<div id="content">
	<p style="font-family: 'Raleway', sans-serif;">Dear <?php echo @$billing_address['first_name']; ?>,</p>

	<p style="font-family: 'Raleway', sans-serif;">I am excited to inform you that we have shipped your Awl & Sundry shoes! You can track them <?php
	
	if(sizeof($link) > 1){
		?> here: <?php
		
		foreach($link as $key => $l){
			echo '<br/><a href="' . $l . '" >Item ' . ($key + 1) . '</a>';
		}
		
    }else{ ?><a href="<?php echo $link[0]; ?>">here</a>.
	</p><?php } ?>

    <p style="font-family: 'Raleway', sans-serif;">Hope you love them as
		much as we do! Thank you again for pledging your support and patiently
		waiting for them.</p>

	<p style="font-family: 'Raleway', sans-serif;">Please reply to this
		email if you have any further questions.</p>

	<p style="font-family: 'Raleway', sans-serif; margin: 0;">
		Thank you,<br />Nikunj
	</p>
</div>
<?php $this->load->view('includes/email_footer'); ?>