<?php

ini_set('display_errors', '1');

class Promocode extends CI_Controller
{

    public $data;
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->user = $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if (!($user['user_type_id'] == 1 || $user['user_type_id'] == 4)) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $this->load->model('promocodemodel');
        $this->data['user'] = $this->user;
        $this->data['additional_js'] = array('admin/promocode', 'jquery-ui-1.10.4.custom.min');
    }

    public function index()
    {
        $data = array();
        $this->load->view('admin/promocode', $this->data);
    }

    public function json_promocode()
    {
        $params = $_REQUEST;
        $columns = array(
            'promocode',
            'discount_per',
            'discount_amount',
            'expiry_date',
            'is_common',
            'COUNT(pa.assign_id)',
            'COALESCE(sum(pa.is_used),0)',
        );
        $config = array(
            'per_page' => $params['length'],
            'page' => $params['start'],
            'order_by' => $columns[$params['order'][0]['column']],
            'order' => $params['order'][0]['dir'],
        );
        $search = '';
        if ($params['search']['value'] != '') {
            $search .= '(';
            foreach ($columns as $key => $value) {
                $search .= $value . " LIKE '%" . $params['search']['value'] . "%' OR ";
                if ($key >= 4) {
                    break;
                }
            }
            $search = substr($search, 0, -3);
            $search .= ')';
        }
        $data = $this->promocodemodel->getPromoCodes($config, $search);
        foreach ($data['result'] as $key => $value) {
            $data['result'][$key]['view_usage'] = '<a class="view-usage" data-id="'
                . $data['result'][$key]['promo_id'] . '">View Usage</a>';
            $data['result'][$key]['assign'] = '<a class="assign" data-id="'
                . $data['result'][$key]['promo_id'] . '">Assign</a>';
            $data['result'][$key]['edit'] = '<a class="edit" data-id="'
                . $data['result'][$key]['promo_id'] . '">Edit</a>';
            $data['result'][$key]['expiry_date'] = date('Y-m-d', strtotime($data['result'][$key]['expiry_date']));
            $data['result'][$key]['is_common'] = $data['result'][$key]['is_common'] == 0 ? 'No' : 'Yes';
        }
        $results = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => $data['num_rows'],
            "recordsFiltered" => $data['num_rows'],
            "data" => $data['result'],
        );

        echo json_encode($results);
    }

    public function create($id = '')
    {
        if ($id == '') {
            $data['discount_criteria'] = 'percentage';
            $data['edit_mode'] = false;
        } else {
            $data['edit_mode'] = true;
            $data['promocode'] = $this->promocodemodel->getPromoCodeDetails($id);
            $data['discount_criteria'] = $data['promocode']['discount_per'] == 0 ? 'amount' : 'percentage';
        }

        $this->load->view('admin/promocode_create', $data);
    }

    public function save()
    {
        $form_data = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('promocode', 'PromoCode', 'required|minlength[1]|maxlength[30]');
        $this->form_validation->set_rules('discount_criteria', 'Discount Criteria', '');
        if ($discount_criteria = $this->input->post('discount_criteria') == 'percentage') {
            $this->form_validation->set_rules('discount_per', 'Discount Percentage', 'required|callback_max_value[100]');
        } else {
            $this->form_validation->set_rules('discount_amount', 'Discount Amount', 'required|callback_max_value[495]');
        }
        $this->form_validation->set_rules('expiry_date', 'Expiry Date', 'required');
        $this->form_validation->set_rules('promo_id', 'Promo Id', '');
        $this->form_validation->set_rules('is_common', 'Available to all', '');
        $this->form_validation->set_rules('one_time', 'One time', '');
        $form_data['is_common'] = ($this->input->post('is_common') == 1) ? 1 : 0;
        $form_data['discount_amount'] = ($form_data['discount_amount'] == '') ? 0 : $form_data['discount_amount'];
        $form_data['unlimited'] = ($this->input->post('unlimited') == 1) ? 1 : 0;
        $form_data['one_time'] = ($this->input->post('is_common') == 1) ? ($this->input->post('one_time') == 1) ? $this->input->post('one_time') : 0 : 0;
        unset($form_data['discount_criteria']);
        if ($this->form_validation->run() != false) {
            $promocode_id = $this->promocodemodel->savepromo($form_data, $form_data['promo_id']);
            if ($form_data['promo_id'] == '') {
                $data['promocode_id'] = $promocode_id;
            } else {
                $data['promocode_id'] = $form_data['promo_id'];
            }
            $data['status'] = 'success';
            $data['is_common'] = $form_data['is_common'];
        } else {
            $data['status'] = 'error';
            $error = $this->form_validation->error_array();
            foreach ($error as $key => $value) {
                $data['error'] .= $value . '<br/>';
            }
        }
        echo json_encode($data);
    }

    public function assign_form($promocode_id)
    {
        $data['promocode'] = $this->promocodemodel->getPromoCodeDetails($promocode_id);
        $this->load->view('admin/promocode_assign', $data);
    }

    public function json_promousers($promocode_id)
    {
        $params = $_REQUEST;
        $columns = array(
            'c',
            'pu.email',
            'is_assigned',
        );
        $config = array(
            'per_page' => $params['length'],
            'page' => $params['start'],
            'order_by' => $columns[$params['order'][0]['column']],
            'order' => $params['order'][0]['dir'],
        );
        $search = '';
        if ($params['search']['value'] != '') {
            $search .= '(';

            $search .= "pu.email LIKE '%" . $params['search']['value'] . "%' ";

            $search .= ')';
        }
        $data = $this->promocodemodel->getPromoUsers($config, $promocode_id, $search);
        foreach ($data['result'] as $key => $value) {
            $data['result'][$key]['c'] = '<input type="checkbox" name="promo_user_id" class="promo_user_id" data-id="'
                . $data['result'][$key]['id'] . '" />';
            $data['result'][$key]['is_assigned'] = $data['result'][$key]['is_assigned'] == 0 ? 'No' : 'Yes';
        }
        $results = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => $data['num_rows'],
            "recordsFiltered" => $data['num_rows'],
            "data" => $data['result'],
        );

        echo json_encode($results);
    }

    public function assign($promocode_id)
    {
        $promo_user_id = $this->input->post('promo_users');
        $promo_user_id = substr($promo_user_id, 0, -1);
        $promo_user_id = explode(',', $promo_user_id);
        $data['no_assigned'] = $this->promocodemodel->assignPromo($promocode_id, $promo_user_id);
        echo json_encode($data);
    }

    public function assign_user($promocode_id)
    {
        $email = $this->input->post('email');
        $userdata = array(
            'email' => $email,
            'add_date' => date("Y-m-d H:i:s"),
            'is_active' => 1,
        );
        $userid[] = $this->promocodemodel->insertPromoUser($userdata);
        $data['no_assigned'] = $this->promocodemodel->assignPromo($promocode_id, $userid);
        echo json_encode($data);
    }

    public function view_usage($promocode_id)
    {
        $data['promocode'] = $this->promocodemodel->getPromoCodeDetails($promocode_id);
        $this->load->view('admin/promocode_usage', $data);
    }

    public function json_view_usage($promocode_id)
    {
        $params = $_REQUEST;
        $columns = array(
            'order_id',
            'invoice_no',
            'email',
            'gross_amt',
            'discount',
        );
        $config = array(
            'per_page' => $params['length'],
            'page' => $params['start'],
            'order_by' => $columns[$params['order'][0]['column']],
            'order' => $params['order'][0]['dir'],
        );
        $search = '';
        if ($params['search']['value'] != '') {
            $search .= '(';
            foreach ($columns as $key => $value) {
                $search .= $value . " LIKE '%" . $params['search']['value'] . "%' OR ";
            }
            $search = substr($search, 0, -3);
            $search .= ')';
        }
        $data = $this->promocodemodel->getPromoCodeUasge($promocode_id, $config, $search);

        $results = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => $data['num_rows'],
            "recordsFiltered" => $data['num_rows'],
            "data" => $data['result'],
        );

        echo json_encode($results);
    }

}
