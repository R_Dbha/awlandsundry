<?php
class Excel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('OrderModel');
        $this->load->model('ShoeModel');
        $this->load->model('AdminViewModel');

        $this->load->library('PHPExcel');
    }

    public function index($order_id)
    {
        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Awl and Sundry");
        $objPHPExcel->getProperties()->setLastModifiedBy("Awl and Sundry");
        $objPHPExcel->getProperties()->setTitle("Order Details");
        $objPHPExcel->getProperties()->setSubject("Order Details");
        $objPHPExcel->getProperties()->setDescription("Order Details, generated using PHP classes.");

        $order_summary = $this->OrderModel->getOrderDet($order_id);
        $order = $this->OrderModel->getbillingNshippingDetails($order_id);
        $items = $order['order']['items'];
        $order_status = $this->AdminViewModel->getStatus($order_id);
        $status_desc = $order_status[0]['status_desc'];

        $estimated_date = (strpos($status_desc, 'Estimated completion date : ') === false) ? '' : str_replace('Estimated completion date : ', '', $status_desc);
        $completion_date = $estimated_date == '' ? '' : date('m/d/Y', strtotime($estimated_date));
        for ($j = 0; $j < sizeof($items); $j++) {
            $items[$j]['shoedesign'] = $this->ShoeModel->get_shoe_design_details($items[$j]['design_id'], 0);
            $items[$j]['can_show_details'] = 1;
            if ($items[$j]['design_id'] !== $items[$j]['item_id'] && $items[$j]['item_id'] !== "0") {
                $items[$j]['shop_images'] = $this->OrderModel->get_shop_images($items[$j]['item_id']);
                $items[$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
            }

            $objPHPExcel->setActiveSheetIndex($j);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                'font' => array(
                    'bold' => true,
                ),
            );

            $style_bold = array(
                'font' => array(
                    'bold' => true,
                    'size' => 15,
                ),
            );
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(24);
            $objPHPExcel->getActiveSheet()->getStyle("A4:K4")->getFont()->setSize(10);
            $objPHPExcel->getActiveSheet()->getStyle("A5:J5")->getFont()->setSize(10);

            $objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle("A4:K4")->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle("A5:K5")->applyFromArray($style);

            $objPHPExcel->getActiveSheet()->getStyle("A2")->applyFromArray($style_bold);
            $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->applyFromArray($style_bold);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);

            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(40);
            $objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(35);
            $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(35);
            $objPHPExcel->getActiveSheet()->getRowDimension('4')->setRowHeight(35);
            $objPHPExcel->getActiveSheet()->getRowDimension('5')->setRowHeight(200);

            $objPHPExcel->getActiveSheet()->getStyle('D4')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('G4')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('H4')->getAlignment()->setWrapText(true);
            $objPHPExcel->getActiveSheet()->getStyle('K5')->getAlignment()->setWrapText(true);

            $objPHPExcel->getActiveSheet()->mergeCells('A1:K1');
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', $this->get_order_category($order_id));

            $objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Shoes');

            $objPHPExcel->getActiveSheet()->mergeCells('A3:C3');
            $objPHPExcel->getActiveSheet()->SetCellValue('A3', 'Order Date : ' . date('m/d/Y', strtotime($order_summary[0]['order_date'])));

            $objPHPExcel->getActiveSheet()->mergeCells('D3:J3');
            $objPHPExcel->getActiveSheet()->SetCellValue('D3', 'Completion date : ' . $completion_date);

            $objPHPExcel->getActiveSheet()->SetCellValue('A4', 'Order Number');
            $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Last');
            $objPHPExcel->getActiveSheet()->SetCellValue('C4', 'Picture（detail attached）');
            $objPHPExcel->getActiveSheet()->SetCellValue('D4', 'Upper material and leather color');
            $objPHPExcel->getActiveSheet()->SetCellValue('E4', 'Pay attention');
            $objPHPExcel->getActiveSheet()->SetCellValue('F4', 'Monogram Position');
            $objPHPExcel->getActiveSheet()->SetCellValue('G4', 'Outsole type and color');
            $objPHPExcel->getActiveSheet()->SetCellValue('H4', 'Metal Toe Taps-yes or no');
            $objPHPExcel->getActiveSheet()->SetCellValue('I4', 'Pair');
            $objPHPExcel->getActiveSheet()->SetCellValue('J4', 'Size & width');
            $objPHPExcel->getActiveSheet()->SetCellValue('K4', 'Comments');

            $shoedesign = $items[$j]['shoedesign'];
            $shoe_details = '';
            if (!$shoedesign['t_is_nothing']) {
                if ($shoedesign['t_is_material']) {
                    $shoe_details .= $shoedesign['toe_material'] . ' ';
                    $shoe_details .= $shoedesign['toe_color'] . ', ';
                }
            }

            if ($shoedesign['is_vamp_enabled']) {
                if (!$shoedesign['v_is_nothing']) {
                    if ($shoedesign['v_is_material']) {
                        $shoe_details .= $shoedesign['vamp_material'] . ' ';
                        $shoe_details .= $shoedesign['vamp_color'] . ', ';
                    }
                }
            }
            if (!$shoedesign['e_is_nothing']) {
                if ($shoedesign['e_is_material']) {
                    $shoe_details .= $shoedesign['eyestay_material'] . ' ';
                    $shoe_details .= $shoedesign['eyestay_color'] . ', ';
                }
            }

            if (!$shoedesign['f_is_nothing']) {
                if ($shoedesign['f_is_material']) {
                    $shoe_details .= $shoedesign['foxing_material'] . ' ';
                    $shoe_details .= $shoedesign['foxing_color'] . ', ';
                }
            }

            $shoe_details .= "Stitching color : " . $shoedesign['stitch_name'];
            if ($shoedesign['is_lace_enabled']) {
                $shoe_details .= " Lace : " . $shoedesign['lace_name'];
            }
            if (!$items[$j]['can_show_details']) {
                $shoe_details = '';
            }
            $monogram = $shoedesign['monogram_text'];
            $text = '';
            if ($shoedesign['right_side'] > 0) {
                $text = " on right counter";
            }
            if ($shoedesign['left_side'] > 0) {
                $text = " on left counter";
            }
            if ($shoedesign['left_shoe'] > 0) {
                $text .= ", left shoe";
                if ($shoedesign['right_shoe'] > 0) {
                    $text .= " and right shoe";
                }
            } elseif ($shoedesign['right_shoe'] > 0) {
                $text .= ", right shoe";
            }
            $size = "Left: " . $items[$j]['left_shoe'] . $items[$j]['left_width'] . " Right:" . $items[$j]['right_shoe'] . $items[$j]['right_width'];
            $last = $items[$j]['can_show_details'] ? $shoedesign['manufacturer_code'] : '';
            $objPHPExcel->getActiveSheet()->getStyle('A5:J5')->getAlignment()->setWrapText(true);

            $objPHPExcel->getActiveSheet()->SetCellValue('A5', $order_id);
            $objPHPExcel->getActiveSheet()->SetCellValue('B5', $last);
            if (isset($items[$j]['shop_images'])) {
                $remote_path = ApplicationConfig::ROOT_BASE . 'files/' . $items[$j]['shop_images'][0]['file_name'];
            } else {
                $remote_path = ApplicationConfig::ROOT_BASE . 'files/designs/' . $items[$j]['image_file'] . "_A0.png";
            }

            $image_path = dirname(__FILE__) . '/../logs/file.jpeg';
            copy($remote_path, $image_path);

            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('Shoe Image');
            $objDrawing->setDescription('Shoe Image');
            $objDrawing->setPath($image_path); // filesystem reference for the image file
            $objDrawing->setWidthAndHeight(180, 180);
            $objDrawing->setResizeProportional(true);

            $objDrawing->setCoordinates('C5');
            $objDrawing->setOffsetY(5);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

            $objPHPExcel->getActiveSheet()->SetCellValue('D5', $shoe_details);
            $objPHPExcel->getActiveSheet()->SetCellValue('E5', '');
            $objPHPExcel->getActiveSheet()->SetCellValue('F5', $monogram . $text);
            $objPHPExcel->getActiveSheet()->SetCellValue('G5', '');
            $objPHPExcel->getActiveSheet()->SetCellValue('H5', '');
            $objPHPExcel->getActiveSheet()->SetCellValue('I5', $items[$j]['quantity'] . ' Pair');
            $objPHPExcel->getActiveSheet()->SetCellValue('J5', $size);
            $objPHPExcel->getActiveSheet()->SetCellValue('K5', $order_summary[0]['comments']);

            $objPHPExcel->getActiveSheet()->setTitle('Item ' . ($j + 1));
            if ($j != sizeof($items) - 1) {
                $objPHPExcel->createSheet($j + 1);
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Type: application/force-download');
        header('Content-Type: application/octet-stream');
        header('Content-Type: application/download');
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="order-' . $order_id . '.xls"');
        $objWriter->save('php://output');
    }
    private function get_order_category($order_id)
    {
        return 'MTO-hand welted';
    }
}
