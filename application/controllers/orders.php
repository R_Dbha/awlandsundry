<?php

ini_set('display_errors', 0);

class Orders extends CI_Controller
{

    public $data;
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminOrderModel');
        $this->load->model('AdminViewModel');
        $this->load->model('OrderModel');
        $this->load->model('ShoeModel');
        $this->load->model('CommonModel');
        $this->load->model('UserModel');
        $this->load->model('CustomShoeModel');
        $this->load->model('PartnerModel');
        $this->load->library('form_validation');
        $this->load->helper('common_helper');

        $this->user = $this->session->userdata('user_details');
        if (!($this->user && ($this->user['user_type_id'] == 1 || $this->user['user_type_id'] == 4))) {
            redirect('login');
        }
        $this->data['user'] = $this->user;
        $this->data['additional_js'] = array(
            'https://js.stripe.com/v1/',
            base_url() . 'assets/js/jquery.formance.min.js',
            'jquery.validate.min',
            base_url() . 'assets/js/admin/orders.js',
            'fileuploader',
        );
    }

    public function mto()
    {
        $config = array();
        $config['per_page'] = 450;
        $config['page'] = 0;
        $this->data['form'] = 'mto';

        $filters = $this->input->post();
        $order_source = 'mto';

        $result = $this->AdminOrderModel->getProcessedOrders($config, $order_source, $filters);
        $styles = $this->AdminOrderModel->get_styles();
        $types = $this->AdminOrderModel->simple_get('custom_collection_types', array('id', 'name'), array('style_name' => $styles[0]['name']), 'order');
        $users = $this->AdminOrderModel->simple_get('users', array('user_id id', 'concat(first_name," ",last_name, " - ",email) name'), '', 'first_name');

        $this->data['edit']['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['edit']['styles'] = generate_options($styles);
        $this->data['edit']['types'] = generate_options($types);
        $this->data['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['style'] = generate_options($this->styles, 0, 0, set_value('style', ''));
        $this->data['last'] = generate_options($this->lasts, 0, 0, set_value('last', ''));
        $this->data['order_status'] = generate_options($this->order_status, 0, 0, set_value('order_status', ''));
        $this->data['orders'] = $result['rows'];

        $this->latest_shipping_billing_address($this->user['user_id']);
        $this->data['kickstarter'] = false;

        $this->load->view('admin/new', $this->data);
    }

    public function kso()
    {
        $config = array();
        $config['per_page'] = 450;
        $config['page'] = 0;
        $this->data['form'] = 'kso';
        $filters = $this->input->post();
        $order_source = 'kick starter';
        $result = $this->AdminOrderModel->getProcessedOrders($config, $order_source, $filters);

        $styles = $this->AdminOrderModel->get_styles();
        $types = $this->AdminOrderModel->simple_get('custom_collection_types', array('id', 'name'), array('style_name' => $styles[0]['name']), 'order');
        $users = $this->AdminOrderModel->simple_get('users', array('user_id id', 'concat(first_name," ",last_name, " - ",email) name'), '', 'first_name');

        $this->data['edit']['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['edit']['styles'] = generate_options($styles);
        $this->data['edit']['types'] = generate_options($types);
        $this->data['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['style'] = generate_options($this->styles, 0, 0, set_value('style', ''));
        $this->data['last'] = generate_options($this->lasts, 0, 0, set_value('last', ''));
        $this->data['order_status'] = generate_options($this->order_status, 0, 0, set_value('order_status', ''));
        $this->data['orders'] = $result['rows'];

        $this->latest_shipping_billing_address($this->user['user_id']);
        $this->data['kickstarter'] = true;
        $this->load->view('admin/new', $this->data);
    }

    public function partner()
    {
        $config = array();
        $config['per_page'] = 450;
        $config['page'] = 0;
        $this->data['form'] = 'partner';
        $filters = $this->input->post();
        $order_source = 'partner';
        $result = $this->AdminOrderModel->getProcessedOrders($config, $order_source, $filters);
        $partner = $this->PartnerModel->get_partners();

        $styles = $this->AdminOrderModel->get_styles();
        $types = $this->AdminOrderModel->simple_get('custom_collection_types', array('id', 'name'), array('style_name' => $styles[0]['name']), 'order');
        $users = $this->AdminOrderModel->simple_get('users', array('user_id id', 'concat(first_name," ",last_name, " - ",email) name'), '', 'first_name');

        $this->data['edit']['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['edit']['styles'] = generate_options($styles);
        $this->data['edit']['types'] = generate_options($types);
        $this->data['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['style'] = generate_options($this->styles, 0, 0, set_value('style', ''));
        $this->data['last'] = generate_options($this->lasts, 0, 0, set_value('last', ''));
        $this->data['order_status'] = generate_options($this->order_status, 0, 0, set_value('order_status', ''));
        $this->data['partner_name'] = generate_options($partner, 0, 0, set_value('partner_id', ''));
        $this->data['orders'] = $result['rows'];

        $this->latest_shipping_billing_address($this->user['user_id']);
        $this->data['kickstarter'] = false;
        $this->load->view('admin/new', $this->data);
    }

    public function spring()
    {
        $config = array();
        $config['per_page'] = 450;
        $config['page'] = 0;
        $this->data['form'] = 'spring';
        $filters = $this->input->post();
        $order_source = "spring";

        $result = $this->AdminOrderModel->getProcessedOrders($config, $order_source, $filters);
        $styles = $this->AdminOrderModel->get_styles();
        $types = $this->AdminOrderModel->simple_get('custom_collection_types', array('id', 'name'), array('style_name' => $styles[0]['name']), 'order');
        $users = $this->AdminOrderModel->simple_get('users', array('user_id id', 'concat(first_name," ",last_name, " - ",email) name'), '', 'first_name');

        $this->data['edit']['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['edit']['styles'] = generate_options($styles);
        $this->data['edit']['types'] = generate_options($types);
        $this->data['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['style'] = generate_options($this->styles, 0, 0, set_value('style', ''));
        $this->data['last'] = generate_options($this->lasts, 0, 0, set_value('last', ''));
        $this->data['partner_name'] = generate_options($this->lasts, 0, 0, set_value('partner_name', ''));
        $this->data['order_status'] = generate_options($this->order_status, 0, 0, set_value('order_status', ''));
        $this->data['orders'] = $result['rows'];

        $this->latest_shipping_billing_address($this->user['user_id']);
        $this->data['kickstarter'] = false;
        $this->load->view('admin/new', $this->data);
    }

    public function rtw()
    {
        $config = array();
        $config['per_page'] = 450;
        $config['page'] = 0;
        $this->data['form'] = 'rtw';
        $filters = $this->input->post();
        $order_source = 'rtw';
        $result = $this->AdminOrderModel->getProcessedOrders($config, $order_source, $filters);
        $partner = $this->PartnerModel->get_partners();

        $styles = array(
            array('id' => 'oxford', 'name' => 'Oxford'),
            array('id' => 'derby', 'name' => 'Derby'),
            array('id' => 'monkstrap', 'name' => 'Monkstrap'),
            array('id' => 'chukka', 'name' => 'Chukka'),
        );
        $types = array(array('id' => '1', 'name' => 'The Ready to Wear'));
        $users = $this->AdminOrderModel->simple_get('users', array('user_id id', 'concat(first_name," ",last_name, " - ",email) name'), '', 'first_name');

        $this->data['edit']['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['edit']['styles'] = generate_options($styles);
        $this->data['edit']['types'] = generate_options($types);
        $this->data['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['style'] = generate_options($styles, 0, 0, set_value('style', ''));
        $this->data['last'] = generate_options($types, 0, 0, set_value('last', ''));
        $this->data['order_status'] = generate_options($this->order_status, 0, 0, set_value('order_status', ''));
        $this->data['orders'] = $result['rows'];

        $this->latest_shipping_billing_address($this->user['user_id']);
        $this->data['kickstarter'] = false;
        $this->load->view('admin/new', $this->data);
    }

    public function update_summary($order_id)
    {
        $config = array();
        $config['per_page'] = 1;
        $config['page'] = 0;
        $result = $this->AdminOrderModel->getProcessedOrders($config, '', array('order_number' => $order_id));
        $data['orders'] = $result['rows'];
        $data['kickstarter'] = $result['rows'][0]['kickstarter_no'] == 0 || $result['rows'][0]['kickstarter_no'] == "" || !isset($result['rows'][0]['kickstarter_no']) ? 0 : 1;
        $this->load->view('admin/order_row', $data);
    }

    private function latest_shipping_billing_address($user_id)
    {
        $this->data['billing'] = $this->AdminOrderModel->simple_get('address_details', '*,', array('is_billing' => 1, 'user_id' => $user_id));
        $this->data['shipping'] = $this->AdminOrderModel->simple_get('address_details', '*, phone as telephone ,first_name as firstname, last_name as lastname', array('is_shipping' => 1, 'user_id' => $user_id));
    }

    public function create_order()
    {
        $this->session->unset_userdata('create_order_id');
        $styles = $this->AdminOrderModel->get_styles();
        $types = $this->AdminOrderModel->simple_get('custom_collection_types', array('id', 'name'), array('style_name' => $styles[0]['name']), 'order');
        $users = $this->AdminOrderModel->simple_get('users', array('user_id id', 'concat(first_name," ",last_name, " - ",email) name'), '', 'first_name');
        $rtw_styles = array(
            array('id' => 'oxford', 'name' => 'Oxford'),
            array('id' => 'derby', 'name' => 'Derby'),
            array('id' => 'monkstrap', 'name' => 'Monkstrap'),
            array('id' => 'chukka', 'name' => 'Chukka'),
        );
        $this->data['edit']['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $this->data['edit']['styles'] = generate_options($styles);
        $this->data['edit']['rtw_styles'] = generate_options($rtw_styles);
        $this->data['edit']['types'] = generate_options($types);

        $this->data['style'] = generate_options($this->styles, 0, 0, set_value('style', ''));
        $this->data['last'] = generate_options($this->lasts, 0, 0, set_value('last', ''));
        $this->data['order_status'] = generate_options($this->order_status, 0, 0, set_value('order_status', ''));
        $this->data['orders'] = $result['rows'];

        $this->latest_shipping_billing_address($this->user['user_id']);

        $this->load->view('admin/order_create', $this->data);
    }

    public function shipping_billing_address($user_id)
    {
        $this->latest_shipping_billing_address($user_id);
        $data['billing'] = $this->load->view('admin/order_billing_address', $this->data, true);
        $data['shipping'] = $this->load->view('admin/order_shipping_address', $this->data, true);
        echo json_encode($data);
    }

    public function order_details()
    {
        $order_id = $this->input->post('order_id');
        $data = $this->OrderModel->getbillingNshippingDetails($order_id);
        $data['order_summary'] = $this->OrderModel->getOrderDet($order_id);

        if ($data['order_summary'][0]['store_credit_trans_id'] !== null && $data['order_summary'][0]['card_amount']) {
            $data['order_summary'][0]['giftcard'] = true;
        } else {
            $data['order_summary'][0]['giftcard'] = false;
        }
        $totalamt = 0;

        for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
            $totalamt += $data['order']['items'][$j]['quantity'] * $data['order']['items'][$j]['item_amt'];

            $shoedesign = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);
            $shoemodel = json_decode($shoedesign['shoemodel'], true);

            $a = $shoemodel['measurement'];
            $b = $data['order']['items'][$j];
            $data['order']['items'][$j]['measurement'] = array(
                'left' => ['size' => $b['m_left_size'], 'width' => $b['m_left_width'], 'girth' => $b['m_lgirth'], 'instep' => $b['m_linstep'], 'heel' => (isset($a['left']['heel']) ? $a['left']['heel'] : '0'), 'ankle' => (isset($a['left']['ankle']) ? $a['left']['ankle'] : '0')],
                'right' => ['size' => $b['m_right_size'], 'width' => $b['m_right_width'], 'girth' => $b['m_rgirth'], 'instep' => $b['m_rinstep'], 'heel' => (isset($a['right']['heel']) ? $a['right']['heel'] : '0'), 'ankle' => (isset($a['right']['ankle']) ? $a['right']['ankle'] : '0')],
                'size_type' => (isset($a['size_type']) ? $a['size_type'] : 'CM'),
            );

            $data['order']['items'][$j]['shoedesign'] = $shoedesign;
            $data['order']['items'][$j]['can_show_details'] = 1;
            if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0" && $data['order']['items'][$j]['last_id'] != '6') {
                $data['order']['items'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
            }
            if ($data['order']['items'][$j]['last_id'] == '6') {
                $data['order']['items'][$j]['shop_images'] = $this->OrderModel->get_readytowear_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['last_name'] = 'The Ready To Wear';
                $data['order']['items'][$j]['style_name'] = $this->OrderModel->get_readytowear_style($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['can_show_details'] = 0;
            }
            $data['order']['items'][$j]['is_remake_request_sent'] = !$this->OrderModel->is_remake_request_sent($order_id, $data['order']['items'][$j]['design_id']);
            $data['remake_instruction'] = $this->OrderModel->get_remake_instruction($order_id, $data['order']['items'][$j]['design_id']);
            $data['order']['items'][$j]['details']['left_shoe'] = generate_options($this->var_size, 0, 0, @$data['order']['items'][$j]['left_shoe']);
            $data['order']['items'][$j]['details']['right_shoe'] = generate_options($this->var_size, 0, 0, @$data['order']['items'][$j]['right_shoe']);
            $data['order']['items'][$j]['details']['left_width'] = generate_options($this->var_width, 0, 0, @$data['order']['items'][$j]['left_width']);
            $data['order']['items'][$j]['details']['right_width'] = generate_options($this->var_width, 0, 0, @$data['order']['items'][$j]['right_width']);
            $data['order']['items'][$j]['details']['last_id'] = $data['order']['items'][$j]['last_id'];
        }

        $data['vendors'] = $this->AdminViewModel->getVendors();
        $data['status'] = $this->AdminViewModel->getStatus($order_id);
        $status = $this->AdminViewModel->getStatus($order_id);
        $data['vendor'] = $this->UserModel->get_user_details($status[0]['user_id']);

        $discountPerc = $data['order_summary'][0]['discount_perc'];
        $disc = 0.00;

        if ($discountPerc != 0) {
            $data['disc'] = ($totalamt * $discountPerc) / 100;
            $netamt = $totalamt - $disc;
        } else {
            $netamt = $totalamt;
            $data['disc'] = 0;
        }

        $styles = $this->AdminOrderModel->get_styles();
        $types = $this->AdminOrderModel->simple_get('custom_collection_types', array('id', 'name'), array('style_name' => $styles[0]['name']), 'order');
        $users = $this->AdminOrderModel->simple_get('users', array('user_id id', 'concat(first_name," ",last_name) name'));

        $data['edit']['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $data['edit']['styles'] = generate_options($styles);
        $data['edit']['types'] = generate_options($types);
        $rtw_styles = array(
            array('id' => 'oxford', 'name' => 'Oxford'),
            array('id' => 'derby', 'name' => 'Derby'),
            array('id' => 'monkstrap', 'name' => 'Monkstrap'),
            array('id' => 'chukka', 'name' => 'Chukka'),
        );
        $data['edit']['rtw_styles'] = generate_options($rtw_styles);
        $data['shipping_cost'] = 0;
        $data['total_amount'] = number_format($totalamt, 2);
        $data['tax'] = $data['order_summary'][0]['tax_amount'];
        $data['discount_type'] = $data['order_summary'][0]['giftcard'] == true ? 'GIFT CARD' : 'PROMOTION';
        $data['grand_total'] = number_format($netamt + $data['order_summary'][0]['tax_amount'] - $data['disc'], 2);
        $data['netamt'] = number_format($netamt - $data['disc'], 2);

        $this->data = $data;
        $this->load->view('admin/order_details', $this->data);
    }

    public function remake_shoe()
    {
        $order_id = $this->input->post('order_id');
        $shoe_design_id = $this->input->post('design_id');
        $vendor_id = $this->input->post('vendor_id');
        $user_id = $this->input->post('user_id');
        $comment = $this->input->post('comment');
        $recieved_date = $this->input->post('recieved_date');

        $order_summary = $this->OrderModel->getOrderDet($order_id);
        $order_details = $this->OrderModel->getbillingNshippingDetails($order_id);

        $items = $order_details['order']['items'];
        foreach ($items as $item) {
            if ($item['design_id'] == $shoe_design_id) {
                $order_items['design_id'] = $shoe_design_id;
                $order_items['item_id'] = $item['item_id'];
                $order_items['item_name'] = $item['item_name'];
                $order_items['left_shoe'] = $item['left_shoe'];
                $order_items['right_shoe'] = $item['right_shoe'];
                $order_items['item_amt'] = 0;
                $order['items'][] = $order_items;
            }
        }
        $order_main['invoice_no'] = $this->OrderModel->getinvoiceno();
        $order_main['user_id'] = $order_summary[0]['user_id'];
        $order_main['currency'] = 'USD';
        $order_main['gross_amt'] = 0;
        $order_main['user_agent'] = $this->input->user_agent();
        $order_main['discount'] = 0;
        $order_main['discount_perc'] = 0;
        $order_main['order_modified'] = date('Y-m-d H:i:s');
        $order_main['payment_status'] = 'Processed';
        $order_main['order_source'] = $order_summary[0]['order_source'];
        $order_main['comments'] = $comment;
        $order['order_main'] = $order_main;

        $new_order_id = $this->OrderModel->saveorder($order);

        $data = array();
        $billing_details = $order_details['billing'][0];
        $shipping_details = $order_details['shipping'][0];
        $billing = array(
            'order_id' => $new_order_id,
            'user_id' => $billing_details['user_id'],
            'address1' => $billing_details['address1'],
            'address2' => $billing_details['address2'],
            'city' => $billing_details['city'],
            'state' => $billing_details['state'],
            'country' => $billing_details['country'],
            'zipcode' => $billing_details['zipcode'],
            'date_created' => date('Y-m-d H:i:s'),
        );
        $shipping = array(
            'order_id' => $new_order_id,
            'user_id' => $shipping_details['user_id'],
            'firstname' => $shipping_details['firstname'],
            'lastname' => $shipping_details['lastname'],
            'email' => $shipping_details['email'],
            'telephone' => $shipping_details['telephone'],
            'address1' => $shipping_details['address1'],
            'address2' => $shipping_details['address2'],
            'city' => $shipping_details['city'],
            'state' => $shipping_details['state'],
            'country' => $shipping_details['country'],
            'zipcode' => $shipping_details['zipcode'],
            'date_created' => date('Y-m-d H:i:s'),
        );
        $data['billing'] = $billing;
        $data['shipping'] = $shipping;

        $this->OrderModel->savebilling($data);
        $this->OrderModel->insertorderStatus($new_order_id);
        $order_summary = $this->OrderModel->getOrderDet($new_order_id);
        $this->AdminViewModel->assignToVendor($new_order_id, $vendor_id, $order_summary[0]['invoice_no']);

        $details = array('order_id' => $order_id, 'new_order_id' => $new_order_id, 'shoe_design_id' => $shoe_design_id, 'comments' => $comment);
        $this->OrderModel->insertRemakeDetails($details);
        redirect('admin/new', 'refresh');
    }

    public function get_collection()
    {
        $source = $this->input->post('source');
        if ($source === 'custom_collection') {
            $style = $this->input->post('style');
            $style = ($style == 'valentine') ? '' : $style;
            $type = $this->input->post('type');

            $collection = $this->CommonModel->get_custom_collections($style, $type);
            if (is_array($collection)) {
                foreach ($collection as $key => $value) {
                    $attachment_id_array = explode(',', $value['attachment_ids']);
                    $item['attachment_ids'] = $attachment_id_array[0];
                    $image = $this->CommonModel->get_custom_images($item);
                    $collection[$key]['image'] = $image;
                    $collection[$key]['custom_collection_id'] = $value['custom_collection_id'];
                }
            }
        } else if ($source === 'ready_wear') {
            $style = $this->input->post('style');
            $collection = $this->CommonModel->get_readywear_collections($style);
            if (is_array($collection)) {
                foreach ($collection as $key => $value) {
                    $attachment_id_array = explode(',', $value['attachment_ids']);
                    $item['attachment_ids'] = $attachment_id_array[0];
                    $image = $this->CommonModel->get_custom_images($item);
                    $collection[$key]['image'] = $image;
                    $collection[$key]['custom_collection_id'] = $value['ready_wear_id'];
                    $collection[$key]['shoe_name'] = $value['shoe_name'];
                    $collection[$key]['last_name'] = "The Ready To Wear";
                }
            }
        } else {
            $user_id = $this->input->post('user_id');

            $collection = $this->UserModel->get_shoe_designsByUser($user_id, false);
            if (is_array($collection)) {
                foreach ($collection as $key => $value) {
                    $collection[$key]['image'][0] = CustomShoeConfig::DESIGN_BASE . $collection[$key]['image_file'] . '_A0.png';
                    $collection[$key]['shoe_name'] = $collection[$key]['public_name'];

                    $shoedetail = $this->ShoeModel->ge_design_model($collection[$key]['shoe_design_id']);
                    $shoe = json_decode($shoedetail['shoemodel'], true);

                    $collection[$key]['price'] = getPricing($shoe);
                }
            }
        }
        $data['collection'] = $collection;
        $this->load->view('admin/order_shoe_images', $data);
    }

    public function confirm_order()
    {
        $form_data = $this->input->post();
        if (!isset($form_data['order_id']) || $form_data['order_id'] == '' || $form_data['order_id'] == false) {
            $form_data['order_id'] = $this->session->userdata('create_order_id');
        }
        if (isset($form_data['token'])) {
            $this->load->model('Stripe_trans');
            $stripe_result = $this->Stripe_trans->insert($form_data['token'], 'Order - ' . $form_data['order_id'], round($form_data['gross_amt'] * 100, 2));
            if ($stripe_result->paid === true) {
                $this->save_order($form_data);
                echo 1;
            } else {
                echo 0;
            }
        } else {
            $this->save_order($form_data);
            echo 1;
        }
    }

    private function sendOrderSuccessmail($orderId)
    {
        $this->load->library('email');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $users = $this->AdminOrderModel->simple_get('users', array('user_id', 'first_name', 'last_name', 'email'), array('user_id' => $data['order']['items'][0]['user_id']));
        $data['user'] = $users[0];
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        $order_summary = $data['order_summary'][0];
        $data['subtotal'] = $order_summary['gross_amt'];
        $data['discount'] = $order_summary['discount'];
        $data['tax_amount'] = $order_summary['tax_amount'];
        $data['total'] = $order_summary['gross_amt'] - $order_summary['discount'] + $order_summary['tax_amount'];

        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');

        for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
            if ($data['order']['items'][$j]['design_id'] !== 0) {
                $data['shoedesign'][] = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);
                $data['shoedesign'][$j]['can_show_details'] = 1;
                if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0") {
                    $data['shoedesign'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                    $data['shoedesign'][$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
                    // If Ready To Wear Shoe then last_id=6
                    if ($data['order']['items'][$j]['last_id'] == 6) {
                        $data['shoedesign'][$j]['can_show_details'] = 0;
                        $data['shoedesign'][$j]['shop_images'] = $this->OrderModel->get_readytowear_images($data['order']['items'][$j]['item_id']);
                        $data['shoedesign'][$j]['last_name'] = "The Ready To Wear Shoe";
                        $data['shoedesign'][$j]['style_name'] = $this->OrderModel->get_readytowear_style($data['order']['items'][$j]['item_id']);
                    }
                }
                $data['summary'][] = array(
                    'price' => $data['order']['items'][$j]['item_amt'],
                    'left_size' => @$data['order']['items'][$j]['left_shoe'],
                    'right_size' => @$data['order']['items'][$j]['right_shoe'],
                    'left_width' => @$data['order']['items'][$j]['left_width'],
                    'right_width' => @$data['order']['items'][$j]['right_width'],
                    'quantity' => @$data['order']['items'][$j]['quantity'],
                );
            }
        }
        $this->email->to($data['user']['email']);
        $this->email->cc($this->config->item('contact_email'));
        $this->email->subject('Awl & Sundry | Order ' . $orderId . ' Confirmation');
        $msg = $this->load->view('emails/order_confirmation', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    private function save_order($form_data)
    {
        $shipping_address = $form_data['shipping_address'];
        $shipping_address['order_id'] = $form_data['order_id'];
        $billing_address = $form_data['billing_address'];
        $billing_address['order_id'] = $form_data['order_id'];

        $form_data['order_source'] = ($form_data['order_source'] == '') ? 'mto' : $form_data['order_source'];
        $order_data = array('gross_amt' => $form_data['gross_amt'], 'payment_status' => 'Processed', 'comments' => $form_data['comments'], 'order_source' => $form_data['order_source'], 'belt_needed' => $form_data['belt_needed']);

        $this->AdminOrderModel->insert('billing_details', $billing_address);
        $this->AdminOrderModel->insert('shipping_details', $shipping_address);
        $this->AdminOrderModel->update('order', $order_data, array('order_id' => $form_data['order_id']));

        $order_data = $this->AdminOrderModel->simple_get('order', 'invoice_no', array('order_id' => $form_data['order_id']));
        $order_status_data = array('order_id' => $form_data['order_id'], 'invoice_no' => $order_data[0]['invoice_no'], 'status' => 'Order acknowledged', 'add_date' => date('Y-m-d H:i:s'), 'user_id' => $form_data['user_id'], 'status_desc' => 'Order Date : ' . date('Y-m-d H:i:s'), 'is_active' => 1);

        $this->AdminOrderModel->insert('order_status', $order_status_data);
        $this->AdminOrderModel->update('configure', array('last_inserted_no' => $order_data[0]['invoice_no']), array('config_id' => 1));
    }

    public function update_order_details()
    {
        $form_data = $this->input->post();
        if (!isset($form_data['order_id']) || $form_data['order_id'] == '' || $form_data['order_id'] == false) {
            $form_data['order_id'] = $this->session->userdata('create_order_id');
        }
        $summary = $this->OrderModel->order_summary($form_data['order_id']);
        $summary['status'] = ($summary) ? 'success' : 'failure';
        echo json_encode($summary);
    }

    public function vendorAssigning()
    {
        $orderId = $_POST['order_id'];
        $vendorId = $_POST['vendor'];
        $invoiceNo = $_POST['invoice'];
        $status = $_POST['status'];
        $trackingNo = $_POST['trackingNo'];
        $carrier = $_POST['carrier'];
        $shippingDate = $_POST['shippingDate'];
        $deliveryDate = $_POST['deliveryDate'];
        $refundamount = $_POST['refundamount'];
        $refundreason = $_POST['refundreason'];

        if ($status == "Refund") {
            $this->refund($orderId, $invoiceNo, $refundamount, $refundreason);
        } else if ($status == "Testing") {
            $this->testing($orderId, $invoiceNo);
        } else if ($status == "Cancelled") {
            $this->cancelled($orderId, $invoiceNo);
        } else if ($status == "Order Shipped") {
            $this->orderShipped($orderId, $invoiceNo, $trackingNo, $carrier, $shippingDate, $deliveryDate);
        } else if ($status == "Manufacturing in progress") {
            if ($this->AdminViewModel->assignToVendor($orderId, $vendorId, $invoiceNo)) {
                $this->sendOrdermailtoManufacturer($orderId, $vendorId);
            }
        }
    }

    public function refund($orderId, $invoiceNo, $refundamount, $refundreason)
    {
        if ($this->AdminViewModel->refund($orderId, $invoiceNo, $refundamount, $refundreason)) {

        }
    }

    public function testing($orderId, $invoiceNo)
    {
        if ($this->AdminViewModel->testing($orderId, $invoiceNo)) {

        }
    }

    public function cancelled($orderId, $invoiceNo)
    {
        if ($this->AdminViewModel->cancelled($orderId, $invoiceNo)) {

        }
    }

    public function orderShipped($orderId, $invoiceNo, $trackingNo, $carrier, $shippingDate, $deliveryDate)
    {
        if ($this->AdminViewModel->orderShipped($orderId, $invoiceNo, $trackingNo, $carrier, $shippingDate, $deliveryDate)) {

        }
    }

    public function sendOrdermailtoManufacturer($orderId, $vendorId)
    {
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);

        $this->load->library('email');
        $this->load->model('OrderModel');

        $allVendors = $this->db->where(" (user_id=" . $vendorId . " OR parent_id=" . $vendorId . ") AND user_type_id=3 AND is_enabled=1 ", null, false)
            ->get('users')->result_array();

        foreach ($allVendors as $val) {
            $vendor = [$val];

            $to = $vendor[0]['email'];
            $data['vendor'] = $vendor;

            $this->email->set_newline("\r\n");
            $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
            $this->email->to('info@robertshoes.it');
            $this->email->cc($this->config->item('contact_email'));
            $this->email->subject('Awl & Sundry Order ' . $orderId);

            $msg = $this->load->view('orderassignedTovendorEmail', $data, true);

            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
        }
    }

    public function save_custom_item()
    {
        $form_data = $this->input->post(); // attachment_ids, price, style_name, last_id, customer, action
        $form_data['is_editable'] = 0;
        $form_data['type_id'] = 1000;
        $customer = $form_data['customer'];
        $action = $form_data['action'];
        unset($form_data['customer']); // exclude customer from form_data to insert in custom_collection
        unset($form_data['action']);
        $custom_collection_id = $this->AdminOrderModel->insert('custom_collection', $form_data);

        $data = array();
        $shoe_design_id = 1000;
        $data['item'] = $this->ShoeModel->get_shoe_design_details($shoe_design_id, 0);
        $data['item']['shop_images'] = $this->OrderModel->get_shop_images($custom_collection_id);
        $data['item']['can_show_details'] = (int) $this->OrderModel->can_show_details($custom_collection_id);
        $data['item']['custom_collection_id'] = $custom_collection_id;
        $order_details = array();
        if ($action == 'edit' && $design_id !== false) {
            $order_details = $this->AdminOrderModel->simple_get('order_details', '*', array('order_id' => $order_id, 'design_id' => $design_id));
            $data['edit'] = true;
        } else {
            $order_details = $this->OrderModel->get_latest_order_details($customer);
            $data['edit'] = false;
        }
        if ($order_details) {
            $data['item']['details'] = $order_details[0];
        }
        $size = $this->var_size;
        $width = $this->var_width;
        $data['item']['item_amt'] = $form_data['price'];
        $data['item']['details']['left_shoe'] = generate_options($size, 0, 0, @$data['item']['details']['left_shoe']);
        $data['item']['details']['right_shoe'] = generate_options($size, 0, 0, @$data['item']['details']['right_shoe']);
        $data['item']['details']['left_width'] = generate_options($width, 0, 0, @$data['item']['details']['left_width']);
        $data['item']['details']['right_width'] = generate_options($width, 0, 0, @$data['item']['details']['right_width']);

        $this->load->view('admin/order_item_details', $data);
    }

    public function edit_comment($order_id)
    {
        $form_data = $this->input->post();
        $this->AdminOrderModel->update('order', $form_data, array('order_id' => $order_id));
    }

    public function get_types()
    {
        $style = $this->input->post('style');
        $types = $this->AdminOrderModel->simple_get('custom_collection_types cc', array('id', 'name'), array('style_name' => $style), 'cc.order');
        echo generate_options($types);
    }

    public function get_item_details()
    {
        $source = $this->input->post('source');
        $order_id = $this->input->post('order_id');
        $shoe_design_id = $this->input->post('shoe_design_id');
        $design_id = $this->input->post('design_id');
        $action = $this->input->post('action');

        if ($action == 'edit' && $design_id !== false) {
            $order_details = $this->AdminOrderModel->simple_get('order_details', '*', array('order_id' => $order_id, 'design_id' => $design_id));
            $data['edit'] = true;
        } else {
            $order_details = $this->AdminOrderModel->simple_get('order_details', '*', array('order_id' => $order_id));
            $data['edit'] = false;
        }
        $data['item'] = $this->ShoeModel->get_shoe_design_details($shoe_design_id, 0);
        $tempData = json_decode($data['item']['shoemodel'], true);

        $data['item']['soleId'] = $tempData['sole']['id'];
        $soleName = $this->db->get_where('style_sole', array('id' => $tempData['sole']['id']))->row('sole_name');
        if (!is_array($soleName) && $soleName != '' && $soleName != null) {
            $data['item']['soleName'] = (!is_array($soleName) && $soleName != '' && $soleName != null) ? $soleName : '';
        }
        $data['item']['soleCode'] = $tempData['sole']['code'];
        $data['item']['soleColorId'] = $tempData['sole']['colorId'];
        $data['item']['soleColorCode'] = $tempData['sole']['color'];
        $soleColorName = $this->db->get_where('style_sole_color', array('id' => $tempData['sole']['colorId']))->row('color_name');
        if (!is_array($soleColorName) && $soleColorName != '' && $soleColorName != null) {
            $data['item']['soleColorName'] = (!is_array($soleColorName) && $soleColorName != '' && $soleColorName != null) ? $soleColorName : '';
        }
        $data['item']['hasPatina'] = (isset($tempData['patina'])) ? '1' : '0';
        $data['item']['patinaImg'] = (isset($tempData['patina'])) ? $data['item']['image_file'] . '_' . $tempData['patina']['color'] . '.jpg' : '';
        $data['item']['patina_material'] = (isset($tempData['patina'])) ? $this->db->get_where('material_def', array('material_def_id' => $tempData['patina']['matId']))->row('material_name') : '';
        $data['item']['patina_color'] = (isset($tempData['patina'])) ? $this->db->get_where('material_colors', array('material_color_id' => $tempData['patina']['clrId']))->row('color_name') : '';
        unset($data['item']['shoemodel']);

        $data['item']['can_show_details'] = 1;
        $data['item']['item_amt'] = $this->input->post('price');
        $data['item']['is_readywear'] = 0;
        $data['item']['details'] = array();
        if ($source === 'custom_collection') {
            $custom_collection_id = $this->input->post('custom_collection_id');
            $data['item']['shop_images'] = $this->OrderModel->get_shop_images($custom_collection_id);
            $data['item']['can_show_details'] = (int) $this->OrderModel->can_show_details($custom_collection_id);
            $data['item']['custom_collection_id'] = $custom_collection_id;
        }
        if ($source === 'ready_wear') {
            $ready_wear_id = $this->input->post('custom_collection_id');
            $data['item'] = $this->CommonModel->get_ready_wear_item($ready_wear_id);
            $data['item']['shop_images'] = $this->OrderModel->get_readytowear_images($ready_wear_id);
            $data['item']['can_show_details'] = 0;
            $data['item']['ready_wear_id'] = $ready_wear_id;
            $data['item']['last_name'] = "The Ready To Wear";
            $data['item']['can_show_details'] = 0;
            $data['item']['is_readywear'] = 1;
            $data['item']['item_amt'] = $data['item']['price'];
            $data['item']['custom_collection_id'] = $ready_wear_id;
            $data['item']['details'] = array();
        }

        if ($order_details) {
            $data['item']['details'] = $order_details[0];
        }

        $size = $this->var_size;
        $width = $this->var_width;

        $data['item']['details']['left_shoe'] = generate_options($size, 0, 0, @$data['item']['details']['left_shoe']);
        $data['item']['details']['right_shoe'] = generate_options($size, 0, 0, @$data['item']['details']['right_shoe']);
        $data['item']['details']['left_width'] = generate_options($width, 0, 0, @$data['item']['details']['left_width']);
        $data['item']['details']['right_width'] = generate_options($width, 0, 0, @$data['item']['details']['right_width']);

        $this->load->view('admin/order_item_details', $data);
    }

    public function order_add_item()
    {
        $form_data = $this->input->post();

        if (!isset($form_data['order_id']) || $form_data['order_id'] == '') {
            $form_data['order_id'] = $this->session->userdata('create_order_id');
        }
        $action = $form_data['action'];
        $rtw = $form_data['is_readywear'];
        if ($form_data['custom_collection_id'] == '') {
            $shoedetail = $this->ShoeModel->ge_design_model($form_data['design_id']);
            $shoemodel = json_decode($shoedetail['shoemodel'], true);
            $shoemodel['reDesignId'] = $form_data['design_id'];
            $shoemodel['angle'] = 'A0';
            if (!isset($shoemodel['style']['isLaceEnabled'])) {
                $model = $this->CustomShoeModel->get_style_defaults($shoemodel['style']['id']);
                $shoemodel['style']['isLaceEnabled'] = $model['style']['isLaceEnabled'];
            }
            $shoemodel['monogram'] = array('text' => $form_data['monogram'], 'leftSide' => 1, 'leftShoe' => 1, 'rightShoe' => 0, 'rightSide' => 0);
            $shoemodel['monogram']['leftSide'] = 1;
            $shoe = $this->ShoeModel->savemodel($this->user['user_id'], $shoemodel);
            $form_data['design_id'] = $shoe['shoe_design_id'];
        } else {
            $shoe = $this->ShoeModel->save_custom_model($this->user['user_id'], $form_data['design_id']);
            if (isset($form_data['monogram']) && $form_data['monogram'] != '') {
                $shoemodel['monogram'] = array('text' => $form_data['monogram'], 'leftSide' => 1, 'leftShoe' => 1, 'rightShoe' => 0, 'rightSide' => 0);
                $this->ShoeModel->insert_monogram($shoe['shoe_design_id'], $shoemodel['monogram']);
            }
            $form_data['design_id'] = $shoe['shoe_design_id'];
        }
        $form_data['item_id'] = $form_data['custom_collection_id'];
        unset($form_data['custom_collection_id']);
        unset($form_data['is_readywear']);
        unset($form_data['monogram']);
        unset($form_data['action']);

        if ($action == 'edit') {
            $old_design_id = $form_data['old_design_id'];
            unset($form_data['old_design_id']);
            $this->AdminOrderModel->update('order_details', $form_data, array('order_id' => $form_data['order_id'], 'design_id' => $old_design_id));
        } else {
            if (isset($form_data['order_id']) && $form_data['order_id'] !== '' && $form_data['order_id'] !== false) { // add item in an existing order
                unset($form_data['customer']);
                $this->AdminOrderModel->insert('order_details', $form_data);
            } else {
                if (isset($form_data['customer'])) { // if new order, insert order and put order_d into session
                    $order_data['invoice_no'] = $this->OrderModel->getinvoiceno();
                    $order_data['user_id'] = $form_data['customer'];
                    $order_data['currency'] = 'USD';
                    $order_data['gross_amt'] = 0;
                    $order_data['discount_perc'] = 0;
                    $order_data['discount'] = 0;
                    $order_data['tax_amount'] = 0;
                    $order_data['order_modified'] = date('Y-m-d H:i:s');
                    $order_data['payment_status'] = 'Not Processed';
                    $order_data['comments'] = '';
                    $order_data['user_agent'] = $this->input->user_agent();
                    $order_data['promocode_id'] = 0;
                    $form_data['order_id'] = $this->AdminOrderModel->insert('order', $order_data);
                    $this->session->set_userdata('create_order_id', $form_data['order_id']);
                    unset($form_data['customer']);
                    $this->AdminOrderModel->insert('order_details', $form_data);
                }
            }
        }

        $amount = $this->AdminOrderModel->simple_get('order_details', 'sum(item_amt * quantity) total_amount', array('order_id' => $form_data['order_id']));
        $gross_amount = $amount['0']['total_amount'];

        $this->AdminOrderModel->update('order', array('gross_amt' => $gross_amount), array('order_id' => $form_data['order_id']));

        $order = $this->OrderModel->getbillingNshippingDetails($form_data['order_id']);
        $i = 0;
        foreach ($order['order']['items'] as $key => $item) {
            if ($form_data['design_id'] == $order['order']['items'][$key]['design_id']) {
                $i = $key;
                break;
            }
        }
        $data['order']['items'][$key] = $order['order']['items'][$key];
        $data['order']['items'][$key]['is_readywear'] = $rtw;
        if ($rtw) {
            $data['order']['items'][$key]['can_show_details'] = 0;
            if ($form_data['item_id'] !== "") {
                $data['order']['items'][$key]['shop_images'] = $this->OrderModel->get_readytowear_images($form_data['item_id']);
                $data['order']['items'][$key]['can_show_details'] = 0;
                $data['order']['items'][$key]['last_name'] = 'The Ready To Wear';
                $data['order']['items'][$key]['style_name'] = $this->OrderModel->get_readytowear_style($form_data['item_id']);
            }
            $styles = array(
                array('id' => 'oxford', 'name' => 'Oxford'),
                array('id' => 'derby', 'name' => 'Derby'),
                array('id' => 'monkstrap', 'name' => 'Monkstrap'),
                array('id' => 'chukka', 'name' => 'Chukka'),
            );
        } else {
            $data['order']['items'][$key]['shoedesign'] = $this->ShoeModel->get_shoe_design_details($form_data['design_id'], 0);
            $data['order']['items'][$key]['can_show_details'] = 1;
            if ($form_data['item_id'] !== "") {
                $data['order']['items'][$key]['shop_images'] = $this->OrderModel->get_shop_images($form_data['item_id']);
                $data['order']['items'][$key]['can_show_details'] = (int) $this->OrderModel->can_show_details($form_data['item_id']);
            }
            $styles = $this->AdminOrderModel->get_styles();
            $types = $this->AdminOrderModel->simple_get('custom_collection_types', array('id', 'name'), array('style_name' => $styles[0]['name']), 'order');
            $data['edit']['types'] = generate_options($types);
        }
        $users = $this->AdminOrderModel->simple_get('users', array('user_id id', 'concat(first_name," ",last_name) name'));

        $data['edit']['users'] = generate_options($users, 0, 0, $this->user['user_id']);
        $data['edit']['styles'] = generate_options($styles);
        $data['edit']['types'] = generate_options($types);

        $this->load->view('admin/order_items', $data);
    }

    public function upload()
    {
        $allowedExtensions = array();
        $sizeLimit = 2 * 1024 * 1024;
        require 'vendor/uploader/php.php';
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
        $result = $uploader->handleUpload('files/custom/order/');
        $result['attachment_id'] = $this->AdminOrderModel->insert('attachments', array('name' => '', 'file_name' => str_replace('files/', '', $result['file_name']), 'file_type' => $result['ext']));
        echo json_encode($result);
    }

    public function edit_shipping_address()
    {
        $form_data = $this->input->post();
        $order_id = $form_data['order_id'];
        unset($form_data['order_id']);
        $this->AdminOrderModel->update('shipping_details', $form_data, array('order_id' => $order_id));
        echo $this->db->last_query();
    }

    public function edit_billing_address()
    {
        $form_data = $this->input->post();
        $order_id = $form_data['order_id'];
        unset($form_data['order_id']);
        $this->AdminOrderModel->update('billing_details', $form_data, array('order_id' => $order_id));
    }

    public function delete_item()
    {
        $order_id = $this->input->post('order_id');
        $design_id = $this->input->post('design_id');
        echo $this->AdminOrderModel->delete('order_details', array('order_id' => $order_id, 'design_id' => $design_id));
    }

    public function edit_details($design_id, $order_id = 0)
    {

        $order_id = ($order_id == 0) ? $this->session->userdata('create_order_id') : $order_id;

        $form_data = $this->input->post();
        $shoemodel['monogram']['text'] = '';
        $form_data['design_id'] = $design_id;

        if (isset($form_data['monogram']) && $form_data['monogram'] != '') {
            $shoedetail = $this->ShoeModel->ge_design_model($design_id);
            $shoemodel = json_decode($shoedetail['shoemodel'], true);
            $shoemodel['monogram'] = array('text' => $form_data['monogram'], 'leftSide' => 1, 'leftShoe' => 1, 'rightShoe' => 0, 'rightSide' => 0);
            $shoe = $this->ShoeModel->savemodel('', $shoemodel);
            $form_data['design_id'] = $shoe['shoe_design_id'];
        }
        unset($form_data['monogram']);

        $tmpData = $this->db->get_where('shoe_design', ['shoe_design_id' => $design_id])->row('shoemodel');
        $tmpData = json_decode($tmpData, true);

        $tmpData['measurement'] = array(
            'left' => array(
                'size' => $form_data['m_left_size'],
                'width' => $form_data['m_left_width'],
                'girth' => $form_data['m_lgirth'],
                'instep' => $form_data['m_linstep'],
                'heel' => $form_data['m_lheel'],
                'ankle' => $form_data['m_lankle'],
            ),
            'right' => array(
                'size' => $form_data['m_right_size'],
                'width' => $form_data['m_right_width'],
                'girth' => $form_data['m_rgirth'],
                'instep' => $form_data['m_rinstep'],
                'heel' => $form_data['m_rheel'],
                'ankle' => $form_data['m_rankle'],
            ),
            'size_type' => $form_data['size_type'],
        );

        unset($form_data['size_type']);
        unset($form_data['m_lheel']);
        unset($form_data['m_rheel']);
        unset($form_data['m_lankle']);
        unset($form_data['m_rankle']);

        $tmpData = json_encode($tmpData);
        $this->AdminOrderModel->update('shoe_design', ['shoemodel' => $tmpData], array('shoe_design_id' => $design_id));

        $this->AdminOrderModel->update('order_details', $form_data, array('order_id' => $order_id, 'design_id' => $design_id));
        $order_data = $this->AdminOrderModel->simple_get('order_details', 'sum(item_amt) gross_amount', array('order_id' => $order_id));
        $gross_amount = $order_data[0]['gross_amount'];
        $this->AdminOrderModel->update('order', array('gross_amt' => $gross_amount), array('order_id' => $order_id));
        $order_summary = $this->AdminOrderModel->simple_get('order', 'gross_amt,discount_perc,tax_amount', array('order_id' => $order_id));
        $order_summary[0]['monogram'] = $shoemodel['monogram']['text'];
        $order_summary[0]['design_id'] = $form_data['design_id'];
        echo json_encode($order_summary[0]);
    }

    public function edit_status($status)
    {
        $form_data = $this->input->post();
        $order_id = $form_data['order_id'];
        unset($form_data['order_id']);
        $this->AdminOrderModel->update('order_status', array('status' => $status), array('order_id' => $order_id));
        echo $status;
    }

    public function updateOrderStatus($data)
    {
        $this->db->set('is_active', 0);
        $this->db->where('order_id', $data['order_id']);
        $this->db->update('order_status');
        $this->db->insert('order_status', $data);
        echo "1";
    }

    public function edit_user()
    {
        $form_data = $this->input->post();
        $order_id = $form_data['order_id'];
        unset($form_data['order_id']);
        $user_data = $this->AdminOrderModel->simple_get('users', 'user_id', array('email' => $form_data['email']));

        if (!$user_data) {
            $form_data['user_type_id'] = 2;
            $form_data['password'] = md5('password');
            $form_data['username'] = $form_data['email'];
            $form_data['customer_category'] = 'Customer';
            $form_data['is_enabled'] = 1;
            $form_data['date_created'] = date('Y-m-d H:i:s');
            $user_id = $this->AdminOrderModel->insert('users', $form_data);
        } else {
            $user_id = $user_data[0]['user_id'];
        }

        $this->AdminOrderModel->edit('order', array('user_id' => $user_id), array('order_id' => $order_id));
        $this->AdminOrderModel->edit('billing_details', array('user_id' => $user_id), array('order_id' => $order_id));
        $this->AdminOrderModel->edit('shipping_details', array('user_id' => $user_id), array('order_id' => $order_id));
    }

    public function apply_giftcard()
    {
        $order_id = $this->input->post('order_id');
        $giftcard = $this->input->post('giftcard');
        $user_data = $this->AdminOrderModel->simple_get('order', 'user_id', array('order_id' => $order_id));
        $user_id = $user_data[0]['user_id'];
        $user_data = $this->AdminOrderModel->simple_get('users', 'email', array('user_id' => $user_id));
        $email = $user_data[0]['email'];
        $card = $this->UserModel->IsValidCardNo($giftcard, $email);
        if ($card && !$this->UserModel->IsgiftCardProcessed($giftcard, $user_id)) {
            $date = date('Y-m-d H:i:s');
            $store_credit = array('user_id' => $user_id, 'user_email' => $email, 'credit_amount' => $card[0]['amount'], 'date_added' => $date, 'date_modified' => $date, 'is_active' => 1);
            $store_credit_id = $this->AdminOrderModel->insert('store_credit', $store_credit);
            $store_credit_trans = array('store_credit_id' => $store_credit_id, 'gift_card_code' => $giftcard, 'card_amount' => $card[0]['amount'], 'is_receipt' => 1, 'is_issue' => 0);
            $this->AdminOrderModel->insert('store_credit_trans', $store_credit_trans);

            $order_data = $this->AdminOrderModel->simple_get('order', 'gross_amt', array('order_id' => $order_id));
            $gross_amount = $order_data[0]['gross_amt'];
            $store_credit_trans['gift_card_code'] = $order_id;
            $store_credit_trans['is_receipt'] = 0;
            $store_credit_trans['is_issue'] = 1;
            $store_credit_trans['card_amount'] = $gross_amount >= $card[0]['amount'] ? $card[0]['amount'] : $gross_amount;
            $this->AdminOrderModel->insert('store_credit_trans', $store_credit_trans);
            $this->AdminOrderModel->insert('giftcard_trans', array('giftcard_id' => $card[0]['id'], 'order_id' => $order_id, 'amount' => $store_credit_trans['card_amount']));

            $discount_perc = $store_credit_trans['card_amount'] / $gross_amount * 100;
            $this->AdminOrderModel->edit('order', array('discount_perc' => $discount_perc), array('order_id' => $order_id));

            $data['error'] = false;
            $data['msg'] = 'Giftcard applied';
        } else {
            $data['error'] = true;
            $data['msg'] = 'Invalid Giftcard';
        }
        echo json_encode($data);
    }

    public function apply_promo()
    {
        $order_id = $this->input->post('order_id');
        $promocode = $this->input->post('promocode');
        $user_data = $this->AdminOrderModel->simple_get('order', 'user_id', array('order_id' => $order_id));
        $user_id = $user_data[0]['user_id'];
        $user_data = $this->AdminOrderModel->simple_get('users', 'email', array('user_id' => $user_id));
        $email = $user_data[0]['email'];
        $common_promocode = $this->ShoeModel->isCommonPromocode($email, $promocode);
        $promo_det = $this->ShoeModel->getPromoEmail($email);
        if ($promo_det != null) {
            $promo_user_id = $promo_det[0]['id'];
            $promocodedet = $this->ShoeModel->getvalidPromocodes($promo_user_id, $promocode);
            if ($promocodedet != null) {
                if ($promocodedet[0]['discount_per'] != 0) {
                    $this->AdminOrderModel->edit('order', array('discount_perc' => $promocodedet[0]['discount_per'], 'promocode_id' => $promocodedet[0]['promocode_id']), array('order_id' => $order_id));
                } else if ($promocodedet[0]['discount_amount'] != 0) {
                    $order_data = $this->AdminOrderModel->simple_get('order', 'gross_amt', array('order_id' => $order_id));
                    $gross_amount = $order_data[0]['gross_amt'];
                    $discount_perc = $promocodedet[0]['discount_amount'] / $gross_amount * 100;
                    $this->AdminOrderModel->edit('order', array('discount_perc' => $discount_perc, 'promocode_id' => $promocodedet[0]['promocode_id']), array('order_id' => $order_id));
                }
                $data['error'] = false;
                $data['msg'] = 'Promocode applied';
                $this->AdminOrderModel->edit('promo_assign', array('is_used' => 1), array('promocode_id' => $promocodedet[0]['promocode_id'], 'promouser_id' => $promo_user_id));
            } else {
                $data['error'] = true;
                $data['msg'] = 'Invalid Promocode';
            }
        } else {
            $data['error'] = true;
            $data['msg'] = 'Promocode is not assigned to the user';
        }
        echo json_encode($data);
    }

    public function remove_promo()
    {
        $order_id = $this->input->post('order_id');
        $this->AdminOrderModel->edit('order', array('discount_perc' => 0, 'promocode_id' => 0), array('order_id' => $order_id));
        $data['error'] = false;
        $data['msg'] = 'Promocode removed';
        echo json_encode($data);
    }

    public function remove_giftcard()
    {
        $order_id = $this->input->post('order_id');
        $this->AdminOrderModel->edit('order', array('discount_perc' => 0), array('order_id' => $order_id));
        $this->AdminOrderModel->delete('store_credit_trans', array('gift_card_code' => $order_id));
        $data['error'] = false;
        $data['msg'] = 'giftcard removed';
        echo json_encode($data);
    }

    public function updateTrackingNo()
    {
        $orderId = $_POST['order_id'];
        $edittrackingNo = $_POST['edittrackingNo'];
        $status_desc = $_POST['status_desc'];
        $status_desc = str_replace(get_string_inbetween($status_desc, 'TrackingNo:', ', Estimated'), $edittrackingNo, $status_desc);
        $data['orderId'] = $orderId;
        $data['status_desc'] = $status_desc;
        if ($this->AdminViewModel->updateStatusDesc($status_desc, $orderId)) {
            echo $status_desc;
        }
    }

    public function get_string_inbetween($string, $start, $end)
    {
        $string = " " . $string;
        $pos = strpos($string, $start);
        if ($pos == 0) {
            return "";
        }

        $pos += strlen($start);
        $len = strpos($string, $end, $pos) - $pos;
        return substr($string, $pos, $len);
    }

    public $styles = array(
        array('id' => 'oxford', 'name' => 'Oxford'),
        array('id' => 'derby', 'name' => 'Derby'),
        array('id' => 'monk', 'name' => 'Monk'),
        array('id' => 'loafer', 'name' => 'Loafer'),
    );
    public $lasts = array(
        array('id' => '1', 'name' => 'The Townsend'),
        array('id' => '3', 'name' => 'The Archibald'),
        array('id' => '5', 'name' => 'The Wallace'),
        array('id' => '4', 'name' => 'The Harvey'),
        array('id' => '2', 'name' => 'The Eldridge'),
    );
    public $order_status = array(
        array('id' => '', 'name' => 'All'),
        array('id' => 'Order acknowledged', 'name' => 'Order Acknowledged'),
        array('id' => 'Manufacturing in progress', 'name' => 'Manufacturing in progress'),
        array('id' => 'order shipped', 'name' => 'Order Shipped'),
        array('id' => 'Refund', 'name' => 'Refund'),
        array('id' => 'testing', 'name' => 'Testing'),
    );
    public $var_width = array(
        array('id' => 'Regular', 'name' => 'Regular'),
        array('id' => 'Wide', 'name' => 'Wide'),
        array('id' => 'Extra Wide', 'name' => 'Extra Wide'),
        array('id' => 'Triple Wide', 'name' => 'Triple Wide'),
    );
    public $var_size = array(
        array('id' => '5', 'name' => '5'),
        array('id' => '5.5', 'name' => '5.5'),
        array('id' => '6', 'name' => '6'),
        array('id' => '6.5', 'name' => '6.5'),
        array('id' => '7', 'name' => '7'),
        array('id' => '7.5', 'name' => '7.5'),
        array('id' => '8', 'name' => '8'),
        array('id' => '8.5', 'name' => '8.5'),
        array('id' => '9', 'name' => '9'),
        array('id' => '9.5', 'name' => '9.5'),
        array('id' => '10', 'name' => '10'),
        array('id' => '10.5', 'name' => '10.5'),
        array('id' => '11', 'name' => '11'),
        array('id' => '11.5', 'name' => '11.5'),
        array('id' => '12', 'name' => '12'),
        array('id' => '12.5', 'name' => '12.5'),
        array('id' => '13', 'name' => '13'),
        array('id' => '13.5', 'name' => '13.5'),
        array('id' => '14', 'name' => '14'),
    );

}
