<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Common extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("CommonModel");
        $this->load->model('OrderModel');
        $this->load->model('UserModel');
        $this->load->model('ShoeModel');
    }

    public function index()
    {
        $data['meta'] = '<meta name="description" content="Design your custom men\'s handmade shoes online. '
            . 'Our shoes are crafted by seasoned artisans using full grain leather and Hand Welt construction.  ">';
        $data['page_title'] = 'Awl & Sundry - Design Handmade Custom Shoes For Men';

        $data['bodyclass'] = "index";
        $data['additional_js'] = array('build/mediaelement-and-player.min', 'supersized.3.2.7.min', 'supersized.shutter.min', 'build/mediaelement.min', 'index');
        $data['additional_css'] = array("js/build/mediaelementplayer");
        $this->load->view('index', $data);
    }

    private function validatelogin()
    {
        $isvalid = array("IsValid" => true, 'errors' => array());
        if (trim($this->input->post('email')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['email'] = "Error";
        }
        if (trim($this->input->post('password')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['password'] = "Error";
        }
        return $isvalid;
    }

    public function login()
    {
        $data['bodyclass'] = "login";
        $data['errors'] = array();
        $this->load->library('../controllers/twitterlogin');
        $data['twitter_login_link'] = $this->twitterlogin->get_login_link();
        $isloginsubmit = $this->input->post('login');
        if ($isloginsubmit != null) {
            $data['loginsubmit'] = $isloginsubmit;
            $validations = $this->validatelogin();
            if ($validations['IsValid']) {
                $user = $this->UserModel->login($this->input->post('email'), md5($this->input->post('password')));
                if ($user == null) {
                    $unVerifiedUser = $this->UserModel->isPendingVerification($this->input->post('email'), md5($this->input->post('password')));
                    if ($unVerifiedUser != null) {
                        $validations['IsValid'] = false;
                        $data['errors']['unverified'] = "Error";
                        $data['errors']['msg'] = "User account did'nt verified.";
                        $this->load->view('login', $data);
                    } else {
                        $validations['IsValid'] = false;
                        $data['email'] = $this->input->post('email');
                        $data['errors']['invalid'] = "Error";
                        $data['errors']['msg'] = "Invalid UserName/Password";
                        $this->load->view('login', $data);
                    }
                } else {
                    $this->session->set_userdata('user_details', $user);
                    $redirect = $this->session->userdata('redirect');
                    if ($redirect != null && $user['user_type_id'] != 1 && $user['user_type_id'] != 3) {
                        $this->session->unset_userdata('redirect');
                        redirect($redirect, 'refresh');
                    } else {
                        if ($user['user_type_id'] == 2) {
                            redirect('my-account', 'refresh');
                        } else if ($user['user_type_id'] == 1) {
                            redirect('admin/index', 'refresh');
                        } else if ($user['user_type_id'] == 3) {
                            redirect('admin/vendor', 'refresh');
                        } else if ($user['user_type_id'] == 4) {
                            redirect('admin/intern', 'refresh');
                        }
                        redirect('', 'refresh');
                    }
                }
            } else {
                $data['errors'] = $validations['errors'];
                $data['errors']['msg'] = "Invalid UserName/Password";
                $data['email'] = $this->input->post('email');
                $this->load->view('login', $data);
            }
        } else {
            $user = $this->session->userdata('user_details');
            if ($user != null) {
                $redirect = $this->session->userdata('redirect');
                if ($redirect != null && $user['user_type_id'] != 1 && $user['user_type_id'] != 3) {
                    $this->session->unset_userdata('redirect');
                    redirect($redirect, 'refresh');
                } else {
                    if ($user['user_type_id'] == 2) {
                        redirect('my-account', 'refresh');
                    } else if ($user['user_type_id'] == 1) {
                        redirect('admin/index', 'refresh');
                    } else if ($user['user_type_id'] == 3) {
                        redirect('admin/vendor', 'refresh');
                    } else if ($user['user_type_id'] == 4) {
                        redirect('admin/intern', 'refresh');
                    }
                    redirect('', 'refresh');
                }
            } else {
                $this->load->view('login', $data);
            }
        }
    }

    public function forgotPassword()
    {
        $data = array();
        $data['error'] = null;
        $data['bodyclass'] = 'forgot_password';
        $this->load->view('forgot_password', $data);
    }

    public function logout()
    {
        $user = $this->session->userdata('user_details');
        if ($user != null && $user['is_facebookuser']) {
            redirect('facebooklogin/logoutfacebook');
        }
        $this->session->sess_destroy();
        redirect('', 'refresh');
    }

    public function signup()
    {
        $usersess = $this->session->userdata('user_details');
        $this->load->library('form_validation');
        $this->load->helper('string');
        $data = array('errors' => array());
        $data['bodyclass'] = "registration";
        $data['fname'] = $this->input->post('fname');
        $data['lname'] = $this->input->post('lname');
        $data['email'] = $this->input->post('email');
        $isSubmit = $this->input->post('register');

        if ($isSubmit != null) {
            $validations = $this->validateregister();
            if ($validations['IsValid']) {
                $token = random_string('alnum', 20);
                $password = $this->input->post('password');
                $time = strtotime(date('Y-m-d H:i:s'));
                $dob = date('Y-m-d', $time);
                $user = array(
                    'first_name' => $this->input->post('fname'),
                    'last_name' => $this->input->post('lname'),
                    'email' => $this->input->post('email'),
                    'password' => md5($password),
                    'username' => $this->input->post('email'),
                    'birth_date' => $dob,
                    'token' => $token,
                );
                $userdet = $this->UserModel->register_user($user);
                if ($userdet != null) {
                    $this->mailchimpintegration($user['email']);
                    $this->session->set_userdata('success', 'Registration Successful');
                    redirect('login', 'refresh');
                } else {
                    $data['errors']['faild'] = "Error";
                }
            } else {
                $data['errors'] = $validations['errors'];
            }
        }
        if ($usersess != null) {
            if ($usersess['is_facebookuser']) {
                $redirect = $this->session->userdata('redirect');
                if ($redirect != null && $usersess['user_type_id'] != 1 && $usersess['user_type_id'] != 3) {
                    $this->session->unset_userdata('redirect');
                    redirect($redirect, 'refresh');
                } else {
                    redirect('my-account', 'refresh');
                }
            }
        }
        $this->load->view('registration', $data);
    }

    private function validateregister()
    {
        $this->load->helper('email');
        $isvalid = array("IsValid" => true, 'errors' => array());
        if (trim($this->input->post('fname')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['fname'] = "Please give First name";
        }
        if (trim($this->input->post('lname')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['fname'] = "Please give Last name";
        }
        if (trim($this->input->post('email')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['email'] = "Email should not be Empty";
        } else if (!valid_email($this->input->post('email'))) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['emailvalid'] = "Email id is not valid";
        } else if (!$this->UserModel->uniqueemail($this->input->post('email'))) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['emailunique'] = "Email id already exists";
        }
        if (trim($this->input->post('password')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['spassword'] = "Please give password";
        } else if (strlen($this->input->post('password')) < 6) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['passlength'] = "Minimum 6 character required";
        }
        return $isvalid;
    }

    public function registrationemail($user)
    {
        $fromemail = $this->config->item('from_email');
        $this->load->library('email');
        $this->email->to($user['email']);
        $this->email->from($fromemail);
        $this->email->subject('Welcome to Awl and Sundry');
        $msg = $this->load->view('emails/registrationemail', $user, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    public function mailchimpintegration($email)
    {
        $config = array('apikey' => '39c50254155d9583cec13a336c110060-us8', 'secure' => false);
        $this->load->library('MCAPI', $config, 'mail_chimp');
        $list_id = '6b8ea607f9';
        if ($this->mail_chimp->listSubscribe($list_id, $email)) {
            return true;
        }
        return false;
    }

    public function payment_error()
    {
        $this->load->view('payment_error');
    }

    public function order_confirmation($order_id)
    {

        $details = $this->OrderModel->getbillingNshippingDetails($order_id);
        $order_summary = $this->OrderModel->getOrderDet($order_id);

        $data['billing_address'] = $details['billing'][0];
        $data['shipping_address'] = $details['shipping'][0];
        $data['items'] = $details['order']['items'];
        $data['order_details'] = $order_summary[0];
        $total = $order_summary[0]['gross_amt'] - ($order_summary[0]['gross_amt'] * $order_summary[0]['discount_perc']) / 100;
        $data['order_details']['discount'] = round($order_summary[0]['gross_amt'] * $order_summary[0]['discount_perc'] / 100);
        $data['order_details']['tax_amount'] = round($order_summary[0]['tax_amount']);
        $data['order_details']['shipping_cost'] = 0;
        $data['order_details']['order_total'] = $total + $order_summary[0]['tax_amount'];
        $data['bodyclass'] = "order_confirmation";
        $this->load->view('order_confirmation', $data);
    }

    public function press()
    {
        $data['bodyclass'] = "press";
        $this->load->view('press', $data);
    }

    public function faqs()
    {
        $data['bodyclass'] = "faqs";
        $data['additional_js'] = "faqs";
        $this->load->view('faqs', $data);
    }

    public function privacy_policy()
    {
        $data['bodyclass'] = "privacy_policy";
        $this->load->view('privacy_policy', $data);
    }

    public function terms()
    {
        $data['bodyclass'] = "terms";
        $this->load->view('terms', $data);
    }

    public function save_address_details()
    {
        $address = array('user_id' => $this->input->post('userId'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'company' => $this->input->post('company'),
            'phone' => $this->input->post('phone'),
            'fax' => $this->input->post('fax'),
            'address1' => $this->input->post('address1'),
            'address2' => $this->input->post('address2'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'country' => $this->input->post('country'),
            'zipcode' => $this->input->post('zipcode'),
            'date_created' => date('Y-m-d H:i:s'),
            'is_billing' => $this->input->post('is_billing'),
            'is_shipping' => $this->input->post('is_shipping'));
        $res = $this->UserModel->save_address_details($address);
        if ($res != null) {
            echo $res;
        }

    }

    public function shoe_design($shoe_design_id)
    {
        $shoe_design = $this->ShoeModel->get_shoe_design_details($shoe_design_id, 0);
        $shoedetail = $this->ShoeModel->ge_design_model($shoe_design_id);
        $shoe = json_decode($shoedetail['shoemodel'], true);
        $materials[] = $shoe['quarter']['material']; //, $shoe['toe']['material'], $shoe['vamp']['material'], $shoe['eyestay']['material'], $shoe['foxing']['material']);
        if (isset($shoe['toe']['isMaterial'])) {
            $materials[] = $shoe['toe']["material"];
        }
        if (isset($shoe['style']['isVampEnabled'])) {
            if (isset($shoe['vamp']['isMaterial'])) {
                $materials[] = $shoe['vamp']["material"];
            }
        }
        if (isset($shoe['eyestay']['isMaterial'])) {
            $materials[] = $shoe['eyestay']["material"];
        }
        if (isset($shoe['foxing']['isMaterial'])) {
            $materials[] = $shoe['foxing']["material"];
        }
        $shoe_design['price'] = $this->getpriceformaterial($materials);
        $shoe_design['id'] = $shoe_design_id;
        $images[] = ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoe_design['image_file'] . "_A0.png";
        $images[] = ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoe_design['image_file'] . "_A1.png";
        $images[] = ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoe_design['image_file'] . "_A2.png";
        $images[] = ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoe_design['image_file'] . "_A3.png";
        $images[] = ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoe_design['image_file'] . "_A4.png";
        $images[] = ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoe_design['image_file'] . "_A5.png";
        $images[] = ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoe_design['image_file'] . "_A6.png";
        $images[] = ApplicationConfig::ROOT_BASE . 'files/designs/' . $shoe_design['image_file'] . "_A7.png";
        $image = $images[0];
        $content_url = base_url() . 'common/shoe_design/' . $shoe_design_id;
        $meta = <<<EOD
            <meta property="og:title" content="Checkout this custom pair of shoes I just designed at Awlandsundry.com" />
            <meta property="og:url" content="$content_url" />
            <meta property="og:site_name" content="https://www.awlandsundry.com/" />
            <meta property="og:image" content="$image" />
            <meta property="og:description" content="A wise sage once said, 'you can judge a man by his shoes'. Checkout this custom made pair I just designed at Awl & Sundry." />
            <meta property="fb:app_id" content="250777875062389" />

            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:site" content="@awlandsundry">
            <meta name="twitter:creator" content="@awlandsundry">
            <meta name="twitter:title" content="&nbsp;">
            <meta name="twitter:description" content="A wise sage once said, 'you can judge a man by his shoes'. Checkout this custom made pair I just designed at Awl & Sundry.">
            <meta name="twitter:image" content="$image">
            <meta name="twitter:url" content="$content_url" >
            <meta name="twitter:domain" content="awlandsundry.com">
            <meta name="twitter:app:id:iphone" content="">
            <meta name="twitter:app:id:ipad" content="">
            <meta name="twitter:app:id:googleplay" content="">
            <meta name="twitter:app:url:iphone" content="">
            <meta name="twitter:app:url:ipad" content="">
            <meta name="twitter:app:url:googleplay" content="">
EOD;
        $data['relatedshoes'] = $this->get_related_shoes($shoe_design_id);
        $data['images'] = $images;
        $data['meta'] = $meta;
        $data['item'] = $shoe_design;
        $data['bodyclass'] = 'classic_1 details';
        $data['additional_js'] = array('jquery.sumoselect.min', 'details');
        $data['additional_css'] = 'css/sumoselect';
        $this->load->view('shoe_design', $data);
    }

    public function get_related_shoes($shoe_design_id)
    {
        $styleId = $this->ShoeModel->getStyleId($shoe_design_id);
        return $this->ShoeModel->get_related_shoes(array('style_id' => $styleId, 'sort' => 'q_base', 'order' => 'DESC'));
    }

    public function forgot_password()
    {
        $fromForgot = $this->input->post('submit');
        $data['bodyclass'] = "forgot_password";
        if ($fromForgot != null) {
            $token = random_string('alnum', 20);
            $email = $this->input->post('email');
            $this->UserModel->insertPasswordToken($email, $token);
            $userdet = $this->UserModel->getUserDetByEmail($email);
            if ($userdet != null && $email != null) {
                $to = $email;
                $verify = base_url() . 'common/setnewpwd?token=' . $token;
                $data['emaildetails'] = array('link' => $verify, 'fname' => $userdet['first_name'], 'to' => $email);
                $msg = $this->load->view('emails/forgotPasswordmail', $data, true);
                $subject = "Forgot Password";
                $email_from = 'Awl & Sundry <' . $this->config->item('from_email') . '>';
                $headers = "From: " . $email_from . "\r\n";
                $headers .= "Return-Path: " . $this->config->item('from_email') . "\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
                $headers .= "Reply-To: noreply@awlandsundry.com\r\n";

                mail($to, $subject, $msg, $headers);
                redirect('common/forgotmessage', 'refresh');
            } else {
                $data['error'] = 'error';
                $this->load->view('forgot_password', $data);
            }
        } else {
            $data['error'] = null;
            $this->load->view('forgot_password', $data);
        }
    }

    public function forgotmessage()
    {
        $data['bodyclass'] = "registration";
        $this->load->view('forgotpwdmessage', $data);
    }

    public function cart()
    {
        $this->load->model("CustomShoeModel");
        $angle = "_A1";
        $ext = ".png";
        $base_url = CustomShoeConfig::IMG_BASE;
        $cart_contents = $this->cart->contents();

        if (is_array($cart_contents)) {
            $items = array();
            $i = 0;
            $total = 0;
            foreach ($cart_contents as $key => $item) {
                $options = $this->cart->product_options($item['rowid']);
                if (strtolower($item['name']) !== "gift card") {
                    $shoe = array();
                    $shoe = json_decode($options['shoe'], true);
                    $shoe_design = array();
                    if (isset($shoe['from']) && strtolower($shoe['from']) === 'custom_collection') {
                        $shoe_design['last_style']['last_name'] = $shoe['last_name'];
                        $shoe_design['last_style']['style_name'] = $shoe['style_name'];
                        $shoe_design['image'] = urldecode($shoe['image']);
                        $shoe_design['size'] = $shoe['size'];
                    } else {
                        $shoe_design['last_style'] = $this->CustomShoeModel->get_last_style_details($shoe['style']['id']);
                        $shoe_design['img_base'] = $shoe['style']['code'] . "_" . $shoe['toe']['type'] . $shoe['vamp']['type'] . $shoe['eyestay']['type'] . $shoe['foxing']['type'];
                        $shoe_design['base'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['quarter']['matfolder'] . "/" . $shoe['style']['code'] . "_" . $shoe['quarter']['base'] . $angle . $ext;
                        $shoe_design['toe'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['toe']['matfolder'] . "/" . $shoe_design['img_base'] . $shoe['toe']['material'] . $shoe['toe']['color'] . $shoe['toe']['part'] . $angle . $ext;
                        $shoe_design['eyestay'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['eyestay']['matfolder'] . "/" . $shoe_design['img_base'] . $shoe['eyestay']['material'] . $shoe['eyestay']['color'] . $shoe['eyestay']['part'] . $angle . $ext;
                        $shoe_design['vamp'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['vamp']['matfolder'] . "/" . $shoe_design['img_base'] . $shoe['vamp']['material'] . $shoe['vamp']['color'] . $shoe['vamp']['part'] . $angle . $ext;
                        $shoe_design['foxing'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['foxing']['matfolder'] . "/" . $shoe_design['img_base'] . $shoe['foxing']['material'] . $shoe['foxing']['color'] . $shoe['foxing']['part'] . $angle . $ext;
                        $shoe_design['stitch'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['stitch']['folder'] . "/" . $shoe_design['img_base'] . $shoe['stitch']['code'] . $angle . $ext;
                        $shoe_design['lace'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['lace']['folder'] . "/" . $shoe_design['img_base'] . $shoe['lace']['code'] . $angle . $ext;
                        $shoe_design['size'] = $shoe['size'];
                    }
                    $items[$i]['shoe'] = $shoe_design;
                } else {
                    $items[$i]['giftcard']['from'] = $options['sender_name'];
                    $items[$i]['giftcard']['to'] = $options['recipient_name'];
                }
                $items[$i]['name'] = $item['name'];
                $items[$i]['quantity'] = $item['qty'];
                $items[$i]['row_id'] = $item['rowid'];
                $items[$i]['price'] = number_format($item['price'], 2);
                $total += $item['subtotal'];
                $i++;
            }
        } else {

        }
        $data['items'] = $items;
        $user = $this->session->userdata('user_details');
        $cart_discount = $this->session->userdata('cart_discount');
        if ($user !== false) {
            $store = $this->calculate_store_credit_discount($user, $total);
            $store_credit_amount = $store['amount'];
            $data['balance'] = number_format(round($store_credit_amount), 2);
            $data['subtotal'] = number_format(round($total) - round($store_credit_amount), 2);
            $this->addGiftCardDetToSession($total, $store_credit_amount, $store['store_credit_id']);
        } else {
            $data['balance'] = 0;
            $data['subtotal'] = number_format(round($total), 2);
        }
        $data['promo_discount_amt'] = 0;
        if ($cart_discount != false) {
            if ($cart_discount['promo_discount'] != false) {
                $data['promo_discount_amt'] = number_format(round($cart_discount['promo_discount_amount']), 2);
                $data['subtotal'] = number_format(round($data['subtotal'] - $cart_discount['promo_discount_amount']), 2);
            }
        }
        $data['base_url'] = $base_url;
        $data['bodyclass'] = 'cart';
        $data['additional_js'] = 'shoecart';
        $this->load->view('cart', $data);
    }

    public function addGiftCardDetToSession($total, $gc_discount, $store_credit_id)
    {
        $cart_discount = $this->session->userdata('cart_discount');
        $card_disc_perc = 0;
        $promo_disc_amount = 0;
        $total = $this->cart->total();
        if ($cart_discount != false) {
            if ($cart_discount['giftcard_discount'] != 0) {
                $total = $this->cart->total() - ($this->cart->total() * $cart_discount['giftcard_discount']) / 100;
                $card_disc_perc = $cart_discount['giftcard_discount'];
            }
            if (isset($cart_discount['promo_discount'])) {
                $total = $total - $cart_discount['promo_discount_amount'];
                $promo_disc_amount = $cart_discount['promo_discount_amount'];
            }
        }
        if ($card_disc_perc == 0) {
            if ($gc_discount > 0) {
                $card_disc_perc = ($gc_discount / $total) * 100;
            }
        }
        $result['netAmt'] = round(($this->cart->total() - $promo_disc_amount - ($this->cart->total() * $card_disc_perc) / 100));
        $result['discount'] = round($this->cart->total() - $result['netAmt']); //$gc_discount;
        $cartpromoDet['discount'] = round($this->cart->total() - $result['netAmt']); //$gc_discount;
        $cartpromoDet['giftcard_id'] = $store_credit_id; //isset($cart_discount['giftcard_id']) ? $cart_discount['giftcard_id'] : 0;
        if ($this->cart->total() > 0) {
            $cartpromoDet['discount_perc'] = round(($this->cart->total() - $result['netAmt']) / $this->cart->total() * 100);
        } else {
            $cartpromoDet['discount_perc'] = 0;
        }
        $cartpromoDet['promo_discount'] = isset($cart_discount['promo_discount']) ? $cart_discount['promo_discount'] : 0;
        $cartpromoDet['promo_discount_amount'] = isset($cart_discount['promo_discount_amount']) ? round($cart_discount['promo_discount_amount']) : 0;
        $cartpromoDet['giftcard_discount'] = round($card_disc_perc, 2);
        $cartpromoDet['promo_assign_id'] = isset($cart_discount['promo_assign_id']) ? $cart_discount['promo_assign_id'] : 0;
        $cartpromoDet['total'] = round($this->cart->total());
        $this->session->unset_userdata('cart_discount');
        $this->session->set_userdata('cart_discount', $cartpromoDet);
    }

    public function calculate_store_credit_discount($user, $total_amount)
    {
        $this->load->model('UserModel');
        $storecredit_amount = 0;
        $res = $this->UserModel->getStoreCreditBalance($user['email'], $user['user_id']);
        $storecredit['amount'] = $res['credit_amount'];
        $storecredit['store_credit_id'] = $res['store_credit_id'];
        if ($storecredit['amount'] >= $total_amount) {
            $storecredit_amount = $total_amount;
        } else {
            $storecredit_amount = $storecredit['amount'];
        }
        $store['amount'] = round($storecredit_amount);
        $store['store_credit_id'] = $storecredit['store_credit_id'];

        return $store;
    }

    public function getpriceformaterial($materials)
    {
        //$materials = array("M4", "M4", "M4", "M4");
        $price = 350;
        if ((in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials)) && (!in_array("M3", $materials) && !in_array("M4", $materials))) {
            //echo "C <br />";
            $price = 350;
        } else if (!(in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials) || in_array("M4", $materials)) && in_array("M3", $materials)) {
            //echo "AL <br />";
            $price = 1650;
        } else if (!(in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials) || in_array("M3", $materials)) && in_array("M4", $materials)) {
            //echo "OS <br />";
            $price = 700;
        } else {
            if ((in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials)) && in_array("M3", $materials) && in_array("M4", $materials)) {
                //echo "C & AL & OS <br />";
                $price = 700;
            } else if ((in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials)) && (in_array("M3", $materials) || in_array("M4", $materials))) {
                //echo "C & (AL | OS) <br />";
                $price = 600;
            } else {
                //echo "OS - AL<br />";
                $price = 750;
            }
        }
        return $price;
    }

    public function validate_shipping_details()
    {
        $shipping_overlay = '';
        $shipping_error = 0;
        $this->load->library('form_validation');
        $shipping_details = $this->input->post('shipping_details');
        $this->form_validation->set_rules('shipping_details[first_name]', 'First Name', 'required');
        $this->form_validation->set_rules('shipping_details[last_name]', 'Last Name', 'required');
        $this->form_validation->set_rules('shipping_details[address1]', 'Address1', 'required');
        $this->form_validation->set_rules('shipping_details[address2]', 'Address2', '');
        $this->form_validation->set_rules('shipping_details[city]', 'City', 'required');
        $this->form_validation->set_rules('shipping_details[state]', 'State', "required");
        $this->form_validation->set_rules('shipping_details[zipcode]', 'Zip code', 'required|max_length[7]|numeric');
        $this->form_validation->set_rules('shipping_details[phone]', 'Telephone', 'required|max_length[15]');
        if ($this->form_validation->run() == false) {
            $data['errors'] = $this->form_validation->error_array();
            $shipping_error = 1;
            $shipping_overlay = 'hidden';
        }
        $data['shipping_country'] = $this->get_countries(@$shipping_details['country']);
        $data['shipping_state'] = $this->get_states(@$shipping_details['country'], @$shipping_details['state']);
        $data['shipping_details'] = $shipping_details;
        $data['shipping_error'] = $shipping_error;
        $data['shipping_overlay'] = $shipping_overlay;
        $this->load->view('shipping_details', $data);
    }

    public function validate_billing_details()
    {
        $billing_error = 0;
        $billing_overlay = '';
        $billing_form = '';

        $payment_methods = array(array('id' => 'Paypal', 'name' => 'Paypal'), array('id' => 'Credit Card', 'name' => 'Credit Card'));
        $method_credit_card = 'hidden';
        $method_paypal = '';

        $this->load->library('form_validation');
        $billing_details = $this->input->post('billing_details');
        $payment_method_selected = $billing_details['payment_method'];
        $disc = $this->session->userdata('cart_discount');
        if ($disc['discount_perc'] == 100) {
            $this->form_validation->set_rules('billing_details[payment_method]', 'Payment Method', '');
        } else {
            $this->form_validation->set_rules('billing_details[payment_method]', 'Payment Method', 'required');
        }
        $tot_amt = $this->getCartDetails();
        if ($disc['discount_perc'] == 0) {
            $billing_total = $tot_amt;
        } else {
            $billing_total = $tot_amt - $disc['discount_perc'] * $tot_amt / $disc['discount_perc'];
        }
        if ($payment_method_selected != "Paypal") {
            $this->form_validation->set_rules('billing_details[first_name]', 'First Name', 'required');
            $this->form_validation->set_rules('billing_details[last_name]', 'Last Name', 'required');
            $this->form_validation->set_rules('billing_details[address1]', 'Address1', 'required');
            $this->form_validation->set_rules('billing_details[address2]', 'Address2', '');
            $this->form_validation->set_rules('billing_details[city]', 'City', 'required');
            $this->form_validation->set_rules('billing_details[state]', 'State', 'required');
            $this->form_validation->set_rules('billing_details[zipcode]', 'Zip code', 'max_length[6]|numeric');
            $method_credit_card = '';
            $method_paypal = 'hidden';
        }
        if ($this->form_validation->run() == false) {
            if ($disc['discount_perc'] == 100) {
                $billing_overlay = '';
                $billing_error = 0;
                $billing_total = 0;
                $billing_form = 'invisible';
            } else {
                $billing_overlay = 'hidden';
                $data['errors'] = $this->form_validation->error_array();
                $billing_error = 1;
            }
        } else {
            if ($disc['discount_perc'] == 100) {
                $billing_overlay = '';
                $billing_error = 0;
                $billing_total = 0;
                $billing_form = 'invisible';
            }
        }

        $billing_details['payment_method'] = $this->generate_options($payment_methods, 0, 0, @$payment_method_selected);
        $data['billing_country'] = $this->get_countries(@$billing_details['country']);
        $data['billing_state'] = $this->get_states(@$billing_details['country'], @$billing_details['state']);
        $data['billing_details'] = $billing_details;
        $data['billing_error'] = $billing_error;
        $data['billing_total'] = $billing_total;
        $data['billing_form'] = $billing_form;
        $data['billing_overlay'] = $billing_overlay;
        $data['method_credit_card'] = $method_credit_card;
        $data['method_paypal'] = $method_paypal;
        $this->load->view('billing_details', $data);
    }

    public function get_checkout_amount()
    {
        $tot_amt = $this->getCartDetails();
        $disc = $this->session->userdata('cart_discount');
        $giftCardTotal = $this->getGiftCardTotal();
        $giftCardDiscount = 0;
        $discAmt = 0;
        if ($disc != false) {
            $giftCardDiscount = $giftCardTotal * $disc['discount_perc'] / 100;
            $discAmt = $disc['discount'];
        }
        $total_amt = $data['gross_amt'] = number_format($tot_amt - $giftCardTotal + $giftCardDiscount, 2);
        if ($this->input->post('state') == 'New Jersey') {
            $taxrate = 7; //7 percentage
            $data['taxAmt'] = round(($total_amt - $discAmt) * $taxrate / 100, 2);
        } else {
            $data['taxAmt'] = 0;
        }
        $data['discount'] = $discAmt;
        $data['sub_total'] = number_format($total_amt - $discAmt, 2);
        $data['total_amt'] = number_format(round($data['taxAmt'] + ($total_amt - $discAmt), 2), 2);

        echo json_encode($data);
    }

    public function checkout()
    {
        $userdetails = $this->session->userdata('user_details');
        $this->session->set_userdata('redirect', base_url() . 'checkout');

        if ($userdetails == false) {
            redirect('login');
        }

        if (sizeof($this->cart->contents()) > 0) {
            $i = 0;
            foreach ($this->cart->contents() as $items) {
                $item[$i]['name'] = $items['name'];
                $item[$i]['price'] = $items['price'];
                $i++;
            }
        } else {
            redirect('my-account', 'refresh');
        }

        $data = array();
        $user_id = $userdetails['user_id'];
        $shipping_overlay = 'hidden';
        $billing_overlay = '';
        $review_overlay = '';
        $billing_form = '';
        $shipping_error = 1;
        $billing_error = 1;
        $method_credit_card = '';
        $method_paypal = 'hidden';
        $tot_amt = $this->getCartDetails();
        if ($userdetails !== false) {
            $store = $this->calculate_store_credit_discount($userdetails, $tot_amt);
            $store_credit_amount = $store['amount'];
            $data['balance'] = round($store_credit_amount, 2);
            $data['subtotal'] = round($tot_amt - $store_credit_amount, 2);
            $this->addGiftCardDetToSession($tot_amt, $store_credit_amount, $store['store_credit_id']);
        }
        $payment_methods = array(array('id' => 'Paypal', 'name' => 'Paypal'), array('id' => 'Credit Card', 'name' => 'Credit Card'));

        if ($tot_amt == 0) {
            $billing_form = 'hidden';
        }

        $disc = $this->session->userdata('cart_discount');
        if (($disc['promo_discount'] != 0 || $disc['promo_discount_amount'] != 0) && $disc['promo_assign_id'] == -1) {
            if (isset($disc['promo_code'])) {
                $promo_det = $this->ShoeModel->getPromoEmail($userdetails['email']);

                if ($promo_det != null) {
                    $promo_user_id = $promo_det[0]['id'];

                    $promocodedet = $this->ShoeModel->getvalidPromocodes($promo_user_id, $disc['promo_code']);
                    if ($promocodedet != null) {
                        $sess_disc = $this->session->userdata('cart_discount');
                        unset($sess_disc['promo_code']);
                        $sess_disc['promo_assign_id'] = $promocodedet[0]['assign_id'];
                        $this->session->unset_userdata('cart_discount');
                        $this->session->set_userdata('cart_discount', $sess_disc);
                        $new_sess = $this->session->userdata('cart_discount');
                        $disc = $this->session->userdata('cart_discount');
                    }
                }
            }
        }
        if ($disc != null) {
            if (isset($disc['discount'])) {
                $discAmt = $disc['discount'];
            } else {
                $discount = round($disc['discount_perc'], 2);
                $discAmt = round($tot_amt * $discount / 100, 2);
            }
        } else {
            $discount = 0;
            $discAmt = 0;
        }
        $payment_method_selected = 'Credit Card';

        if ($this->input->post() !== false) {
            $shipping_details = $this->input->post('shipping_details');
            $this->load->library('form_validation');
            $billing_details = $this->input->post('billing_details');
            $payment_method_selected = $billing_details['payment_method'];

            if ($disc['discount_perc'] == 100) {
                $this->form_validation->set_rules('billing_details[payment_method]', 'Payment Method', '');
            } else {
                $this->form_validation->set_rules('billing_details[payment_method]', 'Payment Method', 'required');
            }
            $this->form_validation->set_rules('shipping_details[first_name]', 'First Name', 'required');
            $this->form_validation->set_rules('shipping_details[last_name]', 'Last Name', 'required');
            $this->form_validation->set_rules('shipping_details[address1]', 'Address1', 'required');
            $this->form_validation->set_rules('shipping_details[address2]', 'Address2', '');
            $this->form_validation->set_rules('shipping_details[city]', 'City', 'required');
            $this->form_validation->set_rules('shipping_details[state]', 'State', "required");
            $this->form_validation->set_rules('shipping_details[zipcode]', 'Zip code', 'required|max_length[7]|numeric');
            $this->form_validation->set_rules('shipping_details[phone]', 'Telephone', 'required|max_length[15]');

            if ($payment_method_selected != "Paypal") {
                $ispaypal = "req";

                $this->form_validation->set_rules('stripeToken', 'Stripe Token', 'required');
                $this->form_validation->set_rules('billing_details[first_name]', 'First Name', 'required');
                $this->form_validation->set_rules('billing_details[last_name]', 'Last Name', 'required');
                $this->form_validation->set_rules('billing_details[address1]', 'Address1', 'required');
                $this->form_validation->set_rules('billing_details[address2]', 'Address2', '');
                $this->form_validation->set_rules('billing_details[city]', 'City', 'required');
                $this->form_validation->set_rules('billing_details[state]', 'State', 'required');
                $this->form_validation->set_rules('billing_details[zipcode]', 'Zip code', 'max_length[6]|numeric');
                $method_credit_card = '';
                $method_paypal = 'hidden';
            } else {
                $cardrequired = "";
            }
            $count_of_shipping_errors = 0;
            if ($this->form_validation->run() === false) {
                $errors = $this->form_validation->error_array();
                $count_of_shipping_errors = isset($errors['shipping_details']) ? sizeof($errors['shipping_details']) : 0;
            }

            if ($this->form_validation->run() !== false || (!$count_of_shipping_errors && $disc['discount_perc'] == 100)) {
                $order_id = $this->session->userdata('order_id');
                if ($order_id === false) {
                    $order_id = $this->get_order_id();
                    if ($disc != null) {
                        if ($disc['giftcard_id'] != 0) {
                            $this->load->model('UserModel');
                            $total = $disc['total'];
                            $this->UserModel->updateStoreCreditAmount($disc['giftcard_id'], $disc['giftcard_discount'], $order_id, $disc['total']);
                        }
                        if ($disc['promo_assign_id'] != 0) {
                            $this->ShoeModel->updatepromoAssign($disc['promo_assign_id'], $order_id);
                        }
                        $this->session->unset_userdata('cart_discount');
                    }
                }
                if ($shipping_details['state'] == 'New Jersey') {
                    $taxrate = 7; //7 percentage
                    $giftCardTotal = $this->getGiftCardTotal();
                    $giftCardDiscount = $giftCardTotal * $disc['discount_perc'] / 100;
                    $total_amt = $tot_amt - $giftCardTotal + $giftCardDiscount;
                    $taxAmt = round(($total_amt - $discAmt) * $taxrate / 100, 2);
                } else {
                    $taxAmt = 0;
                }

                $comment = $this->input->post('comment');
                $this->OrderModel->updateOrderComment($order_id, $comment, $taxAmt);
                $this->savebilling_shipping($order_id);

                $ccdata = array('card_type' => $payment_method_selected,
                    'card_no' => str_replace(' ', '', $this->input->post('card_number')),
                    'card_owner' => $this->input->post('card_owner'),
                    'exp_date' => $this->input->post('exp_date_month') . '20' . $this->input->post('exp_date_year'),
                    'card_cvv2' => $this->input->post('card_cvv_code'),
                );
                $token = $this->input->post('stripeToken');
                $this->session->set_userdata('stripe_token', $token);
                $this->session->set_userdata('ccdata', $ccdata);
                redirect('pay-by-card/' . $order_id, 'refresh');
            } else {
                $data['errors'] = $this->form_validation->error_array();
                $data['errors']['shippingbilling'] = "Error";
                $shipping_details = $this->input->post('shipping_details');

                if (!isset($data['errors']['shipping_details']) || !sizeof($data['errors']['shipping_details'])) {
                    $billing_overlay = 'hidden';
                    $shipping_error = '0';
                    if (!isset($data['errors']['billing_details']) || !sizeof($data['errors']['billing_details'])) {
                        $review_overlay = 'hidden';
                        $billing_error = '0';
                    }
                }
            }
        } else {
            $shipping_details = $this->UserModel->get_user_address_details($user_id, 'is_shipping');
            $billing_details = $this->UserModel->get_user_address_details($user_id, 'is_billing');
            $shipping_details = $shipping_details[0];
            $billing_details = $billing_details[0];
            $billing_details['country'] = isset($billing_details['country']) ? $billing_details['country'] : 'United States';
            $shipping_details['country'] = isset($shipping_details['country']) ? $shipping_details['country'] : 'United States';
        }

        $billing_details['payment_method'] = $this->generate_options($payment_methods, 0, 0, @$payment_method_selected);
        $data['shipping']['shipping_country'] = $this->get_countries(@$shipping_details['country']);
        $data['billing']['billing_country'] = $this->get_countries(@$billing_details['country']);

        $data['shipping']['shipping_state'] = $this->get_states(@$shipping_details['country'], @$shipping_details['state']);
        $data['billing']['billing_state'] = $this->get_states(@$billing_details['country'], @$billing_details['state']);

        if (isset($shipping_details['state']) && $shipping_details['state'] == 'New Jersey') {
            $taxrate = 7; //7 percentage
            $giftCardTotal = $this->getGiftCardTotal();
            $giftCardDiscount = $giftCardTotal * $disc['discount_perc'] / 100;
            $total_amt = $tot_amt - $giftCardTotal + $giftCardDiscount;
            $taxAmt = round(($total_amt - $discAmt) * $taxrate / 100, 2);
        } else {
            $taxAmt = 0;
        }
        $data['billing_form'] = '';
        if ($tot_amt - $discAmt == 0) {
            $data['billing_form'] = 'invisible';
        }
        $data['items'] = $item;
        $data['number_of_items'] = $i;
        $data['disc'] = $discAmt;
        $data['tax'] = number_format(round($taxAmt, 2), 2);
        $data['sub_total'] = number_format(round($tot_amt - $discAmt, 2), 2);
        $data['net_total'] = number_format(round($tot_amt - $discAmt + $taxAmt, 2), 2);
        $data['shipping_cost'] = number_format(0, 2);

        $data['shipping']['shipping_details'] = $shipping_details;
        $data['shipping']['errors'] = isset($data['errors']) ? $data['errors'] : array();
        $data['shipping']['shipping_error'] = $shipping_error;
        $data['shipping']['shipping_overlay'] = $shipping_overlay;

        $data['billing']['errors'] = isset($data['errors']) ? $data['errors'] : array();
        $data['billing']['billing_details'] = $billing_details;
        $data['billing']['billing_error'] = $billing_error;
        $data['billing']['billing_overlay'] = $billing_overlay;
        $data['billing']['method_credit_card'] = $method_credit_card;
        $data['billing']['method_paypal'] = $method_paypal;

        $data['review_overlay'] = $review_overlay;
        $data['bodyclass'] = 'checkout';
        $data['additional_js'] = array('https://js.stripe.com/v1/', 'jquery.formance.min', 'jquery.validate.min', 'checkout');
        $this->load->view('checkout', $data);
    }

    public function get_stripe_key()
    {
        $this->load->config('charge');
        $stripe_public_key = $this->config->item('stripe_public_key');
        echo $stripe_public_key;
    }

    public function get_errors_list($errors)
    {
        $list = '';
        $flag = false;
        foreach ($errors as $error) {
            if (strpos($error, 'field is required') !== false) {

                if (!$flag) {
                    $list .= ' * Required fields can not be empty<br/>';
                    $flag = true;
                }
            } else {
                $list .= ' * ' . $error . '<br/>';
            }
        }
        return $list;
    }

    private function savebilling_shipping($order_id)
    {
        $userdet = $this->session->userdata('user_details');
        $billing_details = $this->input->post('billing_details');
        $shipping_details = $this->input->post('shipping_details');
        $data = array();
        $billing = array(
            'order_id' => $order_id,
            'user_id' => $userdet['user_id'],
            'address1' => $billing_details['address1'],
            'address2' => $billing_details['address2'],
            'city' => $billing_details['city'],
            'state' => $billing_details['state'],
            'country' => $billing_details['country'],
            'zipcode' => $billing_details['zipcode'],
            'date_created' => date('Y-m-d H:i:s'),
        );
        $shipping = array(
            'order_id' => $order_id,
            'user_id' => $userdet['user_id'],
            'firstname' => $shipping_details['first_name'],
            'lastname' => $shipping_details['last_name'],
            'email' => $userdet['email'],
            'telephone' => $shipping_details['phone'],
            'address1' => $shipping_details['address1'],
            'address2' => $shipping_details['address2'],
            'city' => $shipping_details['city'],
            'state' => $shipping_details['state'],
            'country' => $shipping_details['country'],
            'zipcode' => $shipping_details['zipcode'],
            'date_created' => date('Y-m-d H:i:s'),
        );
        $data['billing'] = $billing;
        $data['shipping'] = $shipping;
        $this->OrderModel->savebilling($data);
    }

    private function getGiftCardTotal()
    {
        $tot_amount = 0;

        if (sizeof($this->cart->contents()) > 0) {
            foreach ($this->cart->contents() as $items):
                if (strtolower($items['name']) == 'gift card') {
                    $tot_amount += $items['price'];
                }
            endforeach;
        }
        return $tot_amount;
    }

    private function getCartDetails()
    {
        $tot_amount = 0;

        if (sizeof($this->cart->contents()) > 0) {
            foreach ($this->cart->contents() as $items):
                if (strtolower($items['name']) == 'gift card') {
                    $tot_amount += $items['qty'] * $items['price'];
                } else {
                    $option = $this->cart->product_options($items['rowid']);
                    $shoe = json_decode($option['shoe'], true);

                    if (isset($shoe['from']) && $shoe['from'] == 'custom_collection') {
                        $shoe_design_id = $shoe['shoe_design_id'];
                        $shoedetail = $this->ShoeModel->ge_design_model($shoe_design_id);
                        $shoe = json_decode($shoedetail['shoemodel'], true);
                        $tot_amount += $items['qty'] * $items['price'];
                    } else {
                        $materials[] = $shoe['quarter']['material']; //, $shoe['toe']['material'], $shoe['vamp']['material'], $shoe['eyestay']['material'], $shoe['foxing']['material']);
                        if (isset($shoe['toe']['isMaterial'])) {
                            $materials[] = $shoe['toe']["material"];
                        }
                        if (isset($shoe['style']['isVampEnabled'])) {
                            if (isset($shoe['vamp']['isMaterial'])) {
                                $materials[] = $shoe['vamp']["material"];
                            }
                        }
                        if (isset($shoe['eyestay']['isMaterial'])) {
                            $materials[] = $shoe['eyestay']["material"];
                        }
                        if (isset($shoe['foxing']['isMaterial'])) {
                            $materials[] = $shoe['foxing']["material"];
                        }
                        $tot_amount += $items['qty'] * $this->getpriceformaterial($materials);
                    }
                }
            endforeach;
        }
        return $tot_amount;
    }

    public function get_order_id()
    {
        $userdetails = $this->session->userdata('user_details');
        $disc = $this->session->userdata('cart_discount');
        if ($disc == null) {
            $disc['discount_perc'] = 0;
        } else {
            if (isset($disc['discount'])) {
                $disc['discount_perc'] = round($disc['discount'] / $disc['total'] * 100, 2);
            }
        }
        $order_main = array();
        $order = array();
        $order_items = array();
        $items = array();
        $tot_amt = 0;
        if (sizeof($this->cart->contents()) > 0) {
            foreach ($this->cart->contents() as $items):
                $option = $this->cart->product_options($items['rowid']);
                if (strtolower($items['name']) == 'gift card') {
                    $tot_amt += $order_items['item_amt'] = $items['qty'] * $items['price'];
                    $order_items['item_name'] = 'Gift Card';
                    $order_items['design_id'] = '';
                    $order_items['item_id'] = $option['giftcard_id'];
                    $order_items['left_shoe'] = '';
                    $order_items['right_shoe'] = '';
                    $order_items['left_width'] = '';
                    $order_items['right_width'] = '';
                    $order_items['quantity'] = $items['qty'];
                    $order['items'][] = $order_items;
                } else {
                    $shoe = json_decode($option['shoe'], true);
                    $name = $items['name'];

                    $order_items['left_shoe'] = $shoe['size']['left']['text'];
                    $order_items['right_shoe'] = $shoe['size']['right']['text'];
                    $order_items['left_width'] = $shoe['size']['left']['width'];
                    $order_items['right_width'] = $shoe['size']['right']['width'];
                    if (isset($shoe['from']) && $shoe['from'] === 'custom_collection') {
                        $savedShoe = $this->save_custom_collection_item($shoe['shoe_design_id'], $userdetails['user_id']);
                        $shoe = $savedShoe;
                        $name = $shoe['details']['name'];
                        $order_items['design_id'] = $shoe['shoeDesignId'];

                        $tot_amt += $order_items['item_amt'] = $items['qty'] * $items['price'];
                    } else {
                        $savedShoe = $this->saveCartItem($shoe, $userdetails['user_id']);
                        $shoe = $savedShoe;
                        $name = $shoe['details']['name'];
                        $order_items['design_id'] = $shoe['shoeDesignId'];
                        $materials[] = $shoe['quarter']['material'];
                        if ($shoe['toe']['isMaterial']) {
                            $materials[] = $shoe['toe']["material"];
                        }
                        if ($shoe['style']['isVampEnabled']) {
                            if ($shoe['vamp']['isMaterial']) {
                                $materials[] = $shoe['vamp']["material"];
                            }
                        }
                        if ($shoe['eyestay']['isMaterial']) {
                            $materials[] = $shoe['eyestay']["material"];
                        }
                        if ($shoe['foxing']['isMaterial']) {
                            $materials[] = $shoe['foxing']["material"];
                        }
                        $tot_amt += $order_items['item_amt'] = $items['qty'] * $this->getpriceformaterial($materials);
                    }

                    $order_items['item_name'] = $name;

                    $order_items['quantity'] = $items['qty'];
                    $order['items'][] = $order_items;
                }
            endforeach;
        }
        $order_main['invoice_no'] = $this->OrderModel->getinvoiceno();
        $order_main['user_id'] = $userdetails['user_id'];
        $order_main['currency'] = 'USD';
        $order_main['gross_amt'] = $tot_amt;
        $order_main['user_agent'] = $this->input->user_agent();
        if ($disc != null) {
            if (isset($disc['discount'])) {
                $order_main['discount'] = $disc['discount'];
                $order_main['discount_perc'] = round($disc['discount'] / $tot_amt * 100, 2);
            } else {
                $order_main['discount_perc'] = $disc['discount_perc'];
                $discount = round($disc['discount_perc'], 2);
                $discAmt = round($tot_amt * $discount / 100, 2);
                $order_main['discount'] = $discAmt;
            }
        } else {
            $order_main['discount_perc'] = 0;
            $order_main['discount'] = 0;
        }
        $order_main['order_modified'] = date('Y-m-d H:i:s');
        $order_main['payment_status'] = 'Not Processed';
        $order['order_main'] = $order_main;
        $order_id = $this->OrderModel->saveorder($order);
        $this->session->set_userdata('order_id', $order_id);
        return $order_id;
    }

    private function saveCartItem($cartItem, $userId)
    {
        $savedShoe = $this->ShoeModel->savemodel($userId, $cartItem);
        $cartItem = json_decode($savedShoe['shoemodel'], true);
        $cartItem['shoeDesignId'] = $savedShoe['shoe_design_id'];
        $cartItem['details'] = $savedShoe;
        return $cartItem;
    }

    private function save_custom_collection_item($shoe_design_id, $userId)
    {
        $savedShoe = $this->ShoeModel->save_custom_model($userId, $shoe_design_id);
        $cartItem = json_decode($savedShoe['shoemodel'], true);
        $cartItem['shoeDesignId'] = $savedShoe['shoe_design_id'];
        $cartItem['details'] = $savedShoe;
        return $cartItem;
    }

    private function generate_options($array = array(), $from = 1, $to = 12, $selected = 0)
    {
        $options = '';
        if (sizeof($array) == 0) {
            if ($from >= 0 && $to > 0) {
                for ($i = $from; $i <= $to; $i++) {
                    $text = (isset($selected) && $selected == $i) ? 'selected="selected" ' : '';
                    $options .= '<option ' . $text . ' value="' . $i . '" >' . $i . '</option>';
                }
            }
        } else {
            foreach ($array as $key => $value) {
                $text = (isset($selected) && $selected == $value['id']) ? 'selected="selected" ' : '';
                $options .= '<option value = "' . $value['id'] . '" ' . $text . ' >' . $value['name'] . '</option>';
            }
        }
        return $options;
    }

    public function get_states_ajax($country)
    {
        echo $this->get_states($country, '');
    }

    public function get_states($country, $selected)
    {
        $country = urldecode($country);
        $states = $this->UserModel->get_states($country);
        $prefix = '<option value="">State/Province</option>';
        if (strtolower($country) == 'canada') {
            $prefix = '<option value="">Province</option>';
        }

        if (strtolower($country) == 'united kingdom') {
            $prefix = '<option value="">County</option>';
        }

        if (strtolower($country) == 'united states' || strtolower($country) == 'australia') {
            $prefix = '<option value="">State</option>';
        }

        return $prefix . $this->generate_options($states, 0, 0, $selected);
    }

    public function get_countries($selected)
    {
        $courtries = $this->UserModel->get_countries();
        return $this->generate_options($courtries, 1, 2, $selected);
    }

    public function keepmail()
    {
        $email = $this->input->post('email');
        $this->mailchimpintegration($email);
        $this->UserModel->insertKeepMail($email);
        $this->send_news_letter_email($email);
        return true;
    }

    public function mailTest()
    {
        $data = array();
        $data['promocode'] = 'A&S December';
        $this->load->view('emails/newsletter_signup', $data);
    }

    private function send_news_letter_email($email)
    {
        $data = array();
        $data['promocode'] = 'A&S December';
        $this->load->library('email');
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to($email);
        $this->email->subject('Welcome to Awl & Sundry');
        $msg = $this->load->view('emails/newsletter_signup', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    public function processGiftCard()
    {
        $user = $this->session->userdata('user_details');
        $this->session->set_userdata('redirect', base_url() . 'cart');
        $data = null;
        $data['errors'] = null;
        if ($user == false) {
            $data['is_login'] = false;
            $data['is_valid'] = false;
            $data['url'] = 'login';
        } else {
            $data['is_login'] = true;
            $gc_code = $this->input->post('code');
            $validations = $this->validate_giftCard($gc_code, $user['email'], $user['user_id']);
            if ($validations['IsValid']) {
                $data['is_valid'] = true;
                $this->load->model('UserModel');
                $cardAmount = $this->UserModel->getGiftCardAmount($gc_code, $user['email']);
                $credit_sum = array(
                    'user_id' => $user['user_id'],
                    'user_email' => $user['email'],
                    'credit_amount' => $cardAmount,
                    'date_added' => date('Y-m-d H:i:s'),
                    'date_modified' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                );
                $credit_det = array(
                    'gift_card_code' => $gc_code,
                    'card_amount' => $cardAmount,
                    'is_receipt' => 1,
                    'is_issue' => 0,
                );

                $this->UserModel->insertGiftcardDetails($credit_sum, $credit_det);
                $data['errors']['validCard'] = 'Your gift card is successfully added to store credit ';
                $res = $this->UserModel->getStoreCreditBalance($user['email'], $user['user_id']);
                $data['balance'] = $res['credit_amount'];
                $from = $this->input->post('from');
                if ($from == 'cart') {
                    $this->addGiftCardDetToSession(0, $res['credit_amount'], $res['store_credit_id']);
                }
            } else {
                $data['is_valid'] = false;
                $data['errors'] = $validations['errors'];
            }
        }
        echo json_encode($data);
    }

    public function validate_giftCard($gc_code, $userEmail, $userId)
    {
        $this->load->model('UserModel');
        $isvalid = array("IsValid" => true, 'errors' => array());
        if (!$this->UserModel->IsValidCardNo($gc_code, $userEmail)) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['validCard'] = "This is not a valid gift card code";
        } else if ($this->UserModel->IsgiftCardProcessed($gc_code, $userId)) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['validCard'] = "This gift card is already used.";
        }
        return $isvalid;
    }

    public function setnewpwd()
    {
        $token = $_GET['token'];
        $this->load->model('UserModel');
        redirect('common/resetPassword/' . $token, 'refresh');
    }

    public function resetpassword($token)
    {
        $data['bodyclass'] = 'forgot_password';
        $fromresetpwd = $this->input->post('submit');

        if ($fromresetpwd != null) {
            $token = $this->input->post('hid_token');
            $newPwd = $this->input->post('newPwd');
            $user = $this->UserModel->resetPassword($token, $newPwd);
            if ($user != null) {
                $this->session->set_userdata('user_details', $user);
                if ($user['user_type_id'] == 2) {
                    redirect('my-account', 'refresh');
                } else if ($user['user_type_id'] == 1) {
                    redirect('admin/admin_view', 'refresh');
                } else if ($user['user_type_id'] == 3) {
                    redirect('admin/vendor', 'refresh');
                }
            } else {
                die('Reset password failed. Please try again !!! <br /><br /> <a href="' . base_url() . '">Click here to go Home</a>');
            }
        } else {
            $data['token'] = $token;
            $this->load->view('resetpassword', $data);
        }
    }

    public function send_email()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('from_email', 'From Email', 'required|valid_email');
        $this->form_validation->set_rules('to_emails', 'To Email', 'required|valid_emails');
        $this->form_validation->set_rules('message', 'Message', 'max_length[320]');
        if ($this->form_validation->run() == false) {
            $data['errors'] = $this->form_validation->error_array();
            $data['status'] = 'fail';
        } else {
            $this->load->library('email');
            $form_data = $this->input->post();
            $data['url'] = $form_data['url'];
            $data['image'] = $form_data['image'];
            $data['message'] = $form_data['message'];
            $data['text'] = $this->email->to($form_data['to_emails']);
            $this->email->from($form_data['from_email']);
            $this->email->subject('Awl and Sundry Design');
            $msg = $this->load->view('emails/share_email', $data, true);
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
            $data['status'] = 'success';
        }
        echo json_encode($data);
    }

    public function email_newsletter()
    {
        $this->load->view('emails/new-templates/newsletter_signup');
    }

    public function order_confirm()
    {
        $data = array();
        $msg = $this->load->view('emails/new-templates/order_confirmation', $data, true);
        $msg = preg_replace("/[\r\n]+/", "", $msg);
        $msg = preg_replace("/[\s\s]+/", " ", $msg);
        echo $msg;
    }

    public function email1()
    {
        $this->load->view('emails/new-templates/email2');
    }

}

function _log($data)
{
    echo '<script type="text/javascript">console.log(' . json_encode(@$data or "1") . ')</script>';
}
