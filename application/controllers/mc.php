<?php

class Mc extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function createMCStore()
    {
        exit;
        $this->load->library('MailChimp');
        $listId = $this->config->item('listId');

        $storeArray = array(
            'id' => 'awlandsundry_1',
            'list_id' => $listId,
            'name' => 'Awl & Sundry',
            'currency_code' => 'USD',
        );

        $result = $this->mailchimp->post('ecommerce/stores', $storeArray);

        echo '<pre>';
        print_r($result);
        exit;
    }

    public function viewMcCustomers()
    {
        $this->load->library('MailChimp');

        $storeId = $this->config->item('storeId');

        $result = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/customers/');
        unset($result['_links']);

        for ($i = 0; $i < count($result['customers']); $i++) {
            unset($result['customers'][$i]['_links']);
        }

        echo '<pre>';
        print_r($result);
    }

    public function viewMcOrders()
    {
        $this->load->library('MailChimp');

        $storeId = $this->config->item('storeId');

        $result = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/orders/');
        unset($result['_links']);

        for ($i = 0; $i < count($result['orders']); $i++) {
            unset($result['orders'][$i]['_links']);
            unset($result['orders'][$i]['customer']['_links']);
            for ($j = 0; $j < count($result['orders'][$i]['lines']); $j++) {
                unset($result['orders'][$i]['lines'][$j]['_links']);
            }
        }

        echo '<pre>';
        print_r($result);
    }

    public function deleteMcOrders()
    {
        $this->load->library('MailChimp');

        $storeId = $this->config->item('storeId');

        $result = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/orders/');
        unset($result['_links']);

        for ($i = 0; $i < count($result['orders']); $i++) {
            $this->mailchimp->delete('ecommerce/stores/' . $storeId . '/orders/' . $result['orders'][$i]['id']);
        }

        echo '<pre>';
        print_r($result);
    }

    public function deleteCartMC()
    {
        $this->load->library('MailChimp');
        $storeId = $this->config->item('storeId');
        $result = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/carts');

        for ($i = 0; $i < count($result['carts']); $i++) {
            $this->mailchimp->delete('ecommerce/stores/' . $storeId . '/carts/' . $result['carts'][$i]['id']);
        }
        echo '<pre>';
        print_r($result);
        exit;
    }

    public function viewCartMC()
    {
        $this->load->library('MailChimp');
        $storeId = $this->config->item('storeId');

        $sessData = $this->session->userdata('user_details');

        if (isset($sessData['user_id'])) {
            $result = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/carts/AWL_CART_' . $sessData['user_id']);
            if (isset($result['id'])) {
                unset($result['_links']);
                unset($result['customer']['_links']);
                for ($i = 0; $i < count($result['lines']); $i++) {
                    unset($result['lines'][$i]['_links']);
                }
            }

            echo '<pre>';
            print_r($result);
            exit;
        } else {
            $result = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/carts');
            if (isset($result['store_id'])) {
                unset($result['_links']);
                for ($i = 0; $i < count($result['carts']); $i++) {
                    unset($result['carts'][$i]['customer']['_links']);
                    unset($result['carts'][$i]['_links']);
                    for ($j = 0; $j < count($result['carts'][$i]['lines']); $j++) {
                        unset($result['carts'][$i]['lines'][$j]['_links']);
                    }
                }
            }

            echo '<pre>';
            print_r($result);
            exit;
        }
    }

    public function viewProductsMC()
    {
        $this->load->library('MailChimp');
        $storeId = $this->config->item('storeId');

        $result = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/products', array('count' => 1000));
        unset($result['_links']);
        for ($i = 0; $i < count($result['products']); $i++) {
            unset($result['products'][$i]['_links']);
            for ($j = 0; $j < count($result['products'][$i]['variants']); $j++) {
                unset($result['products'][$i]['variants'][$j]['_links']);
            }
        }

        echo '<pre>';
        print_r($result);
        exit;
    }

    public function deleteProductsMC()
    {
        $this->load->library('MailChimp');
        $storeId = $this->config->item('storeId');

        $result = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/products', array('count' => 1000));
        $results = [];
        for ($i = 0; $i < count($result['products']); $i++) {
            $results[] = $this->mailchimp->delete('ecommerce/stores/' . $storeId . '/products/' . $result['products'][$i]['id']);
        }

        echo '<pre>';
        print_r($results);
        exit;
    }

    public function addProductsToMC()
    {

        $this->load->model("CommonModel");

        $data = $this->db->select('p.mc_id as id,p.name as title,
            IF(p.shoe_type = "RTW","Ready to Wear","Custom Shoes") as type,
            p.type as tmpType,
            IFNULL(GROUP_CONCAT(CONCAT(v.mc_variant_id,"|",v.title)),CONCAT(p.mc_id,"|","")) as variants
            ', false)->join('mailchimp_products_variant as v', 'v.product_id = p.id', 'left')
            ->group_by('p.id')
            ->get('mailchimp_products as p')->result_array();

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['variants'] = explode(',', $data[$i]['variants']);
            $tmpType = $data[$i]['tmpType'];

            if ($data[$i]['type'] == 'Ready to Wear') {
                $tmp = $this->db->get_where('ready_wear', array('mc_product_id' => $data[$i]['id']))->row_array();
                $data[$i]['description'] = $tmp['description'] . '';
            } else {
                $tmp = $this->db->group_by('style_name')->order_by('sort_order', 'ASC')->get_where('last_styles', array('style_name' => $tmpType))->row_array();
                $data[$i]['description'] = $tmp['style_description'] . '';
                $data[$i]['image_url'] = ($tmp['img_file'] != '' && $tmp['img_file'] != null) ? ApplicationConfig::IMG_BASE . 'thumb/' . $tmp['img_file'] . '_A0.png' : '';
            }
            $data[$i]['vendor'] = $data[$i]['type'];

            unset($data[$i]['tmpType']);

            for ($j = 0; $j < count($data[$i]['variants']); $j++) {
                $variantsArr = explode('|', $data[$i]['variants'][$j]);
                $data[$i]['variants'][$j] = array_combine(['id', 'title'], $variantsArr);

                if ($data[$i]['type'] == 'Ready to Wear') {
                    $tmp = $this->db->get_where('ready_wear', array('mc_product_id' => $data[$i]['id'], 'mc_variant_id' => $variantsArr[0]))->row_array();
                    $images = $this->CommonModel->get_custom_images($tmp);
                    $data[$i]['variants'][$j]['price'] = $tmp['price'] * 1;
                    $data[$i]['variants'][$j]['image_url'] = $images[0] . '';
                    $data[$i]['variants'][$j]['url'] = ApplicationConfig::ROOT_BASE . 'shop/products/' . $tmp['color'] . '/' . $tmp['shoe_name'];

                    $data[$i]['images'][] = array('id' => $data[$i]['variants'][$j]['id'] . '_Image_' . $i . '_' . $j, 'url' => $images[0] . '', 'variant_ids' => array($data[$i]['variants'][$j]['id']));

                    if ($data[$i]['image_url'] == '' || $data[$i]['image_url'] == null) {
                        $data[$i]['image_url'] = $images[0] . '';
//                        $data[$i]['url'] = ApplicationConfig::ROOT_BASE . 'shop/products/' . $tmp['color'] . '/' . $tmp['shoe_name'];
                    }
                } else {
                    $tmp = $this->db->select('last_styles.*,last_item_def.def_code')
                        ->join('last_item_def', 'last_item_def.last_item_def_id = last_styles.last_id')
                        ->get_where('last_styles', array('last_item_def.last_name' => $variantsArr[1], 'last_styles.style_name' => $tmpType))->row_array();
                    if (count($tmp)) {
                        $data[$i]['variants'][$j]['price'] = $tmp['price'] * 1;
                        $data[$i]['variants'][$j]['image_url'] = ApplicationConfig::IMG_BASE . $tmp['def_code'] . '/' . $tmp['folder_name'] . '/' . $tmp['stitch_folder'] . '/' . $tmp['img_file'] . '_A0.png';

                        $data[$i]['images'][] = array('id' => $data[$i]['variants'][$j]['id'] . '_Image_' . $i . '_' . $j, 'url' => $data[$i]['variants'][$j]['image_url'] . '', 'variant_ids' => array($data[$i]['variants'][$j]['id']));
                    }
                }
            }
        }

        $this->load->library('MailChimp');
        $storeId = $this->config->item('storeId');

        foreach ($data as $dataVal) {
            $check = $this->mailchimp->get('ecommerce/stores/' . $storeId . '/products/' . $dataVal['id']);

            if (isset($check['id'])) {
                $tmp = $this->mailchimp->patch('ecommerce/stores/' . $storeId . '/products/' . $check['id'], $dataVal);
            } else {
                $tmp = $this->mailchimp->post('ecommerce/stores/' . $storeId . '/products', $dataVal);
            }
            if (!isset($tmp['id'])) {
                echo '<pre>';
                print_r($tmp);
                print_r($dataVal);
            }
        }
    }

    public function addProductsToDB()
    {
        $this->db->truncate('mailchimp_products');
        $this->db->truncate('mailchimp_products_variant');

        $array = [];
        $array['RTW'] = $this->_addProductsRTW();
        $array['CS'] = $this->_addProductsCS();
        $array['CC'] = $this->_addProductsCC();
        echo '<pre>';
        print_r($array);
    }

    private function _addProductsCS()
    {
        $cs = $this->db->select('last_styles.last_style_id,last_styles.style_name,last_item_def.last_item_def_id,last_item_def.last_name')
            ->join('last_item_def', 'last_item_def.last_item_def_id = last_styles.last_id AND last_item_def.def_code != "rtw"')
            ->order_by('last_styles.style_name')
            ->get('last_styles')->result_array();

        $dispArray = [];
        foreach ($cs as $csVal) {
//            $mcId = 'CS_' . $this->_formatName(str_replace('The ', '', $csVal['last_name'])) . '_' . $this->_formatName($csVal['style_name']);
            $mcId = 'CS_' . $this->_formatName($csVal['style_name']);
            $name = $this->_formatName($csVal['last_name'], true);
            $type = $this->_formatName($csVal['style_name'], true);

            $mc_variant_id = $mcId . '_' . $this->_formatName(str_replace('The ', '', $csVal['last_name']));
            $variant = $name;

            $checkProd = $this->db->get_where('mailchimp_products', array('mc_id' => $mcId, 'shoe_type' => 'CS'))->row_array();
            if (count($checkProd) == 0) {
                $this->db->insert('mailchimp_products', array(
                    'shoe_type' => 'CS', 'mc_id' => $mcId,
                    'name' => ucwords(str_replace("_", ' ', $type)), 'type' => $type,
                ));
                $prodId = $this->db->insert_id();
            } else {
                $prodId = $checkProd['id'];
            }

            $variArray = array('product_id' => $prodId, 'mc_variant_id' => $mc_variant_id, 'title' => $variant);
            $checkVariant = $this->db->get_where('mailchimp_products_variant', $variArray)->num_rows();
            if ($checkVariant == 0) {
                $this->db->insert('mailchimp_products_variant', $variArray);

                $dispArray[$prodId]['id'] = $mcId;
                $dispArray[$prodId]['title'] = ucwords(str_replace("_", ' ', $type));
                $dispArray[$prodId]['type'] = $type;
                $dispArray[$prodId]['variants'][] = array('id' => $mc_variant_id, 'title' => $variant);
            }
        }
        return $dispArray;
    }

    private function _addProductsCC()
    {
        $cc = $this->db->select('cc.style_name,ln.last_name,CONCAT(ln.last_name," ",cc.style_name) as last_style', false)
            ->join('last_item_def as ln', 'ln.last_item_def_id = cc.last_id')
            ->group_by('last_style')
            ->order_by('cc.style_name')
            ->get_where('custom_collection as cc', array('cc.shoe_name !=' => '', 'cc.style_name !=' => '', 'cc.last_id >' => '0'))->result_array();
        $dispArray = [];

        foreach ($cc as $ccVal) {
//            $mcId = 'CS_' . $this->_formatName(str_replace('The ', '', $ccVal['last_name'])) . '_' . $this->_formatName($ccVal['style_name']);
            $mcId = 'CS_' . $this->_formatName($ccVal['style_name']);
            $name = $this->_formatName($ccVal['last_name'], true);
            $type = $this->_formatName($ccVal['style_name'], true);

            $mc_variant_id = $mcId . '_' . $this->_formatName(str_replace('The ', '', $ccVal['last_name']));
            $variant = $name;

            $checkProd = $this->db->get_where('mailchimp_products', array('mc_id' => $mcId, 'shoe_type' => 'CS'))->row_array();
            if (count($checkProd) == 0) {
                $this->db->insert('mailchimp_products', array(
                    'shoe_type' => 'CS', 'mc_id' => $mcId,
                    'name' => ucwords(str_replace("_", ' ', $type)), 'type' => $type,
                ));
                $prodId = $this->db->insert_id();
            } else {
                $prodId = $checkProd['id'];
            }

            $variArray = array('product_id' => $prodId, 'mc_variant_id' => $mc_variant_id, 'title' => $variant);
            $checkVariant = $this->db->get_where('mailchimp_products_variant', $variArray)->num_rows();
            if ($checkVariant == 0) {
                $this->db->insert('mailchimp_products_variant', $variArray);
            }

            $dispArray[$prodId]['id'] = $mcId;
            $dispArray[$prodId]['title'] = ucwords(str_replace("_", ' ', $type));
            $dispArray[$prodId]['type'] = $type;
            $dispArray[$prodId]['variants'][] = array('id' => $mc_variant_id, 'title' => $variant);
        }

        return $dispArray;
    }

    private function _addProductsRTW()
    {
        $rtw = $this->db->get('ready_wear')->result_array();

        $dispArray = [];

        foreach ($rtw as $rtwVal) {
            $mcId = 'RTW_' . $this->_formatName($rtwVal['shoe_name']) . '_' . $this->_formatName($rtwVal['style_name']);
            $name = $this->_formatName($rtwVal['shoe_name'], true);
            $type = $this->_formatName($rtwVal['style_name'], true);

            $mc_variant_id = 'RTW_' . $this->_formatName($rtwVal['shoe_name']) . '_' . $this->_formatName($rtwVal['color']);
            $variant = $this->_formatName($rtwVal['color'], true);

            $checkProd = $this->db->get_where('mailchimp_products', array('mc_id' => $mcId, 'shoe_type' => 'RTW'))->row_array();
            if (count($checkProd) > 0) {
                $prodId = $checkProd['id'];
            } else {
                $this->db->insert('mailchimp_products', array(
                    'shoe_type' => 'RTW', 'mc_id' => $mcId,
                    'name' => $name, 'type' => $type,
                ));

                $prodId = $this->db->insert_id();
            }

            $variArray = array('product_id' => $prodId, 'mc_variant_id' => $mc_variant_id, 'title' => $variant);
            $checkVariant = $this->db->get_where('mailchimp_products_variant', $variArray)->num_rows();
            if ($checkVariant == 0) {
                $this->db->insert('mailchimp_products_variant', $variArray);
            }

            $this->db->where('id', $rtwVal['id'])->update('ready_wear', array('mc_product_id' => $mcId, 'mc_variant_id' => $mc_variant_id));

            $dispArray[$prodId]['id'] = $mcId;
            $dispArray[$prodId]['title'] = $name;
            $dispArray[$prodId]['type'] = $type;
            $dispArray[$prodId]['variants'][] = array('id' => $mc_variant_id, 'title' => $variant);
        }
        return $dispArray;
    }

    private function _formatName($str, $allowSpace = false)
    {
        $str = ucwords($str);
        if (!$allowSpace) {
            $str = str_replace(' ', '_', $str);
        }

        return $str;
    }

}
