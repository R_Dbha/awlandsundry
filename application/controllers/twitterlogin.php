<?php

class TwitterLogin extends CI_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->config('social');

        $this->data['consumer_key'] = $this->config->item('twitter_consumer_key');
        $this->data['consumer_secret'] = $this->config->item('twitter_consumer_key_secret');
    }

    public function index()
    {
        $this->load->library('twitter_oauth', $this->data);

        $token = $this->twitter_oauth->getRequestToken();

        $_SESSION['oauth_request_token'] = $token['oauth_token'];
        $_SESSION['oauth_request_token_secret'] = $token['oauth_token_secret'];

        $request_link = $this->twitter_oauth->getAuthorizeURL($token);

        $data['link'] = $request_link;
        echo $data['link'];
        $this->load->view('twitter/home', $data);
    }

    public function get_login_link()
    {
        $this->load->library('twitter_oauth', $this->data);

        $token = $this->twitter_oauth->getRequestToken();

        $_SESSION['oauth_request_token'] = $token['oauth_token'];
        $_SESSION['oauth_request_token_secret'] = $token['oauth_token_secret'];

        $request_link = $this->twitter_oauth->getAuthorizeURL($token);

        return $request_link;
    }

    public function access()
    {
        $this->data['oauth_token'] = $_SESSION['oauth_request_token'];
        $this->data['oauth_token_secret'] = $_SESSION['oauth_request_token_secret'];
        $this->load->library('twitter_oauth', $this->data);
        $oauth_verifier = $_GET['oauth_verifier'];
        $tokens = $this->twitter_oauth->getAccessToken($oauth_verifier);
        $_SESSION['oauth_access_token'] = $tokens['oauth_token'];
        $_SESSION['oauth_access_token_secret'] = $tokens['oauth_token_secret'];

        $user_info = $this->twitter_oauth->get('account/verify_credentials');
        print_r($user_info);
    }

    public function logout()
    {
        session_destroy();
        $this->load->view('twitter/logout');
    }

}
