<?php

class GiftCard extends CI_Controller
{

    public $data;
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('cart');
        $this->load->library('form_validation');
        $this->load->model('GiftCardModel');
        $this->load->model('OrderModel');
    }

    private function check_privilege()
    {
        $this->user = $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if (!($user['user_type_id'] == 1 || $user['user_type_id'] == 4)) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
    }

    public function index()
    {
        $data = array();
        if ($this->input->post() !== false) {
            $form_data = $this->input->post();
            if ($this->input->post('choose_amount') === "") {
                $this->form_validation->set_rules('custom_amount', 'Amount', 'required');
            }
            $this->form_validation->set_rules('sender_name', 'Sender Name', 'required');
            $this->form_validation->set_rules('sender_email', 'Sender Email', 'required|valid_email');
            $this->form_validation->set_rules('recipient_name', 'Recipient Name', 'required');
            $this->form_validation->set_rules('recipient_email', 'Recipient Email', 'required|valid_email');
            $this->form_validation->set_rules('message', 'Message', '');
            if ($this->form_validation->run() == false) {
                $data = $form_data;
                $data['errors'] = $this->form_validation->error_array();
            } else {
                $form_data['card_number'] = $this->generateGiftCardNumber(10);
                $form_data['is_active'] = 0;
                $form_data['amount'] = ($form_data['choose_amount'] !== '') ? $form_data['choose_amount'] : $form_data['custom_amount'];
                unset($form_data['custom_amount']);
                unset($form_data['choose_amount']);
                unset($form_data['submit']);
                $data = $form_data;
                $giftcard_id = $this->GiftCardModel->insert_gift_card_details($form_data);

                $cart_data = array(
                    'id' => $this->session->userdata('session_id') . "_CS_" . sizeof($this->cart->contents()),
                    'qty' => 1,
                    'price' => $data['amount'],
                    'name' => 'Gift Card',
                    'options' => array('giftcard_id' => $giftcard_id,
                        'sender_email' => $data['sender_email'],
                        'sender_name' => $data['sender_name'],
                        'recipient_email' => $data['recipient_email'],
                        'recipient_name' => $data['recipient_name'],
                        'card_number' => $data['card_number'],
                        'message' => $data['message'],
                        'time_zone' => '',
                        'send_mail_on' => $data['send_mail_on'],
                    ),
                );
                $this->cart->insert($cart_data);
                redirect('cart', 'refresh');
            }
        }

        $data['bodyclass'] = 'gift_card';
        $data['additional_js'] = array('giftcard', 'jquery.datetimepicker.full.min');
        $data['additional_css'] = 'css/jquery.datetimepicker';
        $data['page_title'] = 'Purchase Gift Card - Awl & Sundry';
        $data['meta'] = '<meta name="description" content="Enter your Awl & Sundry gift card number and your discount will be reflected at checkout." >';
        $this->load->view('gift_card', $data);
    }

    public function sent_gift_card_mails()
    {
        $gift_cards = $this->GiftCardModel->get_gift_cards_to_be_delivered_now();
        $i = 0;
        if ($gift_cards !== null) {
            foreach ($gift_cards as $key => $value) {
                $this->send_gift_card_email($value['order_id']);
                $this->GiftCardModel->change_status($value['id']);
                $i++;
            }
            echo 'Sent ' . $i . ' Gift Cards ';
        } else {
            echo 'Sent ' . $i . ' Gift Cards ';
        }
    }

    public function send_gift_card_email($order_id)
    {
        $this->load->model('OrderModel');
        $options = $this->OrderModel->gift_card_order($order_id);
        $this->load->library('email');
        $data['recipient_name'] = $options['recipient_name'];
        $data['sender_name'] = $options['sender_name'];
        $data['card_number'] = $options['card_number'];
        $data['message'] = $options['message'];
        $data['giftcard_price'] = $options['amount'];
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to($options['recipient_email']);
        $this->email->cc($options['sender_email']);
        $this->email->bcc($this->config->item('contact_email'));
        $this->email->subject('You\'ve been sent a gift from ' . ucwords($data['sender_name']));
        $msg = $this->load->view('emails/giftCardEmail', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    private function generate_options($array = array(), $from = 1, $to = 12, $selected = 0)
    {
        $options = '';
        if (sizeof($array) == 0) {
            if ($from >= 0 && $to > 0) {
                for ($i = $from; $i <= $to; $i++) {
                    $text = (isset($selected) && $selected == $i) ? 'selected="selected" ' : '';
                    $options .= '<option ' . $text . ' value="' . $i . '" >' . $i . '</option>';
                }
            }
        } else {
            foreach ($array as $key => $value) {
                $text = (isset($selected) && $selected == $value['id']) ? 'selected="selected" ' : '';
                $options .= '<option value = "' . $value['id'] . '" ' . $text . ' >' . $value['name'] . '</option>';
            }
        }
        return $options;
    }

    private function generateGiftCardNumber($length = 8)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    public function getGiftCard()
    {
        $result = array("IsValid" => true);
        $giftcard_number = $this->input->post('giftcard_number');
        $user = $this->session->userdata('user_details');
        $this->load->model('ShoeModel');
        if ($user != null) {
            $email = $user['email'];
            $giftcard_detail = $this->GiftCardModel->getGiftCard($email, $giftcard_number);
            if ($giftcard_detail != null) {
                $giftcard_id = $giftcard_detail[0]['id'];
                $giftcard_balance = $this->GiftCardModel->getBalance($giftcard_id);
                if ($giftcard_balance > 0) {
                    $total = $this->cart->total();
                    if ($giftcard_balance >= $total) {
                        $result['netAmt'] = 0;
                        $result['discount'] = $total;
                    } else {
                        $result['netAmt'] = $total - $giftcard_balance;
                        $result['discount'] = $giftcard_balance;
                    }
                    if ($this->session->userdata('cart_discount') != false) {
                        $cart_discount = $this->session->userdata('cart_discount');
                        if (isset($cart_discount['promo_discount']) && $cart_discount['promo_discount'] != 0) {
                            $discountAmt = ($result['netAmt'] * $cart_discount['promo_discount']) / 100;
                            $result['netAmt'] = $result['netAmt'] - $discountAmt;
                        } elseif (isset($cart_discount['promo_discount_amount']) && $cart_discount['promo_discount_amount'] != 0) {
                            $discountAmt = $cart_discount['promo_discount_amount'];
                            $result['netAmt'] = $result['netAmt'] - $discountAmt;
                        }
                    }
                    $cartGiftCardDetail['discount_perc'] = round(($total - $result['netAmt']) / $total * 100, 2);
                    $cartGiftCardDetail['promo_assign_id'] = isset($cart_discount['promo_assign_id']) ? $cart_discount['promo_assign_id'] : 0;
                    $cartGiftCardDetail['discount'] = $total - $result['netAmt'];
                    $cartGiftCardDetail['promo_discount'] = isset($cart_discount['promo_discount']) ? $cart_discount['promo_discount'] : 0;
                    $cartGiftCardDetail['promo_discount_amount'] = isset($cart_discount['promo_discount_amount']) ? $cart_discount['promo_discount_amount'] : 0;
                    $cartGiftCardDetail['giftcard_discount'] = $result['discount'];
                    $cartGiftCardDetail['giftcard_id'] = $giftcard_id;
                    $cartGiftCardDetail['total'] = $total;
                    $result['discount'] = $total - $result['netAmt'];
                    $this->session->unset_userdata('cart_discount');
                    $this->session->set_userdata('cart_discount', $cartGiftCardDetail);
                    $result['test'] = $this->session->userdata('cart_discount');
                } else {
                    $result['IsValid'] = false;
                    $result['message'] = 'This giftcard had been already used/invalid.';
                }
            } else {
                $result['IsValid'] = false;
                $result['message'] = 'Invalid giftcard number';
            }
        } else {
            $result['IsValid'] = false;
            $result['message'] = 'Please login';
        }
        echo json_encode($result);
    }

    public function view()
    {
        $this->check_privilege();
        $giftcard_orders = $this->GiftCardModel->getGiftCardOrders();
        $this->data['giftcards'] = $giftcard_orders['rows'];
        $this->data['user'] = $this->user;
        $this->data['additional_js'] = array('admin/giftcard');
        $this->load->view('admin/giftcard', $this->data);
    }

    public function show_details($giftcard_id)
    {
        $giftcard_orders = $this->GiftCardModel->getGiftCardOrders(0, $giftcard_id);
        $this->data['giftcard'] = $giftcard_orders['rows'][0];
        $data = $this->OrderModel->getbillingNshippingDetails($giftcard_orders['rows'][0]['order_id']);
        $this->data['billing'] = $data['billing'];
        $this->data['usage'] = $this->GiftCardModel->get_giftcard_usage($giftcard_id);
        $this->load->view('admin/giftcard_details', $this->data);
    }

}
