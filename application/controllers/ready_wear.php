<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ready_Wear extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("CommonModel");
        $this->load->model('OrderModel');
        $this->load->model('UserModel');
        $this->load->model('ShoeModel');
    }

    public function index()
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }

        $collections = $this->CommonModel->get_ready_wear();
        foreach ($collections as $k => $item) {
            $images = $this->CommonModel->get_custom_images($item);
            $collections[$k]['image'] = preg_replace('/.jpg/', '_medium.jpg', $images);
            $similar_shoes = $this->CommonModel->get_similar_ready_wear_shoe($item);
            $collections[$k]['similar_shoes'] = $similar_shoes;
        }

        $data['collections'] = $collections;
        $data['bodyclass'] = "shop";
        $data['additional_js'] = array('ready_wear');
        $this->load->view('ready_wear', $data);
    }

    public function products($color, $name)
    {
        $color = preg_replace('/-/', ' ', urldecode($color));
        $item_id = $this->CommonModel->get_ready_wear_item_by_name($color, urldecode($name));
        $this->details($item_id);
    }

    public function details($item_id = '')
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }

        $data = array();
        if (isset($item_id)) {
            $item = $this->CommonModel->get_ready_wear_item($item_id);
            $similar_shoes = $this->CommonModel->get_similar_ready_wear_shoe($item);
            $item['similar_shoes'] = $similar_shoes;
        }

        $images = $this->CommonModel->get_custom_images($item);
        $content_url = base_url() . 'shop/products/' . $item['style_name'] . '/' . $item['color'] . '/' . $item['shoe_name'];
        $image = $images[0];

        $this->get_meta_and_title($item['style_name'], $meta, $item['color'], $item['shoe_name']);

        $meta .= <<<EOD
            <meta property="og:title" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!" />
            <meta property="og:url" content="$content_url" />
            <meta property="og:site_name" content="https://www.awlandsundry.com/" />
            <meta property="og:image" content="$image" />
            <meta property="og:description" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!" />
            <meta property="fb:app_id" content="250777875062389" />

            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:site" content="@awlandsundry">
            <meta name="twitter:creator" content="@awlandsundry">
            <meta name="twitter:title" content="&nbsp;">
            <meta name="twitter:description" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!">
            <meta name="twitter:image" content="$image">
            <meta name="twitter:url" content="$content_url" >
            <meta name="twitter:domain" content="awlandsundry.com">
            <meta name="twitter:app:id:iphone" content="">
            <meta name="twitter:app:id:ipad" content="">
            <meta name="twitter:app:id:googleplay" content="">
            <meta name="twitter:app:url:iphone" content="">
            <meta name="twitter:app:url:ipad" content="">
            <meta name="twitter:app:url:googleplay" content="">


EOD;
        $data['url'] = $content_url;
        $data['item'] = $item;
        $data['images'] = $images;
        $data['alt'] = 'Handmade custom ' . $item['style_name'] . ' shoes in black calf leather. ';
        $data['meta'] = $meta;
        $data['page_title'] = 'Shop - ' . ucfirst($item['shoe_name']) . ' | Handmade Custom ' . ucfirst($item['style_name']) . ' ' . ucfirst($item['color']) . ' Shoes For Men';
        $data['additional_js'] = array('jquery.sumoselect.min', 'ready_wear_details', 'owlcarousel/owl.carousel.min', 'jquery.nav');
        $data['bodyclass'] = 'classic_1 ready_wear_details';
        $data['additional_css'] = array('css/sumoselect', 'css/owlcarousel/owl.carousel.min');
        $this->load->view('ready_wear_details', $data);
    }

    private function get_meta_and_title($style_name, &$meta, $type = '', $shoe_name = '')
    {
        $meta = '<meta name="description" content="Shop from our top handmade ' . ucfirst($style_name);
        if ($type !== '') {
            $meta .= ' ' . ucfirst($type);
        }
        $meta .= ' shoes collection. Free shipping and 30 day exchange. ">';
        if ($shoe_name !== '') {
            $meta = '<meta name="description" content="Shop ' . ucfirst($shoe_name) . ' - our top handmade ' . ucfirst($style_name);
            $meta .= ' ' . ucfirst($type);
            $meta .= ' shoes collection. Free shipping and 30 day exchange. ">';
        }
    }

    public function get_shoe_source()
    {
        $details = $this->input->post();
        $src = $this->CommonModel->get_ready_wear_img_src($details);
        $src = preg_replace('/.jpg/', '_medium.jpg', $src);
        echo json_encode($src);
    }

}
