<?php

class ShoeCart extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function addtocart($designId = null)
    {
        $this->load->helper('Common');
        $shoe = array();
        $name = 'Custom Shoe';
        $cartId = $this->session->userdata('cartId');
        if ($designId == null) {
            $shoe = $this->session->userdata('step_data');
        } else {
            $this->load->model('ShoeModel');
            $shoedetail = $this->ShoeModel->ge_design_model($designId);
            if ($cartId != null) {
                $shoe = $this->session->userdata('step_data');
            } else {
                $shoe = json_decode($shoedetail['shoemodel'], true);
            }

            $sesShoe = $this->session->userdata('step_data');
            if ($this->input->post('left_size') !== false) {
                $size['left']['text'] = $this->input->post('left_size');
                $size['right']['text'] = $this->input->post('right_size');
                $size['right']['width'] = $this->input->post('right_width');
                $size['left']['width'] = $this->input->post('left_width');
                $shoe['size'] = $size;
            } else {
                $shoe['size'] = $sesShoe['size'];
            }
            $name = $shoedetail['name'];
            // TODO: Add monogram
        }

        $data = array(
            'id' => $this->session->userdata('session_id') . "_CS_" . sizeof($this->session->userdata('cart_contents')),
            'qty' => 1,
            'price' => getPricing($shoe),
            'name' => $name,
            'options' => array(
                "shoe" => json_encode($shoe),
            ),
        );
        if ($cartId != null) {
            $this->removecartitem($cartId);
        }
        $this->cart->insert($data);
    }

    public function getcartitems()
    {
        $this->load->view("common/cart_items");
    }

    public function removecartitem($id = null)
    {
        if ($id == null) {
            $id = $this->input->post('cartId');
        }

        $data = array(
            'rowid' => $id,
            'qty' => 0,
        );
        $this->cart->update($data);
        $this->cart->deleteCartItem($data);
        removeMailChimpCartItem($id);
        $this->session->unset_userdata('cartId');
    }

    public function update_quantity($id = null, $quantity = null)
    {
        if ($id == null) {
            $id = $this->input->post('cartId');
        }

        if ($quantity == null) {
            $quantity = $this->input->post('quantity');
        }

        $data = array(
            'rowid' => $id,
            'qty' => $quantity,
        );
        $this->cart->update($data);
        $cart_contents = $this->cart->contents();
        $total = 0;
        if (is_array($cart_contents)) {
            foreach ($cart_contents as $key => $item) {
                $total += $item['subtotal'];
            }
        }
        $amount = $total;
        $store_credit = 0;
        if ($this->session->userdata('user_details') != false) {
            $user = $this->session->userdata('user_details');
            $res = $this->calculate_store_credit_discount($user, $total);
            $amount = $amount - $res['amount'];
            $store_credit = $res['amount'];
        }
        $cart_data['total'] = number_format($amount, 2);
        $cart_data['credit'] = number_format($store_credit, 2);
        $this->addGiftCardDetToSession($amount, $store_credit);

        updateMailchimpCartQty($id, $quantity);

        echo json_encode($cart_data);
    }

    public function calculate_store_credit_discount($user, $total_amount)
    {
        $this->load->model('UserModel');
        $storecredit_amount = 0;
        $res = $this->UserModel->getStoreCreditBalance($user['email'], $user['user_id']);
        $storecredit['amount'] = $res['credit_amount'];
        $storecredit['store_credit_id'] = $res['store_credit_id'];
        if ($storecredit['amount'] >= $total_amount) {
            $storecredit_amount = $total_amount;
        } else {
            $storecredit_amount = $storecredit['amount'];
        }
        $store['amount'] = $storecredit_amount;
        $store['store_credit_id'] = $storecredit['store_credit_id'];
        return $store;
    }

    public function addGiftCardDetToSession($total, $gc_discount, $store_credit_id = null)
    {
        if ($this->session->userdata('cart_discount') != false) {
            $cart_discount = $this->session->userdata('cart_discount');
            if (isset($cart_discount['promo_discount'])) {
                $total = $this->cart->total() - $cart_discount['promo_discount_amount'];
            } else {
                $total = $this->cart->total();
            }
        } else {
            $total = $this->cart->total();
        }
        if ($gc_discount > 0) {
            $card_disc_perc = ($gc_discount / $total) * 100;
        } else {
            $card_disc_perc = 0;
        }
        $result['netAmt'] = $total - $gc_discount;
        $result['discount'] = $gc_discount;
        $cartpromoDet['discount'] = $gc_discount;
        $cartpromoDet['giftcard_id'] = isset($cart_discount['store_credit_id']) ? $cart_discount['store_credit_id'] : $store_credit_id;
        if ($this->cart->total() > 0) {
            $cartpromoDet['discount_perc'] = ($this->cart->total() - $result['netAmt']) / $this->cart->total() * 100;
        } else {
            $cartpromoDet['discount_perc'] = 0;
        }
        $cartpromoDet['promo_discount'] = isset($cart_discount['promo_discount']) ? $cart_discount['promo_discount'] : 0;
        $cartpromoDet['promo_discount_amount'] = isset($cart_discount['promo_discount_amount']) ? $cart_discount['promo_discount_amount'] : 0;
        $cartpromoDet['giftcard_discount'] = round($card_disc_perc, 2);
        $cartpromoDet['promo_assign_id'] = isset($cart_discount['promo_assign_id']) ? $cart_discount['promo_assign_id'] : 0;
        $cartpromoDet['total'] = $this->cart->total();
        $this->session->unset_userdata('cart_discount');
        $this->session->set_userdata('cart_discount', $cartpromoDet);
    }

    public function edit_custom_item()
    {
        $item_id = $this->input->post('item_id');
        if ($item_id !== false) {
            $this->load->model("CommonModel");
            $shoe = $this->CommonModel->get_custom_collection_item($item_id);
            $shoe_design_id = $shoe['shoe_design_id'];
            $this->load->model('ShoeModel');
            $shoedetail = $this->ShoeModel->ge_design_model($shoe_design_id);
            $stepdata = json_decode($shoedetail['shoemodel'], true);
            if (isset($stepdata['style']['folder'])) {
                $styleArr = ['Derby' => 17, 'Loafer' => 18, 'Oxford' => 14, 'Monkstrap' => 16];
                $stepdata['style']['id'] = $styleArr[$stepdata['style']['folder']];
            }
            if (!isset($stepdata['style']['isLaceEnabled'])) {
                $this->load->model('CustomShoeModel');
                $model = $this->CustomShoeModel->get_style_defaults($stepdata['style']['id']);
                $stepdata['style']['isLaceEnabled'] = $model['style']['isLaceEnabled'];
            }
            $stepdata['angle'] = 'A0';
            $stepdata['monogram'] = array(
                'leftShoe' => 0,
                'rightShoe' => 0,
                'rightSide' => 0,
                'leftSide' => 0,
                'text' => '',
            );
            unset($stepdata['shoeDesignId']);
            $stepdata['reDesignId'] = $shoe_design_id;
            $this->session->set_userdata('ShoeModel', $stepdata);
            return true;
        }
    }

    public function add_custom_item_to_cart()
    {
        $item_id = $this->input->post('item_id');
        $left_size = $this->input->post('left_size');
        $right_size = $this->input->post('right_size');
        $right_width = $this->input->post('right_width');
        $left_width = $this->input->post('left_width');

        $m_left_size = $this->input->post('m_left_size');
        $m_right_size = $this->input->post('m_right_size');
        $m_left_width = $this->input->post('m_left_width');
        $m_right_width = $this->input->post('m_right_width');

        $m_lgirth = $this->input->post('m_lgirth');
        $m_rgirth = $this->input->post('m_rgirth');
        $m_linstep = $this->input->post('m_linstep');
        $m_rinstep = $this->input->post('m_rinstep');

        if ($item_id !== false) {
            $this->load->model("CommonModel");
            $item = $this->CommonModel->get_custom_collection_item($item_id);
            $shoe['shoe_design_id'] = $item['shoe_design_id'];
            $shoe['shoe_name'] = $item['shoe_name'];
            $shoe['custom_collection_id'] = $item_id;
            $shoe['reference_order_no'] = $item['order_no'];
        }
        if (isset($item) && is_array($item)) {
            $images = $this->CommonModel->get_custom_images($item);
        }

        $shoe['image'] = urlencode($images[0]);
        $shoe['style_name'] = $item['folder_name'];
        $shoe['last_name'] = $item['last_name'];

        $shoe['size']['left']['text'] = $left_size;
        $shoe['size']['right']['text'] = $right_size;
        $shoe['size']['right']['width'] = $right_width;
        $shoe['size']['left']['width'] = $left_width;

        $shoe['measurement']['left']['size'] = $m_left_size;
        $shoe['measurement']['right']['size'] = $m_right_size;
        $shoe['measurement']['right']['width'] = $m_right_width;
        $shoe['measurement']['left']['width'] = $m_left_width;

        $shoe['measurement']['left']['girth'] = $m_lgirth;
        $shoe['measurement']['right']['girth'] = $m_rgirth;
        $shoe['measurement']['left']['instep'] = $m_linstep;
        $shoe['measurement']['right']['instep'] = $m_rinstep;

        $shoe['description'] = '';
        $shoe['from'] = 'custom_collection';

        $prodId = getMailChimpProductId($shoe, 'CC');

        $data = array(
            'id' => $this->session->userdata('session_id') . "_CS_" . sizeof($this->cart->contents()),
            'qty' => 1,
            'price' => $item['price'],
            'name' => 'Custom Collection',
            'options' => array(
                "shoe" => json_encode($shoe),
            ),
        );
        $rowid = $this->cart->insert($data);

        $sessData = $this->session->userdata('user_details');
        if (isset($sessData['user_id']) && $rowid != '' && $rowid != null) {
            $data['cartId'] = 'AWL_CART_' . $sessData['user_id'];
            $data['rowid'] = $rowid;
            addCartToMailChimp($data, $prodId, $sessData);
        }
    }

    public function getShoeDesignIdFromCartId()
    {
        $this->load->model('ShoeModel');
        $id = $this->input->post('cartId');
        $options = $this->cart->product_options($id);
        $shoe = json_decode($options['shoe'], true);
        if (isset($shoe->shoe_design_id)) {
            $array['shoe_design_id'] = $shoe->shoe_design_id;
            $array['image_file'] = $this->ShoeModel->get_shoe_designs($shoe->shoe_design_id);
        } else {
            $userdetails = $this->session->userdata('user_details');

            $savedShoe = $this->ShoeModel->savemodel($userdetails['user_id'], $shoe);

            $array['shoe_design_id'] = $savedShoe['shoe_design_id'];
            $array['image_file'] = $savedShoe['image_file'];
        }

        $this->addtocart($array['shoe_design_id']);
        echo json_encode($array);
    }

    public function editcartitem()
    {
        $id = $this->input->post('cartId');
        $options = $this->cart->product_options($id);
        $shoe = json_decode($options['shoe'], true);
        if (isset($shoe['from']) && strtolower($shoe['from']) === 'custom_collection') {
            $shoe_design_id = $shoe['shoe_design_id'];
            $this->load->model('ShoeModel');
            $shoedetail = $this->ShoeModel->ge_design_model($shoe_design_id);
            $stepdata = json_decode($shoedetail['shoemodel'], true);
        } else {
            $stepdata = $shoe;
        }
        $this->session->set_userdata('ShoeModel', $stepdata);
        $this->session->set_userdata('cartId', $id);
        $this->removecartitem($id);
        echo true;
    }

    // public function getPromocode()
    // {
    //     $result = array("IsValid" => true);
    //     $promocode = $this->input->post('promocode');
    //     $user = $this->session->userdata('user_details');
    //     $this->load->model('ShoeModel');
    //     if ($user != null) {
    //         $email = $user['email'];
    //         $common_promocode = $this->ShoeModel->isCommonPromocode($email, $promocode);
    //         $promo_det = $this->ShoeModel->getPromoEmail($email);
    //         if ($promo_det != null) {
    //             $promo_user_id = $promo_det[0]['id'];
    //             $promocodedet = $this->ShoeModel->getvalidPromocodes($promo_user_id, $promocode);
    //             if ($promocodedet != null) {
    //
    //                 if ($this->session->userdata('cart_discount') != false) {
    //                     $cart_discount = $this->session->userdata('cart_discount');
    //                     if (isset($cart_discount['giftcard_discount'])) {
    //                         $total = $this->cart->total() - $cart_discount['giftcard_discount'];
    //                     } else {
    //                         $total = $this->cart->total();
    //                     }
    //                 } else {
    //                     $total = $this->cart->total();
    //                 }
    //                 if ($promocodedet[0]['discount_per'] != 0) {
    //                     $discount = $promocodedet[0]['discount_per'];
    //                     $discountAmt = ($total * $discount) / 100;
    //                 } elseif ($promocodedet[0]['discount_amount'] != 0) {
    //                     $discountAmt = ($total >= $promocodedet[0]['discount_amount']) ? $promocodedet[0]['discount_amount'] : $total;
    //                 }
    //                 $result['netAmt'] = $total - $discountAmt;
    //                 $result['discount'] = $this->cart->total() - $result['netAmt'];
    //                 $cartpromoDet['discount'] = $this->cart->total() - $result['netAmt'];
    //                 $cartpromoDet['giftcard_id'] = isset($cart_discount['giftcard_id']) ? $cart_discount['giftcard_id'] : 0;
    //                 $cartpromoDet['discount_perc'] = round(($this->cart->total() - $result['netAmt']) / $this->cart->total() * 100, 2);
    //                 $cartpromoDet['promo_discount'] = $promocodedet[0]['discount_per'];
    //                 $cartpromoDet['promo_discount_amount'] = $promocodedet[0]['discount_amount'];
    //                 $cartpromoDet['giftcard_discount'] = isset($cart_discount['giftcard_discount']) ? $cart_discount['giftcard_discount'] : 0;
    //                 $cartpromoDet['promo_assign_id'] = $promocodedet[0]['assign_id'];
    //                 $cartpromoDet['total'] = $this->cart->total();
    //                 $this->session->unset_userdata('cart_discount');
    //                 $this->session->set_userdata('cart_discount', $cartpromoDet);
    //                 $result['test'] = $this->session->userdata('cart_discount');
    //             } else {
    //                 $result['IsValid'] = false;
    //                 $result['message'] = 'This promocode had been already used /invalid/ expired .';
    //             }
    //         } else {
    //             $result['IsValid'] = false;
    //             $result['message'] = 'You have no promocode assigned';
    //         }
    //     } else if ($this->ShoeModel->isCommonPromo($promocode)) {
    //
    //         $promocodedet = $this->ShoeModel->getCommonPromoCodeDet($promocode);
    //         if ($promocodedet != null) {
    //             if ($this->session->userdata('cart_discount') != false) {
    //                 $cart_discount = $this->session->userdata('cart_discount');
    //                 if (isset($cart_discount['giftcard_discount'])) {
    //                     $total = $this->cart->total() - $cart_discount['giftcard_discount'];
    //                 } else {
    //                     $total = $this->cart->total();
    //                 }
    //             } else {
    //                 $total = $this->cart->total();
    //             }
    //             if ($promocodedet[0]['discount_per'] != 0) {
    //                 $discount = $promocodedet[0]['discount_per'];
    //                 $discountAmt = ($total * $discount) / 100;
    //             } elseif ($promocodedet[0]['discount_amount'] != 0) {
    //                 $discountAmt = ($total >= $promocodedet[0]['discount_amount']) ? $promocodedet[0]['discount_amount'] : $total;
    //             }
    //             $result['netAmt'] = $total - $discountAmt;
    //             $result['discount'] = $this->cart->total() - $result['netAmt'];
    //             $cartpromoDet['discount'] = $this->cart->total() - $result['netAmt'];
    //             $cartpromoDet['giftcard_id'] = isset($cart_discount['giftcard_id']) ? $cart_discount['giftcard_id'] : 0;
    //             $cartpromoDet['discount_perc'] = round(($this->cart->total() - $result['netAmt']) / $this->cart->total() * 100, 2);
    //             $cartpromoDet['promo_discount'] = $promocodedet[0]['discount_per'];
    //             $cartpromoDet['promo_discount_amount'] = $promocodedet[0]['discount_amount'];
    //             $cartpromoDet['giftcard_discount'] = isset($cart_discount['giftcard_discount']) ? $cart_discount['giftcard_discount'] : 0;
    //             $cartpromoDet['promo_assign_id'] = $promocodedet[0]['assign_id'];
    //             $cartpromoDet['promo_code'] = $promocode;
    //             $cartpromoDet['total'] = $this->cart->total();
    //             $this->session->unset_userdata('cart_discount');
    //             $this->session->set_userdata('cart_discount', $cartpromoDet);
    //             $result['test'] = $this->session->userdata('cart_discount');
    //         } else {
    //             $result['IsValid'] = false;
    //             $result['message'] = 'This promocode has been invalid/ expired .';
    //
    //         }
    //     } else {
    //         $result['IsValid'] = false;
    //         $result['message'] = 'Invalid Promocode';
    //     }
    //     echo json_encode($result);
    // }

    public function getPromocode()
    {
        $result = array(
            "IsValid" => true,
        );
        $promocode = $this->input->post('promocode');
        $user = $this->session->userdata('user_details');
        $this->load->model('ShoeModel');
        if ($user != null) { // || ($this->is_common_promocode($promocode))) {
            $email = $user['email'];
            $common_promocode = $this->ShoeModel->isCommonPromocode($email, $promocode);
            $promo_det = $this->ShoeModel->getPromoEmail($email);
            if ($promo_det != null) {
                $promo_user_id = $promo_det[0]['id'];
                $promocodedet = $this->ShoeModel->getvalidPromocodes($promo_user_id, $promocode);
                if ($promocodedet != null) {
                    if ($promocodedet[0]['maximum_value'] == 0 || $this->cart->total() <= $promocodedet[0]['maximum_value']) {
                        if ($this->session->userdata('cart_discount') != false) {
                            $cart_discount = $this->session->userdata('cart_discount');
                            if (isset($cart_discount['giftcard_discount'])) {
                                $total = $this->cart->total() - ($this->cart->total() * $cart_discount['giftcard_discount']) / 100;
                                $giftcard_discount_amt = ($this->cart->total() * $cart_discount['giftcard_discount']) / 100;
                            } else {
                                $total = $this->cart->total();
                                $giftcard_discount_amt = 0;
                            }
                        } else {
                            $total = $this->cart->total();
                        }
                        if ($cart_discount['promo_discount'] == 0 && $cart_discount['promo_discount_amount'] == 0) {
                            if ($promocodedet[0]['discount_per'] != 0) {
                                $promo_discount_per = $promocodedet[0]['discount_per'];
                                $promo_discountAmt = round((round($total) * $promo_discount_per) / 100);
                            } elseif ($promocodedet[0]['discount_amount'] != 0) {
                                $promo_discountAmt = ($total >= $promocodedet[0]['discount_amount']) ? round($promocodedet[0]['discount_amount']) : round($total);
                            }
                        } else {
                            $promo_discount_per = $cart_discount['promo_discount'];
                            $promo_discountAmt = round($cart_discount['promo_discount_amount']);
                            if ($promocodedet[0]['discount_per'] != 0) {
                                $promo_discount_per = $promocodedet[0]['discount_per'];
                                $promo_discountAmt = round((round($total) * $promo_discount_per) / 100);
                            } elseif ($promocodedet[0]['discount_amount'] != 0) {
                                $promo_discountAmt = ($total >= $promocodedet[0]['discount_amount']) ? round($promocodedet[0]['discount_amount']) : round($total);
                            }
                        }
                        $result['netAmt'] = number_format(round(($this->cart->total() - $giftcard_discount_amt - $promo_discountAmt)), 2);
                        $result['promo_discount'] = number_format(round($promo_discountAmt), 2);
                        $result['discount'] = number_format(round($this->cart->total() - $result['netAmt']), 2);
                        $cartpromoDet['discount'] = round($this->cart->total() - $result['netAmt']);
                        $cartpromoDet['giftcard_id'] = isset($cart_discount['giftcard_id']) ? $cart_discount['giftcard_id'] : 0;
                        $cartpromoDet['discount_perc'] = (($this->cart->total() - $result['netAmt']) / $this->cart->total()) * 100;
                        $cartpromoDet['promo_discount'] = round($promo_discount_per); // $promocodedet[0]['discount_per'];
                        $cartpromoDet['promo_discount_amount'] = round($promo_discountAmt); // $promocodedet[0]['discount_amount'];
                        $cartpromoDet['giftcard_discount'] = isset($cart_discount['giftcard_discount']) ? $cart_discount['giftcard_discount'] : 0;
                        $cartpromoDet['promo_assign_id'] = $promocodedet[0]['assign_id'];
                        $cartpromoDet['total'] = round($this->cart->total());
                        $this->session->unset_userdata('cart_discount');
                        $this->session->set_userdata('cart_discount', $cartpromoDet);
                        $result['test'] = $this->session->userdata('cart_discount');
                    } else {
                        $result['IsValid'] = false;
                        $result['message'] = 'This promocode is not applicable for orders more than $' . $promocodedet[0]['maximum_value'];
                    }
                } else {
                    $result['IsValid'] = false;
                    $result['message'] = 'This promocode had been already used /invalid/ expired .';
                }
            } else {
                $result['IsValid'] = false;
                $result['message'] = 'You have no promocode assigned';
            }
        } else {
            $result['IsValid'] = false;
            $result['message'] = 'Please login';
        }
        echo json_encode($result);
    }

    public function rework_design()
    {
        $design_id = $this->input->post('item_id');
        $this->load->model('ShoeModel');
        $result = $this->ShoeModel->ge_design_model($design_id);
        $shoemodel = json_decode($result['shoemodel'], true);
        $shoemodel['monogram'] = array(
            'leftShoe' => 0,
            'rightShoe' => 0,
            'rightSide' => 0,
            'leftSide' => 0,
            'text' => '',
        );
        $shoemodel['angle'] = 'A0';
        if (!isset($shoemodel['style']['svg'])) {
            $details = $this->ShoeModel->get_SVG_details($shoemodel['style']['id']);
            $shoemodel['style']['svg'] = $details['svg_file'];
            $shoemodel['style']['color'] = $details['def_color'];
        }
        unset($shoemodel['shoeDesignId']);
        $shoemodel['reDesignId'] = $design_id;
        $this->session->set_userdata('ShoeModel', $shoemodel);
        return true;
    }

    public function add_readywear_item_to_cart()
    {
        $item_id = $this->input->post('item_id');
        $prodId = getMailChimpProductId($item_id, 'RTW');
        $left_size = $this->input->post('left_size');
        $right_size = $this->input->post('right_size');
        $right_width = $this->input->post('right_width');
        $left_width = $this->input->post('left_width');

        $m_left_size = $this->input->post('m_left_size');
        $m_right_size = $this->input->post('m_right_size');
        $m_left_width = $this->input->post('m_left_width');
        $m_right_width = $this->input->post('m_right_width');

        $m_lgirth = $this->input->post('m_lgirth');
        $m_rgirth = $this->input->post('m_rgirth');
        $m_linstep = $this->input->post('m_linstep');
        $m_rinstep = $this->input->post('m_rinstep');

        if ($item_id !== false) {
            $this->load->model("CommonModel");
            $item = $this->CommonModel->get_ready_wear_item($item_id);
            $shoe['shoe_design_id'] = $item['shoe_design_id'];
            $shoe['shoe_name'] = $item['shoe_name'];
            $shoe['custom_collection_id'] = $item_id;
            $shoe['reference_order_no'] = '';
        }
        if (isset($item) && is_array($item)) {
            $images = $this->CommonModel->get_custom_images($item);
        }

        $shoe['image'] = urlencode($images[0]);
        $shoe['style_name'] = $item['style_name'];
        $shoe['last_name'] = "The Ready To Wear";

        $shoe['size']['left']['text'] = $left_size;
        $shoe['size']['right']['text'] = $right_size;
        $shoe['size']['right']['width'] = $right_width;
        $shoe['size']['left']['width'] = $left_width;

        $shoe['measurement']['left']['size'] = '';
        $shoe['measurement']['right']['size'] = '';
        $shoe['measurement']['right']['width'] = '';
        $shoe['measurement']['left']['width'] = '';

        $shoe['measurement']['left']['girth'] = '';
        $shoe['measurement']['right']['girth'] = '';
        $shoe['measurement']['left']['instep'] = '';
        $shoe['measurement']['right']['instep'] = '';

        $shoe['description'] = '';
        $shoe['from'] = 'custom_collection';
        $data = array(
            'id' => $this->session->userdata('session_id') . "_CS_" . sizeof($this->cart->contents()),
            'qty' => 1,
            'price' => $item['price'],
            'name' => 'Ready To Wear',
            'options' => array(
                "shoe" => json_encode($shoe),
            ));
        $rowid = $this->cart->insert($data);

        $sessData = $this->session->userdata('user_details');
        if (isset($sessData['user_id']) && $rowid != '' && $rowid != null) {
            $data['cartId'] = 'AWL_CART_' . $sessData['user_id'];
            $data['rowid'] = $rowid;
            addCartToMailChimp($data, $prodId, $sessData);
        }
    }

    // public function processGiftCard()
    // {
    //     $data = null;
    //     $data['errors'] = null;
    //     $data['isLogin'] = false;
    //     if ($this->session->userdata('user_details') != null) {
    //         $data['isLogin'] = true;
    //         $user = $this->session->userdata('user_details');
    //         $gc_code = $this->input->post('code');
    //         $validations = $this->validate_giftCard($gc_code, $user['email'], $user['user_id']);
    //         if ($validations['IsValid']) {
    //             $data['IsValid'] = true;
    //             $this->load->model('UserModel');
    //             $cardAmount = $this->UserModel->getGiftCardAmount($gc_code, $user['email']);
    //             $credit_sum = array(
    //                 'user_id' => $user['user_id'],
    //                 'user_email' => $user['email'],
    //                 'credit_amount' => $cardAmount,
    //                 'date_added' => date('Y-m-d H:i:s'),
    //                 'date_modified' => date('Y-m-d H:i:s'),
    //                 'is_active' => 1,
    //             );
    //             $credit_det = array(
    //                 'gift_card_code' => $gc_code,
    //                 'card_amount' => $cardAmount,
    //                 'is_receipt' => 1,
    //                 'is_issue' => 0,
    //             );
    //             $this->UserModel->insertGiftcardDetails($credit_sum, $credit_det);
    //         } else {
    //             $data['IsValid'] = false;
    //             $data['errors'] = $validations['errors'];
    //         }
    //         if ($user != false) {
    //             $store_credit = $this->getStoreCreditBalance($data['subtotal'], $user['email'], $user['user_id']);
    //         } else {
    //             $store_credit = number_format(0);
    //         }
    //         $data['balance'] = $store_credit;
    //     }
    //     echo json_encode($data);
    // }
    // 
    // public function getStoreCreditBalance($total, $mail, $userId)
    // {
    //     $store_credit = number_format($this->UserModel->getStoreCreditBalance($mail, $userId));
    //     if ($store_credit >= $total) {
    //         $credit_amount = $total;
    //     } else {
    //         $credit_amount = $store_credit;
    //     }
    //     return $credit_amount;
    // }
    // 
    // public function validate_giftCard($gc_code, $userEmail, $userId)
    // {
    //     $this->load->model('UserModel');
    //     $isvalid = array("IsValid" => true, 'errors' => array());
    //     if (!$this->UserModel->IsValidCardNo($gc_code, $userEmail)) {
    //         $isvalid['IsValid'] = false;
    //         $isvalid['errors']['validCard'] = "This is not a valid gift card code";
    //     } else if ($this->UserModel->IsgiftCardProcessed($gc_code, $userId)) {
    //         $isvalid['IsValid'] = false;
    //         $isvalid['errors']['validCard'] = "This gift card is already used.";
    //     }
    //     return $isvalid;
    // }
}
