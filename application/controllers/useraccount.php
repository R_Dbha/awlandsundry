<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class UserAccount extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $sessData = $this->session->userdata('user_details');
        if ($sessData == null) {
            redirect('login');
        } else {
            if (isset($sessData['setCart']) && $sessData['setCart'] == '1') {
                unset($sessData['setCart']);
                $this->session->set_userdata('user_details', $sessData);
                addToMcCartAfterLogin($sessData['user_id']);
            }
        }
    }

    public function index()
    {
        if ($this->session->userdata('user_details') != null) {
            $user = $this->session->userdata('user_details');
            $data['bodyclass'] = "dashboard";
            $data['user'] = $user;
            $data['additional_css'] = 'css/sumoselect';
            $data['additional_js'] = array(
                'tinynav.min',
                'jquery.sumoselect.min',
                'stacktable',
                'dashboard',
            );
            $data['main_content'] = $this->load->view('account/dashboard_mainview', $data, true);
            $this->load->view('account/dashboard', $data);
        }
    }

    public function account_dashboard()
    {
        if ($this->session->userdata('user_details') != null) {
            $this->load->model('UserModel');
            $user = $this->session->userdata('user_details');
            $userDet = $this->UserModel->getUserInfo($user['user_id']);
            $data['address_list'] = $this->UserModel->get_user_address_details($user['user_id']);
            $data['user'] = $userDet;
            $data['orders'] = $this->UserModel->get_orders_status($user['user_id']);
            $this->load->view('account/dashboard_mainview', $data, false);
        }
    }

    public function account_info()
    {
        if ($this->session->userdata('user_details') != null) {
            $this->load->model('UserModel');
            $user = $this->session->userdata('user_details');
            $userDet = $this->UserModel->getUserInfo($user['user_id']);
            $data['user'] = $userDet;
            $this->load->view('account/dashboard_accountinfo', $data, false);
        }
    }

    public function address_book()
    {
        if ($this->session->userdata('user_details') != null) {
            $user = $this->session->userdata('user_details');
            $this->load->model('UserModel');
            $res['address_list'] = $this->UserModel->get_user_address_details($user['user_id']);
            if ($res != null) {
                $this->load->view('account/dashboard_addressbook', $res, false);
            } else {
                $this->load->view('account/dashboard_addressbook');
            }
        }
    }

    public function add_account_info($from = null, $addressId = null)
    {
        if ($this->session->userdata('user_details') != null) {
            $user = $this->session->userdata('user_details');
            $this->load->model('UserModel');
            if ($from == null) {
                $res['from'] = 'dashboard';
            } else if ($from == 'address_book') {
                $res['from'] = 'address_book';
            } else {
                $res['from'] = 'dashboard';
            }
            if ($addressId != null) {
                $res['data'] = $this->UserModel->getAddressInfoByAddressId($addressId, $user['user_id']);
                $res['states'] = $this->UserModel->get_all_states();
                $res['countries'] = $this->UserModel->get_countries();
                $this->load->view('account/dashboard_addaccount', $res);
            } else if ($this->session->userdata('user_details') != null) {
                $user = $this->session->userdata('user_details');
                $res['data']['country'] = 'United States'; // = NULL;
                $res['states'] = $this->UserModel->get_states();
                $res['countries'] = $this->UserModel->get_countries();
                $this->load->view('account/dashboard_addaccount', $res);
            }
        }
    }

    public function edit_accountinfo($from = 'main')
    {
        $res = null;
        if ($this->session->userdata('user_details') != null) {
            $this->load->model('UserModel');
            $user = $this->session->userdata('user_details');
            $res['user'] = $this->UserModel->getUserInfo($user['user_id']);
            if ($from == null) {
                $res['from'] = 'main';
            } else if ($from == 'acinfo') {
                $res['from'] = 'acinfo';
            } else {
                $res['from'] = 'main';
            }
            $this->load->view('account/dashboard_editaccount', $res, false);
        }
    }

    public function account()
    {
        if ($this->session->userdata('user_details') != null) {
            $user = $this->session->userdata('user_details');
            $data['bodyclass'] = "dashboard";
            $data['user'] = $user;

            $this->load->view('account/dashboard', $data);
        } else {
            redirect('', 'refresh');
        }
    }

    public function order_history()
    {
        $res = null;
        if ($this->session->userdata('user_details') != null) {
            $user = $this->session->userdata('user_details');
            $this->load->model('UserModel');
            $this->load->model('OrderModel');
            $orders = $this->UserModel->getOrderDetailsByUser($user['user_id']);
            $order_id = -1;
            $i = -1;
            $totalamt = 0;
            $disc = 0;
            $details = array();
            if (is_array($orders)) {
                foreach ($orders as $order) {
                    if ($order_id !== $order['order_id']) {
                        $order_id = $order['order_id'];
                        $i++;
                        $j = 0;
                        $totalamt = 0;
                        $disc = 0;
                        $details[$i]['order_id'] = $order['order_id'];
                        $details[$i]['invoice_no'] = $order['invoice_no'];
                        $details[$i]['add_date'] = $order['add_date'];
                        $details[$i]['status'] = $order['status'];
                        if ($order['status'] === 'order shipped') {
                            $details[$i]['shipping_carrier'] = trim(get_string_inbetween($order['status_desc'], 'Carrier : ', ', TrackingNo:'));
                            $details[$i]['tracking_number'] = trim(get_string_inbetween($order['status_desc'], 'TrackingNo:', ', Estimated Delivery date :'));
                        }
                    }
                    if ($order['design_id'] !== $order['item_id'] && $order['item_id'] !== "0" && $order['item_type'] != 'readywear') {
                        $details[$i]['items'][$j]['shop_images'] = $this->OrderModel->get_shop_images($order['item_id']);
                    }
                    if ($order['item_type'] == 'readywear') {
                        $details[$i]['items'][$j]['shop_images'] = $this->OrderModel->get_readytowear_images($order['item_id']);
                    }
                    $details[$i]['items'][$j]['image_file'] = $order['image_file'];
                    $details[$i]['items'][$j]['design_id'] = $order['design_id'];
                    $details[$i]['items'][$j]['item_id'] = $order['item_id'];
                    $details[$i]['items'][$j]['item_name'] = $order['item_name'];
                    $details[$i]['items'][$j]['last_name'] = $order['last_name'];
                    $details[$i]['items'][$j]['style_name'] = $order['style_name'];
                    if ($order['item_type'] == 'readywear') {
                        $details[$i]['items'][$j]['style_name'] = $this->OrderModel->get_readytowear_style($order['item_id']);
                    }
                    if (strtolower($order['item_name']) != 'gift card') {
                        $totalamt += $order['item_amt'];
                        $discountPerc = $order['discount_perc'];
                        if ($discountPerc !== 0) {
                            $disc = ($totalamt * $discountPerc) / 100;
                            $totalamt = $totalamt - $disc;
                        }
                    }
                    $details[$i]['net_amt'] = $totalamt;
                    $details[$i]['items'][$j]['item_amt'] = $order['item_amt'];
                    $details[$i]['items'][$j]['left_shoe'] = $order['left_shoe'];
                    $details[$i]['items'][$j]['right_shoe'] = $order['right_shoe'];
                    $details[$i]['items'][$j]['left_width'] = $order['left_width'];
                    $details[$i]['items'][$j]['right_width'] = $order['right_width'];
                    $j++;
                }
            }
            $data['details'] = $details;
            $this->load->view('account/dashboard_orderhistory', $data, false);
        }
    }

    public function save_address_details($addressId = null)
    {
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'fname',
                'label' => 'First Name',
                'rules' => 'required|max_length[200]',
            ),
        );
        $this->form_validation->set_rules($config);
        $this->load->model('UserModel');

        if ($this->input->post('first_name') != null) {
            $address = array(
                'user_id' => $this->input->post('userId'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
                'fax' => $this->input->post('fax'),
                'address1' => $this->input->post('address1'),
                'address2' => $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'zipcode' => $this->input->post('zipcode'),
                'date_created' => date('Y-m-d H:i:s'),
                'is_billing' => $this->input->post('is_billing'),
                'is_shipping' => $this->input->post('is_shipping'),
            );
            if ($addressId != null) {
                $res = $this->UserModel->update_address_details($address, $addressId);
            } else {
                $res = $this->UserModel->save_address_details($address);
            }
            if ($res) {
                updateCustomerAddressToMailChimp($address);
                echo true;
            } else {
                echo false;
            }
        }
    }

    public function update_userinfo()
    {
        $this->load->model('UserModel');
        if ($this->session->userdata('user_details') != null) {
            $user = $this->session->userdata('user_details');
            $validations = $this->validateaccount($user['user_id']);
            if ($validations['IsValid']) {
                $userinfo = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'email' => $this->input->post('email'),
                );
                if ($this->UserModel->updateUserinfo($userinfo, $user['user_id'])) {
                    $userDet = $this->UserModel->getUserInfo($user['user_id']);
                    updateCustomerToMailChimp($userDet);

                    $data['user'] = $userDet;
                }
            }
            echo true;
        }
    }

    private function validateaccount($userId)
    {
        $this->load->helper('email');
        $this->load->model('UserModel');
        $isvalid = array(
            "IsValid" => true,
            'errors' => array(),
        );
        if (trim($this->input->post('first_name')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['first_name'] = "Please give First name";
        }
        if (trim($this->input->post('last_name')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['last_name'] = "Please give Last name";
        }
        if (trim($this->input->post('email')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['email'] = "Email should not be Empty";
        } else if (!valid_email($this->input->post('email'))) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['emailvalid'] = "Email id is not valid";
        } else if (!$this->UserModel->unique_other_email($this->input->post('email'), $userId)) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['emailunique'] = "email already exists";
        }
        return $isvalid;
    }

    public function get_user_address_details($userId)
    {

    }

    public function deleteAddress()
    {
        $addressId = $this->input->post('addressId');
        $type = $this->input->post('type');
        $this->load->model('UserModel');
        if ($this->UserModel->deleteAddress($addressId, $type)) {
            echo true;
        } else {
            echo false;
        }
    }

    public function reset_password()
    {
        $res = null;
        if ($this->input->post('new') == "") {
            $error['new_pwd'] = "Please enter new password!!!";
        }
        if ($this->input->post('current') != null) {
            $this->load->model('UserModel');
            $current = $this->input->post('current');
            $new = $this->input->post('new');
            if ($this->session->userdata('user_details') != null) {
                $user = $this->session->userdata('user_details');
                if ($this->UserModel->changePwd($current, $new, $user['user_id'])) {
                    echo 1;
                } else {
                    $error['pwd'] = "Wrong password!";
                    $res['error'] = $error;
                    $this->load->view('account/dashboard_resetpassword', $res);
                }
            }
        } else {
            $this->load->view('account/dashboard_resetpassword', $res);
        }
    }

    public function view_order_details($orderId)
    {
        $data = null;
        if ($orderId != null) {
            if ($this->session->userdata('user_details') != null) {
                $user = $this->session->userdata('user_details');
                $this->load->model('UserModel');
                $this->load->model('OrderModel');
                $data['order'] = $this->UserModel->getbillingNshippingDetails($orderId, $user['user_id']);
                for ($j = 0; $j < sizeof($data['order']['order']['items']); $j++) {
                    if ($data['order']['order']['items'][$j]['design_id'] !== $data['order']['order']['items'][$j]['item_id'] && $data['order']['order']['items'][$j]['item_id'] !== "0") {
                        $data['order']['order']['items'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['order']['items'][$j]['item_id']);
                    }
                    if (isset($data['order']['order']['items'][$j]['shoemodel'])) {
                        $tmp = json_decode($data['order']['order']['items'][$j]['shoemodel'], true);
                        unset($data['order']['order']['items'][$j]['shoemodel']);
                        if (isset($tmp['monogram'])) {
                            $data['order']['order']['items'][$j]['monogram_text'] = (isset($tmp['monogram']['text'])) ? $tmp['monogram']['text'] : null;
                        }
                        if (isset($tmp['shoetree'])) {
                            $data['order']['order']['items'][$j]['shoetree'] = $tmp['shoetree'];
                        }
                        if (isset($tmp['sole'])) {
                            if (isset($tmp['sole']['colorId']) && isset($tmp['sole']['id'])) {
                                $data['order']['order']['items'][$j]['soleName'] = $this->db->get_where('style_sole', array('id' => $tmp['sole']['id']))->row('sole_name');
                                $data['order']['order']['items'][$j]['soleColorName'] = $this->db->get_where('style_sole_color', array('id' => $tmp['sole']['colorId']))->row('color_name');
                            }
                            if (isset($tmp['sole']['stitchId'])) {
                                $data['order']['order']['items'][$j]['soleWelt'] = $this->db->get_where('style_sole_stitches', array('id' => $tmp['sole']['stitchId']))->row('stitch_name');
                            }
                        }
                    }
                }
                for ($i = 0; $i < sizeof($data['order']['order']['ready_wear']); $i++) {
                    if ($data['order']['order']['ready_wear'][$i]['design_id'] !== $data['order']['order']['ready_wear'][$i]['item_id'] && $data['order']['order']['ready_wear'][$i]['item_id'] !== "0") {
                        $data['order']['order']['ready_wear'][$i]['shop_images'] = $this->OrderModel->get_readytowear_images($data['order']['order']['ready_wear'][$i]['item_id']);
                    }
                }
                $this->load->view('account/dashboard_order_details', $data, false);
            }
        }
    }

    public function gift_certificate()
    {
        $data = null;
        if ($this->session->userdata('user_details') != null) {
            $this->load->model('UserModel');
            $user = $this->session->userdata('user_details');
            $userDet = $this->UserModel->getUserInfo($user['user_id']);
            $res = $this->UserModel->getStoreCreditBalance($user['email'], $user['user_id']);
            if ($res != false) {
                $data['balance'] = $res['credit_amount'];
            } else {
                $data['balance'] = 0;
            }
            $data['user'] = $userDet;
        }
        $this->load->view('account/dashboard_giftcard', $data, false);
    }

    public function saved_designs()
    {
        $this->load->model('UserModel');
        $user = $this->session->userdata('user_details');
        $userId = $user['user_id'];
        $shoedesigns = $this->UserModel->get_shoe_designsByUser($userId, false);
        $data['shoedesigns'] = $shoedesigns;
        $this->load->view('account/saved_shoes', $data, false);
    }

    public function edit_savedshoe($shoeDesignId)
    {
        if ($shoeDesignId != null) {
            $this->load->model('ShoeModel');
            $shoedetail = $this->ShoeModel->ge_design_model($shoeDesignId);
            $shoe = json_decode($shoedetail['shoemodel'], true);
            $shoe['reDesignId'] = $shoeDesignId;
            if (!isset($shoe['style']['isLaceEnabled'])) {
                $this->load->model('CustomShoeModel');
                $model = $this->CustomShoeModel->get_style_defaults($shoe['style']['id']);
                $shoe['style']['isLaceEnabled'] = $model['style']['isLaceEnabled'];
            }
            $shoe['angle'] = 'A0';
            $this->session->set_userdata('ShoeModel', $shoe);
            echo 'create-a-custom-shoe/design-features';
        }
    }

    public function processGiftCard()
    {
        $data = null;
        $data['errors'] = null;
        if ($this->session->userdata('user_details') != null) {
            $user = $this->session->userdata('user_details');
            $gc_code = $this->input->post('code');
            $validations = $this->validate_giftCard($gc_code, $user['email'], $user['user_id']);
            if ($validations['IsValid']) {
                $this->load->model('UserModel');
                $cardAmount = $this->UserModel->getGiftCardAmount($gc_code, $user['email']);
                $credit_sum = array(
                    'user_id' => $user['user_id'],
                    'user_email' => $user['email'],
                    'credit_amount' => $cardAmount,
                    'date_added' => date('Y-m-d H:i:s'),
                    'date_modified' => date('Y-m-d H:i:s'),
                    'is_active' => 1,
                );
                $credit_det = array(
                    'gift_card_code' => $gc_code,
                    'card_amount' => $cardAmount,
                    'is_receipt' => 1,
                    'is_issue' => 0,
                );

                $this->UserModel->insertGiftcardDetails($credit_sum, $credit_det);
                $data['errors']['validCard'] = 'Your gift card is successfully added to store credit ';
            } else {
                $data['errors'] = $validations['errors'];
            }
        }
        $res = $this->UserModel->getStoreCreditBalance($user['email'], $user['user_id']);
        if ($res != false) {
            $data['balance'] = number_format($res['credit_amount'], 2);
        } else {
            $data['balance'] = number_format(0, 2);
        }
        $this->load->view('account/dashboard_giftcard', $data);
    }

    public function checkBalanceGiftCard()
    {
        $data = null;
        $data['errors'] = null;
        if ($this->session->userdata('user_details') != null) {
            $user = $this->session->userdata('user_details');
            $gc_code = $this->input->post('code');
            $validations = $this->validate_giftCard($gc_code, $user['email'], $user['user_id']);
            if ($validations['IsValid']) {
                $this->load->model('UserModel');
                $cardAmount = $this->UserModel->getGiftCardAmount($gc_code, $user['email']);
                $data['errors']['validCard'] = 'Your gift card balance amount is : $' . number_format($cardAmount, 2);
            } else {
                $data['errors'] = $validations['errors'];
            }
            $res = $this->UserModel->getStoreCreditBalance($user['email'], $user['user_id']);
            if ($res != false) {
                $data['balance'] = number_format($res['credit_amount'], 2);
            } else {
                $data['balance'] = number_format(0, 2);
            }
        }
        $this->load->view('account/dashboard_giftcard', $data);
    }

    public function validate_giftCard($gc_code, $userEmail, $userId)
    {
        $this->load->model('UserModel');
        $isvalid = array(
            "IsValid" => true,
            'errors' => array(),
        );
        if (!$this->UserModel->IsValidCardNo($gc_code, $userEmail)) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['validCard'] = "This is not a valid gift card code";
        } else if ($this->UserModel->IsgiftCardProcessed($gc_code, $userId)) {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['validCard'] = "This gift card is already used.";
        }
        return $isvalid;
    }

    public function delete_savedshoe($shoe_design_id)
    {
        $this->load->model('UserModel');
        if ($this->UserModel->delete_design($shoe_design_id)) {
            echo 'success';
        } else {
            echo 'failure';
        }
    }

    public function purchase_shoe($shoe_design_id)
    {
        $redirect = 'create-a-custom-shoe/sizing';
        $this->load->model('ShoeModel');
        $shoedetail = $this->ShoeModel->ge_design_model($shoe_design_id);
        $stepdata = json_decode($shoedetail['shoemodel'], true);
        $sizes = $stepdata['size'];
        if (isset($sizes['left']['text']) && isset($sizes['left']['text'])) {
            $user = $this->session->userdata('user_details');
            if ($user != null) {
                $res = $this->ShoeModel->isPreviouslyOrderd($user['user_id']);
                if ($res != null) {
                    $this->savedItemtoCart($shoedetail, $sizes['left']['text'], $sizes['right']['text'], "D", "D");
                } else {
                    $this->savedItemtoCart($shoedetail, $res['left_shoe'], $res['right_shoe'], "D", "D");
                }
                $redirect = 'cart';
            }
        }

        $this->session->set_userdata('ShoeModel', $stepdata);
        echo $redirect;
    }

    public function savedItemtoCart($shoedetail, $left_size, $right_size, $right_width, $left_width)
    {
        $shoe = json_decode($shoedetail['shoemodel'], true);
        $materials[] = $shoe['quarter']['material'];
        if ($shoe['toe']['isMaterial']) {
            $materials[] = $shoe['toe']["material"];
        }
        if ($shoe['style']['isVampEnabled']) {
            if ($shoe['vamp']['isMaterial']) {
                $materials[] = $shoe['vamp']["material"];
            }
        }
        if ($shoe['eyestay']['isMaterial']) {
            $materials[] = $shoe['eyestay']["material"];
        }
        if ($shoe['foxing']['isMaterial']) {
            $materials[] = $shoe['foxing']["material"];
        }
        $this->load->helper('Common');
        $shoe['size']['left']['text'] = $left_size;
        $shoe['size']['right']['text'] = $right_size;
        $shoe['size']['right']['width'] = $right_width;
        $shoe['size']['left']['width'] = $left_width;
        $shoe['description'] = '';
        $name = $shoedetail['name'];
        $data = array(
            'id' => $this->session->userdata('session_id') . "_CS_" . sizeof($this->cart->contents()),
            'qty' => 1,
            'price' => getPricing($shoe),
            'name' => $name,
            'options' => array(
                "shoe" => json_encode($shoe),
            ),
        );
        $rowid = $this->cart->insert($data);
        $prodId = getMailChimpProductId($shoe, 'CS');

        $sessData = $this->session->userdata('user_details');
        if (isset($sessData['user_id']) && $rowid != '' && $rowid != null) {
            $data['cartId'] = 'AWL_CART_' . $sessData['user_id'];
            $data['rowid'] = $rowid;
            addCartToMailChimp($data, $prodId, $sessData);
        }
    }

    public function trackyourordertest()
    {
        $this->load->library('aftership');
        $params = array();

        $this->aftership->get('dhl', '2254095771', array(
            'fields' => 'customer_name',
        ));
    }

    public function save_shoe_name($shoe_design_id)
    {
        $public_name = $this->input->post('public_name');
        $this->load->model('UserModel');
        $this->UserModel->change_public_name($public_name, $shoe_design_id);
    }

}
