<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';

class Transaction extends CI_Controller
{

    public function __Construct()
    {
        parent::__construct();

        $this->load->model('Stripe_trans');
        $this->load->model('ShoeModel');
        $this->load->model('OrderModel');
        $this->config->load('paypal');
        $this->config->load('klarna');
        $this->load->library('cart');

        $config = array(
            'Sandbox' => $this->config->item('Sandbox'), // Sandbox / testing mode option.
            'APIUsername' => $this->config->item('APIUsername'), // PayPal API username of the API caller
            'APIPassword' => $this->config->item('APIPassword'), // PayPal API password of the API caller
            'APISignature' => $this->config->item('APISignature'), // PayPal API signature of the API caller
            'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
            'APIVersion' => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
        );

        if ($config['Sandbox']) {
            // error_reporting(E_ALL);
        }

        $this->load->library('paypal/Paypal_pro', $config);
    }

    public function get_payment($id)
    {
        $tst=$this->Stripe_trans->get($id);
        echo "<pre>";
        print_r($tst);exit;
    }

    public function get_all_payment()
    {
        $tst=$this->Stripe_trans->get_all();
        echo "<pre>";
        print_r($tst);exit;
    }

    public function payByCard($order_id)
    {
        $user = $this->session->userdata('user_details');
        $ccsess_data = $this->session->userdata('ccdata');
        $token = $this->session->userdata('stripe_token');
        if ($ccsess_data['card_type'] != 'Paypal') {
            $arr = $this->OrderModel->getbillingNshippingDetails($order_id);
            $paymentDet = $this->OrderModel->getOrderDet($order_id);
            $billing = $arr['billing'][0];
            $shipping = $arr['shipping'][0];
            $order = $arr['order']['items'];
            $netAmount = 0;
            if ($paymentDet[0]['discount_perc'] != 0) {
                $netAmount = $paymentDet[0]['gross_amt'] - (($paymentDet[0]['gross_amt'] * $paymentDet[0]['discount_perc']) / 100) + $paymentDet[0]['tax_amount'] + $paymentDet[0]['shipping_price'];
            } else {
                $netAmount = $paymentDet[0]['gross_amt'] + $paymentDet[0]['tax_amount'] + $paymentDet[0]['shipping_price'];
            }
            $netAmount = round($netAmount, 2);
            if ($paymentDet[0]['discount_perc'] != 100) {
                $stripe_result = $this->Stripe_trans->insert($token, 'Order - ' . $order_id, round($netAmount * 100, 2));
                if ($stripe_result->paid === true) {
                    $this->update_order($order_id);
                    redirect('order-confirm/' . $order_id, 'refresh');
                } else {
                    log_message('ERROR', 'Payment Error');
                    $error = array('error' => $stripe_result->message);
                    $this->load->view('payment_error', $error);
                }
            } else {
                $this->update_order($order_id);
                redirect('order-confirm/' . $order_id, 'refresh');
            }
        } else {
            redirect('paypal/' . $order_id, 'refresh');
        }
    }

    public function payByKlarna($order_id)
    {
        $user = $this->session->userdata('user_details');
        $sess_klarna_id = $this->session->userdata('sess_klarna_id');

        $order = $this->OrderModel->getOrderDet($order_id);
        if ($user == false) {
            redirect('login');
        }
        if (sizeof($this->cart->contents()) == 0) {
            redirect('my-account', 'refresh');
        }
        if (isset($order['klarna_status']) && $order['klarna_status'] == "Error") {
            log_message('klarna_error', 'Klarna Order could not be Initiated');
            $this->session->set_flashdata('klarna_error', "Order could not be Initiated");
            redirect('cart');
        }
        $this->load->model('UserModel');
        $userId = $user['user_id'];

        $tot_amt = $this->getCartDetails();
        $disc_amt = 0;
        $disc = $this->session->userdata('cart_discount');

        $discount = 0;
        if ($disc['discount_perc'] == 0) {
            $disc_amt = $tot_amt;
        } else {
            if (isset($disc['discount'])) {
                $disc_amt = $tot_amt - $disc['discount'];
            } else {
                $discount = round($disc['discount_perc'], 2);
                $disc_amt = $tot_amt - round($tot_amt * $discount / 100, 2);
            }
        }
        $discAmt = $tot_amt - $disc_amt;

        $cartItems = array();
        $merchantData = array();
        $merchantData['user'] = $user;
        if (sizeof($this->cart->contents()) > 0) {
            $i = 0;
            $total_items = 0;
            $giftcard = 0;
            foreach ($this->cart->contents() as $items) {
                $price = $items['price'];
                $total_items += 1;
                $options = $this->cart->product_options($items['rowid']);
                if (strtolower($items['name']) == 'gift card') {
                    $giftcard += 1;
                    $cartItems[$i]['type'] = 'gift_card';
                    $cartItems[$i]['reference'] = $options['giftcard_id'];
                    $cartItems[$i]['name'] = $items['name'];
                    $cartItems[$i]['quantity'] = $items['qty'];
                    $cartItems[$i]["unit_price"] = $price * 100;
                    $cartItems[$i]["tax_rate"] = 0;
                    $cartItems[$i]["total_amount"] = $items['qty'] * $items['price'] * 100;
                    $cartItems[$i]["total_tax_amount"] = 0;

                    $merchantData['items'][$i]['name'] = 'gift card';
                    $merchantData['items'][$i]['giftcard_id'] = $options['giftcard_id'];
                    $merchantData['items'][$i]['recipient_name'] = $options['recipient_name'];
                    $merchantData['items'][$i]['sender_name'] = $options['sender_name'];
                    $merchantData['items'][$i]['card_number'] = $options['card_number'];
                    $merchantData['items'][$i]['message'] = $options['message'];
                    $merchantData['items'][$i]['price'] = $items['price'];
                    $merchantData['items'][$i]['recipient_email'] = $options['recipient_email'];
                    $merchantData['items'][$i]['sender_email'] = $options['sender_email'];
                    $i++;
                }
            }
            $arr = $this->OrderModel->getbillingNshippingDetails($order_id);
            if ($total_items > $giftcard) {
                foreach ($arr['order']['items'] as $items) {
                    if ($items['item_name'] == "Gift Card") {
                        continue;
                    }
                    $cartItems[$i]['type'] = 'physical';
                    $cartItems[$i]['reference'] = $items['design_id'];
                    $cartItems[$i]['name'] = $items['item_name'];
                    $cartItems[$i]['quantity'] = $items['quantity'];
                    $cartItems[$i]["unit_price"] = ($items['item_amt'] / $items['quantity']) * 100;
                    $cartItems[$i]["tax_rate"] = 0;
                    $cartItems[$i]["total_amount"] = $items['item_amt'] * 100;
                    $cartItems[$i]["total_tax_amount"] = 0;
                    $i++;
                }
            }

            $merchantData['total_items'] = $total_items;
            if ($disc['discount_perc'] != 0) {
                if ($disc['giftcard_id'] != 0) {
                    $reference = $disc['giftcard_id'];
                }
                if ($disc['promo_assign_id'] != 0) {
                    $reference = $disc['promo_assign_id'];
                }
                $cartItems[$i]['type'] = 'discount';
                $cartItems[$i]['reference'] = $reference;
                $cartItems[$i]['name'] = "discount";
                $cartItems[$i]['quantity'] = 1;
                $cartItems[$i]["unit_price"] = -$discAmt * 100;
                $cartItems[$i]["tax_rate"] = 0;
                $cartItems[$i]["total_amount"] = -$discAmt * 100;
                $cartItems[$i]["total_tax_amount"] = 0;
            }
        }
        $merchant_info = '';
        $merchant_info = json_encode($merchantData, true);
        $countries = array("ax", "al", "dz", "as", "ad", "ai", "aq", "ag", "ar", "am", "aw", "au", "at", "az", "bs", "bh", "bd", "bb", "be", "bz", "bj", "bm", "bt", "bo", "bq", "bw", "bv", "br", "io", "bn", "bg", "bf", "kh", "cm", "ca", "cv", "ky", "td", "cl", "cn", "cx", "cc", "co", "km", "cg", "ck", "cr", "ci", "hr", "cu", "cw", "cy", "cz", "dk", "dj", "dm", "do", "ec", "eg", "sv", "gq", "ee", "et", "fk", "fo", "fj", "fi", "fr", "gf", "pf", "tf", "ga", "gm", "ge", "de", "gh", "gi", "gr", "gl", "gd", "gp", "gu", "gt", "gg", "hm", "va", "hn", "hk", "hu", "is", "in", "id", "ie", "im", "il", "it", "jm", "jp", "je", "jo", "kz", "ke", "ki", "kr", "kw", "kg", "lv", "lb", "ls", "lr", "li", "lt", "lu", "mo", "mk", "mg", "mw", "my", "mv", "ml", "mt", "mh", "mq", "mr", "mu", "yt", "mx", "fm", "md", "mc", "mn", "me", "ms", "ma", "mz", "na", "nr", "np", "nl", "nc", "nz", "ni", "ne", "ng", "nu", "nf", "mp", "no", "om", "pw", "ps", "pa", "py", "pe", "ph", "pn", "pl", "pt", "pr", "qa", "re", "ro", "ru", "rw", "bl", "sh", "kn", "lc", "mf", "pm", "vc", "ws", "sm", "st", "sa", "sn", "rs", "sc", "sl", "sg", "sx", "sk", "si", "sb", "za", "gs", "es", "lk", "sr", "sj", "sz", "se", "ch", "tw", "tj", "tz", "th", "tl", "tg", "tk", "to", "tt", "tn", "tr", "tm", "tc", "tv", "ae", "gb", "us", "um", "uy", "uz", "ve", "vn", "vg", "vi", "wf", "eh", "zm");

        if ($disc['discount_perc'] != 100) {

            if ($_SERVER['HTTP_HOST'] == "dev.awlandsundry.com") {
                $arrMerchantURLs = array(
                    "terms" => "http://dev.awlandsundry.com/terms-and-conditions",
                    "checkout" => "http://dev.awlandsundry.com/create-checkout/{checkout.order.id}",
                    "confirmation" => "http://dev.awlandsundry.com/klarna-order-confirm/{checkout.order.id}",
                    "push" => "http://dev.awlandsundry.com/klarna-create-order/{checkout.order.id}",
                    "validation" => "https://dev.awlandsundry.com/validate-klarna-order/{checkout.order.id}",
                );
            } else {
                $arrMerchantURLs = array(
                    "terms" => "https://" . $_SERVER['HTTP_HOST'] . "/terms-and-conditions",
                    "checkout" => "https://" . $_SERVER['HTTP_HOST'] . "/create-checkout/{checkout.order.id}",
                    "confirmation" => "https://" . $_SERVER['HTTP_HOST'] . "/klarna-order-confirm/{checkout.order.id}",
                    "push" => "https://" . $_SERVER['HTTP_HOST'] . "/klarna-create-order/{checkout.order.id}",
                    "validation" => "https://" . $_SERVER['HTTP_HOST'] . "/validate-klarna-order/{checkout.order.id}",
                );
            }

            if ($this->config->item('KLARNA_DEV') == 1) {
                $connector = Klarna\Rest\Transport\Connector::create(
                    $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_TEST_BASE_URL
                );
            } else {
                $connector = Klarna\Rest\Transport\Connector::create(
                    $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
                );
            }
            try {
                if ($sess_klarna_id) {
                    $klarna_order_id = $sess_klarna_id;
                    $checkout = new Klarna\Rest\Checkout\Order($connector, $sess_klarna_id);
                    $checkout->update([
                        "order_amount" => $disc_amt * 100,
                        "order_tax_amount" => 0,
                        "order_lines" => $cartItems,
                        "merchant_urls" => $arrMerchantURLs,
                        "options" => [
                            "allow_separate_shipping_address" => true,
                            "allow_global_billing_countries" => true,
                        ],
                        "merchant_data" => $merchant_info,
                    ]);
                    $data['checkout'] = $checkout->fetch();
                } else {
                    $checkout = new Klarna\Rest\Checkout\Order($connector);
                    $checkout->create([
                        "purchase_country" => "US",
                        "purchase_currency" => "USD",
                        "locale" => "en-US",
                        "order_amount" => $disc_amt * 100,
                        "order_tax_amount" => 0,
                        "order_lines" => $cartItems,
                        "merchant_urls" => $arrMerchantURLs,
                        "options" => [
                            "allow_separate_shipping_address" => true,
                            "allow_global_billing_countries" => true,
                        ],
                        "shipping_countries" => $countries,
                        "billing_countries" => $countries,
                        "merchant_data" => $merchant_info,
                    ]);

                    $data['checkout'] = $checkout->fetch();
                    $klarna_order_id = $checkout->getId();
                    $this->session->set_userdata('sess_klarna_id', $klarna_order_id);
                }

                $data['klarna_order_id'] = $klarna_order_id;
                $klarna_details['klarna_order_id'] = $klarna_order_id;
                $klarna_details['klarna_status'] = 'Init';
                $klarna_details['klarna_log'] = 'Init : ' . $klarna_order_id;
                $check = $this->OrderModel->save_klarna_order_details($order_id, $klarna_details);
                $data['additional_js'] = array(
                    'klarna/prototype',
                    'klarna/Klarna_Checkout',
                );
                $data['additional_css'] = array(
                    "js/build/mediaelementplayer.min",
                    "css/home",
                );
                $this->load->view('klarna_checkout', $data);
            } catch (Exception $ex) {
                $klarna['klarna_status'] = 'Error';
                $klarna['klarna_log'] = 'Init Error : Order ID ' . $order_id . ' Message : ' . $ex->getMessage();
                $this->OrderModel->save_klarna_order_details($order_id, $klarna);

                log_message('klarna_error', 'Klarna Order could not be confirmed');
                $this->session->unset_userdata('sess_klarna_id', $klarna_order_id);
                $this->session->set_flashdata('klarna_error', "Order could not be processed");
                redirect('cart');
            }
        } else {
            $this->update_order($order_id);
            redirect('order-confirm/' . $order_id, 'refresh');
        }
    }

    public function validateKlarnaOrder($klarna_order_id)
    {
        $this->load->model('UserModel');
        $order = $this->OrderModel->getOrderByklarna($klarna_order_id);
        $klarna = array();
        if (isset($order) && !empty($order)) {
            $klarna = array();
            $klarna['klarna_status'] = 'Validate Order';
            $klarna['klarna_log'] = 'Validate Order ID ' . $order['order_id'];
            $this->OrderModel->save_klarna_order_details($order['order_id'], $klarna);
            header("HTTP/1.1 200 OK");
            exit;
        } else {
            $klarna = array();
            $klarna['klarna_status'] = 'Validate Order : Error';
            $klarna['klarna_log'] = 'Order Id does not exist for Klarna ID ' . $klarna_order_id;
            $this->OrderModel->save_klarna_order_details($order['order_id'], $klarna);
            header("HTTP/1.1 303");
            $message = "Error while validating order";
            header('Location: ' . ApplicationConfig::ROOT_BASE . 'cart?klarna_error=' . $message);
            exit;
        }
    }

    public function klarnaOrderConfirm($klarna_order_id)
    {
        $this->load->model('UserModel');
        $userdetails = $this->session->userdata('user_details');

        $orderId = '';
        $klarna = array();
        $klarnaDetails = array();
        $klarnaErrorStatus = 0;
        if ($userdetails == false) {
            redirect('login');
        }

        $orderId = $this->OrderModel->getOrderByklarna($klarna_order_id);
        try {
            if ($this->config->item('KLARNA_DEV') == 1) {
                $connector = Klarna\Rest\Transport\Connector::create(
                    $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_TEST_BASE_URL
                );
            } else {
                $connector = Klarna\Rest\Transport\Connector::create(
                    $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
                );
            }
            $checkout = new Klarna\Rest\Checkout\Order($connector, $klarna_order_id);
            $data['checkout'] = $checkout->fetch();
            $klarnaDetails = get_object_vars($checkout);
        } catch (Exception $ex) {
            $klarna['klarna_order_id'] = $klarna_order_id;
            $klarna['klarna_status'] = 'Error';
            $klarna['klarna_log'] = 'Error On Fetch Before Confirm : ' . $klarna_order_id . 'Error Message : ' . $ex->getMessage();
            $this->OrderModel->save_klarna_order_details($orderId['order_id'], $klarna);
            $klarnaErrorStatus = 1;
            $this->session->unset_userdata('sess_klarna_id');

            log_message('klarna_error', 'Klarna Order could not be confirmed');
            $this->session->set_flashdata('klarna_error', "Order could not be confirmed");
            redirect('cart');
        }
        $disc = $this->session->userdata('cart_discount');
        if ($checkout->getId() && $orderId && !($klarnaErrorStatus) && $klarnaDetails['status'] == 'checkout_complete') {
            $klarnaDetails['order_id'] = $orderId['order_id'];
            if (isset($disc)) {
                if ($disc['giftcard_id'] != 0) {
                    $this->load->model('UserModel');
                    $total = $disc['total'];
                    $this->UserModel->updateStoreCreditAmount($disc['giftcard_id'], $disc['giftcard_discount'], $orderId['order_id'], $disc['total']);
                    if (isset($disc['giftcard_amount']) && $disc['giftcard_amount'] != 0) {
                        $this->load->model('giftcardmodel');
                        $this->giftcardmodel->insertGiftCardUsage($disc['giftcard_id'], $disc['giftcard_discount'], $orderId['order_id']);
                    }
                }
                if ($disc['promo_assign_id'] != 0) {
                    $this->ShoeModel->updatepromoAssign($disc['promo_assign_id'], $orderId['order_id']);
                }
                $this->session->unset_userdata('cart_discount');

                $referrer = $this->session->userdata('referrer');
                if ($referrer !== false) {
                    $this->load->model('CommonModel');
                    $this->CommonModel->insert_referral_discount($referrer, 25);
                    $this->session->unset_userdata('referrer');
                }
            }

            destroyDBCart($userdetails['user_id']);

            $this->saveklarna_billing_shipping($klarnaDetails);

            $klarna['klarna_order_id'] = $klarna_order_id;
            $klarna['klarna_status'] = 'Confirm';
            $klarna['klarna_log'] = 'Confirm : ' . $klarna_order_id;
            $this->OrderModel->save_klarna_order_details($klarnaDetails['order_id'], $klarna);
            $details = $this->OrderModel->getbillingNshippingDetails($klarnaDetails['order_id']);
            $order_summary = $this->OrderModel->getOrderDet($klarnaDetails['order_id']);
            $data['billing_address'] = $details['billing'][0];
            $data['shipping_address'] = $details['shipping'][0];
            $data['items'] = $details['order']['items'];
            $data['order_details'] = $order_summary[0];
            $total = $order_summary[0]['gross_amt'] - ($order_summary[0]['gross_amt'] * $order_summary[0]['discount_perc']) / 100;
            $data['order_details']['discount'] = round($order_summary[0]['gross_amt'] * $order_summary[0]['discount_perc'] / 100);
            $data['order_details']['tax_amount'] = round($order_summary[0]['tax_amount']);
            $data['order_details']['shipping_cost'] = round($order_summary[0]['shipping_price']);
            $data['order_details']['order_total'] = round($total + $order_summary[0]['tax_amount'] + $order_summary[0]['shipping_price']);
            $data['bodyclass'] = "order_confirmation";
            $data['pepperJam'] = $this->UserModel->isPepperjamUser($userdetails['user_id']);
            $this->session->unset_userdata('order_id');
            $this->removeCartItems();
            $this->session->unset_userdata('sess_klarna_id');
            $this->load->view('klarna_confirmation', $data);
        } else {
            $klarna['klarna_order_id'] = $klarna_order_id;
            $klarna['klarna_status'] = 'Error';
            $klarna['klarna_log'] = 'Error On Confirm : ' . $klarna_order_id;
            $this->OrderModel->save_klarna_order_details($klarnaDetails['order_id'], $klarna);

            log_message('klarna_error', 'Klarna Order could not be confirmed');
            $this->session->set_flashdata('klarna_error', "Order could not be confirmed");
            redirect('cart');
        }
    }

    public function klarnaCreateOrder($klarna_order_id)
    {
        $this->load->model('UserModel');

        $orderId = $this->OrderModel->getOrderByklarna($klarna_order_id);

        $klarna = array();
        if (!empty($orderId) && isset($orderId['klarna_status']) && $orderId['klarna_status'] == 'Confirm') {
            $orderId = $orderId['order_id'];
            try {
                if ($this->config->item('KLARNA_DEV') == 1) {
                    $connector = Klarna\Rest\Transport\Connector::create(
                        $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_TEST_BASE_URL
                    );
                } else {
                    $connector = Klarna\Rest\Transport\Connector::create(
                        $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
                    );
                }
                $order = new Klarna\Rest\OrderManagement\Order($connector, $klarna_order_id);
                $order->updateMerchantReferences([
                    "merchant_reference1" => $orderId,
                ]);
                $klarna = array();
                $klarna['klarna_order_id'] = $klarna_order_id;
                $klarna['klarna_status'] = 'Order Merchant Ref. Updated';
                $klarna['klarna_log'] = 'Order Merchant Ref. Updated: ' . $klarna_order_id;
                $this->OrderModel->save_klarna_order_details($orderId, $klarna);
            } catch (Exception $ex) {
                $connector = Klarna\Rest\Transport\Connector::create(
                    $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
                );
                $order = new Klarna\Rest\OrderManagement\Order($connector, $klarna_order_id);
                $orderDetails = get_object_vars($order->fetch());
                if ($orderDetails['status'] != 'CANCELLED') {
                    $order->cancel();

                    $klarna['klarna_order_id'] = $klarna_order_id;
                    $klarna['klarna_status'] = 'Cancelled';
                    $klarna['klarna_log'] = 'Order Cancelled ' . $klarna_order_id . '. Error on Order Merchant Ref. Updation.';
                    $this->OrderModel->save_klarna_order_details($orderId, $klarna);
                    $this->sendOrderCancelmailKlarna($klarna_order_id);
                }
                exit;
            }
            try {
                if ($this->config->item('KLARNA_DEV') == 1) {
                    $connector = Klarna\Rest\Transport\Connector::create(
                        $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_TEST_BASE_URL
                    );
                } else {
                    $connector = Klarna\Rest\Transport\Connector::create(
                        $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
                    );
                }
                $order = new Klarna\Rest\OrderManagement\Order($connector, $klarna_order_id);
                $order->acknowledge();

                $klarna = array();
                $klarna['klarna_order_id'] = $klarna_order_id;
                $klarna['klarna_status'] = 'Order Acknowledged';
                $klarna['klarna_log'] = 'Order Acknowledged : ' . $klarna_order_id;
                $this->OrderModel->save_klarna_order_details($orderId, $klarna);
                exit;
            } catch (Exception $ex) {
                $connector = Klarna\Rest\Transport\Connector::create(
                    $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
                );
                $order = new Klarna\Rest\OrderManagement\Order($connector, $klarna_order_id);
                $order->cancel();

                $klarna = array();
                $klarna['klarna_order_id'] = $klarna_order_id;
                $klarna['klarna_status'] = 'Cancelled';
                $klarna['klarna_log'] = 'Error on Order Acknowledgement. Order Cancelled : ' . $klarna_order_id . ' Message : ' . $ex->getMessage();
                $this->OrderModel->save_klarna_order_details($orderId, $klarna);
                $this->sendOrderCancelmailKlarna($klarna_order_id);
            }
        } else {
            $connector = Klarna\Rest\Transport\Connector::create(
                $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
            );
            $order = new Klarna\Rest\OrderManagement\Order($connector, $klarna_order_id);
            $orderDetails = $order->fetch();
            if ($orderDetails['status'] != "CANCELLED" && $orderId['klarna_status'] != "Captured") {
                $order->cancel();

                $klarna['klarna_order_id'] = $klarna_order_id;
                $klarna['klarna_status'] = 'Cancelled';
                $klarna['klarna_log'] = 'Order Cancelled : ' . $klarna_order_id;
                $this->OrderModel->save_klarna_order_details($orderId['order_id'], $klarna);
                $this->sendOrderCancelmailKlarna($klarna_order_id);
            }
        }
        exit;
    }

    private function KlarnaCapture($klarna_order_id)
    {
        $orderId = getenv('ORDER_ID') ?: $klarna_order_id;

        $DBOrder = $this->OrderModel->getOrderByklarna($klarna_order_id);

        $cartItems = array();
        if (isset($DBOrder) && $DBOrder['klarna_status'] == "Order Acknowledged") {
            $arr = $this->OrderModel->getbillingNshippingDetails($DBOrder['order_id']);
            $paymentDet = $this->OrderModel->getOrderDet($DBOrder['order_id']);
            $netAmount = $paymentDet[0]['gross_amt'];
            $discountAmt = 0;
            $i = 0;
            foreach ($arr['order']['items'] as $items) {
                $cartItems[$i]['type'] = 'physical';
                $cartItems[$i]['reference'] = $items['design_id'];
                $cartItems[$i]['name'] = $items['item_name'];
                $cartItems[$i]['quantity'] = $items['quantity'] * 1;
                $cartItems[$i]["unit_price"] = ($items['item_amt'] / $items['quantity']) * 100;
                $cartItems[$i]["tax_rate"] = 0;
                $cartItems[$i]["total_amount"] = $items['item_amt'] * 100;
                $cartItems[$i]["total_tax_amount"] = 0;
                $i++;
            }
            if ($paymentDet[0]['discount_perc'] != 0) {
                if (isset($paymentDet[0]['discount'])) {
                    $netAmount = $paymentDet[0]['gross_amt'] - $paymentDet[0]['discount'];
                    $discountAmt = $paymentDet[0]['discount'];
                } else {
                    $netAmount = $paymentDet[0]['gross_amt'] - round($paymentDet[0]['gross_amt'] * $paymentDet[0]['discount_perc'] / 100, 2);
                    $discountAmt = $paymentDet[0]['gross_amt'] * ($paymentDet[0]['discount_perc']) / 100;
                }
                $reference = 'discount';
                if (isset($paymentDet[0]['giftcard_id']) && $paymentDet[0]['giftcard_id'] != 0) {
                    $reference = $disc['giftcard_id'];
                }
                if (isset($paymentDet[0]['promocode_id']) && $paymentDet[0]['promocode_id'] != 0) {
                    $reference = $disc['promocode_id'];
                }
                $cartItems[$i]['type'] = 'discount';
                $cartItems[$i]['reference'] = $reference;
                $cartItems[$i]['name'] = "discount";
                $cartItems[$i]['quantity'] = 1;
                $cartItems[$i]["unit_price"] = -$discountAmt * 100;
                $cartItems[$i]["tax_rate"] = 0;
                $cartItems[$i]["total_amount"] = -$discountAmt * 100;
                $cartItems[$i]["total_tax_amount"] = 0;
            } else {
                $netAmount = $paymentDet[0]['gross_amt'] + $paymentDet[0]['tax_amount'];
            }
            try {
                if ($this->config->item('KLARNA_DEV') == 1) {
                    $connector = Klarna\Rest\Transport\Connector::create(
                        $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_TEST_BASE_URL
                    );
                } else {
                    $connector = Klarna\Rest\Transport\Connector::create(
                        $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
                    );
                }
                $order = new Klarna\Rest\OrderManagement\Order($connector, $klarna_order_id);
                $data = get_object_vars($order->fetch());
                $merchantData = $data['merchant_data'];

                $order->createCapture([
                    "captured_amount" => $netAmount * 100,
                    "description" => "Klarna Capture of the order #" . $DBOrder['order_id'] . " with amount " . $netAmount,
                    "order_lines" => $cartItems,
                ]);
                $klarna['klarna_order_id'] = $klarna_order_id;
                $klarna['klarna_status'] = 'Captured';
                $klarna['klarna_log'] = 'Order Captured : ' . $klarna_order_id;
                $this->OrderModel->save_klarna_order_details($DBOrder['order_id'], $klarna);
                $this->OrderModel->updateOrderStatus($DBOrder['order_id']);
                $this->sendOrderSuccessmailKlarna($DBOrder['order_id'], $merchantData);
            } catch (Exception $e) {
                $klarna['klarna_order_id'] = $klarna_order_id;
                $klarna['klarna_status'] = 'Captured';
                $klarna['klarna_log'] = 'Order Captured Failed: ' . $e->getMessage() . '\n' . json_encode(array(
                    "captured_amount" => $netAmount * 100,
                    "description" => "Klarna Capture of the order #" . $DBOrder['order_id'] . " with amount " . $netAmount * 100,
                    "order_lines" => $cartItems,
                ));
                $this->OrderModel->save_klarna_order_details($DBOrder['order_id'], $klarna);
            }
        }
    }

    private function sendOrderCancelmailKlarna($klarna_order_id)
    {
        $this->load->library('email');
        $order = $this->OrderModel->getOrderByklarna($klarna_order_id);

        $data = $this->OrderModel->getbillingNshippingDetails($order['order_id']);
        $data['order_summary'] = $this->OrderModel->getOrderDet($order['order_id']);

        $order_summary = $data['order_summary'][0];
        $data['subtotal'] = $order_summary['gross_amt'];
        $data['discount'] = $order_summary['discount'];
        $data['tax_amount'] = $order_summary['tax_amount'];
        $data['total'] = $order_summary['gross_amt'] - $order_summary['discount'] + $order_summary['tax_amount'];

        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to($this->config->item('contact_email'));
        $this->email->bcc(array('patel.mitul.m@gmail.com'));

        for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
            if ($data['order']['items'][$j]['item_name'] == 'Gift Card') {
                $options = $this->OrderModel->get_gift_card_order($order['order_id'], $data['order']['items'][$j]['item_id']);
                $data['shoedesign'][$j]['type'] = "Gift Card";
                $data['shoedesign'][$j]['recipient_name'] = $options['recipient_name'];
                $data['shoedesign'][$j]['sender_name'] = $options['sender_name'];
                $data['shoedesign'][$j]['card_number'] = $options['card_number'];
                $data['shoedesign'][$j]['message'] = $options['message'];
                $data['shoedesign'][$j]['giftcard_price'] = $options['amount'];
                $data['shoedesign'][$j]['quantity'] = $options['quantity'];
            }
            if ($data['order']['items'][$j]['design_id'] != 0) {
                $data['shoedesign'][] = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);
                $data['shoedesign'][$j]['type'] = 'Custom shoe';
                $data['shoedesign'][$j]['can_show_details'] = 1;
                if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0") {
                    $data['shoedesign'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                    $data['shoedesign'][$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
                }
                if ($data['order']['items'][$j]['last_id'] == 6) {
                    $data['shoedesign'][$j]['can_show_details'] = 0;
                    $data['shoedesign'][$j]['shop_images'] = $this->OrderModel->get_readytowear_images($data['order']['items'][$j]['item_id']);
                    $data['shoedesign'][$j]['last_name'] = "The Ready To Wear Shoe";
                    $data['shoedesign'][$j]['style_name'] = $this->OrderModel->get_readytowear_style($data['order']['items'][$j]['item_id']);
                }
                $data['summary'][] = array(
                    'price' => $data['order']['items'][$j]['item_amt'],
                    'left_size' => @$data['order']['items'][$j]['left_shoe'],
                    'right_size' => @$data['order']['items'][$j]['right_shoe'],
                    'left_width' => @$data['order']['items'][$j]['left_width'],
                    'right_width' => @$data['order']['items'][$j]['right_width'],
                    'quantity' => @$data['order']['items'][$j]['quantity'],
                );
            }
        }
        $this->email->subject('Klarna Order Cancelled.');
        $msg = $this->load->view('emails/klarna_order_cancellation', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    public function klarna_capture_acknowledged_orders()
    {
        $order_list = $this->OrderModel->getKlarnaAcknowledgedOrders();
        if (!empty($order_list)) {
            foreach ($order_list as $key => $order) {
                if ($this->config->item('KLARNA_DEV') == 1) {
                    $connector = Klarna\Rest\Transport\Connector::create(
                        $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_TEST_BASE_URL
                    );
                } else {
                    $connector = Klarna\Rest\Transport\Connector::create(
                        $this->config->item('KLARNA_MERCHANT_ID'), $this->config->item('KLARNA_SHARED_SECRET'), Klarna\Rest\Transport\ConnectorInterface::NA_BASE_URL
                    );
                }
                $klarnaOrder = new Klarna\Rest\OrderManagement\Order($connector, $order['klarna_order_id']);
                $data = get_object_vars($klarnaOrder->fetch());
                if ($data['fraud_status'] == "ACCEPTED") {
                    $this->KlarnaCapture($order['klarna_order_id']);
                }
                if ($data['fraud_status'] == "REJECTED" && $data['status'] != "CANCELLED") {
                    $klarnaOrder->cancel();

                    $klarna['klarna_order_id'] = $order['klarna_order_id'];
                    $klarna['klarna_status'] = 'Cancelled';
                    $klarna['klarna_log'] = 'Order Cancelled due to Klarna fraud status REJECTED for #' . $order['klarna_order_id'];
                    $this->OrderModel->save_klarna_order_details($order['order_id'], $klarna);
                    $this->sendOrderCancelmailKlarna($order['klarna_order_id']);
                }
            }
            echo "Done";
            exit;
        }
    }

    private function update_order($order_id)
    {
        if ($this->OrderModel->updateOrderStatus($order_id)) {
            $cartdet = $this->session->userdata('cart_discount');
            if ($cartdet != null) {
                if ($cartdet['giftcard_id'] != 0) {
                    $this->load->model('giftcardmodel');
                    $this->giftcardmodel->insertGiftCardUsage($cartdet['giftcard_id'], $cartdet['giftcard_discount'], $order_id);
                }
                if ($cartdet['promo_assign_id'] != 0) {
                    $this->ShoeModel->updatepromoAssign($cartdet['promo_assign_id'], $order_id);
                }
                $this->session->unset_userdata('cart_discount');
            }
            $referrer = $this->session->userdata('referrer');
            if ($referrer !== false) {
                $this->load->model('CommonModel');
                $this->CommonModel->insert_referral_discount($referrer, 25);
                $this->session->unset_userdata('referrer');
            }
            $this->sendOrderSuccessmail($order_id);
        }
    }

    public function test($orderId)
    {
        $this->sendOrderSuccessmail($orderId);
    }

    public function send_gift_card_email($order_id)
    {
        $options = $this->OrderModel->gift_card_order($order_id);
        $this->load->library('email');
        $data['recipient_name'] = $options['recipient_name'];
        $data['sender_name'] = $options['sender_name'];
        $data['card_number'] = $options['card_number'];
        $data['message'] = $options['message'];
        $data['giftcard_price'] = $options['amount'];
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to($options['recipient_email']);
        $this->email->cc($options['sender_email']);
        $this->email->bcc($this->config->item('contact_email'));
        $this->email->subject('You\'ve been sent a gift from ' . ucwords($data['sender_name']));
        $msg = $this->load->view('emails/giftCardEmail', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    public function address_confirmation()
    {
        $this->load->library('email');
        $this->load->model('AdminViewModel');
        $orders = $this->AdminViewModel->get_kickstarter_orders();
        foreach ($orders as $key => $value) {
            if ($key < 10) {
                continue;
            }
            $order_data = $this->OrderModel->getbillingNshippingDetails($value);

            $data['data'] = $order_data['shipping'][0];
            $email = $order_data['shipping'][0]['email'];
            $this->email->to($email);
            $this->email->cc($this->config->item('contact_email'));
            $this->email->from($this->config->item('admin_email'), 'Awl and Sundry');
            $this->email->reply_to($this->config->item('admin_email'), 'Awl and Sundry');
            $this->email->bcc($this->config->item('developer_email'));
            $this->email->subject('Address Confirmation Email');
            $msg = $this->load->view('emails/address_confirmation', $data, true);
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
        }
    }

    private function sendOrderSuccessmail($orderId)
    {
        $user = $this->session->userdata('user_details');
        $to = $user['email'];
        $this->load->library('email');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        $order_summary = $data['order_summary'][0];
        $data['subtotal'] = $order_summary['gross_amt'];
        $data['discount'] = $order_summary['discount'];
        $data['tax_amount'] = $order_summary['tax_amount'];
        $data['shipping_price'] = $order_summary['shipping_price'];
        $data['total'] = $order_summary['gross_amt'] - $order_summary['discount'] + $order_summary['tax_amount'] + $order_summary['shipping_price'];
        $data['user'] = $user;
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');

        $total = 0;
        $giftcard = 0;
        $this->load->model('giftcardmodel');
        foreach ($this->cart->contents() as $items):
            $total += 1;
            if (strtolower($items['name']) == 'gift card') {
                $giftcard += 1;
                $options = $this->cart->product_options($items['rowid']);
                $this->OrderModel->updateGiftCardStatus($options['giftcard_id']);
                $data['recipient_name'] = $options['recipient_name'];
                $data['sender_name'] = $options['sender_name'];
                $data['card_number'] = $options['card_number'];
                $data['message'] = $options['message'];
                $data['giftcard_price'] = $items['price'];
                $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
                $this->email->to($options['recipient_email']);
                $this->email->cc($options['sender_email']);
                $this->email->bcc($this->config->item('contact_email'));
                $this->email->subject('You\'ve been sent a gift from ' . ucwords($data['sender_name']));
                $msg = $this->load->view('emails/giftCardEmail', $data, true);
                $this->email->set_mailtype("html");
                $this->email->message($msg);
                if (!$this->giftcardmodel->has_date($options['giftcard_id'])) {
                    $this->email->send();
                }
                $this->sendGiftCardEmailAdmin($data);
            }
        endforeach;
        if ($total > $giftcard) {
            for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
                if ($data['order']['items'][$j]['design_id'] !== 0) {
                    $data['shoedesign'][] = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);

                    $data['shoedesign'][$j]['hasPatina'] = $data['order']['items'][$j]['hasPatina'];
                    $data['shoedesign'][$j]['patinaImg'] = $data['order']['items'][$j]['patinaImg'];
                    $data['shoedesign'][$j]['patina_material'] = $data['order']['items'][$j]['patina_material'];
                    $data['shoedesign'][$j]['patina_color'] = $data['order']['items'][$j]['patina_color'];

                    $data['shoedesign'][$j]['soleId'] = $data['order']['items'][$j]['soleId'];
                    $data['shoedesign'][$j]['soleName'] = $data['order']['items'][$j]['soleName'];
                    $data['shoedesign'][$j]['soleCode'] = $data['order']['items'][$j]['soleCode'];
                    $data['shoedesign'][$j]['soleColorId'] = $data['order']['items'][$j]['soleColorId'];
                    $data['shoedesign'][$j]['soleColorCode'] = $data['order']['items'][$j]['soleColorCode'];
                    $data['shoedesign'][$j]['soleColorName'] = $data['order']['items'][$j]['soleColorName'];
                    $data['shoedesign'][$j]['soleWelt'] = $data['order']['items'][$j]['soleWelt'];
                    $data['shoedesign'][$j]['shoetree'] = $data['order']['items'][$j]['shoetree'];

                    $data['shoedesign'][$j]['can_show_details'] = 1;
                    if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0") {
                        $data['shoedesign'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                        $data['shoedesign'][$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
                        if ($data['order']['items'][$j]['last_id'] == 6) {
                            $data['shoedesign'][$j]['can_show_details'] = 0;
                            $data['shoedesign'][$j]['shop_images'] = $this->OrderModel->get_readytowear_images($data['order']['items'][$j]['item_id']);
                            $data['shoedesign'][$j]['last_name'] = "The Ready To Wear Shoe";
                            $data['shoedesign'][$j]['style_name'] = $this->OrderModel->get_readytowear_style($data['order']['items'][$j]['item_id']);
                        }
                    }
                    $data['summary'][] = array(
                        'price' => $data['order']['items'][$j]['item_amt'],
                        'left_size' => @$data['order']['items'][$j]['left_shoe'],
                        'right_size' => @$data['order']['items'][$j]['right_shoe'],
                        'left_width' => @$data['order']['items'][$j]['left_width'],
                        'right_width' => @$data['order']['items'][$j]['right_width'],
                        'quantity' => @$data['order']['items'][$j]['quantity'],
                    );
                }
            }
            $this->email->to($to);
            $this->email->cc($this->config->item('contact_email'));
            $this->email->subject('Awl & Sundry | Order ' . $orderId . ' Confirmation');
            $msg = $this->load->view('emails/order_confirmation', $data, true);
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
            $this->sendOrderSuccessmailAdmins($data);
        }
        $this->session->unset_userdata('stripe_token');
        $this->session->unset_userdata('order_id');
        $this->removeCartItems();
    }

    private function sendOrderSuccessmailKlarna($orderId, $merchantData)
    {
        $i = 0;
        $merchantDetails = json_decode($merchantData, true);

        $to = $merchantDetails['user']['email'];
        $this->load->library('email');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        $order_summary = $data['order_summary'][0];
        $data['subtotal'] = $order_summary['gross_amt'];
        $data['discount'] = $order_summary['discount'];
        $data['tax_amount'] = $order_summary['tax_amount'];
        $data['total'] = $order_summary['gross_amt'] - $order_summary['discount'] + $order_summary['tax_amount'];
        $data['user'] = $merchantDetails['user'];

        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $giftcard = 0;

        $this->load->model('giftcardmodel');
        $merchantGiftcards = isset($merchantDetails['items']) ? $merchantDetails['items'] : array();
        foreach ($merchantGiftcards as $items):
            if (strtolower($items['name']) == 'gift card') {
                $giftcard += 1;
                $this->OrderModel->updateGiftCardStatus($items['giftcard_id']);
                $data['recipient_name'] = $items['recipient_name'];
                $data['sender_name'] = $items['sender_name'];
                $data['card_number'] = $items['card_number'];
                $data['message'] = $items['message'];
                $data['giftcard_price'] = $items['price'];
                $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
                $this->email->to($items['recipient_email']);
                $this->email->cc($items['sender_email']);
                $this->email->bcc($this->config->item('contact_email'));
                $this->email->subject('You\'ve been sent a gift from ' . ucwords($data['sender_name']));
                $msg = $this->load->view('emails/giftCardEmail', $data, true);
                $this->email->set_mailtype("html");
                $this->email->message($msg);
                if (!$this->giftcardmodel->has_date($items['giftcard_id'])) {
                    $this->email->send();
                }
                $data['recipient_email'] = $items['recipient_email'];
                $data['sender_email'] = $items['sender_email'];
                $this->sendGiftCardEmailAdmin($data);
            }
        endforeach;
        if ($merchantDetails['total_items'] > $giftcard) {
            for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
                if ($data['order']['items'][$j]['item_name'] == 'Gift Card') {
                    $options = $this->OrderModel->get_gift_card_order($orderId, $data['order']['items'][$j]['item_id']);
                    $data['shoedesign'][$j]['name'] = "Gift Card";
                    $data['shoedesign'][$j]['recipient_name'] = $options['recipient_name'];
                    $data['shoedesign'][$j]['sender_name'] = $options['sender_name'];
                    $data['shoedesign'][$j]['card_number'] = $options['card_number'];
                    $data['shoedesign'][$j]['message'] = $options['message'];
                    $data['shoedesign'][$j]['giftcard_price'] = $options['amount'];
                    $data['shoedesign'][$j]['quantity'] = $options['quantity'];
                }
                if ($data['order']['items'][$j]['design_id'] != '0') {
                    $data['shoedesign'][] = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);
                    $data['shoedesign'][$j]['can_show_details'] = 1;

                    $data['shoedesign'][$j]['hasPatina'] = $data['order']['items'][$j]['hasPatina'];
                    $data['shoedesign'][$j]['patinaImg'] = $data['order']['items'][$j]['patinaImg'];
                    $data['shoedesign'][$j]['patina_material'] = $data['order']['items'][$j]['patina_material'];
                    $data['shoedesign'][$j]['patina_color'] = $data['order']['items'][$j]['patina_color'];

                    $data['shoedesign'][$j]['soleId'] = $data['order']['items'][$j]['soleId'];
                    $data['shoedesign'][$j]['soleName'] = $data['order']['items'][$j]['soleName'];
                    $data['shoedesign'][$j]['soleCode'] = $data['order']['items'][$j]['soleCode'];
                    $data['shoedesign'][$j]['soleColorId'] = $data['order']['items'][$j]['soleColorId'];
                    $data['shoedesign'][$j]['soleColorCode'] = $data['order']['items'][$j]['soleColorCode'];
                    $data['shoedesign'][$j]['soleColorName'] = $data['order']['items'][$j]['soleColorName'];

                    if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0") {
                        $data['shoedesign'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                        $data['shoedesign'][$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
                    }
                    if ($data['order']['items'][$j]['last_id'] == 6) {
                        $data['shoedesign'][$j]['can_show_details'] = 0;
                        $data['shoedesign'][$j]['shop_images'] = $this->OrderModel->get_readytowear_images($data['order']['items'][$j]['item_id']);
                        $data['shoedesign'][$j]['last_name'] = "The Ready To Wear Shoe";
                        $data['shoedesign'][$j]['style_name'] = $this->OrderModel->get_readytowear_style($data['order']['items'][$j]['item_id']);
                    }
                    $data['summary'][] = array(
                        'price' => $data['order']['items'][$j]['item_amt'],
                        'left_size' => @$data['order']['items'][$j]['left_shoe'],
                        'right_size' => @$data['order']['items'][$j]['right_shoe'],
                        'left_width' => @$data['order']['items'][$j]['left_width'],
                        'right_width' => @$data['order']['items'][$j]['right_width'],
                        'quantity' => @$data['order']['items'][$j]['quantity'],
                    );
                }
            }
            $this->email->to($to);
            $this->email->cc($this->config->item('contact_email'));
            $this->email->bcc(array('patel.mitul.m@gmail.com'));
            $this->email->subject('Awl & Sundry | Order ' . $orderId . ' Confirmation');
            $msg = $this->load->view('emails/order_confirmation', $data, true);
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
            $this->sendOrderSuccessmailAdmins($data);
        }
    }

    public function send_html_mail()
    {
        $to = 'mosten@mostenmediation.com';
        $this->load->library('email');
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to($to);
        $this->email->cc([$this->config->item('contact_email')]);
        $this->email->bcc('ajith@kraftlabs.com');
        $this->email->subject('Order confirmation.');
        $msg = $this->load->view('emails/html', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
        echo 'sent';
    }

    private function sendOrderSuccessmailAdmins($data)
    {
        $this->email->to($this->config->item('contact_email'));
        // $this->email->bcc(array('patel.mitul.m@gmail.com'));
        $this->email->subject('New order at Awl & Sundry.');
        $data['isAdmin'] = true;
        $msg = $this->load->view('emails/order_confirmation', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    private function removeCartItems()
    {
        $this->cart->destroy();
        $this->session->unset_userdata('cartId');
        destroyCart();
    }

    public function paypal($order_id)
    {
        $arr = $this->OrderModel->getbillingNshippingDetails($order_id);
        $paymentDet = $this->OrderModel->getOrderDet($order_id);
        $billing = $arr['billing'][0];
        $shipping = $arr['shipping'][0];
        $order = $arr['order']['items'];
        $config['business'] = $this->config->item('contact_email');
        $config['cpp_header_image'] = ''; //Image header url [750 pixels wide by 90 pixels high]
        $config['return'] = base_url() . 'payment-success/' . $order_id;
        $config['cancel_return'] = base_url() . 'payment-cancel/' . $order_id;
        $config['notify_url'] = base_url() . 'ipn/' . $order_id; //IPN Post
        $config['production'] = !$this->config->item('Sandbox'); //Its false by default and will use sandbox
        $tot_amt = $this->getCartDetails();
        $config['discount_amount_cart'] = round($tot_amt * $paymentDet[0]['discount_perc'] / 100); //This means 20% discount
        $config['tax_cart'] = $paymentDet[0]['tax_amount'];
        $config["invoice"] = $paymentDet[0]['invoice_no']; //'8433456'; //The invoice id
        $config['shipping'] = $shipping['address1'];
        $config['shipping2'] = $shipping['address2'];

        $config['first_name'] = $shipping['firstname'];
        $config['last_name'] = $shipping['lastname'];
        $config['address1'] = $shipping['address1'];
        $config['address2'] = $shipping['address2'];
        $config['city'] = $shipping['city'];
        $config['state'] = $shipping['state'];
        $config['zip'] = $shipping['zipcode'];
        $config['email'] = $shipping['email'];

        $this->load->library('paypal', $config);
        foreach ($order as $ord) {
            $this->paypal->add($ord['item_name'], $ord['item_amt'], 1);
        }
        $this->paypal->add('Shipping cost', $paymentDet[0]['shipping_price'], 1);
        if ($paymentDet[0]['discount_perc'] != 100) {
            $this->paypal->pay(); //Proccess the payment
        } else {
            $this->update_order($order_id);
            redirect('order-confirm/' . $order_id, 'refresh');
        }
    }

    private function getCartDetails()
    {
        $this->load->helper('Common');
        $tot_amount = 0;
        if (sizeof($this->cart->contents()) > 0) {
            foreach ($this->cart->contents() as $items):
                if (strtolower($items['name']) == 'gift card') {
                    $tot_amount += $items['qty'] * $items['price'];
                } else {
                    $option = $this->cart->product_options($items['rowid']);
                    $shoe = json_decode($option['shoe'], true);

                    if (isset($shoe['from']) && $shoe['from'] == 'custom_collection') {
                        $shoe_design_id = $shoe['shoe_design_id'];
                        $shoedetail = $this->ShoeModel->ge_design_model($shoe_design_id);
                        $shoe = json_decode($shoedetail['shoemodel'], true);
                        $tot_amount += $items['qty'] * $items['price'];
                    } else {
                        $tot_amount += $items['qty'] * getPricing($shoe);
                    }
                }
            endforeach;
        }
        return $tot_amount;
    }

    public function getpriceformaterial($materials)
    {
        $price = 350;
        if ((in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials)) && (!in_array("M3", $materials) && !in_array("M4", $materials))) {
            $price = 350;
        } else if (!(in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials) || in_array("M4", $materials)) && in_array("M3", $materials)) {
            $price = 1850;
        } else if (!(in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials) || in_array("M3", $materials)) && in_array("M4", $materials)) {
            $price = 800;
        } else {
            if ((in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials)) && in_array("M3", $materials) && in_array("M4", $materials)) {
                $price = 700;
            } else if ((in_array("M0", $materials) || in_array("M1", $materials) || in_array("M2", $materials)) && (in_array("M3", $materials) || in_array("M4", $materials))) {
                if ((in_array("M3", $materials))) {
                    $price = 950;
                } else {
                    $price = 650;
                }
            } else {
                $price = 1250;
            }
        }
        return $price;
    }

    public function paymentsuccess($order_id)
    {
        $redata = $this->input->post();
        log_message('error', json_encode($redata));
        if ($redata['payment_status'] == 'Completed') {
            $this->update_order($order_id);
        }
        redirect('order-confirm/' . $order_id, 'refresh');
    }

    public function paymentcancel($id)
    {
        $this->session->unset_userdata('order_id');
        $this->load->view('paypal_payment_cancel');
    }

    public function ipn($order_id)
    {
        $redata = $this->input->post();
        log_message('info', json_encode($redata));
        if ($redata['payment_status'] == 'Completed') {
            $this->update_order($order_id);
        }
    }

    public function sendGiftCardEmailAdmin($options)
    {
        $this->load->library('email');
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to($this->config->item('admin_email'));
        $this->email->bcc($this->config->item('contact_email'));

        $this->email->subject('A new E-Gift Card has been purchased');
        $msg = $this->load->view('emails/giftCardEmailAdmin', $options, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    private function saveklarna_billing_shipping($address_details)
    {
        $userdet = $this->session->userdata('user_details');
        $order_id = $address_details['order_id'];
        $data = array();
        $billing_street_address2 = isset($address_details['billing_address']['street_address2']) ? $address_details['billing_address']['street_address2'] : "";
        $shipping_street_address2 = isset($address_details['shipping_address']['street_address2']) ? $address_details['shipping_address']['street_address2'] : "";
        $billing = array(
            'order_id' => $order_id,
            'user_id' => $userdet['user_id'],
            'first_name' => $address_details['billing_address']['given_name'],
            'last_name' => $address_details['billing_address']['family_name'],
            'address1' => $address_details['billing_address']['street_address'],
            'address2' => $billing_street_address2,
            'city' => $address_details['billing_address']['city'],
            'state' => $address_details['billing_address']['region'],
            'country' => $address_details['billing_address']['country'],
            'zipcode' => $address_details['billing_address']['postal_code'],
            'date_created' => date('Y-m-d H:i:s'),
        );
        $shipping = array(
            'order_id' => $order_id,
            'user_id' => $userdet['user_id'],
            'firstname' => $address_details['shipping_address']['given_name'],
            'lastname' => $address_details['shipping_address']['family_name'],
            'email' => $userdet['email'],
            'telephone' => $address_details['shipping_address']['phone'],
            'address1' => $address_details['shipping_address']['street_address'],
            'address2' => $shipping_street_address2,
            'city' => $address_details['shipping_address']['city'],
            'state' => $address_details['shipping_address']['region'],
            'country' => $address_details['shipping_address']['country'],
            'zipcode' => $address_details['shipping_address']['postal_code'],
            'date_created' => date('Y-m-d H:i:s'),
        );

        $address_shipping = array(
            'user_id' => $userdet['user_id'],
            'first_name' => $address_details['shipping_address']['given_name'],
            'last_name' => $address_details['shipping_address']['family_name'],
            'phone' => $address_details['shipping_address']['phone'],
            'address1' => $address_details['shipping_address']['street_address'],
            'address2' => $shipping_street_address2,
            'city' => $address_details['shipping_address']['city'],
            'state' => $address_details['shipping_address']['region'],
            'country' => $address_details['shipping_address']['country'],
            'zipcode' => $address_details['shipping_address']['postal_code'],
            'date_created' => date('Y-m-d H:i:s'),
            'is_billing' => "0",
            'is_shipping' => "1",
        );
        $address_billing = array(
            'user_id' => $userdet['user_id'],
            'first_name' => $address_details['billing_address']['given_name'],
            'last_name' => $address_details['billing_address']['family_name'],
            'phone' => $address_details['billing_address']['phone'],
            'address1' => $address_details['shipping_address']['street_address'],
            'address2' => $billing_street_address2,
            'city' => $address_details['shipping_address']['city'],
            'state' => $address_details['shipping_address']['region'],
            'country' => $address_details['shipping_address']['country'],
            'zipcode' => $address_details['shipping_address']['postal_code'],
            'date_created' => date('Y-m-d H:i:s'),
            'is_billing' => "1",
            'is_shipping' => "0",
        );
        $res = $this->UserModel->save_address_details($address_shipping);
        $res = $this->UserModel->save_address_details($address_billing);
        $data['billing'] = $billing;
        $data['shipping'] = $shipping;
        $this->OrderModel->savebilling($data);
    }

}
