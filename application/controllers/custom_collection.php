<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Custom_Collection extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("CommonModel");
        $this->load->model('OrderModel');
        $this->load->model('UserModel');
        $this->load->model('ShoeModel');
    }

    public function index($style_name = '')
    {
        $style_name = ($style_name == '') ? 'oxford' : $style_name;

        $types = $this->CommonModel->get_custom_collection_types($style_name);
        if ($types == false) {
            redirect('products/oxford');
        }

        $i = 0;
        if (is_array($types)) {
            foreach ($types as $values) {
                $types[$i]['link'] = 'products/' . $values['style_name'] . '/' . str_replace(' ', '_', $values['name']);
                $i++;
            }
        }

        $style = $this->CommonModel->get_style_details($style_name);
        $collections = array();
        if (is_array($types)) {
            foreach ($types as $key => $value) {
                $collections[$value['name']] = $this->CommonModel->get_custom_collections($style_name, $value['id']);
                if (is_array($collections[$value['name']])) {
                    foreach ($collections[$value['name']] as $k => $item) {
                        $collections[$value['name']][$k]['image'] = $this->CommonModel->get_custom_images($item);
                        $collections[$value['name']][$k]['alt'] = 'Handmade custom ' . $style_name . ' shoes in black calf leather. ';
                    }
                }
            }
        }

        $this->get_meta_and_title($style_name, $meta);
        $data['meta'] = $meta;
        $data['page_title'] = 'Shop - Handmade Custom ' . ucfirst($style_name) . ' Shoes For Men';
        $data['collections'] = $collections;
        $data['types'] = $types;
        $data['style'] = $style;
        $data['bodyclass'] = 'get_inspired_2';
        $data['additional_js'] = 'custom_collection';
        $this->load->view('custom_collection', $data);
    }

    public function collection($collection_name = '')
    {
        $collection_name = ($collection_name == '') ? 'oxford' : $collection_name;

        $types = $this->CommonModel->get_custom_collection_types($collection_name);
        if ($types == false) {
            redirect('products/oxford');
        }
        $i = 0;
        if (is_array($types)) {
            foreach ($types as $values) {
                $types[$i]['link'] = 'products/' . $values['style_name'] . '/' . str_replace(' ', '_', $values['name']);
                $i++;
            }
        }

        $collections = array();
        if (is_array($types)) {
            foreach ($types as $key => $value) {
                $collections[$value['name']] = $this->CommonModel->get_custom_collections($style_name, $value['id']);
                if (is_array($collections[$value['name']])) {
                    foreach ($collections[$value['name']] as $k => $item) {
                        $collections[$value['name']][$k]['image'] = $this->CommonModel->get_custom_images($item);
                        $collections[$value['name']][$k]['alt'] = 'Handmade custom ' . $style_name . ' shoes in black calf leather. ';
                    }
                }
            }
        }

        $data['page_title'] = 'Shop - Handmade Custom ' . ucfirst($style_name) . ' Shoes For Men';
        $data['collections'] = $collections;
        $data['types'] = $types;
        $data['bodyclass'] = 'get_inspired_2 collection';
        $data['additional_js'] = 'custom_collection';
        $data['additional_css'] = 'css/custom_collection';
        $this->load->view(str_replace('-', '_', $collection_name), $data);
    }

    public function type($style_name = '', $type = '')
    {
        if ($type === '') {
            redirect('products/oxford');
        }

        $types = $this->CommonModel->get_custom_collection_types($style_name);
        if ($types == false) {
            redirect('products/oxford');
        }
        $select_types = $types;
        $i = 0;

        $type = str_replace('_', ' ', $type);
        foreach ($types as $values) {
            if ($values['name'] === $type) {
                $select_types = array($values);
            }
            $types[$i]['link'] = 'products/' . $values['style_name'] . '/' . str_replace(' ', '_', $values['name']);
            $i++;
        }
        $style = $this->CommonModel->get_style_details($style_name);
        $collections = array();
        if (is_array($select_types)) {
            foreach ($select_types as $key => $value) {
                $collections[$value['name']] = $this->CommonModel->get_custom_collections($style_name, $value['id']);
                if (is_array($collections[$value['name']])) {
                    foreach ($collections[$value['name']] as $k => $item) {
                        $collections[$value['name']][$k]['image'] = $this->CommonModel->get_custom_images($item);
                        $collections[$value['name']][$k]['alt'] = 'Handmade custom ' . $style_name . ' shoes in black calf leather. ';
                    }
                }
            }
        }
        $this->get_meta_and_title($style_name, $meta, $type);
        $data['meta'] = $meta;
        $data['page_title'] = 'Shop - Handmade Custom ' . ucfirst($style_name) . ' ' . ucfirst($type) . ' Shoes For Men';
        $data['collections'] = $collections;
        $data['types'] = $types;
        $data['select_types'] = $select_types;
        $data['style'] = $style;
        $data['bodyclass'] = 'get_inspired_2 collection_type';
        $data['additional_js'] = 'custom_collection';
        $this->load->view('custom_collection_type', $data);
    }

    public function products($style, $type, $name)
    {
        $item_id = $this->CommonModel->get_custom_collection_item_by_name($style, $type, urldecode($name));
        if ($item_id > 0) {
            $this->details($item_id);
        } else {
            redirect(base_url('shop'));
        }
    }

    public function special_product($type, $name)
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $item_id = $this->CommonModel->get_special_item_by_name($name);
        $data = array();
        if (isset($item_id)) {
            $item = $this->CommonModel->get_custom_collection_item($item_id);
        }

        $images = $this->CommonModel->get_custom_images($item);
        $content_url = base_url() . 'special/' . $type . '/' . $item['shoe_name'];
        $image = $images[0];

        $this->get_meta_and_title($item['style_name'], $meta, $type, $name);

        $meta .= <<<EOD
            <meta property="og:title" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!" />
            <meta property="og:url" content="$content_url" />
            <meta property="og:site_name" content="https://www.awlandsundry.com/" />
            <meta property="og:image" content="$image" />
            <meta property="og:description" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!" />
            <meta property="fb:app_id" content="250777875062389" />

            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:site" content="@awlandsundry">
            <meta name="twitter:creator" content="@awlandsundry">
            <meta name="twitter:title" content="&nbsp;">
            <meta name="twitter:description" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!">
            <meta name="twitter:image" content="$image">
            <meta name="twitter:url" content="$content_url" >
            <meta name="twitter:domain" content="awlandsundry.com">
            <meta name="twitter:app:id:iphone" content="">
            <meta name="twitter:app:id:ipad" content="">
            <meta name="twitter:app:id:googleplay" content="">
            <meta name="twitter:app:url:iphone" content="">
            <meta name="twitter:app:url:ipad" content="">
            <meta name="twitter:app:url:googleplay" content="">

EOD;
        $data['similar_products'] = $this->CommonModel->get_similar_shoes_special($item_id, $item['type_id']);
        $data['url'] = $content_url;
        $data['item'] = $item;
        $data['images'] = $images;
        $data['meta'] = $meta;
        $data['page_title'] = 'Shop - ' . ucfirst($item['shoe_name']) . ' | Handmade Custom ' . ucfirst($item['style_name']) . ' ' . ucfirst($type) . ' Shoes For Men';
        $data['additional_js'] = array('jquery.sumoselect.min', 'details');
        $data['bodyclass'] = 'classic_1 details';
        $data['additional_css'] = 'css/sumoselect';
        $this->load->view('details', $data);
    }

    public function special($type)
    {
        $types = $this->CommonModel->get_custom_collection_types($type);

        $select_types = $types;
        if (is_array($select_types)) {
            foreach ($select_types as $key => $value) {
                $collections[$value['name']] = $this->CommonModel->get_custom_collections_by_type($value['id']);
                if (is_array($collections[$value['name']])) {
                    foreach ($collections[$value['name']] as $k => $item) {
                        $collections[$value['name']][$k]['image'] = $this->CommonModel->get_custom_images($item);
                    }
                }
            }
        }

        $this->get_meta_and_title('Valentine', $meta);
        $data['meta'] = $meta;
        $data['types'] = $types;
        $data['select_types'] = $select_types;
        $data['style'] = array(
            'style_class' => 'special',
            'style_name' => " Exclusive Valentine's Collection",
            'style_description' => "In a world where we spend most of our time working, Valentine's Day gives us a break, to pause everything, and experience the people we love.<br/>  Whether we spend it with friends, family, or a fun night meeting new people, men need to be prepared for all the paths they may find themselves on.<br/>  With shoes designed specifically to entice the emotions of Valentine's Day, men can leave their \"work minds\" at home, and embody the masculine sexuality that they want to emulate on Valentine's Day night. Be prepared to receive looks of desire, to be a powerhouse of confidence, and be ready to be the life of the party.<br/> Be ready for Awl & Sundry.",
        );
        $data['collections'] = $collections;
        $data['page_title'] = 'Shop - Handmade Custom ' . ucfirst($type) . ' Shoes For Men';
        $data['bodyclass'] = 'get_inspired_2 collection_type';
        $data['additional_js'] = 'custom_collection';
        $this->load->view('special', $data);
    }

    public function details($item_id = '')
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $data = array();
        if (isset($item_id)) {
            $item = $this->CommonModel->get_custom_collection_item($item_id);
        }
        if (isset($item) && is_array($item)) {
            $types = $this->CommonModel->get_custom_collection_types($item['style_name']);
            $i = 0;
            foreach ($types as $values) {
                $types[$i]['link'] = 'products/' . $values['style_name'] . '/' . str_replace(' ', '_', $values['name']);
                $i++;
                if ($item['type_id'] === $values['id']) {
                    $type = $values['name'];
                }
            }
        }

        $images = $this->CommonModel->get_custom_images($item);
        $content_url = base_url() . 'products/' . $item['style_name'] . '/' . $type . '/' . $item['shoe_name'];
        $image = $images[0];

        $this->get_meta_and_title($item['style_name'], $meta, $type, $item['shoe_name']);

        $meta .= <<<EOD
            <meta property="og:title" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!" />
            <meta property="og:url" content="$content_url" />
            <meta property="og:site_name" content="https://www.awlandsundry.com/" />
            <meta property="og:image" content="$image" />
            <meta property="og:description" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!" />
            <meta property="fb:app_id" content="250777875062389" />

            <meta name="twitter:card" content="summary_large_image">
            <meta name="twitter:site" content="@awlandsundry">
            <meta name="twitter:creator" content="@awlandsundry">
            <meta name="twitter:title" content="&nbsp;">
            <meta name="twitter:description" content="The World at Your Feet! Buy your custom, handmade pair from over 2 billion design variations today!">
            <meta name="twitter:image" content="$image">
            <meta name="twitter:url" content="$content_url" >
            <meta name="twitter:domain" content="awlandsundry.com">
            <meta name="twitter:app:id:iphone" content="">
            <meta name="twitter:app:id:ipad" content="">
            <meta name="twitter:app:id:googleplay" content="">
            <meta name="twitter:app:url:iphone" content="">
            <meta name="twitter:app:url:ipad" content="">
            <meta name="twitter:app:url:googleplay" content="">


EOD;
        $data['similar_products'] = $this->CommonModel->get_similar_shoes($item_id);
        $data['url'] = $content_url;
        $data['item'] = $item;
        $data['types'] = $types;
        $data['images'] = $images;
        $data['alt'] = 'Handmade custom ' . $item['style_name'] . ' shoes in black calf leather. ';
        $data['meta'] = $meta;
        $data['page_title'] = 'Shop - ' . ucfirst($item['shoe_name']) . ' | Handmade Custom ' . ucfirst($item['style_name']) . ' ' . ucfirst($type) . ' Shoes For Men';
        $data['additional_js'] = array('jquery.sumoselect.min', 'details');
        $data['bodyclass'] = 'classic_1 details';
        $data['additional_css'] = 'css/sumoselect';
        $this->load->view('details', $data);
    }

    private function get_meta_and_title($style_name, &$meta, $type = '', $shoe_name = '')
    {
        $meta = '<meta name="description" content="Shop from our top handmade ' . ucfirst($style_name);
        if ($type !== '') {
            $meta .= ' ' . ucfirst($type);
        }
        $meta .= ' shoes collection. Free shipping and 30 day exchange. ">';
        if ($shoe_name !== '') {
            $meta = '<meta name="description" content="Shop ' . ucfirst($shoe_name) . ' - our top handmade ' . ucfirst($style_name);
            $meta .= ' ' . ucfirst($type);
            $meta .= ' shoes collection. Free shipping and 30 day exchange. ">';
        }
    }

    public function shop()
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $this->load->model("CustomShoeModel");
        $filters = array();
        $config = array('limit' => 6, 'start' => 0);
        $collections = $this->CommonModel->get_collections($filters, $config);
        foreach ($collections as $k => $item) {
            $collections[$k]['image'] = $this->CommonModel->get_custom_images($item);
        }
        $data['types'] = $this->CommonModel->get_custom_collection_types('oxford');
        $data['styles'] = $this->CustomShoeModel->get_styles(1);
        $data['lasts'] = $this->CustomShoeModel->get_last_styles('oxford');
        $colors = $this->CommonModel->get_material_colors();

        $color_codes = array("502220", "000000", "653F3A", "B85F43", "1F1711", "67322C", "28140D", "53311E", "9F825C", "172A27", "5A534B", "9C9179", "623224", "2F3037", "20181D", "262A1F", "52020B", "38615F", "2B1C22", "6A361F", "ECE6E6");
        foreach ($colors as $key => $color) {
            $colors[$key]['color_code'] = $color_codes[$key];
        }
        $data['colors'] = $colors;
        $data['materials'] = $this->CommonModel->get_materials();
        foreach ($data['materials'] as $key => $row) {
            $data['materials'][$key]['colors'] = $this->CommonModel->get_materials_by_material_id($row['material_def_id']);
        }

        $data['collections'] = $collections;
        $data['bodyclass'] = "shop";
        $data['additional_js'] = array(base_url() . 'assets/js/shop.js');
        $data['additional_css'] = array('css/shop');
        $this->load->view('shop', $data);
    }

    public function get_collections()
    {
        $config = $this->input->post('config');
        $filters = $this->input->post('filters');
        $collections = $this->CommonModel->get_collections($filters, $config);
        foreach ($collections as $k => $item) {
            $collections[$k]['image'] = $this->CommonModel->get_custom_images($item);
        }
        $data['collections'] = $collections;
        $this->load->view('shop_collection', $data);
    }

}
