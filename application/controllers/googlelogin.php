<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Googlelogin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('google');
    }

    public function signup()
    {
        redirect($this->google->loginURL());
    }

    public function index()
    {
        if (isset($_GET['code'])) {
            try {
                $this->google->getAuthenticate();
                $user_profile = $this->google->getUserInfo();

                if (isset($user_profile['id'])) {
                    $this->load->model('UserModel');
                    $gpuser = $this->UserModel->loginwithgoogle($user_profile['id'], $user_profile['email']);
                    if ($gpuser == null) {
                        $userData = $this->UserModel->register_googleuser($user_profile);
                        $sendyArr = ['name' => trim($userData['first_name'] . ' ' . $userData['last_name']), 'email' => $userData['email']];
                        addCustomerToSendy($sendyArr);
                        addCustomerToMailChimp($userData);

                        $this->mailchimpintegration($user_profile['email']);
                        $gpuser = $this->UserModel->loginwithgoogle($user_profile['id']);
                    }
                    $this->session->set_userdata('user_details', $gpuser);
                    $redirect = $this->session->userdata('redirect');
                    if ($redirect != null) {
                        $this->session->unset_userdata('redirect');
                    }

                    if ($redirect != null) {
                        echo '<script type="text/javascript">window.location.href="' . $redirect . '"; window.close();</script>';
                    } else {
                        echo '<script type="text/javascript">window.location.href="' . base_url() . '"; window.close();</script>';
                    }
                } else {
                    echo '<script type="text/javascript">window.location.href="' . base_url('login') . '"; window.close();</script>';
                }
            } catch (Exception $ex) {
                echo '<script type="text/javascript">window.location.href="' . base_url('login') . '"; window.close();</script>';
            }
        } else {
            echo '<script type="text/javascript">window.location.href="' . base_url('login') . '"; window.close();</script>';
        }
    }

    public function mailchimpintegration($email)
    {
        $config = array(
            'apikey' => '39c50254155d9583cec13a336c110060-us8',
            'secure' => false,
        );
        $this->load->library('MCAPI', $config, 'mail_chimp');
        $list_id = '6b8ea607f9';
        if ($this->mail_chimp->listSubscribe($list_id, $email)) {
            return true;
        }
        return false;
    }

}
