<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Linkedin_signup extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->config('social');

        $linkedin_config = array(
            'appKey' => $this->config->item('linkedin_app_key'),
            'appSecret' => $this->config->item('linkedin_app_secret'),
            'callbackUrl' => base_url() . 'linkedin_signup/data/',
        );

        $this->load->library('linkedin', $linkedin_config);
    }

    public function index()
    {
        echo '<form id="linkedin_connect_form" action="' . base_url() . 'linkedin/signup" method="post">';
        echo '<input type="submit" value="Login with LinkedIn" />';
        echo '</form>';
    }

    public function initiate()
    {

        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        $token = $this->linkedin->retrieveTokenRequest();

        $this->session->set_flashdata('oauth_request_token_secret', $token['linkedin']['oauth_token_secret']);
        $this->session->set_flashdata('oauth_request_token', $token['linkedin']['oauth_token']);

        $link = "https://api.linkedin.com/uas/oauth/authorize?oauth_token=" . $token['linkedin']['oauth_token'];
        redirect($link);
    }

    public function cancel()
    {
        echo 'Linkedin user cancelled login';
    }

    public function logout()
    {
        session_unset();
        $_SESSION = array();
        echo "Logout successful";
    }

    public function data()
    {

        $this->linkedin->setResponseFormat(LINKEDIN::_RESPONSE_JSON);
        $this->load->model('UserModel');

        $oauth_token = $this->session->flashdata('oauth_request_token');
        $oauth_token_secret = $this->session->flashdata('oauth_request_token_secret');

        $oauth_verifier = $this->input->get('oauth_verifier');
        try {
            $response = $this->linkedin->retrieveTokenAccess($oauth_token, $oauth_token_secret, $oauth_verifier);
        } catch (Exception $ex) {
            redirect('login');
        }
        if ($response['success'] === true) {
            $oauth_expires_in = $response['linkedin']['oauth_expires_in'];
            $oauth_authorization_expires_in = $response['linkedin']['oauth_authorization_expires_in'];

            $response = $this->linkedin->setTokenAccess($response['linkedin']);
            $profile = $this->linkedin->profile('~:(id,first-name,last-name,email-address)');
            $user = json_decode($profile['linkedin']);
            $user_profile = array(
                'linkedin_id' => $user->id,
                'last_name' => $user->lastName,
                'first_name' => $user->firstName,
                'email' => $user->emailAddress);

            $linkedin_user = $this->UserModel->login_with_linkedin($user->id);
            if ($linkedin_user == null) {
                $userData = $this->UserModel->register_linkedin_user($user_profile);
                $sendyArr = ['name' => trim($userData['first_name'] . ' ' . $userData['last_name']), 'email' => $userData['email']];
                addCustomerToSendy($sendyArr);
                addCustomerToMailChimp($userData);
                $this->mailchimpintegration($user_profile['email']);
                $linkedin_user = $this->UserModel->login_with_linkedin($user->id);
            }
            $this->session->set_userdata('user_details', $linkedin_user);
            addToMcCartAfterLogin($linkedin_user['user_id']);

            $redirect = $this->session->userdata('redirect');
            if ($redirect != null) {
                $this->session->unset_userdata('redirect');
                redirect($redirect, 'refresh');
            }
            redirect('my-account', 'refresh');
        } else {
            echo "Request token retrieval failed:";
        }
    }

    public function mailchimpintegration($email)
    {
        $config = array('apikey' => '39c50254155d9583cec13a336c110060-us8', 'secure' => false);
        $this->load->library('MCAPI', $config, 'mail_chimp');
        $list_id = '6b8ea607f9';
        if ($this->mail_chimp->listSubscribe($list_id, $email)) {
            return true;
        }
        return false;
    }

}
