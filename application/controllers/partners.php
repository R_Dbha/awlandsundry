<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Partners extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('PartnerModel');
        $this->load->model('CustomShoeModel');
        $this->load->model('ShoeModel');
        $this->load->model('OrderModel');
        $this->load->model('UserModel');
        $this->load->model('Stripe_trans');
        $this->load->helper('common_helper');

        $this->user = $this->session->userdata('user_details');
        if (!($this->user && ($this->user['user_type_id'] == 5 || $this->user['user_type_id'] == 6))) {
            if ($this->router->method !== 'login') {
                redirect('partners/login');
            }
        }
        $partner = $this->PartnerModel->get_partner_details($this->user['user_id']);
        $this->partner_id = $partner['partner_id'];
        $this->data['partner_image'] = ($partner['attachment_id'] == 0) ? '' : $partner['file_name'];
        $this->data['user'] = $this->user;
        $this->data['additional_js'] = array(
            'fileuploader',
            'https://js.stripe.com/v1/',
            'jquery.formance.min',
            'jquery.validate.min',
            "shoedesign/modelrenderer",
            "shoedesign/customshoe",
            'admin/partner',
        );
    }

    public function login()
    {
        $this->user = $this->session->userdata('user_details');
        if (($this->user && ($this->user['user_type_id'] == 5 || $this->user['user_type_id'] == 6))) {
            redirect('partners/index');
        } else {
            $this->session->unset_userdata('user_details');
        }
        $data = array();
        $form_data = $this->input->post();
        if ($form_data) {
            $user = $this->UserModel->login($form_data['email'], md5($form_data['password']));
            if ($user == null) {
                $data['email'] = $form_data['email'];
                $data['error']['invalid_login'] = 'Invalid User Name or Password';
            } else {
                if (!($user['user_type_id'] == 5 || $user['user_type_id'] == 6)) {
                    $data['error']['invalid_login'] = 'Invalid User Name or Password';
                } else {
                    $this->session->set_userdata('user_details', $user);
                    redirect('partners/index');
                }
            }
        }
        $this->load->view('partner/login', $data);
    }

    public function logout()
    {
        $this->session->unset_userdata('user_details');
        $this->session->unset_userdata('order_id');
        redirect('partners/login');
    }

    public function index()
    {
        $partner = $this->PartnerModel->get_partner_details($this->user['user_id']);
        $this->config->set_item('app_title', $partner['partner_name']);
        $result = $this->PartnerModel->getPartnerOrders($$partner['partner_id']);
        $this->data['orders'] = $result['rows'];

        $this->load->view('partner/index', $this->data);
    }

    public function create_order()
    {
        $this->cart->destroy();
        $this->session->unset_userdata('order_id');
        $lasts = $this->CustomShoeModel->get_last_styles('oxford');
        $last_option = '';
        foreach ($lasts as $last) {
            $last_option .= '<option value="' . $last['last_item_def_id'] . '" ';
            if ($last['manufacturer_code'] == '1258') {
                $last_option .= ' selected = "selected" ';
            }
            $last_option .= ' >' . $last['manufacturer_code'] . '</option>';
        }
        $this->data['style'] = generate_options($this->styles);
        $this->data['last'] = $last_option;
        $this->data['width'] = generate_options($this->var_width);
        $this->data['size'] = generate_options($this->var_size);
        $this->data['styles'] = $this->CustomShoeModel->get_styles(1);

        $this->load->view('partner/create_order', $this->data);
    }

    public function select_style()
    {
        $style = $this->input->post('style');
        $data['lasts'] = $this->CustomShoeModel->get_last_styles($style);
        $this->load->view('partner/lasts', $data);
    }

    public function get_by_design_number()
    {
        $design_number = $this->input->post('design_number');
        $partner = $this->PartnerModel->get_partner_details($this->user['user_id']);
        $data['designs'] = $this->PartnerModel->get_partner_designs_by_design_number($partner['partner_id'], $design_number);
        if ($data['designs'][0]['attachment_ids'] !== null) {
            $this->load->model('CommonModel');
            $data['designs'][0]['image'] = $this->CommonModel->get_custom_images($data['designs'][0]);
            $data['designs'][0]['shop_image'] = true;
        } else {
            $data['designs'][0]['shop_image'] = false;
        }
        $this->load->view('partner/designs', $data);
    }

    public function select_last()
    {
        $style = $this->input->post('style');
        $last = $this->input->post('last');
        $partner = $this->PartnerModel->get_partner_details($this->user['user_id']);
        $data['designs'] = $this->PartnerModel->get_partner_designs($partner['partner_id'], $style, $last);
        $this->load->view('partner/designs', $data);
    }

    public function select_design()
    {
        $shoe_design_id = $this->input->post('shoe_design_id');

        $shoedetail = $this->ShoeModel->ge_design_model($shoe_design_id);
        $shoemodel = json_decode($shoedetail['shoemodel'], true);
        $shoemodel['reDesignId'] = $designId;
        $shoemodel['angle'] = 'A0';
        if (!isset($shoemodel['style']['isLaceEnabled'])) {
            $model = $this->CustomShoeModel->get_style_defaults($shoe_design_id);
            $shoemodel['style']['isLaceEnabled'] = $model['style']['isLaceEnabled'];
        }
        $shoemodel['size'] = array(
            'left' => array(
                'value' => '5',
                'text' => '5',
                'width' => 'D',
            ),
            'right' => array(
                'value' => '5',
                'text' => '5',
                'width' => 'D',
            ),
        );

        $shoemodel['measurement'] = array(
            'left' => array(
                'size' => '',
                'width' => '',
                'girth' => '',
                'instep' => '',
            ),
            'right' => array(
                'size' => '',
                'width' => '',
                'girth' => '',
                'instep' => '',
            ),
        );
        $monogram = array(
            'leftShoe' => 0,
            'leftSide' => 0,
            'rightShoe' => 0,
            'rightSide' => 0,
            'text' => '',
        );
        $shoemodel['monogram'] = $monogram;

        $this->session->unset_userdata('ShoeModel');
        $this->session->set_userdata('ShoeModel', $shoemodel);

        $data['model'] = $shoemodel;
        $data['materials'] = $this->CustomShoeModel->style_material_colors();
        $data['styledetails'] = $this->CustomShoeModel->get_style_details($shoemodel['style']['id']);
        $data['stitches'] = $this->CustomShoeModel->get_style_stitches($shoemodel['style']['id']);
        if ($data['styledetails']['is_lace_enabled']) {
            $data['laces'] = $this->CustomShoeModel->get_style_laces($shoemodel['style']['id']);
            $data['lace_model'] = $shoemodel['lace'];
        }

        echo json_encode($data);
    }

    public function getPrice()
    {
        $this->load->helper('common');
        $model = $this->input->post('ShoeModel');
        $price = array(
            'CP' => 420,
            'CG' => 420,
            'CS' => 420,
            'AL' => 3000,
            'OS' => 2600,
        );
        echo '$' . getPricing($model, $price);
    }

    public function save_order()
    {
        $form_data = $this->input->post();
        $customer_data = $this->PartnerModel->simple_get('users', 'user_id', array(
            'email' => $form_data['customer_email'],
        ));
        if ($customer_data) {
            $customer_id = $customer_data[0]['user_id'];
        } else {
            $customer_data = array(
                'first_name' => $form_data['customer_name'],
                'email' => $form_data['customer_email'],
                'password' => md5($form_data['customer_email']),
                'is_enabled' => 1,
                'customer_category' => 'Customer',
                'date_created' => date('Y-m-d:H:i:s'),
            );
            $customer_id = $this->PartnerModel->insert('users', $customer_data);
        }
        $order_id = $this->session->userdata('order_id');
        if ($order_id == false) {

            $order_main = array();
            $order = array();
            $order_items = array();
            $items = array();
            $tot_amt = 0;
            if (sizeof($this->cart->contents()) > 0) {
                foreach ($this->cart->contents() as $items) {
                    $option = $this->cart->product_options($items['rowid']);
                    $shoe = json_decode($option['shoe'], true);
                    $name = $items['name'];

                    $order_items['left_shoe'] = $shoe['size']['left']['text'];
                    $order_items['right_shoe'] = $shoe['size']['right']['text'];
                    $order_items['left_width'] = $shoe['size']['left']['width'];
                    $order_items['right_width'] = $shoe['size']['right']['width'];

                    $order_items['m_left_size'] = $shoe['measurement']['left']['size'];
                    $order_items['m_left_width'] = $shoe['measurement']['left']['width'];
                    $order_items['m_right_size'] = $shoe['measurement']['right']['size'];
                    $order_items['m_right_width'] = $shoe['measurement']['right']['width'];

                    $order_items['m_lgirth'] = $shoe['measurement']['left']['girth'];
                    $order_items['m_linstep'] = $shoe['measurement']['left']['instep'];
                    $order_items['m_rgirth'] = $shoe['measurement']['right']['girth'];
                    $order_items['m_rinstep'] = $shoe['measurement']['right']['instep'];

                    $savedShoe = $this->saveCartItem($shoe, $customer_id);
                    $shoe = $savedShoe;
                    $name = $shoe['details']['name'];
                    $order_items['design_id'] = $shoe['shoeDesignId'];
                    $order_items['item_id'] = 0;

                    $tot_amt += $order_items['item_amt'] = $items['qty'] * $items['price'];
                    $order_items['item_name'] = $name;

                    $order_items['quantity'] = $items['qty'];
                    $order['items'][] = $order_items;
                }

                $order_main['invoice_no'] = $this->OrderModel->getinvoiceno();
                $order_main['user_id'] = $userdetails['user_id'];
                $order_main['currency'] = 'USD';
                $order_main['gross_amt'] = $tot_amt;
                $order_main['user_agent'] = $this->input->user_agent();
                $order_main['invoice_no'] = $this->OrderModel->getinvoiceno();
                $order_main['user_id'] = $customer_id;
                $order_main['currency'] = 'USD';
                $order_main['gross_amt'] = $tot_amt;
                $order_main['user_agent'] = $this->input->user_agent();

                $order_main['order_modified'] = date('Y-m-d H:i:s');
                $order_main['payment_status'] = 'Not Processed';
                $order['order_main'] = $order_main;
                $order_id = $this->OrderModel->saveorder($order);
                $this->session->set_userdata('order_id', $order_id);
                $billing = array(
                    'order_id' => $order_id,
                    'user_id' => $customer_id,
                    'address1' => '',
                    'address2' => '',
                    'city' => '',
                    'state' => '',
                    'country' => '',
                    'zipcode' => '',
                    'date_created' => date('Y-m-d H:i:s'),
                );
                $shipping = array(
                    'order_id' => $order_id,
                    'user_id' => $customer_id,
                    'firstname' => $form_data['customer_name'],
                    'lastname' => '',
                    'email' => $form_data['customer_email'],
                    'telephone' => $form_data['cell_number'],
                    'address1' => '',
                    'address2' => '',
                    'city' => '',
                    'state' => '',
                    'country' => '',
                    'zipcode' => '',
                    'date_created' => date('Y-m-d H:i:s'),
                );

                $data['billing'] = $billing;
                $data['shipping'] = $shipping;
                $this->OrderModel->savebilling($data);
                $this->PartnerModel->insert('partner_orders', array(
                    'partner_id' => $this->partner_id,
                    'order_id' => $order_id,
                    'amount' => $tot_amt,
                    'amount_paid' => $form_data['amount_paying'],
                    'date_created' => date('Y-m-d:H:i:s'),
                ));
            }
            $stripe_result = $this->Stripe_trans->insert($form_data['token'], 'AwlandSundry.com', round($form_data['amount_paying'] * 100, 2));
            if ($stripe_result->paid === true) {
                $this->OrderModel->updateOrderStatus($order_id);
                $this->cart->destroy();
                $details = $this->OrderModel->getbillingNshippingDetails($order_id);
                $data['items'] = $details['order']['items'];
                $order_summary = $this->OrderModel->getOrderDet($order_id);
                $data['order_details'] = $order_summary[0];
                $data['order_details']['order_total'] = $tot_amt;
                $data['order_details']['amount_paid'] = $form_data['amount_paying'];
                $data['order_details']['pending'] = $tot_amt - $form_data['amount_paying'];
                $this->load->view('partner/order_confirmation', $data);
            } else {
                echo 'error';
            }
        }
    }

    private function save_billing_shipping()
    {
        $billing = array(
            'order_id' => $order_id,
            'user_id' => $userdet['user_id'],
            'address1' => $billing_details['address1'],
            'address2' => $billing_details['address2'],
            'city' => $billing_details['city'],
            'state' => $billing_details['state'],
            'country' => $billing_details['country'],
            'zipcode' => $billing_details['zipcode'],
            'date_created' => date('Y-m-d H:i:s'),
        );
        $shipping = array(
            'order_id' => $order_id,
            'user_id' => $userdet['user_id'],
            'firstname' => $shipping_details['first_name'],
            'lastname' => $shipping_details['last_name'],
            'email' => $userdet['email'],
            'telephone' => $shipping_details['phone'],
            'address1' => $shipping_details['address1'],
            'address2' => $shipping_details['address2'],
            'city' => $shipping_details['city'],
            'state' => $shipping_details['state'],
            'country' => $shipping_details['country'],
            'zipcode' => $shipping_details['zipcode'],
            'date_created' => date('Y-m-d H:i:s'),
        );
    }

    private function saveCartItem($cartItem, $userId)
    {
        $savedShoe = $this->ShoeModel->savemodel($userId, $cartItem);
        $cartItem = json_decode($savedShoe['shoemodel'], true);
        $cartItem['shoeDesignId'] = $savedShoe['shoe_design_id'];
        $cartItem['details'] = $savedShoe;
        return $cartItem;
    }

    public function addtocart()
    {
        $this->load->helper('common');
        $shoe = $this->input->post('ShoeModel');
        $savedShoe = $this->ShoeModel->savemodel($this->user['user_id'], $shoe);
        $shoe = json_decode($savedShoe['shoemodel'], true);
        $shoe['shoeDesignId'] = $savedShoe['shoe_design_id'];
        $this->session->unset_userdata('ShoeModel');
        $this->session->set_userdata('ShoeModel', $shoe);

        $price = array(
            'CP' => 420,
            'CG' => 420,
            'CS' => 420,
            'AL' => 3000,
            'OS' => 2600,
        );
        $data = array(
            'id' => $this->session->userdata('session_id') . "_CS_" . sizeof($this->cart->contents()),
            'qty' => 1,
            'price' => getPricing($shoe, $price),
            'name' => 'Custom Shoe',
            'options' => array(
                "shoe" => json_encode($shoe),
            ),
        );
        $this->cart->insert($data);
        $angle = "_A1";
        $ext = ".png";
        $base_url = CustomShoeConfig::IMG_BASE;
        $cart_contents = $this->cart->contents();
        if (is_array($cart_contents)) {
            $items = array();
            $i = 0;
            $total = 0;
            foreach ($cart_contents as $key => $item) {
                $options = $this->cart->product_options($item['rowid']);
                $shoe = json_decode($options['shoe'], true);
                $shoe_design['last_style'] = $this->CustomShoeModel->get_last_style_details($shoe['style']['id']);
                $shoe_design['img_base'] = $shoe['style']['code'] . "_" . $shoe['toe']['type'] . $shoe['vamp']['type'] . $shoe['eyestay']['type'] . $shoe['foxing']['type'];
                $shoe_design['base'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['quarter']['matfolder'] . "/" . $shoe['style']['code'] . "_" . $shoe['quarter']['base'] . $angle . $ext;
                $shoe_design['toe'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['toe']['matfolder'] . "/" . $shoe_design['img_base'] . $shoe['toe']['material'] . $shoe['toe']['color'] . $shoe['toe']['part'] . $angle . $ext;
                $shoe_design['eyestay'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['eyestay']['matfolder'] . "/" . $shoe_design['img_base'] . $shoe['eyestay']['material'] . $shoe['eyestay']['color'] . $shoe['eyestay']['part'] . $angle . $ext;
                $shoe_design['vamp'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['vamp']['matfolder'] . "/" . $shoe_design['img_base'] . $shoe['vamp']['material'] . $shoe['vamp']['color'] . $shoe['vamp']['part'] . $angle . $ext;
                $shoe_design['foxing'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['foxing']['matfolder'] . "/" . $shoe_design['img_base'] . $shoe['foxing']['material'] . $shoe['foxing']['color'] . $shoe['foxing']['part'] . $angle . $ext;
                $shoe_design['stitch'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['stitch']['folder'] . "/" . $shoe_design['img_base'] . $shoe['stitch']['code'] . $angle . $ext;
                $shoe_design['lace'] = $shoe['last']['folder'] . "/" . $shoe['style']['folder'] . "/" . $shoe['lace']['folder'] . "/" . $shoe_design['img_base'] . $shoe['lace']['code'] . $angle . $ext;
                $shoe_design['size'] = $shoe['size'];
                $shoe_design['measurement'] = $shoe['measurement'];
                $items[$i]['shoe'] = $shoe_design;
                $items[$i]['shoedesign'] = $this->ShoeModel->get_shoe_design_details($shoe['shoeDesignId'], 0);
                $total += $item['subtotal'];
                $items[$i]['price'] = $item['price'];
                $items[$i]['quantity'] = $item['qty'];
                $items[$i]['subtotal'] = $item['subtotal'];
                $items[$i]['row_id'] = $item['rowid'];
                $i++;
            }
            $data['items'] = $items;
            $data['subtotal'] = number_format(round($total), 2);
            $data['base_url'] = $base_url;
        }
        $this->load->view('partner/cart_items', $data);
    }

    public function remove_cart_item()
    {
        $row_id = $this->input->post('row_id');
        $data = array(
            'rowid' => $row_id,
            'qty' => 0,
        );
        $this->cart->update($data);
    }

    public function update_quantity()
    {
        $row_id = $this->input->post('row_id');
        $quantity = $this->input->post('quantity');
        $data = array(
            'rowid' => $row_id,
            'qty' => $quantity,
        );
        $this->cart->update($data);
    }

    public $styles = array(
        array(
            'id' => 'oxford',
            'name' => 'Oxford',
        ),
        array(
            'id' => 'derby',
            'name' => 'Derby',
        ),
        array(
            'id' => 'monkstrap',
            'name' => 'Monkstrap',
        ),
        array(
            'id' => 'loafer',
            'name' => 'Loafer',
        ),
    );

    public $lasts = array(
        array(
            'id' => '1',
            'name' => 'The Townsend',
        ),
        array(
            'id' => '3',
            'name' => 'The Archibald',
        ),
        array(
            'id' => '5',
            'name' => 'The Wallace',
        ),
        array(
            'id' => '4',
            'name' => 'The Harvey',
        ),
        array(
            'id' => '2',
            'name' => 'The Eldridge',
        ),
    );

    public $var_width = array(
        array(
            'id' => 'D',
            'name' => 'D',
        ),
        array(
            'id' => 'E',
            'name' => 'E',
        ),
        array(
            'id' => 'EE',
            'name' => 'EE',
        ),
        array(
            'id' => 'EEE',
            'name' => 'EEE',
        ),
    );

    public $var_size = array(
        array(
            'id' => '5',
            'name' => '5',
        ),
        array(
            'id' => '5.5',
            'name' => '5.5',
        ),
        array(
            'id' => '6',
            'name' => '6',
        ),
        array(
            'id' => '6.5',
            'name' => '6.5',
        ),
        array(
            'id' => '7',
            'name' => '7',
        ),
        array(
            'id' => '7.5',
            'name' => '7.5',
        ),
        array(
            'id' => '8',
            'name' => '8',
        ),
        array(
            'id' => '8.5',
            'name' => '8.5',
        ),
        array(
            'id' => '9',
            'name' => '9',
        ),
        array(
            'id' => '9.5',
            'name' => '9.5',
        ),
        array(
            'id' => '10',
            'name' => '10',
        ),
        array(
            'id' => '10.5',
            'name' => '10.5',
        ),
        array(
            'id' => '11',
            'name' => '11',
        ),
        array(
            'id' => '11.5',
            'name' => '11.5',
        ),
        array(
            'id' => '12',
            'name' => '12',
        ),
        array(
            'id' => '12.5',
            'name' => '12.5',
        ),
        array(
            'id' => '13',
            'name' => '13',
        ),
        array(
            'id' => '13.5',
            'name' => '13.5',
        ),
        array(
            'id' => '14',
            'name' => '14',
        ),
    );

}
