<?php

class CustomShoe extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('CustomShoeConfig');
        $this->load->model('CustomShoeModel');
    }

    public function sizing()
    {
        if (!$this->session->userdata('BackByMenu')) {
            $tmp = $this->session->userdata('ShoeModel');
            if (isset($tmp['measurement'])) {
                $tmpSess['measurement'] = $tmp['measurement'];
            }
            if (isset($tmp['size'])) {
                $tmpSess['size'] = $tmp['size'];
            }
            $this->session->unset_userdata('ShoeModel');
            $this->session->set_userdata('ShoeModel', $tmpSess);
        }
        $this->session->unset_userdata('BackByMenu');

        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $data = array(
            "hideHeader" => "true",
            "bodyclass" => "step6",
            "additional_js" => array(
                base_url() . "assets/js/shoedesign/customshoe.js",
                base_url() . "assets/js/shoedesign/modelrenderer.js",
                base_url() . "assets/js/shoedesign/sizing.js",
                "jquery.sumoselect.min",
                base_url() . "assets/js/steps.js",
            ),
            "additional_css" => array("css/sumoselect", "css/step"),
        );
        if ($this->session->userdata('ShoeModel')) {
            $model = $this->session->userdata('ShoeModel');
            $data['sizeType'] = (isset($model['measurement'])) ? $model['measurement']['size_type'] : '';
        }
        $data['styledetails'] = $this->CustomShoeModel->get_style_details($model['style']['id']);

        $data['meta'] = '<meta name="description" content="Design your custom handmade shoes. ' . 'Select from over 2 billion design options in leathers, colors, features and styles.">';
        $data['page_title'] = 'Design your custom handmade shoes using our 3D designer ';
        $data['currentStep'] = 'Feet First';
        $this->load->view('shoedesign/sizing', $data);
    }

    public function index()
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $data = array(
            "hideHeader" => "true",
            "bodyclass" => "step1",
            "additional_js" => array(
                base_url() . "assets/js/shoedesign/customshoe.js",
                base_url() . "assets/js/shoedesign/styleselect.js",
            ),
        );
        $data['styles'] = $this->CustomShoeModel->get_styles(4);

        $data['meta'] = '<meta name="description" content="Design your custom handmade shoes. ' . 'Select from over 2 billion design options in leathers, colors, features and styles.">';
        $data['page_title'] = 'Design your custom handmade shoes using our 3D designer ';
        $data['currentStep'] = 'Essential Style';
        $this->load->view('shoedesign/style', $data);
    }

    public function last($style = '')
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $data = array(
            "hideHeader" => "true",
            "bodyclass" => "step2",
            "additional_js" => array(
                base_url() . "assets/js/shoedesign/customshoe.js",
                base_url() . "assets/js/shoedesign/lastselect.js",
            ),
        );
        if ($this->session->userdata('ShoeModel')) {
            $data['ShoeModel'] = $this->session->userdata('ShoeModel');
            if ($style != '') {
                $data['ShoeModel']['style']['styleName'] = $style;
            }
        } else if ($style != '') {
            $data['ShoeModel'] = array("style" => array("styleName" => $style));
            $this->session->set_userdata('ShoeModel', $data['ShoeModel']);
        } else {
            redirect('create-a-custom-shoe/select-style', 'refresh');
        }
        $data['lasts'] = $this->CustomShoeModel->get_last_styles($data['ShoeModel']['style']['styleName']);

        $data['meta'] = '<meta name="description" content="Design your custom handmade shoes. ' . 'Select from over 2 billion design options in leathers, colors, features and styles.">';
        $data['page_title'] = 'Design your custom handmade shoes using our 3D designer ';
        $this->load->view('shoedesign/last', $data);
    }

    public function features()
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $this->load->model('shoemodel');
        $data = array(
            "hideHeader" => "true",
            "bodyclass" => "step3",
            "additional_js" => array(
                base_url() . "assets/js/shoedesign/customshoe.js",
                base_url() . "assets/js/shoedesign/modelrenderer.js",
                base_url() . "assets/js/shoedesign/featureselect.js",
                base_url() . "assets/js/steps.js",
            ),
            "additional_css" => array("css/step"),
        );
        if ($this->session->userdata('ShoeModel')) {
            $model = $this->session->userdata('ShoeModel');
        }
        $data['relatedshoes'] = $this->shoemodel->get_related_shoes(array('style_id' => $model['style']['id'], 'sort' => 'q_base', 'order' => 'DESC'));
        $data['styledetails'] = $this->CustomShoeModel->get_style_details($model['style']['id']);
        $data['properties'] = $this->CustomShoeModel->style_properties_comb($model['style']['id'], true);

        $data['meta'] = '<meta name="description" content="Design your custom handmade shoes. ' . 'Select from over 2 billion design options in leathers, colors, features and styles.">';
        $data['page_title'] = 'Design your custom handmade shoes using our 3D designer ';
        $data['currentStep'] = 'Design Features';
        $this->load->view('shoedesign/feature', $data);
    }

    public function styling()
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $this->load->model('shoemodel');
        $data = array(
            "hideHeader" => "true",
            "bodyclass" => "step4",
            "additional_js" => array(
                base_url() . "assets/js/shoedesign/customshoe.js",
                base_url() . "assets/js/shoedesign/modelrenderer.js",
                base_url() . "assets/js/shoedesign/styling.js",
                base_url() . "assets/js/steps.js",
            ),
            "additional_css" => array("css/step"),
        );
        if ($this->session->userdata('ShoeModel')) {
            $model = $this->session->userdata('ShoeModel');
        }
        // $data['relatedshoes'] = $this->shoemodel->get_related_shoes(array('style_id' => $model['style']['id'], 'sort' => 's.date_created', 'order' => 'DESC'));
        $data['materials'] = $this->CustomShoeModel->style_material_colors();
        $data['styledetails'] = $this->CustomShoeModel->get_style_details($model['style']['id']);
        $data['model'] = $model;

        $data['meta'] = '<meta name="description" content="Design your custom handmade shoes. ' . 'Select from over 2 billion design options in leathers, colors, features and styles.">';
        $data['page_title'] = 'Design your custom handmade shoes using our 3D designer ';
        $data['currentStep'] = 'Materials / Colors';
        $this->load->view('shoedesign/styling', $data);
    }

    public function sole()
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $this->load->model('shoemodel');
        $data = array(
            "hideHeader" => "true",
            "bodyclass" => "step6",
            "additional_js" => array(
                "bootstrap.min",
                base_url() . "assets/js/shoedesign/customshoe.js",
                base_url() . "assets/js/shoedesign/modelrenderer.js",
                base_url() . "assets/js/shoedesign/soleselect.js",
            ), "additional_css" => array(
                "css/bootstrap-modal",
            ),
        );

        $data['soleArr'] = $this->CustomShoeModel->get_sole();

        if ($this->session->userdata('ShoeModel')) {
            $model = $this->session->userdata('ShoeModel');

            $l = $model['size']['left']['width'];
            $r = $model['size']['right']['width'];
            if ($l == 'Extra Wide' || $l == 'Extra Wide' || $r == 'Triple Wide' || $r == 'Triple Wide') {
                $model['sole']['stitch'] = 'ST1';
                $model['sole']['stitchCharge'] = '0';
                $model['sole']['stitchId'] = '1';
                unset($data['soleArr']['soleStitch'][1]);
                $this->session->set_userdata('ShoeModel', $model);
            }
            $material = $this->db->get_where('material_def', array('material_code' => $model['quarter']['material']))->row('material_name');
        }

        $data['relatedshoes'] = $this->shoemodel->get_related_shoes(array('style_id' => $model['style']['id'], 'sort' => 'q_base', 'order' => 'DESC'));
        $data['styledetails'] = $this->CustomShoeModel->get_style_details($model['style']['id']);
        $data['material'] = (isset($model['patina'])) ? 'patina' : ((!is_array($material) && $material != '' && $material != null) ? $material : '');

        $data['meta'] = '<meta name="description" content="Design your custom handmade shoes. ' . 'Select from over 2 billion design options in leathers, colors, features and styles.">';
        $data['page_title'] = 'Design your custom handmade shoes using our 3D designer ';
        $data['currentStep'] = 'Sole Attributes';
        $this->load->view('shoedesign/sole', $data);
    }

    public function stitchlace()
    {
        $req = $this->input->get();
        if (strcmp($req['source'], 'pepperjam') == 0) {
            $this->session->set_userdata('pepperjam', true);
        }
        $this->load->model('shoemodel');
        $data = array(
            "hideHeader" => "true",
            "bodyclass" => "step5",
            "additional_js" => array(
                base_url() . "assets/js/shoedesign/customshoe.js",
                base_url() . "assets/js/shoedesign/modelrenderer.js",
                base_url() . "assets/js/shoedesign/stitchlace.js",
                base_url() . "assets/js/steps.js",
            ),
            "additional_css" => array("css/step"),
        );
        if ($this->session->userdata('ShoeModel')) {
            $model = $this->session->userdata('ShoeModel');
            $sole = $this->db->get_where('style_sole', array('id' => $model['sole']['id']))->row('sole_name');
            $material = $this->db->get_where('material_def', array('material_code' => $model['quarter']['material']))->row('material_name');
        }
        if (!isset($model['monogram'])) {
            $monogram = array('leftShoe' => 0, 'leftSide' => 0, 'rightShoe' => 0, 'rightSide' => 0, 'text' => '');
            $model['monogram'] = $monogram;
            $this->session->unset_userdata('ShoeModel');
            $this->session->set_userdata('ShoeModel', $model);
        }
        $data['monogram'] = $model['monogram'];
        $data['stitch_model'] = $model['stitch'];
        $data['material'] = (isset($model['patina'])) ? 'patina' : ((!is_array($material) && $material != '' && $material != null) ? $material : '');
        $data['sole'] = ((!is_array($sole) && $sole != '' && $sole != null) ? $sole : '');
        $data['relatedshoes'] = $this->shoemodel->get_related_shoes(array('style_id' => $model['style']['id'], 'sort' => 'public_name', 'order' => 'ASC'));
        $data['styledetails'] = $this->CustomShoeModel->get_style_details($model['style']['id']);
        $data['stitches'] = [];
        // $data['stitches'] = $this->CustomShoeModel->get_style_stitches($model['style']['id']);
        if ($data['styledetails']['is_lace_enabled']) {
            $data['laces'] = $this->CustomShoeModel->get_style_laces($model['style']['id']);
            $data['lace_model'] = $model['lace'];
        }

        $data['meta'] = '<meta name="description" content="Design your custom handmade shoes. ' . 'Select from over 2 billion design options in leathers, colors, features and styles.">';
        $data['page_title'] = 'Design your custom handmade shoes using our 3D designer ';
        $data['currentStep'] = 'Personal Touches';
        $this->load->view('shoedesign/stitchlace', $data);
    }

    public function related($designId)
    {
        $this->load->model('ShoeModel');
        $shoedetail = $this->ShoeModel->ge_design_model($designId);
        $shoemodel = json_decode($shoedetail['shoemodel'], true);
        $shoemodel['reDesignId'] = $designId;
        $shoemodel['angle'] = 'A0';
        if (!isset($shoemodel['style']['isLaceEnabled'])) {
            $this->load->model('CustomShoeModel');
            $model = $this->CustomShoeModel->get_style_defaults($shoemodel['style']['id']);
            $shoemodel['style']['isLaceEnabled'] = $model['style']['isLaceEnabled'];
        }
        $this->session->unset_userdata('ShoeModel');
        $this->session->set_userdata('ShoeModel', $shoemodel);
        echo json_encode(array(
            "success" => true,
        ));
    }

    public function saveStepDetails()
    {
        $this->session->unset_userdata('ShoeModel');
        $model = $this->input->post("ShoeModel");
        $step = $this->input->post('CurrentStep');
        $isback = $this->input->post('IsBack');

        $this->session->unset_userdata('BackByMenu');
        if ($isback == 'Yes') {
            $this->session->set_userdata('BackByMenu', 'Yes');
        }

        $this->session->set_userdata('ShoeModel', $model);
        if ($step == 'featureSelector' && $isback == 'Yes') {
            $this->session->set_userdata('prev_style', $model['style']['id']);
        }
        if ($step == "sizing1" || $step == "featureSelector" || $step == "stitchlace" || $step == "styling") {
            $this->saveDesign(false);
        }
        if ($step == "stitchlace" && $isback == 'No') {
            $this->addtocart();
        }
        echo json_encode(array("success" => true));
    }

    public function getShoeModel()
    {
        $result = array("success" => true);
        $currentStep = $this->input->post('CurrentStep');
        $model = array();
        if ($this->session->userdata('ShoeModel')) {
            $model = $this->session->userdata('ShoeModel');
        }

        switch ($currentStep) {
            case "featureSelector":
                if (isset($model['style']) && isset($model['style']['id']) && trim($model['style']['id']) != '' && $model['style']['id'] > 0) {
                    $isPrev = false;
                    if ($this->session->userdata('prev_style') && $this->session->userdata('prev_style') != $model['style']['id']) {
                        $isPrev = true;
                        $this->session->unset_userdata('prev_style');
                    }
                    if ($model['toe']['type'] == 'T00' && $model['vamp']['type'] == 'V00' && $model['eyestay']['type'] == 'E00' && $model['foxing']['type'] == 'F00') {
                        $isPrev = true;
                    }
                    $modelTemp = $this->session->userdata('ShoeModel');
                    if (!isset($model['toe']) || $isPrev) {
                        $model = $this->CustomShoeModel->get_style_defaults($model['style']['id']);
                    } else if (!isset($model['last']['last_name'])) {
                        $names = $this->CustomShoeModel->get_last_style_name($model['reDesignId']);
                        $model['last']['last_name'] = $names['last_name'];
                        $model['style']['style_name'] = $names['style_name'];
                    }
                    $model['measurement'] = $modelTemp['measurement'];
                    $model['size'] = $modelTemp['size'];
                    $model['sole'] = $modelTemp['sole'];
                    $model['shoetree'] = $modelTemp['shoetree'];

                    $this->session->unset_userdata('ShoeModel');
                    $this->session->set_userdata('ShoeModel', $model);
                } else {
                    $result['success'] = false;
                }
                break;
            case "sizing":
                $this->load->model('shoemodel');
                $model = $this->shoemodel->get_shoe_structure();

                $this->session->unset_userdata('ShoeModel');
                $this->session->set_userdata('ShoeModel', $model);

                break;
            default:
                break;
        }
        $result['model'] = $model;
        echo json_encode($result);
    }

    public function getInvalidFeatures()
    {
        $data = $this->input->post();
        $retproperties = $this->CustomShoeModel->get_invalid_properties($data);
        echo json_encode($retproperties);
    }

    public function getPrice()
    {
        $this->load->helper('common');
        $model = $this->input->post('ShoeModel');
        echo '$' . getPricing($model);
    }

    public function get_image_base()
    {
        $imgpath = $this->input->post('imagepath');
        // $path = $_SERVER['DOCUMENT_ROOT'] . '/files/images/' . $imgpath;
        $path = ApplicationConfig::IMG_BASE . $imgpath;
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $result = array("exists" => false);
        // if (file_exists($path)) {
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        $result['exists'] = true;
        $result['base'] = $base64;
        // }
        echo json_encode($result);
    }

    private function addtocart()
    {
        $shoeModel = $this->session->userdata('ShoeModel');
        $prodId = getMailChimpProductId($shoeModel, 'CS');
        $this->load->helper('common');
        $shoe = $this->session->userdata('ShoeModel');
        $data = array(
            'id' => $this->session->userdata('session_id') . "_CS_" . sizeof($this->cart->contents()),
            'qty' => 1,
            'price' => getPricing($shoe),
            'name' => 'Custom Shoe',
            'options' => array(
                "shoe" => json_encode($this->session->userdata('ShoeModel')),
            ),
        );
        $rowid = $this->cart->insert($data);

        $sessData = $this->session->userdata('user_details');
        if (isset($sessData['user_id']) && $rowid != '' && $rowid != null) {
            $data['cartId'] = 'AWL_CART_' . $sessData['user_id'];
            $data['rowid'] = $rowid;
            addCartToMailChimp($data, $prodId, $sessData);
        }
    }

    public function saveDesign($flag = true)
    {
        $this->load->model('ShoeModel');
        $response = array();
        $userdetails = $this->session->userdata('user_details');
        if ($userdetails != false) {
            $userId = $userdetails['user_id'];
            $response['isValidLogin'] = true;
        } else {
            $userId = null;
            $response['isValidLogin'] = false;
        }
        if ($flag) {
            $model = $this->input->post('shoemodel');
        } else {
            $model = $this->session->userdata('ShoeModel');
        }
        $this->session->unset_userdata('ShoeModel');

        if (!isset($model['shoeDesignId'])) {
            $savedShoe = $this->ShoeModel->savemodel($userId, $model);
            $model = json_decode($savedShoe['shoemodel'], true);
            $model['shoeDesignId'] = $savedShoe['shoe_design_id'];
            $response['shoe'] = $model;
            $response['details'] = array("image" => $savedShoe['image_file'], "name" => $savedShoe['name']);
        } else {
            $details = $this->ShoeModel->updateDesign($model, $userId);
            $response['shoe'] = $model;
            $response['details'] = array("image" => $details[0]['image_file'], "name" => $details[0]['name']);
        }
        $this->session->set_userdata('ShoeModel', $model);
        if ($flag) {
            echo json_encode($response);
        }

    }

    public function getRelated()
    {
        $this->load->model('shoemodel');
        $search = $this->input->post('criteria');
        $criteria = $search;
        if (isset($search['part'])) {
            $criteria[$search['part'] . '_id'] = $search['partId'];
        }
        if (isset($search['style'])) {
            $criteria['style_name'] = $search['style'];
        }
        $related = $this->shoemodel->get_related_shoes($criteria);
        echo json_encode($related);
    }

}
