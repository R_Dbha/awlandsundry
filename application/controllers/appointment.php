<?php

class Appointment extends CI_Controller
{

    public $data;
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('AppointmentModel');
    }

    public function index()
    {
        $data = array();
        $data['additional_js'] = array('appointment', 'jquery.datetimepicker.full.min');
        $data['additional_css'] = array('css/appointment', 'css/jquery.datetimepicker');
        $data['bodyclass'] = 'appointment';
        $data['page_title'] = 'Appointment - Awl & Sundry';
        $data['meta'] = '<meta name="description" content="Enter your Awl & Sundry gift card number and your discount will be reflected at checkout." >';
        $this->load->view('appointment', $data);
    }

    public function saveAppointment()
    {
        $input = $this->input->post();
        $date = strtotime(str_replace('/', '-', $input['appointment_date']));
        $time = date('Hi', $date);
        $day = date('D', $date);
        if (!$this->AppointmentModel->checkAvailability($input['appointment_date'])) {
            $error = array('status' => 'error', 'message' => "Appointment slot unavailable, please book another slot.");
            echo json_encode($error);
        } else if ($date < strtotime('22-01-2017 10:00:00')) {
            $error = array('status' => 'error', 'message' => "Appointment available after 21/01/2017, please book another slot.");
            echo json_encode($error);
        } else if ($time < '1000' || $time >= '1900') {
            if ($day == 'Tue' && $time >= '1300') {
                $error = array('status' => 'error', 'message' => "Appointment available on Tuesday's between 10 A.M. to 1 P.M.");
                echo json_encode($error);
            }
            $error = array('status' => 'error', 'message' => "Appointment available betweeen 10 A.M. to 7 P.M.");
            echo json_encode($error);
        } else {
            $dbDateTime = DateTime::createFromFormat('d/m/Y H:i', $input['appointment_date'])->format('Y-m-d H:i:s');
            $appointmentDetails = array(
                "user_name" => $input['name'],
                "email_id" => $input['email'],
                "phone_number" => $input['phone'],
                "special_request" => $input['special_request'],
                "us_shoe_size" => $input['us_shoe_size'],
                "appointment_datetime" => $dbDateTime);
            $result = $this->AppointmentModel->saveAppointment($appointmentDetails);
            if ($result['status'] == 'success') {
                $this->send_appointment_mail($result);
            }
            echo json_encode($result);
        }
    }

    public function getAllAppointments()
    {
        $result = $this->AppointmentModel->getFutureAppointments();
        echo json_encode($result);
    }

    public function send_appointment_mail($result)
    {
        $nameArray = explode(' ', $result['user_name']);
        $data['name'] = $nameArray[0];
        $data['appointment_date'] = date_format(date_create($result['appointment_datetime']), 'g:ia \o\n l jS F Y');

        $this->load->library('email');
        $this->email->from('wecare@awlandsundry.com', 'Awl & Sundry');
        $this->email->to($result['email_id']);
        $this->email->bcc($this->config->item('contact_email'));
        $this->email->subject("Your Awl & Sundry appointment has been confirmed.");
        $msg = $this->load->view('emails/appointmentMail', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }
}
