<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Aftership extends CI_Controller
{

    private $_api_url = "https://api.aftership.com/v4/";
    private $_api_key;

    public function __construct()
    {
        parent::__construct();
        $this->load->config('aftership');
        $this->_api_key = $this->config->item('aftership_api_key');
    }

    private function track($tracking_number)
    {
        $headers = array(
            'aftership-api-key:' . $this->_api_key,
            'content-type:application/json',
        );
        $url = $this->_api_url . 'trackings';
        $request = array();
        $request['tracking'] = array();
        $request['tracking']['tracking_number'] = $tracking_number;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($curl);
        curl_close($curl);

        return $content;
    }

    private function get_tracking($slug = '', $tracking_number = '')
    {
        $headers = array(
            'aftership-api-key:' . $this->_api_key,
            'content-type:application/json',
        );
        $url = $this->_api_url . 'trackings';
        if ($slug !== '' && $tracking_number !== '') {
            $url = $this->_api_url . 'trackings/' . $slug . '/' . $tracking_number;
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($curl);
        curl_close($curl);
        return $content;
    }

    public function tracking()
    {
        $tracking_number = $this->input->post('tracking_number');
        $response = $this->track($tracking_number);
        if ($response) {
            $res = json_decode($response);
            $slug = $res->data->tracking->slug;
        }
        $result_array = json_decode($this->get_tracking($slug, $tracking_number));
        $tracking = $result_array->data->tracking;
        $checkpoints = $tracking->checkpoints;
        $size = sizeof($checkpoints);
        $points = array();
        for ($i = $size - 1; $i >= 0; $i--) {
            $points[] = $checkpoints[$i];
        }
        $data['tracking_number'] = $tracking_number;
        $data['tracking_status'] = $tracking->tag;
        $data['slug'] = strtoupper($slug);
        $data['points'] = $points;
        $this->load->view('aftership', $data);
    }

    public function test($t)
    {
        $result_array = json_decode($this->get_tracking('usps', $t));
        $tracking = $result_array->data->tracking;
        $checkpoints = $tracking->checkpoints;
        $size = sizeof($checkpoints);
        $points = array();
        for ($i = $size - 1; $i >= 0; $i--) {
            $points[] = $checkpoints[$i];
        }
        print_r($result_array);
    }

}
