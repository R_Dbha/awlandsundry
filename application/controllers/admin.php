<?php

/**
 * Description of common
 *
 * @author kraftlabs
 * @property CustomShoeModel $CustomShoeModel
 * @property shoemodel $shoemodel
 */
class Admin extends CI_Controller
{

    private $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('common_helper');
        $this->load->model('AdminViewModel');
        $this->load->model('giftcardmodel');
        $this->load->model('UserModel');
        $this->load->model('OrderModel');
        $this->load->model('ShoeModel');
        $this->load->model('PartnerModel');
        $this->load->model('AppointmentModel');
        $this->load->library('form_validation');
        $this->load->library('pagination');

        $this->load->library('basic_view');
        $this->basic_view->set_default_js(array(
            'resources/scripts/jquery-1.3.2.min',
            'resources/scripts/jquery-1.10.2',
            'resources/scripts/jquery-ui-1.10.4.mem.min',
            'resources/scripts/simpla.jquery.configuration',
            'resources/scripts/facebox',
            'resources/scripts/jquery.wysiwyg',
            'assets/js/admin/common',
            'assets/js/admin/jquery.fancybox',
        ));
        $this->basic_view->set_default_css(array(
            'resources/css/reset',
            'resources/css/cupertino/jquery-ui-1.10.4.custom.min',
            'resources/css/style',
            'resources/css/invalid',
            'resources/css/blue',
            'assets/css/admin/common',
            'assets/css/admin/jquery.fancybox',
        ));
        $this->data = $this->basic_view->get_default_view();
        $this->basic_view->set_default_css('resources/css/ie');
        $this->data['menu'] = 'admin/menu';
        $this->data['profile_link'] = 'admin/profile_link';
        $this->data['ie7css'] = $this->basic_view->get_css();
        $user = $this->session->userdata('user_details');
        $this->data['profile_link_data']['orders'] = 'admin/blank';
        $this->data['profile_link_data']['orders_data'] = '';
        $this->data['profile_link_data']['user'] = $user;
        $this->data['user'] = $user;
    }

    public function index()
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != '1') {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $this->data['title'] = 'Awl &amp; Sundry Admin - Dashboard';

        $year = date('Y');
        $content_data['YTD_Revenue'] = 0;
        $content_data['YTD_COGS'] = 0;
        $content_data['YTD_Gross_margin'] = 0;
        $content_data['YTD_other_expenses'] = 0;
        $content_data['YTD_Net_Profit_loss'] = 0;
        $content_data['YTD_customer_orders'] = 0;
        for ($i = 1; $i <= 12; $i++) {
            $content_data['monthly_report']["$i/$year"] = $this->AdminViewModel->get_order_summary($i, $year);
            $content_data['monthly_report']["$i/$year"]['Gross_margin'] = $content_data['monthly_report']["$i/$year"]['Revenue'] - $content_data['monthly_report']["$i/$year"]['COGS'];
            $content_data['monthly_report']["$i/$year"]['Other_expenses'] = 0;
            $content_data['monthly_report']["$i/$year"]['Net_profit_loss'] = $content_data['monthly_report']["$i/$year"]['Gross_margin'] - $content_data['monthly_report']["$i/$year"]['Other_expenses'];
            $content_data['monthly_report']["$i/$year"]['profit_loss'] = ($content_data['monthly_report']["$i/$year"]['Revenue'] >= $content_data['monthly_report']["$i/$year"]['COGS']) ? "profit" : "loss";

            $content_data['YTD_Revenue'] += $content_data['monthly_report']["$i/$year"]['Revenue'];
            $content_data['YTD_COGS'] += $content_data['monthly_report']["$i/$year"]['COGS'];
            $content_data['YTD_Gross_margin'] += $content_data['monthly_report']["$i/$year"]['Gross_margin'];
            $content_data['YTD_other_expenses'] += $content_data['monthly_report']["$i/$year"]['Other_expenses'];
            $content_data['YTD_Net_Profit_loss'] += $content_data['monthly_report']["$i/$year"]['Net_profit_loss'];
            $content_data['YTD_customer_orders'] += $content_data['monthly_report']["$i/$year"]['Total_customer_orders'];
        }
        $content_data['profit_loss'] = ($content_data['YTD_Revenue'] >= $content_data['YTD_COGS']) ? "profit" : "loss";
        $this->data['content'] = 'admin/dashboard';
        $this->data['content_data'] = $content_data;
        $this->load->view('admin/template', $this->data);
    }

    public function designs_vs_orders($page = 0)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 1) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $config['per_page'] = 10;
        $config['page'] = $page;
        $result = $this->AdminViewModel->designs_vs_orders($config);
        $data['rows'] = $result['rows'];
        $config['base_url'] = base_url() . '/index.php/admin/designs_vs_orders/';
        $config['anchor_class'] = 'page';
        $config['total_rows'] = $result['cnt'];
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/designs_vs_orders', $data);
    }

    public function admin_view($page = 0)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != '1') {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $config['per_page'] = 10;
        $config['page'] = $page;
        $styles = [
            ['id' => 'Oxford', 'name' => 'Oxford'],
            ['id' => 'Derby', 'name' => 'Derby'],
            ['id' => 'Monk', 'name' => 'Monk'],
            ['id' => 'Loafer', 'name' => 'Loafer'],
        ];
        $lasts = [
            ['id' => 'The Townsend', 'name' => 'The Townsend'],
            ['id' => 'The Archibald', 'name' => 'The Archibald'],
            ['id' => 'The Wallace', 'name' => 'The Wallace'],
            ['id' => 'The Harvey', 'name' => 'The Harvey'],
            ['id' => 'The Eldridge', 'name' => 'The Eldridge'],
        ];
        $order_status = [
            ['id' => '', 'name' => 'All'],
            ['id' => 'Order acknowledged', 'name' => 'Order Acknowledged'],
            ['id' => 'Manufacturing in progress', 'name' => 'Manufacturing in progress'],
            ['id' => 'order shipped', 'name' => 'Order Shipped'],
            ['id' => 'Refund', 'name' => 'Refund'],
            ['id' => 'testing', 'name' => 'Testing'],
        ];

        $filters = $this->input->post();
        $this->load->model('PartnerModel');
        $spring_order_ids = $this->AdminViewModel->get_orders_by_promocode('A&S SPRG 14');
        $spring_order_ids = array_merge($spring_order_ids, $this->AdminViewModel->get_kickstarter_orders());
        $spring_order_ids = array_merge($spring_order_ids, $this->PartnerModel->get_partner_orders());
        if ($filters !== false) {
            $order_ids = array();
            $this->form_validation->set_rules('style', 'style', '');
            $this->form_validation->set_rules('last', 'last', '');
            $this->form_validation->set_rules('invoice_number', 'invoice_number', '');
            $this->form_validation->set_rules('order_number', 'order_number', '');
            $this->form_validation->set_rules('customer_name', 'customer_name', '');
            $this->form_validation->set_rules('order_status', 'order_status', '');
            $this->form_validation->run();

            if ($filters['style'] !== '' || $filters['last'] !== '') {
                $order_ids = $this->AdminViewModel->getOrdersByStyleNameLastName($filters['style'], $filters['last']);
            }
            if ($filters['customer_name'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByCustomerName($filters['customer_name']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrdersByCustomerName($filters['customer_name']);
                }
            }
            if ($filters['order_number'] !== '' || $filters['invoice_number'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrders($filters['order_number'], $filters['invoice_number']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrders($filters['order_number'], $filters['invoice_number']);
                }
            }
            if ($filters['order_status'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByStatus($filters['order_status']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrdersByStatus($filters['order_status']);
                }
            }
            $order_ids = array_diff($order_ids, $spring_order_ids);
            $result = $this->AdminViewModel->getProcessedOrders($config, $order_ids, $spring_order_ids);
        } else {
            $result = $this->AdminViewModel->getProcessedOrders($config, array(), $spring_order_ids);
        }
        $data['style'] = $this->generate_options($styles, 0, 0, set_value('style', ''));
        $data['last'] = $this->generate_options($lasts, 0, 0, set_value('last', ''));
        $data['order_status'] = $this->generate_options($order_status, 0, 0, set_value('order_status', ''));
        $data['orders'] = $result;
        $data['orders']['status'] = set_value('order_status', '');
        $config['base_url'] = base_url() . '/index.php/admin/admin_view/';
        $config['anchor_class'] = 'class="page" ';
        $config['total_rows'] = $result['cnt'];
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/admin_index', $data);
    }

    public function partner_view($page = 0)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 1) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $config['per_page'] = 10;
        $config['page'] = $page;

        $styles = [
            ['id' => 'Oxford', 'name' => 'Oxford'],
            ['id' => 'Derby', 'name' => 'Derby'],
            ['id' => 'Monk', 'name' => 'Monk'],
            ['id' => 'Loafer', 'name' => 'Loafer'],
        ];

        $lasts = [
            ['id' => 'The Townsend', 'name' => 'The Townsend'],
            ['id' => 'The Archibald', 'name' => 'The Archibald'],
            ['id' => 'The Wallace', 'name' => 'The Wallace'],
            ['id' => 'The Harvey', 'name' => 'The Harvey'],
            ['id' => 'The Eldridge', 'name' => 'The Eldridge'],
        ];

        $order_status = [
            ['id' => '', 'name' => 'All'],
            ['id' => 'Order acknowledged', 'name' => 'Order Acknowledged'],
            ['id' => 'Manufacturing in progress', 'name' => 'Manufacturing in progress'],
            ['id' => 'order shipped', 'name' => 'Order Shipped'],
            ['id' => 'Refund', 'name' => 'Refund'],
            ['id' => 'testing', 'name' => 'Testing'],
        ];

        $filters = $this->input->post();
        $this->load->model('PartnerModel');
        $order_ids = $this->PartnerModel->get_partner_orders();

        if ($filters !== false) {
            $this->form_validation->set_rules('style', 'style', '');
            $this->form_validation->set_rules('last', 'last', '');
            $this->form_validation->set_rules('invoice_number', 'invoice_number', '');
            $this->form_validation->set_rules('order_number', 'order_number', '');
            $this->form_validation->set_rules('customer_name', 'customer_name', '');
            $this->form_validation->set_rules('order_status', 'order_status', '');
            $this->form_validation->run();

            if ($filters['style'] !== '' || $filters['last'] !== '') {
                $order_ids = $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByStyleNameLastName($filters['style'], $filters['last']));
            }
            if ($filters['customer_name'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByCustomerName($filters['customer_name']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrdersByCustomerName($filters['customer_name']);
                }
            }
            if ($filters['order_number'] !== '' || $filters['invoice_number'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrders($filters['order_number'], $filters['invoice_number']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrders($filters['order_number'], $filters['invoice_number']);
                }
            }
            if ($filters['order_status'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByStatus($filters['order_status']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrdersByStatus($filters['order_status']);
                }
            }
            $result = $this->AdminViewModel->getProcessedOrders($config, $order_ids);
        } else {
            $result = $this->AdminViewModel->getProcessedOrders($config, $order_ids);
        }
        $data['style'] = $this->generate_options($styles, 0, 0, set_value('style', ''));
        $data['last'] = $this->generate_options($lasts, 0, 0, set_value('last', ''));
        $data['order_status'] = $this->generate_options($order_status, 0, 0, set_value('order_status', ''));
        $data['orders'] = $result;
        $data['orders']['status'] = set_value('order_status', '');
        $config['base_url'] = base_url() . '/index.php/admin/partner_view/';
        $config['anchor_class'] = 'page';
        $config['total_rows'] = $result['cnt'];
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/partner_index', $data);
    }

    public function spring_view($page = 0)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 1) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $config['per_page'] = 10;
        $config['page'] = $page;

        $styles = [
            ['id' => 'Oxford', 'name' => 'Oxford'],
            ['id' => 'Derby', 'name' => 'Derby'],
            ['id' => 'Monk', 'name' => 'Monk'],
            ['id' => 'Loafer', 'name' => 'Loafer'],
        ];

        $lasts = [
            ['id' => 'The Townsend', 'name' => 'The Townsend'],
            ['id' => 'The Archibald', 'name' => 'The Archibald'],
            ['id' => 'The Wallace', 'name' => 'The Wallace'],
            ['id' => 'The Harvey', 'name' => 'The Harvey'],
            ['id' => 'The Eldridge', 'name' => 'The Eldridge'],
        ];

        $order_status = [
            ['id' => '', 'name' => 'All'],
            ['id' => 'Order acknowledged', 'name' => 'Order Acknowledged'],
            ['id' => 'Manufacturing in progress', 'name' => 'Manufacturing in progress'],
            ['id' => 'order shipped', 'name' => 'Order Shipped'],
            ['id' => 'Refund', 'name' => 'Refund'],
            ['id' => 'testing', 'name' => 'Testing'],
        ];

        $filters = $this->input->post();
        $order_ids = $this->AdminViewModel->get_orders_by_promocode('A&S SPRG 14');
        if ($filters !== false) {

            $this->form_validation->set_rules('style', 'style', '');
            $this->form_validation->set_rules('last', 'last', '');
            $this->form_validation->set_rules('invoice_number', 'invoice_number', '');
            $this->form_validation->set_rules('order_number', 'order_number', '');
            $this->form_validation->set_rules('customer_name', 'customer_name', '');
            $this->form_validation->set_rules('order_status', 'order_status', '');
            $this->form_validation->run();

            if ($filters['style'] !== '' || $filters['last'] !== '') {
                $order_ids = $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByStyleNameLastName($filters['style'], $filters['last']));
            }
            if ($filters['customer_name'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByCustomerName($filters['customer_name']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrdersByCustomerName($filters['customer_name']);
                }
            }
            if ($filters['order_number'] !== '' || $filters['invoice_number'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrders($filters['order_number'], $filters['invoice_number']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrders($filters['order_number'], $filters['invoice_number']);
                }
            }
            if ($filters['order_status'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByStatus($filters['order_status']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrdersByStatus($filters['order_status']);
                }
            }
            $result = $this->AdminViewModel->getProcessedOrders($config, $order_ids);
        } else {
            $result = $this->AdminViewModel->getProcessedOrders($config, $order_ids);
        }
        $data['style'] = $this->generate_options($styles, 0, 0, set_value('style', ''));
        $data['last'] = $this->generate_options($lasts, 0, 0, set_value('last', ''));
        $data['order_status'] = $this->generate_options($order_status, 0, 0, set_value('order_status', ''));
        $data['orders'] = $result;
        $data['orders']['status'] = set_value('order_status', '');
        $config['base_url'] = base_url() . '/index.php/admin/spring_view/';
        $config['anchor_class'] = 'page';
        $config['total_rows'] = $result['cnt'];
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/spring_index', $data);
    }

    public function remake_shoe()
    {
        $order_id = $this->input->post('order_id');
        $shoe_design_id = $this->input->post('shoe_design_id');
        $vendor_id = $this->input->post('vendor_id');
        $user_id = $this->input->post('user_id');
        $comments = $this->input->post('comments');
        $recieved_date = $this->input->post('recieved_date');

        $order_summary = $this->OrderModel->getOrderDet($order_id);
        $order_details = $this->OrderModel->getbillingNshippingDetails($order_id);

        $items = $order_details['order']['items'];
        foreach ($items as $item) {
            if ($item['design_id'] == $shoe_design_id) {
                $order_items['design_id'] = $shoe_design_id;
                $order_items['item_id'] = $item['item_id'];
                $order_items['item_name'] = $item['item_name'];
                $order_items['left_shoe'] = $item['left_shoe'];
                $order_items['right_shoe'] = $item['right_shoe'];
                $order_items['item_amt'] = 0;
                $order['items'][] = $order_items;
            }
        }
        $order_main['invoice_no'] = $this->OrderModel->getinvoiceno();
        $order_main['user_id'] = $order_summary[0]['user_id'];
        $order_main['currency'] = 'USD';
        $order_main['gross_amt'] = 0;
        $order_main['user_agent'] = $this->input->user_agent();
        $order_main['discount'] = 0;
        $order_main['discount_perc'] = 0;
        $order_main['order_modified'] = date('Y-m-d H:i:s');
        $order_main['payment_status'] = 'Processed';
        $order_main['order_source'] = $order_summary[0]['order_source'];
        $order['order_main'] = $order_main;

        $new_order_id = $this->OrderModel->saveorder($order);
        $data = array();
        $billing_details = $order_details['billing'][0];
        $shipping_details = $order_details['shipping'][0];
        $billing = array(
            'order_id' => $new_order_id,
            'user_id' => $billing_details['user_id'],
            'address1' => $billing_details['address1'],
            'address2' => $billing_details['address2'],
            'city' => $billing_details['city'],
            'state' => $billing_details['state'],
            'country' => $billing_details['country'],
            'zipcode' => $billing_details['zipcode'],
            'date_created' => date('Y-m-d H:i:s'),
        );
        $shipping = array(
            'order_id' => $new_order_id,
            'user_id' => $shipping_details['user_id'],
            'firstname' => $shipping_details['firstname'],
            'lastname' => $shipping_details['lastname'],
            'email' => $shipping_details['email'],
            'telephone' => $shipping_details['telephone'],
            'address1' => $shipping_details['address1'],
            'address2' => $shipping_details['address2'],
            'city' => $shipping_details['city'],
            'state' => $shipping_details['state'],
            'country' => $shipping_details['country'],
            'zipcode' => $shipping_details['zipcode'],
            'date_created' => date('Y-m-d H:i:s'),
        );
        $data['billing'] = $billing;
        $data['shipping'] = $shipping;

        $this->OrderModel->savebilling($data);
        $this->OrderModel->insertorderStatus($new_order_id);
        $order_summary = $this->OrderModel->getOrderDet($new_order_id);
        $this->AdminViewModel->assignToVendor($new_order_id, $vendor_id, $order_summary[0]['invoice_no']);
        $details = array(
            'order_id' => $order_id,
            'new_order_id' => $new_order_id,
            'shoe_design_id' => $shoe_design_id,
            'comments' => $comments,
        );
        $this->OrderModel->insertRemakeDetails($details);
        $this->sendOrdermailtoManufacturer($new_order_id, $vendor_id);
        redirect('admin/admin_view', 'refresh');
    }

    public function remake_view()
    {
        $order_id = $this->input->post('order_id');
        $shoe_design_id = $this->input->post('shoe_design_id');

        $order_summary = $this->OrderModel->getOrderDet($order_id);
        $status = $this->AdminViewModel->getStatus($order_id);
        $vendor = $this->UserModel->get_user_details($status[0]['user_id']);
        $customer = $this->UserModel->get_user_details($order_summary[0]['user_id']);

        $this->data['title'] = 'Awl &amp; Sundry Admin - Remake Shoe';

        $content_data['order_id'] = $order_id;
        $content_data['invoice_no'] = $status[0]['invoice_no'];
        $content_data['customer_name'] = $customer['first_name'] . ' ' . $customer['last_name'];
        $content_data['user_id'] = $order_summary[0]['user_id'];
        $content_data['shoe_design_id'] = $shoe_design_id;
        $content_data['vendor'] = $vendor['first_name'];
        $content_data['vendor_id'] = $vendor['user_id'];

        $this->data['content'] = 'admin/remake_view';
        $this->data['content_data'] = $content_data;
        $this->load->view('admin/template', $this->data);
    }

    public function editcustomer()
    {
        $customer_id = $this->input->post('user_id');
        $category = $this->input->post('customer_category');
        $this->AdminViewModel->change_customer_category($customer_id, $category);
        redirect('admin/viewcustomer/' . $customer_id, 'refresh');
    }

    public function viewcustomer($customer_id)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 1) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $customer_category = [
            ['id' => 'Customer', 'name' => 'Customer'],
            ['id' => 'Blogger', 'name' => 'Blogger'],
            ['id' => 'Marketing', 'name' => 'Marketing'],
        ];

        $this->data['title'] = 'Awl &amp; Sundry Admin - View Customer';

        $content_data = $this->UserModel->get_user_alldetails($customer_id);
        $content_data['customer_category'] = $this->generate_options($customer_category, 0, 0, $content_data['customer_category']);

        $this->data['content'] = 'admin/viewcustomer';
        $this->data['content_data'] = $content_data;

        $this->load->view('admin/template', $this->data);
    }

    public function customers($page = 0)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 1) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $config['per_page'] = 10;
        $config['page'] = $page;

        $customer_category = [
            ['id' => '', 'name' => 'All'],
            ['id' => 'Customer', 'name' => 'Customer'],
            ['id' => 'Blogger', 'name' => 'Blogger'],
            ['id' => 'Marketing', 'name' => 'Marketing'],
        ];
        $filters = $this->input->post();

        $search = array();
        if ($filters !== false) {
            $order_ids = array();
            $this->form_validation->set_rules('customer_category', 'customer_category', '');
            $this->form_validation->set_rules('name', 'name', '');
            $this->form_validation->set_rules('email', 'email', '');
            $this->form_validation->run();
            $search['customer_category'] = $filters['customer_category'];
            $search['first_name'] = $filters['name'];
            $search['last_name'] = $filters['name'];
            $search['email'] = $filters['email'];
        }

        $this->data['title'] = 'Awl &amp; Sundry Admin - Customers';

        $this->data['content'] = 'admin/customers';

        $content_data['customer_category'] = $this->generate_options($customer_category, 0, 0, set_value('customer_category', ''));
        $content_data['customers'] = $this->AdminViewModel->getCustomers($config, $search);

        $config['total_rows'] = $content_data['customers']['cnt'];
        $config['base_url'] = base_url() . '/index.php/admin/customers/';
        $config['anchor_class'] = '';
        $this->pagination->initialize($config);
        $content_data['pagination'] = $this->pagination->create_links();
        $this->data['content_data'] = $content_data;

        $this->load->view('admin/template', $this->data);
    }

    public function payments()
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 1) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $payment_details['data'] = $this->AdminViewModel->getPaymentDetails();
        $payment_details['payments'] = $this->AdminViewModel->getPayments();
        $vendors = $this->AdminViewModel->getVendors();
        $i = 0;
        foreach ($vendors as $value) {
            $vendor[$i]['id'] = $value['user_id'];
            $vendor[$i]['name'] = $value['first_name'];
            $i++;
        }
        $payment_details['select_vendors'] = $this->generate_options($vendor);
        $this->load->view('admin/payments', $payment_details);
    }

    public function getAmountPayable()
    {
        $vendor_id = $this->input->post('vendor_id');
        echo $this->AdminViewModel->getAmountPayable($vendor_id);
    }

    public function promo($promo_id = 0)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] == 1 || $user['user_type_id'] == 4) {

        } else {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        if ($this->input->post('submit') != false) {

            $this->form_validation->set_rules('promocode', 'PromoCode', 'required');
            $this->form_validation->set_rules('discount_criteria', 'Discount Criteria', '');
            if ($discount_criteria = $this->input->post('discount_criteria') == 'percentage') {
                $this->form_validation->set_rules('discount_per', 'Discount Percentage', 'required|callback_max_value[100]');
            } else {
                $this->form_validation->set_rules('discount_amount', 'Discount Amount', 'required|callback_max_value[350]');
            }
            $this->form_validation->set_rules('expiry_date', 'Expiry Date', 'required');
            $this->form_validation->set_rules('promo_id', 'Promo Id', '');
            $this->form_validation->set_rules('is_common', 'Available to all', '');
            $this->form_validation->set_rules('one_time', 'One time', '');
            $is_common = ($this->input->post('is_common') == 1) ? $this->input->post('is_common') : 0;
            $one_time = ($this->input->post('is_common') == 1) ? ($this->input->post('one_time') == 1) ? $this->input->post('one_time') : 0 : 0;
            $data = array();
            if ($this->form_validation->run() != false) {
                $promodata = array(
                    'promocode' => $this->input->post('promocode'),
                    'expiry_date' => date('Y-m-d H:i:s', strtotime($this->input->post('expiry_date'))),
                    'is_active' => 1,
                    'is_common' => $is_common,
                    'one_time' => $one_time,
                );
                if ($discount_criteria == 'percentage') {
                    $promodata['discount_per'] = $this->input->post('discount_per');
                } else {
                    $promodata['discount_amount'] = $this->input->post('discount_amount');
                }
                if ($this->input->post('promo_id') != '') {
                    $promo_id = $this->input->post('promo_id');
                } else {
                    $promo_id = 0;
                }
                $data['saved'] = $this->AdminViewModel->savepromo($promodata, $promo_id);

                redirect('admin/promocode/1');
            } else {
                if ($promo_id != 0) {
                    $data['promo'] = $this->AdminViewModel->getPromoCodeDetails($promo_id);
                }
                $this->form_validation->set_error_delimiters('<div class="err">', '</div>');
                $data['user_type_id'] = $user['user_type_id'];
                $this->load->view('admin/promo_form', $data);
            }
        } else {
            $data = array();
            if ($promo_id != 0) {
                $data['promo'] = $this->AdminViewModel->getPromoCodeDetails($promo_id);
                if ($data['promo']['discount_amount'] == '0') {
                    $data['promo']['discount_criteria'] = 'percentage';
                } else {
                    $data['promo']['discount_criteria'] = 'amount';
                }
            }
            $data['user_type_id'] = $user['user_type_id'];
            $this->load->view('admin/promo_form', $data);
        }
    }

    public function assign()
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] == 1 || $user['user_type_id'] == 4) {

        } else {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $userids = $this->input->post('userid');
        $promo_id = $this->input->post('promo_id');
        $result = $this->AdminViewModel->assignPromo($promo_id, $userids);
        redirect('admin/assignusers/' . $promo_id, 'refresh');
    }

    public function assignusers($promo_id)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] == 1 || $user['user_type_id'] == 4) {

        } else {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        if ($this->input->post('submit') !== false) {
            $userdata = array(
                'email' => $this->input->post('email'),
                'add_date' => date("Y-m-d H:i:s"),
                'is_active' => 1,
            );
            $userid[] = $this->AdminViewModel->insertPromoUser($userdata);
            $inserted = $this->AdminViewModel->assignPromo($promo_id, $userid);
        }
        $data = array();
        if ($promo_id != 0) {
            $data['promo'] = $this->AdminViewModel->getPromoCodeDetails($promo_id);
            $data['promousers'] = $this->AdminViewModel->getPromoUsers($promo_id);
        }
        $data['user_type_id'] = $user['user_type_id'];
        $this->load->view('admin/promousers', $data);
    }

    public function promocode($page = '', $saved = '')
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] == 1 || $user['user_type_id'] == 4) {

        } else {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $config['per_page'] = 15;
        $config['page'] = $page;
        $data['page'] = $config['page'];

        $filters = $this->input->post();
        if ($filters !== false) {
            $this->form_validation->set_rules('promocode', 'promocode', '');
            $this->form_validation->run();
            $data['promo'] = $this->AdminViewModel->getPromoCodes($config, $filters['promocode']);
        } else {
            $data['promo'] = $this->AdminViewModel->getPromoCodes($config);
        }

        $data['saved'] = false;
        if ($saved == 1) {
            $data['saved'] = true;
        }
        $config['base_url'] = base_url() . '/index.php/admin/promocode/';
        $config['anchor_class'] = '';
        $config['total_rows'] = $data['promo']['cnt'];
        $data['total_rows'] = $config['total_rows'];
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['user_type_id'] = $user['user_type_id'];
        $this->load->view('admin/promocode_view', $data);
    }

    public function vendor()
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 3) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $mode = $this->input->post('dropdown_status');
        $partner_order_ids = $this->PartnerModel->get_partner_orders();
        $spring_order_ids = $this->AdminViewModel->get_orders_by_promocode('A&S SPRG 14');
        $spring_order_ids = array_diff($partner_order_ids, $this->AdminViewModel->get_kickstarter_orders());
        $data['orders'] = $this->AdminViewModel->getOrderByStatusForVendors($mode, $user['user_id'], array(), $spring_order_ids);
        $this->load->view('admin/vendor_index', $data);
    }

    public function b2b()
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 3) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $mode = $this->input->post('dropdown_status');
        $partner_order_ids = $this->PartnerModel->get_partner_orders();
        $data['orders'] = $this->AdminViewModel->getOrderByStatusForVendors($mode, $user['user_id'], $partner_order_ids);
        $this->load->view('admin/vendor_index', $data);
    }

    public function spring_vendor_view()
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 3) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $mode = $this->input->post('dropdown_status');
        $spring_order_ids = $this->AdminViewModel->get_orders_by_promocode('A&S SPRG 14');
        $data['orders'] = $this->AdminViewModel->getOrderByStatusForVendors($mode, $user['user_id'], $spring_order_ids);
        $this->load->view('admin/spring_vendor_index', $data);
    }

    public function kickstarter_vendor_view()
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 3) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $mode = $this->input->post('dropdown_status');
        $kickstarter_order_ids = $this->AdminViewModel->get_kickstarter_orders();
        $data['orders'] = $this->AdminViewModel->getOrderByStatusForVendors($mode, $user['user_id'], $kickstarter_order_ids);
        $this->load->view('admin/spring_vendor_index', $data);
    }

    public function intern($page = 0)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 4) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $config['per_page'] = 10;
        $config['page'] = $page;

        $styles = [
            ['id' => 'Oxford', 'name' => 'Oxford'],
            ['id' => 'Derby', 'name' => 'Derby'],
            ['id' => 'Monk', 'name' => 'Monk'],
            ['id' => 'Loafer', 'name' => 'Loafer'],
        ];

        $lasts = [
            ['id' => 'The Townsend', 'name' => 'The Townsend'],
            ['id' => 'The Archibald', 'name' => 'The Archibald'],
            ['id' => 'The Wallace', 'name' => 'The Wallace'],
            ['id' => 'The Harvey', 'name' => 'The Harvey'],
            ['id' => 'The Eldridge', 'name' => 'The Eldridge'],
        ];

        $order_status = [
            ['id' => '', 'name' => 'All'],
            ['id' => 'Order acknowledged', 'name' => 'Order Acknowledged'],
            ['id' => 'Manufacturing in progress', 'name' => 'Manufacturing in progress'],
            ['id' => 'order shipped', 'name' => 'Order Shipped'],
            ['id' => 'Refund', 'name' => 'Refund'],
            ['id' => 'testing', 'name' => 'Testing'],
        ];

        $filters = $this->input->post();

        $kickstarter_order_ids = $this->AdminViewModel->get_kickstarter_orders();

        if ($filters !== false) {
            $order_ids = array();
            $this->form_validation->set_rules('style', 'style', '');
            $this->form_validation->set_rules('last', 'last', '');
            $this->form_validation->set_rules('invoice_number', 'invoice_number', '');
            $this->form_validation->set_rules('order_number', 'order_number', '');
            $this->form_validation->set_rules('customer_name', 'customer_name', '');
            $this->form_validation->set_rules('order_status', 'order_status', '');
            $this->form_validation->run();

            if ($filters['style'] !== '' || $filters['last'] !== '') {
                $order_ids = $this->AdminViewModel->getOrdersByStyleNameLastName($filters['style'], $filters['last']);
            }
            if ($filters['customer_name'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByCustomerName($filters['customer_name']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrdersByCustomerName($filters['customer_name']);
                }
            }
            if ($filters['order_number'] !== '' || $filters['invoice_number'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrders($filters['order_number'], $filters['invoice_number']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrders($filters['order_number'], $filters['invoice_number']);
                }
            }
            if ($filters['order_status'] !== '') {
                if (sizeof($order_ids)) {
                    $order_ids = array_intersect($order_ids, $this->AdminViewModel->getOrdersByStatus($filters['order_status']));
                } else {
                    $order_ids = $this->AdminViewModel->getOrdersByStatus($filters['order_status']);
                }
            }
            $result = $this->AdminViewModel->getProcessedOrders($config, $order_ids, $kickstarter_order_ids);
        } else {
            $result = $this->AdminViewModel->getProcessedOrders($config, array(), $kickstarter_order_ids);
        }
        $data['style'] = $this->generate_options($styles, 0, 0, set_value('style', ''));
        $data['last'] = $this->generate_options($lasts, 0, 0, set_value('last', ''));
        $data['order_status'] = $this->generate_options($order_status, 0, 0, set_value('order_status', ''));
        $data['orders'] = $result;
        $data['orders']['status'] = set_value('order_status', '');
        $config['base_url'] = base_url() . '/index.php/admin/intern/';
        $config['anchor_class'] = 'class="page" ';
        $config['total_rows'] = $result['cnt'];
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/intern_index', $data);
    }

    public function getProcessedOrders($config)
    {
        $data['orders'] = $this->AdminViewModel->getProcessedOrders($config);
        $data['orders']['status'] = '';
        return $data;
    }

    public function getOrderByStatus()
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 1) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $mode = $this->input->post('dropdown_status');
        $data['orders'] = $this->AdminViewModel->getOrderByStatus($mode);
        $this->load->view('admin/admin_index', $data);
    }

    public function detail_view($orderId)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }

        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        if ($data['order_summary'][0]['store_credit_trans_id'] !== null && $data['order_summary'][0]['card_amount']) {
            $data['order_summary'][0]['giftcard'] = true;
        } else {
            $data['order_summary'][0]['giftcard'] = false;
        }
        for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
            $data['order']['items'][$j]['shoedesign'] = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);
            $data['order']['items'][$j]['can_show_details'] = 1;
            if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0" && $data['order']['items'][$j]['last_id'] != '6') {
                $data['order']['items'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
            }
            if ($data['order']['items'][$j]['last_id'] == '6') {
                $this->load->model('CommonModel');
                $shoeDetails = $this->CommonModel->get_ready_wear_item($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['shop_images'] = $this->OrderModel->get_readytowear_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['last_name'] = 'The Ready To Wear';
                $data['order']['items'][$j]['style_name'] = $shoeDetails['style_name'];
                $data['order']['items'][$j]['shoe_name'] = $shoeDetails['shoe_name'];
                $data['order']['items'][$j]['color'] = $shoeDetails['color'];
                $data['order']['items'][$j]['can_show_details'] = 0;
                $data['order']['items'][$j]['is_readywear'] = 1;
            }
            $data['is_remake_request_sent'][$j] = $this->OrderModel->is_remake_request_sent($orderId, $data['order']['items'][$j]['design_id']);
            $data['remake_instruction'] = $this->OrderModel->get_remake_instruction($orderId, $data['order']['items'][$j]['design_id']);
        }

        $data['vendors'] = $this->AdminViewModel->getVendors();
        $data['status'] = $this->AdminViewModel->getStatus($orderId);
        $data['next_order'] = $this->OrderModel->next_order($orderId);
        $data['prev_order'] = $this->OrderModel->prev_order($orderId);
        $this->load->view('admin/order_details_view', $data);
    }

    public function intern_detail_view($orderId)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }

        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
            $data['order']['items'][$j]['shoedesign'] = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);
            $data['order']['items'][$j]['can_show_details'] = 1;
            if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0") {
                $data['order']['items'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
            }
            $data['is_remake_request_sent'][$j] = $this->OrderModel->is_remake_request_sent($orderId, $data['order']['items'][$j]['design_id']);
            $data['remake_instruction'] = $this->OrderModel->get_remake_instruction($orderId, $data['order']['items'][$j]['design_id']);
        }

        $data['vendors'] = $this->AdminViewModel->getVendors();
        $data['status'] = $this->AdminViewModel->getStatus($orderId);
        $this->load->view('admin/intern_order_details_view', $data);
    }

    public function second_stepdetails($orderId, $vendorId)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        $this->load->model('OrderModel');
        $this->load->model('ShoeModel');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);

        for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
            $shoedesign = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);
            $shoemodel = json_decode($shoedesign['shoemodel'], true);
            $data['order']['items'][$j]['measurement'] = $shoemodel['measurement'];
            $data['order']['items'][$j]['shoedesign'] = $shoedesign;

            $base = explode('M', $data['order']['items'][$j]['q_base']);
            $base = $base[0];

            $data['order']['items'][$j]['img1'] = '';
            $data['order']['items'][$j]['img2'] = '';

            if ($base != '' && $base != null) {
                $data['order']['items'][$j]['img1'] = base_url() . 'files/images/L4/Oxford/properties/All/' . $base . '_1.jpg';
                $data['order']['items'][$j]['img2'] = base_url() . 'files/images/L4/Oxford/properties/All/' . $base . '_2.jpg';
            }

            $data['order']['items'][$j]['can_show_details'] = 1;
            if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0" && $data['order']['items'][$j]['last_id'] != '6') {
                $data['order']['items'][$j]['shop_images'] = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['can_show_details'] = (int) $this->OrderModel->can_show_details($data['order']['items'][$j]['item_id']);
            }
            if ($data['order']['items'][$j]['last_id'] == '6') {
                $this->load->model('CommonModel');
                $shoeDetails = $this->CommonModel->get_ready_wear_item($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['shop_images'] = $this->OrderModel->get_readytowear_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['last_name'] = 'The Ready To Wear';
                $data['order']['items'][$j]['style_name'] = $shoeDetails['style_name'];
                $data['order']['items'][$j]['shoe_name'] = $shoeDetails['shoe_name'];
                $data['order']['items'][$j]['color'] = $shoeDetails['color'];
                $data['order']['items'][$j]['can_show_details'] = 0;
                $data['order']['items'][$j]['is_readywear'] = 1;
            }
            $data['remake_instruction'] = $this->OrderModel->get_remake_instruction($orderId, $data['order']['items'][$j]['design_id']);
        }

        $s = $data['order_summary'];
        foreach ($s as $key => $value) {
            $order[$key] = $value;
        }
        $data['user_id'] = $order[$key]['user_id'];
        $data['order_id'] = $order[$key]['order_id'];
        $data['partner_order_id'] = $this->AdminViewModel->is_partner_order($data['order_id']);
        if (isset($data['partner_order_id'])) {
            $data['partner_id'] = $this->AdminViewModel->get_partner_id($data['user_id']);
            $data['partner_logo'] = $this->AdminViewModel->get_partner_logo($data['partner_id']);
            if ($data['partner_id'] == 19) {
                $data['partner_logo'] = 'files/user_profiles/logo/CONNECTICUT_long.png';
            }
        }

        $data['vendors'] = $this->AdminViewModel->getVendorDet($vendorId);
        $data['status'] = $this->AdminViewModel->getStatus($orderId);
        $data['payment_details'] = $this->AdminViewModel->getPaymentDetail($orderId, $vendorId);
        $this->load->view('admin/order_details_vendors_view', $data);
    }

    public function vendorAssigning($orderId)
    {
        $vendorId = $this->input->post('dropdown');
        $invoiceNo = $this->input->post('hid_invoice');
        if ($this->AdminViewModel->assignToVendor($orderId, $vendorId, $invoiceNo)) {
            $this->sendOrdermailtoManufacturer($orderId, $vendorId);
            redirect('admin/admin_view', 'refresh');
        }
    }

    public function refund()
    {
        $orderId = $this->input->post('orderid');
        $invoiceNo = $this->input->post('invoiceno');
        if ($this->AdminViewModel->refund($orderId, $invoiceNo)) {
            echo 1;
        }
    }

    private function sendOrderSuccessMailToVendor($orderId)
    {
        $to = 'Vendor email';

        $this->load->library('email');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
            if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0") {
                $images = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['shop_image_file'] = $images[0];
            }
        }
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        $this->email->set_newline("\r\n");
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to($to);
        $this->email->bcc($this->config->item('contact_email'));
        $this->email->subject('Awl & Sundry Order ' . $orderId);
        $msg = $this->load->view('orderConfirmEmailVendor', $data);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    public function vendor_payment($order_id)
    {
        $vendor_id = $this->input->post('hid_vendorId');
        $this->form_validation->set_rules('comments', 'comments', '');
        $this->form_validation->set_rules('hid_invoice', 'hid_invoice', '');
        $design_id = $this->input->post('hid_designId');
        $data = array(
            'invoice_no' => $this->input->post('hid_invoice'),
            'date' => date('Y-m-d H:i:s'),
            'order_id' => $order_id,
            'design_id' => $design_id,
            'vendor_id' => $vendor_id,
            'comments' => $this->input->post('comments'),
        );
        $payment_id = $this->input->post('id');
        if ($this->form_validation->run() == false) {
            $this->load->model('OrderModel');
            $this->load->model('ShoeModel');
            $orderId = $order_id;
            $vendorId = $vendor_id;
            $data = $this->OrderModel->getbillingNshippingDetails($orderId);
            $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);

            for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
                $data['order']['items'][$j]['shoedesign'] = $this->ShoeModel->get_shoe_design_details($data['order']['items'][$j]['design_id'], 0);
            }

            $data['vendors'] = $this->AdminViewModel->getVendorDet($vendorId);
            $data['status'] = $this->AdminViewModel->getStatus($orderId);
            $data['payment_details'] = $this->AdminViewModel->getPaymentDetail($orderId, $vendorId);
            $this->load->view('admin/order_details_vendors_view', $data);
        } else {
            if ($this->AdminViewModel->savePayment($data, $payment_id)) {
                redirect('admin/second_stepdetails/' . $order_id . '/' . $vendor_id, 'refresh');
            }
        }
    }

    public function save_payment()
    {
        $this->form_validation->set_rules('manufacturing_cost', 'Manufacturing cost', 'numeric');
        $this->form_validation->set_rules('shipping_cost', 'Shipping cost', 'numeric');
        $this->form_validation->set_rules('payment_id', 'payment_id', '');
        $this->form_validation->set_rules('comments', 'comments', '');
        $payment_id = $this->input->post('payment_id');
        $data = array(
            'manufacturing_cost' => $this->input->post('manufacturing_cost'),
            'shipping_cost' => $this->input->post('shipping_cost'),
            'comments' => $this->input->post('comments'),
        );
        if ($this->form_validation->run() == false) {
            $payment_details = $this->AdminViewModel->getPaymentDetails($payment_id);
            $data['payment_details'] = $payment_details[0];
            $this->load->view('admin/edit_payment_form', $data);
        } else {
            if ($this->AdminViewModel->savePayment($data, $payment_id)) {
                redirect('admin/payments', 'refresh');
            }
        }
    }

    public function edit_payment($payment_id)
    {
        $user = $this->session->userdata('user_details');
        if ($user == false) {
            redirect('login', 'refresh');
        }
        if ($user['user_type_id'] != 1) {
            $this->session->sess_destroy();
            redirect('login', 'refresh');
        }
        $payment_details = $this->AdminViewModel->getPaymentDetails($payment_id);
        $data['payment_details'] = $payment_details[0];
        $this->load->view('admin/edit_payment_form', $data);
    }

    public function submit_payment()
    {
        $payment_data = array(
            'vendor_id' => $this->input->post('vendor_id'),
            'amount' => $this->input->post('amount'),
            'payment_method' => $this->input->post('payment_method'),
            'reference_no' => $this->input->post('reference_no'),
            'payment_date' => $this->input->post('date'),
            'comments' => $this->input->post('comments'),
        );

        if ($this->AdminViewModel->submitPayment($payment_data)) {
            redirect('admin/payments', 'refresh');
        }
    }

    private function generate_options($array = array(), $from = 1, $to = 12, $selected = 0)
    {
        $options = '';
        if (sizeof($array) == 0) {
            if ($from >= 0 && $to >= 0) {
                for ($i = $from; $i <= $to; $i++) {
                    $text = (isset($selected) && $selected == $i) ? 'selected="selected" ' : '';
                    $options .= '<option ' . $text . ' >' . $i . '</option>';
                }
            }
        } else {
            foreach ($array as $key => $value) {
                $text = (isset($selected) && $selected === $value['id']) ? 'selected="selected" ' : '';
                $options .= '<option value = "' . $value['id'] . '" ' . $text . ' >' . $value['name'] . '</option>';
            }
        }
        return $options;
    }

    public function shipping($orderId)
    {
        $trackingNo = $this->input->post('trackNo');
        $carrier = $this->input->post('carrier');
        $shippedDate = $this->input->post('shipDate');
        $deliveryDate = $this->input->post('deliveryDate');
        $invoiceNo = $this->input->post('hid_invoice');
        $vendorId = $this->input->post('hid_vendorId');
        $designId = $this->input->post('hid_designId');
        if ($this->AdminViewModel->statusToShipped($orderId, $trackingNo, $carrier, $shippedDate, $deliveryDate, $invoiceNo, $vendorId)) {
            $shipping_data = array(
                'order_id' => $orderId,
                'carrier' => $carrier,
                'tracking_no' => $trackingNo,
                'shipped_date' => $shippedDate,
                'delivery_date' => $deliveryDate,
                'invoice_no' => $invoiceNo,
            );
            $this->shipping_notification($shipping_data, $trackingNo, $carrier);
            redirect('admin/vendor', 'refresh');
        }
    }

    private function shipping_notification($shipping_data, $trackingNo, $carrier)
    {
        $data = $shipping_data;
        $order_details = $this->OrderModel->getbillingNshippingDetails($shipping_data['order_id']);
        $data['shipping_address'] = $order_details['shipping'][0];
        $data['billing_address'] = $order_details['billing'][0];

        $this->load->library('email');

        if ($order_details['order']['items'][0]['kickstarter_no'] != 0) {
            $this->email->from($this->config->item('contact_email'), 'Awl & Sundry');
            $this->email->to($order_details['shipping'][0]['email']);
            $this->email->bcc($this->config->item('contact_email'));
            $this->email->subject('Your Awl & Sundry shoes are on their way | Order ' . $shipping_data['order_id']);
            $tracking_numbers = explode(',', $trackingNo);
            foreach ($tracking_numbers as $key => $tracking_number) {
                $data['link'][] = 'http://track.aftership.com/' . strtolower($carrier) . '/' . trim($tracking_number);
            }

            $msg = $this->load->view('emails/shipping_notification_kickstarter', $data, true);
        } else {
            $this->email->from($this->config->item('from_email'), 'Awl & Sundry');
            $this->email->to($order_details['shipping'][0]['email']);
            $this->email->bcc($this->config->item('contact_email'));
            $this->email->subject('Your Awl & Sundry shoes are on their way | Order ' . $shipping_data['order_id']);
            $msg = $this->load->view('emails/shipping_notification', $data, true);
        }

        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    public function pdf()
    {
        $this->load->helper('pdf_helper');
        $orderId = 75;
        $this->load->model('OrderModel');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        $submit['data'] = $data;
        $this->load->view('pdfreport', $submit);
    }

    public function allowpromocode()
    {
        $isSubmit = $this->input->post('submit');
        if ($isSubmit != null) {
            $validations = $this->validateaddPromocode();
            if ($validations['IsValid']) {
                $promodet = array(
                    'promocode' => $this->input->post('promocode'),
                    'discount_per' => $this->input->post('discount'),
                    'expiry_date' => date("Y-m-d H:i:s", strtotime($this->input->post('expDate'))),
                );
                $this->AdminViewModel->insertPromoCode($promodet);
            }
        }
        $this->load->view('admin/allowpromocode');
    }

    private function validateaddPromocode()
    {
        $isvalid = array("IsValid" => true, 'errors' => array());
        if (trim($this->input->post('promocode')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['promocode'] = "Error";
        }
        if (trim($this->input->post('discount')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['discount'] = "Error";
        }
        if (trim($this->input->post('expDate')) == "") {
            $isvalid['IsValid'] = false;
            $isvalid['errors']['expDate'] = "Error";
        }
        return $isvalid;
    }

    public function admin_changepass()
    {
        if ($this->input->post('submit') != null) {
            $type = "password";
            $newval = $this->input->post('confirmpassword');
            $uId = $this->input->post('hid_user_id');

            $user = $this->session->userdata('user_details');
            if (isset($user['id'])) {
                $uId = $user['id'];
            }

            $this->changeSettings($type, $newval, $uId);
            $data['message'] = 'Password has been changed successfully.';
            $this->load->view('admin/changePassword', $data);
        } else {
            $data['message'] = '';
            $this->load->view('admin/changePassword', $data);
        }
    }

    private function changeSettings($type, $newval, $uId)
    {
        echo ($this->UserModel->changeSettings($type, urldecode($newval), $uId)) ? true : false;
    }

    public function sendOrdermailtoManufacturer($orderId, $vendorId)
    {
        $vendor = $this->AdminViewModel->getVendorDet($vendorId);
        $this->load->library('email');
        $to = $vendor[0]['email'];
        $this->load->model('OrderModel');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        for ($j = 0; $j < sizeof($data['order']['items']); $j++) {
            if ($data['order']['items'][$j]['design_id'] !== $data['order']['items'][$j]['item_id'] && $data['order']['items'][$j]['item_id'] !== "0" && $data['order']['items'][$j]['last_id'] != '6') {
                $images = $this->OrderModel->get_shop_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['shop_image_file'] = $images[0];
            }
            if ($data['order']['items'][$j]['last_id'] == '6') {
                $images = $this->OrderModel->get_readytowear_images($data['order']['items'][$j]['item_id']);
                $data['order']['items'][$j]['shop_image_file'] = $images[0];
            }
        }
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        $data['vendor'] = $vendor;
        $this->email->set_newline("\r\n");
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $this->email->to('info@robertshoes.it');
        $this->email->cc($this->config->item('contact_email'));
        $this->email->subject('Awl & Sundry Order ' . $orderId);
        $msg = $this->load->view('orderassignedTovendorEmail', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    public function importCSVToPromoUsers()
    {
        $filename = "awl_users.csv";
        $fh = fopen($filename, "r") or die("Can't Open File");
        $i = 0;
        while ($line = fgets($fh)) {
            $row = explode(",", $line);
            $usedata = array(
                'email' => $row[0],
                'ip_address' => $row[1],
                'location' => $row[2],
                'add_date' => date('Y-m-d H:i:s', strtotime($row[3])),
                'region' => $row[4],
                'is_active' => 1,
            );
            print_r($usedata);
        }
    }

    public function return_view()
    {

    }

    public function inspirationReview($page = 0, $prev = null)
    {
        $status = $this->input->post();
        if ($status == null) {
            $status['status'] = $prev;
        }
        if ($status['status'] == null) {
            $status['status'] = 'not';
        }

        $config['per_page'] = 10;
        $config['page'] = $page;
        $data['newShoes'] = $this->AdminViewModel->inspirationReview($config, $status['status']);
        $this->data['newShoe'] = $data['newShoes']['result'];
        $this->data['status'] = $status['status'];
        $this->data['page'] = $page;
        $this->data['content'] = 'admin/inspirationReview_view';
        $config['base_url'] = base_url() . '/index.php/admin/inspirationReview_view/';
        $config['anchor_class'] = 'page';
        $config['total_rows'] = $data['newShoes']['no_rows'];
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->load->view('admin/template', $this->data);
    }

    public function admin_inspiration_shoes($data)
    {
        $this->load->view('admin/admin_inspiration_shoes_view', $data);
    }

    public function feedback()
    {
        $this->data['content'] = 'admin/feedback';
        $content_data['feedbacks'] = $this->AdminViewModel->feedback();
        $content_data['feedback_score'] = $this->AdminViewModel->feedback_score();

        $this->data['content_data'] = $content_data;

        $this->load->view('admin/template', $this->data);
    }

    public function inspired($designId)
    {
        $modify = $this->input->post();
        $this->AdminViewModel->inspired($designId, $modify);
        redirect('admin/inspirationReview/' . $modify['page_no'] . '/' . $modify['status'], 'refresh');
    }

    public function users()
    {
        $data['users'] = $this->db->get('users')->result_array();
        $data['roles'] = ['1' => 'Admin', '2' => 'Webuser', '3' => 'Vendor', '4' => 'Intern'];
        $data['additional_js'] = array('admin/adduser');
        $this->load->view('admin/users', $data);
    }

    public function edituser($id)
    {
        $data['user'] = $this->db->get_where('users', ['user_id' => $id])->result_array();
        $data['user'] = (isset($data['user'][0])) ? $data['user'][0] : [];
        $data['roles'] = ['1' => 'Admin', '2' => 'Webuser', '3' => 'Vendor', '4' => 'Intern'];
        $data['additional_js'] = array('admin/adduser');
        $this->load->view('admin/adduser', $data);
    }

    public function adduser()
    {
        $data = array();
        $data['additional_js'] = array('admin/adduser');
        $this->load->view('admin/adduser', $data);
    }

    public function delUser()
    {
        $input = $this->input->post();
        if (isset($input['user_id'])) {
            $this->db->where('user_id', $input['user_id'])->delete('users');

            $error = array('status' => "success");
            echo json_encode($error);
        }
    }

    public function saveUser()
    {
        $input = $this->input->post();
        if (isset($input['user_id'])) {
            $chk = $this->db->get_where('users', ['email' => $input['email'], 'user_id !=' => $input['user_id']])->result_array();
            if (count($chk) > 0) {
                $error = array('status' => "Email id already exists");
                echo json_encode($error);
            } else {
                $this->db->set([
                    'first_name' => $input['fname'],
                    'last_name' => $input['lname'],
                    'email' => $input['email'],
                    'user_type_id' => $input['role'],
                ])
                    ->where('user_id', $input['user_id'])
                    ->update('users');

                $input['status'] = 'success';
                echo json_encode($input);
            }
        } else {
            if (!$this->UserModel->uniqueemail($input['email'])) {
                $error = array('status' => "Email id already exists");
                echo json_encode($error);
            } else {
                $token = random_string('alnum', 20);
                $password = $input['password'];
                $time = strtotime(date('Y-m-d H:i:s'));
                $user = array(
                    'first_name' => $input['fname'],
                    'last_name' => $input['lname'],
                    'email' => $input['email'],
                    'password' => md5($password),
                    'username' => $input['email'],
                    'date_created' => $time,
                    'token' => $token,
                    'user_type_id' => $input['role']);
                $userdet = $this->UserModel->admin_register_user($user);
                echo json_encode($userdet);
            }
        }
    }

    public function appointments()
    {
        $data = array();
        $dbAppointments = $this->AppointmentModel->getFutureAppointmentObject();
        $this->data['appointments'] = $dbAppointments;
        $this->data['additional_js'] = array('admin/appointments');
        $this->load->view('admin/appointments', $this->data);
    }

}
