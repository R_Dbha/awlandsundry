<?php

class FacebookLogin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        parse_str($_SERVER['QUERY_STRING'], $_REQUEST);

        // Load facebook library
        $this->load->library('facebook');

        // $this->load->library('Facebook', array(
        //     "appId" => "250777875062389",
        //     "secret" => "e1247e1fc58372689c141d2ecb6a9b0a",
        //     "user_birthday" => array(
        //         'user_birthday',
        //     ),
        // ));

        // $this->user = $this->facebook->getUser();
    }

    public function index()
    {
        if ($this->user) {

            try {

                $user_profile = $this->facebook->api('/me');
                $this->load->model('UserModel');
                $fbuser = $this->UserModel->loginwithfacebook($user_profile['id']);
                if ($fbuser == null) {
                    $userData = $this->UserModel->register_facebookuser($user_profile);
                    $sendyArr = ['name' => trim($userData['first_name'] . ' ' . $userData['last_name']), 'email' => $userData['email']];
                    addCustomerToSendy($sendyArr);
                    addCustomerToMailChimp($userData);

                    $this->mailchimpintegration($user_profile['email']);
                    $fbuser = $this->UserModel->loginwithfacebook($user_profile['id']);
                }
                $this->session->set_userdata('user_details', $fbuser);
                $redirect = $this->session->userdata('redirect');
                if ($redirect != null) {
                    $this->session->unset_userdata('redirect');
                }
                if ($redirect != null) {
                    echo '<script type="text/javascript">opener.location.href="' . $redirect . '"; window.close();</script>';
                } else {
                    echo '<script type="text/javascript">opener.location.href=opener.location.href; window.close();</script>';
                }
            } catch (FacebookApiException $e) {

                print_r($e);

                $user = null;
            }
        } else {
            redirect($this->facebook->getLoginUrl(array(
                'display' => 'popup',
                'scope' => 'email,user_birthday',
            )));
        }
    }

    public function signup()
    {
        if ($this->facebook->is_authenticated()) {
            try {
                $user_profile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,birthday,email,gender');

                if (isset($user_profile['id'])) {
                    $this->load->model('UserModel');
                    $fbuser = $this->UserModel->loginwithfacebook($user_profile['id'], $user_profile['email']);
                    if ($fbuser == null) {
                        $userData = $this->UserModel->register_facebookuser($user_profile);
                        $sendyArr = ['name' => trim($userData['first_name'] . ' ' . $userData['last_name']), 'email' => $userData['email']];
                        addCustomerToSendy($sendyArr);
                        addCustomerToMailChimp($userData);

                        $this->mailchimpintegration($user_profile['email']);
                        $fbuser = $this->UserModel->loginwithfacebook($user_profile['id']);
                    }
                    $this->session->set_userdata('user_details', $fbuser);
                    $redirect = $this->session->userdata('redirect');
                    if ($redirect != null) {
                        $this->session->unset_userdata('redirect');
                    }

                    if ($redirect != null) {
                        echo '<script type="text/javascript">window.location.href="' . $redirect . '"; window.close();</script>';
                    } else {
                        echo '<script type="text/javascript">window.location.href="' . base_url() . '"; window.close();</script>';
                    }
                } else {
                    echo '<script type="text/javascript">window.location.href="' . base_url('login') . '"; window.close();</script>';
                }
            } catch (FacebookApiException $e) {

                print_r($e);

                $user = null;
            }
        } else {
            redirect($this->facebook->login_url());
        }
    }

    public function fb_login()
    {
        $user_profile = $this->input->post();
        $this->load->model('UserModel');
        $fbuser = $this->UserModel->loginwithfacebook($user_profile['id']);
        if ($fbuser == null) {
            $userData = $this->UserModel->register_facebookuser($user_profile);
            $this->mailchimpintegration($user_profile['email']);
            $sendyArr = ['name' => trim($userData['first_name'] . ' ' . $userData['last_name']), 'email' => $userData['email']];
            addCustomerToSendy($sendyArr);
            addCustomerToMailChimp($userData);

            $fbuser = $this->UserModel->loginwithfacebook($user_profile['id']);
        }
        $this->session->set_userdata('user_details', $fbuser);
        $data['status'] = true;
        $data['msg'] = "Logged in successfully";
        $data['html'] = $this->load->view('login_part', array(), true);
        echo json_encode($data);
    }

    public function logoutfacebook()
    {
        $this->logout();
    }

    public function logout()
    {
        $this->facebook->destroy_session();
        $this->session->sess_destroy();
        redirect('', 'refresh');
    }

    public function mailchimpintegration($email)
    {
        $config = array(
            'apikey' => '39c50254155d9583cec13a336c110060-us8',
            'secure' => false,
        );
        $this->load->library('MCAPI', $config, 'mail_chimp');
        $list_id = '6b8ea607f9';
        if ($this->mail_chimp->listSubscribe($list_id, $email)) {
            return true;
        }
        return false;
    }

    public function mailerlite($data)
    {
        $this->load->config('api');
        $mailerlite_api = $this->config->item('mailerlite_api_key');
        $this->load->library('ML_Subscribers', array($mailerlite_api));

        $ML_Subscribers = new ML_Subscribers($mailerlite_api);

        $subscriber = array(
            'email' => $data['email'],
            'name' => isset($data['name']) ? $data['name'] : '',
            'fields' => array(
                array(
                    'name' => ' ',
                    'value' => " ",
                ),
                array(
                    'name' => ' ',
                    'value' => " ",
                ),
            ),
        );
        $result = $ML_Subscribers->setId('2435493')->add($subscriber);
    }

}
