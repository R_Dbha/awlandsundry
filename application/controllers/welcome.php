<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

ini_set('display_errors', '1');

class Welcome extends CI_Controller
{

    public function index()
    {
        //$this->load->view('welcome_message');
    }

    public function __Construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Stripe_trans');
        $this->load->model('Stripe_cust');

        $this->config->load('paypal');

        $config = array(
            'Sandbox' => $this->config->item('Sandbox'), // Sandbox / testing mode option.
            'APIUsername' => $this->config->item('APIUsername'), // PayPal API username of the API caller
            'APIPassword' => $this->config->item('APIPassword'), // PayPal API password of the API caller
            'APISignature' => $this->config->item('APISignature'), // PayPal API signature of the API caller
            'APISubject' => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
            'APIVersion' => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
        );

        if ($config['Sandbox']) {
            // error_reporting(E_ALL);
        }

        $this->load->library('paypal/Paypal_pro', $config);
    }

    public function Do_direct_payment()
    {
        $DPFields = array(
            'paymentaction' => 'Sale', // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
            'ipaddress' => $this->get_client_ip(), // Required.  IP address of the payer's browser.
            'returnfmfdetails' => '1', // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
        );

        $CCDetails = array(
            'creditcardtype' => 'MasterCard', // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
            'acct' => '345234523452345', // Required.  Credit card number.  No spaces or punctuation.
            'expdate' => '042014', // Required.  Credit card expiration date.  Format is MMYYYY
            'cvv2' => '2334', // Requirements determined by your PayPal account settings.  Security digits for credit card.
            'startdate' => '022013', // Month and year that Maestro or Solo card was issued.  MMYYYY
            'issuenumber' => '34', // Issue number of Maestro or Solo card.  Two numeric digits max.
        );

        $PayerInfo = array(
            'email' => '', // Email address of payer.
            'payerid' => '', // Unique PayPal customer ID for payer.
            'payerstatus' => '', // Status of payer.  Values are verified or unverified
            'business' => '', // Payer's business name.
        );

        $PayerName = array(
            'salutation' => 'Mr', // Payer's salutation.  20 char max.
            'firstname' => 'Aniyan', // Payer's first name.  25 char max.
            'middlename' => '', // Payer's middle name.  25 char max.
            'lastname' => 'P N', // Payer's last name.  25 char max.
            'suffix' => '', // Payer's suffix.  12 char max.
        );

        $BillingAddress = array(
            'street' => 'Street 2', // Required.  First street address.
            'street2' => '', // Second street address.
            'city' => 'Thrissur', // Required.  Name of City.
            'state' => 'Kerala', // Required. Name of State or Province.
            'countrycode' => 'IN', // Required.  Country code.
            'zip' => '679535', // Required.  Postal code of payer.
            'phonenum' => '', // Phone Number of payer.  20 char max.
        );

        $ShippingAddress = array(
            'shiptoname' => '', // Required if shipping is included.  Person's name associated with this address.  32 char max.
            'shiptostreet' => '', // Required if shipping is included.  First street address.  100 char max.
            'shiptostreet2' => '', // Second street address.  100 char max.
            'shiptocity' => '', // Required if shipping is included.  Name of city.  40 char max.
            'shiptostate' => '', // Required if shipping is included.  Name of state or province.  40 char max.
            'shiptozip' => '', // Required if shipping is included.  Postal code of shipping address.  20 char max.
            'shiptocountry' => '', // Required if shipping is included.  Country code of shipping address.  2 char max.
            'shiptophonenum' => '', // Phone number for shipping address.  20 char max.
        );

        $PaymentDetails = array(
            'amt' => .25, // Required.  Total amount of order, including shipping, handling, and tax.
            'currencycode' => 'USD', // Required.  Three-letter currency code.  Default is USD.
            'itemamt' => .25, // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
            'shippingamt' => 0, // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
            'insuranceamt' => 0, // Total shipping insurance costs for this order.
            'shipdiscamt' => 0, // Shipping discount for the order, specified as a negative number.
            'handlingamt' => 0, // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
            'taxamt' => 0, // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
            'desc' => 0, // Description of the order the customer is purchasing.  127 char max.
            'custom' => 0, // Free-form field for your own use.  256 char max.
            'invnum' => '546546', // Your own invoice or tracking number
            'buttonsource' => '5465', // An ID code for use by 3rd party apps to identify transactions.
            'notifyurl' => base_url() . 'index.php/customshoe/paymentsuccess/20', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
            'recurring' => '', // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
        );

        // For order items you populate a nested array with multiple $Item arrays.
        // Normally you'll be looping through cart items to populate the $Item array
        // Then push it into the $OrderItems array at the end of each loop for an entire
        // collection of all items in $OrderItems.

        $OrderItems = array();

        $Item = array(
            'l_name' => 'something', // Item Name.  127 char max.
            'l_desc' => '', // Item description.  127 char max.
            'l_amt' => '.25', // Cost of individual item.
            'l_number' => '10', // Item Number.  127 char max.
            'l_qty' => '1', // Item quantity.  Must be any positive integer.
            'l_taxamt' => '', // Item's sales tax amount.
            'l_ebayitemnumber' => '', // eBay auction number of item.
            'l_ebayitemauctiontxnid' => '', // eBay transaction ID of purchased item.
            'l_ebayitemorderid' => '', // eBay order ID for the item.
        );

        array_push($OrderItems, $Item);

        $Secure3D = array(
            'authstatus3d' => '',
            'mpivendor3ds' => '',
            'cavv' => '',
            'eci3ds' => '',
            'xid' => '',
        );

        $PayPalRequestData = array(
            'DPFields' => $DPFields,
            'CCDetails' => $CCDetails,
            'PayerInfo' => $PayerInfo,
            'PayerName' => $PayerName,
            'BillingAddress' => $BillingAddress,
            'ShippingAddress' => $ShippingAddress,
            'PaymentDetails' => $PaymentDetails,
            'OrderItems' => $OrderItems,
            'Secure3D' => $Secure3D,
        );

        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);

        if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            $errors = array('Errors' => $PayPalResult['ERRORS']);
            $this->load->view('paypal_error', $errors);
        } else {
            die(print_r($PayPalResult));
        }
    }
    public function sendOrderSuccessMailManually($orderId, $token)
    {
        if ($token !== 'zaqwsx') {
            exit();
        }
        $this->load->library('email');
        $this->load->model('OrderModel');
        $details = $this->OrderModel->get_giftcard_details_from_order_id($orderId);

        foreach ($details as $options) {
            $data = array();
            $data['recipient_name'] = $options['recipient_name'];
            $data['sender_name'] = $options['sender_name'];
            $data['card_number'] = $options['card_number'];
            $data['message'] = $options['message'];
            $data['giftcard_price'] = $options['item_amt'];
            $this->email->to($options['recipient_email']);
            $this->email->cc($options['sender_email']);
            $this->email->bcc($this->config->item('contact_email'));
            $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
            $this->email->subject('You\'ve been sent a gift from ' . ucwords($data['sender_name']));
            $msg = $this->load->view('giftcard/giftCardEmail', $data, true);
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            // $this->email->send();
            echo $msg;
        }
    }

    public function Get_balance()
    {
        $GBFields = array('returnallcurrencies' => '1');
        $PayPalRequestData = array('GBFields' => $GBFields);
        $PayPalResult = $this->paypal_pro->GetBalance($PayPalRequestData);

        if (!$this->paypal_pro->APICallSuccessful($PayPalResult['ACK'])) {
            $errors = array('Errors' => $PayPalResult['ERRORS']);
            $this->load->view('paypal_error', $errors);
        } else {
            $data = array('PayPalResult' => $PayPalResult);
            print_r($data);
        }
    }

    public function payByCard($orderid)
    {
        $user = $this->session->userdata('user_details');
        $ccsess_data = $this->session->userdata('ccdata');
        $token = $this->session->userdata('stripe_token');
        if ($ccsess_data['card_type'] != 'Paypal') {
            $this->load->model('OrderModel');
            $arr = $this->OrderModel->getbillingNshippingDetails($orderid);
            $paymentDet = $this->OrderModel->getOrderDet($orderid);
            $billing = $arr['billing'][0];
            $shipping = $arr['shipping'][0];
            $order = $arr['order']['items'];
            $netAmount = 0;
            if ($paymentDet[0]['discount_perc'] != 0) {
                $netAmount = $paymentDet[0]['gross_amt'] - (($paymentDet[0]['gross_amt'] * $paymentDet[0]['discount_perc']) / 100) + $paymentDet[0]['tax_amount'];
            } else {
                $netAmount = $paymentDet[0]['gross_amt'] + $paymentDet[0]['tax_amount'];
            }
            $netAmount = round($netAmount, 2);
            $DPFields = array(
                'paymentaction' => 'Sale', // How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                'ipaddress' => $this->get_client_ip(), // Required.  IP address of the payer's browser.
                'returnfmfdetails' => '1', // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
            );

            $CCDetails = array(
                'creditcardtype' => $ccsess_data['card_type'], // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                'acct' => $ccsess_data['card_no'], // Required.  Credit card number.  No spaces or punctuation.
                'expdate' => $ccsess_data['exp_date'], // Required.  Credit card expiration date.  Format is MMYYYY
                'cvv2' => $ccsess_data['card_cvv2'], // Requirements determined by your PayPal account settings.  Security digits for credit card.
                'startdate' => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
                'issuenumber' => '', // Issue number of Maestro or Solo card.  Two numeric digits max.
            );

            $PayerInfo = array(
                'email' => $user['email'], // Email address of payer.
                'payerid' => '', // Unique PayPal customer ID for payer.
                'payerstatus' => '', // Status of payer.  Values are verified or unverified
                'business' => 'Testers, LLC', // Payer's business name.
            );

            $PayerName = array(
                'salutation' => 'Mr.', // Payer's salutation.  20 char max.
                'firstname' => $user['first_name'], // Payer's first name.  25 char max.
                'middlename' => '', // Payer's middle name.  25 char max.
                'lastname' => $user['last_name'], // Payer's last name.  25 char max.
                'suffix' => '', // Payer's suffix.  12 char max.
            );

            $BillingAddress = array(
                'street' => $billing['address1'], // Required.  First street address.
                'street2' => $billing['address2'], // Second street address.
                'city' => $billing['city'], // Required.  Name of City.
                'state' => $billing['state'], // Required. Name of State or Province.
                'countrycode' => 'US', // Required.  Country code.
                'zip' => $billing['zipcode'], // Required.  Postal code of payer.
                'phonenum' => '555-555-5555', // Phone Number of payer.  20 char max.
            );

            $ShippingAddress = array(
                'shiptoname' => $shipping['firstname'], // Required if shipping is included.  Person's name associated with this address.  32 char max.
                'shiptostreet' => $shipping['address1'], // Required if shipping is included.  First street address.  100 char max.
                'shiptostreet2' => $shipping['address2'], // Second street address.  100 char max.
                'shiptocity' => $shipping['city'], // Required if shipping is included.  Name of city.  40 char max.
                'shiptostate' => $shipping['state'], // Required if shipping is included.  Name of state or province.  40 char max.
                'shiptozip' => $shipping['zipcode'], // Required if shipping is included.  Postal code of shipping address.  20 char max.
                'shiptocountry' => 'US', // Required if shipping is included.  Country code of shipping address.  2 char max.
                'shiptophonenum' => $shipping['telephone'], // Phone number for shipping address.  20 char max.
            );

            $PaymentDetails = array(
                'amt' => round($netAmount, 2), // Required.  Total amount of order, including shipping, handling, and tax.
                'currencycode' => $paymentDet[0]['currency'], // Required.  Three-letter currency code.  Default is USD.
                'itemamt' => round($netAmount, 2),
                'shippingamt' => '0.00', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                'shipdiscamt' => '', // Shipping discount for the order, specified as a negative number.
                'handlingamt' => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                'taxamt' => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
                'desc' => 'Web Order', // Description of the order the customer is purchasing.  127 char max.
                'custom' => '', // Free-form field for your own use.  256 char max.
                'invnum' => $paymentDet[0]['invoice_no'], // Your own invoice or tracking number
                'notifyurl' => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
            );

            $OrderItems = array();
            $Item = array(
                'l_name' => 'Test Widget 123', // Item Name.  127 char max.
                'l_desc' => 'The best test widget on the planet!', // Item description.  127 char max.
                'l_amt' => '95.00', // Cost of individual item.
                'l_number' => '123', // Item Number.  127 char max.
                'l_qty' => '1', // Item quantity.  Must be any positive integer.
                'l_taxamt' => '', // Item's sales tax amount.
                'l_ebayitemnumber' => '', // eBay auction number of item.
                'l_ebayitemauctiontxnid' => '', // eBay transaction ID of purchased item.
                'l_ebayitemorderid' => '', // eBay order ID for the item.
            );
            array_push($OrderItems, $Item);

            $Secure3D = array(
                'authstatus3d' => '',
                'mpivendor3ds' => '',
                'cavv' => '',
                'eci3ds' => '',
                'xid' => '',
            );

            $PayPalRequestData = array(
                'DPFields' => $DPFields,
                'CCDetails' => $CCDetails,
                'PayerInfo' => $PayerInfo,
                'PayerName' => $PayerName,
                'BillingAddress' => $BillingAddress,
                'ShippingAddress' => $ShippingAddress,
                'PaymentDetails' => $PaymentDetails,
                'Secure3D' => $Secure3D,
            );

            if ($paymentDet[0]['discount_perc'] != 100) {
                $stripe_result = $this->Stripe_trans->insert($token, 'AwlandSundry.com', round($netAmount * 100, 2));
                if ($stripe_result->paid == true) {

                    $this->load->model('OrderModel');
                    $this->load->model('ShoeModel');
                    if ($this->OrderModel->updateOrderStatus($orderid)) {
                        $cartdet = $this->session->userdata('cart_discount');
                        if ($cartdet != null) {
                            if ($cartdet['giftcard_id'] != 0) {
                                $this->load->model('giftcardmodel');
                                $this->giftcardmodel->insertGiftCardUsage($cartdet['giftcard_id'], $cartdet['giftcard_discount'], $orderid);
                            }
                            if ($cartdet['promo_assign_id'] != 0) {
                                $this->ShoeModel->updatepromoAssign($cartdet['promo_assign_id'], $orderid);
                            }
                            $this->session->unset_userdata('cart_discount');
                        }
                        $this->sendOrderSuccessmail($orderid);
                    }
                } else {
                    log_message('ERROR', 'Payment Error');
                    $error = array('error' => $stripe_result->message);
                    $this->load->view('common/payment_error', $error);
                }
            } else {
                $this->load->model('OrderModel');
                $this->load->model('ShoeModel');
                if ($this->OrderModel->updateOrderStatus($orderid)) {
                    $cartdet = $this->session->userdata('cart_discount');
                    if ($cartdet != null) {
                        if ($cartdet['giftcard_id'] != 0) {
                            $this->load->model('giftcardmodel');
                            $this->giftcardmodel->insertGiftCardUsage($cartdet['giftcard_id'], $cartdet['giftcard_discount'], $orderid);
                        }if ($cartdet['promo_assign_id'] != 0) {
                            $this->ShoeModel->updatepromoAssign($cartdet['promo_assign_id'], $orderid);
                        }
                        $this->session->unset_userdata('cart_discount');
                    }
                    $this->sendOrderSuccessmail($orderid);
                }
            }
        } else {
            redirect('customshoe/paypal/' . $orderid, 'refresh');
        }
    }

    public function test()
    {
        $array = array('a' => 'b', 'b' => 'c');
        ob_start();
        print_r($array);
        $content = ob_get_contents();
        ob_clean();
        ob_flush();

        log_message('ERROR', $content);
    }

    public function giftcardtest()
    {
        $this->load->library('email');
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');
        $data['recipient_name'] = 'Ajith';
        $data['sender_name'] = 'Ajith';
        $data['card_number'] = 'fgdgdgf';
        $data['message'] = 'Message';
        $data['giftcard_price'] = '100';
        $this->email->to('arun@kraftlabs.com');
        $this->email->cc('ajith@kraftlabs.com');
        $this->email->subject('You\'ve been sent a gift from Ajith');
        $msg = $this->load->view('giftcard/giftCardEmail', $data);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    private function sendOrderSuccessmail($orderId)
    {
        $this->load->library('cart');
        $user = $this->session->userdata('user_details');
        $to = $user['email'];
        $this->load->library('email');
        $this->load->model('OrderModel');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        $this->email->from('<' . $this->config->item('from_email') . '>', 'Awl & Sundry');

        $total = 0;
        $giftcard = 0;
        foreach ($this->cart->contents() as $items):
            $total += 1;
            if (strtolower($items['name']) == 'gift card') {
                $giftcard += 1;
                $options = $this->cart->product_options($items['rowid']);
                $this->OrderModel->updateGiftCardStatus($options['giftcard_id']);
                $data['recipient_name'] = $options['recipient_name'];
                $data['sender_name'] = $options['sender_name'];
                $data['card_number'] = $options['card_number'];
                $data['message'] = $options['message'];
                $data['giftcard_price'] = $items['price'];
                $this->email->to($options['recipient_email']);
                $this->email->cc($options['sender_email']);
                $this->email->bcc($this->config->item('contact_email'));
                $this->email->subject('You\'ve been sent a gift from ' . ucwords($data['sender_name']));
                $msg = $this->load->view('giftcard/giftCardEmail', $data, true);
                $this->email->set_mailtype("html");
                $this->email->message($msg);
                $this->email->send();
            }
        endforeach;
        if ($total > $giftcard) {
            $this->email->to($to);
            $this->email->cc($this->config->item('contact_email'));
            $this->email->subject('Awl & Sundry | Order ' . $orderId . ' Confirmation');
            $msg = $this->load->view('orderconfirmEmail', $data, true);
            $this->email->set_mailtype("html");
            $this->email->message($msg);
            $this->email->send();
            $this->sendOrderSuccessmailAdmins($data, $msg);
        }
        $this->session->unset_userdata('stripe_token');
        $this->session->unset_userdata('order_id');
        $this->removeCartItems();
        redirect('order-confirm/' . $orderId, 'refresh');
    }
    public function test1($orderId)
    {

        $this->load->library('email');
        $this->load->model('OrderModel');
        $data = $this->OrderModel->getbillingNshippingDetails($orderId);
        $data['order_summary'] = $this->OrderModel->getOrderDet($orderId);
        $msg = $this->load->view('orderconfirmEmail', $data, true);
        echo $msg;
    }
    private function sendOrderSuccessmailAdmins($data, $msg)
    {
        $this->email->to($this->config->item('contact_email'));
        $this->email->subject('New order at Awl & Sundry.');
        $msg = $this->load->view('orderconfirmEmailAdmin', $data, true);
        $this->email->set_mailtype("html");
        $this->email->message($msg);
        $this->email->send();
    }

    private function removeCartItems()
    {
        $this->load->library('cart');
        $this->cart->destroy();
        $this->session->unset_userdata('cartId');
    }

    private function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

}
