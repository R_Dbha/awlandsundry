<?php

class LastGenerate extends CI_Controller
{

    public $toes;
    public $vamps;
    public $eyestays;
    public $foxings;
    public $materials;
    public $stitches;
    public $laces;
    public $angles;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('GenerateModel');
    }

    public function index($id)
    {
        $lastStyleId = $id;
        if (!isset($id) || $id == "") {
            die("Don't Play...");
        }
        $details = $this->GenerateModel->get_last_style_details($lastStyleId);
        $baseImg = $details['def_code'] . "_" . $details['def_toe'] . $details['def_vamp'] . $details['def_eyestay'] . $details['def_foxing'] . $details['def_material'] . $details['def_color'];
        $filebase = $_SERVER['DOCUMENT_ROOT'] . "/files/images/";
        $mainimgbase = $filebase . $details['last_folder'] . "/" . $details['folder_name'] . "/" . $details['material_folder'] . "/";
        $thumbbase = $filebase . "thumb/";
        $materials = $this->populatematerials($details['last_folder'], $details['folder_name']);
        $properties = $this->populateproperties($details['last_folder'], $details['folder_name'], $details['is_vamp_enabled']);
        $stitches = $this->populateStitches($details['last_folder'], $details['folder_name'], $details['stitch_folder']);
        $laces = $this->populateLaces($details['last_folder'], $details['folder_name'], $details['lace_folder']);
        if (sizeof($properties) < 1) {
            die("No Properties found!. Please upload thumbnails.");
        }
        $this->GenerateModel->insert_style_properties($lastStyleId, $properties);
        if (sizeof($materials) > 0) {
            $this->GenerateModel->insert_style_materials($lastStyleId, $materials);
        }
        if (sizeof($stitches) > 0) {
            $this->GenerateModel->insert_style_stitches($lastStyleId, $stitches);
        }
        if (sizeof($laces) > 0) {
            $this->GenerateModel->insert_style_laces($lastStyleId, $laces);
        }
        $this->GenerateModel->process_style($mainimgbase, $baseImg, $lastStyleId, 249, 134, 720, 405, array("_A0", "_A7"), ".png", $thumbbase);
        echo "done";
    }

    public function updatestyleproperty($styleId)
    {
        $details = $this->GenerateModel->get_last_style_details($styleId, true);
        $properties = $this->populateproperties($details['last_folder'], $details['folder_name'], $details['is_vamp_enabled']);
        $this->GenerateModel->update_style_properties($styleId, $properties);
        echo 'done';
    }

    private function populateproperties($last_folder, $style_folder, $is_vamp)
    {
        $property_types = array();
        $filebase = $_SERVER['DOCUMENT_ROOT'] . "/files/images/" . $last_folder . "/" . $style_folder . "/properties/";
        $toes = glob($filebase . "Toe/T*");
        foreach ($toes as $toe) {
            $property_types[] = array("property" => substr(str_replace($filebase . 'Toe/', '', $toe), 0, 7), "img" => str_replace($filebase . 'Toe/', '', $toe));
        }
        if ($is_vamp) {
            $vamps = glob($filebase . "Vamp/V*");
            foreach ($vamps as $vamp) {
                $property_types[] = array("property" => substr(str_replace($filebase . 'Vamp/', '', $vamp), 0, 7), "img" => str_replace($filebase . 'Vamp/', '', $vamp));
            }
        }
        $eyestays = glob($filebase . "Eyestay/E*");
        foreach ($eyestays as $eyestay) {
            $property_types[] = array("property" => substr(str_replace($filebase . 'Eyestay/', '', $eyestay), 0, 7), "img" => str_replace($filebase . 'Eyestay/', '', $eyestay));
        }
        $foxings = glob($filebase . "Foxing/F*");
        foreach ($foxings as $foxing) {
            $property_types[] = array("property" => substr(str_replace($filebase . 'Foxing/', '', $foxing), 0, 7), "img" => str_replace($filebase . 'Foxing/', '', $foxing));
        }
        return $property_types;
    }

    private function populatematerials($lastfolder, $stylefolder)
    {
        $filebase = $_SERVER['DOCUMENT_ROOT'] . "/files/images/" . $lastfolder . "/" . $stylefolder . "/";
        $materials = $this->GenerateModel->get_materials();
        $avilmat = array();
        foreach ($materials as $material) {
            if (is_dir($filebase . $material['material_folder'])) {
                $avilmat[] = $material['material_def_id'];
            }
        }
        return $avilmat;
    }

    private function populateStitches($lastfolder, $stylefolder, $stitchfolder)
    {
        $filebase = $_SERVER['DOCUMENT_ROOT'] . "/files/images/" . $lastfolder . "/" . $stylefolder . "/" . $stitchfolder . "/";
        $stitches = $this->GenerateModel->get_stitches();
        $retStitches = array();
        if ($stitches != null) {
            foreach ($stitches as $stitch) {
                $files = glob($filebase . "*T*V*E*F*" . $stitch['stitch_code'] . "*.png");
                if (sizeof($files) > 0) {
                    $retStitches[] = $stitch['stitch_type_id'];
                }
            }
        }
        return $retStitches;
    }

    private function populateLaces($lastfolder, $stylefolder, $lacefolder)
    {
        $filebase = $_SERVER['DOCUMENT_ROOT'] . "/files/images/" . $lastfolder . "/" . $stylefolder . "/" . $lacefolder . "/";
        $laces = $this->GenerateModel->get_laces();
        $retlaces = array();
        if ($laces != null) {
            foreach ($laces as $lace) {
                $files = glob($filebase . "*T*V*E*F*" . $lace['lace_code'] . "*.png");
                if (sizeof($files) > 0) {
                    $retlaces[] = $lace['lace_type_id'];
                }
            }
        }
        return $retlaces;
    }

    private function testing()
    {
        $property_types = array();
        $filebase = $_SERVER['DOCUMENT_ROOT'] . "/properties/";
        $toes = glob($filebase . "Toe/T*");
        foreach ($toes as $toe) {
            $property_types[] = substr(str_replace($filebase . 'Toe/', '', $toe), 0, 7);
        }
        $batch = array();
        foreach ($property_types as $property) {
            $detail = explode('_', $property);
            $batch[] = array('last_style_id' => 1, 'property_type_id' => $detail[0], 'part_enabled' => $detail[1], 'is_nothing' => $detail[2]);
        }
        echo json_encode($batch);
    }

}
