/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 if (window.innerWidth >= 768){
    get_inspired_0 = $('#slider1').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 350,
        slideMargin: 13,
        pager: false,
        auto: false,
        infiniteLoop: false,
        hideControlOnEnd: true,
        preloadImages: 'all'
    });
    get_inspired_1 = $('#slider2').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 350,
        slideMargin: 13,
        pager: false,
        auto: false,
        infiniteLoop: false,
        hideControlOnEnd: true,
        preloadImages: 'all'
    });
    get_inspired_2 = $('#slider3').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 350,
        slideMargin: 13,
        pager: false,
        auto: false,
        infiniteLoop: false,
        hideControlOnEnd: true,
        preloadImages: 'all'
    });
 }else{
    if (window.innerWidth < 479) { 
        get_inspired_0 = $('#slider1').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            controls: true,
            pager: false
        });
        get_inspired_1 = $('#slider2').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            controls: true,
            pager: false
        });
        get_inspired_2 = $('#slider3').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            controls: true,
            pager: false
        });        
    }    
    if (window.innerWidth <= 767 && window.innerWidth >= 479) {   
        get_inspired_0 = $('#slider1').bxSlider({
            minSlides: 2,
            maxSlides: 2,
            slideWidth: 200,
            slideMargin: 5,
            controls: true,
            pager: false
        });
        get_inspired_1 = $('#slider2').bxSlider({
            minSlides: 2,
            maxSlides: 2,
            slideWidth: 200,
            slideMargin: 5,
            controls: true,
            pager: false
        });
        get_inspired_2 = $('#slider3').bxSlider({
            minSlides: 2,
            maxSlides: 2,
            slideWidth: 200,
            slideMargin: 5,
            controls: true,
            pager: false
        });        
    }
 }

$(window).resize(function () {
        if (window.innerWidth <= 767 && window.innerWidth >= 479) {
            $('.get_inspired_2 .bx-loading').hide();
            get_inspired_0.reloadSlider({
                minSlides: 2,
                maxSlides: 2,
                slideWidth: 200,
                slideMargin: 5,
                controls: true,
                pager: false
            });
            get_inspired_1.reloadSlider({
                minSlides: 2,
                maxSlides: 2,
                slideWidth: 200,
                slideMargin: 5,
                controls: true,
                pager: false
            });
            get_inspired_2.reloadSlider({
                minSlides: 2,
                maxSlides: 2,
                slideWidth: 200,
                slideMargin: 5,
                controls: true,
                pager: false
            });
        }


        if (window.innerWidth < 479) {
            $('.get_inspired_2 .bx-loading').hide();
            get_inspired_0.reloadSlider({
                minSlides: 1,
                maxSlides: 1,
                controls: true,
                pager: false
            });
            get_inspired_1.reloadSlider({
                minSlides: 1,
                maxSlides: 1,
                controls: true,
                pager: false
            });
            get_inspired_2.reloadSlider({
                minSlides: 1,
                maxSlides: 1,
                controls: true,
                pager: false
            });            
        }

});

