$(function () {
    $(window).on('hashchange', function () {
        if (typeof window.location.hash !== "undefined" && window.location.hash !== "#" && window.location.hash !== "") {
            loadPage(window.location.hash);
        }
    });
    if (typeof window.location.hash !== "undefined" && window.location.hash !== "#" && window.location.hash !== "") {
        loadPage(window.location.hash);
    }
    if (window.location.hash === '' || window.location.hash === '#') {
        window.location.hash = '#account_dashboard';
    }
    $('body').on('click','.change_public_name',function(){
    	$(this).hide();
    	var input = $(this).parent('p').children('input.public_name'); 
    	$(input).css({'display':'inline-block'});
    	$(input).focus();
    });
    $('body').on('blur','.public_name',function(){
    	$(this).hide();
    	var public_name =$(this).val(); 
    	var a = $(this).parent('p').children('.change_public_name');
    	$(a).text(public_name);
    	$(a).show();
    	var shoe_design_id = $(this).closest('.item.purchased').attr('data-id');
    	$.ajax({
            type: "POST",
            url: baseUrl + "useraccount/save_shoe_name/"+shoe_design_id,
            data: {
                'public_name': public_name,
            },
             dataType: 'json',
            success: function (response) {
            
            }
        });
    	
    });

});

function loadPage(url, msg) {
    if (typeof url !== 'undefined' && url !== "#") {
        url = url.replace("#", "");
    } else {
        url = '';
    }
    hash = url;
    url = window.location.origin + window.location.pathname + '/' + url;
    $.ajax({
        url: url
    }).done(function (resp, respStatus, xhr) {
        $('#account-dashboard').html(resp);
        $('ul#dashboard-menu li a').removeClass('current');
        $('a[href*="' + hash + '"]').addClass('current');


        $('.tinynav option').removeAttr('selected');
        $('.tinynav option[value=' + '#' + hash + ']').attr('selected', 'selected');
        $('.SumoSelect .SlectBox span').text($('.tinynav option:selected').text());
    });
}

$(document).ready(function () {
    $('#confirm_pwd').on('keyup', function () {
        if ($(this).val() == $('#new_pwd').val()) {
            $('#message').html('matching').css('color', 'green');
        } else
            $('#message').html('not matching').css('color', 'red');
    });

    $('ul#dashboard-menu li a').click(function () {

        // $(this).addClass('current');
    });
    $('#dashboard').on('click', '.track_order', track_order);
    $('#dashboard').on('click', '.overlay', closePopUp);
    $('#dashboard').on('click', 'span.close', function () {
        closePopUp();
    });



    // Mobile Dropdown
    $(function () {
        $("#dashboard-menu").tinyNav({
            active: 'account-current'
        });
    });

    $(function () {
        $(".tinynav").SumoSelect();
    });
});


function track_order() {

    var slug = $(this).parent().find('.shipping_carrier').val();
    var tracking_number = $(this).parent().find('.tracking_number').val();
    if (tracking_number === '') {
        alert('order not shipped');
        return;
    }

    $.ajax({
        type: "POST",
        url: baseUrl + "aftership/tracking",
        data: {
            'tracking_number': tracking_number,
            'slug': slug,
        },
        // dataType: 'json',
        success: function (response) {
            $('#tracking-popup-wrapper').html(response);
            $('body').css({'overflow': 'hidden'});
            $('#tracking-popup-wrapper').show();
            $('#index-popup-wrapper').hide();
            $('.overlay').show();
        }
    });
    // console.log(tracking_number, slug);
}
function btn_order_view_details(orderId) {
    window.location.hash = "#view_order_details/" + orderId;
}
function save_address() {
    var isBilling;
    var isShipping;
    var ajxUrl;
    var addressData = {};
    if ((document.getElementById('chkBilling').checked)) {
        isBilling = '1';
    } else {
        isBilling = '0';
    }
    if ((document.getElementById('chkShipping').checked)) {
        isShipping = '1';
    } else {
        isShipping = '0';
    }
    if ($('#address_det_id').val() != "") {
        ajxUrl = base_url + "useraccount/save_address_details/" + $('#address_det_id').val();
    } else {
        ajxUrl = base_url + "useraccount/save_address_details";
    }
    addressData = {userId: userId,
        first_name: $('#fname').val(),
        last_name: $('#lname').val(),
        company: $('#company').val(),
        phone: $('#phone').val(),
        fax: $('#fax').val(),
        address1: $('#address1').val(),
        address2: $('#address2').val(),
        city: $('#city').val(),
        state: $('#ddlState').val(),
        country: $('#ddlCountry').val(),
        zipcode: $('#zipcode').val(),
        is_billing: isBilling,
        is_shipping: isShipping
    }
    // console.log(addressData);
    $.ajax({
        type: "POST",
        url: ajxUrl,
        dataType: "json",
        data: addressData,
        success: function (msg) {
            if ($('#hid_from').val() != 'dashboard') {
                window.location.hash = '#address_book';
            }
            else {
                window.location.hash = '#account_dashboard';
            }
        }
    });
    return false;
}
function validate() {
    $('#account-dashboard input').each(function () {

        /*
		 * if ($(this).val().trim() == "") { //console.log($(this).attr('id') +
		 * "_Err"); console.log($($(this).attr('id') +
		 * "_Err").removeClass('hide')); $($(this).attr('id') +
		 * "_Err").removeClass('hide'); } else { $($(this).attr('id') +
		 * "_Err").addClass('hide'); } //console.log($(this));
		 */
    });
}
function update_userinfo() {
    if ($('#fname').val().trim() != "" && $('#lname').val().trim() != "" && $('#email').val().trim() != "") {
        userData = {
            first_name: $('#fname').val(),
            last_name: $('#lname').val(),
            email: $('#email').val()
        };
        // console.log(addressData);
        $.ajax({
            type: "POST",
            url: base_url + "useraccount/update_userinfo",
            dataType: "json",
            data: userData,
            success: function (msg) {
                if ($('#hid_from').val() == 'main') {
                    window.location.hash = '#account_dashboard';
                } else {
                    window.location.hash = '#account_info';
                }
                // $('#account-dashboard').html(msg);
            }
        });
        return false;
    } else {
        return false;
    }
}
function deleteAccount(id,type) {
    address = {
        addressId: id,
        type:type
    };
    if (confirm('Are you sure you really want to delete this address?')) {
        $.ajax({
            type: "POST",
            url: base_url + "useraccount/deleteAddress",
            dataType: "json",
            data: address,
            success: function (msg) {
                // window.location.reload();
                window.location.hash = '#address_book';
                window.location.reload();
            }
        });
    }
}
function reset_password() {
    if ($('#pwd').val() != "") {
        if ($('#new_pwd').val() != $('#confirm_pwd').val()) {
            $('#message').text("Invalid confirm Password");
            return false;
        } else {
            passwords = {
                current: $('#pwd').val(),
                new : $('#new_pwd').val()
            };
            $.ajax({
                type: "POST",
                url: base_url + "useraccount/reset_password",
                dataType: "html",
                data: passwords,
                success: function (msg) {
                    // alert(msg);
                    if (msg == 1) {
                        // console.log(msg);
                        window.location.hash = "#account_info";
                        // loadPage('#account_info', msg);
                    } else {

                        $('#account-dashboard').html(msg);

                        // alert('Current password is not correct');
                    }

                }
            });
        }
    } else {
        $('#pwd_message').text("Please enter current password");
        return false;
    }
    return false;
}
function edit_savedshoe(shoeId) {
    $.ajax({
        type: "POST",
        url: base_url + "useraccount/edit_savedshoe/" + shoeId,
        success: function (msg) {
            window.location = base_url + msg;
        }
    });
}
function showPopUpExchangeShoe() {
    $('body').css({'overflow': 'hidden'});
    $('#index-popup-wrapper').show();
    $('#tracking-popup-wrapper').hide();
    $('.overlay').show();
}
function closePopUp() {
    $('body').css({'overflow': 'auto'});
    $('#index-popup-wrapper').hide();
    $('#tracking-popup-wrapper').hide();
    $('.overlay').hide();
}
function processGiftCard() {
    if ($('#cer_code').val().trim() !== "") {
        giftcard_code = {
            code: $('#cer_code').val().trim(),
            from: 'dashboard'
        };
        $.ajax({
            type: "POST",
            dataType: "html",
            data: giftcard_code,
            url: base_url + "useraccount/processGiftCard/",
            success: function (msg) {
                $('#account-dashboard').html(msg);
                $('#cer_code').text("");
            }
        });
    } else {
        $('.gc_msg').text('Please enter Gift Certificate Code. ');
        $('#cer_code').focus();
    }
    return false;
}
$('#cer_code').on('keypress', function () {
    $('.gc_msg').text('');
});
function checkbalanceGiftCard() {
    if ($('#cer_code').val().trim() !== "") {
        giftcard_code = {
            code: $('#cer_code').val().trim()
        };
        $.ajax({
            type: "POST",
            dataType: "html",
            data: giftcard_code,
            url: base_url + "useraccount/checkBalanceGiftCard/",
            success: function (msg) {
                $('#account-dashboard').html(msg);
                $('#cer_code').text("");
            }
        });
    } else {
        $('.gc_msg').text('Please enter Gift Certificate Code. ');
        $('#cer_code').focus();
    }
    return false;
}
function purchase_savedshoe(shoeId) {
    $.ajax({
        type: "POST",
        url: base_url + "useraccount/purchase_shoe/" + shoeId,
        success: function (msg) {
            console.log(msg);
            window.location = base_url + msg;
        }
    });
}
function delete_savedshoe(shoeId) {
    $.ajax({
        type: "POST",
        url: base_url + "useraccount/delete_savedshoe/" + shoeId,
        success: function (msg) {
            if (msg === 'success') {
                $(".purchased").filter("[data-id='" + shoeId + "']").remove();
            }else{
                alert('Not Deleted');
            }

        }
    });
}

