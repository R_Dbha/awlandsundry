$(document).ready(function () {
    if (isTouch) {
        if (window.innerWidth <= 767) {
            ModelRenderer.config.isMobile = true;
        }
    }
    CustomShoe.currentStep = "styling";
    CustomShoe.loadModel(function (model) {
        ShoeModel = model;
        CustomShoe.styling.init();
    });
});