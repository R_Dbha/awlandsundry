var imageBase = "https://www.awlandsundry.com/files/images/";
var svgPath = "https://www.awlandsundry.com/files/SVG/";
var designBase = "https://www.awlandsundry.com/files/designs/";
var thumbBase = baseUrl + "files/images/thump/";
Object.keys = Object.keys || function(o) {
    var result = [];
    for (var name in o) {
        if (o.hasOwnProperty(name))
            result.push(name);
    }
    return result;
};
var ShoeModel = {
    ext: '.png',
    baseImg: '',
    angle: '_A0',
    last: {
        code: 'L2',
        id: '',
        folder: ''
    },
    style: {
        code: 'DE',
        id: '',
        folder: ''
    },
    size: {left: {value: 0, text: '0'}, right: {value: 0, text: ''}}
};
ShoeModel.monogram = {
    leftShoe: 0,
    rightShoe: 0,
    leftSide: 0,
    rightSide: 0,
    text: ''
};
/*,
 quarter: {
 base: 'T00V00E01F00M0C0',
 material: 'M0',
 matfolder: 'CP',
 color: 'C0'
 },
 toe: {
 type: 'T00',
 toeId: -1,
 default: 'T00',
 material: 'M0',
 matfolder: 'CP',
 color: 'C0',
 part: 'P0'
 },
 vamp: {
 type: 'V00',
 vampId: -1,
 default: 'V00',
 material: 'M0',
 matfolder: 'CP',
 color: 'C0',
 part: 'P1'
 },
 eyestay: {
 type: 'E01',
 eyestayId: -1,
 default: 'E01',
 material: 'M0',
 matfolder: 'CP',
 color: 'C0',
 part: 'P2'
 },
 foxing: {
 type: 'F00',
 foxingId: -1,
 default: 'F00',
 material: 'M0',
 matfolder: 'CP',
 color: 'C0',
 part: 'P3'
 },
 lace: {code: 'L0', laceId: -1, name: 'Black'},
 stitch: {code: 'S0', stitchId: -1, name: 'Black'},
 size: {left: {value: 0, text: '0'}, right: {value: 0, text: ''}}
 };*/
var ModelRenderer = {
    render: function(stepStatus) {
        ModelRenderer.renderBase();
        ModelRenderer.renderSVG();
        ModelRenderer.renderStitch();
        ModelRenderer.renderLace();
        ModelRenderer.renderMonogram();
        if (stepStatus !== 1) {
            ModelRenderer.renderMaterialColor();
        }
        ModelRenderer.calculatePrice();
    },
    renderMaterialColor: function() {
        var images = [];
        var base = ShoeModel.style.code + "_" + ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type;
        var urlPrefix = ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/";
        images.push({type: 'toe', url: urlPrefix + ShoeModel.toe.matfolder + "/" + base + ShoeModel.toe.material + ShoeModel.toe.color + ShoeModel.toe.part + "_" + ShoeModel.angle + ShoeModel.ext});
        if (ShoeModel.style.isVampEnabled !== "0") {
            images.push({type: 'vamp', url: urlPrefix + ShoeModel.vamp.matfolder + "/" + base + ShoeModel.vamp.material + ShoeModel.vamp.color + ShoeModel.vamp.part + "_" + ShoeModel.angle + ShoeModel.ext});
        } else {
            $("#vamp").hide();
        }
        images.push({type: 'eyestay', url: urlPrefix + ShoeModel.eyestay.matfolder + "/" + base + ShoeModel.eyestay.material + ShoeModel.eyestay.color + ShoeModel.eyestay.part + "_" + ShoeModel.angle + ShoeModel.ext});
        images.push({type: 'foxing', url: urlPrefix + ShoeModel.foxing.matfolder + "/" + base + ShoeModel.foxing.material + ShoeModel.foxing.color + ShoeModel.foxing.part + "_" + ShoeModel.angle + ShoeModel.ext});
        $.each(images, function(i, val) {
            $.ajax({
                url: baseUrl + 'customshoe/get_image_base',
                data: {
                    imagepath: val.url
                },
                dataType: 'JSON',
                type: 'POST',
                success: function(msg) {
                    if (msg.exists) {
                        $("#" + val.type).attr('src', msg.base);
                        $("#" + val.type).show();
                    } else {
                        $("#" + val.type).hide();
                    }
                }
            });
        });
    },
    calculatePrice: function() {
        var materials = [];
        materials.push(ShoeModel.quarter.material);
        if (ShoeModel.style.isVampEnabled === "1") {
            if (ShoeModel.vamp.isMaterial === "1") {
                materials.push(ShoeModel.vamp.material);
            }
        }
        if (ShoeModel.toe.isMaterial === "1") {
            materials.push(ShoeModel.toe.material);
        }
        if (ShoeModel.eyestay.isMaterial === "1") {
            materials.push(ShoeModel.eyestay.material);
        }
        if (ShoeModel.foxing.isMaterial === "1") {
            materials.push(ShoeModel.foxing.material);
        }
        $.ajax({
            url: baseUrl + 'customshoe/get_price',
            data: {
                materials: materials
            },
            type: 'POST',
            success: function(msg) {
                $('#price_div span.price').html(msg);
            }
        });
    },
    renderBase: function() {
        var ext = '.png';
        var urlPrefix = imageBase + ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/" + ShoeModel.quarter.matfolder + "/" + ShoeModel.style.code + "_";
        $("#shape_visualization .visualImgWrap.img").hide();
        ShoeModel.quarter.base = ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type + ShoeModel.quarter.material + ShoeModel.quarter.color;
        $("#base").attr("src", urlPrefix + ShoeModel.quarter.base + "_" + ShoeModel.angle + ext);
        $("#base").show();
    },
    renderMonogram: function() {
        $("#shape_visualization div.monogram").hide();
        $("#shape_visualization div.monogram").removeClass('monogram_A0 monogram_A4');
        if (typeof ShoeModel.monogram !== "undefined" && (ShoeModel.angle === "A0" || ShoeModel.angle === "A4")) {
            if ((ShoeModel.angle === "A0" && ShoeModel.monogram.rightSide === 1) || (ShoeModel.angle === "A4" && ShoeModel.monogram.leftSide == 1)) {
                if ($("#shape_visualization div.monogram").length > 0) {
                    $("#shape_visualization div.monogram img").remove();
                    for (var i = 0; i < ShoeModel.monogram.text.length; i++) {
                        $img = $("<img>");
                        $img.attr("src", imageBase + "monogram/" + ShoeModel.monogram.text[i] + ".png");
                        $("#shape_visualization div.monogram").append($img);
                    }
                    $("#shape_visualization div.monogram").addClass("monogram_" + ShoeModel.angle);
                    $("#shape_visualization div.monogram").show();
                }
            }
        }
    },
    renderStitch: function() {
        var urlPrefix = imageBase + ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/" + ShoeModel.stitch.folder + "/" + ShoeModel.style.code + "_";
        var stitch = ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type + ShoeModel.stitch.code + "_" + ShoeModel.angle + ShoeModel.ext;
        $("#stitch").attr("src", urlPrefix + stitch);
        $("#stitch").show();
    },
    renderLace: function() {
        var urlPrefix = imageBase + ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/" + ShoeModel.lace.folder + "/" + ShoeModel.style.code + "_";
        var lace = ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type + ShoeModel.lace.code + "_" + ShoeModel.angle + ShoeModel.ext;
        $("#lace").attr("src", urlPrefix + lace);
        $("#lace").show();
    },
    renderSVG: function() {
        if ($("#style_path").length > 0) {
            $.ajax({
                url: svgPath + ShoeModel.style.svg + "_" + ShoeModel.angle + ".svg",
                success: function(msg) {
                    $("#style_path").html(arguments[2].responseText);
                }
            });
        }
    },
    errorRender: function() {
        $("#shape_visualization .visualImgWrap.img").hide();
        $("#error").show();
        $("#error").delay(5000).hide(1);
    },
    partNotAvailable: function(imgId) {
        $("#" + imgId).hide();
    }
};
var CustomShoe = {
    adjacentProperties: {
        toe: ["vamp", "quarter", "eyestay"],
        vamp: ["toe", "eyestay", "quarter"],
        eistay: ["vamp", "quarter"],
        foxing: ["quarter"]
    },
    currentStep: 'laststyle',
    saveModel: function(callBack, status) {
        var url = baseUrl + "customshoe/saveStepDetails";
        if (typeof status !== "undefined") {
            url += "/" + status;
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {
                step_data: ShoeModel,
                current_step: CustomShoe.currentStep
            },
            dataType: 'JSON',
            success: function() {
                callBack();
            }
        });
    },
      saveAndShare: function() {
        CustomShoe.stepFour.saveDesign(function(response) {
            ShoeDesign.designDetails(response.shoe.shoeDesignId);
        });
    },
    stepOne: {
        init: function() {
            $('.q_symbol').tooltipsy({offset: [0, 10]});
            //$('.imgShap').tooltipsy({offset: [0, 10]});

            $('.imgShap').attr('title', '');
            $(".createNewNav a.active").parents("li").nextAll().each(function() {
                $(this).find("a").attr("href", "javascript:void(0);");
                $(this).find("a").addClass("no-bg");
            });
            $('.createNewContainer').click(function() {
                $('.pick_your_last li').removeClass('active');
            });
            $("div.basicStyleBox div.imgShapLast").on('click tap', function() {
                $('.pick_your_last li').removeClass('active');
                $(this).addClass('active');
                id = $(this).attr('id');
                window.location.href = baseUrl + "create-a-custom-shoe/select-last/" + id;

            });
            $('.control-back').off('click touchend').on('click touchend', function() {
                window.location.href = baseUrl + "create-a-custom-shoe/select-style";
            });
            $("div.basicStyleBox div.imgShapStyle").off('click touchend').on('click touchend', function() {
                var styleId = $(this).parent('li').find(".ls_style_id").eq(0).val();
                $('.pick_your_last li').removeClass('active');
                $(this).addClass('active');
                // CustomShoe.stepOne.getStyleDetails(styleId);
                ShoeModel.style.id = styleId;
                CustomShoe.stepOne.nextStep();
                $('.control-back').off('click touchend').on('click touchend', function() {
                    CustomShoe.stepOne.closeStyleDetails();
                });
            });
            $("#steptwosubmit").off('click touchend').on('click touchend', function() {
                CustomShoe.stepOne.nextStep();
            });

//            $('.createNewContainer').off('click touchend').on('click touchend',function(e) {
//                if ($('#shape_selection_popup:visible').length === 0)
//                {
//                    e.preventDefault();
//                } else {
//                    CustomShoe.stepOne.closeStyleDetails();
//
//                }
//            });
            $("a.top").off('click touchend').on('click touchend', function() {
                var elem = $('.visualImgWrap img').attr('src');
                var code = elem[elem.length - 5];
                var code = parseInt(code);
                var src = elem.replace(code + ".png", "7.png");
                $('.visualImgWrap img').attr('src', src);

            });
            $("a.rotate.right").off('click touchend').on('click touchend', function() {
                var elem = $('.visualImgWrap img').attr('src');
                var code = elem[elem.length - 5];
                var code = parseInt(code);
                var a = code;
                if (code === 7) {
                    a = 0;
                } else {
                    a++;
                }
                var src = elem.replace(code + ".png", a + ".png");
                $('.visualImgWrap img').attr('src', src);

            });

            $("a.rotate.left").off('click touchend').on('click touchend', function() {
                var elem = $('.visualImgWrap img').attr('src');
                var code = elem[elem.length - 5];
                var code = parseInt(code);
                var a = code;
                if (code === 0) {
                    a = 7;
                } else {
                    a--;
                }
                var src = elem.replace(code + ".png", a + ".png");
                $('.visualImgWrap img').attr('src', src);

            });
        },
        closeStyleDetails: function() {
            $('.shape_popup').animate({height: "0"}, 1500, function() {
                $('.shape_popup').hide();
                $('.shape_popup').removeClass("popup_alive");
            });
            $('.control-back').off('click touchend').on('click touchend', function() {
                window.location.href = baseUrl + "create-a-custom-shoe/select-style";
            });
        },
        getStyleDetails: function(styleId) {
            $.ajax({
                type: "POST",
                url: baseUrl + "customshoe/styledetails",
                data: {
                    styleId: styleId
                },
                dataType: 'json',
                success: function(response) {
                    //console.log(response.last_styles.styles);
                    data = response.style_details;
                    var detDiv = $(".shape_selection_popup");
                    detDiv.find(".visualImgWrap img").attr("src", imageBase + data.last_folder + "/" + data.folder_name + "/" + data.material_folder + "/" + data.img_file + "_" + data.def_angle + ShoeModel.ext);
                    detDiv.find("#shoe_details .item-title").text(data.last_name);
                    detDiv.find("#shoe_details .item-type").text(data.style_name);
                    //  detDiv.find(".stepOneHeading").html('<span>Step 2</span> You have selected ' + data.style_name + ' (' + data.last_name + ')');
                    detDiv.find(".stepOneHeading").html('Take a closer look at your shoe');
                    detDiv.find("#shoe_details .item-desc").text(data.style_description);
                    detDiv.find("#price_div .price").text(data.price);
                    ShoeModel.style.id = data.last_style_id;

                    //console.log(response.last_styles[0].styles);
                    $('.thumbs_list').html('');
                    $('#lastval').val(data.last_id);
                    $.each(response.last_styles[0].styles, function(i, value) {
                        var active = '';
                        if (value.last_style_id === styleId)
                            active = 'class="active"';
                        $('.thumbs_list').append('<li ' + active + '><input type="hidden" class="hid__Id" value="' + value.last_style_id + '"/> <img style="width:79px;" src= "' + imageBase + "thumb" + "/" + value.img_file + "_A0" + ShoeModel.ext + '" /><span>' + value.style_name + '</span></li>');
                    });

                    CustomShoe.stepOne.showPopup();
                    $.each($('.thumbs_list').find('li'), function() {
                        $(this).find('img').off('click touchend').on('click touchend', function() {

                            var styleId = $(this).parents('li').find('input').val();
                            CustomShoe.stepOne.getStyleDetails(styleId);
                        });
                    });
                }
            });
        },
        showPopup: function() {
            var eTop = $('#stepFlowBar').offset().top;
            $('.shape_selection_popup').css({'margin-top': eTop - $(window).scrollTop()});

            $("html, body").animate({scrollTop: "0"}, 500, function() {
                var $selectionPopup = $(".shape_popup");
                $selectionPopup.css("opacity", "0");
                $selectionPopup.css({
                    'display': 'block',
                    'left': $(window).width() / 2,
                    'margin-left': '-' + ($selectionPopup.width() / 2) + 'px'
                }).addClass('popup_alive');
                $selectionPopup.css("opacity", "1");
                $selectionPopup.animate({height: "510"}, 1200);

            });
            document.onkeydown = function(ev) {
                switch (ev.keyCode) {
                    case 37://left
                        var elem = $('.visualImgWrap img').attr('src');
                        var code = elem[elem.length - 5];
                        var code = parseInt(code);
                        var a = code;
                        if (code === 0) {
                            a = 7;
                        } else {
                            a--;
                        }
                        var src = elem.replace(code + ".png", a + ".png");
                        $('.visualImgWrap img').attr('src', src);
                        break;
                    case 38://up
                        break;
                    case 39://right
                        var elem = $('.visualImgWrap img').attr('src');
                        var code = elem[elem.length - 5];
                        var code = parseInt(code);
                        var a = code;
                        if (code === 7) {
                            a = 0;
                        } else {
                            a++;
                        }
                        var src = elem.replace(code + ".png", a + ".png");
                        $('.visualImgWrap img').attr('src', src);
                        break;
                    case 40://down
                        break;
                }
            };
        },
        loadNextLast: function(mode) {
            var lastId = $('#lastval').val();
            $.ajax({
                type: "POST",
                url: baseUrl + "customshoe/lastsByLastId",
                data: {
                    lastId: lastId,
                    mode: mode
                },
                dataType: 'json',
                success: function(response) {
                    $('#lastval').val(response.last_styles[0].last_item_def_id);
                    $('.thumbs_list').html('');
                    $.each(response.last_styles[0].styles, function(i, value) {
                        $('.thumbs_list').append('<li><input type="hidden" class="hid_styleId" value="' + value.last_style_id + '"/> <img style="width:79px;" src= "' + imageBase + "thumb" + "/" + value.img_file + "_A0" + ShoeModel.ext + '" /><span>' + value.style_name + '</span></li>');
                    });
                    CustomShoe.stepOne.getStyleDetails(response.last_styles[0].styles[0].last_style_id);
                    $.each($('.thumbs_list').find('li'), function() {
                        $(this).find('img').off('click touchend').on('click touchend', function() {
                            var styleId = $(this).parents('li').find('input').val();
                            CustomShoe.stepOne.getStyleDetails(styleId);
                        });
                    });
                }
            });
        },
        nextStep: function() {
            CustomShoe.saveModel(function() {
                window.location.href = baseUrl + "create-a-custom-shoe/design-features";
            });
        }
    },
    stepTwo: {
        init: function(model) {

            ShoeModel = model;
            $("#toe_styling").children().remove();
            $("#eyestays_styling").children().remove();
            $("#foxiing_styling").children().remove();
            $('.topSliderRight').hide();
            if (ShoeModel.style.isVampEnabled) {
                $("#vamp_styling").children().remove();
            }
            $("#style_types a").off('click touchend').on('click touchend', function() {
                $("#style_types a").removeClass('active');
                var required = '';
                var _this = $(this);
                switch (_this.text()) {
                    case 'Toe':
                        required = 'Toe';
                        break;
                    case 'Vamp':
                        required = 'Vamp';
                        break;
                    case 'Eyestays' :
                        required = 'Eyestay';
                        break;
                    case 'Foxing':
                        required = 'Foxing';
                        break;
                }
                var cur = _this.attr("id").replace("design_", '');
                _this.addClass('active');
                CustomShoe.stepTwo.fixInvalid(required, cur + "_styling", function() {
                    $(".topSliderRight:visible").hide();
                    $("#" + cur + "_styling").parents(".topSliderRight").show();
                    $('li.disabled').tooltipsy({offset: [0, 10]});
                });
            });
            $("#shape_visualization img").off('click touchend').on('click touchend', function(e) {
                CustomShoe.stepTwo.showPopup();
            });
            $("a.rotate.left").click(function() {
                var angle = parseInt(ShoeModel.angle.replace('A', ''));
                if (angle === 0) {
                    ShoeModel.angle = "A7";
                } else {
                    angle = angle - 1;
                    ShoeModel.angle = "A" + angle;
                }
                ModelRenderer.render(2);
            });
            $("a.top").click(function() {
                ShoeModel.angle = "A7";
                ModelRenderer.render(2);
            });
            $("a.rotate.right").off('click touchend').on('click touchend', function() {
                var angle = parseInt(ShoeModel.angle.replace('A', ''));
                if (angle === 7) {
                    ShoeModel.angle = "A0";
                } else {
                    angle = angle + 1;
                    ShoeModel.angle = "A" + angle;
                }
                ModelRenderer.render(2);
            });
            document.onkeydown = function(ev) {
                switch (ev.keyCode) {
                    case 37://left
                        var angle = parseInt(ShoeModel.angle.replace('A', ''));
                        if (angle === 0) {
                            ShoeModel.angle = "A7";
                        } else {
                            angle = angle - 1;
                            ShoeModel.angle = "A" + angle;
                        }
                        ModelRenderer.render(2);
                        break;
                    case 38://up
                        break;
                    case 39://right
                        var angle = parseInt(ShoeModel.angle.replace('A', ''));
                        if (angle === 7) {
                            ShoeModel.angle = "A0";
                        } else {
                            angle = angle + 1;
                            ShoeModel.angle = "A" + angle;
                        }
                        ModelRenderer.render(2);
                        break;
                    case 40://down
                        break;
                }
            };

            $("#shape_visualization img").error(function() {
                if ($(this).attr("id") === "base") {
                    ModelRenderer.errorRender();
                } else {
                    ModelRenderer.partNotAvailable($(this).attr("id"));
                }
            });
            $("#stepthreesubmit").off('click touchend').on('click touchend', function() {
                CustomShoe.stepTwo.nextStep();
            });
            $("#style_path").off('click touchend').on('click touchend', 'path', function(e) {

                CustomShoe.stepTwo.partSelected(e.currentTarget);
            });
            $("#style_path").on('mousemove', 'path', function(e) {
                $(this).css({'cursor': 'pointer'});
                CustomShoe.stepTwo.partTouched(e, $(e.currentTarget).attr('id').replace('path_', ''));
            });
            $("#style_path").on('mouseleave', 'path', function(e) {
                $(this).css({'cursor': 'default'});
                $('div#tooltip').remove();
            });
            CustomShoe.stepTwo.loadProperties(function() {
                CustomShoe.stepTwo.renderProperties(function() {
                    CustomShoe.stepTwo.initCarousel('.topSliderRight ul.jHcarousel');
                    ModelRenderer.render(2);
                    //CustomShoe.stepTwo.showPopup();
                    $("body").css("overflow", "hidden");
                    $('#status').fadeOut(); // will first fade out the loading animation
                    $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                    $('body').delay(350).css({'overflow': 'visible'});
                });
            });

        },
        partTouched: function(e, text) {
            $('.visualImgWrap img').css({'cursor': 'pointer'});
            var part = text.replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            });
            $('div#tooltip').remove();
            $('<div id="tooltip">' + part + '</div>').appendTo('body');
            var tooltipX = e.pageX - 8;
            var tooltipY = e.pageY + 8;
            $('div#tooltip').css({top: tooltipY, left: tooltipX});
        },
        partSelected: function(part) {
            var spart = $(part).attr('id').replace('path_', '');
            CustomShoe.stepTwo.showPopup();
            if (spart !== "eyestay") {
                $("#design_" + spart).click();
            } else {
                $("#design_eyestays").click();
            }
        },
        initCarousel: function(selector) {

            var slider = $(selector).bxSlider({
                minSlides: 5,
                maxSlides: 7,
                slideWidth: 151,
                slideMargin: 6,
                infiniteLoop: false,
                pager: false,
                hideControlOnEnd: true,
                nextText: '',
                prevText: ''
            });
        },
        showPopup: function() {
            var eTop = $('#stepFlowBar').offset().top;
            $('.shape_selection_popup').css({'margin-top': eTop - $(window).scrollTop()});

            $('#topSlider').animate({left: "0"}, 1200);
            $('.toe_selection').css("display", "block");
            $("html, body").animate({scrollTop: "0"}, 500, function() {
                var $selectionPopup = $(".shape_popup");
                $selectionPopup.css("opacity", "0");
                $selectionPopup.css({
                    'display': 'block',
                    'left': $(window).width() / 2,
                    'margin-left': '-' + ($selectionPopup.width() / 2) + 'px'
                }).addClass('popup_alive');
                $selectionPopup.css("opacity", "1");
                $selectionPopup.animate({height: "126"}, 1200);
            });
        },
        loadProperties: function(loadComplete) {
            $.ajax({
                type: "POST",
                url: baseUrl + "customshoe/styleproperties/" + ShoeModel.style.id,
                dataType: 'JSON',
                success: function(properties) {
                    CustomShoe.stepTwo.populateProperties(properties, loadComplete);
                }
            });
        },
        fixInvalid: function(required, page, callBack) {
            var data = {Toe: ShoeModel.toe.type, Eyestay: ShoeModel.eyestay.type, Vamp: ShoeModel.vamp.type, Foxing: ShoeModel.foxing.type, required: required, styleId: ShoeModel.style.id};
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: baseUrl + 'customshoe/get_properties/',
                data: data,
                success: function(properties) {
                    $("#" + page + " li").removeClass('disabled').removeAttr('title');
                    if (typeof properties.notavailable !== "undefined") {
                        $.each(properties.notavailable, function(i, val) {
                            if (required == 'Eyestay') {
                                if (ShoeModel.style.folder == 'Oxford') {
                                    $("#" + page + " img[data-id='" + val.property_type_id + "']").parent().addClass("disabled").attr("title", "Select an appropriate vamp to activate this design");
                                } else {
                                    $("#" + page + " img[data-id='" + val.property_type_id + "']").parent().addClass("disabled").attr("title", "Select a different design to access this option");
                                }
                            } else {
                                $("#" + page + " img[data-id='" + val.property_type_id + "']").parent().addClass("disabled").attr("title", "Select a different design to access this option");
                            }
                            if (i === (properties.notavailable.length - 1)) {
                                callBack();
                            }
                        });
                    } else {
                        callBack();
                    }
                }
            });
        },
        propertySelected: function() {
            if (!$(this).parent().hasClass('disabled')) {
                var key = $(this).attr("data-key");
                var idx = $(this).attr("data-idx");
                var property = CustomShoe.properties[key][idx];
                var loadMaterial = false;
                var base = "";
                var part = "";

                if (property.type === "Toe") {
                    if (ShoeModel.toe.isMaterial == 0 && property.isMaterial == 1) {
                        loadMaterial = true;
                        base = property.code + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type;
                        part = ShoeModel.toe.part;

                    }
                    ShoeModel.toe.toeId = property.id;
                    ShoeModel.toe.type = property.code;
                    ShoeModel.toe.isMaterial = property.isMaterial;
                    ShoeModel.toe.isNothing = property.isNothing;
                    ShoeModel.toe.idx = idx;
                    // ShoeModel.toe.color = ShoeModel.style.color;
                } else if (property.type === "Vamp") {
                    if (ShoeModel.vamp.isMaterial == 0 && property.isMaterial == 1) {
                        loadMaterial = true;
                        base = ShoeModel.toe.type + property.code + ShoeModel.eyestay.type + ShoeModel.foxing.type;
                        part = ShoeModel.vamp.part;
                    }
                    ShoeModel.vamp.vampId = property.id;
                    ShoeModel.vamp.type = property.code;
                    ShoeModel.vamp.isMaterial = property.isMaterial;
                    ShoeModel.vamp.isNothing = property.isNothing;
                    ShoeModel.vamp.idx = idx;
                    //  ShoeModel.vamp.color = ShoeModel.style.color;
                } else if (property.type === "Eyestay") {
                    if (ShoeModel.eyestay.isMaterial == 0 && property.isMaterial == 1) {
                        loadMaterial = true;
                        base = ShoeModel.toe.type + ShoeModel.vamp.type + property.code + ShoeModel.foxing.type;
                        part = ShoeModel.eyestay.part;
                    }
                    ShoeModel.eyestay.eyestayId = property.id;
                    ShoeModel.eyestay.type = property.code;
                    ShoeModel.eyestay.isMaterial = property.isMaterial;
                    ShoeModel.eyestay.isNothing = property.isNothing;
                    ShoeModel.eyestay.idx = idx;
                    // ShoeModel.eyestay.color = ShoeModel.style.color;
                } else if (property.type === "Foxing") {
                    if (ShoeModel.foxing.isMaterial == 0 && property.isMaterial == 1) {
                        loadMaterial = true;
                        base = ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + property.code;
                        part = ShoeModel.foxing.part;
                    }
                    ShoeModel.foxing.foxingId = property.id;
                    ShoeModel.foxing.type = property.code;
                    ShoeModel.foxing.isMaterial = property.isMaterial;
                    ShoeModel.foxing.isNothing = property.isNothing;
                    ShoeModel.foxing.idx = idx;
                    //  ShoeModel.foxing.color = ShoeModel.style.color;
                }
                if (loadMaterial) {
                    var nearestMaterials = [];
                    $.each(CustomShoe.adjacentProperties[property.type.toLowerCase()], function(i, val) {
                        var material = {
                            matId: ShoeModel[val].matId,
                            clrId: ShoeModel[val].clrId,
                            clrCode: ShoeModel[val].color,
                            matCode: ShoeModel[val].material,
                            matFolder: ShoeModel[val].matfolder
                        };
                        if (val !== 'quarter') {
                            if (ShoeModel[val].isMaterial == 1) {
                                nearestMaterials.push(material);
                            }
                        } else {
                            nearestMaterials.push(material);
                        }
                        if (parseInt(i) === parseInt(CustomShoe.adjacentProperties[property.type.toLowerCase()].length - 1)) {
                            $.ajax({
                                type: "POST",
                                url: baseUrl + "customshoe/unsmooth_property",
                                data: {
                                    lsfolder: ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/",
                                    materials: nearestMaterials,
                                    base: base,
                                    part: part,
                                    stylecode: ShoeModel.style.code
                                },
                                dataType: 'json',
                                success: function(foundMat) {
                                    ShoeModel[property.type.toLowerCase()].matId = foundMat.matId;
                                    ShoeModel[property.type.toLowerCase()].clrId = foundMat.clrId;
                                    ShoeModel[property.type.toLowerCase()].material = foundMat.matCode;
                                    ShoeModel[property.type.toLowerCase()].matfolder = foundMat.matFolder;
                                    ShoeModel[property.type.toLowerCase()].color = foundMat.clrCode;
                                    ModelRenderer.render(2);
                                }
                            });
                        }
                    });
                } else {
                    ModelRenderer.render(2);
                }
            }
        },
        renderProperties: function(renderComplete) {
            var idx = 0;
            $.each(CustomShoe.properties, function(j, val) {
                CustomShoe.preloadImg(val, function(succArr, errArr) {
                    $.each(succArr, function(i, img) {
                        var $li = $("<li>");
                        var $img = $("<img>");
                        $img.attr("height", '80');
                        $img.attr("src", img.url);
                        $img.attr("data-key", j);
                        $img.attr("data-idx", img.arridx);
                        $img.attr("data-id", img.id);
                        $img.click(CustomShoe.stepTwo.propertySelected);
                        $li.append($img);
                        if (j === "Foxing") {
                            $("#foxing_styling").append($li);
                        } else if (j === "Toe") {
                            $("#toe_styling").append($li);
                        } else if (j === "Eyestay") {
                            $("#eyestays_styling").append($li);
                        } else if (j === "Vamp") {
                            $("#vamp_styling").append($li);
                        }
                        if (i === (succArr.length - 1)) {
                            if (idx === (Object.keys(CustomShoe.properties).length - 1)) {
                                renderComplete();
                            }
                            idx++;
                        }
                    });
                });
            });
        },
        populateProperties: function(properties, callBack) {
            var imgBase = imageBase + ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/properties/";
            var items = {};
            var idx = 0;
            $.each(properties, function(k, property) {
                items[k] = [];
                $.each(property, function(i, val) {
                    items[k].push({type: k, idx: i, id: val[k + "Id"], code: val.code, name: val.name, isMaterial: val.isMaterial, isNothing: val.isNothing, url: imgBase + k + "/" + val.image});
                    if (i === (property.length - 1)) {
                        if (idx === (Object.keys(properties).length - 1)) {
                            CustomShoe.properties = items;
                            callBack();
                        }
                        idx++;
                    }
                });
            });
        },
        nextStep: function() {
            $.ajax({
                type: "POST",
                url: baseUrl + "customshoe/saveStepDetails",
                data: {
                    step_data: ShoeModel
                },
                dataType: 'json',
                success: function() {
                    window.location.href = baseUrl + "create-a-custom-shoe/materials-and-colors";
                }
            });
        }
    },
    stepThree: {
        previousMaterialIndex: 0,
        init: function(stepData) {
            ShoeModel = stepData;
            $(".createNewNav a.active").parents("li").nextAll().each(function() {
                $(this).find("a").attr("href", "javascript:void(0);");
                $(this).find("a").addClass("no-bg");
            });
            $('.step_three').on("click touchstart", ".topSliderRight img", function() {
                $('#img_wrap').html('');
                $(this).clone().appendTo('#img_wrap');
                $("#img_wrap").next("span").find(".bold").text(CustomShoe.allMaterials[$(this).attr("data-mat")].colors[$(this).attr("data-col")].name);
                $("#img_wrap").next("span").find(".mat-name").text(CustomShoe.allMaterials[$(this).attr("data-mat")].name);

                var overTopPos = $(this).offset().top;
                var overLeftPos = $(this).offset().left;
                $elem = $('.material');
                var centerWPos = $(window).width() / 2;
                if (centerWPos < overLeftPos) {
                    var normalDivWidth = $('#topSliderRight li').width() + 4;
                    var hoverDivWidth = $('.material').outerWidth() - 44;
                    var newLeftPos = (overLeftPos - hoverDivWidth) + normalDivWidth;
                    $elem.css({'top': overTopPos + 110, 'left': newLeftPos});
                } else {
                    $elem.css({'top': overTopPos + 110, 'left': overLeftPos});
                }
                $('.material').removeClass('hidden');
            });
            $('.step_three').on("mouseleave", ".topSliderRight img", function() {
                $('.material').addClass('hidden');
                $('#img_wrap').children().remove();
            });
            $("#steptwosubmit").off('click touchend').on('click touchend', function() {
                CustomShoe.stepThree.nextStep();
            });

            $("#shape_visualization").on('mousemove', "img", function(ev) {
                if (!$('div#tooltip').length) {
                    $('.visualImgWrap img').css({'cursor': 'default'});
                }
                var height = 438;
                var width = 779;
                var imgPos = $(this).offset(),
                        mousePos = {x: ev.pageX - parseInt(imgPos.left), y: ev.pageY - parseInt(imgPos.top)};
                var flag = false;
                var j;
                $("#shape_visualization img").each(function(i, val) {
                    j = i;
                    if ($(this).attr('id') !== "error") {
                        if ($(this).is(":visible")) {
                            var canvas = $('#democan')[0];
                            canvas.width = canvas.width;
                            var ctx = canvas.getContext('2d');
                            ctx.drawImage($('#' + $(this).attr('id'))[0], 0, 0, 720, 405, 0, 0, width, height);
                            var alpha = ctx.getImageData(mousePos.x, mousePos.y, 1, 1).data[3];
                            var tper = ((100 * alpha / 255) << 0);

                            if (tper === 0) {
                                $('div#tooltip').remove();
                            }
                            else {
                                var text = $(this).attr('id');
                                if (text === 'base') {
                                    flag = true;
                                } else {
                                    CustomShoe.stepTwo.partTouched(ev, text);
                                    flag = false;
                                    return false;
                                }

                            }
                        }
                    }


                });
                if (j === $("#shape_visualization img").length - 1 && flag) {
                    $('#style_path').css('z-index', 8);
                    elem = document.elementFromPoint(ev.pageX, ev.pageY);
                    if (elem !== null && $(elem).attr('id') !== undefined) {
                                               CustomShoe.stepTwo.partTouched(ev, $(elem).attr('id').replace('path_', ''));
                    }
                    $('#style_path').css('z-index', 0);
                }

            });
            $("#shape_visualization").on('mouseleave', "img", function(ev) {
                $('div#tooltip').remove();
                $('.visualImgWrap img').css({'cursor': 'default'});
            });
            $("#shape_visualization").off('click tap').on('click tap', "img", function(ev) {
                var height = 438;
                var width = 779;
                var imgPos = $(this).offset(),
                        mousePos = {x: ev.pageX - parseInt(imgPos.left), y: ev.pageY - parseInt(imgPos.top)};
                $("#shape_visualization img").each(function(i, val) {
                    if ($(this).attr('id') === "toe" || $(this).attr('id') === "vamp" || $(this).attr('id') === "eyestay" || $(this).attr('id') === "foxing") {
                        if ($(this).is(":visible")) {
                            var canvas = $('#democan')[0];
                            canvas.width = canvas.width;
                            var ctx = canvas.getContext('2d');
                            ctx.drawImage($('#' + $(this).attr('id'))[0], 0, 0, 720, 405, 0, 0, width, height);
                            var alpha = ctx.getImageData(mousePos.x, mousePos.y, 1, 1).data[3];
                            var tper = ((100 * alpha / 255) << 0);
                            if (tper > 0) {
                                $(".topSliderInner").hide();
                                $("#" + $(this).attr('id') + "_matco").show();
                                $("#" + $(this).attr('id') + "_matco").find(".topSliderRight").hide();
                                $("#" + $(this).attr('id') + "_materials").find("li a").eq(CustomShoe.stepThree.previousMaterialIndex).click();
                                CustomShoe.stepThree.showPopup();
                                return false;
                            }
                        }
                    }
                    if (i === ($("#shape_visualization img").length - 1)) {
                        $(".topSliderInner").hide();
                        $("#quarter_matco").show();
                        $("#quarter_matco").find(".topSliderRight").hide();
                        $("#quarter_materials").find("li a").eq(CustomShoe.stepThree.previousMaterialIndex).click();
                        CustomShoe.stepThree.showPopup();
                    }
                });
            });
            $("#shape_visualization img").error(function() {
                if ($(this).attr("id") === "base") {
                    ModelRenderer.errorRender();
                } else {
                    ModelRenderer.partNotAvailable($(this).attr("id"));
                }
            });
            $("a.rotate.left").click(function() {
                var angle = parseInt(ShoeModel.angle.replace('A', ''));
                if (angle === 0) {
                    ShoeModel.angle = "A7";
                } else {
                    angle = angle - 1;
                    ShoeModel.angle = "A" + angle;
                }
                ModelRenderer.render(2);
            });
            $("a.top").click(function() {
                ShoeModel.angle = "A7";
                ModelRenderer.render(2);
            });
            $("a.rotate.right").click(function() {
                var angle = parseInt(ShoeModel.angle.replace('A', ''));
                if (angle === 7) {
                    ShoeModel.angle = "A0";
                } else {
                    angle = angle + 1;
                    ShoeModel.angle = "A" + angle;
                }
                ModelRenderer.render(2);
            });
            document.onkeydown = function(ev) {
                switch (ev.keyCode) {
                    case 37://left
                        var angle = parseInt(ShoeModel.angle.replace('A', ''));
                        if (angle === 0) {
                            ShoeModel.angle = "A7";
                        } else {
                            angle = angle - 1;
                            ShoeModel.angle = "A" + angle;
                        }
                        ModelRenderer.render(2);
                        break;
                    case 38://up
                        break;
                    case 39://right
                        var angle = parseInt(ShoeModel.angle.replace('A', ''));
                        if (angle === 7) {
                            ShoeModel.angle = "A0";
                        } else {
                            angle = angle + 1;
                            ShoeModel.angle = "A" + angle;
                        }
                        ModelRenderer.render(2);
                        break;
                    case 40://down
                        break;
                }
            };
            CustomShoe.stepThree.loadAttributes(function() {
                CustomShoe.stepTwo.initCarousel('.jHcarousel');
                ModelRenderer.render(2);
                //CustomShoe.stepThree.showPopup();
                $("body").css("overflow", "hidden");
                $('#status').fadeOut(); // will first fade out the loading animation
                $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                $('body').delay(350).css({'overflow': 'visible'});
            });
        },
        loadAttributes: function(loadComplete) {
            $.ajax({
                type: "POST",
                url: baseUrl + "customshoe/styleattributes",
                dataType: 'JSON',
                success: function(attributes) {
                    CustomShoe.allMaterials = attributes.allMaterials;
                    CustomShoe.propertyStitches = attributes.propertyStitches;
                    CustomShoe.propertyLaces = attributes.propertyLaces;
                    CustomShoe.stepThree.populateMaterials(attributes.propertyMaterials, function() {
                        CustomShoe.stepThree.populateStitches(function() {
                            CustomShoe.stepThree.populateLaces(loadComplete);
                        });
                    });
                }
            });
        },
        materialSelected: function() {
            var material = CustomShoe.allMaterials[$(this).attr("data-mat")];
            var color = material['colors'][$(this).attr("data-col")];
            if ($(this).attr("data-type") === "quarter") {
                ShoeModel.quarter.material = material.code;
                ShoeModel.quarter.matfolder = material.folder;
                ShoeModel.quarter.color = color.code;
                ShoeModel.quarter.matId = $(this).attr("data-mat");
                ShoeModel.quarter.clrId = $(this).attr("data-col");
            } else if ($(this).attr("data-type") === "toe") {
                ShoeModel.toe.material = material.code;
                ShoeModel.toe.matfolder = material.folder;
                ShoeModel.toe.color = color.code;
                ShoeModel.toe.matId = $(this).attr("data-mat");
                ShoeModel.toe.clrId = $(this).attr("data-col");
            } else if ($(this).attr("data-type") === "eyestay") {
                ShoeModel.eyestay.material = material.code;
                ShoeModel.eyestay.matfolder = material.folder;
                ShoeModel.eyestay.color = color.code;
                ShoeModel.eyestay.matId = $(this).attr("data-mat");
                ShoeModel.eyestay.clrId = $(this).attr("data-col");
            } else if ($(this).attr("data-type") === "foxing") {
                ShoeModel.foxing.material = material.code;
                ShoeModel.foxing.matfolder = material.folder;
                ShoeModel.foxing.color = color.code;
                ShoeModel.foxing.matId = $(this).attr("data-mat");
                ShoeModel.foxing.clrId = $(this).attr("data-col");
            } else if ($(this).attr("data-type") === "vamp") {
                ShoeModel.vamp.material = material.code;
                ShoeModel.vamp.matfolder = material.folder;
                ShoeModel.vamp.color = color.code;
                ShoeModel.vamp.matId = $(this).attr("data-mat");
                ShoeModel.vamp.clrId = $(this).attr("data-col");
            }
            ModelRenderer.render(2);
        },
        stitchSelected: function() {
            $(this).parents('.dropdown').removeClass('dropdown');
            ShoeModel.stitch.code = $(this).attr("data-code");
            ShoeModel.stitch.name = $(this).find("sapn").eq(0).text();
            ShoeModel.stitch.id = $(this).attr("data-id");
            ModelRenderer.render(2);
        },
        laceSelected: function() {
            $(this).parents('.dropdown').removeClass('dropdown');
            ShoeModel.lace.code = $(this).attr("data-code");
            ShoeModel.lace.name = $(this).find("sapn").eq(0).text();
            ShoeModel.lace.id = $(this).attr("data-id");
            ModelRenderer.render(2);
        },
        populateMaterials: function(propertyMaterials, callback) {
            /*
             * <li>
             <a id="matco_quarter" href="javascript:;"><span>Quarter</span></a>
             </li>
             */
            $("#shape_selection_popup .topSliderInner").hide();
            var idx = 0;
            if (Object.keys(propertyMaterials).length > 0) {
                $.each(propertyMaterials, function(key, mat) {
                    if (Object.keys(mat).length > 0) {
                        $.each(mat, function(j, val) {
                            if (val.colors.length > 0) {
                                var $li, $img, $mli, $ma, $rdiv, $rul;
                                $mli = $("<li>");
                                $ma = $("<a>");
                                $ma.attr("href", "javascript:;");
                                $ma.attr("data-mid", val.material_id);
                                $ma.attr("data-cat", key);
                                $ma.click(function() {
                                    $(".topSliderRight").hide();
                                    $(".styleList a.active").removeClass("active");
                                    var key = $(this).data('cat');
                                    var id = $(this).data('mid');
                                    $(this).addClass("active");
                                    $("#" + key + "_colors_" + id).show();
                                    CustomShoe.stepThree.previousMaterialIndex = parseInt(j);
                                });
                                $ma.text(val.material_name);
                                $mli.append($ma);
                                $rdiv = $("<div>");
                                $rdiv.attr("id", key + "_colors_" + val.material_id);
                                $rdiv.addClass("topSliderRight");
                                $rdiv.hide();
                                $rul = $("<ul>");
                                $rul.addClass("img-style jHcarousel");
                                $("#" + key + "_materials").append($mli);
                                $rdiv.append($rul);
                                $("#" + key + "_matco").append($rdiv);
                                CustomShoe.preloadImg(val.colors, function(succArr, errArr) {
                                    if (succArr.length > 0) {
                                        $.each(succArr, function(i, img) {
                                            $li = $("<li>");
                                            $img = $("<img>");
                                            $img.attr("src", img.url);
                                            $img.attr("data-mat", img.matId);
                                            $img.attr("data-col", img.clrId);
                                            $img.attr("data-type", key);
                                            $img.click(CustomShoe.stepThree.materialSelected);
                                            $li.append($img);
                                            //$("#" + key + "_matco").append($li);
                                            $("#" + key + "_colors_" + val.material_id + " ul").append($li);
                                            if (i === (succArr.length - 1)) {
                                                if (parseInt(j) === parseInt(Object.keys(mat)[(Object.keys(mat).length - 1)])) {
                                                    if (idx === (Object.keys(propertyMaterials).length - 1)) {
                                                        callback();
                                                    }
                                                    idx++;
                                                }
                                            }
                                        });
                                    } else {
                                        if (parseInt(j) === parseInt(Object.keys(mat)[(Object.keys(mat).length - 1)])) {
                                            if (idx === (Object.keys(propertyMaterials).length - 1)) {
                                                callback();
                                            }
                                            idx++;
                                        }
                                    }
                                });
                            } else {
                                if (parseInt(j) === parseInt(Object.keys(mat)[(Object.keys(mat).length - 1)])) {
                                    if (idx === (Object.keys(propertyMaterials).length - 1)) {
                                        callback();
                                    }
                                    idx++;
                                }
                            }
                        });
                    } else {
                        if (idx === (Object.keys(propertyMaterials).length - 1)) {
                            callback();
                        }
                        idx++;
                    }
                });
            } else {
                callback();
            }
            //callback();
        },
        populateStitches: function(callback) {
            if (CustomShoe.propertyStitches.length > 0) {
                var $sli, $selem, $sspan;
                $("#stitching_color").children().remove();
                $.each(CustomShoe.propertyStitches, function(i, stitch) {
                    $sli = $("<li>");
                    $selem = $("<div>").addClass("color-elem");
                    $sspan = $("<span>").text(stitch.stitch_name);
                    $selem.css("background", stitch.stitch_color);
                    $sli.attr("data-id", stitch.stitch_id);
                    $sli.attr("data-code", stitch.stitch_code);
                    $sli.append($selem);
                    $sli.append($sspan);
                    $sli.click(CustomShoe.stepThree.stitchSelected);
                    $("#stitching_color").append($sli);
                    if (i === (CustomShoe.propertyStitches.length - 1)) {
                        callback();
                    }
                });
            } else {
                callback();
            }
        },
        populateLaces: function(callback) {
            var $lli, $lelem, $lspan;
            $("#lacing_color").children().remove();
            if (CustomShoe.propertyLaces.length > 0) {
                $.each(CustomShoe.propertyLaces, function(i, lace) {
                    $lli = $("<li>");
                    $lelem = $("<div>").addClass("color-elem");
                    $lspan = $("<span>").text(lace.lace_name);
                    $lelem.css("background", lace.color_code);
                    $lli.attr("data-id", lace.lace_id);
                    $lli.attr("data-code", lace.lace_code);
                    $lli.append($lelem);
                    $lli.append($lspan);
                    $lli.click(CustomShoe.stepThree.laceSelected);
                    $("#lacing_color").append($lli);
                    if (i === (CustomShoe.propertyLaces.length - 1)) {
                        callback();
                    }
                });
            } else {
                $("#lacing").hide();
                callback();
            }
        },
        closePopup: function() {


            $(".colors_popup").animate({top: "-40"}, 1200, function() {
                //$(".colors_popup").hide();
            });
            $('.designStep .shape_popup').animate({height: "0"}, 1200, function() {

                $('.designStep .shape_selection_popup').hide();
                $('.designStep .shape_selection_popup').removeClass("popup_alive");
                //$('.designStep #stepMain').css({'position':'fixed','top':'50'});
            });


        },
        nextStep: function() {
            CustomShoe.saveModel(function() {
                window.location.href = baseUrl + "create-a-custom-shoe/monogram";
            });
        },
        prevStep: function() {
            CustomShoe.saveModel(function() {
                window.location.href = baseUrl + "create-a-custom-shoe/design-features";
            });
        },
        showPopup: function() {
            var eTop = $('#stepFlowBar').offset().top;
            $('.shape_selection_popup').css({'margin-top': eTop - $(window).scrollTop()});
            $('.shape_selection_popup').find('colorsMenuWrap').css({'margin-top': eTop - $(window).scrollTop()});

            $('#topSlider').animate({left: "0"}, 1200);
            $('.toe_selection').css("display", "block");
            $("html, body").animate({scrollTop: "0"}, 500, function() {
                var $selectionPopup = $(".shape_popup");
                $selectionPopup.css("opacity", "0");
                $selectionPopup.css({
                    'display': 'block',
                    'left': $(window).width() / 2,
                    'margin-left': '-' + ($selectionPopup.width() / 2) + 'px'
                }).addClass('popup_alive');
                $selectionPopup.css("opacity", "1");
                $selectionPopup.animate({height: "126"}, 1200);

                var $colorsPopup = $(".colors_popup");
                $colorsPopup.css("opacity", "0");
                $colorsPopup.css({
                    'display': 'block',
                    'left': $(window).width() / 2,
                    'margin-left': '-' + ($colorsPopup.width() / 2) + 'px'
                }).addClass('popup_alive');
                $colorsPopup.css("opacity", "1");
                $colorsPopup.animate({top: "85"}, 1100);
            });
        }
    },
    stepFour: {
        init: function(stepData) {
            ShoeModel = stepData;
            $(".createNewNav a.active").parents("li").nextAll().each(function() {
                $(this).find("a").attr("href", "javascript:void(0);");
                $(this).find("a").addClass("no-bg");
            });
            $(".step_four .view").click(function() {
                CustomShoe.stepFour.showPopup();
            });
            $(".sfoursave").click(function() {
                CustomShoe.stepFour.validate();
            });
            $("a.rotate.left").click(function() {
                var angle = parseInt(ShoeModel.angle.replace('A', ''));
                if (angle === 0) {
                    ShoeModel.angle = "A7";
                } else {
                    angle = angle - 1;
                    ShoeModel.angle = "A" + angle;
                }
                ModelRenderer.render(2);
            });
            $("a.top").click(function() {
                ShoeModel.angle = "A7";
                ModelRenderer.render(2);
            });
            $("a.rotate.right").click(function() {
                var angle = parseInt(ShoeModel.angle.replace('A', ''));
                if (angle === 7) {
                    ShoeModel.angle = "A0";
                } else {
                    angle = angle + 1;
                    ShoeModel.angle = "A" + angle;
                }
                ModelRenderer.render(2);
            });
            document.onkeydown = function(ev) {
                switch (ev.keyCode) {
                    case 37://left
                        var angle = parseInt(ShoeModel.angle.replace('A', ''));
                        if (angle === 0) {
                            ShoeModel.angle = "A7";
                        } else {
                            angle = angle - 1;
                            ShoeModel.angle = "A" + angle;
                        }
                        ModelRenderer.render(2);
                        break;
                    case 38://up
                        break;
                    case 39://right
                        var angle = parseInt(ShoeModel.angle.replace('A', ''));
                        if (angle === 7) {
                            ShoeModel.angle = "A0";
                        } else {
                            angle = angle + 1;
                            ShoeModel.angle = "A" + angle;
                        }
                        ModelRenderer.render(2);
                        break;
                    case 40://down
                        break;
                }
            };
            $("#shape_visualization img").error(function() {
                if ($(this).attr("id") === "base") {
                    ModelRenderer.errorRender();
                } else {
                    ModelRenderer.partNotAvailable($(this).attr("id"));
                }
            });
            if (typeof ShoeModel.size.left !== "undefined" && ShoeModel.size.left.value !== "0") {
                $("#leftlength").val(ShoeModel.size.left.value);
                $("#left_shoe .dropdown .selected").html(ShoeModel.size.left.value);
            }
            if (typeof ShoeModel.size.right !== "undefined" && ShoeModel.size.right.value !== "0") {
                $("#rightlength").val(ShoeModel.size.right.value);
                $("#right_shoe .dropdown .selected").html(ShoeModel.size.right.value);
            }

            if (ShoeModel.size.left.text !== 0 && ShoeModel.size.right.text !== 0) {
                $('#dk_container_rightlength .dk_options_inner li').removeClass('dk_option_current');
                $('#dk_container_rightlength .dk_options_inner li a[data-dk-dropdown-value="' + ShoeModel.size.right.value + '"]').parent().addClass('dk_option_current');
                $('#dk_container_rightlength .dk_toggle .dk_label').text(ShoeModel.size.right.text);

                $('#rightlength option').removeAttr('selected');
                $('#rightlength option[value="' + ShoeModel.size.right.value + '"]').attr('selected', true);


                $('#dk_container_leftlength .dk_options_inner li').removeClass('dk_option_current');
                $('#dk_container_leftlength .dk_options_inner li a[data-dk-dropdown-value="' + ShoeModel.size.left.value + '"]').parent().addClass('dk_option_current');
                $('#dk_container_leftlength .dk_toggle .dk_label').text(ShoeModel.size.left.text);


                $('#leftlength option').removeAttr('selected');
                $('#leftlength option[value="' + ShoeModel.size.left.value + '"]').attr('selected', true);
            }
            $('#middle_container').click(function(e) {
                if ($('#shape_selection_popup:visible').length != 0)
                {
                    CustomShoe.stepFour.closePopup();
                    //e.preventDefault();
                }
            });
            ModelRenderer.render(3);
        },
        showPopup: function() {
            $("html, body").animate({scrollTop: "0"}, 500, function() {
                var $selectionPopup = $(".shape_popup");
                $selectionPopup.css("opacity", "0");
                $selectionPopup.css({
                    'display': 'block',
                    'left': $(window).width() / 2,
                    'margin-left': '-' + ($selectionPopup.width() / 2) + 'px',
                    'margin-top': 110 + 'px'
                }).addClass('popup_alive');
                $selectionPopup.css("opacity", "1");
                $selectionPopup.animate({height: "444"}, 1200);
            });
        },
        closePopup: function() {
            $(".colors_popup").animate({top: "-53"}, 1350);
            $('.designStep .shape_popup').animate({height: "0"}, 1200, function() {
                $('.designStep .shape_selection_popup').hide();
                $('.designStep .shape_selection_popup').removeClass("popup_alive");
                $('.designStep #stepMain').css({'position': 'fixed', 'top': '50'});
            });
        },
        validate: function() {
            if ($("#rightlength").val() === null || $("#leftlength").val() === null || $("#leftlength").val() === 'all' || $("#leftlength").val() === 'all') {
                alert("Please select your sizes.");
                return false;
            } else {
                if ($("#rightlength").val() === $("#leftlength").val()) {
                    CustomShoe.stepFour.nextStep();
                }
            }
            if ($("#rightlength").val() !== $("#leftlength").val()) {
                messagepopup('error');
            }

        },
        nextStep: function(url) {
            var rsize = $('#rightlength option:selected').val();
            var lsize = $('#leftlength option:selected').val();

            ShoeModel.size.left.value = lsize;
            ShoeModel.size.left.text = lsize;
            ShoeModel.size.right.value = rsize;
            ShoeModel.size.right.text = rsize;

            CustomShoe.saveModel(function() {
                var dsid = "";
                if (typeof ShoeModel.shoeDesignId !== "undefined") {
                    dsid = ShoeModel.shoeDesignId;
                }
                if (typeof url === 'undefined') {
                    ShoeCart.addDesign(dsid, function() {
                        window.location.href = baseUrl + "custom-shoe/order";
                    });
                } else {
                    window.location.href = baseUrl + "customshoe/" + url;
                }
            });
        },
        saveDesign: function() {
            var callback;
            if (typeof arguments[0] === "function") {
                callback = arguments[0];
            }
            $.ajax({
                type: "POST",
                url: baseUrl + "customshoe/saveDesign",
                data: {
                    shoemodel: ShoeModel
                },
                dataType: 'json',
                success: function(response) {
                    if (!response.isValidLogin) {
                        messagepopup('save');
                    } else {
                        ShoeModel = response.shoe;
                        if (typeof callback !== "undefined") {
                            callback(response);
                        } else {
                            alert('Your design successfully saved!');
                        }
                    }
                }
            });
        },
        renameShare: function() {
            CustomShoe.stepFour.saveDesign(function(data) {
                var href = $('#twitter').attr('href');
                var new_href = href.replace('##', ShoeModel.shoeDesignId);
                $('#twitter').attr('href', new_href);

                href = $('#fb').attr('href');
                new_href = href.replace('##', ShoeModel.shoeDesignId);
                $('#fb').attr('href', new_href);

                href = $('#pinterest').attr('href');
                new_href = href.replace('##', data.details.image);
                $('#pinterest').attr('href', new_href);

                $(".messageBox_rename .popup_shoe_image img").attr("src", designBase + data.details.image + "_A0.png");
                messagepopup('rename');
            });
        },
        updateName: function(share) {
            var elem = $('#shoe_name_input').val();
            if (elem === '') {
                alert('Shoe name can not be empty');
                return;
            }
            $.ajax({
                type: "POST",
                url: baseUrl + "customshoe/updatedesign",
                data: {
                    name: elem,
                    share: share,
                    designId: ShoeModel.shoeDesignId
                },
                success: function(response) {
                    if (response == "success") {
                        if (share === "no") {
                            $('.edit_last').hide();
                            $('.edit_first').show();

                            $('.shoe_name').text(elem);
                        } else {
                            $('.shoe_rename_box').hide();
                            $('.shoe_share_box').show();
                        }
                    } else {
                        alert('Shoe name is already added.');
                        elem.focus();
                        return false;
                    }
                }
            });
        }
    },
    stepFive: {
        init: function(stepdata, redirectUrl) {
            ShoeModel = stepdata;
            $("a.rotate.left").off('click touchend').on('click touchend', function() {
                var angle = parseInt(ShoeModel.angle.replace('A', ''));
                if (angle === 0) {
                    ShoeModel.angle = "A7";
                } else {
                    angle = angle - 1;
                    ShoeModel.angle = "A" + angle;
                }
                ModelRenderer.render(2);
            });
            $("a.top").off('click touchend').on('click touchend', function() {
                ShoeModel.angle = "A7";
                ModelRenderer.render(2);
            });
            $("a.rotate.right").off('click touchend').on('click touchend', function() {
                var angle = parseInt(ShoeModel.angle.replace('A', ''));
                if (angle === 7) {
                    ShoeModel.angle = "A0";
                } else {
                    angle = angle + 1;
                    ShoeModel.angle = "A" + angle;
                }
                ModelRenderer.render(2);
            });
            $("#shape_visualization img").error(function() {
                if ($(this).attr("id") === "base") {
                    ModelRenderer.errorRender();
                } else {
                    ModelRenderer.partNotAvailable($(this).attr("id"));
                }
            });


            // CustomShoe.stepFive.showPopup('monogram', redirectUrl);


            ShoeModel.monogram.leftShoe = 1;
            ShoeModel.monogram.leftSide = 1;
            ShoeModel.angle = "A4";
            if (ShoeModel.monogram.text === '') {
                $('.add_monogram').show();
                $('.remove_monogram').hide();
            } else {
                $('.add_monogram').hide();
                $('.remove_monogram').show();
            }
            ModelRenderer.render(2);
            document.onkeydown = function(ev) {
                switch (ev.keyCode) {
                    case 37://left
                        var angle = parseInt(ShoeModel.angle.replace('A', ''));
                        if (angle === 0) {
                            ShoeModel.angle = "A7";
                        } else {
                            angle = angle - 1;
                            ShoeModel.angle = "A" + angle;
                        }
                        ModelRenderer.render(2);
                        break;
                    case 38://up
                        break;
                    case 39://right
                        var angle = parseInt(ShoeModel.angle.replace('A', ''));
                        if (angle === 7) {
                            ShoeModel.angle = "A0";
                        } else {
                            angle = angle + 1;
                            ShoeModel.angle = "A" + angle;
                        }
                        ModelRenderer.render(2);
                        break;
                    case 40://down
                        break;
                }
            };
//            $('.visualImgWrap img').attr('ondragstart', "return false");
//            $('.visualImgWrap img').attr('ondrop', "return false");
//            var last_position = {},
//                    $output = $('.visualImgWrap');
//            $output.on('mousedown', function(evt) {
//                $output.on('mouseup mousemove', function handler(evt) {
//                    if (evt.type === 'mouseup') {
//                    } else {
//                        if (typeof(last_position.x) != 'undefined') {
//                            var deltaX = last_position.x - evt.clientX,
//                                    deltaY = last_position.y - evt.clientY;
//                            if (Math.abs(deltaX) > Math.abs(deltaY) && deltaX > 0) {//right
//                                var angle = parseInt(ShoeModel.angle.replace('A', ''));
//                                if (angle === 7) {
//                                    ShoeModel.angle = "A0";
//                                } else {
//                                    angle = angle + 1;
//                                    ShoeModel.angle = "A" + angle;
//                                }
//                                ModelRenderer.render(2);
//
//                            } else if (Math.abs(deltaX) > Math.abs(deltaY) && deltaX < 0) {//left
//                                var angle = parseInt(ShoeModel.angle.replace('A', ''));
//                                if (angle === 0) {
//                                    ShoeModel.angle = "A7";
//                                } else {
//                                    angle = angle - 1;
//                                    ShoeModel.angle = "A" + angle;
//                                }
//                                ModelRenderer.render(2);
//                            }
//                        }
//                    }
//                    $output.off('mouseup mousemove', handler);
//                });
//                last_position = {
//                    x: evt.clientX,
//                    y: evt.clientY
//                };
//            });
        },
        showPopup: function(name, redirectUrl) {
            //alert(redirectUrl);

            $(".messageBox_monogram .msgTop div.msgHead").hide();
            $(".messageBox_monogram .msgBottom div.msgOptionWrap").hide();
            if (redirectUrl == baseUrl + 'create-a-custom-shoe/sizing') {
                CustomShoe.stepFive.nextSelection('#msgOption1', '#msgHead1');
            } else {
                $("#msgHead1").show();
                $("#msgOption1").show();
            }
            if (typeof ShoeModel.monogram === "undefined" || ShoeModel.monogram.text === "undefined") {
                delete ShoeModel['monogram'];
                messagepopup(name);
            } else {
                $('#size_box').val(ShoeModel.monogram.text);
                $('.add_monogram').hide();
                $('.remove_monogram').show();

            }

        },
        selectShoe: function() {
            if ($("#msgOption2 .checkbox input:checked").length > 0) {
                ShoeModel.monogram = {
                    leftShoe: 0,
                    rightShoe: 0,
                    leftSide: 0,
                    rightSide: 0,
                    text: ''
                };
                $("#msgOption2 .checkbox input:checked").each(function() {
                    if ($(this).val() === MonogramConfig.leftShoe) {
                        ShoeModel.monogram.leftShoe = 1;
                    } else if ($(this).val() === MonogramConfig.rightShoe) {
                        ShoeModel.monogram.rightShoe = 1;
                    }
                });
                CustomShoe.stepFive.nextSelection('#msgOption2', '#msgHead2');
            } else {
                alert("Please select a shoe");
            }
        },
        selectSide: function() {
            if ($("#msgOption3 .checkbox input:checked").length > 0) {
                $("#msgOption3 .checkbox input:checked").each(function() {
                    if ($(this).val() === MonogramConfig.leftSide) {
                        ShoeModel.monogram.leftSide = 1;
                    } else if ($(this).val() === MonogramConfig.rightSide) {
                        ShoeModel.monogram.rightSide = 1;
                    }
                });
                if ($("#msgOption3 .checkbox input:checked").length === 1 && $("#msgOption3 .checkbox input:checked").val() === MonogramConfig.rightSide && ShoeModel.angle !== "A0") {
                    ShoeModel.angle = "A0";
                    ModelRenderer.render(2);
                } else if ($("#msgOption3 .checkbox input:checked").length === 1 && $("#msgOption3 .checkbox input:checked").val() === MonogramConfig.leftSide && ShoeModel.angle !== "A4") {
                    ShoeModel.angle = "A4";
                    ModelRenderer.render(2);
                }
                messagepopupclose();
            } else {
                alert("Please select side");
            }
        },
        addText: function(e) {
            if ($('#size_box').val().trim() !== "") {
                var regex = /^[a-zA-Z ]*$/;
                if (regex.test($('#size_box').val())) {
                    ShoeModel.monogram.text = $('#size_box').val().toUpperCase();
                    if (typeof e !== "undefined") {
                        if (e.type !== "keyup") {
                            $('.add_monogram').hide();
                            $('.remove_monogram').show();
                        }
                    } else {
                        $('.add_monogram').hide();
                        $('.remove_monogram').show();
                    }

                    ModelRenderer.render(2);
                } else {
                    alert("Alphabets Only");
                }
            } else {
                if (typeof e !== "undefined") {
                    if (e.type !== "keyup") {
                        alert("Monogram cannot be empty");
                    }
                    else {
                        ShoeModel.monogram.text = '';
                        ModelRenderer.render(2);
                    }
                } else {
                    alert("Monogram cannot be empty");
                }

            }
        },
        addedBack: function() {
            $('.add_monogram').show();
            $('.remove_monogram').hide();
            $('#size_box').val(ShoeModel.monogram.text);
        },
        removeMonoNext: function() {
            ShoeModel.monogram.text = '';
            CustomShoe.stepFive.nextStep('stepfour');
        },
        nextStep: function(url) {
//            if (typeof url === "undefined" ){ //&& (typeof ShoeModel.monogram.text === "undefined" || ShoeModel.monogram.text.trim() === "")) {
//                alert("Please enter a text for monogram.");
//            } else {
            CustomShoe.saveModel(function() {
                if (typeof url === 'undefined') {
                    window.location.href = baseUrl + "create-a-custom-shoe/sizing";
                } else {
                    window.location.href = baseUrl + "customshoe/" + url;
                }
            });
//            }
        },
        nextSelection: function(content, title) {
            $(content).hide();
            $(title).hide();
            $(content).next().show();
            $(title).next().show();
        },
        prevSelection: function(content, title) {
            $(content).hide();
            $(title).hide();
            $(content).prev().show();
            $(title).prev().show();
        }
    },
    preloadImg: function(arr, fn) {
        var errImgs = [];
        var succImgs = [];
        var $img;
        $.each(arr, function(i, val) {
            $img = $("<img>");
            val.arridx = i;
            $img.load(function() {
                succImgs.push(val);
                if ((succImgs.length + errImgs.length) === arr.length) {
                    fn(succImgs, errImgs);
                }
            }).each(function() {
                if (this.complete) {
                    $(this).trigger('load');
                }
            });
            $img.error(function() {
                errImgs.push(val);
                if ((succImgs.length + errImgs.length) === arr.length) {
                    fn(succImgs, errImgs);
                }
            });
            $img[0].src = val.url;
        });
    }
};
