$(document).ready(function () {
    fbq('trackCustom', 'CustomShoeStep4');

    if (isTouch) {
        if (window.innerWidth <= 767) {
            ModelRenderer.config.isMobile = true;
            $(".lether-types li").contents().filter(function () {
                return this.nodeType == Node.TEXT_NODE;
            }).remove();
        }
    }
    
    CustomShoe.currentStep = "styling";
    ModelRenderer.container.svg = '';
    CustomShoe.handleTop();
    
    CustomShoe.loadModel(function (model) {
        ShoeModel = model;
        CustomShoe.styling.init();
        var tmpText = $("#quarter_lethers").find('a.active-leather').text();
        
        if (typeof tmpText !== 'undefined' && tmpText !== '') {
            $(".bottomHeading").text(tmpText);
        }
    });
});