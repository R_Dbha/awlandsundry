$(document).ready(function () {
    fbq('trackCustom', 'CustomShoeStep1');

    $("#custom_fitting_checkbox").change(customFittingClick);
    CustomShoe.currentStep = "sizing";
    CustomShoe.handleTop();

    CustomShoe.loadModel(function (model) {
        ShoeModel = model;
        CustomShoe.sizing.init();
    });

    $('.close').on('click', function () {
        $('body').css({'overflow': 'auto', 'position': 'static'});
        $('#email-popup-wrapper').hide();
        $('#width-popup-wrapper').hide();
        $('#sizing-popup-wrapper').hide();
        $('#measure-popup-wrapper').hide();
        $('#sizes-popup-wrapper').hide();
        $('.overlay').hide();
    });

    $('#s_w_det').click(function () {
        $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
        $('#sizing-popup-wrapper').hide();
        $('#sizes-popup-wrapper').hide();
        $('#width-popup-wrapper').show();
        $('.overlay').show();
    });

    $('#s_s_det').click(function () {
        $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
        $('#width-popup-wrapper').hide();
        $('#sizes-popup-wrapper').hide();
        $('#sizing-popup-wrapper').show();
        $('.overlay').show();
    });

    $('body').on('click', '.shoe-size-det', function () {
        $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
        $('#width-popup-wrapper').hide();
        $('#sizes-popup-wrapper').hide();
        $('#sizing-popup-wrapper').show();
        $('.overlay').show();
    });

    $('#enter-size .help_button').click(function () {
        $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
        $('#width-popup-wrapper').hide();
        $('#sizing-popup-wrapper').hide();
        $('#measure-popup-wrapper').show();
        $('.overlay').show();
    });

    $('#measure_help').click(function () {
        $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
        $('#measure-popup-wrapper').show();
        $('.overlay').show();
    });
    
    $("#left-size").on('change', function () {
        if (checkSizes() && !($("#custom_fitting_checkbox").is(":checked"))) {
            $("#custom_fitting_checkbox").prop("checked", true);
            $("#measurement-wrapper").css("display", "table");
        } else if (!checkSizes() && ($("#custom_fitting_checkbox").is(":checked"))) {
            $("#custom_fitting_checkbox").prop("checked", false);
            $("#measurement-wrapper").css("display", "none");
        }
    });

    $("#right-size").on('change', function () {
        if (checkSizes() && !($("#custom_fitting_checkbox").is(":checked"))) {
            $("#custom_fitting_checkbox").prop("checked", true);
            $("#measurement-wrapper").css("display", "table");
        } else if (!checkSizes() && ($("#custom_fitting_checkbox").is(":checked"))) {
            $("#custom_fitting_checkbox").prop("checked", false);
            $("#measurement-wrapper").css("display", "none");
        }
    });

    $("#left-width").on('change', function () {
        if (checkSizes() && !($("#custom_fitting_checkbox").is(":checked"))) {
            $("#custom_fitting_checkbox").prop("checked", true);
            $("#measurement-wrapper").css("display", "table");
        } else if (!checkSizes() && ($("#custom_fitting_checkbox").is(":checked"))) {
            $("#custom_fitting_checkbox").prop("checked", false);
            $("#measurement-wrapper").css("display", "none");
        }
    });

    $("#right-width").on('change', function () {
        if (checkSizes() && !($("#custom_fitting_checkbox").is(":checked"))) {
            $("#custom_fitting_checkbox").prop("checked", true);
            $("#measurement-wrapper").css("display", "table");
        } else if (!checkSizes() && ($("#custom_fitting_checkbox").is(":checked"))) {
            $("#custom_fitting_checkbox").prop("checked", false);
            $("#measurement-wrapper").css("display", "none");
        }
    });
});

var customFittingClick = function () {
    $("#measurement-wrapper").css("display", (this.checked) ? "table" : "none");
};

var checkSizes = function () {
    var temp_ls = $("#left-size").val();
    var temp_rs = $("#right-size").val();
    var temp_rw = $("#right-width").val();
    var temp_lw = $("#left-width").val();
    var result = false;
    if (temp_ls !== "" && ((temp_ls < 8) || (temp_ls > 12.5))) {
        result = true;
    }
    if (temp_rs !== "" && ((temp_rs < 8) || (temp_rs > 12.5))) {
        result = true;
    }
    if (temp_lw !== "" && temp_lw !== "Regular") {
        result = true;
    }
    if (temp_rw !== "" && temp_rw !== "Regular") {
        result = true;
    }
    return result;
};