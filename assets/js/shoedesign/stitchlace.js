$(document).ready(function () {
    fbq('trackCustom', 'CustomShoeStep6');

    if (isTouch) {
        if (window.innerWidth <= 767) {
            ModelRenderer.config.isMobile = true;
        }
    }

    CustomShoe.tmp = "";
    CustomShoe.currentStep = "stitchlace";
    CustomShoe.handleTop();

    CustomShoe.loadModel(function (model) {
        ShoeModel = model;

        if (typeof ShoeModel.monogram === 'undefined') {
            ShoeModel.monogram = {};
        }

        if ('shoetree' in ShoeModel) {
            $("#shoeTreeChk").prop('checked', (ShoeModel.shoetree == 'true'));
        }

        CustomShoe.stitchlace.init();
    });
});