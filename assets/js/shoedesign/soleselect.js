$(document).ready(function () {
    fbq('trackCustom', 'CustomShoeStep5');

    CustomShoe.currentStep = "soleSelector";
    CustomShoe.handleTop();
    
    CustomShoe.loadModel(function (model) {
        ShoeModel = model;
        CustomShoe.soleSelector.init();

        if (typeof ShoeModel.sole !== 'undefined') {
            if (typeof ShoeModel.sole.stitchId !== 'undefined' && typeof ShoeModel.sole.stitch !== 'undefined') {
                var sel = $(".sole-stitch-chk[data-id='" + ShoeModel.sole.stitchId + "'][data-code='" + ShoeModel.sole.stitch + "']");
                if (sel.length > 0) {
                    sel.prop('checked', 'true');
                    $(".active-stitch-chk").removeClass('active-stitch-chk');
                    sel.addClass('active-stitch-chk');
                }
            } else {
                var sel = $(".sole-stitch-chk:first");
                if (sel.length > 0) {
                    sel.prop('checked', 'true');
                    $(".active-stitch-chk").removeClass('active-stitch-chk');
                    sel.addClass('active-stitch-chk');
                }
            }
        } else {
            var sel = $(".sole-stitch-chk:first");
            if (sel.length > 0) {
                sel.prop('checked', 'true');
                $(".active-stitch-chk").removeClass('active-stitch-chk');
                sel.addClass('active-stitch-chk');
            }
        }

        ModelRenderer.showPrice();
    });
});