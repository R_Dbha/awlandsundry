$(document).ready(function () {
    fbq('trackCustom', 'CustomShoeStep3');

    CustomShoe.currentStep = "featureSelector";
    CustomShoe.handleTop();
    
    if (isTouch) {
        if (window.innerWidth <= 767) {
            ModelRenderer.config.isMobile = true;
        }
    }
    
    ModelRenderer.container.svg = '';
    
    CustomShoe.loadModel(function (model) {
        ShoeModel = model;
        CustomShoe.featureSelector.init();
    });
});