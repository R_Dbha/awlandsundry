var ModelRenderer = {
    render: function () {
        if (ShoeModel.last.folder.trim() !== "" && $(ModelRenderer.container.main).length > 0) {
            this.renderBase();
            this.renderStitch();
            this.renderLace();
            this.renderSVG();
            this.renderMaterialColor();
            this.renderMonogram();
            this.showPrice();
        }
    },
    renderBase: function () {
        var vamp = 'V00';
        if (ShoeModel.style.isVampEnabled !== "0") {
            vamp = ShoeModel.vamp.type;
        }
        this.urlPrefix = imageBase + ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/" + ShoeModel.quarter.matfolder + "/" + ShoeModel.style.code + "_";
        // $(this.container.main + " .visualImgWrap img:not(#overlay)").hide();
        ShoeModel.quarter.base = ShoeModel.toe.type + vamp + ShoeModel.eyestay.type + ShoeModel.foxing.type + ShoeModel.quarter.material + ShoeModel.quarter.color;
        this.quarterbase = ShoeModel.toe.type + vamp + ShoeModel.eyestay.type + ShoeModel.foxing.type;
        $(this.container.quarter).attr("src", this.urlPrefix + ShoeModel.quarter.base + "_" + ShoeModel.angle + ShoeModel.ext);
        $(this.container.quarter).show();
    },
    renderMaterialColor: function () {
        var images = [];
        var base = ShoeModel.style.code + "_" + ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type;
        var urlPrefix = ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/";
        images.push({
            type: ModelRenderer.container.toe,
            url: urlPrefix + ShoeModel.toe.matfolder + "/" + base + ShoeModel.toe.material + ShoeModel.toe.color + ShoeModel.toe.part + "_" + ShoeModel.angle + ShoeModel.ext
        });
        if (ShoeModel.style.isVampEnabled !== "0") {
            images.push({
                type: ModelRenderer.container.vamp,
                url: urlPrefix + ShoeModel.vamp.matfolder + "/" + base + ShoeModel.vamp.material + ShoeModel.vamp.color + ShoeModel.vamp.part + "_" + ShoeModel.angle + ShoeModel.ext
            });
        } else {
            $(ModelRenderer.container.vamp).hide();
        }
        images.push({
            type: ModelRenderer.container.eyestay,
            url: urlPrefix + ShoeModel.eyestay.matfolder + "/" + base + ShoeModel.eyestay.material + ShoeModel.eyestay.color + ShoeModel.eyestay.part + "_" + ShoeModel.angle + ShoeModel.ext
        });
        images.push({
            type: ModelRenderer.container.foxing,
            url: urlPrefix + ShoeModel.foxing.matfolder + "/" + base + ShoeModel.foxing.material + ShoeModel.foxing.color + ShoeModel.foxing.part + "_" + ShoeModel.angle + ShoeModel.ext
        });
        ModelRenderer.loadBaseData(images);
    },
    loadBaseData: function (images) {
        $.each(images, function (i, val) {
            $.ajax({
                url: baseUrl + 'customshoe/get_image_base',
                data: {imagepath: val.url},
                dataType: 'JSON',
                type: 'POST',
                success: function (msg) {
                    if (msg.exists) {
                        $(val.type).attr('src', msg.base);
                        $(val.type).show();
                    } else {
                        $(val.type).hide();
                    }
                }
            });
        });
    },
    renderStitch: function () {
        var urlPref = imageBase + ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/" + ShoeModel.stitch.folder + "/" + ShoeModel.style.code + "_";
        var stitch = ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type + ShoeModel.stitch.code + "_" + ShoeModel.angle + ShoeModel.ext;
        $(ModelRenderer.container.stitch).attr("src", urlPref + stitch);
        $(ModelRenderer.container.stitch).show();
    },
    renderLace: function () {
        if (ShoeModel.style.isLaceEnabled !== "0") {
            var urlPref = imageBase + ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/" + ShoeModel.lace.folder + "/" + ShoeModel.style.code + "_";
            var lace = ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type + ShoeModel.lace.code + "_" + ShoeModel.angle + ShoeModel.ext;
            $(this.container.lace).attr("src", urlPref + lace);
            $(this.container.lace).show();
        } else {
            $(this.container.lace).hide();
        }
    },
    renderMonogram: function () {
        $("#monogram-complementary div.monogram").hide();
//        $(".monogram-badge").hide();
        $("#monogram-complementary div.monogram").removeClass('monogram_A0 monogram_A4');
        if (typeof ShoeModel.monogram !== "undefined" && (ShoeModel.angle === "A0" || ShoeModel.angle === "A4")) {
//        if (typeof ShoeModel.monogram !== "undefined" && ShoeModel.monogram.text !== '') {
            if ((ShoeModel.angle === "A0" && ShoeModel.monogram.rightSide === 1) || (ShoeModel.angle === "A4" && ShoeModel.monogram.leftSide == 1)) {
                if ($(".monogram").length > 0) {
                    $(".monogram > div div").remove();

//                $img = $("<div><h3></div>");
//                $img.children('h3').text(ShoeModel.monogram.text);
//                $("#monogram-complementary div.monogram div.mono-image").append($img);

                    for (var i = 0; i < ShoeModel.monogram.text.length; i++) {
                        $img = $("<div><img></div>");
                        $img.children('img').attr("src", imageBase + "monogram/" + ShoeModel.monogram.text[i] + ".png?v=1");
                        $(ModelRenderer.container.main + " div.monogram div.mono-image").append($img);
                    }
                    $(".monogram").addClass("monogram_A4");
                    $(".monogram").show();
                    $(".monogram-badge").show();
                }
            }
        }
    },
//    renderMonogram: function() {
//        $(ModelRenderer.container.main + " div.monogram").hide();
//        $(ModelRenderer.container.main + " div.monogram").removeClass('monogram_A0 monogram_A4');
//        if (typeof ShoeModel.monogram !== "undefined" && (ShoeModel.angle === "A0" || ShoeModel.angle === "A4")) {
//            if ((ShoeModel.angle === "A0" && ShoeModel.monogram.rightSide === 1) || (ShoeModel.angle === "A4" && ShoeModel.monogram.leftSide == 1)) {
//                if ($(ModelRenderer.container.main + " div.monogram").length > 0) {
//                    $(ModelRenderer.container.main + " div.monogram > div div").remove();
//                    for (var i = 0; i < ShoeModel.monogram.text.length; i++) {
//                        $img = $("<div><img></div>");
//                        $img.children('img').attr("src", imageBase + "monogram/" + ShoeModel.monogram.text[i] + ".png");
//                        $(ModelRenderer.container.main + " div.monogram div.mono-image").append($img);
//                    }
//                    $(ModelRenderer.container.main + " div.monogram").addClass("monogram_" + ShoeModel.angle);
//                    $(ModelRenderer.container.main + " div.monogram").show();
//                }
//            }
//        }
//    },
    renderSVG: function () {
        if ($(this.container.svg).length > 0) {
            var svgPathLocal = svgPath.replace("https://www.", "http://dev.")
            $.ajax({
                url: svgPath + ShoeModel.style.svg + "_" + ShoeModel.angle + ".svg",
                success: function (msg) {
                    $(ModelRenderer.container.svg).html(arguments[2].responseText);
                }
            });
        }
    },
    showPrice: function () {
        $.ajax({
            url: baseUrl + 'customshoe/getPrice',
            data: {ShoeModel: ShoeModel},
            type: 'POST',
            success: function (msg) {
                if ($('#shoeTreeChk').length > 0) {
                    if ('shoetree' in ShoeModel) {
                        if (ShoeModel.shoetree == 'true') {
                            msg = '$' + (parseFloat(msg.replace('$', '')) - parseFloat(SHOE_TREE_PRICE));
                        }
                    }

                    if ($('#shoeTreeChk').is(':checked')) {
                        $("#shoe-trees-block").addClass("check-blue");
                        msg = '$' + (parseFloat(msg.replace('$', '')) + parseFloat(SHOE_TREE_PRICE));
                    }
                }
//                if ($('.stitch-selector').length > 0) {
//                    if ($('.active-stitch').length > 0) {
//                        if ('sole' in ShoeModel) {
//                            if ($.type(ShoeModel.sole) == 'object') {
//                                if ('stitchCharge' in ShoeModel.sole) {
//                                    msg = '$' + (parseFloat(msg.replace('$', '')) - parseFloat(ShoeModel.sole.stitchCharge));
//                                }
//                            }
//                        }
//
//                        msg = '$' + (parseFloat(msg.replace('$', '')) + parseFloat($('.active-stitch > .stitch-selector').data('charge')));
//                    }
//                }
                $(ModelRenderer.container.pricing + ' span.price').html(msg);
            }
        });
    },
    rotate: function (direction) {
        var angle = parseInt(ShoeModel.angle.replace('A', ''));
        if (direction === this.config.rotateSide) {
            ShoeModel.angle = "A0";
        } else if (direction === this.config.rotateTop) {
            ShoeModel.angle = "A7";
        } else {
            if (angle === 7 && direction === this.config.rotateRight) {
                ShoeModel.angle = "A0";
            } else if (angle === 0 && direction === this.config.rotateLeft) {
                ShoeModel.angle = "A7";
            } else {
                angle = direction === this.config.rotateRight ? (angle + 1) : (angle - 1);
                ShoeModel.angle = "A" + angle;
            }
        }
        ModelRenderer.render();
    },
    partNotAvailable: function (imgId) {
        $("#" + imgId).hide();
    },
    bindEvents: function () {
        $(ModelRenderer.container.main + " img").error(function () {
            if ($(this).attr("id") === "base") {
                ModelRenderer.errorRender();
            } else {
                ModelRenderer.partNotAvailable($(this).attr("id"));
            }
        });
        $(ModelRenderer.container.quarter).load(ModelRenderer.resetContainerHeight);
    },
    resetContainerHeight: function () {
        setTimeout(function () {
//            $(ModelRenderer.container.main).height($(ModelRenderer.container.quarter).height());
            if (CustomShoe.currentStep === "sizing") {
                $("#loader").fadeOut(300, function () {
                    $("#step6_shoes").fadeIn(300);
                });
            }
        }, 300);
    },
    container: {
        main: '#design-visual',
        quarter: '#quarter',
        toe: '#toe',
        eyestay: '#eyestay',
        vamp: '#vamp',
        foxing: '#foxing',
        lace: '#lace',
        stitch: '#stitch',
        svg: '#style-svg',
        pricing: '#custom-price',
        canvas: '#pic-canvas'
    },
    config: {
        rotateLeft: 'LEFT',
        rotateRight: 'RIGHT',
        rotateSide: 'SIDE',
        rotateTop: 'TOP',
        renderedWidth: 720,
        renderedHeight: 405,
        isMobile: false
    },
    urlPrefix: '',
    quarterbase: ''
};