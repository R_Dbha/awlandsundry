var ShoeModel = {};
var CustomShoe = {
    currentStep: "",
    adjacentProperties: {
        toe: ["vamp", "quarter", "eyestay"],
        vamp: ["toe", "eyestay", "quarter"],
        eistay: ["vamp", "quarter"],
        foxing: ["quarter"]
    },
    changeView: function () {
        $(".monogram").hide();
        if (!$(this).hasClass('current-item')) {
            $("#rotate-side").removeClass('current-item');
            $("#rotate-top").removeClass('current-item');
            $(this).addClass('current-item');
            var direction = $(this).hasClass('rotate-side') ? ModelRenderer.config.rotateSide : ModelRenderer.config.rotateTop;
            ModelRenderer.rotate(direction);
            return false;
        }
    },
    changeSliderView: function () {
        if (!$(this).hasClass('current-item')) {
            $(".rotate-side").removeClass('current-item');
            $(".rotate-top").removeClass('current-item');
            $(this).addClass('current-item');
            var that = $(this);

            $.each($('img.main-shoe'), function () {
                var src = $(this).attr('src');

                if (that.hasClass('rotate-side')) {
                    src = src.replace('A7.png', 'A0.png');
                } else {
                    src = src.replace('A0.png', 'A7.png');
                }

                $(this).attr('src', src);

            });
            return false;
        }
    },
    handleTop: function () {
        $("body").on('click', '.closeAppointment', function () {
            $('#menu-sticky-wrapper').addClass('no-appointment');
        });
    },
    saveModel: function (callBack, isBack) {
        if (typeof isBack == 'undefined') {
            isBack = "No";
        }
        var url = baseUrl + "customshoe/saveStepDetails";
        $.ajax({
            type: "POST",
            url: url,
            data: {
                ShoeModel: ShoeModel,
                CurrentStep: CustomShoe.currentStep,
                IsBack: isBack
            },
            dataType: 'JSON',
            success: function (result) {
                if (result.success) {
                    callBack();
                } else {
                    alert("Something went wring");
                }
            }
        });
    },
    saveDesign: function () {
        $.ajax({
            type: "POST",
            url: base_url + 'customshoe/saveDesign',
            data: {
                'shoemodel': ShoeModel
            },
            dataType: 'json',
            success: function (msg) {
                if (msg.isValidLogin) {
                    alert('Design saved successfully.');
                } else {
                    $('#overlay1').show();
                    $('#login_modal').show();
                }
            }
        });
        return false;
    },
    loadModel: function (callback) {
        $.ajax({
            type: "POST",
            url: baseUrl + "customshoe/getShoeModel",
            dataType: 'JSON',
            data: {
                CurrentStep: CustomShoe.currentStep
            },
            success: function (result) {
                if (result.success) {
                    if (typeof ModelRenderer != 'undefined') {
                        $(ModelRenderer.container.main + ' img').error(function () {
                            $(this).hide();
                        });
                    }
                    callback(result.model);
                } else {
                    alert("Something went wrong");
                }
            }
        });
    },
    highlightFeture: function (e, text) {
        $('.visualImgWrap img').css({
            'cursor': 'pointer'
        });
        var part = text.replace(/\b[a-z]/g, function (letter) {
            return letter.toUpperCase();
        });
        $('div#tooltip').remove();
        $('<div id="tooltip">' + part + '</div>').appendTo('body');
        var tooltipX = e.pageX - 8;
        var tooltipY = e.pageY + 8;
        $('div#tooltip').css({
            top: tooltipY,
            left: tooltipX
        });
        if (!$(".part-buttons a[data-target='" + text + "']").hasClass('hightlight')) {
            $(".part-buttons a").removeClass("highlight");
            $(".part-buttons a[data-target='" + text + "']").addClass("highlight");
        }
    },
    confirmChangeShoe: function (shoe_design_id) {
        if (confirm('This will delete your current design. Do you want to continue?')) {
            $.ajax({
                type: "POST",
                url: baseUrl + "customshoe/related/" + shoe_design_id,
                dataType: 'JSON',
                success: function (result) {
                    CustomShoe.loadModel(function (model) {
                        ShoeModel = model;
                        CustomShoe[CustomShoe.currentStep].init();
                    });
                },
            });
        }
    },
    reloadRelated: function (criteria, slider) {
//        $.ajax({
//            type: "POST",
//            url: baseUrl + "customshoe/getRelated",
//            dataType: 'JSON',
//            data: {
//                criteria: criteria
//            },
//            success: function(result) {
//                var $li, $a, $img;
//                slider.hide();
//                slider.find('li').remove();
//                $.each(result, function(i, val) {
//                    $li = $("<li>");
//                    $a = $('<a  onclick="return CustomShoe.confirmChangeShoe(' + val.shoe_design_id + ');">');
//                    $img = $('<img  src="' + designBase + val.image_file + '_A0.png" alt="' + val.style_last + '">');
//                    $a.append($img);
//                    $li.append($a);
//                    slider.append($li);
//                });
//                slider.show();
//                slider.reloadSlider();
//            }
//        });
    },
    soleSelector: {
        slider: [],
        sliderLength: 0,
        isPageLoaded: false,
        init: function () {
            this.bindEvents();
            $("#loader").fadeOut(300, function () {
                $("#step4_shoes").fadeIn(300, function () {
                    if (typeof ShoeModel.sole !== 'undefined') {
                        if (typeof ShoeModel.sole.id !== 'undefined' && typeof ShoeModel.sole.code !== 'undefined') {
                            var sel = $(".sole-selector[data-id='" + ShoeModel.sole.id + "'][data-code='" + ShoeModel.sole.code + "']");
                            if (sel.length > 0) {
                                sel.trigger('click');
                            }
                        }
                        if (typeof ShoeModel.sole.colorId !== 'undefined' && typeof ShoeModel.sole.color !== 'undefined') {
                            var sel = $(".color-selector[data-id='" + ShoeModel.sole.colorId + "'][data-code='" + ShoeModel.sole.color + "']");
                            if (sel.length > 0) {
                                sel.trigger('click');
                            }
                        }
//                                if (typeof ShoeModel.sole.stitchId !== 'undefined' && typeof ShoeModel.sole.stitch !== 'undefined') {
//                                    var sel = $(".stitch-selector[data-id='" + ShoeModel.sole.stitchId + "'][data-code='" + ShoeModel.sole.stitch + "']");
//                                    if (sel.length > 0) {
//                                        sel.trigger('click');
//                                    }
//                                }
                    }
                });
            });
        },
        nextStep: function () {
            if ($('.active-sole').length == 0 || $('.active-color').length == 0) {
                var msg = '';
                if ($('.active-sole').length == 0) {
                    msg = 'Outsole Construction';
                }
                if ($('.active-color').length == 0) {
                    msg += (msg == '') ? 'Sole Edge Color' : ' and Sole Edge Color';
                }
                alert('Please select ' + msg);
                return false;
            }
            var soleId = $('.active-sole').find('a').data('id');
            var soleCode = $('.active-sole').find('a').data('code');
            var soleColorId = $('.active-color').find('a').data('id');
            var soleColorCode = $('.active-color').find('a').data('code');
            var soleStitchId = (typeof $('.active-stitch-chk').data('id') != 'undefined') ? $('.active-stitch-chk').data('id') : '';
            var soleStitchCode = (typeof $('.active-stitch-chk').data('code') != 'undefined') ? $('.active-stitch-chk').data('code') : '';
            var soleStitchCharge = (typeof $('.active-stitch-chk').data('charge') != 'undefined') ? $('.active-stitch-chk').data('charge') : '';

            ShoeModel.sole = {'id': soleId, 'code': soleCode, 'colorId': soleColorId, 'color': soleColorCode, 'stitchId': soleStitchId, 'stitch': soleStitchCode, 'stitchCharge': soleStitchCharge};
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + "create-a-custom-shoe/details";
            });
        },
        prevStep: function () {
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + 'create-a-custom-shoe/materials-and-colors';
                return false;
            }, 'Yes');
        },
        bindEvents: function () {
            this.initSliders();
//            $(".soleImg").on('mouseenter', function() {
//                $("#color-tooltip").remove();
//                var $tooltip = $('<div id="color-tooltip">').text($(this).parent('a').data('name'));
//                $tooltip.hide();
//                $("body").append($tooltip);
//                var pos = $(this).offset();
//                var height = $(this).outerHeight();
//                var left = (pos.left + ($(this).outerWidth() / 2)) - ($tooltip.outerWidth() / 2);
//                $tooltip.css({
//                    top: (pos.top + height) + "px",
//                    left: left
//                });
//                $tooltip.show(200);
//            });

//            $(".soleImg").on('mouseleave', function() {
//                $("#color-tooltip").remove();
//            });

            $('body').on('click', '.close', function () {
                $('body').css({'overflow': 'auto', 'position': 'static'});
                $('#sizing-popup-wrapper').hide();
                $('.stitchImg').hide();
                $('.overlay').hide();
            });

            // $('body').on('mouseout', '.showStitch', function () {
            //     $('.stitchImg').hide();
            //     $("#myModal").modal('hide');
            // });

//            $('body').on('mouseover', '.showStitch', function () {
//                $('.modalOverLay').removeClass('check-blue');
//                if ($(".sole-stitch-chk[data-code='" + $(this).data('code') + "']").is(":checked")) {
//                    $('.modalOverLay').addClass('check-blue');
//                }
//
//                $('.stitchImg').hide();
//                $('.stitchImg_' + $(this).data('code')).show();
//
//                $("#myModal").modal('show');
//            });

            $('#myModal').on('hidden.bs.modal', function () {
                $('body').removeClass('filter-scroll');
            });

            $('#myModal').on('show.bs.modal', function () {
                $('body').addClass('filter-scroll');
            });

            $('body').on('change', '.sole-stitch-chk', function () {
                var pr = parseFloat($('.price').text().replace('$', ''));

                if ($(this).is(':checked')) {
                    $(".active-stitch-chk").prop('checked', false);
                    pr = pr - $('.active-stitch-chk').data('charge');

                    $(".active-stitch-chk").removeClass('active-stitch-chk');
                    $(this).addClass('active-stitch-chk');
                    pr = pr + $(this).data('charge');
                } else {
                    $(this).removeClass('active-stitch-chk');
                    pr = pr - $(this).data('charge');
                }
                if (pr >= 0) {
                    $('.price').text('$' + pr);
                }

//                $(".sole-stitch-chk:checked").prop('checked', false);
//                $(this).prop('checked', false);
//                $(".active-stitch").removeClass('active-stitch');
//                $(this).addClass('active-stitch');
//
//                if (!$(this).hasClass('active-stitch')) {
//                    var pr = parseFloat($('.price').text().replace('$', ''));
//
//                    if ($('.active-stitch').length > 0) {
//                        pr = pr - $('.active-stitch > .stitch-selector').data('charge');
//                    } else if ('sole' in ShoeModel) {
//                        if ($.type(ShoeModel.sole) == 'object') {
//                            if ('stitchCharge' in ShoeModel.sole) {
//                                pr = pr - ShoeModel.sole.stitchCharge;
//                            }
//                        }
//                    }
//                    pr = pr + $(this).data('charge');
//                    if (pr > 0) {
//                        $('.price').text('$' + pr);
//                    }
//                }
//
//                $('.stitch-selector').parent('li').removeClass('active-stitch');
//                $(this).parent('li').addClass('active-stitch');

            });
            setTimeout(function () {
                $(".sole-stitch-chk:checked").removeClass('active-stitch-chk').addClass('active-stitch-chk');
                $(".sole-stitch-chk:checked").closest('.stich-option').removeClass('check-blue').addClass('check-blue');
            }, 500);
//            $('body').on('click', '.stitch-selector', function () {
//                if (!$(this).hasClass('active-stitch')) {
//                    var pr = parseFloat($('.price').text().replace('$', ''));
//
//                    if ($('.active-stitch').length > 0) {
//                        pr = pr - $('.active-stitch > .stitch-selector').data('charge');
//                    } else if ('sole' in ShoeModel) {
//                        if ($.type(ShoeModel.sole) == 'object') {
//                            if ('stitchCharge' in ShoeModel.sole) {
//                                pr = pr - ShoeModel.sole.stitchCharge;
//                            }
//                        }
//                    }
//                    pr = pr + $(this).data('charge');
//                    if (pr > 0) {
//                        $('.price').text('$' + pr);
//                    }
//                }
//
//                $('.stitch-selector').parent('li').removeClass('active-stitch');
//                $(this).parent('li').addClass('active-stitch');
//
//            });
            $('body').on('click', '.color-selector', function () {
                $('.color-selector').parent('li').removeClass('active-color');
                $(this).parent('li').addClass('active-color');
            });
            $('body').on('click', '.sole-selector', function () {
                $('.sole-selector').parent('li').removeClass('active-sole');
                $(this).parent('li').addClass('active-sole');
                $(this).data('name');
                var tmpText = $(this).data('name');
                if (typeof tmpText !== 'undefined' && tmpText !== '') {
                    $(".bottomHeading").text(tmpText);
                }
                $(".disabledColor").removeClass('disabledColor');
                $(".clrFor").addClass('disabledColor');
                $(".clrFor_" + $(this).data('id')).removeClass('disabledColor');

                if ($(".active-color").length > 0) {
                    if ($(".active-color").hasClass('disabledColor')) {
                        console.log($(".clrFor:not(.disabledColor):first"));
                        console.log($(".clrFor:not(.disabledColor):first").find('.color-selector'));
                        $(".clrFor:not(.disabledColor):first").find('.color-selector').trigger('click');
                    }
                }

            });

            $(".next-btn").on("click", CustomShoe.soleSelector.nextStep);
            $("#back-btn").on('click', CustomShoe.soleSelector.prevStep);
        },
        initSliders: function () {
            // $(".vertSlider").each(function () {
            //     var attribs = {
            //         auto: false,
            //         minSlides: 3,
            //         maxSlides: 3,
            //         slideWidth: 150,
            //         slideMargin: 5,
            //         moveSlides: 1,
            //         infiniteLoop: false,
            //         pager: false,
            //         hideControlOnEnd: true,
            //         captions: true,
            //         onSliderLoad: CustomShoe.soleSelector.sliderLoadComplete
            //     };
            //     if ($(this).hasClass('vertSlider')) {
            //         attribs.mode = 'vertical';
            //         attribs.slideWidth = 332;
            //     }
            //     CustomShoe.soleSelector.slider.push($(this).bxSlider(attribs));
            // });
        },
        sliderLoadComplete: function () {
            if (!CustomShoe.soleSelector.isPageLoaded) {
                CustomShoe.soleSelector.sliderLength++;
                if (CustomShoe.soleSelector.sliderLength === ($(".vertSlider").length)) {
                    CustomShoe.soleSelector.isPageLoaded = true;
                    $("#loader").fadeOut(300, function () {
                        $("#step4_shoes").fadeIn(300, function () {
                            $.each(CustomShoe.soleSelector.slider, function (idx, val) {
                                val.reloadSlider();
                            });

                            if (typeof ShoeModel.sole !== 'undefined') {
                                if (typeof ShoeModel.sole.id !== 'undefined' && typeof ShoeModel.sole.code !== 'undefined') {
                                    var sel = $(".sole-selector[data-id='" + ShoeModel.sole.id + "'][data-code='" + ShoeModel.sole.code + "']");
                                    if (sel.length > 0) {
                                        sel.trigger('click');
                                    }
                                }
                                if (typeof ShoeModel.sole.colorId !== 'undefined' && typeof ShoeModel.sole.color !== 'undefined') {
                                    var sel = $(".color-selector[data-id='" + ShoeModel.sole.colorId + "'][data-code='" + ShoeModel.sole.color + "']");
                                    if (sel.length > 0) {
                                        sel.trigger('click');
                                    }
                                }
//                                if (typeof ShoeModel.sole.stitchId !== 'undefined' && typeof ShoeModel.sole.stitch !== 'undefined') {
//                                    var sel = $(".stitch-selector[data-id='" + ShoeModel.sole.stitchId + "'][data-code='" + ShoeModel.sole.stitch + "']");
//                                    if (sel.length > 0) {
//                                        sel.trigger('click');
//                                    }
//                                }
                            }
                        });
                    });
                }
            }
        }
    },
    styleSelector: {
        slider: {},
        isPageLoaded: false,
        init: function () {
            this.bindEvents();
//            CustomShoe.currentStep = "styleSelector";
            var products = [];
            var cnt = 1;
            $('.step1_main_slider li').each(function () {
                var shoe_name = $(this).not('.bx-clone').find('h3.main-shoe').text();
                if (shoe_name) {
                    products.push({
                        'name': shoe_name, // Name or ID is required.
                        'price': '0',
                        'brand': 'Awl & Sundry',
                        'category': 'Custom Shoes',
                        'position': cnt
                    });
                    cnt++;
                }
            });
//            CustomShoe.loadModel(function(model) {
//                ShoeModel = model;
//                if (typeof ShoeModel.style.id != 'undefined') {
//                    $(".last-style-id[value='" + ShoeModel.style.id + "']").closest('li').find('a').trigger('click');
//                }
//            });
            dataLayer.push({
                'ecommerce': {
                    'currencyCode': 'USD', // Local currency is optional.
                    'impressions': products
                }
            });
        },
        nextStep: function () {
            ShoeModel.style = {
                styleName: $('#step1_shoes li a.current-shoe').attr("id"),
                svg: $('#step1_shoes li a.current-shoe').attr("id"),
                id: $('#step1_shoes li a.current-shoe').parents("li").find(".last-style-id").val(),
                folder: $('#step1_shoes li a.current-shoe').parents("li").find(".folder_name").val(),
                code: $('#step1_shoes li a.current-shoe').parents("li").find(".def_code").val(),
                isVampEnabled: $('#step1_shoes li a.current-shoe').parents("li").find(".is_vamp_enabled").val(),
                isLaceEnabled: $('#step1_shoes li a.current-shoe').parents("li").find(".is_lace_enabled").val(),
            };
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + "create-a-custom-shoe/design-features";
            });
        },
        prevStep: function () {
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + 'create-a-custom-shoe';
                return false;
            }, 'Yes');
        },
        bindEvents: function () {
            $('#step1_shoes li a').on('click touchend', function () {
                $('#step1_shoes li a').removeClass('current-shoe');
                $(this).addClass('current-shoe');
                CustomShoe.styleSelector.slider.goToSlide($(this).data("index"));
            });
            var custom = {
                pager: false,
                controls: true,
                pagerSelector: '',
                preloadImages: 'all',
            };
            if (isTouch) {
                if (window.innerWidth <= 767) {
                    custom.pager = true;
                    custom.controls = false;
                    custom.pagerSelector = '.custom_pager';
                    custom.preloadImages = 'all';
                }
            }
            var selectedSlide = 0;
            if (typeof ShoeModel.style.id != 'undefined') {
                selectedSlide = $(".last-style-id[value='" + ShoeModel.style.id + "']").closest('li').index();
            }
            this.slider = $('.step1_main_slider').bxSlider({
                auto: false,
                pager: custom.pager,
                controls: custom.controls,
                pagerSelector: custom.pagerSelector,
                preloadImages: 'all',
                startSlide: selectedSlide,
                onSlideAfter: function ($slideElement, oldIndex, newIndex) {
                    $('#step1_shoes li a').removeClass('current-shoe');
                    $("#step1_shoes li a[data-index='" + newIndex + "']").addClass("current-shoe");
                },
                onSliderLoad: function () {
                    if (!CustomShoe.styleSelector.isPageLoaded) {
                        CustomShoe.styleSelector.isPageLoaded = true;
                        $("#loader").fadeOut(300, function () {
                            if (typeof ShoeModel.style.id != 'undefined') {
                                $(".last-style-id[value='" + ShoeModel.style.id + "']").closest('li').find('a').trigger('click');
                            }
                            $("#main_shoe_common").fadeIn(300, function () {
                                CustomShoe.styleSelector.slider.reloadSlider();
                                $("#step1_shoes").show();
                            });
                        });
                    }
                }
            });
            $(".next-step").on("click", CustomShoe.styleSelector.nextStep);
            $("#back-btn").on('click', CustomShoe.styleSelector.prevStep);

            $("body").on('click touchend', '.rotate-side,.rotate-top', CustomShoe.changeSliderView);
        }
    },
    /*lastSelector: {
     slider: {},
     isPageLoaded: false,
     init: function () {
     if (typeof ShoeModel.style === "undefined"
     || typeof ShoeModel.style.styleName === "undefined") {
     window.location.href = baseUrl
     + "create-a-custom-shoe/select-style";
     return;
     }
     this.bindEvents();
     },
     nextStep: function () {
     if ($('#step2_shoes li a.current-shoe').length === 0) {
     alert("Please select a style.");
     return false;
     }
     ShoeModel.style.id = $('#step2_shoes li a.current-shoe').parents(
     "li").find("input:hidden").val();
     var main_shoe =  $('#step2_shoes li a.current-shoe').attr('id');
     dataLayer.push({
     'event': 'productClick',
     'ecommerce': {
     'click': {
     'actionField': {'list': 'Custom Shoes'},      // Optional list property.
     'products': [{
     'name': main_shoe+'/'+ShoeModel.style.styleName,                      // Name or ID is required.
     'price': '0',
     'brand': 'Awl & Sundry',
     'category': 'Custom Shoes'
     }]
     }
     },
     'eventCallback': function() {
     CustomShoe.saveModel(function () {
     window.location.href = baseUrl
     + "create-a-custom-shoe/design-features";
     });
     }
     });
     
     
     },
     prevStep: function () {
     window.location.href = baseUrl
     + "create-a-custom-shoe/select-style";
     },
     bindEvents: function () {
     $('#step2_shoes li a').click(
     function () {
     $('#step2_shoes li a').removeClass('current-shoe');
     $(this).addClass('current-shoe');
     CustomShoe.lastSelector.slider.goToSlide($(this).data(
     "index"));
     });
     var custom = {
     pager: false,
     controls: true,
     pagerSelector: ''
     };
     if (isTouch) {
     if (window.innerWidth <= 767) {
     custom.pager = true;
     custom.controls = false;
     custom.pagerSelector = '.custom_pager';
     }
     }
     
     this.slider = $('.step2_main_slider')
     .bxSlider(
     {
     auto: false,
     pager: custom.pager,
     controls: custom.controls,
     pagerSelector: custom.pagerSelector,
     onSlideAfter: function ($slideElement,
     oldIndex, newIndex) {
     $('#step2_shoes li a').removeClass(
     'current-shoe');
     $(
     "#step2_shoes li a[data-index='"
     + newIndex + "']")
     .addClass("current-shoe");
     },
     onSliderLoad: function () {
     if (!CustomShoe.lastSelector.isPageLoaded) {
     CustomShoe.lastSelector.isPageLoaded = true;
     $("#loader")
     .fadeOut(
     300,
     function () {
     $(
     "#main_shoe_common")
     .fadeIn(
     300,
     function () {
     $(
     "#step2_shoes")
     .show();
     CustomShoe.lastSelector.slider
     .reloadSlider();
     });
     });
     }
     }
     });
     $("#back-btn").on("click", CustomShoe.lastSelector.prevStep);
     $("#next-step").on("click", CustomShoe.lastSelector.nextStep);
     }
     },*/
    featureSelector: {
        sliders: {},
        sliderLength: 0,
        isPageLoaded: false,
        init: function () {
            this.bindEvents();
            ModelRenderer.bindEvents();
            ModelRenderer.render();
            var price = $(ModelRenderer.container.pricing + ' span.price').html();
            price = price.replace('$', '');
            dataLayer.push({
                'ecommerce': {
                    'detail': {
                        'actionField': {'list': 'Custom Shoes'}, // 'detail' actions have an optional list property.
                        'products': [{
                                'name': $("#style-last-name h3").text(), // Name or ID is required.
                                'price': price,
                                'brand': 'Awl & Sundry',
                                'category': 'Custom Shoes'
                            }]
                    }
                }
            });
        },
        sliderLoadComplete: function () {
            if (!CustomShoe.featureSelector.isPageLoaded) {
                CustomShoe.featureSelector.sliderLength++;
                if (CustomShoe.featureSelector.sliderLength === ($("div.part-buttons a").length + 1)) {
                    CustomShoe.featureSelector.isPageLoaded = true;
                    CustomShoe.featureSelector.partChange($(".part-buttons a").last());
                    $("#step3_main_slider .bx-wrapper").addClass("none");
                    $("#loader").fadeOut(300, function () {
                        $("#step3_container").fadeIn(300, function () {
                            CustomShoe.featureSelector.sliders['All'].reloadSlider();

                            var selected = $('.property[data-t="' + ShoeModel.toe.type + '"][data-e="' + ShoeModel.eyestay.type + '"][data-f="' + ShoeModel.foxing.type + '"][data-v="' + ShoeModel.vamp.type + '"]').find('a');
                            if (selected.length > 0) {
                                $('.property[data-t="' + ShoeModel.toe.type + '"][data-e="' + ShoeModel.eyestay.type + '"][data-f="' + ShoeModel.foxing.type + '"][data-v="' + ShoeModel.vamp.type + '"]').find('a').trigger('click')
                            } else {
                                $('.property').first().find('a').trigger('click');
                            }
                        });
                    });
                }
            }
        },
        partChange: function (elem) {
            var target = "";
            if (elem.tagName === "path") {
                target = $(elem).attr("id").replace('path_', '').charAt(0).toUpperCase() + $(elem).attr("id").replace("path_", '').slice(1);
            } else {
                target = $(elem).data('target');
            }
            target = 'All';
            CustomShoe.featureSelector.fixInvalid(target, target + "_properties", function () {
                $(".part-buttons a").removeClass('active');
                $("#step3_main_slider .bx-wrapper").addClass("none");
                $("#" + target + "_properties").parents(".bx-wrapper").removeClass("none");
                CustomShoe.featureSelector.sliders[target].reloadSlider();
                // $("#property_title span").text(target.toUpperCase() + "FEATURES");
                $('a[data-target="' + target + '"]').addClass('active');
//                if (target === 'Foxing') {
//                    if (ShoeModel.angle !== 'A6') {
//                        ShoeModel.angle = 'A6';
//                    }
//                } else {
//                    if (ShoeModel.angle !== 'A0') {
//                        ShoeModel.angle = 'A0';
//                    }
//                }

                ModelRenderer.render();
            });
        },
        featureSelect: function () {
            var _this = $(this);
            if (!_this.parents('li').hasClass('disabled')) {
                $(".property a").removeClass("step3_current_shoe");
                _this.addClass("step3_current_shoe");
                var smType = _this.data("type").toLowerCase();
                smType = 'toe';

                var toeObj = _this.find('.property-toe').val();
                toeObj = JSON.parse(toeObj);
                ShoeModel['toe'].toeId = toeObj.ToeId;
                ShoeModel['toe'].type = toeObj.code;
                ShoeModel['toe'].isMaterial = toeObj.isMaterial;
                ShoeModel['toe'].isNothing = toeObj.isNothing;

                var eyestayObj = _this.find('.property-eyestay').val();
                eyestayObj = JSON.parse(eyestayObj);
                ShoeModel['eyestay'].eyestayId = eyestayObj.EyestayId;
                ShoeModel['eyestay'].type = eyestayObj.code;
                ShoeModel['eyestay'].isMaterial = eyestayObj.isMaterial;
                ShoeModel['eyestay'].isNothing = eyestayObj.isNothing;

                var foxingObj = _this.find('.property-foxing').val();
                foxingObj = JSON.parse(foxingObj);
                ShoeModel['foxing'].foxingId = foxingObj.FoxingId;
                ShoeModel['foxing'].type = foxingObj.code;
                ShoeModel['foxing'].isMaterial = foxingObj.isMaterial;
                ShoeModel['foxing'].isNothing = foxingObj.isNothing;

                var vampObj = _this.find('.property-vamp').val();
                vampObj = JSON.parse(vampObj);
                ShoeModel['vamp'].vampId = vampObj.VampId;
                ShoeModel['vamp'].type = vampObj.code;
                ShoeModel['vamp'].isMaterial = vampObj.isMaterial;
                ShoeModel['vamp'].isNothing = vampObj.isNothing;

//                var property = {
//                    'isMaterial': _this.find('.property-ismaterial').val(),
//                    'id': _this.find('.property-id').val(),
//                    'isNothing': _this.find('.property-isnothing').val(),
//                    'code': _this.find('.property-code').val()
//                };
//                ShoeModel[smType][smType + 'Id'] = property.id;
//                ShoeModel[smType].type = property.code;
//                ShoeModel[smType].isMaterial = property.isMaterial;
//                ShoeModel[smType].isNothing = property.isNothing;

                var criteria = {
                    style: ShoeModel.style.styleName,
                    part: smType,
                    partId: '2'
                };
                CustomShoe.reloadRelated(criteria, CustomShoe.featureSelector.sliders.Related);
                ModelRenderer.render();

                if (ShoeModel[smType].isMaterial == 0) {
                    var nearestMaterials = [];
                    $.each(CustomShoe.adjacentProperties[smType], function (i, val) {
                        var material = {
                            matId: ShoeModel[val].matId,
                            clrId: ShoeModel[val].clrId,
                            clrCode: ShoeModel[val].color,
                            matCode: ShoeModel[val].material,
                            matFolder: ShoeModel[val].matfolder
                        };
                        if (val !== 'quarter') {
                            if (ShoeModel[val].isMaterial == 1) {
                                nearestMaterials.push(material);
                            }
                        } else {
                            nearestMaterials.push(material);
                        }
                        if (parseInt(i) === parseInt(CustomShoe.adjacentProperties[smType].length - 1) && 1 == 2) {
                            $.ajax({
                                type: "POST",
                                url: baseUrl + "customshoe/unsmooth_property",
                                data: {
                                    lsfolder: ShoeModel.last.folder + "/" + ShoeModel.style.folder + "/",
                                    materials: nearestMaterials,
                                    base: ShoeModel.toe.type + ShoeModel.vamp.type + ShoeModel.eyestay.type + ShoeModel.foxing.type,
                                    part: ShoeModel[smType].part,
                                    stylecode: ShoeModel.style.code
                                },
                                dataType: 'json',
                                success: function (foundMat) {
                                    ShoeModel[smType].matId = foundMat.matId;
                                    ShoeModel[smType].clrId = foundMat.clrId;
                                    ShoeModel[smType].material = foundMat.matCode;
                                    ShoeModel[smType].matfolder = foundMat.matFolder;
                                    ShoeModel[smType].color = foundMat.clrCode;
                                    ModelRenderer.render();
                                }
                            });
                        }
                    });
                } else {
                    ModelRenderer.render();
                }
            }
        },
        fixInvalid: function (required, page, callBack) {
            var data = {
                Toe: ShoeModel.toe.type,
                Eyestay: ShoeModel.eyestay.type,
                Vamp: ShoeModel.vamp.type,
                Foxing: ShoeModel.foxing.type,
                required: required,
                styleId: ShoeModel.style.id
            };
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                url: baseUrl + 'customshoe/getInvalidFeatures/',
                data: data,
                success: function (properties) {
                    $("#" + page + " li").removeClass('disabled').removeAttr('title');
                    if (typeof properties.notavailable !== "undefined") {
                        $.each(properties.notavailable, function (i, val) {
                            if (required == 'Eyestay') {
                                if (ShoeModel.style.folder == 'Oxford') {
                                    $("#" + page + " input.property-id[value='" + val.property_type_id + "']").parents("li.property").addClass("disabled").attr("title", "Select an appropriate vamp to activate this design");
                                } else {
                                    $("#" + page + " input.property-id[value='" + val.property_type_id + "']").parents("li.property").addClass("disabled").attr("title", "Select a different design to access this option");
                                }
                            } else {
                                $("#" + page + " input.property-id[value='" + val.property_type_id + "']").parents("li.property").addClass("disabled").attr("title", "Select a different design to access this option");
                            }
                            if (i === (properties.notavailable.length - 1)) {
                                callBack();
                            }
                        });
                    } else {
                        callBack();
                    }
                }
            });
        },
        nextStep: function () {
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + 'create-a-custom-shoe/materials-and-colors';
                return false;
            });
        },
        prevStep: function () {
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + "create-a-custom-shoe/select-style";
                return false;
            }, 'Yes');
        },
        bindEvents: function () {

            $(".part-buttons a").on("click touchend", function () {
                CustomShoe.featureSelector.partChange(this);
            });
            $(".property a").on('click touchend', CustomShoe.featureSelector.featureSelect);
            $(ModelRenderer.container.svg).on('mousemove', 'path', function (e) {
                $(this).css({'cursor': 'pointer'});
                var text = $(e.currentTarget).attr('id').replace('path_', '').charAt(0).toUpperCase() + $(e.currentTarget).attr('id').replace("path_", '').slice(1);
                CustomShoe.highlightFeture(e, text);
            });
            $(ModelRenderer.container.svg).on('mouseleave', 'path', function (e) {
                $(this).css({'cursor': 'default'});
                $('div#tooltip').remove();
                $(".part-buttons a").removeClass("highlight");
            });
            $(ModelRenderer.container.svg).off('click touchend').on('click touchend', 'path', function () {
                CustomShoe.featureSelector.partChange(this);
            });
            $("#rotate-wrapper").on('click touchend', 'a.rotate-left, a.rotate-right', function () {
                var direction = $(this).hasClass('rotate-left') ? ModelRenderer.config.rotateLeft : ModelRenderer.config.rotateRight;
                ModelRenderer.rotate(direction);
                return false;
            });
            $("body").on('click touchend', '#rotate-side,#rotate-top', CustomShoe.changeView);
            $(".next-btn").on('click', CustomShoe.featureSelector.nextStep);
            $("#back-btn").on('click', CustomShoe.featureSelector.prevStep);
            $('#rotate-side').trigger('click');
            this.initSliders();
        },
        initSliders: function () {
            var max = 5;
            var tmpCnt = $('#All_properties').find('li').length;
            var controls = true;
            if (tmpCnt <= max) {
                controls = false;
            }
            var attribs = {
                controls: controls,
                auto: false,
                minSlides: max,
                maxSlides: max,
                slideWidth: 120,
                slideMargin: 5,
                moveSlides: 4,
                infiniteLoop: false,
                pager: false,
                hideControlOnEnd: true,
                onSliderLoad: CustomShoe.featureSelector.sliderLoadComplete
            };
            if (ModelRenderer.config.isMobile) {
                attribs.minSlides = 2;
                attribs.pager = false;
                attribs.maxSlides = 2;
                attribs.moveSlides = 2;
                attribs.slideWidth = 100, attribs.controls = true;
                attribs.slideMargin = 0;
            }

            this.sliders["All"] = $('#All_properties').bxSlider(attribs);
//            this.sliders["Eyestay"] = $('#Eyestay_properties').bxSlider(attribs);
//            this.sliders["Foxing"] = $('#Foxing_properties').bxSlider(attribs);
//            if (ShoeModel.style.isVampEnabled !== "0") {
//                this.sliders["Vamp"] = $('#Vamp_properties').bxSlider(attribs);
//            }

            if (window.innerWidth >= 768) {
                sl = this.sliders["Related"] = $('.step3_bottom_slider').bxSlider({
                    auto: false,
                    minSlides: 3,
                    maxSlides: 3,
                    slideWidth: 300,
                    slideMargin: 5,
                    moveSlides: 3,
                    infiniteLoop: false,
                    pager: false,
                    hideControlOnEnd: true,
                    onSliderLoad: CustomShoe.featureSelector.sliderLoadComplete
                });
            } else {
                if (ModelRenderer.config.isMobile) {
                    if (window.innerWidth < 479) {
                        sl = this.sliders["Related"] = $('.step3_bottom_slider').bxSlider({
                            auto: false,
                            minSlides: 1,
                            maxSlides: 1,
                            slideWidth: 300,
                            slideMargin: 0,
                            moveSlides: 1,
                            infiniteLoop: false,
                            pager: false,
                            hideControlOnEnd: true,
                            onSliderLoad: CustomShoe.featureSelector.sliderLoadComplete
                        });

                    }
                    if (window.innerWidth <= 767 && window.innerWidth >= 479) {
                        sl = this.sliders["Related"] = $('.step3_bottom_slider').bxSlider({
                            auto: false,
                            minSlides: 2,
                            maxSlides: 2,
                            slideWidth: 200,
                            slideMargin: 0,
                            moveSlides: 2,
                            infiniteLoop: false,
                            pager: false,
                            hideControlOnEnd: true,
                            onSliderLoad: CustomShoe.featureSelector.sliderLoadComplete
                        });
                    }
                }
            }
            $(window).resize(function () {
                if (window.innerWidth < 479) {
                    sl.reloadSlider({
                        auto: false,
                        minSlides: 1,
                        maxSlides: 1,
                        slideWidth: 300,
                        slideMargin: 0,
                        moveSlides: 1,
                        infiniteLoop: false,
                        pager: false,
                        hideControlOnEnd: true,
                        onSliderLoad: CustomShoe.featureSelector.sliderLoadComplete
                    });
                }
                if (window.innerWidth <= 767 && window.innerWidth >= 479) {
                    sl.reloadSlider({
                        auto: false,
                        minSlides: 2,
                        maxSlides: 2,
                        slideWidth: 200,
                        slideMargin: 0,
                        moveSlides: 2,
                        infiniteLoop: false,
                        pager: false,
                        hideControlOnEnd: true,
                        onSliderLoad: CustomShoe.featureSelector.sliderLoadComplete
                    });
                }
            });
            $("ul.step_3_slider.none").parents(".bx-wrapper").addClass("none");
            $("ul.step_3_slider.none").removeClass("none");
        }
    },
    styling: {
        sliders: {},
        sliderLength: 0,
        isPageLoaded: false,
        active: {},
        init: function () {
            this.bindEvents();
            ModelRenderer.bindEvents();
            ModelRenderer.render();
        },
        featureSelect: function () {
            var feature = $(this).data('feature');
            var prevFeature = $(this).parent().find('.active').data('feature');
            var prevMat = $("#" + prevFeature + "_lethers").find('.active-leather').parent().find('input.mat-code').val();
            var matCode = ShoeModel[$(this).data('feature')].material;
            if ($("#" + feature + "_lethers").find("#" + feature + "_" + prevMat + "_colors").length > 0) {
                matCode = prevMat;
            }
            $(".lether-container").addClass("none");
            $('#' + feature + '_lethers').removeClass('none');
            $(".step4_bnt_group a").removeClass('active');
            $(this).addClass('active');
            $('#' + feature + '_lethers a.mat-name').removeClass('active-leather');
            $('#' + feature + '_lethers div.lether-types li input[value="' + matCode + '"]').parent().find("a.mat-name").addClass('active-leather');
            $("#" + feature + "_lethers").find("div.lether-colors div.lether-color ").addClass('none').removeClass('active-leather-container');
            $("#" + feature + "_lethers").find("#" + feature + "_" + matCode + "_colors").removeClass('none').addClass('active-leather-container');
            CustomShoe.styling.sliders[feature][matCode].reloadSlider();
        },
        letherSelect: function () {
            var matCode = $(this).parent().find('input.mat-code').val();
            var feature = $(this).parent().find('input.feature-type').val();
            $(this).parents('ul').find('.active-leather').removeClass('active-leather');
            $(this).addClass('active-leather');
            $("#" + feature + "_lethers").find("div.lether-colors div.lether-color ").addClass('none').removeClass('active-leather-container');
            $("#" + feature + "_lethers").find("#" + feature + "_" + matCode + "_colors").removeClass('none').addClass('active-leather-container');
            CustomShoe.styling.sliders[feature][matCode].reloadSlider();

            $('#step4_design').show();
            $('#rotate-wrapper').show();
            $('#tab_wrap').show();
            $('.patinaDispImage').hide();
            $('.patinaSlider').hide();
            if ($(this).hasClass('patina')) {
                $('#step4_design').hide();
                $('#rotate-wrapper').hide();
                $('#tab_wrap').hide();
                $('.patinaDispImage').show();
                $('.patinaSlider').show();
                $('.show-patina:first').trigger('click');
            }
            var tmpText = $("#quarter_lethers").find('a.active-leather').text();
            if (typeof tmpText !== 'undefined' && tmpText !== '') {
                tmpText = tmpText.replace(' (NEW!)', '');
                $(".bottomHeading").text(tmpText);
            }
            $('.active-leather-li').removeClass('active-leather-li');
            $('.active-leather').parent().addClass('active-leather-li');
        },
        colorSelect: function () {
            if ($(this).hasClass('show-patina')) {
                var code = $(this).parent().find('.clr-code').val();
                $(".patina-container").hide();
                $(".patina-container-" + code).show();
                $(".patina-container-" + code).find('.view_patina:first').trigger('click');
                var arr = ['quarter', 'foxing', 'toe', 'vamp', 'eyestay'];
                $.each(arr, function (idx, val) {
                    ShoeModel[val].material = 'M0';
                    ShoeModel[val].matfolder = 'CP';
                    ShoeModel[val].color = 'C0';
                    ShoeModel[val].matId = '1';
                    ShoeModel[val].clrId = '1';
                });
                ModelRenderer.render();

                var feature = 'patina';
                var code = $(this).parents(".lether-color").find('input.mat-code').val();
                var folder = $(this).parents(".lether-color").find('input.mat-folder').val();
                var matId = $(this).parents(".lether-color").find('input.mat-id').val();
                var clrId = $(this).parent().find('.clr-id').val();
                var clrCode = $(this).parent().find('.clr-code').val();

                var tmpObj = {material: code, matfolder: folder, color: clrCode, matId: matId, clrId: clrId};
                $('.active-color').removeClass('active-color');
                $(this).parent().addClass('active-color');

                ShoeModel[feature] = tmpObj;
                $.each(CustomShoe.styling.sliders['Patina'], function (idx, val) {
                    val.reloadSlider();
                });
            } else {
                if (isTouch) {
                    CustomShoe.styling.showMaterialDetails(arguments[0].target);
                }
                delete ShoeModel['patina'];
                var feature = $(this).parents(".lether-color").data('feature');
                var code = $(this).parents(".lether-color").find('input.mat-code').val();
                var folder = $(this).parents(".lether-color").find('input.mat-folder').val();
                var matId = $(this).parents(".lether-color").find('input.mat-id').val();
                var clrId = $(this).parent().find('.clr-id').val();
                var clrCode = $(this).parent().find('.clr-code').val();
                $('#' + feature + '_' + code + '_colors input[value="' + ShoeModel[feature].color + '"]').parent().removeClass('active-color');
                $(this).parent().addClass('active-color');
                ShoeModel[feature].material = code;
                ShoeModel[feature].matfolder = folder;
                ShoeModel[feature].color = clrCode;
                ShoeModel[feature].matId = matId;
                ShoeModel[feature].clrId = clrId;
                var criteria = {
                    style: ShoeModel.style.styleName,
                    material: {matId: matId, colorId: clrId}
                };
                CustomShoe.reloadRelated(criteria, CustomShoe.styling.sliders.Related);
                ModelRenderer.render();
                return false;
            }
        },
        nextStep: function () {
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + 'create-a-custom-shoe/sole-attributes';
                return false;
            });
        },
        prevStep: function () {
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + "create-a-custom-shoe/design-features";
            }, 'Yes');
        },
        showMaterialDetails: function (elem) {
            if (!$(elem).parents('li').find('a').hasClass('show-patina')) {
                var a = $(elem).clone();
                var clrName = $(elem).parents('li').eq(0).find('input.clr_name').val();
                var matName = $(elem).parents('.lether-color').eq(0).find('.let-mat-name').val();
                var pos = $(elem).offset();
                pos.top = pos.top + 36;
                $('#material_hover_elament').css({left: pos.left + 'px', top: pos.top + 'px'});
                $('#material_hover_elament').find('img').remove();
                $('#material_hover_elament').find('.material_color_name span').text(clrName);
                $('#material_hover_elament').find('.material_name span').text(matName);
                $('#material_hover_elament').prepend(a);
                $('#material_hover_elament').show();
            }
        },
        hideMaterialDetails: function () {
            $('#material_hover_elament').hide();
        },
        bindEvents: function () {
            var tmp = [];
            $(".patina-container").each(function () {
                var slid = $(this).find("ul.patina-slider").bxSlider({
                    auto: false,
                    minSlides: 5,
                    maxSlides: 5,
                    slideWidth: 80,
                    slideMargin: 5,
                    moveSlides: 4,
                    infiniteLoop: true,
                    pager: false,
                    onSliderLoad: CustomShoe.styling.sliderLoadComplete
                });
                tmp.push(slid);
            });
            CustomShoe.styling.sliders['Patina'] = tmp;
            this.initSliders();
            $("body").on('click', '.view_patina', function () {
                $("#pat_img_disp").attr('src', $(this).data('image'))
            });
            $(".step4_bnt_group a").on('click', CustomShoe.styling.featureSelect);
            $(".lether-types a.mat-name").on('click', CustomShoe.styling.letherSelect);

            $('body').on('click', '.patina-trigger', function () {
                $(".mat-name.patina:visible").trigger('click');
            });

            $(".color-slider a.color-selector").on('click', CustomShoe.styling.colorSelect);
            if (!isTouch) {
                $(".color-slider a.color-selector").on('mouseenter', function () {
                    CustomShoe.styling.showMaterialDetails(arguments[0].target);
                });
            }
            $(".color-slider a.color-selector").on('mouseleave', CustomShoe.styling.hideMaterialDetails);
            $('#material_hover_elament').on('click', CustomShoe.styling.hideMaterialDetails);
            $(ModelRenderer.container.main).on('mousemove', 'img', function (e) {
                var visibleImg = '';
                $(this).css({'cursor': 'pointer'});
                var imgPos = $(this).offset();
                var mousePos = {x: e.pageX - parseInt(imgPos.left), y: e.pageY - parseInt(imgPos.top)};
                visibleImg = CustomShoe.styling.getPartOnPosition(mousePos.x, mousePos.y, e.pageX, e.pageY, false);
                if (visibleImg != '') {
                    var material = $("#" + visibleImg + "_" + ShoeModel[visibleImg].material + "_colors").find("input.let-mat-name").val();
                    var color = $('#' + visibleImg + '_' + ShoeModel[visibleImg].material + '_colors').find('ul.color-slider input.clr-code[value="' + ShoeModel[visibleImg].color + '"]').parent().find('input.clr_name').val();
                    CustomShoe.highlightFeture(e, material + " - " + color);
                }
            });
            $(ModelRenderer.container.main).on('mouseleave', 'img', function (e) {
                $(this).css({'cursor': 'default'});
                $('div#tooltip').remove();
                $(".part-buttons a").removeClass("highlight");
            });
//            $(ModelRenderer.container.main).off('click touchend').on('click touchend', 'img', function (e) {
            $(ModelRenderer.container.main).off('click touchend').on('click touchend', 'img', function (e) {
                $('div#tooltipTmp').remove();
                var imgPos = $(this).offset();
                var mousePos = {};

                if (e.type == 'touchstart' || e.type == 'touchmove' || e.type == 'touchend' || e.type == 'touchcancel') {
                    var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
                    mousePos = {x: touch.pageX - parseInt(imgPos.left), y: touch.pageY - parseInt(imgPos.top)}
                } else {
                    mousePos = {x: e.pageX - parseInt(imgPos.left), y: e.pageY - parseInt(imgPos.top)}
                }

                var canvasHeight = $(ModelRenderer.container.canvas).height();
                var canvasWidth = $(ModelRenderer.container.canvas).width();
                var imgHeight = $(this).height()
                var imgWidth = $(this).width();

                mousePos = {
                    x: ((mousePos.x * canvasWidth) / imgWidth),
                    y: ((mousePos.y * canvasHeight) / imgHeight),
                };

//                var visibleImg = CustomShoe.styling.getPartOnPosition(mousePos.x + 12, mousePos.y + 19, e.pageX + 12, e.pageY + 19, false);
                var visibleImg = CustomShoe.styling.getPartOnPosition(mousePos.x, mousePos.y, e.pageX, e.pageY, false);
                if ($('.part-buttons a[data-feature="' + visibleImg + '"]').length > 0) {
                    $('.part-buttons a[data-feature="' + visibleImg + '"]').trigger('click');
                } else {
                    $('.part-buttons a[data-feature="quarter"]').trigger('click');
                }
            });
            $("#rotate-wrapper").on('click touchend', 'a.rotate-left, a.rotate-right', function () {
                var direction = $(this).hasClass('rotate-left') ? ModelRenderer.config.rotateLeft : ModelRenderer.config.rotateRight;
                ModelRenderer.rotate(direction);
                return false;
            });
            $("body").on('click touchend', '#rotate-side,#rotate-top', CustomShoe.changeView);
            $(".next-btn").on('click', CustomShoe.styling.nextStep);
            $("#back-btn").on('click', CustomShoe.styling.prevStep);
            $((ShoeModel.angle == 'A7') ? '#rotate-top' : '#rotate-side').trigger('click');
        },
        initSliders: function () {
            var attribs = {
                controls: true,
                auto: false,
                minSlides: 7,
                maxSlides: 7,
                slideWidth: 127,
                slideMargin: 5,
                moveSlides: 6,
                infiniteLoop: false,
                pager: false,
                hideControlOnEnd: true,
                onSliderLoad: CustomShoe.styling.sliderLoadComplete
            };
            if (ModelRenderer.config.isMobile) {
                attribs.minSlides = 3;
                attribs.pager = false;
                attribs.slideWidth = 120;
                attribs.maxSlides = 3;
                attribs.moveSlides = 3;
                attribs.controls = true;
                attribs.slideMargin = 0;
            }
            var isRendered = false;

            $("div.lether-color").each(function () {
                var matCode = $(this).find(".mat-code").val();
                var feature = $(this).data("feature");
                var sldr = $(this).find("ul.color-slider").bxSlider(attribs);
                if (typeof CustomShoe.styling.sliders[feature] === 'undefined') {
                    isRendered = false;
                    CustomShoe.styling.sliders[feature] = {};
                }
                CustomShoe.styling.sliders[feature][matCode] = sldr;
                if (!isRendered && ShoeModel[feature].material === matCode) {
                    $("#" + feature + "_lethers").find("div.lether-colors div.lether-color ").addClass('none').removeClass('active-leather-container');
                    $("#" + feature + "_lethers").find("#" + feature + "_" + matCode + "_colors").removeClass('none').addClass('active-leather-container');
                    $("#" + feature + "_lethers .lether-types .active-leather").removeClass('active-leather');
                    $('#' + feature + '_lethers .lether-types input.mat-code[value="' + matCode + '"]').parent().find('a').addClass('active-leather');
                    $('.active-leather-li').removeClass('active-leather-li');
                    $('.active-leather').parent().addClass('active-leather-li');
                    CustomShoe.styling.sliders[feature][matCode].reloadSlider();

                    $('#' + feature + '_' + matCode + '_colors .active-color').removeClass('active-color');
                    $('#' + feature + '_' + matCode + '_colors input[value="' + ShoeModel[feature].color + '"]').parent().addClass('active-color');
                    isRendered = true;
                }
            });
            var custom = {
                minSlides: 3,
                maxSlides: 3,
                moveSlides: 2,
                sliderWidth: 300,
                hideControlOnEnd: true,
                pager: false
            };
            if (ModelRenderer.config.isMobile) {
                custom.minSlides = 1;
                custom.maxSlides = 1;
                custom.moveSlides = 1;
                custom.sliderWidth = 250;
                custom.pager = false;
            }
//            CustomShoe.styling.sliders['Related'] = $('.step4_bottom_slider').bxSlider({
//                auto: false,
//                minSlides: custom.minSlides,
//                maxSlides: custom.maxSlides,
//                slideWidth: custom.sliderWidth,
//                slideMargin: 5,
//                moveSlides: custom.moveSlides,
//                infiniteLoop: false,
//                pager: custom.pager,
//                hideControlOnEnd: true,
//                onSliderLoad: CustomShoe.styling.sliderLoadComplete
//            });
        },
        sliderLoadComplete: function () {
            if (!CustomShoe.styling.isPageLoaded) {
                CustomShoe.styling.sliderLength++;
                if (CustomShoe.styling.sliderLength === ($("div.part-buttons a").length + 1)) {
                    CustomShoe.styling.isPageLoaded = true;
                    $("#loader").fadeOut(300, function () {
                        $("#step4_shoes").fadeIn(300, function () {
                            CustomShoe.styling.sliders['quarter'][ShoeModel.quarter.material].reloadSlider();
//                            CustomShoe.styling.sliders['Related'].reloadSlider();
                            $.each(CustomShoe.styling.sliders['Patina'], function (idx, val) {
                                val.reloadSlider();
                            });
                        });
                    });
                }
            }
        },
        getPartOnPosition: function (pageX, pageY, mouseX, mouseY, isSvg) {
            if (typeof isSvg == 'undefined') {
                isSvg = true;
            }
            var hoverElem = '';
            $(ModelRenderer.container.main + " img").each(function (i, val) {
                j = i;
                var isToe = ($(this).attr('id') === ModelRenderer.container.toe.replace('#', ''));
                var isEyestay = ($(this).attr('id') === ModelRenderer.container.eyestay.replace('#', ''));
                var isVamp = ($(this).attr('id') === ModelRenderer.container.vamp.replace('#', ''));
                var isFoxing = ($(this).attr('id') === ModelRenderer.container.foxing.replace('#', ''));

                if (isToe || isEyestay || isVamp || isFoxing) {
                    if ($(this).is(":visible")) {
                        var canvas = $(ModelRenderer.container.canvas)[0];
                        canvas.width = canvas.width;
                        var ctx = canvas.getContext('2d');
//                        ctx.drawImage($('#' + $(this).attr('id'))[0], 0, 0, 720, 405, 0, 0, ModelRenderer.config.renderedWidth, ModelRenderer.config.renderedHeight);

                        ctx.drawImage($('#' + $(this).attr('id'))[0], 0, 0, $(".visualImgWrap  img").width(), $(".visualImgWrap  img").height(), 0, 0, $(".visualImgWrap  img").width(), $(".visualImgWrap  img").height());

                        var alpha = ctx.getImageData(pageX, pageY, 1, 1).data[3];
                        var tper = ((100 * alpha / 255) << 0);

                        if (tper !== 0) {
                            hoverElem = $(this).attr('id');
                            return false;
                        }
                    }
                }
            });
            if (hoverElem === '' && isSvg) {
                $(ModelRenderer.container.svg).css('z-index', 8);
                var elem = document.elementFromPoint(mouseX, (mouseY - $("body").scrollTop()));
                if (elem !== null && $(elem).attr('id') !== undefined) {
                    hoverElem = $(elem).attr('id').replace('path_', '');
                }
                $(ModelRenderer.container.svg).css('z-index', 0);
            } else {
                if (hoverElem == "") {
                    var canvas = $(ModelRenderer.container.canvas)[0];
                    canvas.width = canvas.width;
                    var ctx = canvas.getContext('2d');
//                    ctx.drawImage($(ModelRenderer.container.quarter)[0], 0, 0,
//                            720, 405, 0, 0, ModelRenderer.config.renderedWidth,
//                            ModelRenderer.config.renderedHeight);
                    var alpha = ctx.getImageData(pageX, pageY, 1, 1).data[3];
                    var tper = ((100 * alpha / 255) << 0);

                    if (tper !== 0) {
                        hoverElem = ModelRenderer.container.quarter.replace('#', '');
                    }
                }
            }
            return hoverElem;
        }
    },
    stitchlace: {
        sliders: {},
        sliderLength: 0,
        isPageLoaded: false,
        sliderCount: 0,
        init: function () {
            this.bindEvents();
            ShoeModel.angle = 'A7';
            $("#rotate-side").removeClass('current-item');
            $("#rotate-top").removeClass('current-item');
            $("#rotate-top").addClass('current-item');
            ModelRenderer.bindEvents();
            ModelRenderer.render();
            if (ShoeModel.monogram.text.trim() !== "") {
                $("#remove-monogram").css("visibility", "visible");
            }
        },
        stitchSelected: function () {
            $(".monogram").hide();
            $("#stitching-slider li").removeClass('active-color');
            $(this).parent().addClass('active-color');
            ShoeModel.stitch.code = $(this).find("input.stitch-code").eq(0).val();
            ShoeModel.stitch.name = $(this).data("tooltip");
            ShoeModel.stitch.id = $(this).find("input.stitch-id").eq(0).val();
            if (ShoeModel.angle !== 'A7') {
                ShoeModel.angle = 'A7';
                $("#rotate-side").removeClass('current-item');
                $("#rotate-top").removeClass('current-item');
                $("#rotate-top").addClass('current-item');
            }
            ModelRenderer.render();
        },
        laceSelected: function () {
            $(".monogram").hide();
            $("#lacing-slider li").removeClass('active-color');
            $(this).parent().addClass('active-color');
            ShoeModel.lace.code = $(this).find("input.lace-code").eq(0).val();
            ShoeModel.lace.name = $(this).data("tooltip");
            ShoeModel.lace.id = $(this).find('input.lace-id').eq(0).val();
            if (ShoeModel.angle !== 'A7') {
                ShoeModel.angle = 'A7';
                $("#rotate-side").removeClass('current-item');
                $("#rotate-top").removeClass('current-item');
                $("#rotate-top").addClass('current-item');
            }
            ModelRenderer.render();
        },
        changeMonogram: function () {
            ShoeModel.monogram.leftShoe = 1;
            ShoeModel.monogram.leftSide = 1;
            if ($(this).val().trim() === "" || /^[a-zA-Z()]+$/.test($(this).val())) {
                ShoeModel.monogram.text = $(this).val().toUpperCase();
//                if (ShoeModel.angle !== 'A4') {
//                    ShoeModel.angle = 'A4';
//                }
//                $("#rotate-side").removeClass('current-item');
//                $("#rotate-top").removeClass('current-item');

                if (ShoeModel.monogram.text.trim() !== "") {
                    $("#remove-monogram").css("visibility", "visible");
                } else {
                    $("#remove-monogram").css("visibility", "hidden");
                }
                ModelRenderer.render();
            } else {
                alert("Please use alphabets only.");
            }
        },
        removeMonogram: function () {
            ShoeModel.monogram.text = "";
            $("#monogram-text").val("");
            $("#monogram-text").trigger("keyup");
            $("#monogram-text").trigger("blur");
            $("#remove-monogram").css("visibility", "hidden");
//            ModelRenderer.render();
        },
        nextStep: function () {
            ShoeModel.shoetree = $('#shoeTreeChk').is(':checked');

            CustomShoe.saveModel(function () {
                fbq('track', 'AddToCart');
                window.location.href = baseUrl + 'cart';
                return false;
            });
        },
        prevStep: function () {
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + 'create-a-custom-shoe/sole-attributes';
                return false;
            }, 'Yes');
        },
        tabChange: function () {
            $(".monogram").hide();
            $('#tab_wrap > .tab').removeClass('current-item');
            $(this).addClass('current-item');
            var curtab = $(this).attr('id').replace('-tab', '');
            $('.slide-container').addClass('none');
            $('.' + curtab + '-tab').removeClass('none');

            if (typeof CustomShoe.stitchlace.sliders[curtab] != 'undefined') {
                CustomShoe.stitchlace.sliders[curtab].reloadSlider();
                if (ShoeModel.angle !== "A7") {
                    ShoeModel.angle = "A7";
                    ModelRenderer.render();
                    $("#rotate-side").removeClass('current-item');
                    $("#rotate-top").removeClass('current-item');
                    $("#rotate-top").addClass('current-item');
                }
            }

        },
        showOverlay: function () {
            if (!$('#monoGramChk').is(':checked')) {
                $('#monoGramChk').prop('checked', true);
                $("#monogram-complementary").addClass("check-blue-1");
            }
            $(".monogram-badge").show();
            if (ShoeModel.angle !== "A4") {
                ShoeModel.angle = "A4";
                ModelRenderer.render();
            }
            $("#rotate-side").removeClass('current-item');
            $("#rotate-top").removeClass('current-item');
            $("#overlay").show();
        },
        hideOverlay: function () {

            $("#overlay").hide();

//            if ($("#monogram-text").val().trim() === "" || /^[a-zA-Z()]+$/.test($("#monogram-text").val())) {
            if ($("#monogram-text").val().trim() === "") {
                $(".monogram").hide();
                $(".monogram-badge").hide();
                CustomShoe.tmp = '1';
                if ($('#monoGramChk').is(':checked')) {
                    $('#monoGramChk').prop('checked', false);
                    $("#monogram-complementary").removeClass("check-blue-1");
                }
                setTimeout(function () {
                    CustomShoe.tmp = '';
                }, 500);
            }

        },
        bindEvents: function () {
            this.initSliders();
            $('#tab_wrap > .tab').on('click touchend', CustomShoe.stitchlace.tabChange);
            $("#stitching-slider div.color").on('click touchend', CustomShoe.stitchlace.stitchSelected);
            if ($("#lacing-slider").length > 0) {
                $("#lacing-slider div.color").on('click touchend', CustomShoe.stitchlace.laceSelected);
            }
            $("#monogram-text").on('keyup', CustomShoe.stitchlace.changeMonogram);
            $("#monogram-text").on('focus', CustomShoe.stitchlace.showOverlay);
            $("#monogram-text").on('blur', CustomShoe.stitchlace.hideOverlay);
            $("#rotate-wrapper").on('click touchend', 'a.rotate-left, a.rotate-right', function () {
                var direction = $(this).hasClass('rotate-left') ? ModelRenderer.config.rotateLeft : ModelRenderer.config.rotateRight;
                ModelRenderer.rotate(direction);
                return false;
            });
            $("body").on('click touchend', '#rotate-side,#rotate-top', CustomShoe.changeView);
            $(".next-btn").on('click', CustomShoe.stitchlace.nextStep);
            $("#back-btn").on('click', CustomShoe.stitchlace.prevStep);
            $("#remove-monogram").on('click touchend', CustomShoe.stitchlace.removeMonogram);

            $("body").on('change', '#shoeTreeChk', function () {
                var pr = parseFloat($('.price').text().replace('$', ''));
                if ($(this).is(':checked')) {
                    pr = pr + parseFloat(SHOE_TREE_PRICE);
                } else if (pr > 0) {
                    pr = pr - parseFloat(SHOE_TREE_PRICE);
                }
                $('.price').text('$' + pr);

            });

            $("body").on('change', '#monoGramChk', function () {
                if (CustomShoe.tmp == '1') {
                    $("#monogram-complementary").removeClass("check-blue-1");
                    $(this).prop('checked', false);
                } else {
                    if ($(this).is(':checked')) {
                        $("#monogram-complementary").addClass("check-blue-1");
                        $("#monogram-text").trigger('focus');
                    } else {
                        $("#monogram-complementary").removeClass("check-blue-1");
                        $("#remove-monogram").trigger('click');
                    }
                }

            });

            $("div.color").on('mouseenter', function () {
                $("#color-tooltip").remove();
                var $tooltip = $('<div id="color-tooltip">').text($(this).data("tooltip"));
                $tooltip.hide();
                $("body").append($tooltip);
                var pos = $(this).offset();
                var height = $(this).outerHeight();
                var left = (pos.left + ($(this).outerWidth() / 2)) - ($tooltip.outerWidth() / 2);
                $tooltip.css({top: (pos.top + height) + "px", left: left});
                $tooltip.show(200);
            });
            $("div.color").on('mouseleave', function () {
                $("#color-tooltip").remove();
            });

            $(ModelRenderer.container.main).on('mousemove', 'img', function (e) {
                var visibleImg = '';
                var imgPos = $(this).offset();
                var mousePos = {x: e.pageX - parseInt(imgPos.left), y: e.pageY - parseInt(imgPos.top)};
                visibleImg = CustomShoe.stitchlace.getPrartOnPosition(mousePos.x, mousePos.y);
                if (visibleImg != '') {
                    CustomShoe.highlightFeture(e, ShoeModel[visibleImg].name);
                } else {
                    $('div#tooltip').remove();
                }
            });
            $(ModelRenderer.container.main).on('mouseleave', 'img', function (e) {
                $('div#tooltip').remove();
            });
            $((ShoeModel.angle == 'A0') ? '#rotate-side' : '#rotate-top').trigger('click');

            $("body").on('change', '#shoeTreeChk', function () {
                if ($(this).is(':checked')) {
                    $("#shoe-trees-block").addClass("check-blue");
                } else {
                    $("#shoe-trees-block").removeClass("check-blue");
                }
            })
        },
        initSliders: function () {
            var attribs = {
                auto: false,
                minSlides: 10,
                maxSlides: 10,
                slideWidth: 100,
                slideMargin: 5,
                moveSlides: 4,
                infiniteLoop: false,
                pager: false,
                hideControlOnEnd: true,
                onSliderLoad: CustomShoe.stitchlace.sliderLoadComplete
            };
            if (ModelRenderer.config.isMobile) {
                attribs.minSlides = 3;
                attribs.pager = false;
                attribs.maxSlides = 3;
                attribs.moveSlides = 3;
                attribs.slideWidth = 120;
                attribs.controls = true;
                attribs.slideMargin = 0;
            }
            CustomShoe.stitchlace.sliderCount = 1;
            if (ShoeModel.style.isLaceEnabled !== "0" && $("#lacing-slider").length) {
                this.sliders['lacing'] = $("#lacing-slider").bxSlider(attribs);
                CustomShoe.stitchlace.sliderCount = 2;
            }
            var custom = {
                minSlides: 3,
                maxSlides: 3,
                moveSlides: 2,
                sliderWidth: 300,
                pager: false
            };
            if (ModelRenderer.config.isMobile) {
                custom.minSlides = 1;
                custom.maxSlides = 1;
                custom.moveSlides = 1;
                custom.sliderWidth = 250;
                custom.pager = false;
            }
            if ($("#stitching-slider").length > 0) {
                this.sliders['stitching'] = $("#stitching-slider").bxSlider(attribs);
            } else {
                $("#loader").fadeOut(300, function () {
                    $("#step5_shoes").fadeIn(300, function () {
                    });
                });
            }
//            this.sliders['related'] = $('.step5_bottom_slider').bxSlider({
//                auto: false,
//                minSlides: custom.minSlides,
//                maxSlides: custom.maxSlides,
//                slideWidth: custom.sliderWidth,
//                slideMargin: 5,
//                moveSlides: custom.moveSlides,
//                infiniteLoop: false,
//                pager: custom.pager,
//                hideControlOnEnd: true,
//                onSliderLoad: CustomShoe.stitchlace.sliderLoadComplete
//            });
        },
        sliderLoadComplete: function () {
            if (!CustomShoe.styling.isPageLoaded) {
                CustomShoe.styling.sliderLength++;
                if (CustomShoe.styling.sliderLength === CustomShoe.stitchlace.sliderCount) {
                    CustomShoe.styling.isPageLoaded = true;
                    $("#loader").fadeOut(300, function () {
                        $("#step5_shoes").fadeIn(300, function () {
                            if (typeof CustomShoe.stitchlace.sliders.lacing != 'undefined') {
                                CustomShoe.stitchlace.sliders.lacing.reloadSlider();
                            } else {
                                CustomShoe.stitchlace.sliders.stitching.reloadSlider();
                            }
//                            CustomShoe.stitchlace.sliders.related.reloadSlider();
                        });
                    });
                }
            }
        },
        getPrartOnPosition: function (pageX, pageY) {
            var hoverElem = '';
            $(ModelRenderer.container.main + " img").each(function (i, val) {
                j = i;
                if ($(this).attr('id') === ModelRenderer.container.stitch.replace('#', '')
                        || $(this).attr('id') === ModelRenderer.container.lace.replace('#', '')) {
                    if ($(this).is(":visible")) {
                        var canvas = $(ModelRenderer.container.canvas)[0];
                        canvas.width = canvas.width;
                        var ctx = canvas.getContext('2d');
                        var alpha = ctx.getImageData(pageX, pageY, 1, 1).data[3];
                        var tper = ((100 * alpha / 255) << 0);
                        if (tper !== 0) {
                            hoverElem = $(this).attr('id');
                            return false;
                        }
                    }
                }
            });
            return hoverElem;
        }
    },
    sizing: {
        init: function () {
            this.bindEvents();
            $("#m_size_left").val(ShoeModel.measurement.left.size);
            $("#m_width_left").val(ShoeModel.measurement.left.width);
            $("#m_size_right").val(ShoeModel.measurement.right.size);
            $("#m_width_right").val(ShoeModel.measurement.right.width);

            $("#m_girth_left").val(ShoeModel.measurement.left.girth);
            $("#m_girth_right").val(ShoeModel.measurement.right.girth);
            $("#m_instep_left").val(ShoeModel.measurement.left.instep);
            $("#m_instep_right").val(ShoeModel.measurement.right.instep);

            $("#m_heel_left").val(ShoeModel.measurement.left.heel);
            $("#m_heel_right").val(ShoeModel.measurement.right.heel);
            $("#m_ankle_left").val(ShoeModel.measurement.left.ankle);
            $("#m_ankle_right").val(ShoeModel.measurement.right.ankle);

            if (ShoeModel.size.left.value !== '') {
                $('.CaptionCont').css('color', "#444");
                $("#left-size").val(ShoeModel.size.left.value);
                $('#left-size').parent().children(".SlectBox").children('span').text(ShoeModel.size.left.value);
                $("#right-size").val(ShoeModel.size.right.value);
                $('#right-size').parent().children(".SlectBox").children('span').text(ShoeModel.size.right.value);
            }
            if (ShoeModel.size.left.width !== '') {
                $('.CaptionCont').css('color', "#444");
                var tmpLwidth = ShoeModel.size.left.width;
                tmpLwidth = (tmpLwidth == 'D') ? 'Regular' : ((tmpLwidth == 'E') ? 'Wide' : ((tmpLwidth == 'EE') ? 'Extra Wide' : ((tmpLwidth == 'EEE') ? 'Triple Wide' : tmpLwidth)));

                var tmpRwidth = ShoeModel.size.left.width;
                tmpRwidth = (tmpRwidth == 'D') ? 'Regular' : ((tmpRwidth == 'E') ? 'Wide' : ((tmpRwidth == 'EE') ? 'Extra Wide' : ((tmpRwidth == 'EEE') ? 'Triple Wide' : tmpRwidth)));

                $("#left-width").val(tmpLwidth);
                $("#right-width").val(tmpRwidth);
                $('#right-width').parent().children(".SlectBox").children('span').text(ShoeModel.size.right.width);
                $('#left-width').parent().children(".SlectBox").children('span').text(ShoeModel.size.left.width);
            }
//            ModelRenderer.renderBase();
        },
        nextStep: function () {
            var lsize = $("#left-size").val();
            var rsize = $("#right-size").val();
            var lwidth = $("#left-width").val();
            var rwidth = $("#right-width").val();

            var m_lsize = $("#m_size_left").val();
            var m_rsize = $("#m_size_right").val();
            var m_lwidth = $("#m_width_left").val();
            var m_rwidth = $("#m_width_right").val();

            var m_lgirth = $("#m_girth_left").val();
            var m_rgirth = $("#m_girth_right").val();
            var m_linstep = $("#m_instep_left").val();
            var m_rinstep = $("#m_instep_right").val();

            var m_lheel = $("#m_heel_left").val();
            var m_rheel = $("#m_heel_right").val();
            var m_lankle = $("#m_ankle_left").val();
            var m_rankle = $("#m_ankle_right").val();

            if (lsize.trim() === "" || rsize.trim() === "" || lwidth.trim() === "" || rwidth.trim() === "") {
                alert("Please specify your shoe size");
                return false;
            }

            if ($(this).hasClass('validateSize')) {
                var valid = 0;
                $.each($(".onlyNumber"), function () {
                    if ($(this).val() == '') {
                        valid++;
                    }
                });

                if (valid > 0) {
                    alert("Please specify your custom measurements");
                    return false;
                }
            }

            ShoeModel.measurement.left.size = m_lsize;
            ShoeModel.measurement.left.width = m_lwidth;
            ShoeModel.measurement.right.size = m_rsize;
            ShoeModel.measurement.right.width = m_rwidth;

            ShoeModel.measurement.left.girth = m_lgirth;
            ShoeModel.measurement.left.instep = m_linstep;
            ShoeModel.measurement.right.girth = m_rgirth;
            ShoeModel.measurement.right.instep = m_rinstep;

            ShoeModel.measurement.left.heel = m_lheel;
            ShoeModel.measurement.left.ankle = m_lankle;
            ShoeModel.measurement.right.heel = m_rheel;
            ShoeModel.measurement.right.ankle = m_rankle;

            ShoeModel.measurement.size_type = $('#sizeType').val();

            ShoeModel.size.left.value = lsize;
            ShoeModel.size.left.text = lsize;
            ShoeModel.size.left.width = lwidth;
            ShoeModel.size.right.value = rsize;
            ShoeModel.size.right.text = rsize;
            ShoeModel.size.right.width = rwidth;

            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + "create-a-custom-shoe/select-style";
            });
        },
        bindEvents: function () {
            $('body').on('click touchend', '.sizeType', function () {
                $('.activeSize').removeClass('activeSize');
                $(this).addClass('activeSize');
                $('#sizeType').val($(this).text());
            });
//    -------------------------------------
            $('.onlyNumber').keypress(function (event) {
                var $this = $(this);
                if (event.which == 46 && $this.val() == '') {
                    event.preventDefault();
                }

//                var val = $this.val();
//                if (val.length == 5 && event.which != 8) {
//                    event.preventDefault();
//                } else if (val.length == 4 && event.which == 46) {
//                    event.preventDefault();
//                }

                if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                        ((event.which < 48 || event.which > 57) &&
                                (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                }

                var text = $(this).val();
                if ((event.which == 46) && (text.indexOf('.') == -1)) {
                    setTimeout(function () {
                        if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                            $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                        }
                    }, 1);
                }

                if ((text.indexOf('.') != -1) &&
                        (text.substring(text.indexOf('.')).length > 2) &&
                        (event.which != 0 && event.which != 8) &&
                        ($(this)[0].selectionStart >= text.length - 2)) {
                    alert('Please enter valid measurements or email customer care at wecare@awlandsundry.com for assistance.');
                    event.preventDefault();
                }
            });

            $('.onlyNumber').bind("paste", function (e) {
                var text = e.originalEvent.clipboardData.getData('Text');
                if ($.isNumeric(text)) {
                    if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
                        e.preventDefault();
                        $(this).val(text.substring(0, text.indexOf('.') + 3));
                    }
                } else {
                    e.preventDefault();
                }
            });
//    -------------------------------------
            $('#size-wrapper').on('change', 'select', function () {
                var lsize = $("#left-size").val();
                var rsize = $("#right-size").val();
                var lwidth = $("#left-width").val();
                var rwidth = $("#right-width").val();

                ShoeModel.size.left.value = lsize;
                ShoeModel.size.left.text = lsize;
                ShoeModel.size.left.width = lwidth;
                ShoeModel.size.right.value = rsize;
                ShoeModel.size.right.text = rsize;
                ShoeModel.size.right.width = rwidth;
            });
            $('#enter-size').on('change', 'input', function () {
                var m_lsize = $("#m_size_left").val();
                var m_rsize = $("#m_size_right").val();
                var m_lwidth = $("#m_width_left").val();
                var m_rwidth = $("#m_width_right").val();

                var m_lgirth = $("#m_girth_left").val();
                var m_rgirth = $("#m_girth_right").val();
                var m_linstep = $("#m_instep_left").val();
                var m_rinstep = $("#m_instep_right").val();

                var m_lheel = $("#m_heel_left").val();
                var m_rheel = $("#m_heel_right").val();
                var m_lankle = $("#m_ankle_left").val();
                var m_rankle = $("#m_ankle_right").val();

                ShoeModel.measurement.left.size = m_lsize;
                ShoeModel.measurement.left.width = m_lwidth;
                ShoeModel.measurement.right.size = m_rsize;
                ShoeModel.measurement.right.width = m_rwidth;

                ShoeModel.measurement.left.girth = m_lgirth;
                ShoeModel.measurement.left.instep = m_linstep;
                ShoeModel.measurement.right.girth = m_rgirth;
                ShoeModel.measurement.right.instep = m_rinstep;

                ShoeModel.measurement.left.heel = m_lheel;
                ShoeModel.measurement.left.ankle = m_lankle;
                ShoeModel.measurement.right.heel = m_rheel;
                ShoeModel.measurement.right.ankle = m_rankle;
            });

            $(".next-step").on("click", CustomShoe.sizing.nextStep);
            $(".step6_sizes").SumoSelect();
            $("#show-sizechart").on('click touchend', function () {
                $(".size_chart_view_wrapper").show();
            });

            $("#size-chart-close").on('click touchend', function () {
                $(".size_chart_view_wrapper").hide();
            });

            $("#left-size").on('change', function () {
                $('.CaptionCont').css('color', "#a0a0a0");
                if ($(this).val() !== '') {
                    $('.CaptionCont').css('color', "#444");
                }
                if ($("#right-size").val() == "") {
                    $('#right-size').parent().children(".SlectBox").children('span').text($("#left-size").val());
                    $("#right-size").val($("#left-size").val());
                    if ($("#left-width").val() == "") {
                        $("#left-width").val("Regular");
                        $('#left-width').parent().children(".SlectBox").children('span').text("Regular");
                    }
                    if ($("#right-width").val() == "") {
                        $("#right-width").val("Regular");
                        $('#right-width').parent().children(".SlectBox").children('span').text("Regular");
                    }
                }
            });

            $("#right-size").on('change', function () {
                $('.CaptionCont').css('color', "#a0a0a0");
                if ($(this).val() !== '') {
                    $('.CaptionCont').css('color', "#444");
                }
                if ($("#left-size").val() == "") {
                    $('#left-size').parent().children(".SlectBox").children('span').text($("#right-size").val());
                    $("#left-size").val($("#right-size").val());
                    if ($("#left-width").val() == "") {
                        $("#left-width").val("Regular");
                        $('#left-width').parent().children(".SlectBox").children('span').text("Regular");
                    }
                    if ($("#right-width").val() == "") {
                        $("#right-width").val("Regular");
                        $('#right-width').parent().children(".SlectBox").children('span').text("Regular");
                    }
                }
            });

            $("#left-width").on('change', function () {
                $('.CaptionCont').css('color', "#a0a0a0");
                if ($(this).val() !== '') {
                    $('.CaptionCont').css('color', "#444");
                }
                if ($("#left-width").val() != "") {
                    $('#left-width').parent().children(".SlectBox").children('span').text($("#left-width").val());
                }
            });

            $("#right-width").on('change', function () {
                $('.CaptionCont').css('color', "#a0a0a0");
                if ($(this).val() !== '') {
                    $('.CaptionCont').css('color', "#444");
                }
                if ($("#right-width").val() != "") {
                    $('#right-width').parent().children(".SlectBox").children('span').text($("#right-width").val());
                }
            });
        }
    }
};

$('body').on('click', '.menuLinks', function () {
    if (!$(this).hasClass('current-step')) {
        if (!$(this).closest('li').hasClass('bg-none') && !$(this).closest('li').hasClass('active-li')) {
            var href = $(this).data('href');
            CustomShoe.saveModel(function () {
                window.location.href = baseUrl + href;
                return false;
            }, 'Yes');
        }
    }
});