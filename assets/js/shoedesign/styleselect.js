$(document).ready(function () {
    fbq('trackCustom', 'CustomShoeStep2');

    CustomShoe.handleTop();
    CustomShoe.currentStep = "styleSelector";
    CustomShoe.loadModel(function (model) {
        ShoeModel = model;
        if (typeof ShoeModel.style.id != 'undefined') {
            $(".last-style-id[value='" + ShoeModel.style.id + "']").closest('li').find('a').trigger('click');
        }

        CustomShoe.styleSelector.init();
    });
});