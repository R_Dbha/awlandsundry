$(document).ready(function () {
    // $('#shop-filter').sliding_menu_js();
    console.log($(window).width()+"==>"+$(document).width())
    if (!$('.slicknav_menu').is(':visible')) {
        if($(window).width()>768)
            $('#shop-filter').height($(window).height() - 245);
    }
    $('#footer-wrapper').css({'visibility': 'hidden'});
    $('#footer-menu').css({'visibility': 'hidden'});
    $('.products-filter-heading').click(function () {
        if ($('#shop-filter').hasClass('products-filter-mobile-expand')) {
            $('#shop-filter').removeClass('products-filter-mobile-expand');
            $('body').removeClass('filter-scroll');
        } else {
            $('#shop-filter').addClass('products-filter-mobile-expand');
            $('body').addClass('filter-scroll');
        }
    });
    $('.filter-heading').click(function () {
        if ($(this).parent().hasClass('products-filter-mobile-expand')) {
            $(this).parent().removeClass('products-filter-mobile-expand');
            $('body').removeClass('filter-scroll');
        } else {
            $(this).parent().addClass('products-filter-mobile-expand');
            $('body').addClass('filter-scroll');
        }
    });
    $('div.product-filter-mobile-close').click(function () {
        $('#shop-filter').removeClass('products-filter-mobile-expand');
        $('body').removeClass('filter-scroll');
    });
    $('body').on('click', '.filter-option', filter_collection);
    $('body').on('click', '.filter-option-inner', filter_collection_inner);
    
    $('body').on('keyup', '.shoe-name', filter_collection_by_name);

    $('body').on('click', '.products-filter-clear', function () {
        filters = {
            'type': [],
            'style': [],
            'material': [],
            'color': [],
            'last':[],
            'shoe_name':""
        };
        $('.shoe-name').val('');
        $('#product-list').html('');
        $('.filter-option').each(function () {
            $(this).removeClass('active');
            $(this).find('input').prop("checked", false);
        });
        config.ended = false;
        config.limit = 6;
        config.start = 0;
        load_collections('filter');
    });

    $('#loader.spinner').css({
        'margin-left': $('.product-container').width() / 2 - 40,
    });
    $(window).on('resize', function () {
        $('#loader.spinner').css({
            'margin-left': $('.product-container').width() / 2 - 40,
        });
    });
    var i = 0;
    var imageLoaded = function () {
        i++;
        if (i === config.limit * 2) {
            $('#loader.spinner').hide();
            $('#product-list').show();
        }
    }
    $(".image-set img").each(function () {
        var tmpImg = new Image();
        tmpImg.onload = imageLoaded;
        tmpImg.src = $(this).attr('src');
    });

    // $('#loader.spinner').hide();
});
var filters = {
    'type': [],
    'style': [],
    'material': [],
    'color': [],
    'last':[],
     'shoe_name':""
};
var config = {
    'start': 0,
    'limit': 6,
    'ended': false,
    'requestSent': false,
}

function filter_collection_by_name() {
    config.start = 0;
    config.limit = 6;
    config.ended = false;
    filters.shoe_name = $(this).val();
    $('#product-list').html('');
    $('#loader.spinner').css({
        'margin-left': $('.product-container').width() / 2 - 40,
    });
    $('#loader.spinner').show();
    load_collections('filter');
}
function filter_collection(e) {
    if($(e.target).parent().hasClass('filter-option-inner') || $(e.target).parents().hasClass('filter-option-inner')){
        return;
    }
    config.start = 0;
    config.limit = 6;
    config.ended = false;
    var category = $(this).data('category');
    //console.log($(this).children('p'));
    var value = $(this).children('p').text();
    
    if ($(this).hasClass('active')) {
        $(this).children('.filter-container').hide();
        $(this).removeClass('active');
        var index = filters[category].indexOf(value);
        filters[category].splice(index, 1);
        $(this).find('input').prop("checked", false);
    } else {
        $(this).children('.filter-container').show();
        $(this).addClass('active');
        filters[category].push(value);
        $(this).find('input').prop("checked", true);
    }
    $('#product-list').html('');
    $('#loader.spinner').css({
        'margin-left': $('.product-container').width() / 2 - 40,
    });
    $('#loader.spinner').show();
    load_collections('filter');
}
function filter_collection_inner(e) {
    config.start = 0;
    config.limit = 6;
    config.ended = false;
    var category = $(this).data('category');
    var value = $(this).find('input').val();
    if ($(this).hasClass('active')) {
        $(this).children('.filter-container').hide();
        $(this).removeClass('active');
        var index = filters[category].indexOf(value);
        filters[category].splice(index, 1);
        $(this).find('input').prop("checked", false);
    } else {
        $(this).children('.filter-container').show();
        $(this).addClass('active');
        filters[category].push(value);
        $(this).find('input').prop("checked", true);
    }
    $('#product-list').html('');
    $('#loader.spinner').css({
        'margin-left': $('.product-container').width() / 2 - 40,
    });
    $('#loader.spinner').show();
    load_collections('filter');
}
function load_collections(type) {
    config.requestSent = true;
    $.ajax({
        type: "POST",
        url: baseUrl + "shop/get_collections",
        data: {
            'filters': filters,
            'config': config
        },
        dataType: 'html',
        success: function (response) {
            if (response == '') {
                config.ended = true;
            }
	    if(type == 'filter'){
		$('#product-list').html('');
	    }
            $('#product-list').append(response);
            config.requestSent = false;
            $('#loader.spinner').hide();
        }
    });
}

$(window).scroll(function () {
    if ($(window).scrollTop() + 500 >= $(document).height() - $(window).height()) {
        if (config.ended || config.requestSent) {
            return;
        }
        config.start = config.start + config.limit;
        config.limit = 3;
        load_collections('scroll');
    }

    if ($(window).scrollTop() >= 150) {
        $('body').addClass('scrollstop');
        if (!$('.slicknav_menu').is(':visible')) {
            $('#shop-filter').height($(window).height());
        }
    } else {
        $('body').removeClass('scrollstop');
        if (!$('.slicknav_menu').is(':visible')) {
            $('#shop-filter').height($(window).height() - $(window).scrollTop() - 20);
        }
    }
    return;
});
