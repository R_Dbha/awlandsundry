// Avoid `console` errors in browsers that lack a console..

function is_touch_device() {
    return (('ontouchstart' in window)
            || (navigator.MaxTouchPoints > 0)
            || (navigator.msMaxTouchPoints > 0));
}

var IS_IPAD = navigator.userAgent.match(/iPad/i) !== null;
var IS_IPHONE = (navigator.userAgent.match(/iPhone/i) !== null) || (navigator.userAgent.match(/iPod/i) !== null);
$(function() {


    if (IS_IPAD || IS_IPHONE) {

//        $('a').on('click touchend', function() {
//            var link = $(this).attr('href');
//            window.open(link, '_self'); // opens in new window as requested
//
//            return false; // prevent anchor click    
//        });
    }
});

(function() {
    if ($('body').hasClass('home')) {
        if (!('querySelector' in document && 'localStorage' in window && 'addEventListener' in window)) {
            alert('For a better site experience, please update your browser or use a different one');
        }
    }
}());
(function() {

    var method;

    var noop = function() {
    };

    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'

    ];

    var length = methods.length;

    var console = (window.console = window.console || {});



    while (length--) {

        method = methods[length];



        // Only stub undefined methods.

        if (!console[method]) {

            console[method] = noop;

        }

    }

}());



var baseHosturl = baseUrl;

var hostname = window.location.host;



var faqIdSplit = ''

var href = top.location.href;

var hash = window.location.hash;

var vars = hash.replace('#', '').split("/");
var s = vars[0], q = vars[1];
//    for (var i = 0; i < vars.length; i++) {
//        if (vars[i].match("^#s"))
//            s = vars[i].substring(3);
//        if (vars[i].match("^q"))
//            q = vars[i].substring(2);
//    }


//var faqId = href.substr(href.lastIndexOf('/') + 1);



function faqScroll(id) {

    var $target = $(id);
    offsetVal = ($target.index() === 0) ? 90 : 90;

    $('html, body').stop().animate({
        'scrollTop': $target.offset().top - offsetVal

    }, 400, 'swing', function() {

        //     target - 331;
    });
    $('.faq').find('.answer').addClass('hidden');
    if (q !== '') {
        $('.faq').find('.' + q).removeClass('hidden');
        q = '';
    }

}




// Place any jQuery/helper plugins in here.

function gotoStep1() {
    window.location.href = baseUrl + 'create-a-custom-shoe/select-style';

}

jQuery(document).ready(function() {
    $('.step_four .dropdown .carat').html('(US)');
    $('#promo_email').on('blur', function() {
        if ($(this).val() !== '') {
            $(this).addClass('email');
        } else {
            $(this).removeClass('email');
        }
    });

    $('.step_four .createStep5 a').on('click touchend', function() {
        CustomShoe.stepFour.nextStep('monogram');
    });
    $('.step_five .createStep3 a , .step_four .createStep3 a  ').on('click touchend', function() {
        window.location.href = baseUrl + 'create-a-custom-shoe/materials-and-colors';
    });
    $('.step_three .createStep2 a, .step_four .createStep2 a , .step_five .createStep2 a').on('click touchend', function() {
        window.location.href = baseUrl + 'create-a-custom-shoe/design-features';
    });
    $('.step_two .createStepLast a, .step_three .createStepLast a, .step_four .createStepLast a , .step_five .createStepLast a').on('click touchend', function() {
        messagepopup('step_last');
    });
    $('.step_two .createStep1 a, .step_three .createStep1 a, .step_four .createStep1 a , .step_five .createStep1 a').on('click touchend', function() {
        messagepopup('step1');
    });
    $('.step_style .createStep1 a').on('click touchend', function() {
        gotoStep1();
    });

    if (jQuery(this).find('.step_four').length) {

        jQuery('#menu_left').animate({left: "0"}, 1200);

        jQuery('#shoe_details').animate({right: "0"}, 1200);

        jQuery('#button_right').animate({right: "0"}, 1200);

    }



    if (jQuery(this).find('body.step_one').length) {

        jQuery('#visualization img').css("opacity", "0.1");

        jQuery('#menu_left').animate({left: "0"}, 1200);

    }



    if (jQuery(this).find('body.step_two').length) {

        jQuery('#shoe_summary').animate({left: "0"}, 1200);

        jQuery('#shoe_details').animate({right: "0"}, 1200, function() {

            jQuery('.color_menu').fadeIn('fast');

        });

    }



    if (jQuery('body').hasClass('designStep')) {

        jQuery("html,body").animate({scrollTop: 0}, "fast");

    }



    jQuery('.sidebar li').mouseover(function() {

        jQuery('.sidebar li.active').removeClass('active').addClass('unactive');

    });



    jQuery('.sidebar').mouseleave(function() {

        jQuery('.sidebar li.unactive').removeClass('unactive').addClass('active');

    });

    jQuery('.checkbox img.q_symbol').mouseover(function() {
        $(this).parent().find('.counter').show();

    });



    jQuery('.checkbox img.q_symbol').mouseleave(function() {

        $(this).parent().find('.counter').hide();

    });
    jQuery('.msgOption img.q_symbol').mouseover(function() {
        $(this).parent().find('.desc').show();

    });



    jQuery('.msgOption img.q_symbol').mouseleave(function() {

        $(this).parent().find('.desc').hide();

    });


    /*
     if (jQuery(this).find('.jcarousel').length) {
     
     jQuery('.jcarousel').jcarousel({
     
     vertical: true,
     
     scroll: 1
     
     });
     
     }
     */


    var toggle = function() {

        //jQuery('#shopping_cart_mini').stop(true, true).toggleClass("active", 200);

    };



    var dd = jQuery('.shopping_cart_mini');

    if (jQuery('html').hasClass('touch')) {
        jQuery("#shopping_cart_mini").on('tap', function() {

            if (dd.css("display") == "none")

            {
                if ($(document).find('.inspiration').length) {

                    dd.show().addClass("active").animate({"margin-top": "5"}, 200);
                } else {

                    dd.show().addClass("active").animate({"margin-top": "-1"}, 200);
                }

                jQuery('#toolbar .right ul li.cart').addClass('active');

            }

        });

        jQuery("#header").on('tap', function() {

            if (dd.css("display") == "block") {

                //  dd.hide();

                //    var mTop = dd.height() + 110;

                //    dd.removeClass("active");

                //   dd.css({"margin-top": "-" + mTop + "px"});

                //  jQuery('#toolbar .right ul li.cart').removeClass('active');

            }

        });

        jQuery("#middle_container").on('tap', function() {

            if (dd.css("display") == "block") {

                dd.hide();

                var mTop = dd.height() + 110;

                dd.removeClass("active");

                dd.css({"margin-top": "-" + mTop + "px"});

                jQuery('#toolbar .right ul li.cart').removeClass('active');

            }

        });

        jQuery('.cart').on('tap', function(e) {

            e.stopPropagation();

        });



        jQuery('#toolbar .right').on('tap', function(e) {

            e.stopPropagation();

        });

        dd.on('tap', function(e) {

            e.stopPropagation();

        });

        jQuery("#headerNavLinks li a").on('tap', function() {

            if ((jQuery(this).attr("id")) !== 'shopping_cart_mini') {

                dd.hide();

                var mTop = dd.height() + 110;

                dd.removeClass("active");

                dd.css({"margin-top": "-" + mTop + "px"});

                jQuery('#toolbar .right ul li.cart').removeClass('active');

            }

        });
    } else {

        jQuery("#shopping_cart_mini").mouseover(function() {

            if (dd.css("display") == "none")

            {
                if ($(document).find('.inspiration').length) {

                    dd.show().addClass("active").animate({"margin-top": "5"}, 200);
                } else {

                    dd.show().addClass("active").animate({"margin-top": "-1"}, 200);
                }

                jQuery('#toolbar .right ul li.cart').addClass('active');

            }

        });

        jQuery("#header").mouseover(function() {

            if (dd.css("display") == "block") {

                //  dd.hide();

                //    var mTop = dd.height() + 110;

                //    dd.removeClass("active");

                //   dd.css({"margin-top": "-" + mTop + "px"});

                //  jQuery('#toolbar .right ul li.cart').removeClass('active');

            }

        });

        jQuery("#middle_container").mouseover(function() {

            if (dd.css("display") == "block") {

                dd.hide();

                var mTop = dd.height() + 110;

                dd.removeClass("active");

                dd.css({"margin-top": "-" + mTop + "px"});

                jQuery('#toolbar .right ul li.cart').removeClass('active');

            }

        });

        jQuery('.cart').mouseover(function(e) {

            e.stopPropagation();

        });



        jQuery('#toolbar .right').mouseover(function(e) {

            e.stopPropagation();

        });

        dd.mouseover(function(e) {

            e.stopPropagation();

        });

        jQuery("#headerNavLinks li a").mouseover(function() {

            if ((jQuery(this).attr("id")) !== 'shopping_cart_mini') {

                dd.hide();

                var mTop = dd.height() + 110;

                dd.removeClass("active");

                dd.css({"margin-top": "-" + mTop + "px"});

                jQuery('#toolbar .right ul li.cart').removeClass('active');

            }

        });
    }


    var $cartPopup = jQuery('.shopping_cart_mini');

    var centerWindow = jQuery(window).width() / 2;

    var marRight = jQuery('.wrapper').width() / 2;

    var totRight = centerWindow - marRight;

    $cartPopup.css({
        'display': 'block',
        //'margin-right': '-' + ($('.wrapper').width() / 2) + 'px'

    });





    if (jQuery(this).find('#faq').length) {

        jQuery('.faq').first().find('h3').css("margin-top", "0");

        jQuery('.faq').first().find('header').css("padding-top", "0");

    }



    jQuery('.step_two #visualization img').off('click touchend').on('click touchend', function() {

        jQuery("#main").children("#menu_left").animate({left: "-200px"});

        jQuery("#main").children("#menu_right").animate({right: "-200px"});

        jQuery(".view_favorites, .view_inspiration").removeClass('active');

        jQuery(".view_inspiration").text('view inspiration');

        jQuery(".view_favorites").text('view my favorites');

        jQuery('#menu_left.toe_detailing').animate({left: "0"}, 1200);

        jQuery('.toe_selection').css("display", "block");

    });



    jQuery('.view_inspiration').off('click touchend').on('click touchend', function() {

        jQuery(this).toggleClass('active');

        if (jQuery(this).hasClass('active')) {

            jQuery('#menu_left.inspiration').animate({left: "0"}, 1200);

            jQuery(this).text('Close inspiration');

        } else {

            jQuery('#menu_left.inspiration').animate({left: "-200px"}, 1200);

            jQuery(this).text('view inspiration');

        }

    });



    jQuery('.close_menu').off('click touchend').on('click touchend', function() {

        if (jQuery(this).parent().hasClass('inspiration')) {

            jQuery(this).parent().animate({left: "-200px"});

            jQuery(".view_inspiration").removeClass('active');

            jQuery(".view_inspiration").text('view inspiration');

        } else {

            jQuery(this).parent().animate({right: "-200px"});

            jQuery(".view_favorites").removeClass('active');

            jQuery(".view_favorites").text('view my favorites');

        }

    });



    jQuery('.view_favorites').off('click touchend').on('click touchend', function() {

        jQuery(this).toggleClass('active');

        if (jQuery(this).hasClass('active')) {

            jQuery('#menu_right.favorites').animate({right: "0"}, 1200);

            jQuery(this).text('Close favorites');

        } else {

            jQuery('#menu_right.favorites').animate({right: "-200px"}, 1200);

            jQuery(this).text('View my favorites');

        }

    });



    jQuery('#toe_detailing li, .detail').off('click touchend').on('click touchend', function() {

        jQuery('#menu_right.material').animate({right: "0"}, 1200);

        jQuery('.material #design_menu .expanded').next('.level2').hide(function() {

            jQuery('.inspiration #design_menu .expanded').removeClass('expanded');

        });

        jQuery('.material #design_menu li:first').addClass('expanded');

        jQuery('.material #design_menu li:first').next('.level2').show();

    });



    jQuery(function() {

        if (jQuery('body').hasClass('designStep')) {

            jQuery('#header').addClass('headerTop');

            jQuery('#toolbar').addClass('toolbarScroll');

            jQuery('#logo a').data('size', 'small');

            jQuery('#logo a').stop().css({'background-image': "url('" + baseHosturl + "assets/img/logo-small.png')"});

            jQuery('#toolbar').sticky({topSpacing: 0});

        } else {

            jQuery('#logo a').data('size', 'big');

        }

    });



    if (jQuery(this).find('body.designStep').length) {

    } else {

        jQuery(window).scroll(function() {

            if (jQuery(document).scrollTop() > 0)

            {



            }

            else

            {



            }

        });

        jQuery('#toolbar').sticky({topSpacing: 0});
        jQuery('body.about #logo,body.giftcard #logo, body.contact #logo,body.accounts #logo, body.inspiration #logo, body.cart #logo').sticky({topSpacing: 0});
        jQuery('.about .patterned-rule-blue, .giftcard .patterned-rule-blue').sticky({topSpacing: 30});
    }



    if (jQuery(this).find('.patterned-rule-blue').length) {

        //jQuery('.patterned-rule-blue').sticky({topSpacing: 38});

    }



    if (jQuery(this).find('.cart h2').length) {

        jQuery('.cart .main h2').sticky({topSpacing: 39})

    }



    if (jQuery(this).find('.headlines').length) {

        jQuery('.headlines').sticky({topSpacing: 90})

    }

    if (jQuery(this).find('body.giftcard').length) {
        jQuery('#giftcard').sticky({topSpacing: 85});
        jQuery('#card').sticky({topSpacing: 85});
    }


    if (jQuery(this).find('body.about').length) {

        //jQuery('.patterned-rule-blue').sticky({topSpacing: 42});

        jQuery('section.sidebar').sticky({topSpacing: 92});

    }







    if (jQuery(this).find('video,audio').length) {

        jQuery('video,audio').mediaelementplayer();

    }



    if (jQuery(this).find('.accounts').length) {

        //jQuery('.accounts #secondary').sticky({topSpacing:38});

    }



    if (jQuery(this).find('body.inspiration').length) {

        jQuery('.inspiration #main').sticky({topSpacing: 38});

    }

    if (jQuery(this).find('body.designStep').length) {

        jQuery('.designStep #stepMain').sticky({topSpacing: 39});

    }



    jQuery('#base_last li').off('click touchend').on('click touchend', function() {

        var title = jQuery(this).data("item-title");

        var subtitle = jQuery(this).data("item-subtitle");

        var desc = jQuery(this).data("item-desc");

        jQuery('#visualization img').css("opacity", "1");

        jQuery('#visualization .overlay').css("display", "none");

        jQuery('#base_last li').removeClass('active');

        jQuery(this).addClass('active');

        jQuery('#shoe_details .item-title').text(title);

        jQuery('#shoe_details .item-subtitle').text(subtitle + ' toe');

        jQuery('#shoe_details .item-desc').text(desc);

        jQuery('#menu_right').animate({right: '0'}, 1200);

    });
    choose_amount();
    function choose_amount() {
        if ($('#choose_amount').val() === '-1') {
            $('#custom_amount').show();
        } else {
            $('#custom_amount').hide();
        }
    }
    $('#choose_amount').change(function() {
        if ($(this).val() === '-1') {
            $('#custom_amount').show();
        } else {
            $('#custom_amount').hide();
        }
    });

    jQuery('#closure_type li').off('click touchend').on('click touchend', function() {

        var type = jQuery(this).data("item-type");

        jQuery('#shoe_details .item-type').text(type + ' lace-up');

        jQuery('#closure_type li').removeClass('active');

        jQuery(this).addClass('active');

        jQuery('#menu_right').animate({right: '0'}, 1200);

    });



    jQuery('.color_menu a').on(is_touch_device() ? 'tap' : 'click', function(e) {

        e.preventDefault();

        if (jQuery(this).parent().hasClass('dropdown')) {

            jQuery('#main').find('.dropdown').removeClass('dropdown');

            jQuery(this).parent().removeClass('dropdown');

        } else {

            jQuery('#main').find('.dropdown').removeClass('dropdown');

            jQuery(this).parent().addClass('dropdown');

        }

        jQuery('#main').children('#menu_left').animate({left: "-200px"}, 1200);

        jQuery('#main').children('#menu_right').animate({right: "-200px"}, 1200);
        return false;

    });



    jQuery('.color_menu ul').mouseleave(function() {

        //jQuery(this).parent().removeClass('dropdown');

    });



    if (jQuery(this).find('body.cart').length) {
        jQuery('.cart select').dropkick({
            theme: 'black',
            change: function(value, label) {
                $(this).dropkick('theme', value);
            }
        });
    }





    if (jQuery(this).find('body.contact').length) {





        jQuery('#contact_top').sticky({topSpacing: 35});


        $('#faq a[href^="#"]').off('click touchend').on('click touchend', function(e) {

            e.preventDefault();

            var target = this.hash

            faqScroll(target);



        });









        jQuery(this).find('.answer').addClass('hidden');

        jQuery(".question").off('click touchend').on('click touchend', function() {

            if (jQuery(this).next('.answer').hasClass('hidden')) {

                $("li .sizingChart:visible").hide();

                jQuery(".faq").find('.answer').addClass('hidden');

                jQuery(this).next('.answer').removeClass('hidden');

            } else {

                $("li .sizingChart:visible").hide();

                jQuery(this).next('.answer').addClass('hidden');

                jQuery(".faq").find('.answer').addClass('hidden');

            }

        });

    }

    /*	jQuery.fn.center = function () {
     
     this.css("position","absolute");
     
     this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
     
     $(window).scrollTop()) + "px");
     
     this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
     
     $(window).scrollLeft()) + "px");
     
     return this;
     
     }
     
     jQuery(".login .main").center();	
     
     */

    /*jQuery('.inspiration .item').click(function() {
     
     jQuery('#popupOverlay').css("display", "block");
     
     jQuery('#design_menu li:first').addClass('expanded');
     
     jQuery('#design_menu li:first').next('.level2').show();		
     
     var $selectionPopup = jQuery(".popup");
     
     jQuery('#design_menu li:first').addClass('expanded');
     
     jQuery('#design_menu li:first').next('.level2').show();		
     
     $selectionPopup.css("opacity", "0");	
     
     $selectionPopup.css({
     
     'display': 'block',
     
     'top': $(window).height()/2 + $(window).scrollTop(),
     
     'left': $(window).width()/2,
     
     'margin-left': '-' + ($selectionPopup.width() / 2) + 'px',
     
     'margin-top': '-' + (545 / 2) + 'px'
     
     });
     
     $selectionPopup.css("opacity", "1");	
     
     
     
     jQuery('#selection_popup_content').animate({right: "210px"}, 1200, function(){
     
     jQuery('.popup #community').animate({"right": "770px"}, 1);
     
     });
     
     jQuery('#side_menu').animate({right: "15px"}, 1200);
     
     
     
     return false;
     
     });*/



    jQuery('#inspiration li').off('click touchend').on('click touchend', function() {

        jQuery('#inspiration_popup #community').css("opacity", 1);

        jQuery(this).parent().find('.active').removeClass('active');

        jQuery(this).addClass('active');

        jQuery('#inspiration_popup #design_menu li').parent().find('.expanded').next('.level2').hide();

        jQuery('#inspiration_popup #design_menu li').parent().find('.expanded').removeClass();

        jQuery('#inspiration_popup #design_menu li:first').next('.level2').toggle('fast');

        jQuery('#inspiration_popup #design_menu li:first').toggleClass('expanded');

        var $selectionPopup = jQuery("#inspiration_popup");

        $selectionPopup.css("opacity", "0");

        $selectionPopup.css({
            'display': 'block',
            'width': 0,
            'right': '50%',
            'margin-right': '-490px',
        });

        $selectionPopup.css("opacity", "1");

        $selectionPopup.animate({width: "785px"}, 800, 'linear')

        return false;

    });

    $('.share_btns').off('click touchend').on('click touchend', function(event) {
        if (typeof ShoeModel.shoeDesignId === undefined) {
            event.preventDefault();
            messagepopup('share');
        }

    });

//    jQuery('.cart .shoe img').off('click touchend').on('click touchend',function() {
//        //console.log(arguments);
//        //alert($(this).val());
//        jQuery('#cartpopupOverlay').css("display", "block");
//
//        var $selectionPopup = jQuery("#selection_popup");
//        //console.log($(this).parents('.shoe'));
//        var shoe = $(this).parents('.shoe');
//        $("#visualization").html(shoe.html());
//        var cartId = $(this).parents(".cart-item").find(".cartId").val();
//        $("#cart_item_detail_id").val(cartId);
//        $.ajax({
//            type: "POST",
//            url: baseUrl + "index.php/shoecart/getShoeDesignIdFromCartId",
//            data: {
//                cartId: cartId
//            },
//            //dataType:'json',
//            success: function(response) {
//                console.log(response);
//                $selectionPopup.css("opacity", "0");
//                $selectionPopup.css({
//                    'display': 'block',
//                    'top': '90px',
//                    'width': '543px',
//                    'right': $(window).width() / 2,
//                    'margin-right': '-255.5px'
//                });
//                $selectionPopup.css("opacity", "1");
//                var href = $('#twitter').attr('href');
//                var new_href = href.replace('##', response.shoe_design_id);
//                $('#twitter').attr('href', new_href);
//
//                href = $('#fb').attr('href');
//                new_href = href.replace('##', response.shoe_design_id);
//                $('#fb').attr('href', new_href);
//
//                href = $('#pinterest').attr('href');
//                new_href = href.replace('##', response.image_file);
//                $('#pinterest').attr('href', new_href);
//
//                jQuery('#selection_popup_content').animate({right: "0"}, 1200);
//            }
//        });
//
//
//
//
//        return false;
//
//    });



    /*jQuery('#favorites li').click(function() {
     
     jQuery('#favorites_popup').css("display", "block");
     
     jQuery(this).parent().find('.active').removeClass('active');
     
     jQuery(this).addClass('active');
     
     jQuery('.popup #design_menu li').parent().find('.expanded').next('.level2').hide();
     
     jQuery('.popup #design_menu li').parent().find('.expanded').removeClass();
     
     jQuery('.popup #design_menu li:first').next('.level2').toggle('fast');
     
     jQuery('.popup #design_menu li:first').toggleClass('expanded');		
     
     var $selectionPopup = jQuery("#favorites_popup");
     
     jQuery('#favorites_popup .level1 li:first').addClass('expanded');
     
     jQuery('#favorites_popup .level1 li:first').next('.level2').show();		
     
     $selectionPopup.css("opacity", "0");	
     
     $selectionPopup.css({
     
     'display': 'block',
     
     'width': 0,
     
     'left': $(window).width()/2,
     
     'margin-left': '-490px',
     
     });
     
     $selectionPopup.css("opacity", "1");	
     
     $selectionPopup.animate({width: "785px"}, 800, 'linear')
     
     });*/
    function editYourOrderClose() {
        //$(".messageBox").removeClass('popupbounce');
        $('.messageBox').animate({"height": "0", "top": "0"}, 500, function() {
            $('.messageBox').css("display", "none");
            $('#popupOverlay').fadeOut();
        });
    }

    jQuery('#cartpopupOverlay').off('click touchend').on('click touchend', function() {
        jQuery('#selection_popup_content').animate({right: "-560px"}, 1200, function() {

            jQuery('.popup').css("display", "none");

            jQuery('#cartpopupOverlay').css("display", "none");

        });

        return false;


    });
    jQuery('#popupOverlay').off('click touchend').on('click touchend', function() {
        if (!$('body').hasClass('checkout')) {
            closeSelectionPopup();

            closeFavoritesPopup();

            messagepopupclose();

            if (jQuery('body').hasClass('cart')) {

                editYourOrderClose();

            }
        }
    });



    /*	jQuery('.popup .community').click(function() {
     
     var toogleRight = jQuery('.popup #community').css("right") == "575px" ? "770px" : "575px";
     
     jQuery('.popup #community').animate({"right": toogleRight});
     
     });
     
     */



    jQuery('#accounts_selection_popup .community').off('click touchend').on('click touchend', function() {

        jQuery("#accounts_selection_popup #community").show();

        var toggleRight = jQuery("#accounts_selection_popup #community").css("right") == "770px" ? "575px" : "770px";

        jQuery("#accounts_selection_popup #community").animate({"right": toggleRight});

    });



    jQuery('#inpiration_selection_popup .community').off('click touchend').on('click touchend', function() {

        jQuery("#inpiration_selection_popup #community").show();

        var toggleRight = jQuery("#inpiration_selection_popup #community").css("right") == "770px" ? "575px" : "770px";

        jQuery("#inpiration_selection_popup #community").animate({"right": toggleRight});

    });





    jQuery('#inspiration_popup .community').off('click touchend').on('click touchend', function() {

        var toggleWidth = jQuery("#inspiration_popup").width() == 785 ? "980px" : "785px";

        jQuery('#inspiration_popup').animate({'width': toggleWidth}, 200, 'linear');

        var toggleWidthContent = jQuery('#inspiration_popup #selection_popup_content').css("left") == "15px" ? "210px" : "15px";

        jQuery('#inspiration_popup #selection_popup_content').animate({left: toggleWidthContent}, 200, 'linear');

        var toggleWidthMenu = jQuery('#inspiration_popup #side_menu').css("left") == "575px" ? "770px" : "575px";

        jQuery('#inspiration_popup #side_menu').animate({left: toggleWidthMenu}, 200, 'linear');

    });



    jQuery('#favorites_popup .community').off('click touchend').on('click touchend', function() {

        var toggleWidth = jQuery("#favorites_popup").width() == 785 ? "980px" : "785px";

        jQuery('#favorites_popup').animate({'width': toggleWidth}, 200, 'linear');

        var toggleWidthContent = jQuery('#favorites_popup #selection_popup_content').css("right") == "15px" ? "210px" : "15px";

        jQuery('#favorites_popup #selection_popup_content').animate({right: toggleWidthContent}, 200, 'linear');

        var toggleWidthMenu = jQuery('#favorites_popup #side_menu').css("right") == "575px" ? "770px" : "575px";

        jQuery('#favorites_popup #side_menu').animate({right: toggleWidthMenu}, 200, 'linear');

    });



    jQuery('.apply').off('click touchend').on('click touchend', function() {

        jQuery(this).toggleClass('active');

        if (jQuery(this).text() == "Apply to design")
            jQuery(this).text('Design applied');

        else
            jQuery(this).text('Apply to design');

    });



    jQuery('#inspiration_popup .close').off('click touchend').on('click touchend', function() {

        if (jQuery("#inspiration_popup").width() == 980) {

            var toggleWidth = jQuery("#inspiration_popup").width() == 785 ? "980px" : "785px";

            jQuery('#inspiration_popup').animate({'width': toggleWidth}, 200, 'linear');

            var toggleWidthContent = jQuery('#inspiration_popup #selection_popup_content').css("left") == "15px" ? "210px" : "15px";

            jQuery('#inspiration_popup #selection_popup_content').animate({left: toggleWidthContent}, 200, 'linear');

            var toggleWidthMenu = jQuery('#inspiration_popup #side_menu').css("left") == "575px" ? "770px" : "575px";

            jQuery('#inspiration_popup #side_menu').animate({left: toggleWidthMenu}, 200, 'linear');

        }

        jQuery('#inspiration_popup').animate({width: "0px"}, 800, function() {

            jQuery('#inspiration_popup').css("display", "none");

        });

        return false;

    });



    jQuery('#favorites_popup .close').off('click touchend').on('click touchend', function() {

        if (jQuery("#favorites_popup").width() == 980) {

            var toggleWidth = jQuery("#favorites_popup").width() == 785 ? "980px" : "785px";

            jQuery('#favorites_popup').animate({'width': toggleWidth}, 200, 'linear');

            var toggleWidthContent = jQuery('#favorites_popup #selection_popup_content').css("left") == "15px" ? "210px" : "15px";

            jQuery('#favorites_popup #selection_popup_content').animate({left: toggleWidthContent}, 200, 'linear');

            var toggleWidthMenu = jQuery('#inspiration_popup #side_menu').css("left") == "575px" ? "770px" : "575px";

            jQuery('#favorites_popup #side_menu').animate({left: toggleWidthMenu}, 200, 'linear');

        }

        jQuery('#favorites_popup').animate({width: "0px"}, 800, function() {

            jQuery('#favorites_popup').css("display", "none");

        });

        return false;

    });







    /*jQuery('.popup .close').click(function() {
     
     closeSelectionPopup();
     
     return false;
     
     });*/



    jQuery('.cart .popup .close').off('click touchend').on('click touchend', function() {

        jQuery('#selection_popup_content').animate({right: "-560px"}, 1200, function() {

            jQuery('.popup').css("display", "none");

            jQuery('#cartpopupOverlay').css("display", "none");

        });

        return false;

    });





    jQuery('#design_menu li, .inspiration #design_menu li, .favorites #design_menu li, .material #design_menu li').off('click touchend').on('click touchend', function() {

        jQuery(this).parent().find('.expanded').next('.level2').hide();

        jQuery(this).parent().find('.expanded').removeClass();

        jQuery(this).next('.level2').toggle('fast');

        jQuery(this).toggleClass('expanded');

    });



    /*jQuery("#links ul li.account").hover(function(){
     
     jQuery("#account_content .action").css("background", "white");
     
     }, function() {
     
     jQuery("#account_content .action").css("background", "none");
     
     });*/



    $('#links ul .giftcard , #links ul .account').on('click touch', function() {
        var elemClass = $(this).attr('class');
        var $elem = jQuery(".action > ." + elemClass);
        var $siblings = $elem.siblings();
        $siblings.addClass("hidden");
        $elem.removeClass("hidden");

    });

    jQuery(".links ul li, #design_main_links ul li, #links ul li:not(.account )").hover(function() {
        var elemClass = $(this).attr('class');
        var $elem = jQuery(".action > ." + elemClass);
        var $siblings = $elem.siblings();
        $siblings.addClass("hidden");
        $elem.removeClass("hidden");
        jQuery('.top .account span').css({"height": "85px"});
    }, function() {
        if ($(this).hasClass('giftcard')) {
            return;
        }
        var $elem = jQuery(".action .default");
        var $siblings = $elem.siblings();
        $siblings.addClass("hidden");
        $elem.removeClass("hidden");
        jQuery('.top .account span').css({"height": "105px"});
    });



    jQuery("#toe_detailing li").mouseover(function() {



        var elemClass = $(this).attr('class').split(" ")[0];

        var $elem = jQuery("#details .detail." + elemClass);

        var $siblings = jQuery('#details').children('.detail');



        $siblings.addClass("hidden");

        $elem.delay(1000).queue(function() {

            jQuery(this).removeClass("hidden");

        });

    });



    jQuery("#details .detail").mouseleave(function() {

        var $siblings = jQuery('#details').children();

        setTimeout(function() {

            $siblings.addClass("hidden");

        }, 200);

    });



    jQuery("#menu_right.material ul.level2 li").mouseover(function() {


        var elemClass = $(this).attr('class');

        var $elem = jQuery("#materials .material." + elemClass);

        var $siblings = jQuery('#materials').children();



        $siblings.addClass("hidden");

        setTimeout(function() {

            $elem.removeClass("hidden");

        }, 2000);

    });



    /*jQuery("#materials .material").mouseleave(function(){
     
     var $siblings = jQuery('#materials').children();
     
     setTimeout(function(){		
     
     $siblings.addClass("hidden");
     
     },200);
     
     });	*/



    // code for validation - checkout

    jQuery(".numericOnly").keydown(function(e) {

        if (!(e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 46 || (e.keyCode >= 35 && e.keyCode <= 40) || (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105))) {

            e.preventDefault();

        }

    });



    jQuery('.alphaNumericOnly').keydown(function(e) {

        var code = e.keyCode ? e.keyCode : e.which;

        var pressedKey = String.fromCharCode(code);

        var alphaNumeric = /^[(a-z)(A-Z)(0-9)]+$/;

        //var alphaNumeric = /[a-zA-Z0-9]/g;

        //var alphaNumeric = /^([-a-z0-9_ ])+$/i;

        if (!pressedKey.match(alphaNumeric))

        {

            if (!(e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 46 || e.keyCode == 188 || e.keyCode == 32 || (e.keyCode >= 35 && e.keyCode <= 40))) {

                e.preventDefault();

            }

        }

    });



});







jQuery(document).mouseup(function(e)

{

    var container = jQuery(".shopping_cart_mini");

    if (!container.is(e.target) && container.has(e.target).length === 0)

    {

        var mTop = jQuery('.shopping_cart_mini').height() + 110;

        jQuery('.shopping_cart_mini').removeClass("active").animate({"margin-top": "-" + mTop + "px"}, 500, function() {

            jQuery('#toolbar .right ul li.cart').removeClass('active');

            jQuery('.shopping_cart_mini').hide();

        });

    }

});


/*
 jQuery(document).mouseup(function (e)
 
 {
 
 //console.log(e.target);
 
 var colorSelectionTab = jQuery('.colorsMenuWrap a');
 
 var colorSelectionCon = jQuery('.colorsMenuWrap li img');
 
 var toolTipContentDiv = jQuery('#materials');
 
 var container = jQuery(".designStep .shape_selection_popup");
 
 if (!container.is(e.target) && container.has(e.target).length === 0)  
 
 {
 
 if (!colorSelectionTab.is(e.target) && colorSelectionTab.has(e.target).length === 0)  
 
 {
 
 if (!colorSelectionCon.is(e.target) && colorSelectionCon.has(e.target).length === 0)  
 
 {
 
 if (!toolTipContentDiv.is(e.target) && toolTipContentDiv.has(e.target).length === 0)  
 
 {
 
 if(container.css("display")!='none')
 
 {
 
 $(".colors_popup").animate({top: "-40"}, 300, function(){
 
 $(".colors_popup").hide();
 
 });
 
 container.animate({height: "0"}, 1200, function(){
 
 container.removeClass("popup_alive");
 
 //jQuery('.designStep #stepMain').css({'position':'fixed','top':'50'});
 
 container.hide();
 
 });
 
 }
 
 }
 
 }
 
 }
 
 }
 
 });
 
 
 */


function closeSelectionPopup() {

    /*	jQuery('#community').animate({"right": "400px"}, 300, function() {
     
     if (jQuery('.popup').find('#side_menu').length) {
     
     jQuery('#community').animate({"right": "-560px"}, 100);
     
     jQuery('#selection_popup_content').animate({right: "-560px"}, 1400);
     
     jQuery('#side_menu').animate({right: "-560px"}, 1400, function() {
     
     jQuery('.popup').css("display","none");
     
     jQuery('#popupOverlay').css("display", "none");
     
     });
     
     } else {
     
     jQuery('#selection_popup_content').animate({right: "-560px"}, 1200, function() {
     
     jQuery('.popup').css("display","none");
     
     jQuery('#popupOverlay').css("display", "none");
     
     });
     
     }
     
     });*/

    jQuery('.popup').animate({height: "0"}, 500, function() {

        jQuery('.popup').css("display", "none");

        jQuery('#popupOverlay').css("display", "none");

        jQuery('.popup').html("");

    });


    if (jQuery('body').hasClass('inspiration') || jQuery('body').hasClass('accounts')) {
        jQuery('body').css('overflow', 'visible');
    }

}



function closeFavoritesPopup() {

    jQuery('#favorites_popup #community').removeClass('showed');

    jQuery('#favorites_popup #community').animate({left: "-560px"}, 800, function() {

        jQuery('#favorites_popup #community').hide();

        jQuery('#favorites_popup #community').animate({left: "-560px"}, 800);

        jQuery('#favorites_popup #selection_popup_content').animate({left: "-560px"}, 1200);

        jQuery('#favorites_popup #side_menu').animate({left: "-640px"}, 1200, function() {

            jQuery('#favorites_popup').css("display", "none");

        });

    });

}















function messagepopup(type) {

    jQuery('#popupOverlay').fadeIn();

    var $selectionPopup = jQuery(".messageBox_" + type);

    $selectionPopup.addClass('popupbounce');

    $selectionPopup.css("opacity", "0");

    $selectionPopup.css({
        'display': 'block',
        //'top': $(window).height()/2 + $(window).scrollTop(),

        'left': jQuery(window).width() / 2,
        'margin-left': '-' + ($selectionPopup.width() / 2) + 'px',
        'margin-top': '-' + (305 / 2) + 'px'

    });

    $selectionPopup.css("opacity", "1");

    $selectionPopup.animate({"height": "325", "top": jQuery(window).height() / 2 + jQuery(window).scrollTop()}, 1200);



    return false;

}

function messagepopupclose() {

    jQuery(".messageBox").removeClass('popupbounce');

    jQuery('.messageBox').animate({"height": "0", "top": "0"}, 500, function() {

        jQuery('.messageBox').css("display", "none");

        jQuery('#popupOverlay').fadeOut();

    });

}



$('#search_username #username').blur(function() {

    var val = $(this).val();

    if (val != "") {

        $(this).addClass('uname_focused');

    } else {

        $(this).removeClass('uname_focused');

    }

});





$('#email').blur(function() {

    var val = $(this).val();

    if (val != "") {

        $(this).addClass('focused');

    } else {

        $(this).removeClass('focused');

    }

});

function saveEmail() {
    var email = $('#promo_email').val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        alert("Not a valid e-mail address");
        return false;
    } else {
        ga('send', 'event', 'Acquisition', 'Newsletter');
        $.ajax({
            type: "POST",
            url: baseUrl + "common/saveemail",
            data: {email: email},
            success: function(msg) {
                if (msg === '0') {
                    alert('Email already registered');
                    return false;
                } else {
                    $('#preload').hide();
                    $('#promo_code').html(msg);
                    $('#promo_message').show();
                    $('#promo_email').val("").blur();
                }
            }
        });
    }
}

function keepEmail() {

    var keepemail = $('#email').val();

    var atpos = keepemail.indexOf("@");

    var dotpos = keepemail.lastIndexOf(".");

    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= keepemail.length)

    {

        alert("Not a valid e-mail address");

        return false;

    } else {
        ga('send', 'event', 'Acquisition', 'Newsletter');
        $.ajax({
            type: "POST",
            url: baseUrl + "common/keepmail",
            data: {email: keepemail},
            success: function(msg) {
                $('.thanksforsignup h2').show();
                $('#email').val("").blur();
            }
        });
    }

    /*	if(keepemail!=''){
     
     $('.thanksforsignup h2').show();
     
     $('#email').val("").blur();
     
     }else{
     
     $('#email').focus();
     
     }*/

}



/*-----------*/





jQuery(function() {

    if (jQuery('body').hasClass('designStep')) {

        var eTop = jQuery('#stepFlowBar').offset().top;

        jQuery('.shape_selection_popup').css({'margin-top': eTop - jQuery(window).scrollTop()});

        jQuery(window).scroll(function() {

            var eTop = jQuery('#stepFlowBar').offset().top;

            jQuery('.shape_selection_popup').css({'margin-top': eTop - jQuery(window).scrollTop()});

            if (jQuery('.shape_selection_popup').hasClass('popup_alive')) {

                //jQuery('.designStep #stepMain').css({'position':'static'});

            }

        });

    }

    jQuery(function() {

        if (jQuery('body').hasClass('step_two') || jQuery('body').hasClass('step_three') || jQuery('body').hasClass('step_four') || jQuery('body').hasClass('step_five')) {
            $('.login a').attr('href', 'javascript:void(0);');

            $('.login a').off('click touchend').on('click touchend', function() {
                CustomShoe.saveModel(function() {
                    window.location.href = baseUrl + "login";
                });
                return false;
            });
        }
    });


    if (jQuery('body').hasClass('accounts')) {

        jQuery(window).scroll(function() {

            if (($('.accounts #secondary').css("position")) == 'fixed') {

                accountSliderClose();

            }

        });

    }



    if (jQuery('body').hasClass('contact')) {



        if (faqIdSplit === undefined) {
        } else {

            faqScroll('#' + s);

        }



    }

    /*
     
     $('.step_three .createStep1 a').click(function(){
     
     if(!confirm('Are you sure you want to do that? Selecting this option will delete your current design')){
     return false;
     }else{
     return true;
     }
     
     });
     
     
     $('.step_three .createStep2 a').click(function(){
     
     if(!confirm('Are you sure you want to do that? Selecting this option will default your leather texture to calf plain black')){
     return false;
     }else{
     return true;
     }
     
     });
     */



});



jQuery(window).load(function() {
    if (is_touch_device()) {
        $("#shape_visualization").off('mousemove', "img");
        $('body').remove("#style_path");
    }
    validate_checkout_page();
    validate_checkout_page_shipping();

    jQuery('#shape_selection_popup').on("mouseenter", ".topSliderRight a.bx-prev, .topSliderRight a.bx-next", function() {
        $(this).click();
    });


    jQuery('.step_five #preloader , .step_four #preloader').fadeOut(300);

    jQuery('#login_button_wrap .grey').off('click touchend').on('click touchend', function() {
        $('#login_button_wrap').hide();
        $('#email_signup').show();

    });



    /* login dropdown */

    $('.login a').mouseover(function() {
        var cartPopup = $('.login_pop_mini');
        var centerWindow = $(window).width() / 2;
        var marRight = $('.wrapper').width() / 2;
        var totRight = centerWindow - marRight;
        cartPopup.css({
            'right': totRight
        });
        $('.login_pop_mini').slideDown(150);



    });

    $('.login_pop_mini').mouseleave(function() {
        $(this).hide();
    });

    $('#headerNavLinks li').mouseover(function() {
        if (!$(this).hasClass('login')) {
            $('.login_pop_mini').hide();
        }

    });

    /* login dropdown ends here */

});

function validate_checkout_page_shipping() {

    if ((jQuery('#shippingstateid').val()) === '1') {

        jQuery('#dk_container_shipping_state').addClass('invalid_box');

    } else {

        jQuery('#dk_container_shipping_state').removeClass('invalid_box');

    }   
    jQuery('#dk_container_shipping_state .dk_options_inner li a').on('click touchend', function() {

        var selTxt = jQuery(this).text();

        if (selTxt !== 'State') {

            jQuery('#dk_container_shipping_state').removeClass('invalid_box');

            jQuery('#selectedShippingStateId').val(selTxt);

        }
    });
}

function validate_checkout_page() {
    if ((jQuery('#paymentid').val()) === '1') {

        jQuery('#dk_container_payment_method').addClass('invalid_box');

    } else {

        jQuery('#dk_container_payment_method').removeClass('invalid_box');

    }


    if ((jQuery('#billingstateid').val()) == '1') {

        jQuery('#dk_container_card_state').addClass('invalid_box');

    } else {

        jQuery('#dk_container_card_state').removeClass('invalid_box');

    }





    jQuery('#dk_container_payment_method .dk_options_inner li a').off('click touchend').on('click touchend', function() {

        var selTxt = jQuery(this).text();

        if (selTxt !== 'Choose Payment Method') {

            jQuery('#dk_container_payment_method').removeClass('invalid_box');

        }

    });

    jQuery('#dk_container_exp_date_month .dk_options_inner li a').off('click touchend').on('click touchend', function() {

        var selTxt = jQuery(this).text();

        if (selTxt !== '--') {

            jQuery('#dk_container_exp_date_month').removeClass('invalid_box');

        }

    });

    jQuery('#dk_container_exp_date_year .dk_options_inner li a').off('click touchend').on('click touchend', function() {

        var selTxt = jQuery(this).text();

        if (selTxt !== '--') {

            jQuery('#dk_container_exp_date_year').removeClass('invalid_box');

        }

    });

    jQuery('#dk_container_card_state .dk_options_inner li a').off('click touchend').on('click touchend', function() {

        var selTxt = jQuery(this).text();

        if (selTxt !== 'State') {

            jQuery('#selectedBillingStateId').val(selTxt);

            jQuery('#dk_container_card_state').removeClass('invalid_box');

        }

    });
    jQuery('#dk_container_shipping_state .dk_options_inner li a').off('click touchend').on('click touchend', function() {

        var selTxt = jQuery(this).text();

        if (selTxt !== 'State') {

            jQuery('#selectedShippingStateId').val(selTxt);

            jQuery('#dk_container_shipping_state').removeClass('invalid_box');

        }

    });


}
var $dk_key_word = '';
var timer;
function dk_onkeyup(e) {
    if (e.keyCode === 9) {
        $(this).addClass('dk_focus dk_open');
        $dk_key_word = '';
        return;
    }
    $dk_key_word += String.fromCharCode(e.keyCode);
    timer = setTimeout(function() {
        clearTimeout(timer);
        $dk_key_word = '';
    }, 1000);
    elements = $(this).find('li a:contains("' + $dk_key_word + '")');
    if (elements.length > 0) {
        $(this).find('li').removeClass('dk_option_current');
        elements.eq(0).parent().addClass('dk_option_current');
        $(this).find('span.dk_label').html(elements.eq(0).html());
        $(this).removeClass('dk_theme_black dk_theme_').addClass('dk_theme_' + elements.eq(0).attr('data-dk-dropdown-value'));
        $(this).removeClass('dk_focus dk_open');
        $(this).next('select').val(elements.eq(0).html());
        $(this).next('select').trigger('change');
    }
}
