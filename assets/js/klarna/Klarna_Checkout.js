/**
 * Copyright 2016 Klarna AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @category   Klarna
 * @package    Klarna_Kco
 * @author     Jason Grim <jason.grim@klarna.com>
 */

/**
 * Klarna checkout JS
 */
if (typeof Klarna == 'undefined') {
    var Klarna = {};
}

Klarna.Checkout = Class.create();
Klarna.Checkout.prototype = {
    initialize: function (saveUrl, failureUrl, reloadUrl, messageId, frontEndAddress, addressUrl, frontEndShipping) {
        console.log("Checkout JS Loaded"+addressUrl);
        this.loadWaiting = false;
        this.suspended = false;
        this.saveUrl = saveUrl;
        this.failureUrl = failureUrl;
        this.reloadUrl = reloadUrl;
        this.addressUrl = addressUrl;
        this.messageId = messageId;
        this.frontEndAddress = frontEndAddress || false;
        this.frontEndShipping = frontEndShipping || false;
        this.onSave = this.saveResponse.bindAsEventListener(this);
        this.onComplete = this.resetLoadWaiting.bindAsEventListener(this);
        document.observe('dom:loaded', this.attachEvents.bind(this));
    },

    attachEvents: function () {
        if (window._klarnaCheckout) {
            window._klarnaCheckout(function (api) {
                console.log(api);
                api.on({
                    'change': function (data) {
                        console.log("change - Start");                        
                        console.log(data);
                        console.log("change - End");
                        if (!checkout.frontEndShipping && !data.given_name) {
                             console.log("Inside Chage Event's AJAX");
                            var request = new Ajax.Request(
                                checkout.saveUrl,
                                {
                                    method: 'post',
                                    parameters: data
                                }
                            );
                        }
                    },
                    'shipping_option_change': function (data) {
                        checkout.reloadContainer();
                    },
                    'shipping_address_change': function (data) {
                        console.log("shipping_address_change - Start");
                        console.log(data);
                        console.log("shipping_address_change - End");
                        
                        if (checkout.frontEndAddress) {
                            console.log("Inside frontEndAddress");
                            checkout.suspend();
                            checkout.showLoader();
console.log(checkout.addressUrl);
                            var request = new Ajax.Request(
                                checkout.addressUrl,
                                {
                                    method: 'post',
                                    onComplete: checkout.onComplete,
                                    onSuccess: checkout.onSave,
                                    onFailure: checkout.ajaxFailure.bind(checkout),
                                    parameters: data
                                }
                            );
                            /*$.ajax({
                                url: checkout.addressUrl,
                                type: "POST",
                                data: data,
                                success: checkout.onSave,
                                complete: checkout.onComplete,
                                error: checkout.ajaxFailure.bind(checkout)
                            });*/
                    console.log(request);
                        } else {
                            console.log("Not Inside frontEndAddress");
                            checkout.reloadContainer();
                        }
                    }
                });
            });
        }
    },

    ajaxFailure: function () {
        location.href = this.failureUrl;
    },

    suspend: function () {
        console.log("Inside suspend - Start=>");
        if (!this.suspended && window._klarnaCheckout) {
            console.log("Inside suspend2");
            window._klarnaCheckout(function (api) {
                console.log("Inside suspend3");
                api.suspend();
            });

            this.suspended = true;
        }
        console.log("Inside suspend - End");
    },

    resume: function () {
        console.log("Inside Resume - Start");
        if (this.suspended && window._klarnaCheckout) {
            window._klarnaCheckout(function (api) {
                api.resume();
            });

            this.suspended = false;
        }
        console.log("Inside Resume - Start");
    },

    showLoader: function () {
        console.log("Inside showLoader");
        this.loadWaiting = true;
        jQuery('.klarna_loader').show();
    },

    hideLoader: function () {
        console.log("Inside hideLoader");
        this.loadWaiting = false;
        jQuery('.klarna_loader').hide();
    },

    saveResponse: function (transport) {
        var response = {};
console.log("Inside SaveResponse");
console.log(transport);
        if (transport && transport.responseText) {
            try {
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
            }
        }

        this.setResponse(response);
    },

    resetLoadWaiting: function () {
        this.hideLoader();
        this.resume();
    },

    setResponse: function (response) {
        console.log("Inside setResponse");
        console.log(response);
        var messageBox = jQuery(this.messageId);
        var messageBoxWrapper = jQuery(this.messageId + '_wrapper');
        var messageBoxContent = jQuery(this.messageId + '_content');

        if (response.redirect) {
            location.href = response.redirect;
            return true;
        }
        messageBox.hide();
        if (response.update_section) {
            jQuery(response.update_section.name).update(response.update_section.html);
        }
        if (response.update_sections) {
            response.update_sections.forEach(function (update_section) {
                jQuery(update_section.name).update(update_section.html);
            });
        }
        if (response.success) {
            messageBoxWrapper.removeClassName('error-msg');
            messageBoxWrapper.addClassName('success-msg');
            messageBox.show();
            messageBoxContent.update(response.success);
        }
        if (response.error) {
            messageBoxWrapper.removeClassName('success-msg');
            messageBoxWrapper.addClassName('error-msg');
            messageBox.show();
            messageBoxContent.update(response.error);
        }
        return false;
    },

    reloadContainer: function () {
        console.log("Inside reloadContainer");
        if (this.loadWaiting !== false) return;

        this.showLoader();

        var request = new Ajax.Request(
            this.reloadUrl,
            {
                onComplete: this.onComplete,
                onSuccess: this.onSave,
                onFailure: checkout.ajaxFailure.bind(checkout)
            }
        );
    }
};

/*Klarna.Form = Class.create();
Klarna.Form.prototype = {
    initialize: function (form, saveUrl, suspend) {
        this.form = form;
        this.saveUrl = saveUrl;
        this.suspendKlarna = suspend ? true : false;
        this.onSave = checkout.saveResponse.bindAsEventListener(this);
        this.onComplete = checkout.resetLoadWaiting.bindAsEventListener(this);

        if ($(this.form)) {
            $(this.form).observe('submit', function (event) {
                this.save();
                Event.stop(event);
            }.bind(this));
        }
    },

    save: function () {
        if (checkout.loadWaiting !== false) return;

        if (this.suspendKlarna) {
            checkout.suspend();
        }

        checkout.showLoader();

        var validator = new Validation(this.form);
        if (validator.validate()) {
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method: 'post',
                    onComplete: checkout.onComplete,
                    onSuccess: checkout.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
        } else {
            checkout.resetLoadWaiting();
        }
    }
};*/
