$(document).ready(function () {


    $(function () {
        $('#userLists').dataTable({
            "iDisplayLength": 10,
            "bInfo": true,
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bAutoWidth": true,
            "aaSorting": [[0, "desc"]],
            "aoColumnDefs": [{"sType": "numeric"}]
        });
    });

    $('body').on('click', '.delUser', function(){
        if (confirm("Are you sure to delete this user?")) {
            $.ajax({
                type: "POST",
                url: baseUrl + "admin/delUser",
                data: {'user_id': $(this).attr('data-id') },
                success: function (response) {
                    responseObj = JSON.parse(response);
                    if(responseObj != null && responseObj != undefined){
                        alert('User has been deleted.');
                        window.location.reload();
                    } else {
                        alert('Unknown error, please try again later');
                    }
                }
            });
        }
    });

    $('#register_form').on('submit', function(e){
        e.preventDefault();
        $("#error").text("");
        $("#success").text("");

        var userJson = {};
        var datastring = $("#register_form").serializeArray();
        $.each(datastring, function() {
            if (userJson[this.name] !== undefined) {
                if (!userJson[this.name].push) {
                    userJson[this.name] = [userJson[this.name]];
                }
                userJson[this.name].push(this.value || '');
            } else {
                userJson[this.name] = this.value || '';
            }
        });
        if (typeof userJson.user_id == 'undefined') {
            if(userJson.password.length < 6){
                $("#error").text("Password should have minimum 6 characters");
                return;
            }
            if(userJson.confirmpassword.length < 6){
                $("#error").text("Confirm Password should have minimum 6 characters");
                return;
            }
            if(userJson.password !== userJson.confirmpassword){
                $("#error").text("Password and Confirm Password do not match");
                return;
            }
        }

        if(userJson.role === "admin"){
            userJson.role = "1";
        } else if(userJson.role === "webuser"){
            userJson.role = "2";
        } else if(userJson.role === "vendor"){
            userJson.role = "3";
        } else if(userJson.role === "intern"){
            userJson.role = "4";
        }
        $("#loadIcon").show();
        $("#btnAddUser").prop("disabled", true);
        $("#fname").prop("disabled", true);
        $("#lname").prop("disabled", true);
        $("#email").prop("disabled", true);
        $("#password").prop("disabled", true);
        $("#confirmpassword").prop("disabled", true);
        $("#role").prop("disabled", true);
        $.ajax({
            type: "POST",
            url: baseUrl + "admin/saveUser",
            data: userJson,
            success: function (response) {
                $("#loadIcon").hide();
                $("#btnAddUser").prop("disabled", false);
                $("#fname").prop("disabled", false);
                $("#lname").prop("disabled", false);
                $("#email").prop("disabled", false);
                $("#password").prop("disabled", false);
                $("#confirmpassword").prop("disabled", false);
                $("#role").prop("disabled", false);
                responseObj = JSON.parse(response);
                if(responseObj != null && responseObj != undefined){
                    if(responseObj.status == "Email id already exists"){
                        $("#error").text("Email id already exists");
                    }else if(responseObj.status == "success"){
                        if (typeof userJson.user_id == 'undefined') {
                            $("#success").text("User created successfully, user id is " + responseObj.user_id);
                            $("#fname").val("");
                            $("#lname").val("");
                            $("#email").val("");
                            $("#password").val("");
                            $("#confirmpassword").val("");
                        } else {
                            $("#success").text("User updated successfully.");
                            setTimeout(function() {
                                window.location.href = baseUrl + "admin/users";
                            }, 2000);
                        }
                    }else {
                        $("#error").text("Unknown error, please try again later");
                    }
                } else {
                    $("#error").text("Unknown error, please try again later");
                }
            }
        });
    });
});