$(document).ready(function(){
	$('#grid tbody tr').bind('click touchend', function(){
		window.location = $(this).find('a').attr('href');
		return false;
	});
	$('.pagination .page').bind('click', function(e){
		e.preventDefault();
		/*
		 * var page_number = $(this).attr('data-page-number'); var form =
		 * $('#filters'); form.attr('action', form.attr('action') + '/' +
		 * page_number);
		 */
		$("#filters").attr("action", $(this).attr("href"));
		$('#filters').submit();
	});
});
function change_tab(tab_name){
	$('#tab span').removeClass('active');
	$.each($('#tab a'), function(){
		if(tab_name === $(this).attr('href')){
			$(this).parent('span').addClass('active');
		}
	});
	tab_name = tab_name.replace("#", ".");
	$('.order-items').removeClass('active');
	$('' + tab_name + '').addClass('active');
}
$(function(){
	$(window)
		.bind(
			'hashchange',
			function(){
				if(typeof window.location.hash !== "undefined"
					&& window.location.hash !== "#"
					&& window.location.hash !== ""){
					change_tab(window.location.hash);
				}
			});
	if(typeof window.location.hash !== "undefined"
		&& window.location.hash !== "#" && window.location.hash !== ""){
		change_tab(window.location.hash);
	}
	if(window.location.hash === '' || window.location.hash === '#'){
		change_tab('#item1');
	}
});
