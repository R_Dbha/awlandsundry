$(function () {
    var dataTable = $('#giftcards').DataTable({
        "iDisplayLength": 50,
        "aaSorting": [[0, "desc"]],
        "aoColumnDefs": [
            {"sType": "numeric"}
        ]
    });
});

$(document).ready(function () {
    $('body').on('click', '#giftcards tbody tr', show_details);
});

function show_details() {
    var giftcard_id = $(this).find('.gift-card-id').val();
    $.ajax({
        type: "POST",
        url: base_url + "giftcard/show_details/" + giftcard_id,
        dataType: 'html',
        success: function (response) {
            $('#giftcard .modal-content').html(response);
            $('#giftcard').modal();

        },
    });
}
