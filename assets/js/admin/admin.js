$(document).ready(
	function(){
		$('#orders tbody tr').on('mouseover', function(){
			 $(this).find('.quick-view').show();
		});
		$('#orders tbody tr').on('mouseout', function(){
			 $(this).find('.quick-view').hide();
		});
		$('body').on('click', '#create-order', show_create_order_modal);
		$('#orders tbody tr').on('click', show_order_details);
		$('body').on('change', '.show-selected', show_selected);
		$('body').on('change', '.type, .user_list', show_shoes);
		$('body').on('change', '.style', get_types);
		$('body').on('click', '.select-and-proceed', proceed);
		$('body').on('click', '.back', back);
		$('body').on('click', '#confirm-item', order_add_item);
		$('body').on('click', '#btn-edit-shipping', show_edit_shipping);
		$('body').on('click', '#btn-edit-billing', show_edit_billing);
		$('body').on('click', '.btn-edit-sizing', show_edit_sizing);
		$('body').on('click', '.btn-edit-shoe', show_edit_shoe);
		$('body').on('click', '#btn-edit-comment', show_edit_comment);
		$('body').on('click', '#done-edit-billing', done_edit_billing);
		$('body').on('click', '#done-edit-shipping', done_edit_shipping);
		$('body').on('click', '.done-edit-size', done_edit_size);
		$('body').on('click', '#done-edit-comment', done_edit_comment);
		$('body').on('click', '.done-edit-item', done_edit_item);
		$('body').on('click', '.delete-item', delete_item);
		$('body').on('change', '#customer', populate_address);
		$('body').on('click', '#confirm-order', confirm_order);
		$('body').on('change',
			'#shipping-billing-address .shipping-address input',
			on_change_shipping);
		$('body').on('change',
			'#shipping-billing-address .billing-address input',
			on_change_billing);
		$('body').on('click', '.save-custom-item', save_custom_item);
		$('#order-details').on('hidden.bs.modal', function(){
			$('#order-details .modal-body').html('');
		});
		$('#order-create').on('hidden.bs.modal', function(){
			$('#order-create .modal-body').html('');
		});
	});
function confirm_order(){
	var customer = $('#customer').val();
	var firstname = $('#shipping_firstname').val();
	var lastname = $('#shipping_lastname').val();
	var address1 = $('#shipping_address1').val();
	var address2 = $('#shipping_address2').val();
	var city = $('#shipping_city').val();
	var country = $('#shipping_country').val();
	var state = $('#shipping_state').val();
	var zipcode = $('#shipping_zipcode').val();
	var telephone = $('#shipping_telephone').val();
	var shipping_address = {
		'firstname' : firstname,
		'lastname' : lastname,
		'address1' : address1,
		'address2' : address2,
		'city' : city,
		'state' : state,
		'country' : country,
		'zipcode' : zipcode,
		'telephone' : telephone,
		'user_id' : customer
	};
	firstname = $('#billing_first_name').val();
	lastname = $('#billing_last_name').val();
	address1 = $('#billing_address1').val();
	address2 = $('#billing_address2').val();
	city = $('#billing_city').val();
	country = $('#billing_country').val();
	state = $('#billing_state').val();
	zipcode = $('#billing_zipcode').val();
	billing_address = {
		'address1' : address1,
		'address2' : address2,
		'city' : city,
		'state' : state,
		'country' : country,
		'zipcode' : zipcode,
		'user_id' : customer
	};
	var belt = $('#belt').is(':checked') ? '1':'0';
	var data = {
		'billing_address' : billing_address,
		'shipping_address' : shipping_address,
		'gross_amt' : $('#subtotal').val(),
		'user_id' : customer,
		'comments' : $('#order_comments').val(),
		'order_source' : $('#order-source').val(),
		'belt_needed' : belt,
	}
	$.ajax({
		type : "POST",
		url : base_url + "orders/confirm_order/",
		dataType : 'html',
		data : data,
		success : function(response){
			if(response == '1'){
				window.location = window.location;
			}
		},
	});
}
function save_custom_item(){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	var attachment_ids = $(container).find('.attachment_id').val();
	var price = $(container).find('.u_price').val();
	var style = $(container).find('.u_style').val();
	var last = $(container).find('.u_last').val();
	var customer = $('#customer').val();
	if(container != '#add-item'){
		action = 'edit';
	}else{
		action = 'add';
	}
	$.ajax({
		type : "POST",
		url : base_url + "orders/save_custom_item/",
		data : {
			'attachment_ids' : attachment_ids,
			'price' : price,
			'style_name' : style,
			'last_id' : last,
			'customer' : customer,
			'action' : action
		},
		dataType : 'html',
		success : function(response){
			$(container).find('.item-details').html(response);
			$(container).find('.new-item-images').bxSlider({
				controls : true,
				prevText : '<i class="fa fa-arrow-left"></>',
				nextText : '<i class="fa fa-arrow-right"></>',
			});
		},
	});
	$(container).find('.order_add_item').css({
		'margin-left' : '-100%'
	});
}
function populate_address(){
	var customer_id = $(this).val();
	$.ajax({
		type : "POST",
		url : base_url + "orders/shipping_billing_address/" + customer_id,
		dataType : 'json',
		success : function(response){
			$('#shipping-billing-address .shipping-address').html(
				response.shipping);
			$('#shipping-billing-address .billing-address').html(
				response.billing);
		},
	});
}
function on_change_billing(){
	var firstname = $('#billing_first_name').val();
	var lastname = $('#billing_last_name').val();
	var address1 = $('#billing_address1').val();
	var address2 = $('#billing_address2').val();
	var city = $('#billing_city').val();
	var country = $('#billing_country').val();
	var state = $('#billing_state').val();
	var zipcode = $('#billing_zipcode').val();
	$('#order-review .billing-address').html(
		'<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>' + address1
			+ ', ' + address2 + '</h5><h5>' + city + ', ' + state + ', '
			+ country + ',' + zipcode + '</h5>');
}
function on_change_shipping(){
	var firstname = $('#shipping_firstname').val();
	var lastname = $('#shipping_lastname').val();
	var address1 = $('#shipping_address1').val();
	var address2 = $('#shipping_address2').val();
	var city = $('#shipping_city').val();
	var country = $('#shipping_country').val();
	var state = $('#shipping_state').val();
	var zipcode = $('#shipping_zipcode').val();
	var telephone = $('#shipping_telephone').val();
	$('#order-review .shipping-address').html(
		'<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>' + address1
			+ ', ' + address2 + '</h5><h5>' + city + ', ' + state + ', '
			+ country + ',' + zipcode + '</h5></h5>' + telephone + '</h5>');
}
function show_create_order_modal(){
	$.ajax({
		type : "POST",
		url : base_url + "orders/create_order/",
		dataType : 'html',
		success : function(response){
			$('#order-create .modal-body').html(response);
			$('#order-create').modal();
		},
	});
}
function show_edit_shoe(){
	var id = '#' + $(this).closest('.tab-pane').attr('id');
	$("a[href='" + id + "']").html('Edit ' + id.replace('#', ''));
	$("a[href='" + id + "']").next('i').hide();
	var design_id = $(id).find('.design_id').val();
	var order_id = $('#order_id').text();
	$(id).children('.details').hide();
	$(id).children('.add-edit').show();
	$('.edit-btn').hide();
}
function delete_item(){
	var id = $(this).parent('a').attr('href');
	var tab = $(this).parents('li.items');
	var tabs;
	var tab_pane = $(id);
	var design_id = $(tab_pane).find('.design_id').val();
	var order_id = $('#order_id').text();
	var confir = confirm('Do you really want to delete the item?');
	var box = $(this).parents('.box');
	var data = {
		'design_id' : design_id,
		'order_id' : order_id
	};
	if(confir){
		$(box).children('.overlay').show();
		$(box).children('.loading-img').show();
		$.ajax({
			type : "POST",
			url : base_url + "orders/delete_item/",
			dataType : 'html',
			data : data,
			success : function(response){
				$(tab).remove();
				$(tab_pane).remove();
				tabs = $('#order-details').find('li.items');
				$(tabs).each(function(key){
					i = $(this).find('i');
					$(this).children('a').html('Item ' + (key + 1));
					$(this).children('a').append(i);
				});
				$('.order-summary').addClass('active');
				$('#order-summary').addClass('active');
				$(box).children('.overlay').hide();
				$(box).children('.loading-img').hide();
			},
		});
	}
}
function show_edit_sizing(e){
	e.preventDefault();
	$('.edit-btn').hide();
	$(this).closest('.box').find('.editable').show();
	$(this).closest('.box').find('.not-editable').hide();
}
function done_edit_comment(e){
	e.preventDefault();
	var editable = $(this).closest('.box').find('.editable');
	var not_editable = $(this).closest('.box').find('.not-editable');
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	var order_id = $('#order_id').text();
	var comment = $('#edit-comment textarea').val();
	var data = {
		'comments' : comment,
	}
	$.ajax({
		type : "POST",
		url : base_url + "orders/edit_comment/" + order_id,
		data : data,
		dataType : 'html',
		success : function(response){
			$(not_editable).html(comment);
			$(editable).hide();
			$(not_editable).show();
			$('.edit-btn').show();
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
		}
	});
}
function done_edit_size(e){
	e.preventDefault();
	var editable = $(this).closest('.box').find('.editable');
	var not_editable = $(this).closest('.box').find('.not-editable');
	var left_shoe = $(editable).find('.left_shoe').val();
	var left_width = $(editable).find('.left_width').val();
	var right_shoe = $(editable).find('.right_shoe').val();
	var right_width = $(editable).find('.right_width').val();
	var m_left_size = $(editable).find('.m_left_size').val();
	var m_left_width = $(editable).find('.m_left_width').val();
	var m_right_size = $(editable).find('.m_right_size').val();
	var m_right_width = $(editable).find('.m_right_width').val();
	var quantity = $(editable).find('.quantity').val();
	var design_id = $(editable).find('.design_id').val();
	var order_id = $('#order_id').text();
	var data = {
		'left_shoe' : left_shoe,
		'left_width' : left_width,
		'right_shoe' : right_shoe,
		'right_width' : right_width,
		'm_left_size' : m_left_size,
		'm_left_width' : m_left_width,
		'm_right_size' : m_right_size,
		'm_right_width' : m_right_width,
		'quantity' : quantity
	};
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "orders/edit_details/" + order_id + '/' + design_id,
		data : data,
		dataType : 'html',
		success : function(response){
			html = '<dt>Quantity :</dt>' + '<dd>' + quantity + ' PAIR</dd>'
				+ '<dt>Left shoe size :</dt>' + '<dd>' + left_shoe + ' '
				+ left_width + '</dd>' + '<dt>Right shoe size :</dt>' + '<dd>'
				+ right_shoe + ' ' + right_width + '</dd>'
				+ '<dt>Left shoe measurement :</dt>' + '<dd>' + m_left_size
				+ 'cm - ' + m_left_width + 'cm</dd>'
				+ '<dt>Right shoe measurement :</dt>' + '<dd>' + m_right_size
				+ 'cm - ' + m_right_width + 'cm</dd>';
			$(not_editable).html(html);
			$(editable).hide();
			$(not_editable).show();
			$('.edit-btn').show();
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
		}
	});
}
function done_edit_shipping(){
	var order_id = $('#order_id').text();
	var firstname = $('#shipping_firstname').val();
	var lastname = $('#shipping_lastname').val();
	var address1 = $('#shipping_address1').val();
	var address2 = $('#shipping_address2').val();
	var city = $('#shipping_city').val();
	var country = $('#shipping_country').val();
	var state = $('#shipping_state').val();
	var zipcode = $('#shipping_zipcode').val();
	var telephone = $('#shipping_telephone').val();
	var data = {
		'order_id' : order_id,
		'firstname' : firstname,
		'lastname' : lastname,
		'address1' : address1,
		'address2' : address2,
		'city' : city,
		'state' : state,
		'country' : country,
		'zipcode' : zipcode,
		'telephone' : telephone,
	};
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "orders/edit_shipping_address",
		data : data,
		dataType : 'html',
		success : function(response){
			$('#edit-shipping-address').hide();
			$('#shipping-address').html(
				'<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>'
					+ address1 + ', ' + address2 + '</h5><h5>' + city + ', '
					+ state + ', ' + country + ',' + zipcode + '</h5></h5>'
					+ telephone + '</h5>');
			$('#shipping-address').show();
			$('.edit-btn').show();
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
		}
	});
}
function done_edit_billing(){
	var order_id = $('#order_id').text();
	var firstname = $('#billing_first_name').val();
	var lastname = $('#billing_last_name').val();
	var address1 = $('#billing_address1').val();
	var address2 = $('#billing_address2').val();
	var city = $('#billing_city').val();
	var country = $('#billing_country').val();
	var state = $('#billing_state').val();
	var zipcode = $('#billing_zipcode').val();
	var data = {
		'order_id' : order_id,
		'address1' : address1,
		'address2' : address2,
		'city' : city,
		'state' : state,
		'country' : country,
		'zipcode' : zipcode,
	};
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "orders/edit_billing_address",
		data : data,
		dataType : 'html',
		success : function(response){
			$('#edit-billing-address').hide();
			$('#billing-address').html(
				'<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>'
					+ address1 + ', ' + address2 + '</h5><h5>' + city + ', '
					+ state + ', ' + country + ',' + zipcode + '</h5>');
			$('#billing-address').show();
			$('.edit-btn').show();
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
		}
	});
}
function show_edit_comment(e){
	e.preventDefault();
	$('.edit-btn').hide();
	$('#edit-comment').show();
	$('#comment').hide();
}
function show_edit_billing(e){
	e.preventDefault();
	$('.edit-btn').hide();
	$('#edit-billing-address').show();
	$('#billing-address').hide();
}
function show_edit_shipping(e){
	e.preventDefault();
	$('.edit-btn').hide();
	$('#edit-shipping-address').show();
	$('#shipping-address').hide();
}
function order_add_item(e){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	e.preventDefault();
	self = $(this);
	$(this).hide();
	var data = {
		'order_id' : $('#order_id').text(),
		'customer' : $('#customer').val(),
		'design_id' : $(container).find('.new-design_id').val(),
		'custom_collection_id' : $(container).find('.new-custom_collection_id')
			.val(),
		'item_amt' : $(container).find('.new-price').text(),
		'left_shoe' : $(container).find('.left_shoe').val(),
		'right_shoe' : $(container).find('.right_shoe').val(),
		'left_width' : $(container).find('.left_width').val(),
		'right_width' : $(container).find('.right_width').val(),
		'm_left_size' : $(container).find('.m_left_size').val(),
		'm_right_size' : $(container).find('.m_right_size').val(),
		'm_left_width' : $(container).find('.m_left_width').val(),
		'm_right_width' : $(container).find('.m_right_width').val(),
		'quantity' : $(container).find('.new-quantity').val(),
		'monogram' : $(container).find('.new-monogram').val(),
	}
	if(container != '#add-item'){
		var tab_pane = $(this).closest('.tab-pane');
		design_id = $(tab_pane).find('.design_id').val();
		data.old_design_id = design_id;
		data.action = 'edit';
	}else{
		data.action = 'add';
	}
	$
		.ajax({
			type : "POST",
			url : base_url + "orders/order_add_item",
			data : data,
			dataType : 'html',
			success : function(response){
				if(data.action == 'add'){
					$(response).insertBefore("#add-item");
					var cnt = $('li.items').length + 1;
					$('.nav-tabs li').removeClass('active');
					$(
						'<li class="items active"><a href="#item-'
							+ cnt
							+ '"  data-toggle="tab">Item '
							+ cnt
							+ '<i title="Delete Item" '
							+ ' class="delete-item fa fa-times pull-right"></i></a></li>')
						.insertBefore("#add-item-btn");
					$("#add-item").removeClass('active');
					$("#item-" + cnt).addClass('active');
					setTimeout(function(){
						$("#item-" + cnt).find('.order_images').bxSlider({
							infiniteLoop : true,
							preloadImages : 'all',
							controls : true,
							prevText : '<i class="fa fa-arrow-left"></>',
							nextText : '<i class="fa fa-arrow-right"></>',
						});
					}, 100);
				}else{
					$(tab_pane).html($(response).html());
					var id = $(tab_pane).attr('id');
					$("a[href='#" + id + "']").html(capitalize(id));
					$("a[href='#" + id + "']").next('i').show();
					setTimeout(function(){
						$(tab_pane).find('.order_images').bxSlider({
							infiniteLoop : true,
							preloadImages : 'all',
							controls : true,
							prevText : '<i class="fa fa-arrow-left"></>',
							nextText : '<i class="fa fa-arrow-right"></>',
						});
					}, 100);
				}
				self.show();
				back(container);
				update_order_details(data.order_id);
				$('.show-selected').val('');
				$('.attachment_id').val('');
				$('.qq-upload-list').html('');
				$('.collection').html('');
			}
		});
	return false;
}
function update_order_details(order_id){
	var data = {
		'order_id' : order_id
	};
	$.ajax({
		type : "POST",
		url : base_url + "orders/update_order_details",
		data : data,
		dataType : 'json',
		success : function(response){
			if(response.status == 'success'){
				$('#subtotal').val(response.subtotal);
				$('#number_of_items').val(response.number_of_items);
			}
		}
	});
}
function done_edit_item(){
}
function proceed(e){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	e.preventDefault();
	var selected = $(container).find('.show-selected').val();
	var data;
	var order_id = $('#order_id').text();
	var shoe_design_id = $(this).parent('.shoe-details')
		.find('.shoe_design_id').val();
	var price = $(this).parent('.shoe-details').find('.price').text();
	if(selected == 'custom_collection'){
		custom_collection_id = $(this).parent('.shoe-details').find(
			'.custom_collection_id').val()
		data = {
			'shoe_design_id' : shoe_design_id,
			'custom_collection_id' : custom_collection_id,
			'source' : 'custom_collection',
			'price' : price,
			'order_id' : order_id
		};
	}else{
		data = {
			'shoe_design_id' : shoe_design_id,
			'source' : 'saved_shoes',
			'price' : price,
			'order_id' : order_id
		};
	}
	if(container != '#add-item'){
		design_id = $(this).closest('.tab-pane').find('.design_id').val();
		data.design_id = design_id;
		data.action = 'edit';
	}else{
		data.action = 'add';
	}
	$.ajax({
		type : "POST",
		url : base_url + "orders/get_item_details",
		data : data,
		dataType : 'html',
		success : function(response){
			$(container).find('.item-details').html(response);
			$(container).find('.new-item-images').bxSlider({
				controls : true,
				prevText : '<i class="fa fa-arrow-left"></>',
				nextText : '<i class="fa fa-arrow-right"></>',
			});
		}
	});
	$(container).find('.order_add_item').css({
		'margin-left' : '-100%'
	});
}
function back(container){
	if(typeof container == 'object'){
		container = '#' + $(this).parents('.add-edit').attr('id');
	}
	$(container).find('.item-details').html('');
	$(container).find('.order_add_item').css({
		'margin-left' : '0%'
	});
}
function show_selected(){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	$(this).parents('.box-body').find('.group').hide();
	$(this).parents('.box-body').find('.' + $(this).val()).show();
	show_shoes(container);
}
function get_types(){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	var style = $(this).val();
	$.ajax({
		type : "POST",
		url : base_url + "orders/get_types",
		data : {
			'style' : style
		},
		dataType : 'html',
		success : function(response){
			$(container).find('.type').html(response);
			show_shoes(container);
		}
	});
}
function show_shoes(container){
	if(typeof container == 'object'){
		container = '#' + $(this).parents('.add-edit').attr('id');
	}
	var selected = $(container).find('.show-selected').val();
	var data;
	if(selected == 'custom_collection'){
		var style = $(container).find('.style').val();
		var type = $(container).find('.type').val();
		data = {
			'style' : style,
			'type' : type,
			'source' : 'custom_collection'
		};
	}else if(selected == 'saved_shoes'){
		var user_id = $(container).find('.user_list').val();
		data = {
			'user_id' : user_id,
			'source' : 'saved_shoes'
		};
	}
	if(selected !== 'upload_images'){
		$.ajax({
			type : "POST",
			url : base_url + "orders/get_collection",
			data : data,
			dataType : 'html',
			success : function(response){
				$(container).find('.collection').html(response);
				setTimeout(function(){
					$('.shoe_images').bxSlider({
						infiniteLoop : true,
						preloadImages : 'all',
						controls : true,
						prevText : '<i class="fa fa-arrow-left"></>',
						nextText : '<i class="fa fa-arrow-right"></>',
					});
				}, 1);
			}
		});
	}else{
		$(container).find('.collection').html('');
		$('.attachment_id').val('');
		$('.qq-upload-list').html('');
	}
}
function show_order_details(){
	var order_id = $(this).find('.order_id').val();
	var box = $(this).parents('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "orders/order_details",
		data : {
			order_id : order_id,
		},
		dataType : 'html',
		success : function(response){
			$('#order-details .modal-body').html(response);
			$('#order-details').modal('show', {
				backdrop : 'static'
			});
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
			setTimeout(function(){
				$('.order_images').bxSlider({
					infiniteLoop : true,
					preloadImages : 'all',
					controls : true,
					prevText : '<i class="fa fa-arrow-left"></>',
					nextText : '<i class="fa fa-arrow-right"></>',
				});
			}, 100);
		}
	});
}

$(function(){
	$('#orders').dataTable({
		"bInfo" : true,
		"bPaginate" : true,
		"bLengthChange" : true,
		"bFilter" : true,
		"bSort" : true,
		"bAutoWidth" : true,
		"aaSorting" : [[0, "desc"]]
	});
});
function capitalize(string){
	return string.charAt(0).toUpperCase() + string.slice(1);
}