$(function () {
    var dataTable = $('#promocodes').DataTable({
        "processing": false,
        "serverSide": true,
        "ajax": base_url + 'promocode/json_promocode',
        "columns": [
            {"data": 'promocode'},
            {"data": 'discount_per'},
            {"data": 'discount_amount'},
            {"data": 'expiry_date'},
            {"data": 'is_common'},
            {"data": 'assigned_users'},
            {"data": 'used_users'},
            {"data": 'view_usage', "bSortable": false},
            {"data": 'assign', "bSortable": false},
            {"data": 'edit', "bSortable": false},
        ],
    });
});

$(document).ready(function () {
    $('body').on('click', '#create-promocode', create_promocode);
    $('body').on('change', '.discount_criteria', change_discount_criteria);
    $('body').on('change', '.is_common', change_common);
    $('body').on('click', '#save_promocode', save_promocode);
    $('body').on('click', '#assign_promocode', assign_promocode);
    $('body').on('click', '#promocodes .assign', assign);
    $('body').on('click', '#promocodes .edit', edit_promocode);
    $('body').on('click', '#promocodes .view-usage', view_usage);
    $('body').on('click', '#assign_user', assign_user);
});
function assign_user() {
    var promocode_id = $('#promocode-id').val();
    var promocode_email = $('#promocode_email').val();
    $.ajax({
        type: "POST",
        url: base_url + "promocode/assign_user/" + promocode_id,
        dataType: 'json',
        data: {
            'email': promocode_email,
        },
        success: function (response) {
            alert('Assigned to 1 User ');
            window.location = window.location;
        },
    });
}
function view_usage() {
    var promocode_id = $(this).data('id');
    $.ajax({
        type: "POST",
        url: base_url + "promocode/view_usage/" + promocode_id,
        dataType: 'html',
        success: function (response) {
            $('#promocode .modal-content').html(response);
            $('#promocode').modal();
            var dataTable = $('#promocode_usage').DataTable({
                "processing": false,
                "serverSide": true,
                "ajax": base_url + 'promocode/json_view_usage/' + promocode_id,
                "columns": [
                    {"data": 'order_id'},
                    {"data": 'invoice_no'},
                    {"data": 'email'},
                    {"data": 'gross_amt'},
                    {"data": 'discount'},
                ]

            });
        },
    });
}

function edit_promocode() {
    var promocode_id = $(this).data('id');
    $.ajax({
        type: "POST",
        url: base_url + "promocode/create/" + promocode_id,
        dataType: 'html',
        success: function (response) {
            $('#promocode .modal-content').html(response);
            $('#promocode').modal();
            change_discount_criteria();
            change_common();
        },
    });
}
function assign() {
    var promocode_id = $(this).data('id');
    show_assign_form(promocode_id);
}
function create_promocode() {
    $.ajax({
        type: "POST",
        url: base_url + "promocode/create/",
        dataType: 'html',
        success: function (response) {
            $('#promocode .modal-content').html(response);
            $('#promocode').modal();
            $('#expiry_date').datepicker({
                minDate: '0',
                dateFormat:'yy-mm-dd'
            });
            change_discount_criteria();
            change_common();
        },
    });
}
function assign_promocode() {
    if (!$('.promo_user_id:checked').length) {
        alert('Select atleast one user to assign promocode');
        return false;
    }
    var promo_users = '';
    $('.promo_user_id:checked').each(function () {
        promo_users += $(this).data('id') + ',';
    });
    var promocode_id = $('#promocode-id').val();
    $.ajax({
        type: "POST",
        url: base_url + "promocode/assign/" + promocode_id,
        dataType: 'json',
        data: {
            'promo_users': promo_users,
        },
        success: function (response) {
            alert('Assigned to ' + response.no_assigned + ' Users ');
            window.location = window.location;
        },
    });

}
function change_common() {
    if ($('.is_common').is(':checked')) {
        $('.one_time').show();
    } else {
        $('.one_time').hide();
    }
}
function change_discount_criteria() {
    if ($('.discount_criteria').val() == 'percentage') {
        $('.discount_amount').hide();
        $('.discount_per').show();
    } else {
        $('.discount_amount').show();
        $('.discount_per').hide();
    }
}
function save_promocode() {
    var data = $('#promocode-form').serialize();
    $.ajax({
        type: "POST",
        url: base_url + "promocode/save/",
        dataType: 'json',
        data: data,
        success: function (response) {
            if (response.status == 'success') {
                if (response.is_common == '0') {
                    show_assign_form(response.promocode_id);
                } else {
                    alert('Promocode added successfully');
                    window.location = window.location;
                }
            } else {
                $('.error').html(response.error);
            }
        },
    });
}

function show_assign_form(promocode_id) {
    $.ajax({
        type: "POST",
        url: base_url + "promocode/assign_form/" + promocode_id,
        dataType: 'html',
        success: function (response) {
            $('#promocode .modal-content').html(response);
            $('#promocode').modal();
            var dataTable = $('#promocode_users').DataTable({
                "processing": false,
                "serverSide": true,
                "ajax": base_url + 'promocode/json_promousers/' + promocode_id,
                "columns": [
                    {"data": 'c', "bSortable": false},
                    {"data": 'email'},
                    {"data": 'is_assigned'},
                ]

            });
        },
    });
}
