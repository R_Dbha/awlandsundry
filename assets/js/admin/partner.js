var step1_main_slider, step2_main_slider, step3_main_slider, step4_main_slider, step3_current_shoe;
var steps_slider;
$(document).ready(
	function(){
		$('#orders tbody tr').on('mouseover', function(){
			$(this).find('.quick-view').show();
		});
		$('#orders tbody tr').on('mouseout', function(){
			$(this).find('.quick-view').hide();
		});
		$('body').on('click', '#create-order', show_create_order_modal);
		$('body').on('click', '.create-new-shoe', create_new_shoe);
		$('body').on('click', '.select-existing-shoe', select_existing_shoe);
		$('body').on('click', '.select-design-number', get_by_design_number);
		$('body').on('change', '.change-style', change_style);
		$('body').on('click', '.select-style', select_style);
		$('body').on('change', '.change-last', change_last);
		$('body').on('click', '.select-last', select_last);
		$('body').on('click', '.select-main-back', select_main_back);
		$('body').on('click', '.select-style-back', select_style_back);
		$('body').on('click', '.select-last-back', select_last_back);
		$('body').on('click', '.select-design', select_design);
		$('body').on('click', '.select-design-back', select_design_back);
		$('body').on('click', '.leather-types', change_leather_type);
		$('body').on('mouseover', '.leather-color .select-color',
			leather_color_hover_in);
		$('body').on('mouseleave', '.select-color', color_hover_out);
		$('body').on('mouseover', '.stitch-lace-color .select-color ',
			color_hover_in);
		$('body').on('click', '.leather-color .select-color',
			select_leather_color);
		$('body').on('click', '.stitch-lace-color .select-color',
			select_stitch_lace_color);
		$('body').on('change', '#sizing input, #sizing select', set_size);
		$('body').on('change', '.monogram-text', set_monogram);
		$('body').on('click', '.confirm-design', add_to_cart);
		$('body').on('click', '.remove-cart-item', remove_cart_item);
		$('body').on('change', '.cart-item .quantity', update_quantity);
		$('#orders tbody tr').on('click', show_order_details);
		$('body').on('change', '.show-selected', show_selected);
		$('body').on('change', '.type, .user_list', show_shoes);
		$('body').on('change', '.style', get_types);
		$('body').on('click', '.select-and-proceed', proceed);
		$('body').on('click', '.back', back);
		$('body').on('click', '#confirm-item', order_add_item);
		$('body').on('click', '#btn-edit-shipping', show_edit_shipping);
		$('body').on('click', '#btn-edit-billing', show_edit_billing);
		$('body').on('click', '.btn-edit-sizing', show_edit_sizing);
		$('body').on('click', '.btn-edit-shoe', show_edit_shoe);
		$('body').on('click', '#btn-edit-comment', show_edit_comment);
		$('body').on('click', '#done-edit-billing', done_edit_billing);
		$('body').on('click', '#done-edit-shipping', done_edit_shipping);
		$('body').on('click', '.done-edit-size', done_edit_size);
		$('body').on('click', '#done-edit-comment', done_edit_comment);
		$('body').on('click', '.done-edit-item', done_edit_item);
		$('body').on('click', '.delete-item', delete_item);
		$('body').on('change', '#customer', populate_address);
		$('body').on('click', '#confirm-order', confirm_order);
		$('body').on('change',
			'#shipping-billing-address .shipping-address input',
			on_change_shipping);
		$('body').on('change',
			'#shipping-billing-address .billing-address input',
			on_change_billing);
		$('body').on('click', '.save-custom-item', save_custom_item);
		$('#order-details').on('hidden.bs.modal', function(){
			$('#order-details .modal-body').html('');
		});
		$('#order-create').on('hidden.bs.modal', function(){
			$('#order-create .modal-body').html('');
		});
	});
function remove_cart_item(){
	var box = $(this).closest('.box');
	var row_id = $(this).parent('.box-tools').find('.cart-id').val();
	var cart_item = $(this).parents('.cart-item');
	if(confirm('Are you sure you want to remove the item ?')){
		$(box).children('.overlay').show();
		$(box).children('.loading-img').show();
		$.ajax({
			type : "POST",
			url : baseUrl + "partners/remove_cart_item",
			data : {
				row_id : row_id
			},
			success : function(response){
				$(cart_item).remove();
				$(box).children('.overlay').hide();
				$(box).children('.loading-img').hide();
				var amount = 0;
				$('.subtotal').each(function(i){
					amount += parseInt($(this).val());
				});
				$('#amount_paying').val(amount);
			}
		});
	}
}
function update_quantity(){
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	var cart_item = $(this).parents('.cart-item');
	var row_id = $(cart_item).find('.cart-id').val();
	var quantity = $(cart_item).find('.quantity').val();
	$.ajax({
		type : "POST",
		url : baseUrl + "partners/update_quantity",
		data : {
			row_id : row_id,
			quantity : quantity
		},
		success : function(response){
			$(cart_item).find('.subtotal').val(
				$(cart_item).find('.price').val()
					* $(cart_item).find('.quantity').val());
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
			var amount = 0;
			$('.subtotal').each(function(i){
				amount += parseInt($(this).val());
			});
			$('#amount_paying').val(amount);
		}
	});
}
function set_monogram(){
	if($(this).val().trim() === "" || /^[a-zA-Z()]+$/.test($(this).val())){
		ShoeModel.monogram.text = ($(this).val()).toUpperCase();
		var index = $('#design-visualA4').index();
		ModelRenderer.container.main = '#design-visualA4';
		ShoeModel.angle = "A4";
		ShoeModel.monogram.leftSide = 1;
		$.when(ModelRenderer.renderAllAngles()).then(function(){
			step4_main_slider.goToSlide(index);
		});
	}else{
		alert("Please use alphabets only.");
	}
}
function set_size(){
	var m_lsize = $('#sizing .m_lsize').val();
	var m_lwidth = $('#sizing .m_lwidth').val();
	var m_lgirth = $('#sizing .m_lgirth').val();
	var m_linstep = $('#sizing .m_linstep').val();
	var m_rsize = $('#sizing .m_rsize').val();
	var m_rwidth = $('#sizing .m_rwidth').val();
	var m_rgirth = $('#sizing .m_rgirth').val();
	var m_rinstep = $('#sizing .m_rinstep').val();
	var lsize = $('#sizing .lsize').val();
	var lwidth = $('#sizing .lwidth').val();
	var rsize = $('#sizing .rsize').val();
	var rwidth = $('#sizing .rwidth').val();
	ShoeModel.measurement.left.size = m_lsize;
	ShoeModel.measurement.left.width = m_lwidth;
	ShoeModel.measurement.right.size = m_rsize;
	ShoeModel.measurement.right.width = m_rwidth;
	ShoeModel.measurement.left.girth = m_lgirth;
	ShoeModel.measurement.left.instep = m_linstep;
	ShoeModel.measurement.right.girth = m_rgirth;
	ShoeModel.measurement.right.instep = m_rinstep;
	ShoeModel.size.left.value = lsize;
	ShoeModel.size.left.text = lsize;
	ShoeModel.size.left.width = lwidth;
	ShoeModel.size.right.value = rsize;
	ShoeModel.size.right.text = rsize;
	ShoeModel.size.right.width = rwidth;
}
function select_leather_color(){
	var tab_pane = $(this).closest('.tab-pane');
	feature = ($(tab_pane).attr('id')).replace('tab-', '');
	var code = $(this).data('material-code');
	var folder = $(this).data('material-folder');
	var clrCode = $(this).data('color-code');
	var matId = $(this).data('material-id');
	var clrId = $(this).data('color-id');
	ShoeModel[feature].material = code;
	ShoeModel[feature].matfolder = folder;
	ShoeModel[feature].color = clrCode;
	ShoeModel[feature].matId = matId;
	ShoeModel[feature].clrId = clrId;
	ModelRenderer.renderAllAngles();
	ModelRenderer.showPrice();
	$(tab_pane).find('.select-color').removeClass('selected');
	$(this).addClass('selected');
}
function select_stitch_lace_color(){
	var tab_pane = $(this).closest('.tab-pane');
	feature = ($(tab_pane).attr('id')).replace('tab-', '');
	var code = $(this).data('code');
	var name = $(this).data('color-name');
	var id = $(this).data('id');
	ShoeModel[feature].code = code;
	ShoeModel[feature].name = name;
	ShoeModel[feature].id = id;
	ModelRenderer.renderAllAngles();
	$(tab_pane).find('.select-color').removeClass('selected');
	$(this).addClass('selected');
}
function leather_color_hover_in(){
	var pos = ($(this).offset().left - $(this).closest('.leather-colors')
		.offset().left)
		/ $(this).closest('.leather-colors').width() * 100;
	var preview = $(this).closest('.leather-colors').children('.preview');
	if(pos >= 50){
		$(preview).css({
			'left' : '0px'
		});
	}else{
		$(preview).css({
			'left' : ($(this).closest('.leather-colors').width() - 130) + 'px'
		});
	}
	$(preview).show();
	$(preview).find('img').attr('src', $(this).attr('src'));
	$(preview).find('span.pull-left').text($(this).data('material-name'));
	$(preview).find('span.pull-right').text($(this).data('color-name'));
}
function color_hover_in(){
	var pos = ($(this).offset().left - $(this).closest('.box-body').offset().left)
		/ $(this).closest('.box-body').width() * 100;
	var preview = $(this).closest('.stitch-lace-color').children('.preview');
	if(pos >= 50){
		$(preview).css({
			'left' : '0px'
		});
	}else{
		$(preview).css({
			'left' : ($(this).closest('.box-body').width() - 130) + 'px'
		});
	}
	$(preview).show();
	$(preview).find('div.preview-color').css({
		'background' : $(this).data('color-code')
	});
	$(preview).find('span.pull-left').text($(this).data('color-name'));
}
function color_hover_out(){
	$('.preview').hide();
}
function show_create_order_modal(){
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "partners/create_order/",
		dataType : 'html',
		success : function(response){
			$('#order-create .modal-body').html(response);
			$('#order-create').modal();
			setTimeout(function(){
				step1_main_slider = $('.step1_main_slider').bxSlider({
					infiniteLoop : false,
					preloadImages : 'all',
					controls : true,
					prevText : '<i class="fa fa-arrow-left"></>',
					nextText : '<i class="fa fa-arrow-right"></>',
					hideControlOnEnd : true,
					onSliderLoad : function(){
						$('.step1_main_slider').css({
							'visibility' : 'visible'
						});
					},
					onSlideAfter : function(e, i, j){
						$('.change-style option').eq(j).prop('selected', true);
					}
				});
				steps_slider = $('#steps').bxSlider({
					controls : false,
					randomStart : false,
					infiniteLoop : false,
					hideControlOnEnd : true,
					preloadImages : 'all',
					useCSS : false
				// mode: 'fade',
				});
				$(box).children('.overlay').hide();
				$(box).children('.loading-img').hide();
				$('#sizing').children('.overlay').show();
				$('#details').children('.overlay').show();
			}, 300);
			$.ajax({
				type : "POST",
				url : baseUrl + "common/get_stripe_key/",
				success : function(response){
					Stripe.setPublishableKey(response);
				}
			});
			$("#order-form").validate({
				submitHandler : submit,
				rules : {
					"card-cvc" : {
						cardCVC : true,
						required : true
					},
					"card-number" : {
						cardNumber : true,
						required : true
					},
					"card-expiry-year" : "cardExpiry" // we don't validate
				// month
				// separately
				},
				invalidHandler : function(event, validator){
					return false;
				},
			});
			// add custom rules for credit card validating
			jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber,
				"Please enter a valid card number");
			jQuery.validator.addMethod("cardCVC", Stripe.validateCVC,
				"Please enter a valid security code");
			jQuery.validator.addMethod("cardExpiry", validateExpiry,
				"Please enter a valid expiration");
			addInputNames();
			$('#card-number').formance('format_credit_card_number');
		},
	});
}
function validateExpiry(){
	return Stripe.validateExpiry($("select.card-expiry-month").val(), $(
		"select.card-expiry-year").val());
}
function validateCard(){
	$('#card-number').validate();
	return Stripe.validateCardNumber($('#card-number').val())
		&& Stripe.validateCVC($('#card-cvc').val()) && validateExpiry();
}
function submit(form){
	removeInputNames(); // THIS IS IMPORTANT!
	$(form['review_submit']).attr("disabled", "disabled")
	Stripe.createToken({
		number : $('.card-number').val(),
		cvc : $('.card-cvc').val(),
		exp_month : $('.card-expiry-month').val(),
		exp_year : $('.card-expiry-year').val(),
	}, function(status, response){
		if(response.error){
			$(form['submit-button']).removeAttr("disabled");
			$("#order-form span.error").html(response.error.message);
			addInputNames();
			return false;
		}else{
			var token = response['id'];
			var data = $("#order-form").serialize();
			data += '&token=' + token;
			$.ajax({
				type : "POST",
				url : baseUrl + "partners/save_order/",
				data : data,
				success : function(response){
					if(response !== 'error'){
						$('.order-confirmation .row').html(response);
						$('.order-review').hide();
						$('.order-confirmation').show();
					}else{
						alert('Something went wrong, Please try again later');
						$(form['review_submit']).removeAttr("disabled");
						addInputNames();
					}
				}
			});
		}
	});
	return false;
}
function addInputNames(){
	// Not ideal, but jQuery's validate plugin requires fields to have names
	// so we add them at the last possible minute, in case any javascript
	// exceptions have caused other parts of the script to fail.
	$(".card-number").attr("name", "card-number")
	$(".card-cvc").attr("name", "card-cvc")
	$("select.card-expiry-year").attr("name", "card-expiry-year")
}
function removeInputNames(){
	$(".card-number").removeAttr("name")
	$(".card-cvc").removeAttr("name")
	$("select.card-expiry-year").removeAttr("name")
	$("select.card-expiry-month").removeAttr("name")
}
function change_style(){
	var index = $(this).find("option:selected").index();
	step1_main_slider.goToSlide(index);
}
function change_last(){
	var index = $(this).find("option:selected").index();
	step2_main_slider.goToSlide(index);
}
function select_existing_shoe(){
	$('.select-shop-shoe').show();
	$('.toggle').removeClass('select-last-back').addClass('select-main-back');
	$('#step3 .slide_container').html('');
	steps_slider.goToSlide(3);
}
function create_new_shoe(){
	$('.create-shoe').show();
	steps_slider.goToSlide(1);
}
function select_main_back(){
	$('.create-shoe, .select-shop-shoe').hide();
	steps_slider.goToSlide(0);
	$('.change-style').attr('disabled', 'disabled');
}
function select_style_back(){
	steps_slider.goToSlide(1);
	$('.change-last').attr('disabled', 'disabled');
	$('.change-style').removeAttr('disabled');
}
function select_last_back(){
	steps_slider.goToSlide(2);
	$('.change-last').removeAttr('disabled');
}
function select_design_back(){
	$('#sizing').children('.overlay').show();
	$('#details').children('.overlay').show();
	steps_slider.goToSlide(3);
}
function change_leather_type(){
	var parent = $(this).closest('.tab-pane');
	$(parent).find('.leather-colors .colors').hide();
	$(parent).find('.leather-colors .' + $(this).val()).show();
}
function select_style(){
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	var style = $('.change-style').val();
	$.ajax({
		type : "POST",
		url : base_url + "partners/select_style/",
		dataType : 'html',
		data : {
			'style' : style,
		},
		success : function(response){
			$('#step2 .slide_container').html(response);
			setTimeout(function(){
				step2_main_slider = $('.step2_main_slider').bxSlider({
					infiniteLoop : false,
					preloadImages : 'all',
					controls : true,
					prevText : '<i class="fa fa-arrow-left"></>',
					nextText : '<i class="fa fa-arrow-right"></>',
					hideControlOnEnd : true,
					onSliderLoad : function(){
						$('.step2_main_slider').css({
							'visibility' : 'visible'
						});
					},
					onSlideAfter : function(e, i, j){
						$('.change-last option').eq(j).prop('selected', true);
					}
				});
				step2_main_slider.goToSlide(3);
				steps_slider.goToSlide(2);
				$('.change-last').removeAttr('disabled');
				$('.change-style').attr('disabled', 'disabled');
				$(box).children('.overlay').hide();
				$(box).children('.loading-img').hide();
			}, 300);
		},
	});
}
function get_by_design_number(){
	design_number = $('.design-number').val();
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	shop = true;
	$.ajax({
		type : "POST",
		url : base_url + "partners/get_by_design_number/",
		dataType : 'html',
		data : {
			'design_number' : design_number,
		},
		success : function(response){
			$('#step3 .slide_container').html(response);
			setTimeout(function(){
				step3_main_slider = $('.step3_main_slider').bxSlider(
					{
						infiniteLoop : false,
						preloadImages : 'all',
						controls : true,
						prevText : '<i class="fa fa-arrow-left"></>',
						nextText : '<i class="fa fa-arrow-right"></>',
						hideControlOnEnd : true,
						onSliderLoad : function(){
							$('.step3_main_slider').css({
								'visibility' : 'visible'
							});
							step3_current_shoe = $('.step3_main_slider li').eq(
								0).find('.shoe_design_id').val();
							temp = $('.step3_main_slider li').eq(0).find(
								'.design_name').val();
							$('.design').val(temp);
						},
						onSlideAfter : function(e, i, j){
							step3_current_shoe = $('.step3_main_slider li').eq(
								j).find('.shoe_design_id').val();
							temp = $('.step3_main_slider li').eq(j).find(
								'.design_name').val();
							$('.design').val(temp);
						}
					});
				$('.toggle').removeClass('select-main-back').addClass(
					'select-last-back');
				steps_slider.goToSlide(3);
				$('.change-last').attr('disabled', 'disabled');
				$(box).children('.overlay').hide();
				$(box).children('.loading-img').hide();
			}, 300);
		},
	});
}
var shop = true;
function select_last(){
	var last = $('.change-last').val();
	var style = $('.change-style').val();
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	shop = false;
	$.ajax({
		type : "POST",
		url : base_url + "partners/select_last/",
		dataType : 'html',
		data : {
			'last' : last,
			'style' : style,
		},
		success : function(response){
			$('#step3 .slide_container').html(response);
			setTimeout(function(){
				step3_main_slider = $('.step3_main_slider').bxSlider(
					{
						infiniteLoop : false,
						preloadImages : 'all',
						controls : true,
						prevText : '<i class="fa fa-arrow-left"></>',
						nextText : '<i class="fa fa-arrow-right"></>',
						hideControlOnEnd : true,
						onSliderLoad : function(){
							$('.step3_main_slider').css({
								'visibility' : 'visible'
							});
							step3_current_shoe = $('.step3_main_slider li').eq(
								0).find('.shoe_design_id').val();
							temp = $('.step3_main_slider li').eq(0).find(
								'.design_name').val();
							$('.design').val(temp);
						},
						onSlideAfter : function(e, i, j){
							step3_current_shoe = $('.step3_main_slider li').eq(
								j).find('.shoe_design_id').val();
							temp = $('.step3_main_slider li').eq(j).find(
								'.design_name').val();
							$('.design').val(temp);
						}
					});
				$('.toggle').removeClass('select-main-back').addClass(
					'select-last-back');
				steps_slider.goToSlide(3);
				$('.change-last').attr('disabled', 'disabled');
				$(box).children('.overlay').hide();
				$(box).children('.loading-img').hide();
			}, 300);
		},
	});
}
function select_design(){
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "partners/select_design/",
		dataType : 'json',
		data : {
			'shoe_design_id' : step3_current_shoe,
		},
		success : function(response){
			ShoeModel = response.model;
			ModelRenderer.config.renderedWidth = 614;
			ModelRenderer.config.renderedHeight = 347;
			$.when(ModelRenderer.renderAllAngles()).then(function(){
				setTimeout(function(){
					step4_main_slider = $('.step4_main_slider').bxSlider({
						infiniteLoop : false,
						preloadImages : 'all',
						controls : true,
						prevText : '<i class="fa fa-arrow-left"></>',
						nextText : '<i class="fa fa-arrow-right"></>',
						hideControlOnEnd : true,
						onSliderLoad : function(){
							$('.step4_main_slider').css({
								'visibility' : 'visible'
							});
						},
						onSlideAfter : function(e, i, j){
						}
					});
					ModelRenderer.showMaterialsAndColors(response.materials);
					ModelRenderer.showStitches(response.stitches);
					if(typeof response.laces !== 'undefined'){
						$('.tab-lacing').show();
						ModelRenderer.showLaces(response.laces);
					}else{
						$('.tab-lacing').hide();
					}
					steps_slider.goToSlide(4);
					$(box).children('.overlay').hide();
					$(box).children('.loading-img').hide();
					$('#sizing').children('.overlay').hide();
					if(shop){
						$('#details').children('.overlay').show();
					}else{
						$('#details').children('.overlay').hide();
					}
				}, 300);
			});
		},
	});
}
ModelRenderer.panels = ['quarter', 'toe', 'eyestay', 'vamp', 'foxing'];
ModelRenderer.showStitches = function(stitches){
	var html = '';
	$(stitches).each(
		function(i, stitch){
			selected = (ShoeModel.stitch.id == stitch.stitch_id) ? 'selected'
				: '';
			html += '<div class="col-xs-3"> <div class="select-color '
				+ selected + '"  data-color-name = "' + stitch.stitch_name
				+ '" data-code = "' + stitch.stitch_code
				+ '" data-color-code = "' + stitch.stitch_color
				+ '" data-id = "' + stitch.stitch_id + '" style="background:'
				+ stitch.stitch_color + '" ></div></div>';
		});
	$('#tab-stitch .stitching-color').append(
		'<div class="colors">' + html + '</div>');
}
ModelRenderer.showLaces = function(laces){
	var html = '';
	$(laces).each(
		function(i, lace){
			selected = (ShoeModel.lace.id == lace.lace_id) ? 'selected' : '';
			html += '<div class="col-xs-3"> <div class="select-color '
				+ selected + '"  data-color-name = "' + lace.lace_name
				+ '" data-code = "' + lace.lace_code + '" data-id = "'
				+ lace.lace_id + '" data-color-code = "' + lace.color_code
				+ '" style="background:' + lace.color_code + '" ></div></div>';
		});
	$('#tab-lace .lacing-color').append(
		'<div class="colors">' + html + '</div>');
}
ModelRenderer.showMaterialsAndColors = function(materials){
	$(ModelRenderer.panels)
		.each(
			function(i, panel){
				if(typeof materials[panel] !== 'undefined'){
					$('.tab-' + panel).show();
					$('#tab-' + panel + ' .leather-colors .leather-color')
						.html('');
					$('#tab-' + panel + ' .leather-types').html('');
					$(materials[panel])
						.each(
							function(j, material){
								$('#tab-' + panel + ' .leather-types').append(
									'<option value="'
										+ material.material_folder + '">'
										+ material.material_name + '</option>');
								var html = '';
								$(material.colors)
									.each(
										function(k, color){
											selected = (ShoeModel[panel].clrId == color.clrId) ? 'selected'
												: '';
											html += '<div class="col-xs-3"> <img class="select-color '
												+ selected
												+ '" data-color-name = "'
												+ color.clrName
												+ '" data-material-code = "'
												+ material.material_code
												+ '" data-material-folder = "'
												+ material.material_folder
												+ '" data-material-id = "'
												+ material.material_id
												+ '" data-color-code = "'
												+ color.clrCode
												+ '" data-color-id = "'
												+ color.clrId
												+ '" src="'
												+ color.url + '"/></div>'
										});
								$(
									'#tab-' + panel
										+ ' .leather-colors .leather-color')
									.append(
										'<div class="colors '
											+ material.material_folder + '">'
											+ html + '</div>');
							});
					$('#tab-' + panel + ' .leather-types').val(
						ShoeModel[panel].matfolder);
					$('#tab-' + panel + ' .leather-colors .leather-color')
						.find('.' + ShoeModel[panel].matfolder).show();
				}else{
					$('.tab-' + panel).hide();
				}
			});
}
ModelRenderer.angles = ['A7', 'A0', 'A4'];
ModelRenderer.renderAllAngles = function(){
	var html = '';
	$('#step4 .step4_main_slider').html();
	$(ModelRenderer.angles).each(
		function(i, angle){
			ModelRenderer.container = {
				main : '#design-visual' + angle,
				quarter : '#quarter' + angle,
				toe : '#toe' + angle,
				eyestay : '#eyestay' + angle,
				vamp : '#vamp' + angle,
				foxing : '#foxing' + angle,
				lace : '#lace' + angle,
				stitch : '#stitch' + angle,
			// svg : '#style-svg' + angle,
			// canvas : '#pic-canvas' + angle
			};
			html = '<div id="design-visual' + angle
				+ '" class="design-visual">' + '<div class="visualImgWrap">'
				+ '<img id="quarter' + angle + '" />' + '<img id="stitch'
				+ angle + '" />' + '<img id="lace' + angle + '" />'
				+ '<img id="toe' + angle + '" />' + '<img id="vamp' + angle
				+ '" />' + '<img id="eyestay' + angle + '" />'
				+ '<img id="foxing' + angle + '" />';
			// + '<div id="style-svg'
			// + angle
			// + '" class="style-svg" style="z-index: 0;"></div>'
			// + '<canvas style="display:none;" height="347" width="614"
			// id="pic-canvas'
			// + angle + '" class="pic-canvas"></canvas>'
			if(angle == 'A4'){
				html += '<div class="monogram" style="display: none;">'
					+ '<div class="mono-image"></div>' + '</div>'
			}
			html += '</div>' + '</div>';
			$('#step4 .step4_main_slider').append(html);
			ShoeModel.angle = angle;
			ModelRenderer.renderBase();
			ModelRenderer.renderStitch();
			ModelRenderer.renderLace();
			ModelRenderer.renderMaterialColor();
			ModelRenderer.renderMonogram();
		});
	ModelRenderer.container.pricing = '#custom-price';
	ModelRenderer.showPrice();
}
ModelRenderer.showPrice = function(){
	$.ajax({
		url : baseUrl + 'partners/getPrice',
		data : {
			ShoeModel : ShoeModel
		},
		type : 'POST',
		success : function(msg){
			$(ModelRenderer.container.pricing + ' span.price').html(msg);
		}
	});
};
function add_to_cart(){
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		url : baseUrl + 'partners/addtocart',
		data : {
			ShoeModel : ShoeModel
		},
		dataType : 'html',
		type : 'POST',
		success : function(response){
			$('.cart-items').html(response);
			var amount = 0;
			$('.subtotal').each(function(i){
				amount += parseInt($(this).val());
			});
			$('#amount_paying').val(amount);
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
			$("a[href='#order-review']").tab('show');
		}
	});
};
function confirm_order(){
	var customer = $('#customer').val();
	var firstname = $('#shipping_firstname').val();
	var lastname = $('#shipping_lastname').val();
	var address1 = $('#shipping_address1').val();
	var address2 = $('#shipping_address2').val();
	var city = $('#shipping_city').val();
	var country = $('#shipping_country').val();
	var state = $('#shipping_state').val();
	var zipcode = $('#shipping_zipcode').val();
	var telephone = $('#shipping_telephone').val();
	var shipping_address = {
		'firstname' : firstname,
		'lastname' : lastname,
		'address1' : address1,
		'address2' : address2,
		'city' : city,
		'state' : state,
		'country' : country,
		'zipcode' : zipcode,
		'telephone' : telephone,
		'user_id' : customer
	};
	firstname = $('#billing_first_name').val();
	lastname = $('#billing_last_name').val();
	address1 = $('#billing_address1').val();
	address2 = $('#billing_address2').val();
	city = $('#billing_city').val();
	country = $('#billing_country').val();
	state = $('#billing_state').val();
	zipcode = $('#billing_zipcode').val();
	billing_address = {
		'address1' : address1,
		'address2' : address2,
		'city' : city,
		'state' : state,
		'country' : country,
		'zipcode' : zipcode,
		'user_id' : customer
	};
	var belt = $('#belt').is(':checked') ? '1' : '0';
	var data = {
		'billing_address' : billing_address,
		'shipping_address' : shipping_address,
		'gross_amt' : $('#subtotal').val(),
		'user_id' : customer,
		'comments' : $('#order_comments').val(),
		'order_source' : $('#order-source').val(),
		'belt_needed' : belt,
	}
	$.ajax({
		type : "POST",
		url : base_url + "orders/confirm_order/",
		dataType : 'html',
		data : data,
		success : function(response){
			if(response == '1'){
				window.location = window.location;
			}
		},
	});
}
function save_custom_item(){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	var attachment_ids = $(container).find('.attachment_id').val();
	var price = $(container).find('.u_price').val();
	var style = $(container).find('.u_style').val();
	var last = $(container).find('.u_last').val();
	var customer = $('#customer').val();
	if(container != '#add-item'){
		action = 'edit';
	}else{
		action = 'add';
	}
	$.ajax({
		type : "POST",
		url : base_url + "orders/save_custom_item/",
		data : {
			'attachment_ids' : attachment_ids,
			'price' : price,
			'style_name' : style,
			'last_id' : last,
			'customer' : customer,
			'action' : action
		},
		dataType : 'html',
		success : function(response){
			$(container).find('.item-details').html(response);
			$(container).find('.new-item-images').bxSlider({
				controls : true,
				prevText : '<i class="fa fa-arrow-left"></>',
				nextText : '<i class="fa fa-arrow-right"></>',
			});
		},
	});
	$(container).find('.order_add_item').css({
		'margin-left' : '-100%'
	});
}
function populate_address(){
	var customer_id = $(this).val();
	$.ajax({
		type : "POST",
		url : base_url + "orders/shipping_billing_address/" + customer_id,
		dataType : 'json',
		success : function(response){
			$('#shipping-billing-address .shipping-address').html(
				response.shipping);
			$('#shipping-billing-address .billing-address').html(
				response.billing);
		},
	});
}
function on_change_billing(){
	var firstname = $('#billing_first_name').val();
	var lastname = $('#billing_last_name').val();
	var address1 = $('#billing_address1').val();
	var address2 = $('#billing_address2').val();
	var city = $('#billing_city').val();
	var country = $('#billing_country').val();
	var state = $('#billing_state').val();
	var zipcode = $('#billing_zipcode').val();
	$('#order-review .billing-address').html(
		'<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>' + address1
			+ ', ' + address2 + '</h5><h5>' + city + ', ' + state + ', '
			+ country + ',' + zipcode + '</h5>');
}
function on_change_shipping(){
	var firstname = $('#shipping_firstname').val();
	var lastname = $('#shipping_lastname').val();
	var address1 = $('#shipping_address1').val();
	var address2 = $('#shipping_address2').val();
	var city = $('#shipping_city').val();
	var country = $('#shipping_country').val();
	var state = $('#shipping_state').val();
	var zipcode = $('#shipping_zipcode').val();
	var telephone = $('#shipping_telephone').val();
	$('#order-review .shipping-address').html(
		'<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>' + address1
			+ ', ' + address2 + '</h5><h5>' + city + ', ' + state + ', '
			+ country + ',' + zipcode + '</h5></h5>' + telephone + '</h5>');
}
function show_edit_shoe(){
	var id = '#' + $(this).closest('.tab-pane').attr('id');
	$("a[href='" + id + "']").html('Edit ' + id.replace('#', ''));
	$("a[href='" + id + "']").next('i').hide();
	var design_id = $(id).find('.design_id').val();
	var order_id = $('#order_id').text();
	$(id).children('.details').hide();
	$(id).children('.add-edit').show();
	$('.edit-btn').hide();
}
function delete_item(){
	var id = $(this).parent('a').attr('href');
	var tab = $(this).parents('li.items');
	var tabs;
	var tab_pane = $(id);
	var design_id = $(tab_pane).find('.design_id').val();
	var order_id = $('#order_id').text();
	var confir = confirm('Do you really want to delete the item?');
	var box = $(this).parents('.box');
	var data = {
		'design_id' : design_id,
		'order_id' : order_id
	};
	if(confir){
		$(box).children('.overlay').show();
		$(box).children('.loading-img').show();
		$.ajax({
			type : "POST",
			url : base_url + "orders/delete_item/",
			dataType : 'html',
			data : data,
			success : function(response){
				$(tab).remove();
				$(tab_pane).remove();
				tabs = $('#order-details').find('li.items');
				$(tabs).each(function(key){
					i = $(this).find('i');
					$(this).children('a').html('Item ' + (key + 1));
					$(this).children('a').append(i);
				});
				$('.order-summary').addClass('active');
				$('#order-summary').addClass('active');
				$(box).children('.overlay').hide();
				$(box).children('.loading-img').hide();
			},
		});
	}
}
function show_edit_sizing(e){
	e.preventDefault();
	$('.edit-btn').hide();
	$(this).closest('.box').find('.editable').show();
	$(this).closest('.box').find('.not-editable').hide();
}
function done_edit_comment(e){
	e.preventDefault();
	var editable = $(this).closest('.box').find('.editable');
	var not_editable = $(this).closest('.box').find('.not-editable');
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	var order_id = $('#order_id').text();
	var comment = $('#edit-comment textarea').val();
	var data = {
		'comments' : comment,
	}
	$.ajax({
		type : "POST",
		url : base_url + "orders/edit_comment/" + order_id,
		data : data,
		dataType : 'html',
		success : function(response){
			$(not_editable).html(comment);
			$(editable).hide();
			$(not_editable).show();
			$('.edit-btn').show();
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
		}
	});
}
function done_edit_size(e){
	e.preventDefault();
	var editable = $(this).closest('.box').find('.editable');
	var not_editable = $(this).closest('.box').find('.not-editable');
	var left_shoe = $(editable).find('.left_shoe').val();
	var left_width = $(editable).find('.left_width').val();
	var right_shoe = $(editable).find('.right_shoe').val();
	var right_width = $(editable).find('.right_width').val();
	var m_left_size = $(editable).find('.m_left_size').val();
	var m_left_width = $(editable).find('.m_left_width').val();
	var m_right_size = $(editable).find('.m_right_size').val();
	var m_right_width = $(editable).find('.m_right_width').val();
	var quantity = $(editable).find('.quantity').val();
	var design_id = $(editable).find('.design_id').val();
	var order_id = $('#order_id').text();
	var data = {
		'left_shoe' : left_shoe,
		'left_width' : left_width,
		'right_shoe' : right_shoe,
		'right_width' : right_width,
		'm_left_size' : m_left_size,
		'm_left_width' : m_left_width,
		'm_right_size' : m_right_size,
		'm_right_width' : m_right_width,
		'quantity' : quantity
	};
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "orders/edit_details/" + order_id + '/' + design_id,
		data : data,
		dataType : 'html',
		success : function(response){
			html = '<dt>Quantity :</dt>' + '<dd>' + quantity + ' PAIR</dd>'
				+ '<dt>Left shoe size :</dt>' + '<dd>' + left_shoe + ' '
				+ left_width + '</dd>' + '<dt>Right shoe size :</dt>' + '<dd>'
				+ right_shoe + ' ' + right_width + '</dd>'
				+ '<dt>Left shoe measurement :</dt>' + '<dd>' + m_left_size
				+ 'cm - ' + m_left_width + 'cm</dd>'
				+ '<dt>Right shoe measurement :</dt>' + '<dd>' + m_right_size
				+ 'cm - ' + m_right_width + 'cm</dd>';
			$(not_editable).html(html);
			$(editable).hide();
			$(not_editable).show();
			$('.edit-btn').show();
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
		}
	});
}
function done_edit_shipping(){
	var order_id = $('#order_id').text();
	var firstname = $('#shipping_firstname').val();
	var lastname = $('#shipping_lastname').val();
	var address1 = $('#shipping_address1').val();
	var address2 = $('#shipping_address2').val();
	var city = $('#shipping_city').val();
	var country = $('#shipping_country').val();
	var state = $('#shipping_state').val();
	var zipcode = $('#shipping_zipcode').val();
	var telephone = $('#shipping_telephone').val();
	var data = {
		'order_id' : order_id,
		'firstname' : firstname,
		'lastname' : lastname,
		'address1' : address1,
		'address2' : address2,
		'city' : city,
		'state' : state,
		'country' : country,
		'zipcode' : zipcode,
		'telephone' : telephone,
	};
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "orders/edit_shipping_address",
		data : data,
		dataType : 'html',
		success : function(response){
			$('#edit-shipping-address').hide();
			$('#shipping-address').html(
				'<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>'
					+ address1 + ', ' + address2 + '</h5><h5>' + city + ', '
					+ state + ', ' + country + ',' + zipcode + '</h5></h5>'
					+ telephone + '</h5>');
			$('#shipping-address').show();
			$('.edit-btn').show();
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
		}
	});
}
function done_edit_billing(){
	var order_id = $('#order_id').text();
	var firstname = $('#billing_first_name').val();
	var lastname = $('#billing_last_name').val();
	var address1 = $('#billing_address1').val();
	var address2 = $('#billing_address2').val();
	var city = $('#billing_city').val();
	var country = $('#billing_country').val();
	var state = $('#billing_state').val();
	var zipcode = $('#billing_zipcode').val();
	var data = {
		'order_id' : order_id,
		'address1' : address1,
		'address2' : address2,
		'city' : city,
		'state' : state,
		'country' : country,
		'zipcode' : zipcode,
	};
	var box = $(this).closest('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "orders/edit_billing_address",
		data : data,
		dataType : 'html',
		success : function(response){
			$('#edit-billing-address').hide();
			$('#billing-address').html(
				'<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>'
					+ address1 + ', ' + address2 + '</h5><h5>' + city + ', '
					+ state + ', ' + country + ',' + zipcode + '</h5>');
			$('#billing-address').show();
			$('.edit-btn').show();
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
		}
	});
}
function show_edit_comment(e){
	e.preventDefault();
	$('.edit-btn').hide();
	$('#edit-comment').show();
	$('#comment').hide();
}
function show_edit_billing(e){
	e.preventDefault();
	$('.edit-btn').hide();
	$('#edit-billing-address').show();
	$('#billing-address').hide();
}
function show_edit_shipping(e){
	e.preventDefault();
	$('.edit-btn').hide();
	$('#edit-shipping-address').show();
	$('#shipping-address').hide();
}
function order_add_item(e){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	e.preventDefault();
	self = $(this);
	$(this).hide();
	var data = {
		'order_id' : $('#order_id').text(),
		'customer' : $('#customer').val(),
		'design_id' : $(container).find('.new-design_id').val(),
		'custom_collection_id' : $(container).find('.new-custom_collection_id')
			.val(),
		'item_amt' : $(container).find('.new-price').text(),
		'left_shoe' : $(container).find('.left_shoe').val(),
		'right_shoe' : $(container).find('.right_shoe').val(),
		'left_width' : $(container).find('.left_width').val(),
		'right_width' : $(container).find('.right_width').val(),
		'm_left_size' : $(container).find('.m_left_size').val(),
		'm_right_size' : $(container).find('.m_right_size').val(),
		'm_left_width' : $(container).find('.m_left_width').val(),
		'm_right_width' : $(container).find('.m_right_width').val(),
		'quantity' : $(container).find('.new-quantity').val(),
		'monogram' : $(container).find('.new-monogram').val(),
	}
	if(container != '#add-item'){
		var tab_pane = $(this).closest('.tab-pane');
		design_id = $(tab_pane).find('.design_id').val();
		data.old_design_id = design_id;
		data.action = 'edit';
	}else{
		data.action = 'add';
	}
	$
		.ajax({
			type : "POST",
			url : base_url + "orders/order_add_item",
			data : data,
			dataType : 'html',
			success : function(response){
				if(data.action == 'add'){
					$(response).insertBefore("#add-item");
					var cnt = $('li.items').length + 1;
					$('.nav-tabs li').removeClass('active');
					$(
						'<li class="items active"><a href="#item-'
							+ cnt
							+ '"  data-toggle="tab">Item '
							+ cnt
							+ '<i title="Delete Item" '
							+ ' class="delete-item fa fa-times pull-right"></i></a></li>')
						.insertBefore("#add-item-btn");
					$("#add-item").removeClass('active');
					$("#item-" + cnt).addClass('active');
					setTimeout(function(){
						$("#item-" + cnt).find('.order_images').bxSlider({
							infiniteLoop : true,
							preloadImages : 'all',
							controls : true,
							prevText : '<i class="fa fa-arrow-left"></>',
							nextText : '<i class="fa fa-arrow-right"></>',
						});
					}, 100);
				}else{
					$(tab_pane).html($(response).html());
					var id = $(tab_pane).attr('id');
					$("a[href='#" + id + "']").html(capitalize(id));
					$("a[href='#" + id + "']").next('i').show();
					setTimeout(function(){
						$(tab_pane).find('.order_images').bxSlider({
							infiniteLoop : true,
							preloadImages : 'all',
							controls : true,
							prevText : '<i class="fa fa-arrow-left"></>',
							nextText : '<i class="fa fa-arrow-right"></>',
						});
					}, 100);
				}
				self.show();
				back(container);
				update_order_details(data.order_id);
				$('.show-selected').val('');
				$('.attachment_id').val('');
				$('.qq-upload-list').html('');
				$('.collection').html('');
			}
		});
	return false;
}
function update_order_details(order_id){
	var data = {
		'order_id' : order_id
	};
	$.ajax({
		type : "POST",
		url : base_url + "orders/update_order_details",
		data : data,
		dataType : 'json',
		success : function(response){
			if(response.status == 'success'){
				$('#subtotal').val(response.subtotal);
				$('#number_of_items').val(response.number_of_items);
			}
		}
	});
}
function done_edit_item(){
}
function proceed(e){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	e.preventDefault();
	var selected = $(container).find('.show-selected').val();
	var data;
	var order_id = $('#order_id').text();
	var shoe_design_id = $(this).parent('.shoe-details')
		.find('.shoe_design_id').val();
	var price = $(this).parent('.shoe-details').find('.price').text();
	if(selected == 'custom_collection'){
		custom_collection_id = $(this).parent('.shoe-details').find(
			'.custom_collection_id').val()
		data = {
			'shoe_design_id' : shoe_design_id,
			'custom_collection_id' : custom_collection_id,
			'source' : 'custom_collection',
			'price' : price,
			'order_id' : order_id
		};
	}else{
		data = {
			'shoe_design_id' : shoe_design_id,
			'source' : 'saved_shoes',
			'price' : price,
			'order_id' : order_id
		};
	}
	if(container != '#add-item'){
		design_id = $(this).closest('.tab-pane').find('.design_id').val();
		data.design_id = design_id;
		data.action = 'edit';
	}else{
		data.action = 'add';
	}
	$.ajax({
		type : "POST",
		url : base_url + "orders/get_item_details",
		data : data,
		dataType : 'html',
		success : function(response){
			$(container).find('.item-details').html(response);
			$(container).find('.new-item-images').bxSlider({
				controls : true,
				prevText : '<i class="fa fa-arrow-left"></>',
				nextText : '<i class="fa fa-arrow-right"></>',
			});
		}
	});
	$(container).find('.order_add_item').css({
		'margin-left' : '-100%'
	});
}
function back(container){
	if(typeof container == 'object'){
		container = '#' + $(this).parents('.add-edit').attr('id');
	}
	$(container).find('.item-details').html('');
	$(container).find('.order_add_item').css({
		'margin-left' : '0%'
	});
}
function show_selected(){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	$(this).parents('.box-body').find('.group').hide();
	$(this).parents('.box-body').find('.' + $(this).val()).show();
	show_shoes(container);
}
function get_types(){
	var container = '#' + $(this).parents('.add-edit').attr('id');
	var style = $(this).val();
	$.ajax({
		type : "POST",
		url : base_url + "orders/get_types",
		data : {
			'style' : style
		},
		dataType : 'html',
		success : function(response){
			$(container).find('.type').html(response);
			show_shoes(container);
		}
	});
}
function show_shoes(container){
	if(typeof container == 'object'){
		container = '#' + $(this).parents('.add-edit').attr('id');
	}
	var selected = $(container).find('.show-selected').val();
	var data;
	if(selected == 'custom_collection'){
		var style = $(container).find('.style').val();
		var type = $(container).find('.type').val();
		data = {
			'style' : style,
			'type' : type,
			'source' : 'custom_collection'
		};
	}else if(selected == 'saved_shoes'){
		var user_id = $(container).find('.user_list').val();
		data = {
			'user_id' : user_id,
			'source' : 'saved_shoes'
		};
	}
	if(selected !== 'upload_images'){
		$.ajax({
			type : "POST",
			url : base_url + "orders/get_collection",
			data : data,
			dataType : 'html',
			success : function(response){
				$(container).find('.collection').html(response);
				setTimeout(function(){
					$('.shoe_images').bxSlider({
						infiniteLoop : true,
						preloadImages : 'all',
						controls : true,
						prevText : '<i class="fa fa-arrow-left"></>',
						nextText : '<i class="fa fa-arrow-right"></>',
					});
				}, 1);
			}
		});
	}else{
		$(container).find('.collection').html('');
		$('.attachment_id').val('');
		$('.qq-upload-list').html('');
	}
}
function show_order_details(){
	var order_id = $(this).find('.order_id').val();
	var box = $(this).parents('.box');
	$(box).children('.overlay').show();
	$(box).children('.loading-img').show();
	$.ajax({
		type : "POST",
		url : base_url + "orders/order_details",
		data : {
			order_id : order_id,
		},
		dataType : 'html',
		success : function(response){
			$('#order-details .modal-body').html(response);
			$('#order-details').modal('show', {
				backdrop : 'static'
			});
			$(box).children('.overlay').hide();
			$(box).children('.loading-img').hide();
			setTimeout(function(){
				$('.order_images').bxSlider({
					infiniteLoop : true,
					preloadImages : 'all',
					controls : true,
					prevText : '<i class="fa fa-arrow-left"></>',
					nextText : '<i class="fa fa-arrow-right"></>',
				});
			}, 100);
		}
	});
}
$(function(){
	$('#orders').dataTable({
		"bInfo" : true,
		"bPaginate" : true,
		"bLengthChange" : true,
		"bFilter" : true,
		"bSort" : true,
		"bAutoWidth" : true,
		"aaSorting" : [[0, "desc"]]
	});
});
function capitalize(string){
	return string.charAt(0).toUpperCase() + string.slice(1);
}