$(document).ready(function () {
    $('#orders tbody').on('mouseover', 'tr', function () {
        $(this).find('.quick-view').show();
    });
    $('#orders tbody').on('mouseout', 'tr', function () {
        $(this).find('.quick-view').hide();
    });
    $('body').on('click', '#create-order', show_create_order_modal);
    $('#orders tbody').on('click', 'tr', show_order_details);
    $('body').on('change', '.show-selected', show_selected);
    $('body').on('change', '.type, .user_list', show_shoes);
    $('body').on('change', '.style', get_types);
    $('body').on('change', '.rtw_style', show_shoes);
    $('body').on('click', '.select-and-proceed', proceed);
    $('body').on('click', '.back', back);
    $('body').on('click', '#confirm-item', order_add_item);
    $('body').on('click', '#btn-edit-shipping', show_edit_shipping);
    $('body').on('click', '#btn-edit-billing', show_edit_billing);
    $('body').on('click', '#btn-edit-status', show_edit_status);
    $('body').on('click', '#btn-edit-user', show_edit_user_details);
    $('body').on('click', '.btn-edit-sizing', show_edit_sizing);
    $('body').on('click', '.btn-edit-shoe', show_edit_shoe);
    $('body').on('click', '#btn-edit-comment', show_edit_comment);
    $('body').on('click', '#done-edit-billing', done_edit_billing);
    $('body').on('click', '#done-edit-status', done_edit_status);
    $('body').on('click', '#done-edit-user', done_edit_user);
    $('body').on('click', '#done-edit-shipping', done_edit_shipping);
    $('body').on('click', '.done-edit-size', done_edit_size);
    $('body').on('click', '#done-edit-comment', done_edit_comment);
    $('body').on('click', '.done-edit-item', done_edit_item);
    $('body').on('click', '.delete-item', delete_item);
    $('body').on('change', '#customer', populate_address);
    $('body').on('click', '#confirm-order', confirm_order);
    $('body').on('click', '#btn-apply-promo', apply_promo);
    $('body').on('click', '#btn-apply-giftcard', apply_gift_card);
    $('body').on('click', '#btn-remove-promo', remove_promo);
    $('body').on('click', '#btn-remove-giftcard', remove_gift_card);
    $('body').on('click', '.cancel-edit', cancel_edit);
    $('body').on('click', '.btn-revoke-shoe', revoke_shoe_edit);
    $('body').on('click', '.done-revokeshoe', revoke_shoe_done);

    $('body').on('click', '.payment-mode', function () {
        if ($('.payment-mode:checked').val() == 'credit-card') {
            $(this).closest('.box-body').find('.payment').show();
        } else {
            $(this).closest('.box-body').find('.payment').hide();
        }
    });

    $('body').on('change', '#shipping-billing-address .shipping-address input', on_change_shipping);
    $('body').on('change', '#shipping-billing-address .billing-address input', on_change_billing);
    $('body').on('click', '.save-custom-item', save_custom_item);

    $('#order-details').on('hidden.bs.modal', function () {
        var order_id = $('#order_id').text();
        $.ajax({
            type: "POST",
            url: base_url + "orders/update_summary/" + order_id,
            dataType: 'html',
            success: function (response) {
                if (response !== '') {
                    $('#orders').find('.order_id[value="' + order_id + '"]').closest('tr').replaceWith(response);
                }
            }
        });
        $('#order-details .modal-body').html('');
    });

    $('#order-create').on('hidden.bs.modal', function () {
        $('#order-create .modal-body').html('');
    });
});

function apply_promo() {
    var order_id = $('#order_id').text();
    var promocode = $('.promocode').val();
    if (promocode == '') {
        alert('Invalid promocode');
    }

    $.ajax({
        type: "POST",
        url: base_url + "orders/apply_promo/",
        dataType: 'json',
        data: {'order_id': order_id, 'promocode': promocode},
        success: function (response) {
            if (!response.error) {
                $('#apply').addClass('hidden');
                $('#btn-remove-promo').removeClass('hidden');
            }
            alert(response.msg);
        }
    });
}

function apply_gift_card() {
    var order_id = $('#order_id').text();
    var giftcard = $('.giftcard').val();
    if (giftcard == '') {
        alert('Invalid promocode');
    }

    $.ajax({
        type: "POST",
        url: base_url + "orders/apply_giftcard/",
        dataType: 'json',
        data: {'order_id': order_id, 'giftcard': giftcard},
        success: function (response) {
            if (!response.error) {
                $('#apply').addClass('hidden');
                $('#btn-remove-giftcard').removeClass('hidden');
            }
            alert(response.msg);
        }
    });
}

function remove_promo() {
    var order_id = $('#order_id').text();
    $.ajax({
        type: "POST",
        url: base_url + "orders/remove_promo/",
        dataType: 'json',
        data: {order_id: order_id},
        success: function (response) {
            if (!response.error) {
                $('#apply').removeClass('hidden');
                $('#btn-remove-promo').addClass('hidden');
            }
            alert(response.msg);
        }
    });
}

function remove_gift_card() {
    var order_id = $('#order_id').text();
    $.ajax({
        type: "POST",
        url: base_url + "orders/remove_giftcard/",
        dataType: 'json',
        data: {order_id: order_id},
        success: function (response) {
            if (!response.error) {
                $('#apply').removeClass('hidden');
                $('#btn-remove-giftcard').addClass('hidden');
            }
            alert(response.msg);
        }
    });
}

function cancel_edit() {
    $('.edit-btn').show();
    $(this).closest('.box').find('.editable').hide();
    $(this).closest('.box').find('.not-editable').show();
}

function confirm_order() {
    $('#confirm-order').hide();

    var box = $(this).parents('.box');
    $(box).children('.overlay').show();
    $(box).children('.loading-img').show();

    var customer = $('#customer').val();
    var firstname = $('#shipping_firstname').val();
    var lastname = $('#shipping_lastname').val();
    var address1 = $('#shipping_address1').val();
    var address2 = $('#shipping_address2').val();
    var city = $('#shipping_city').val();
    var country = $('#shipping_country').val();
    var state = $('#shipping_state').val();
    var zipcode = $('#shipping_zipcode').val();
    var telephone = $('#shipping_telephone').val();
    var shipping_address = {
        'firstname': firstname,
        'lastname': lastname,
        'address1': address1,
        'address2': address2,
        'city': city,
        'state': state,
        'country': country,
        'zipcode': zipcode,
        'telephone': telephone,
        'user_id': customer
    };
    firstname = $('#billing_first_name').val();
    lastname = $('#billing_last_name').val();
    address1 = $('#billing_address1').val();
    address2 = $('#billing_address2').val();
    city = $('#billing_city').val();
    country = $('#billing_country').val();
    state = $('#billing_state').val();
    zipcode = $('#billing_zipcode').val();
    var billing_address = {
        'address1': address1,
        'address2': address2,
        'city': city,
        'state': state,
        'country': country,
        'zipcode': zipcode,
        'user_id': customer,
        'first_name': firstname,
        'last_name': lastname,
    };
    var belt = $('#belt').is(':checked') ? '1' : '0';
    var data = {
        'billing_address': billing_address,
        'shipping_address': shipping_address,
        'gross_amt': $('#subtotal').val(),
        'user_id': customer,
        'comments': $('#order_comments').val(),
        'order_source': $('#order-source').val(),
        'belt_needed': belt,
    };

    if (!($('#subtotal').val() == 0 || $('.payment-mode:checked').val() == 'cash')) {
        if ($('.card-number').val() == '' || $.formance.creditCardType($('.card-number').val()) == null) {
            alert('Please enter a valid Credit Card Number');
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
            $('#confirm-order').show();
            return false;
        }
        if ($('.card-cvc').val() == '' || !$('.card-cvc').formance('validate_credit_card_cvc')) {
            alert('Please enter a valid Credit Card CVV Number');
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
            $('#confirm-order').show();
            return false;
        }
        Stripe.createToken({number: $('.card-number').val(), cvc: $('.card-cvc').val(), exp_month: $('.card-expiry-month').val(), exp_year: $('.card-expiry-year').val()}, function (status, response) {
            if (response.error) {
                alert(response.error.message);
                $(box).children('.overlay').hide();
                $(box).children('.loading-img').hide();
                $('#confirm-order').show();
                return false;
            } else {
                var token = response['id'];
                data.token = token;
                $.ajax({
                    type: "POST",
                    url: base_url + "orders/confirm_order/",
                    dataType: 'html',
                    data: data,
                    success: function (response) {
                        if (response == '1') {
                            window.location = window.location;
                        } else {
                            alert('Payment Error');
                            $(box).children('.overlay').hide();
                            $(box).children('.loading-img').hide();
                            $('#confirm-order').show();
                        }
                    }
                });
            }
        });
    } else {
        $.ajax({
            type: "POST",
            url: base_url + "orders/confirm_order/",
            dataType: 'html',
            data: data,
            success: function (response) {
                if (response == '1') {
                    window.location = window.location;
                }
            }
        });
    }
}

function save_custom_item() {
    var container = '#' + $(this).parents('.add-edit').attr('id');
    var attachment_ids = $(container).find('.attachment_id').val();
    var price = $(container).find('.u_price').val();
    var style = $(container).find('.u_style').val();
    var last = $(container).find('.u_last').val();
    var customer = $('#customer').val();

    action = (container != '#add-item') ? 'edit' : 'add';

    $.ajax({
        type: "POST",
        url: base_url + "orders/save_custom_item/",
        data: {
            'attachment_ids': attachment_ids,
            'price': price,
            'style_name': style,
            'last_id': last,
            'customer': customer,
            'action': action
        },
        dataType: 'html',
        success: function (response) {
            $(container).find('.item-details').html(response);
            $(container).find('.new-item-images').bxSlider({controls: true, prevText: '<i class="fa fa-arrow-left"></>', nextText: '<i class="fa fa-arrow-right"></>'});
        }
    });
    $(container).find('.order_add_item').css({'margin-left': '-100%'});
}

function populate_address() {
    var customer_id = $(this).val();
    $.ajax({
        type: "POST",
        url: base_url + "orders/shipping_billing_address/" + customer_id,
        dataType: 'json',
        success: function (response) {
            $('#shipping-billing-address .shipping-address').html(response.shipping);
            $('#shipping-billing-address .billing-address').html(response.billing);
        }
    });
}

function on_change_billing() {
    var firstname = $('#billing_first_name').val();
    var lastname = $('#billing_last_name').val();
    var address1 = $('#billing_address1').val();
    var address2 = $('#billing_address2').val();
    var city = $('#billing_city').val();
    var country = $('#billing_country').val();
    var state = $('#billing_state').val();
    var zipcode = $('#billing_zipcode').val();
    $('#order-review .billing-address').html('<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>' + address1 + ', ' + address2 + '</h5><h5>' + city + ', ' + state + ', ' + country + ',' + zipcode + '</h5>');
}

function on_change_shipping() {
    var firstname = $('#shipping_firstname').val();
    var lastname = $('#shipping_lastname').val();
    var address1 = $('#shipping_address1').val();
    var address2 = $('#shipping_address2').val();
    var city = $('#shipping_city').val();
    var country = $('#shipping_country').val();
    var state = $('#shipping_state').val();
    var zipcode = $('#shipping_zipcode').val();
    var telephone = $('#shipping_telephone').val();
    $('#order-review .shipping-address').html('<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>' + address1 + ', ' + address2 + '</h5><h5>' + city + ', ' + state + ', ' + country + ',' + zipcode + '</h5></h5>' + telephone + '</h5>');
}

function show_create_order_modal() {
    $.ajax({
        type: "POST",
        url: base_url + "orders/create_order/",
        dataType: 'html',
        success: function (response) {
            $('#order-create .modal-body').html(response);
            $('#order-create').modal();
            $('.card-number').formance('format_credit_card_number');
            $('.card-cvc').formance('format_credit_card_cvc');
        }
    });

    $.ajax({
        type: "POST",
        url: baseUrl + "common/get_stripe_key/",
        success: function (response) {
            Stripe.setPublishableKey(response);
        }
    });
}

function show_edit_shoe() {
    var id = '#' + $(this).closest('.tab-pane').attr('id');
    $("a[href='" + id + "']").html('Edit ' + id.replace('#', ''));
    $("a[href='" + id + "']").next('i').hide();
    $(id).children('.details').hide();
    $(id).children('.add-edit').show();
    $('.edit-btn').hide();
}

function delete_item() {
    var id = $(this).parent('a').attr('href');
    var tab = $(this).parents('li.items');
    var tabs;
    var tab_pane = $(id);
    var design_id = $(tab_pane).find('.design_id').val();
    var order_id = $('#order_id').text();
    var confir = confirm('Do you really want to delete the item?');
    var box = $(this).parents('.box');

    if (confir) {
        $(box).children('.overlay').show();
        $(box).children('.loading-img').show();
        $.ajax({
            type: "POST",
            url: base_url + "orders/delete_item/",
            dataType: 'html',
            data: {'design_id': design_id, 'order_id': order_id},
            success: function (response) {
                $(tab).remove();
                $(tab_pane).remove();
                tabs = $('#order-details').find('li.items');
                $(tabs).each(function (key) {
                    i = $(this).find('i');
                    $(this).children('a').html('Item ' + (key + 1));
                    $(this).children('a').append(i);
                });
                $('.order-summary').addClass('active');
                $('#order-summary').addClass('active');
                $(box).children('.overlay').hide();
                $(box).children('.loading-img').hide();
            }
        });
    }
}

function show_edit_sizing(e) {
    e.preventDefault();
    $('.edit-btn').hide();
    $(this).closest('.box').find('.editable').show();
    $(this).closest('.box').find('.not-editable').hide();
}

function done_edit_comment(e) {
    e.preventDefault();
    var editable = $(this).closest('.box').find('.editable');
    var not_editable = $(this).closest('.box').find('.not-editable');
    var box = $(this).closest('.box');
    $(box).children('.overlay').show();
    $(box).children('.loading-img').show();
    var order_id = $('#order_id').text();
    var comment = $('#edit-comment textarea').val();

    $.ajax({
        type: "POST",
        url: base_url + "orders/edit_comment/" + order_id,
        data: {'comments': comment},
        dataType: 'html',
        success: function (response) {
            $(not_editable).html(comment);
            $(editable).hide();
            $(not_editable).show();
            $('.edit-btn').show();
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
        }
    });
}

function done_edit_size(e) {
    e.preventDefault();
    var editable = $(this).closest('.box').find('.editable');
    var not_editable = $(this).closest('.box').find('.not-editable');
    var left_shoe = $(editable).find('.left_shoe').val();
    var left_width = $(editable).find('.left_width').val();
    var right_shoe = $(editable).find('.right_shoe').val();
    var right_width = $(editable).find('.right_width').val();
    var m_left_size = $(editable).find('.m_left_size').val();
    var m_left_width = $(editable).find('.m_left_width').val();
    var m_right_size = $(editable).find('.m_right_size').val();
    var m_right_width = $(editable).find('.m_right_width').val();

    var m_lgirth = $(editable).find('.m_lgirth').val();
    var m_linstep = $(editable).find('.m_linstep').val();
    var m_rgirth = $(editable).find('.m_rgirth').val();
    var m_rinstep = $(editable).find('.m_rinstep').val();

    var m_lheel = $(editable).find('.m_lheel').val();
    var m_rheel = $(editable).find('.m_rheel').val();
    var m_lankle = $(editable).find('.m_lankle').val();
    var m_rankle = $(editable).find('.m_rankle').val();

    var sizeType = $(editable).find('.sizeType').val();

    var quantity = $(editable).find('.quantity').val();
    var item_amt = $(editable).find('.item_amt').val();
    var design_id = $(editable).find('.design_id').val();
    var monogram = $(editable).find('.item_monogram').val();
    var order_id = $('#order_id').text();
    var data = {
        'left_shoe': left_shoe,
        'left_width': left_width,
        'right_shoe': right_shoe,
        'right_width': right_width,
        'm_left_size': m_left_size,
        'm_left_width': m_left_width,
        'm_right_size': m_right_size,
        'm_right_width': m_right_width,
        'm_lgirth': m_lgirth,
        'm_linstep': m_linstep,
        'm_rgirth': m_rgirth,
        'm_rinstep': m_rinstep,

        'm_lheel': m_lheel,
        'm_rheel': m_rheel,
        'm_lankle': m_lankle,
        'm_rankle': m_rankle,

        'size_type': sizeType,

        'quantity': quantity,
        'item_amt': item_amt,
        'monogram': monogram
    };

    var box = $(this).closest('.box');
    $(box).children('.overlay').show();
    $(box).children('.loading-img').show();

    $.ajax({
        type: "POST",
        url: base_url + "orders/edit_details/" + design_id + '/' + order_id,
        data: data,
        dataType: 'json',
        success: function (response) {
            var html = '<dt>Price :</dt>' + '<dd>$' + item_amt + '</dd>'
                    + '<dt>Quantity :</dt>' + '<dd>' + quantity + ' PAIR</dd><hr/>'
                    + '<dt>Left shoe size :</dt>' + '<dd>' + left_shoe + ' '
                    + left_width + '</dd>' + '<dt>Right shoe size :</dt>' + '<dd>'
                    + right_shoe + ' ' + right_width + '</dd>'
                    + '<dt>Left shoe measurement :</dt>'
                    + '<dd> Length : ' + m_left_size + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Width : ' + m_left_width + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Girth : ' + m_lgirth + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Instep : ' + m_linstep + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Heel : ' + m_lheel + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Ankle : ' + m_lankle + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dt>Right shoe measurement :</dt>'
                    + '<dd> Length : ' + m_right_size + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Width : ' + m_right_width + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Girth : ' + m_rgirth + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Instep : ' + m_rinstep + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Heel : ' + m_rheel + ' ' + (sizeType.toLowerCase()) + '</dd>'
                    + '<dd> Ankle : ' + m_rankle + ' ' + (sizeType.toLowerCase()) + '</dd>';
            $('#sub-total').text(response.gross_amt);
            $('#discount').text(Math.round(response.gross_amt * response.discount_perc / 100));
            $('#tax').text(Math.round(response.tax_amount));
            var grand_total = parseInt(response.gross_amt) - (response.gross_amt * response.discount_perc / 100) + parseInt((response.tax_amount));
            $('#grand-total').text(grand_total);
            $(editable).find('.design_id').val(response.design_id);
            if (response.monogram !== '') {
                $('.monogram_text').removeClass('hidden');
                $('dd.monogram_text').text(response.monogram + ' On Left Shoe Left Side');
            }
            $(not_editable).html(html);
            $(editable).hide();
            $(not_editable).show();
            $('.edit-btn').show();
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
            update_order_details(order_id);
        }
    });
}

function done_edit_shipping() {
    var order_id = $('#order_id').text();
    var firstname = $('#shipping_firstname').val();
    var lastname = $('#shipping_lastname').val();
    var address1 = $('#shipping_address1').val();
    var address2 = $('#shipping_address2').val();
    var city = $('#shipping_city').val();
    var country = $('#shipping_country').val();
    var state = $('#shipping_state').val();
    var zipcode = $('#shipping_zipcode').val();
    var telephone = $('#shipping_telephone').val();
    var email = $('#shipping_email').val();
    var data = {
        'order_id': order_id,
        'firstname': firstname,
        'lastname': lastname,
        'address1': address1,
        'address2': address2,
        'city': city,
        'state': state,
        'country': country,
        'zipcode': zipcode,
        'telephone': telephone,
        'email': email
    };
    var box = $(this).closest('.box');
    $(box).children('.overlay').show();
    $(box).children('.loading-img').show();
    $.ajax({
        type: "POST",
        url: base_url + "orders/edit_shipping_address",
        data: data,
        dataType: 'html',
        success: function (response) {
            $('#edit-shipping-address').hide();
            $('#shipping-address').html('<h5>' + firstname + ' ' + lastname + '</h5>' + '<h5>' + address1 + ', ' + address2 + '</h5><h5>' + city + ', ' + state + ', ' + country + ',' + zipcode + '</h5><h5>' + telephone + '</h5>' + '<h5>' + email + '</h5>');
            $('#shipping-address').show();
            $('.edit-btn').show();
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
        }
    });
}

function done_edit_status() {
    var order_id = $('#order_id').text();
    var status = $('#status-select :selected').text();
    var invoice = $('#invoice').text();
    var vendor = $('#vendor-select :selected').attr('value');
    var trackingNo = $('#trackingno').val();
    var carrier = $('#carrier').val();
    var shippingDate = $('#shippingDate').val();
    var deliveryDate = $('#deliveryDate').val();
    var refundamount = $('#refundamount').val();
    var refundreason = $('#refundreason').val();

    var data = {
        'order_id': order_id,
        'status': status,
        'vendor': vendor,
        'invoice': invoice,
        'trackingNo': trackingNo,
        'carrier': carrier,
        'shippingDate': shippingDate,
        'deliveryDate': deliveryDate,
        'refundamount': refundamount,
        'refundreason': refundreason,
    };

    var box = $(this).closest('.box');
    $(box).children('.overlay').show();
    $(box).children('.loading-img').show();
    $.ajax({
        type: "POST",
        url: base_url + "orders/vendorAssigning",
        data: data,
        dataType: 'html',
        success: function (response) {
            $('#show-status').hide();
            $('#status').html(status);
            $('#status_desc').html(response);
            if (status != "Manufacturing in progress") {
                $("#show-vendor").hide();
            }
            $('#edit-status').show();
            $('.edit-btn').show();
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
        }
    });
}

function done_edit_user() {
    var order_id = $('#order_id').text();
    var first_name = $('#user-first-name').val();
    var last_name = $('#user-last-name').val();
    var email = $('#user-email').val();

    var box = $(this).closest('.box');
    $(box).children('.overlay').show();
    $(box).children('.loading-img').show();

    $.ajax({
        type: "POST",
        url: base_url + "orders/edit_user",
        data: {'order_id': order_id, 'first_name': first_name, 'last_name': last_name, 'email': email},
        dataType: 'html',
        success: function (response) {
            $('#edit-user-details').hide();
            $('#user-details').html('<dl class="dl-horizontal"><dt>Name :</dt><dd>' + first_name + ' ' + last_name + '</dd><dt>Email :</dt><dd>' + email + '</dd> </dl>');
            $('#user-details').show();
            $('.edit-btn').show();
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
        }
    });
}

function done_edit_billing() {
    var order_id = $('#order_id').text();
    var firstname = $('#billing_first_name').val();
    var lastname = $('#billing_last_name').val();
    var address1 = $('#billing_address1').val();
    var address2 = $('#billing_address2').val();
    var city = $('#billing_city').val();
    var country = $('#billing_country').val();
    var state = $('#billing_state').val();
    var zipcode = $('#billing_zipcode').val();
    var data = {
        'order_id': order_id,
        'address1': address1,
        'address2': address2,
        'city': city,
        'state': state,
        'country': country,
        'zipcode': zipcode,
        'first_name': firstname,
        'last_name': lastname,
    };

    var box = $(this).closest('.box');
    $(box).children('.overlay').show();
    $(box).children('.loading-img').show();

    $.ajax({
        type: "POST",
        url: base_url + "orders/edit_billing_address",
        data: data,
        dataType: 'html',
        success: function (response) {
            $('#edit-billing-address').hide();
            $('#billing-address').html('<h5>' + firstname + ' ' + lastname + '</h5><h5>' + address1 + ', ' + address2 + '</h5><h5>' + city + ', ' + state + ', ' + country + ',' + zipcode + '</h5>');
            $('#billing-address').show();
            $('.edit-btn').show();
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
        }
    });
}

function show_edit_comment(e) {
    e.preventDefault();
    $('.edit-btn').hide();
    $('#edit-comment').show();
    $('#comment').hide();
}

function show_edit_billing(e) {
    e.preventDefault();
    $('.edit-btn').hide();
    $('#edit-billing-address').show();
    $('#billing-address').hide();
}

function show_edit_status(e) {
    e.preventDefault();
    $('.edit-btn').hide();
    $('#vendor').hide();
    $('#show-status').show();
    $('#vendor').hide();
    $('#shipped').hide();
    $('#refund').hide();
    $('#status-select').change(function () {
        var status = $('#status-select :selected').text();

        if (status == 'Order acknowledged') {
            $('#vendor').show();
            $('#shipped').hide();
            $('#refund').hide();
        } else if (status == 'Order Shipped') {
            $('#vendor').hide();
            $('#shipped').show();
            $('#refund').hide();
        } else if (status == 'Refund') {
            $('#vendor').hide();
            $('#shipped').hide();
            $('#refund').show();
        } else if (status == 'Manufacturing in progress') {
            $('#vendor').show();
            $('#shipped').hide();
            $('#refund').hide();
        } else {
            $('#vendor').hide();
            $('#shipped').hide();
            $('#refund').hide();
        }
    });
    $('#edit-status').hide();
}

function show_edit_user_details(e) {
    e.preventDefault();
    $('.edit-btn').hide();
    $('#edit-user-details').show();
    $('#user-details').hide();
}

function show_edit_shipping(e) {
    e.preventDefault();
    $('.edit-btn').hide();
    $('#edit-shipping-address').show();
    $('#shipping-address').hide();
}

function order_add_item(e) {
    var container = '#' + $(this).parents('.add-edit').attr('id');
    e.preventDefault();
    self = $(this);
    $(this).hide();
    var data = {
        'order_id': $('#order_id').text(),
        'customer': $('#customer').val(),
        'design_id': $(container).find('.new-design_id').val(),
        'custom_collection_id': $(container).find('.new-custom_collection_id').val(),
        'is_readywear': $(container).find('.is_readywear').val(),
        'item_amt': $(container).find('.new-price').text(),
        'left_shoe': $(container).find('.left_shoe').val(),
        'right_shoe': $(container).find('.right_shoe').val(),
        'left_width': $(container).find('.left_width').val(),
        'right_width': $(container).find('.right_width').val(),
        'm_left_size': $(container).find('.m_left_size').val(),
        'm_right_size': $(container).find('.m_right_size').val(),
        'm_left_width': $(container).find('.m_left_width').val(),
        'm_right_width': $(container).find('.m_right_width').val(),
        'm_lgirth': $(container).find('.m_lgirth').val(),
        'm_linstep': $(container).find('.m_linstep').val(),
        'm_rgirth': $(container).find('.m_rgirth').val(),
        'm_rinstep': $(container).find('.m_rinstep').val(),
        'quantity': $(container).find('.new-quantity').val(),
        'monogram': $(container).find('.new-monogram').val(),
    };

    if (container != '#add-item') {
        var tab_pane = $(this).closest('.tab-pane');
        design_id = $(tab_pane).find('.design_id').val();
        data.old_design_id = design_id;
        data.action = 'edit';
    } else {
        data.action = 'add';
    }

    $.ajax({
        type: "POST",
        url: base_url + "orders/order_add_item",
        data: data,
        dataType: 'html',
        success: function (response) {
            if (data.action == 'add') {
                $(response).insertBefore("#add-item");
                var cnt = $('li.items').length + 1;
                $('.nav-tabs li').removeClass('active');
                $('<li class="items active"><a href="#item-' + cnt + '" data-toggle="tab">Item '
                        + cnt + '<i title="Delete Item" class="delete-item fa fa-times pull-right"></i></a></li>')
                        .insertBefore("#add-item-btn");
                $("#add-item").removeClass('active');
                $("#item-" + cnt).addClass('active');
                setTimeout(function () {
                    $("#item-" + cnt).find('.order_images').bxSlider({infiniteLoop: true, preloadImages: 'all', controls: true, prevText: '<i class="fa fa-arrow-left"></>', nextText: '<i class="fa fa-arrow-right"></>'});
                }, 100);
            } else {
                $(tab_pane).html($(response).html());
                var id = $(tab_pane).attr('id');
                $("a[href='#" + id + "']").html(capitalize(id));
                $("a[href='#" + id + "']").next('i').show();
                setTimeout(function () {
                    $(tab_pane).find('.order_images').bxSlider({infiniteLoop: true, preloadImages: 'all', controls: true, prevText: '<i class="fa fa-arrow-left"></>', nextText: '<i class="fa fa-arrow-right"></>'});
                }, 100);
            }
            self.show();
            back(container);
            update_order_details(data.order_id);
            $('.show-selected').val('');
            $('.attachment_id').val('');
            $('.qq-upload-list').html('');
            $('.collection').html('');
        }
    });
    return false;
}

function update_order_details(order_id) {
    $.ajax({
        type: "POST",
        url: base_url + "orders/update_order_details",
        data: {'order_id': order_id},
        dataType: 'json',
        success: function (response) {
            if (response.status == 'success') {
                $('#subtotal').val(response.subtotal);
                $('#number_of_items').val(response.number_of_items);
            }
        }
    });
}

function done_edit_item() {
}

function proceed(e) {
    e.preventDefault();

    var container = '#' + $(this).parents('.add-edit').attr('id');
    var selected = $(container).find('.show-selected').val();
    var data;
    var order_id = $('#order_id').text();
    var shoe_design_id = $(this).parent('.shoe-details').find('.shoe_design_id').val();
    var price = $(this).parent('.shoe-details').find('.price').text();

    if (selected == 'custom_collection') {
        custom_collection_id = $(this).parent('.shoe-details').find('.custom_collection_id').val()
        data = {
            'shoe_design_id': shoe_design_id,
            'custom_collection_id': custom_collection_id,
            'source': 'custom_collection',
            'price': price,
            'order_id': order_id
        };
    } else if (selected == 'ready_wear') {
        custom_collection_id = $(this).parent('.shoe-details').find('.custom_collection_id').val()
        data = {
            'shoe_design_id': shoe_design_id,
            'custom_collection_id': custom_collection_id,
            'source': 'ready_wear',
            'price': price,
            'order_id': order_id
        };
    } else {
        data = {
            'shoe_design_id': shoe_design_id,
            'source': 'saved_shoes',
            'price': price,
            'order_id': order_id
        };
    }

    data.action = 'add';
    if (container != '#add-item') {
        design_id = $(this).closest('.tab-pane').find('.design_id').val();
        data.design_id = design_id;
        data.action = 'edit';
    }

    $.ajax({
        type: "POST",
        url: base_url + "orders/get_item_details",
        data: data,
        dataType: 'html',
        success: function (response) {
            $(container).find('.item-details').html(response);
            $(container).find('.new-item-images').bxSlider({controls: true, prevText: '<i class="fa fa-arrow-left"></>', nextText: '<i class="fa fa-arrow-right"></>'});
        }
    });

    $(container).find('.order_add_item').css({'margin-left': '-100%'});
}

function back(container) {
    if (typeof container == 'object') {
        container = '#' + $(this).parents('.add-edit').attr('id');
    }
    $(container).find('.item-details').html('');
    $(container).find('.order_add_item').css({'margin-left': '0%'});
}

function show_selected() {
    var container = '#' + $(this).parents('.add-edit').attr('id');
    $(this).parents('.box-body').find('.group').hide();
    $(this).parents('.box-body').find('.' + $(this).val()).show();
    show_shoes(container);
}

function get_types() {
    var container = '#' + $(this).parents('.add-edit').attr('id');
    var style = $(this).val();
    $.ajax({
        type: "POST",
        url: base_url + "orders/get_types",
        data: {'style': style},
        dataType: 'html',
        success: function (response) {
            $(container).find('.type').html(response);
            show_shoes(container);
        }
    });
}

function show_shoes(container) {
    if (typeof container == 'object') {
        container = '#' + $(this).parents('.add-edit').attr('id');
    }
    var selected = $(container).find('.show-selected').val();
    var data;
    if (selected == 'custom_collection') {
        var style = $(container).find('.style').val();
        var type = $(container).find('.type').val();
        data = {'style': style, 'type': type, 'source': 'custom_collection'};
    } else if (selected == 'ready_wear') {
        var style = $(container).find('.rtw_style').val();
        data = {'style': style, 'source': 'ready_wear'};
    } else if (selected == 'saved_shoes') {
        var user_id = $(container).find('.user_list').val();
        data = {'user_id': user_id, 'source': 'saved_shoes'};
    }

    if (selected !== 'upload_images') {
        $.ajax({
            type: "POST",
            url: base_url + "orders/get_collection",
            data: data,
            dataType: 'html',
            success: function (response) {
                $(container).find('.collection').html(response);
                setTimeout(function () {
                    $('.shoe_images').bxSlider({infiniteLoop: true, preloadImages: 'all', controls: true, prevText: '<i class="fa fa-arrow-left"></>', nextText: '<i class="fa fa-arrow-right"></>'});
                }, 1);
            }
        });
    } else {
        $(container).find('.collection').html('');
        $('.attachment_id').val('');
        $('.qq-upload-list').html('');
    }
}

function show_order_details() {
    var order_id = $(this).find('.order_id').val();
    var box = $(this).parents('.box');
    $(box).children('.overlay').show();
    $(box).children('.loading-img').show();

    $.ajax({
        type: "POST",
        url: base_url + "orders/order_details",
        data: {order_id: order_id},
        dataType: 'html',
        success: function (response) {
            $('#order-details .modal-body').html(response);
            $('#order-details').modal('show', {backdrop: 'static'});
            $(box).children('.overlay').hide();
            $(box).children('.loading-img').hide();
            setTimeout(function () {
                $('.order_images').bxSlider({infiniteLoop: true, preloadImages: 'all', controls: true, prevText: '<i class="fa fa-arrow-left"></>', nextText: '<i class="fa fa-arrow-right"></>'});
            }, 100);
        }
    });
}

function revoke_shoe_edit() {
    $('.btn-revoke-shoe').hide();
    $('.edit-btn').hide();
    $('.revokeshoe .editable').show();
}

function revoke_shoe_done() {
    var order_id = $('.order_id').val();
    var invoice_no = $('.invoice_no').val();
    var customer_id = $('.customer_id').val();
    var vendor_id = $('.vendor_id').val();
    var comment = $('.comment').val();
    var design_id = $('.design_id').val();
    $.ajax({
        type: "POST",
        url: base_url + "orders/remake_shoe",
        data: {order_id: order_id, invoice_no: invoice_no, customer_id: customer_id, comment: comment, design_id: design_id, vendor_id: vendor_id},
        dataType: 'html',
        success: function (response) {
            alert("Order Remake Request Sent Successfully");
        }
    });

    $('.edit-btn').show();
    $('.btn-revoke-shoe').hide();
    $('.revokeshoe .editable').hide();
    $('.revokeshoe').hide();
}

$(function () {
    $('#orders').dataTable({
        "iDisplayLength": 50,
        "bInfo": true,
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "bAutoWidth": true,
        "aaSorting": [[0, "desc"]],
        "aoColumnDefs": [{"sType": "numeric"}]
    });
});

$.extend($.fn.dataTableExt.oSort, {
    "num-html-pre": function (a) {
        var x = String(a).replace(/<[\s\S]*?>/g, "");
        return parseFloat(x);
    },
    "num-html-asc": function (a, b) {
        return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
    "num-html-desc": function (a, b) {
        return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
});

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}