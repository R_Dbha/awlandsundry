var slider_loaded = false;
var selectedSize = "";
var oldSelection = null;

$(window).load(function () {
    $(".ready_wear_details").on("click", "#add-readywear-item-to-cart", addCustomItemToCart);
    $(".ready_wear_details").on("click", ".size_options_selector", sizeClickListener);

    $('.overlay, .close').on('click', function () {
        $('body').css({'overflow': 'auto', 'position': 'static'});
        $('#email-popup-wrapper').hide();
        $('#sizing-popup-wrapper').hide();
        $('#measure-popup-wrapper').hide();
        $('.overlay').hide();
    });

    $('#email-popup-wrapper input, #email-popup-wrapper textarea').on('keydown', function () {
        $(this).next('span').text('').hide();
    });

    var i = 0;
    var imageLoaded = function () {
        i++;
        if (i === (5)) {
            $('#loader.spinner').hide();
            $('#details_section').show();
        }
    };

    $(".ready_prod_image_container img").each(function () {
        var tmpImg = new Image();
        tmpImg.onload = imageLoaded;
        tmpImg.src = $(this).attr('src');
    });

    dataLayer.push({'ecommerce': {'detail': {'actionField': {'list': 'Ready To Wear Shoes'}, 'products': [{'name': $('.details_child h1').text(), 'price': '195', 'brand': 'Awl & Sundry', 'category': 'Ready To Wear Shoes', 'variant': $('p.product-color').text()}]}}});
});

$(document).ready(function () {
    $(document).ready(function () {
        $('#nav').onePageNav({
            currentClass: 'current',
            changeHash: false,
            scrollSpeed: 750,
            scrollThreshold: 0.2,
            filter: '',
            easing: 'swing',
            begin: function () {},
            end: function () {},
            scrollChange: function ($currentListItem) {}
        });
    });

    $('.owl-carousel').owlCarousel({
        loop: true, margin: 10, responsiveClass: true,
        responsive: {
            0: {items: 1, nav: false, animateIn: true},
            768: {items: 1, nav: false},
            992: {items: 4, nav: true, loop: false, margin: 20}
        }
    });
});

$(window).scroll(function () {
    if ($('.ready_image_section').css('display') != 'none') {
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            $(".image_thumbs_fixed, .details_fixed").addClass("stickybox");
        } else {
            $(".image_thumbs_fixed, .details_fixed").removeClass("stickybox");
        }

        if ($('.product-gallery__thumb.last').hasClass('current')) {
            $(".details_fixed").addClass('stickybox1');
        } else {
            $(".details_fixed").removeClass('stickybox1');
        }
    }
});

function PopupCenter(pageURL, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}
function send_email() {
    var table = $(this).parents('table');
    var from_email = $(table).find('.from_email').val();
    var to_emails = $(table).find('.to_emails').val();
    var message = $(table).find('.message').val();
    var shoe_id = $(table).find('.shoe_id').val();
    var type = $(table).find('.type').val();
    var image = $(table).find('.image').val();
    var url = window.location.href;

    $.ajax({
        type: "POST",
        url: baseUrl + "common/send_email",
        data: {from_email: from_email, to_emails: to_emails, message: message, shoe_id: shoe_id, type: type, image: image, url: url},
        dataType: 'json',
        success: function (response) {
            if (response.status === "success") {
                $('#share_email_success').show();
                setTimeout(function () {
                    $('#email-popup-wrapper').hide();
                    $('.overlay').hide();
                }, 1000);
            } else {
                for (var key in response.errors) {
                    if (response.errors.hasOwnProperty(key)) {
                        var val = response.errors[key];
                        $(table).find('.' + key).next('span').text(val).show();
                    }
                }
            }
        }
    });
}

function show_email_popup() {
    $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
    $('#email-popup-wrapper').find('input[type=text],input[type=email]').val('');
    $('#email-popup-wrapper').find('textarea').val('');
    $('#email-popup-wrapper').find('span').text('');
    $('#sizing-popup-wrapper').hide();
    $('#width-popup-wrapper').hide();
    $('#email-popup-wrapper').show();
    $('.overlay').show();
}

function addToCart() {
    var left_size = $("#select_left_size").val();
    var right_size = $("#select_right_size").val();
    var right_width = $("#right_width").val();
    var left_width = $("#left_width").val();
    if (left_size.trim() === "" || right_size.trim() === "" || right_width.trim() === "" || left_width.trim() === "") {
        alert("Please specify your shoe size");
        return false;
    }

    var shoe_design_id = $("#shoe_design_id").val();
    $.ajax({
        type: "POST",
        url: baseUrl + "shoecart/addtocart/" + shoe_design_id,
        data: {left_size: left_size, right_size: right_size, right_width: right_width, left_width: left_width, item_id: custom_collection_id},
        success: function (msg) {
            fbq('track', 'AddToCart');
            window.location.href = baseUrl + "cart";
        }
    });
}

function sizeClickListener() {
    if (oldSelection != null) {
        oldSelection.removeClass('size_options_selector--selected');
    }
    $(this).addClass('size_options_selector--selected');
    selectedSize = $(this).children().text();
    oldSelection = $(this);
}

function addCustomItemToCart() {
    var left_size = selectedSize;
    var right_size = selectedSize;

    if (left_size.trim() === "" || right_size.trim() === "") {
        alert("Please specify your shoe size");
        return false;
    }

    var ready_wear_id = $("#ready_wear_id").val();
    var detailsChild = $(".details_child h1").text();

    $.ajax({
        type: "POST",
        url: baseUrl + "shoecart/add_readywear_item_to_cart",
        data: {left_size: left_size, right_size: right_size, right_width: "D", left_width: "D", item_id: ready_wear_id},
        success: function (msg) {
            fbq('track', 'AddToCart');
            dataLayer.push({'event': 'addToCart', 'ecommerce': {'currencyCode': 'USD', 'add': {'products': [{'name': detailsChild, 'price': '195', 'brand': 'Awl & Sundry', 'category': 'Ready To Wear Shoes', 'variant': $(".product-color").text(), 'quantity': 1}]}}});
            window.location.href = baseUrl + "cart";
        }
    });
}

function setHeights() {
    var i = 0;
    $('.ready_image_section ul li').siblings().each(function () {
        var p = $(this).find(".ready_prod_image").height();
        imgHeights[i++] = p;
    });

    for (var i = 0; i < imgHeights.length; i++) {
        alert(imgHeights[i]);
    }
}