$(document).ready(function () {
    if (isTouch) {
        if (window.innerWidth <= 767) {
            $('.mobile_share > a:last-of-type').click(function () {
                $('.mobile_share #share_buttons').css({'height': '160px'});
            });
            $('body').on('click', function (e) {
                if (e.target.id == 'share2' || e.target.className == 'not-ready' || e.target.className == 'ready') {
                } else {
                    $('.mobile_share #share_buttons').css({'height': '0'});
                }
            });
        }
    }


    $('.not-ready').click(save);
    $('.close-button , .overlay1').on('click', function () {
        $('#login_modal').hide();
        $('#overlay1').hide();
        $('#signup_modal').hide();
    });

    $('#register').on('click', function () {
        $('#login_modal').hide();
        $('#signup_modal').show();
    });

    $('#back').on('click', function () {
        $('#login_modal').show();
        $('#signup_modal').hide();
    });

    $('#popup_login').click(login);
    $('#btnRegister').click(registration);
});

function PopupCenter(pageURL, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function save() {
    var id = $(this).attr('id');
    CustomShoe.saveModel(function () {
        $.ajax({
            type: "POST",
            url: base_url + 'customshoe/saveDesign',
            data: {'shoemodel': ShoeModel},
            dataType: 'json',
            success: function (response) {
                $('#share_buttons a').attr('data-id', response.shoe.shoeDesignId);
                $('#share_buttons a').attr('data-image', response.details.image);
                share(id);
            }
        });
    }, 'Yes');
}

function share(e) {
    var element;
    if (typeof e === 'string') {
        element = $('#' + e);
    } else {
        element = $(this);
    }

    var design_id = $(element).attr('data-id');
    var image = $(element).attr('data-image');
    var id = $(element).attr('id');
    var links = {'facebook': 'http://www.facebook.com/sharer.php?u=' + baseUrl + 'common/shoe_design/' + design_id,
        'twitter': 'http://twitter.com/share?url=' + baseUrl + 'common/shoe_design/' + design_id + '&text=Checkout this custom pair of shoes I just designed at Awlandsundry.com',
        'pinterest': 'http://www.pinterest.com/pin/create/button/?url=' + baseUrl + 'common/shoe_design/' + design_id + '&description=Checkout this custom pair of shoes I just designed at Awlandsundry.com&media=' + baseUrl + 'files/designs/' + image + '_A0.png',
        'google': 'https://plus.google.com/share?url=' + baseUrl + 'common/shoe_design/' + design_id,
        'tumblr': 'http://tumblr.com/share?s=&v=3&t=Checkout this custom pair of shoes I just designed at Awlandsundry.com&u=' + baseUrl + 'common/shoe_design/' + design_id,
    };

    $('.mobile_share #share_buttons').css({'height': '0'});
    PopupCenter(links[id], 'myPop1', 500, 500);
}

$(window).resize(function () {
    ModelRenderer.resetContainerHeight();
});

$(window).load(function () {
    ModelRenderer.resetContainerHeight();
});

function login() {
    var email = $('#l_email').val();
    var password = $('#l_password').val();
    $.ajax({
        type: "POST",
        url: base_url + 'common/login_popup',
        data: {'email': email, 'password': password},
        dataType: 'json',
        success: function (data) {
            if (data.status) {
                alert(data.msg);
                $('#login_modal').hide();
                CustomShoe.saveDesign();
                $('#replace').remove();
                $('.secondary-links').prepend(data.html);
            } else {
                $('#errors').text(data.msg);
            }
        }
    });
}

function registration() {
    var fname = $('#fname').val();
    var lname = $('#lname').val();
    var email = $('#s_email').val();
    var password = $('#s_password').val();
    $.ajax({
        type: "POST",
        url: base_url + 'common/signup_popup',
        data: {'fname': fname, 'lname': lname, 'email': email, 'password': password},
        dataType: 'json',
        success: function (data) {
            fbq('track', 'CompleteRegistration');

            if (data.status) {
                alert(data.msg);
                $('#signup_modal').hide();
                $('#replace').remove();
                $('.secondary-links').prepend(data.html);
                CustomShoe.saveDesign();
            } else {
                $('#error_msg').text(data.msg);
                $('#fnameerror').text(data.validation.errors.fname);
                $('#lnameerror').text(data.validation.errors.lname);
                $('#emailerror').text(data.validation.errors.email);
                $('#passworderror').text(data.validation.errors.password);
            }

        }
    });
}

function fb_popup($fb_url) {
    window.open($fb_url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}