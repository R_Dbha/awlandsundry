/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {
    $('#places').dataTable({
        "bInfo": true,
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "bAutoWidth": true,
    });
    $('#users').dataTable({
        "bInfo": true,
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": true,
        "bAutoWidth": true,
    });
});
$(document).ready(function () {
    $('.get-coordinates').on('click', get_coordinates);
});
function get_coordinates() {
    var district_name;
    if ($('#district_name option:selected').val() === '0') {
        district_name = '';
    } else {
        district_name = $('#district_name option:selected').text();
    }
    show_loading();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: base_url + "admin/get_coordinates/",
        data: {
            'place_name': $('#place_name').val(),
            'district_name': district_name,
        },
        success: function (response) {
            if (response.status === "success") {
                $('#latitude').val(response.latitude);
                $('#longitude').val(response.longitude);
            } else {
                alert('zero results');
            }
            hide_loading();
        }
    });
}

function show_loading() {

}
function hide_loading() {

}