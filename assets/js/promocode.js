$(function () {
    var dataTable = $('#promocodes').DataTable({
        "processing": false,
        "serverSide": true,
        "ajax": base_url + 'promocode/json_promocode',
        "columns": [
            {"data": 'promocode'},
            {"data": 'discount_per'},
            {"data": 'discount_amount'},
            {"data": 'expiry_date'},
            {"data": 'is_common'},
            {"data": 'assigned_users'},
            {"data": 'used_users'},
        ]

    });
});

$(document).ready(function () {
    $('body').on('click', '#create-promocode', function () {
        $.ajax({
            type: "POST",
            url: base_url + "promocode/create/",
            dataType: 'html',
            success: function (response) {
                $('#promocode .modal-content').html(response);
                $('#promocode').modal();
                change_discount_criteria();
                change_common();
            },
        });
    });
    $('body').on('change', '.discount_criteria', change_discount_criteria);
    $('body').on('change', '.is_common', change_common);
    $('body').on('click', '#save_promocode', save_promocode);
});
function change_common() {
    if ($('.is_common').is(':checked')) {
        $('.one_time').show();
    } else {
        $('.one_time').hide();
    }
}
function change_discount_criteria() {
    if ($('.discount_criteria').val() == 'percentage') {
        $('.discount_amount').hide();
        $('.discount_per').show();
    } else {
        $('.discount_amount').show();
        $('.discount_per').hide();
    }
}
function save_promocode() {
    var data = $('#promocode-form').serialize();
    $.ajax({
        type: "POST",
        url: base_url + "promocode/save/",
        dataType: 'json',
        data: data,
        success: function (response) {

        },
    });
}
