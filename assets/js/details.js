
var slider_loaded = false;

$(window).load(function () {
    $("#custom_fitting_checkbox").change(customFittingClick);
    $(".details").on("click", '#add-to-cart', addToCart);
    $(".details").on("click", '#add-custom-item-to-cart', addCustomItemToCart);
    $(".details").on("click", '#edit-costom-item', editCustomItem);
    $(".details").on("click", '#edit-shoedesign', editShoeDesign);
    $('#get-inspired-social .email').on('click', show_email_popup);
    $('#email-popup-wrapper #send-email').on('click', send_email);

    $('.overlay, .close').on('click', function () {
        $('body').css({'overflow': 'auto', 'position': 'static'});
        $('#email-popup-wrapper').hide();
        $('#width-popup-wrapper').hide();
        $('#sizing-popup-wrapper').hide();
        $('#measure-popup-wrapper').hide();
        $('.overlay').hide();
    });

    $('#email-popup-wrapper input, #email-popup-wrapper textarea').on('keydown', function () {
        $(this).next('span').text('').hide();
    });

    $(function () {
        $("#left_width").SumoSelect();
        $("#right_width").SumoSelect();
        $("#select_right_size").SumoSelect();
        $("#select_left_size").SumoSelect();
    });

    slider = $('.get_inspired_slider').bxSlider({
        useCSS: false,
        auto: false,
        pager: false,
        minSlides: 1,
        maxSlides: 1,
        slideWidth: 1000,
        infiniteLoop: true,
        hideControlOnEnd: false,
        preloadImages: 'all',
        onSliderLoad: function () {
            if (!slider_loaded) {
                slider_loaded = true;
                $("#loader").fadeOut(100, function () {
                    $('#get_inspired').fadeIn(300);
                    slider.reloadSlider();
                });
            }
            $('#get_inspired-wrapper .spinner').hide();
            $('#get_inspired').show();
        }
    });
    if (isTouch) {
        if (window.innerWidth < 767) {
            slider.reloadSlider({minSlides: 1, maxSlides: 1, controls: false, pager: true});
        }
    }
    $('#s_w_det').click(function () {
        $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
        $('#email-popup-wrapper').hide();
        $('#sizing-popup-wrapper').hide();
        $('#width-popup-wrapper').show();
        $('.overlay').show();
    });

    $('#s_s_det').click(function () {
        $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
        $('#email-popup-wrapper').hide();
        $('#width-popup-wrapper').hide();
        $('#sizing-popup-wrapper').show();
        $('.overlay').show();
    });

    $('#enter-size .help_button').click(function () {
        $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
        $('.overlay').show();
        $('#measure-popup-wrapper').show();
    });

    $('#measure-info ul').bxSlider({
        pager: false,
        auto: false,
        hideControlOnEnd: true,
        ticker: false,
        responsive: true,
        touchEnabled: true,
        infiniteLoop: false
    });
});

function PopupCenter(pageURL, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var targetWin = window.open(pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function send_email() {
    var table = $(this).parents('table');
    var from_email = $(table).find('.from_email').val();
    var to_emails = $(table).find('.to_emails').val();
    var message = $(table).find('.message').val();
    var shoe_id = $(table).find('.shoe_id').val();
    var type = $(table).find('.type').val();
    var image = $(table).find('.image').val();
    var url = window.location.href;

    $.ajax({
        type: "POST",
        url: baseUrl + "common/send_email",
        data: {from_email: from_email, to_emails: to_emails, message: message, shoe_id: shoe_id, type: type, image: image, url: url},
        dataType: 'json',
        success: function (response) {
            if (response.status === "success") {
                $('#share_email_success').show();
                setTimeout(function () {
                    $('#email-popup-wrapper').hide();
                    $('.overlay').hide();
                }, 1000);
            } else {
                for (var key in response.errors) {
                    if (response.errors.hasOwnProperty(key)) {
                        var val = response.errors[key];
                        $(table).find('.' + key).next('span').text(val).show();
                    }
                }
            }

        }
    });
}

var customFittingClick = function () {
    // console.log("check box " + $("#custom_fitting_checkbox").is(":checked"));
    $("#enter-size").css("display", (this.checked) ? "block" : "none");
};

function show_email_popup() {
    $('body').css({'overflow-y': 'scroll', 'position': 'fixed', 'width': '100%'});
    $('#email-popup-wrapper').find('input[type=text],input[type=email]').val('');
    $('#email-popup-wrapper').find('textarea').val('');
    $('#email-popup-wrapper').find('span').text('');
    $('#sizing-popup-wrapper').hide();
    $('#width-popup-wrapper').hide();
    $('#email-popup-wrapper').show();
    $('.overlay').show();
}

function addToCart() {
    var left_size = $("#select_left_size").val();
    var right_size = $("#select_right_size").val();
    var right_width = $("#right_width").val();
    var left_width = $("#left_width").val();
    if (left_size.trim() === "" || right_size.trim() === "" || right_width.trim() === "" || left_width.trim() === "") {
        alert("Please specify your shoe size");
        return false;
    }

    var shoe_design_id = $("#shoe_design_id").val();
    $.ajax({
        type: "POST",
        url: baseUrl + "shoecart/addtocart/" + shoe_design_id,
        data: {left_size: left_size, right_size: right_size, right_width: right_width, left_width: left_width},
        success: function (msg) {
            fbq('track', 'AddToCart');
            window.location.href = baseUrl + "cart";
        }
    });
}

function editCustomItem() {
    var custom_collection_id = $("#custom_collection_id").val();
    $.ajax({
        type: "POST",
        url: baseUrl + "shoecart/edit_custom_item",
        data: {item_id: custom_collection_id},
        success: function (msg) {
            window.location.href = baseUrl + "create-a-custom-shoe/design-features";
        }
    });
}

function editShoeDesign() {
    var shoe_design_id = $("#shoe_design_id").val();
    $.ajax({
        type: "POST",
        url: baseUrl + "shoecart/rework_design",
        data: {item_id: shoe_design_id},
        success: function (msg) {
            window.location.href = baseUrl + "create-a-custom-shoe/design-features";
        }
    });
}

function addCustomItemToCart() {
    var left_size = $("#select_left_size").val();
    var right_size = $("#select_right_size").val();
    var right_width = $("#right_width").val();
    var left_width = $("#left_width").val();

    var m_lsize = $("#m_size_left").val();
    var m_rsize = $("#m_size_right").val();
    var m_lwidth = $("#m_width_left").val();
    var m_rwidth = $("#m_width_right").val();

    var m_lgirth = $("#m_girth_left").val();
    var m_rgirth = $("#m_girth_right").val();
    var m_linstep = $("#m_instep_left").val();
    var m_rinstep = $("#m_instep_right").val();

    if (left_size.trim() === "" || right_size.trim() === "" || right_width.trim() === "" || left_width.trim() === "") {
        alert("Please specify your shoe size");
        return false;
    }

    var custom_collection_id = $("#custom_collection_id").val();
    $.ajax({
        type: "POST",
        url: baseUrl + "shoecart/add_custom_item_to_cart",
        data: {
            left_size: left_size,
            right_size: right_size,
            right_width: right_width,
            left_width: left_width,
            m_left_size: m_lsize,
            m_right_size: m_rsize,
            m_right_width: m_rwidth,
            m_left_width: m_lwidth,
            m_lgirth: m_lgirth,
            m_rgirth: m_rgirth,
            m_linstep: m_linstep,
            m_rinstep: m_rinstep,
            item_id: custom_collection_id
        },
        success: function (msg) {
            fbq('track', 'AddToCart');
            window.location.href = baseUrl + "cart";
        }
    });
}

$("#select_left_size").on('change', function () {
    if ($("#select_right_size").val() == "") {
        $('#select_right_size').parent().children(".SlectBox").children('span').text($("#select_left_size").val())
        $("#select_right_size").val($("#select_left_size").val());
        if ($("#left_width").val() == "") {
            $("#left_width").val("D");
            $('#left_width').parent().children(".SlectBox").children('span').text("D");
        }

        if ($("#right_width").val() == "") {
            $("#right_width").val("D");
            $('#right_width').parent().children(".SlectBox").children('span').text("D");
        }
    }
    if (checkSizes() && !($("#custom_fitting_checkbox").is(":checked"))) {
        $("#custom_fitting_checkbox").prop("checked", true);
        $("#enter-size").css("display", "block");
    } else if (!checkSizes() && ($("#custom_fitting_checkbox").is(":checked"))) {
        $("#custom_fitting_checkbox").prop("checked", false);
        $("#enter-size").css("display", "none");
    }
});

$("#select_right_size").on('change', function () {
    if ($("#select_left_size").val() == "") {
        $('#select_left_size').parent().children(".SlectBox").children('span').text($("#select_right_size").val())
        $("#select_left_size").val($("#select_right_size").val());
        if ($("#left_width").val() == "") {
            $("#left_width").val("D");
            $('#left_width').parent().children(".SlectBox").children('span').text("D");
        }

        if ($("#right_width").val() == "") {
            $("#right_width").val("D");
            $('#right_width').parent().children(".SlectBox").children('span').text("D");
        }
    }

    if (checkSizes() && !($("#custom_fitting_checkbox").is(":checked"))) {
        $("#custom_fitting_checkbox").prop("checked", true);
        $("#enter-size").css("display", "block");
    } else if (!checkSizes() && ($("#custom_fitting_checkbox").is(":checked"))) {
        $("#custom_fitting_checkbox").prop("checked", false);
        $("#enter-size").css("display", "none");
    }
});

$("#left_width").on('change', function () {
    if ($("#left_width").val() != "") {
        $('#left_width').parent().children(".SlectBox").children('span').text($("#left_width").val());
    }
    if (checkSizes() && !($("#custom_fitting_checkbox").is(":checked"))) {
        $("#custom_fitting_checkbox").prop("checked", true);
        $("#enter-size").css("display", "block");
    } else if (!checkSizes() && ($("#custom_fitting_checkbox").is(":checked"))) {
        $("#custom_fitting_checkbox").prop("checked", false);
        $("#enter-size").css("display", "none");
    }
});

$("#right_width").on('change', function () {
    if ($("#right_width").val() != "") {
        $('#right_width').parent().children(".SlectBox").children('span').text($("#right_width").val());
    }
    if (checkSizes() && !($("#custom_fitting_checkbox").is(":checked"))) {
        $("#custom_fitting_checkbox").prop("checked", true);
        $("#enter-size").css("display", "block");
    } else if (!checkSizes() && ($("#custom_fitting_checkbox").is(":checked"))) {
        $("#custom_fitting_checkbox").prop("checked", false);
        $("#enter-size").css("display", "none");
    }
});

var checkSizes = function () {
    var temp_ls = $("#select_left_size").val();
    var temp_rs = $("#select_right_size").val();
    var temp_rw = $("#right_width").val();
    var temp_lw = $("#left_width").val();
    var result = false;
    if (temp_ls != "" && ((temp_ls < 8) || (temp_ls > 12.5))) {
        result = true;
    }
    if (temp_rs != "" && ((temp_rs < 8) || (temp_rs > 12.5))) {
        result = true;
    }
    if (temp_lw != "" && temp_lw != "D") {
        result = true;
    }
    if (temp_rw != "" && temp_rw != "D") {
        result = true;
    }
    return result;
};