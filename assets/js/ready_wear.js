$(document).ready(function () {
    var i = 0;
    var imageLoaded = function () {
        i++;
        if (i === 2) {
            $('#loader.spinner').hide();
            $('#product-list').show();
        }
    };

    $(".product-img-container img").each(function () {
        var tmpImg = new Image();
        tmpImg.onload = imageLoaded;
        tmpImg.src = $(this).attr('src');
    });

    $(".custom-color").click(function (e) {
        var color = $(this).find('p.color-dot-text').text();
        var shoe_name = $(this).attr('id');
        var product_link = $(this).parents('.product-box').find(".product-img-container a");
        var product_imgs = $(this).parents('.product-box').find(".product-img-container img");
        var name = $(this).parents('.product-box').find(".product-details-container a p.product-color");
        shoe_name = shoe_name.substr(0, shoe_name.indexOf('-color'));

        $.ajax({
            type: "POST",
            url: baseUrl + "ready_wear/get_shoe_source",
            data: {shoe_name: shoe_name, shoe_color: color},
            success: function (response) {
                if (response == "") {
                    return false;
                }

                var images = $.parseJSON(response);
                name.text(color);
                $(product_link).attr("href", baseUrl + 'shop/products/' + color + '/' + shoe_name);
                $(product_imgs[0]).attr("src", images[0]);
                $(product_imgs[1]).attr("src", images[1]);
            }
        });
    });

    $(document).on('click', '.readywear_banner', function (event) {
        event.preventDefault();
        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({'scrollTop': $target.offset().top - 200}, 900, 'swing');
    });

    $(".size-group-li").click(function (e) {
        e.preventDefault();
        
        selectedSize = $(this).text();
        var ready_wear_id = $(this).parents('.product-box').find("#ready_wear_id").val();
        var shoeName = $(this).parents('.product-box').find(".product-name").text();
        
        $.ajax({
            type: "POST",
            url: baseUrl + "shoecart/add_readywear_item_to_cart",
            data: {left_size: selectedSize, right_size: selectedSize, right_width: "D", left_width: "D", item_id: ready_wear_id},
            success: function (msg) {
                //alert("Product Added in Cart");
                fbq('track', 'AddToCart');
                dataLayer.push({
                    'event': 'addToCart',
                    'ecommerce': {'currencyCode': 'USD', 'add': {'products': [{'name': shoeName, 'price': '195', 'brand': 'Awl & Sundry', 'category': 'Ready To Wear Shoes', 'variant': $(this).parents('.product-box').find(".product-color").text(), 'quantity': 1}]}}
                });
                window.location.href = baseUrl + 'cart/';
            }
        });
    });

    /*$('.product-image-link').click(function (e) {
        var color = $(this).parents('.product-box').find('.product-color').text();
        var shoe_name = $(this).parents('.product-box').find('.product-name').text();

        dataLayer.push({
            'event': 'productClick',
            'ecommerce': {'click': {'actionField': {'list': 'Search Results'}, 'products': [{'name': shoe_name, 'price': '195', 'brand': "Awl & Sundry", 'category': "Ready To Wear Shoes", 'variant': color}]}},
            'eventCallback': function () {
                color = color.replace(/\s+/g, '-').toLowerCase();
                document.location = baseUrl + 'shop/products/' + color + '/' + shoe_name;
            }
        });
    });*/
});

$(window).load(function () {
    var productList = [];
    var j = 0;

    $(".product-box").each(function () {
        j++;
        var name = $(this).children().children().children('h3.product-name').text();
        var variant = $(this).children().children().children('p.product-color').text();
        productList.push({'name': name, 'price': 195, 'brand': 'Awl & Sundry', 'category': 'Ready To wear Shoes', 'variant': variant, 'position': j});
    });

    dataLayer.push({'ecommerce': {'currencyCode': 'USD', 'impressions': productList}});
});