var category = "Custom Shoes";

var ShoeCart = {
    init: function () {
        $(".shopping_cart_mini, .cart-items").on("click", 'a.delete', ShoeCart.removeCartItem);
        $(".shopping_cart_mini, .cart-items").on("change", 'input.quantity', ShoeCart.updateQuantity);
        $(".cart-items").on("click", 'a.edit', ShoeCart.editCartItem);

        $(".cart").on('click', "#selection_popup .rework.edit", function () {
            ShoeCart.editCartItem($(this).parents('ul').find('#cart_item_detail_id').val());
        });

        jQuery('.cart .popup .edit').off('click touchend').on('click touchend', function () {
            ShoeCart.editCartItem;
        });

        $("#promocode").on("click", '#apply_promocode', ShoeCart.validatePromo);
        $("#cart-checkout").on("click", '#cart-checkout_btn', ShoeCart.checkout);

        $('#cart-continue_btn').click(function () {
            window.location.href = baseUrl;
        });
    },
    loadCart: function () {
        $.ajax({
            type: "POST",
            url: baseUrl + "customshoe/getcartitems",
            success: function (msg) {
                $(".shopping_cart_mini").html(msg);
                $(".shopping_cart_mini .ico-img img").error(function () {
                    $(this).hide();
                });
            }
        });
    },
    removeCartItem: function () {
        if (confirm('Are you sure you want to remove the following item(s) from your cart?')) {
            var cartId = $(this).parents(".cart-items").find(".cartId").val();
            var productName = $(this).parents(".cart-items").find("p.cart-product-style span").text();
            var price = $(this).parents(".cart-items").find(".cart-price span").text();
            price = price.replace(/\D/g, '') / 100;
            var quantity = $(this).parents(".cart-items").find(".quantity").val();

            $.ajax({
                type: "POST",
                url: baseUrl + "shoecart/removecartitem",
                data: {cartId: cartId},
                success: function (msg) {
                    if ($("body").hasClass("cart")) {
                        var shoe_collection = [];
                        shoe_collection["Oxford"] = "Horatio";
                        shoe_collection["Chukka Boot"] = "Abbott";
                        shoe_collection["Monkstrap"] = "Francesco";
                        shoe_collection["Derby"] = "Andrea";

                        if ($.inArray(productName, shoe_collection)) {
                            category = "Ready To Wear Shoes";
                            productName = shoe_collection[productName];
                        }

                        dataLayer.push({'event': 'removeFromCart', 'ecommerce': {'remove': {'products': [{'name': productName, 'price': price, 'brand': "Awl & Sundry", 'category': category, 'quantity': quantity}]}}});
                        window.location.href = baseUrl + "cart";
                    }
                }
            });
        }
    },
    updateQuantity: function () {
        var cartId = $(this).parents(".cart-items").find(".cartId").val();
        var quantity = $(this).parents(".cart-items").find(".quantity").val();
        if (!isNormalInteger(quantity)) {
            alert('Quantity should be a positive integer');
            return;
        }

        $.ajax({
            type: "POST",
            url: baseUrl + "shoecart/update_quantity",
            dataType: "json",
            data: {cartId: cartId, quantity: quantity},
            success: function (msg) {
                if ($("body").hasClass("cart")) {
                    $('#total-price h1').html('$' + msg.total);
                    $('#total-balance h1').html('$' + msg.credit);
                }
            }
        });
    },
    editCartItem: function () {
        var cartId = $(this).parents(".cart-items").find(".cartId").val();
        if (typeof cartId == "undefined" && typeof arguments !== "undefined" && typeof arguments[0] !== "undefined") {
            cartId = arguments[0];
        }

        $.ajax({
            type: "POST",
            url: baseUrl + "shoecart/editcartitem",
            data: {cartId: cartId},
            success: function (msg) {
                window.location.href = baseUrl + "create-a-custom-shoe/design-features";
            }
        });
    },
    addDesignToStepFour: function (designId) {
        $.ajax({
            url: baseUrl + "customshoe/addDesignToStepFour/" + designId,
            success: function (msg) {
                window.location.href = baseUrl + "create-a-custom-shoe/details";
            }
        });
    },
    addDesign: function (designId, fn) {
        var url = baseUrl + "shoecart/addtocart?" + ((new Date()).getTime());
        if (designId !== "") {
            url += "/" + designId;
        }
        $.ajax({
            url: url,
            success: function (msg) {
                if ($("body").hasClass("cart")) {
                    window.location.href = baseUrl + "custom-shoe/order";
                } else {
                    if (typeof fn !== "undefined") {
                        fn();
                    } else {
                        alert("Successfully added to cart");
                    }
                }
            }
        });
    },
    validatePromo: function () {
        if ($('#promocode #code').val().trim() === '') {
            alert('Please enter Promocode and then click');
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: baseUrl + "shoecart/getPromocode",
                data: {promocode: $('#promocode #code').val()},
                dataType: 'json',
                success: function (msg) {
                    if (msg.IsValid) {
                        if (msg.promo_discount > 0) {
                            $('#lblDiscount').html('PROMO DISCOUNT     <span>$' + msg.promo_discount + '</span>');
                            $('#total-promo-balance').children().text('$' + msg.promo_discount);
                        } else {
                            $('#lblDiscount').text('');
                        }
                        $('#total-price h1').text('$' + msg.netAmt);
                    } else {
                        alert(msg.message);
                        $('#lblDiscount').text('');
                    }
                }
            });
        }
    },
    validateGiftCard: function () {
        if ($('#giftcard').val().trim() === '') {
            alert('Please enter Gift Card number and then click');
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: baseUrl + "giftcard/getGiftCard",
                data: {giftcard_number: $('#giftcard').val()},
                dataType: 'json',
                success: function (msg) {
                    if (msg.IsValid) {
                        $('#lblDiscount').text('Discount : $ ' + msg.discount);
                        $('#lblCartTotal').text('$' + msg.netAmt);
                    } else {
                        alert(msg.message);
                        $('#lblDiscount').text('');
                    }
                }
            });
        }
    },
    checkout: function () {
        var products = [];
        var shoe_collection = [];
        shoe_collection["Oxford"] = "Horatio";
        shoe_collection["Chukka Boot"] = "Abbott";
        shoe_collection["Monkstrap"] = "Francesco";
        shoe_collection["Derby"] = "Andrea";

        $('.cart-items').each(function () {
            var category = $(this);
            var name = $(this).children(".cart-products").children(".cart-product-details").children(".cart-product-style span").text();
            if ($.inArray(name, shoe_collection)) {
                name = shoe_collection[name];
                category = 'Ready To Wear Shoes';
            }
            products.push({'name': name, 'price': '195', 'brand': 'Awl & Sundry', 'category': 'Shoes', 'quantity': $(this).find(".quantity").val()});
        });

        fbq('track', 'InitiateCheckout');

        dataLayer.push({'event': 'checkout', 'ecommerce': {'checkout': {'actionField': {'step': 1, 'option': 'checkout'}, 'products': products}}});

        var comments = $("#comments").val();
        if (typeof comments !== 'undefined') {
            $.ajax({
                type: "POST",
                dataType: "json",
                data: {'comment': comments},
                url: base_url + "common/addKlarnaComments/",
                success: function (msg) {
                    window.location = baseUrl + "checkout";
                    return false;
                }
            });
        } else {
            window.location = baseUrl + "checkout";
            return false;
        }
    }
};

$(document).ready(function () {
    ShoeCart.init();
});

function isNormalInteger(str) {
    var n = ~~Number(str);
    return String(n) === str && n > 0;
}

function processGiftCard() {
    if ($('#cer_code').val().trim() !== "") {
        giftcard_code = {code: $('#cer_code').val().trim(), from: 'cart'};
        $.ajax({
            type: "POST",
            dataType: "json",
            data: giftcard_code,
            url: base_url + "common/processGiftCard/",
            success: function (msg) {
                if (!msg.is_login) {
                    window.location = base_url + msg.url;
                } else {
                    if (msg.is_valid) {
                        alert('your card balnce is :' + msg.balance);
                        window.location = window.location;
                    } else {
                        alert(msg.errors.validCard);
                    }
                }
            }
        });
    } else {
        alert('Please enter Gift Certificate Code.');
        $('.gc_msg').text('Please enter Gift Certificate Code. ');
        $('#cer_code').focus();
    }
    return false;
}