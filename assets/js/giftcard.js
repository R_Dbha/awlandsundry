$(document).ready(function () {
    var amountOffset, detailsOffset;
    g_card_resize();
    if (window.location.hash !== '') {
        changeHash();
    }
    if (isTouch) {
        if (window.innerWidth <= 767) {
            amountOffset = 70;
            detailsOffset = 70;
        } else {
            amountOffset = 200;
            detailsOffset = 170;
        }
    } else {
        amountOffset = 200;
        detailsOffset = 170;
    }

    $('.gift_card a.gift_card_btn').click(function () {
        $('#choose_amount').show();
        $('html, body').animate({scrollTop: $("#choose_amount").offset().top - amountOffset}, 1000);
    });

    $('.choose_amount').click(function () {
        $('#amount').val($(this).find('input').val());
        $('.choose_amount').removeClass("g_card_active");
        $('#custom_amount').val('');
        $(this).addClass("g_card_active");
        $('#choose_amount span.none').text('');
        $('#details_wrap').show();
        $('html, body').animate({scrollTop: $("#details_wrap").offset().top - detailsOffset}, 1000);
    });

    $('.othr_amt').click(function () {
        $('.choose_amount').removeClass("g_card_active");
        $(this).addClass("g_card_active");
        $('#details_wrap').show();
        $('html, body').animate({scrollTop: $("#details_wrap").offset().top - detailsOffset}, 1000);
    });

    $('.choose_amount').each(function () {
        if ($(this).find('input').val() === $('#amount').val()) {
            $(this).addClass('g_card_active');
        }
    });

    $('#custom_amount').change(function () {
        var custom_amount = $(this).val();
        if (!isNormalInteger(custom_amount)) {
            alert('Amout should be a positive integer');
            $('#custom_amount').val('');
            return;
        }

        $('.choose_amount').removeClass('g_card_active');
        $('#amount').val('');
        $('#choose_amount span.none').text('');
    });

    $('#details_wrap input').change(function () {
        $(this).parent('p').children('span.none').text('');
    });

    $('#datetime').datetimepicker({mask: '9999/19/39 29:00', defaultTime: '10:00', minDate: '-1970/01/01'});
});

function isNormalInteger(str) {
    var n = ~~Number(str);
    return String(n) === str && n > 0;
}

function g_card_resize() {
    var totalheight = window.innerHeight;
    var gift_card = totalheight;
    var menu_height = $('#header-wrapper').outerHeight();
    $('#g_card').css('height', gift_card - menu_height);
}

$(window).on('hashchange', function () {
    changeHash();
});

function changeHash() {
    var id = window.location.hash;
    $('#choose_amount').show();
    console.log(id);
    if ($(id).length) {
        $("html, body").stop(true, true).animate({scrollTop: $(id).offset().top - 200}, 1000);
    }
}