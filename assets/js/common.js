var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
var bottom_slider;
var get_inspired_0, get_inspired_1, get_inspired_2;

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$(document).ready(function () {
    $('body').on('submit', '#register_form', function (e) {
        fbq('track', 'CompleteRegistration');
    });

    //header deletion code
    $('.closeAppointment').on('click', function (e) {
        var appointmentHeight = $("#appointmentHeader").outerHeight();
        $('#header-wrapper').addClass('hide-appointment');
        var headerHeight = $('#header-wrapper-sticky-wrapper').outerHeight();
        $('#header-wrapper-sticky-wrapper').css('height', headerHeight - appointmentHeight);
        $('#faq-menu-wrapper').addClass('hide-appointment');
        $('#aboutMenu-wrapper-sticky-wrapper').addClass('hide-appointment');
        $('#aboutMenu-wrapper').addClass('hide-appointment');
        $("#aboutMenu-wrapper").css('top', headerHeight);
        $('#shop-filter').addClass('hide-appointment');
    });

    if (isTouch) {
        if (window.innerWidth <= 767) {
//            $('.menu-right').appendTo('#slick');
            $('.menu-left').appendTo('#header');
//            $('.progress li').click(function () {
//                $('#progress_msg').show();
//                $('#progress_msg').html($(this).attr('title'));
//                setTimeout(function () {
//                    $('#progress_msg').hide();
//                    $('#progress_msg').html('');
//                }, 1000);
//            });
        }
    }

    if (isTouch) {
        if (window.innerWidth <= 767) {
//            $('#header-wrapper').addClass('hide-appointment');
            if (window.innerHeight > window.innerWidth) {
                $('#get_inspired_2_banner_wrapper').removeClass('landscape_bg');
                $('#get_inspired_2_banner_wrapper').addClass('portrait_bg');
            } else {
                $('#get_inspired_2_banner_wrapper').removeClass('portrait_bg');
                $('#get_inspired_2_banner_wrapper').addClass('landscape_bg');
            }
        }
    }

    bottom_slider = $('.bottom_slider').bxSlider({
        auto: true, controls: true, minSlides: 2, maxSlides: 2, slideWidth: 597, slideMargin: 10
    });

    if ($('body').hasClass('get_inspired')) {
        if (isTouch) {
            if (window.innerWidth <= 767) {
                bottom_slider.reloadSlider({minSlides: 1, maxSlides: 1, });
            }
        }
    }

    // Ajith VP
    var totalheight = window.innerHeight;
    // var heighthead = $('#header').height();
    // var slider_height = totalheight - heighthead;
    var slider_height = totalheight;
    var menu_height = $('#menu').outerHeight();
    setTimeout(adjustBanner, 100);
    setTimeout(function () {
        $('#slider').css('height', slider_height - menu_height);
        $('#slider-wrapper').css('height', slider_height);
        var heighthead = $('#header').height();
        $('#get_inspired_2_banner_wrapper').css('height', (totalheight - menu_height) - 10);
    }, 100);

    var headerHeight = $('#header').height();
    var faqHeight = (slider_height - headerHeight) - menu_height;
    $('#faq-contact').css('height', faqHeight);

    if ($('body').hasClass('jobs')) {
        var scrolloc = $('#jobs_description > h2').offset().top;
        $('#jobs_marketing').click(function () {
            $('#jobs_description').addClass('showMe');
            $('body, html').animate({
                scrollTop: scrolloc - 150
            }, 1000);
        });
    }
});

$(window).load(function () {
    if (isTouch) {
        if (window.innerWidth <= 767) {
            $('#supersized').addClass('touch-slider');
            $(".menu-left").sticky({topSpacing: 50});
        }
    }

    $("li.mobile-shop a").on("click", function (e) {
        e.preventDefault();
        if ($('li.mobile-dropdown').css('display') == 'none') {
            $("li.mobile-dropdown").css("display", "block");
        } else {
            $("li.mobile-dropdown").css("display", "none");
        }
    });

    $("li.mobile-custom a").on("click", function (e) {
        e.preventDefault();
        if ($('li.mobile-dropdown-1').css('display') == 'none') {
            $("li.mobile-dropdown-1").css("display", "block");
        } else {
            $("li.mobile-dropdown-1").css("display", "none");
        }
    });

    if (isTouch) {
        $('body').addClass('touch');
    }
});

$('span.close').click(function () {
    $('body').css({'overflow': 'auto', 'position': 'static'});
    $('.overlay').hide();
});

var adjustBanner = function () {
    var totalheight = window.innerHeight;
    var slider_height = totalheight;
    var menu_height = $('#menu').outerHeight();
    var headerHeight = $('#header-wrapper-sticky-wrapper').outerHeight();
    $('#slider').css('height', slider_height - headerHeight - menu_height);
    $('#slider-wrapper').css('height', slider_height - headerHeight);
}

$(window).resize(adjustBanner);

$(window).resize(function () {
    var totalheight = window.innerHeight;
    var slider_height = totalheight;
    var headerHeight = $('#header').height();
    var menu_height = $('#menu').outerHeight();

    $('#get_inspired_2_banner').css('height', slider_height - headerHeight);
    $('#get_inspired_2_banner_wrapper').css('height', (totalheight - menu_height) - 10);
});

if (isTouch) {
    if (window.innerWidth <= 767) {
        $(window).resize(function () {
            if (window.innerHeight > window.innerWidth) {
                $('#get_inspired_2_banner_wrapper').removeClass('landscape_bg');
                $('#get_inspired_2_banner_wrapper').addClass('portrait_bg');
            } else {
                $('#get_inspired_2_banner_wrapper').removeClass('portrait_bg');
                $('#get_inspired_2_banner_wrapper').addClass('landscape_bg');
            }
        });
    }
}

$.fn.accordion = function () {
    for (var i = 0; i < this.length; i++) {
        this[i].onclick = function () {
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
};