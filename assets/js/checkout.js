var copy = false;
var checkoutSlider;

$(document).ready(function () {
    $('#checkout').on('click', '#shipping-address .checkout-overlay', function (e) {
        $('.checkout-overlay').removeClass('hidden');
        $('#shipping-address .checkout-overlay').addClass('hidden');
    });

    $('#checkout').on('click', '#billing-address .checkout-overlay', function (e) {
        $('.checkout-overlay').removeClass('hidden')
        $('#billing-address .checkout-overlay').addClass('hidden');
    });

    $('#checkout').on('click', '#review .checkout-overlay', function (e) {
        $.when(validate_shipping_address(e)).then(function () {
            if (is_shipping_error) {
                $.when(validate_billing_address(e)).then(function () {
                    if (is_billing_error) {
                        $('.checkout-overlay').removeClass('hidden')
                        $('#review .checkout-overlay').addClass('hidden');
                    }
                });
            }
        });
    });

    $.ajax({
        type: "POST",
        url: baseUrl + "common/get_stripe_key/",
        success: function (response) {
            Stripe.setPublishableKey(response);
        }
    });

    if (!$('#billing-address').find('.checkout-overlay').hasClass('hidden')) {
        $('#billing-address').find('span.error').html('');
    }

    $("#checkout-form").validate({
        submitHandler: submit,
        rules: {
            "card-cvc": {cardCVC: true, required: true},
            "card-number": {cardNumber: true, required: true},
            // "card-expiry-year": "cardExpiry" // we don't validate month separately
        },
        invalidHandler: function (event, validator) {
            if ($('#payment_method').val() === "Paypal" || ($('#billing_total').val() === "0" && is_billing_error)) {
                removeInputNames();
                form.submit();
                return false;
            }
        },
    });

    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
    jQuery.validator.addMethod("cardExpiry", validateExpiry, "Please enter a valid expiration");

    addInputNames();

    //format card number using formance plugin
    $('#card-number').formance('format_credit_card_number');
    $('#checkout').on('change', '.payment-method', function () {
        if ($(this).val() === 'Paypal') {
            $('.method-credit-card').addClass('hidden');
            $('.method-paypal').removeClass('hidden');
        } else {
            $('.method-credit-card').removeClass('hidden');
            $('.method-paypal').addClass('hidden');
        }
    });

    $('#checkout').on('change', 'input, select', function () {
        $(this).parent('td').children('span.error').text('');
    });

    $('#checkout').on('keydown', '#card-cvc', function () {});

    $('#checkout').on('click', '#copy_same_address', function () {
        var class_name;
        if ($(this).is(':checked')) {
            $('#shipping-address input').each(function () {
                class_name = $(this).attr('class');
                if (typeof class_name !== 'undefined') {
                    class_name = class_name.replace('valid', '');
                    $('#billing-address').find('.' + class_name).val($(this).val());
                }
            });
            $('#shipping-address .country').each(function () {
                copy = true;
                $('#billing-address').find('.country').val($(this).val()).change();
            });
        }
    });

    $('#checkout').on('change', '.country', function () {
        var country = $(this).val();
        var state = $(this).parents('.form').find('.state');
        $.ajax({
            type: "POST",
            url: baseUrl + "common/get_states_ajax/" + country,
            success: function (msg) {
                var name = $(state).attr('name');
                var id = $(state).attr('name');

                if (msg == $(state).val() || msg == '') {
                    $(state).replaceWith('<input id="' + id + '" type="text" name="' + name + '" class="state">');
                } else {
                    $(state).replaceWith('<select id="' + id + '" name="' + name + '" class="state">' + msg + '</select>');
                }

                if (copy) {
                    $(state).val($('#shipping-address .state').val());
                    copy = false;
                }
            }
        });
    });

    $('#checkout').on('change', '#shipping_state', function () {
        var state = $(this).val();
        if (state === 'New Jersey' || state === 'NJ') {
            $.ajax({
                type: "POST",
                url: baseUrl + "common/get_checkout_amount",
                data: {state: state},
                dataType: "json",
                success: function (msg) {
                    $('#sub_total').text('$' + msg.sub_total);
                    $('#tax_total').text('$' + msg.taxAmt);
                    $('#net_total').text('$' + msg.total_amt);
                    $('#hid_from_state').val(state);
                }
            });
        } else {
            if ($('#hid_from_state').val() === 'New Jersey' || $('#hid_from_state').val() === 'NJ') {
                $.ajax({
                    type: "POST",
                    url: baseUrl + "common/get_checkout_amount",
                    data: {state: state},
                    dataType: "json",
                    success: function (msg) {
                        $('#sub_total').text('$' + msg.sub_total);
                        $('#tax_total').text('$' + msg.taxAmt);
                        $('#net_total').text('$' + msg.total_amt);
                        $('#hid_from_state').val(state);
                    }
                });
            }
        }
    });

    $('#checkout').on('click', '#shipping_next', validate_shipping_address);
    $('#checkout').on('click', '#billing_next', validate_billing_address);

    if (isTouch) {
        if (window.innerWidth <= 767) {
            $('#checkout-form').find('.checkout-container').height($('#billing-address').height());
            checkoutSlider = $('#checkout-form').bxSlider({
                auto: false, controls: false, pager: false, infiniteLoop: false, hideControlOnEnd: true, responsive: true, touchEnabled: true,
                onSlideAfter: function () {
                    $("html, body").animate({scrollTop: 0}, 500);
                }
            });
        }
    }
});

var is_shipping_error = false;
var is_billing_error = false;

function validate_shipping_address(e) {
    e.preventDefault();
    var country = $("#shipping_country").val();
    var countryArray = ["United States", "Germany", "Canada", "UK", "Austria", "Switzerland", "USA", "United Kingdom"];

    if (country == 'select') {
        alert('Select Shipping Country');
        return;
    }

    if (country == 'North Korea' || country == 'Pakistan' || country == 'Syria' || country == 'Iraq') {
        alert('We are sorry, but we don\'t deliver to ' + country);
        return;
    }

    if (countryArray.indexOf(country) === -1) {
        alert("Shipping cost of $35 will be charged for your country");
        $("#shipping_cost").text('$' + 35 + ".00");
        var total_value = ($("#sub_total").text()).substring(1);
        total_value = parseFloat(total_value.replace(',', '')) + 35.00;
        $("#net_total").text('$' + total_value + ".00");
        $("#net_total").number(true, 2);
    } else {
        $("#shipping_cost").text('$0.00');
        var sub_total_value = ($("#sub_total").text()).substring(1);
        var tax_value = ($("#tax_total").text()).substring(1);
        total_value = parseFloat(sub_total_value.replace(',', '')) + parseFloat(tax_value);
        $("#net_total").text('$' + total_value + '.00');
        $("#net_total").number(true, 2);
    }

    $.ajax({
        type: "POST",
        url: baseUrl + "common/validate_shipping_details",
        data: $('#checkout form').serialize(),
        dataType: 'html',
        success: function (response) {
            $('#shipping-address').html(response);
            if ($('#shipping_error').val() === '1') {
                $('#billing-address').find('.checkout-overlay').removeClass('hidden');
            } else {
                is_shipping_error = true;
                if (is_billing_error) {
                    $('#billing-address').find('.checkout-overlay').removeClass('hidden');
                    $('#review').find('.checkout-overlay').addClass('hidden');
                } else {
                    $('#billing-address').find('.checkout-overlay').addClass('hidden');
                    $('#review').find('.checkout-overlay').removeClass('hidden');
                    if (isTouch) {
                        if (window.innerWidth <= 767) {
                            checkoutSlider.goToNextSlide();
                        }
                    }
                }
            }
        }
    });
}

function validate_billing_address(e) {
    e.preventDefault();

    var card_name = $('#card-name').val();
    var card_number = $('#card-number').val();
    var card_expiry_month = $('#card-expiry-month').val();
    var card_expiry_year = $('#card-expiry-year').val();
    var card_cvc = $('#card-cvc').val();
    var country = $("#billing_country").val();

    if (country == 'select' && $('#billing-address div.form:not("invisible")')) {
        alert('Select Billing Country');
        return;
    }
    $.ajax({
        type: "POST",
        url: baseUrl + "common/validate_billing_details",
        data: $('#checkout form').serialize(),
        dataType: 'html',
        success: function (response) {
            fbq('track', 'AddPaymentInfo');

            $('#billing-address').html(response);
            // Reset card details since they are not sending to the server
            $('#card-name').val(card_name);
            $('#card-number').val(card_number);
            $('#card-expiry-month').val(card_expiry_month);
            $('#card-expiry-year').val(card_expiry_year);
            $('#card-cvc').val(card_cvc);

            if ($('#billing_error').val() === '1') {
                if ($('.payment-method').val() !== "Paypal" && !validateCard()) {
                    $('#card').prev('span.error').text('Invalid Payment Details');
                    addInputNames();
                    $('#card-number').formance('format_credit_card_number');
                    $('#billing-address').find('.checkout-overlay').addClass('hidden');
                    is_billing_error = false;
                } else {
                    is_billing_error = true;
                    $('#review').find('.checkout-overlay').removeClass('hidden');
                }
            } else {
                if ($('#billing_total').val() !== "0" && $('.payment-method').val() !== "Paypal" && !validateCard()) {
                    $('#card').prev('span.error').text('Invalid Payment Details');
                    addInputNames();
                    $('#card-number').formance('format_credit_card_number');
                    $('#billing-address').find('.checkout-overlay').addClass('hidden');
                    is_billing_error = false;
                } else {
                    is_billing_error = true;
                    if (isTouch) {
                        if (window.innerWidth <= 767) {
                            checkoutSlider.goToNextSlide();
                        }
                    }
                    if (is_shipping_error) {
                        $('#review').find('.checkout-overlay').addClass('hidden');
                    } else {
                        $('#shipping-address').find('.checkout-overlay').removeClass('hidden');
                    }
                }

            }
        }
    });
}

function validateExpiry() {
    return Stripe.validateExpiry($("select.card-expiry-month").val(), $("select.card-expiry-year").val());
}

function validateCard() {
    $('#card-number').validate();
    return Stripe.validateCardNumber($('#card-number').val()) && Stripe.validateCVC($('#card-cvc').val()) && validateExpiry();
}

function addInputNames() {
    $(".card-number").attr("name", "card-number")
    $(".card-cvc").attr("name", "card-cvc")
    $("select.card-expiry-year").attr("name", "card-expiry-year")
}

function removeInputNames() {
    $(".card-number").removeAttr("name")
    $(".card-cvc").removeAttr("name")
    $("select.card-expiry-year").removeAttr("name")
    $("select.card-expiry-month").removeAttr("name")
}

function submit(form) {
    removeInputNames(); // THIS IS IMPORTANT!

    onCheckoutOption(2, $(".payment-method").val());
    $(form['review_submit']).attr("disabled", "disabled");

    Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val(),
    }, function (status, response) {
        if (response.error) {
            $(form['submit-button']).removeAttr("disabled");
            $('#review').find('.checkout-overlay').removeClass('hidden');
            $('#billing-address').find('.checkout-overlay').addClass('hidden');
            $("#payment-method span.error").html(response.error.message);
            addInputNames();
            return false;
        } else {
            var token = response['id'];
            var input = $("<input name='stripeToken' value='" + token + "' style='display:none;' />");
            $(form['submit-button']).removeAttr("disabled");
            form.appendChild(input[0]);
            fbq('track', 'Purchase');
            
            form.submit();
        }
    });
    return false;
}

function onCheckoutOption(step, checkoutOption) {
    dataLayer.push({'event': 'checkoutOption', 'ecommerce': {'checkout_option': {'actionField': {'step': step, 'option': checkoutOption}}}});
}
