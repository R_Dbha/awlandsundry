var player;
var featured;
var isiPad = navigator.userAgent.match(/iPad/i) != null;


$(document).ready(function () {
    if (isiPad) {
        // bannerSlider();
    }

    if (isTouch) {
        if (window.innerWidth <= 767) {
            if (window.innerHeight > window.innerWidth) {
                $('#slider-wrapper').removeAttr('class');
                $('#slider-wrapper').addClass('portrait_bg');
            } else {
                $('#slider-wrapper').removeAttr('class');
                $('#slider-wrapper').addClass('landscape_bg');
            }
        }
    }
    if (typeof window.localStorage['popup_shown'] === "undefined") {
        //  $('body').css({'overflow': 'hidden', 'position': 'fixed', 'width': '100%'});
        if (!isTouch) {
            $('.overlay').show();
            window.localStorage['popup_shown'] = 1
        }
    }
    /*player = new MediaElementPlayer('#process', {
     features: ['playpause', 'current', 'progress', 'duration', 'tracks', 'volume']
     });
     player.media.addEventListener('ended', function (e) {
     $('.mejs-container').hide(500);
     }, false);*/

    if (isiPad) {
        //$('#spinner_overlay').show();
        //$(' #message').show();
    } else {
        $('#spinner_overlay').hide();
        $('#message').hide();
    }
    $('#feedback-tab').click(function () {
        $('.feedback-score-table input[type="radio"]:checked').each(function () {
            $(this).prop('checked', false);
        });
        $('#feedback-comments').val('');
        $('#feedback-form').show();
        $('#feedback-form-popup').show();
        $('#feedback-thankyou').hide();
    });
    $('button.cancel, .modal-dialog-sop-theme-title-close, button.finish').click(function () {
        $('#feedback-form').hide();
    });



});
if (isTouch) {
    if (window.innerWidth <= 767) {
        $(window).resize(function () {
            if (window.innerHeight > window.innerWidth) {
                $('#slider-wrapper').removeAttr('class');
                $('#slider-wrapper').addClass('portrait_bg');
            } else {
                $('#slider-wrapper').removeAttr('class');
                $('#slider-wrapper').addClass('landscape_bg');
            }
        });
    }
}
function send_feedback() {
    if (typeof $(".feedback-score-table input[name=score]:checked").val() !== 'undefined' || $('#feedback-comments').val() !== "") {
        $.ajax({
            type: "POST",
            url: baseUrl + "common/send_feedback",
            data: $('#feedback-form-popup').serialize(),
            dataType: 'html',
            success: function (response) {
                $('#feedback-form-popup').hide();
                $('#feedback-thankyou').show();
            }
        });
    } else {
        alert('You haven\'t entered any feed back yet ');
        return false;
    }

    return false;
}

$(function ($) { // on document load
    $('#menuitem').ddscrollSpy({scrolltopoffset: -90});
    $('.slicknav_nav').ddscrollSpy({scrolltopoffset: -100});
});
/*
 // MAIN SLIDER
 jQuery(function ($) {
 
 $.supersized({
 // Functionality
 slide_interval: 5000, // Length between transitions
 transition: 1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
 transition_speed: 700, // Speed of transition,
 
 // Components							
 slide_links: 'blank', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
 slides: [// Slideshow Images
 {
 image: baseUrl + 'assets/css/images/slider/slider_4.jpg',
 title: 'PRE ORDER NOW',
 url: baseUrl + 'shop/ready-wear',
 alt: 'Hand crafted Shoes',
 head: 'SPRING/SUMMER 2017',
 sub_head: 'Introducing our first-ever ready-to-wear collection<br/><strike>$225</strike>&nbsp$195<br/><br/>',
 class_name: 'lefthead'
 },
 {
 image: baseUrl + 'assets/css/images/slider/kam.jpg',
 title: 'LEARN MORE',
 url: baseUrl + 'kam-chancellor-collection',
 alt: 'Introducing Kam Chancellor\'s Signature Collection',
 head: 'Introducing<br/> Kam Chancellor\'s<br/> Signature Collection',
 sub_head: '',
 class_name: 'righthead'
 },
 {
 image: baseUrl + 'assets/css/images/slider/slider1.jpg',
 title: 'LEARN MORE',
 url: baseUrl + 'products/oxford/casual/David',
 alt: 'Buy a custom pair and educate a child in need',
 head: 'Step Easily Into ' + "<br>" + ' an Original Pair',
 sub_head: 'We\'re partners: You design your shoes,<br/> we\'ll custom make them by hand for you',
 class_name: 'righthead'
 },
 {
 image: baseUrl + 'assets/css/images/slider/picture3.jpg',
 title: 'GET INSPIRED',
 url: baseUrl + 'shop',
 alt: 'Design custom handmade shoes',
 head: 'Express Your Inner Rebel ',
 sub_head: 'Your individuality can ' + "<br>" + ' stand out by designing a shoe ' + "<br>" + ' that makes an impression',
 class_name: 'lefthead'
 },
 {
 image: baseUrl + 'assets/css/images/slider/Brown_Monkstrap.jpg',
 title: 'Design Now',
 url: baseUrl + 'create-a-custom-shoe',
 alt: 'Hand crafted Shoes',
 head: 'Fit your personality ' + "<br>" + 'so perfectly,  your feet ' + "<br>" + ' will be jealous',
 sub_head: 'Custom shoes means ' + "<br>" + ' ideal fit and ideal design',
 class_name: 'righthead'
 },
 {
 image: baseUrl + 'assets/css/images/slider/Number5.jpg',
 title: 'GET INSPIRED',
 url: baseUrl + 'products/oxford/classics/Sullivan',
 alt: 'buy online custom shoes',
 head: 'Walk the walk',
 sub_head: 'Awl & Sundry lets the boss ' + "<br>" + ' dress like the boss ',
 class_name: 'lefthead'
 },
 {
 image: baseUrl + 'assets/css/images/slider/Picture1.JPG',
 title: 'LEARN MORE',
 url: baseUrl + '#workshop',
 alt: 'custom dress shoes online',
 head: 'The future, now ' + "<br>" + ' The way it always ' + "<br>" + 'should have been',
 sub_head: ' Your shoes are handstitched and ' + "<br>" + ' every shoe is hand welted to last',
 class_name: 'lefthead'
 },
 
 ]
 });
 });
 */


$('#workshop').on('click', function () {
    player.play();
    $('.mejs-container').show(500);
});

$("#newsletter-email").keyup(function () {
    $('.newletter-content p.show').hide();
});
$("#newsletter_email").keyup(function () {
    $('.newsletter p.show').hide();
});
function keepEmail(mode) {
    if (mode != 'footer') {
        var keepemail = $('#newsletter-email').val();
    } else {
        var keepemail = $('#newsletter_email').val();
    }
    if (keepemail === "")
    {
        if (mode != 'footer') {
            $('.newletter-content p.show').text('Please enter email address');
            $('.newletter-content p.show').show();
        } else {
            $('.newsletter p.show').text('Please enter email address');
            $('.newsletter p.show').show();
        }
        return false;
    }
    var atpos = keepemail.indexOf("@");

    var dotpos = keepemail.lastIndexOf(".");

    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= keepemail.length)
    {
        if (mode != 'footer') {
            $('.newletter-content p.show').text('Not a valid e-mail address');
            $('.newletter-content p.show').show();
            $('#newsletter-email').val("");
        } else {
            $('.newsletter p.show').text('Not a valid e-mail address');
            $('.newsletter p.show').show();
            $('#newsletter_email').val("");
        }
        return false;
    } else {
        //ga('send', 'event', 'Acquisition', 'Newsletter');
        $.ajax({
            type: "POST",
            url: baseUrl + "common/keepmail",
            data: {email: keepemail},
            success: function (msg) {
                if (mode != 'footer') {
                    $('.newletter-content form.hide').hide();
                    $('.newletter-content p.show').text('YOU ARE SIGNED UP ! THANK YOU !');
                    $('.newletter-content p.show').show();

                } else {
                    $('.keepmail_msg').text('YOU ARE SIGNED UP ! THANK YOU !');
                    $('.keepmail_msg').show();
                    $('#newsletter_email').val("");
                }
            }
        });
    }
    return false;
}

$('.featured-slider').addClass('touch');
var slider = $('.testimonial-container').bxSlider({
    minSlides: 2,
    maxSlides: 2,
    slideWidth: 500,
    pager: true,
    controls: true,
    infiniteLoop: false,
});
featured = $('.featured-slider').bxSlider({
    minSlides: 2,
    maxSlides: 2,
    slideWidth: 520,
    slideMargin: 10,
    pager: true
});



$(window).load(function () {
    // $('#supersized').appendTo("#slider");
    $('#spinner_overlay').hide();
    $('#slider-wrapper').css('visibility', 'visible');
    if (isTouch) {
        if (window.innerWidth <= 767) {
            slider.reloadSlider({
                minSlides: 1,
                maxSlides: 1,
                controls: false
            });

            featured.reloadSlider({
                minSlides: 1,
                maxSlides: 1,
                pager: true,
                controls: false

            });
        }
    }
    $('#slidecaption a').attr('target', '_blank');
});

$(window).resize(function () {
    if (isiPad) {
        $('#supersized li').each(function (i) {
            filename = $(this).find('img').attr('src');
            landscape = filename.indexOf('1024');
            portrait = filename.indexOf('768');
            if (landscape !== -1) {
                var a = filename.replace('1024', '768');
                $(this).find('img').remove();
                $(this).find('a').append("<img src=" + a + ">");
                $(this).find('img').css({'position': 'fixed', 'top': '0', 'left': '0'});
            }
            if (portrait !== -1) {
                var a = filename.replace('768', '1024');
                $(this).find('img').remove();
                $(this).find('a').append("<img src=" + a + ">");
                $(this).find('img').css({'position': 'fixed', 'top': '0', 'left': '0'});
            }

        });

    }
});