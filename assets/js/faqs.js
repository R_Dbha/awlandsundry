$(document).ready(function () {
    if (window.location.hash !== '') {
        changeHash();
        $(window.location.hash).find('.answer').show().addClass('hide');
    }

    $('#menuitem a').click(function (e) {
        var top = 120;
        if ($("#faq-menu-wrapper").hasClass('hide-appointment')) {
            top = 90;
        }
        e.preventDefault();
        $('.answer').hide().removeClass('hide');
        $('#menuitem a').removeClass('selected');
        elem = $(this).attr('href');
        $("html, body").stop(true, true).animate({scrollTop: $(elem).offset().top - top}, 1000);
        $('#menuitem').find("a[href='" + elem + "']").addClass('selected');
    });
});

$(window).on('hashchange', function () {
    changeHash();
});

function changeHash() {
    var id = window.location.hash;
    setTimeout(function () {
        console.log($("a[href='" + id + "']"))
        $("a[href='" + id + "']").trigger('click');
    }, 1000);

//    var id = window.location.hash;
//    $('.answer').hide().removeClass('hide');
//    if ($(id).length) {
//        $('#menuitem a').removeClass('selected');
//        $("html, body").stop(true, true).animate({scrollTop: $(id).offset().top - 90}, 1000);
//
//        $('#menuitem').find("a[href='" + id + "']").addClass('selected');
//    }
}

/** faq question **/
$('.question').click(function () {
    if ($(this).parent('h4').next('.answer').hasClass('hide')) {
        $(this).parent('h4').next('.answer').hide().removeClass('hide');
    } else {
        $('.hide').hide();
        $(this).parent('h4').next('.answer').show().addClass('hide');
    }
});