$(document).ready(function () {
    $(".accordion").accordion();
    var responseObj = null;
    getExistingAppointments();
    $("#appointment_date").prop("disabled", true);
    var MONTH_NAMES = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    var DAY_NAMES = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

    $('.mapIframe').click(function () {
        $(this).find('iframe').addClass('clicked')
    }).mouseleave(function () {
        $(this).find('iframe').removeClass('clicked')
    });

    $('#appointment_form').on('submit', function (e) {
        e.preventDefault();
        $("#error").text("");
        $("#appoitnmentStatus").hide();
        $("#statusError").text("");
        $(".appoitnmentStatusSuccess").hide();

        var appointmentJson = {};
        var datastring = $("#appointment_form").serializeArray();
        $.each(datastring, function () {
            if (appointmentJson[this.name] !== undefined) {
                if (!appointmentJson[this.name].push) {
                    appointmentJson[this.name] = [appointmentJson[this.name]];
                }
                appointmentJson[this.name].push(this.value || '');
            } else {
                appointmentJson[this.name] = this.value || '';
            }
        });

        $("#loadIcon").show();
        $("#btnConfirm").prop("disabled", true);
        $("#name").prop("disabled", true);
        $("#email").prop("disabled", true);
        $("#phone").prop("disabled", true);
        $("#appointment_date").prop("disabled", true);
        $("#special_request").prop("disabled", true);
        $.ajax({
            type: "POST",
            url: baseUrl + "appointment/saveAppointment",
            data: appointmentJson,
            success: function (response) {
                fbq('track', 'ConfirmAppointment');

                $("#loadIcon").hide();
                $("#btnConfirm").prop("disabled", false);
                $("#name").prop("disabled", false);
                $("#email").prop("disabled", false);
                $("#phone").prop("disabled", false);
                $("#appointment_date").prop("disabled", false);
                $("#special_request").prop("disabled", false);
                var responseObj = JSON.parse(response);

                if (responseObj != null && responseObj != undefined) {
                    if (responseObj.status == "error") {
                        $("#error").text(responseObj.message);
                    } else if (responseObj.status == "success") {
                        var tempDateTimeObj = appointmentJson['appointment_date'].split(' ');
                        var tempDate = tempDateTimeObj[0];
                        var tempTime = tempDateTimeObj[1];
                        tempDate = tempDate.split('/');
                        tempTime = tempTime.split(':');
                        var dateObj = new Date(tempDate[2], tempDate[1] - 1, tempDate[0], tempTime[0], tempTime[1], 0, 0);
                        $("#form_div").hide();
                        $("#appoitnmentStatus").show();
                        $(".appoitnmentStatusSuccess").show();
                        // $("#appoint_message").text("Your appointment at 18 West 23rd Street, 4th Floor on " +
                        $("#appoint_message").text("Your appointment at 530 5th Ave on " +
                                DAY_NAMES[dateObj.getDay()] + ", " + MONTH_NAMES[dateObj.getMonth()] + " " + dateObj.getDate() +
                                " at " + tempTime[0] + ":" + tempTime[1] + " has been requested.");
                    } else {
                        $("#form_div").hide();
                        $("#appoitnmentStatus").show();
                        $("#statusError").text("Your appointment has been created successfully");
                    }
                } else {
                    $("#form_div").hide();
                    $("#appoitnmentStatus").show();
                    $("#statusError").text("Couldn't create appointment, Please try again");
                }
            },
            error: function (response) {
                $("#form_div").hide();
                $("#appoitnmentStatus").show();
                $("#statusError").text("Couldn't create appointment, Please try again");
            }
        });
    });


    $('[data-modal-open]').on('click', function (e) {
        $("#error").text("");
        $("#form_div").show();
        $("#appoitnmentStatus").hide();
        $("#statusError").text("");
        $(".appoitnmentStatusSuccess").hide();
        $("#name").val("");
        $("#email").val("");
        $("#phone").val("");
        $("#appointment_date").val("");
        $("#special_request").val("");

        var targeted_modal_class = jQuery(this).attr('data-modal-open');
        $('[data-modal="' + targeted_modal_class + '"]').fadeIn(350);

        e.preventDefault();
    });

    $('[data-modal-close]').on('click', function (e) {
        var targeted_modal_class = jQuery(this).attr('data-modal-close');
        $('[data-modal="' + targeted_modal_class + '"]').fadeOut(350);

        e.preventDefault();
    });
});

var getExistingAppointments = function () {
    $.ajax({
        type: "GET",
        url: baseUrl + "appointment/getAllAppointments",
        success: function (response) {
            $("#appointment_date").prop("disabled", false);
            responseObj = JSON.parse(response);
            $('#appointment_date').datetimepicker({
                format: 'd/m/Y H:i',
                minDate: '-1970/01/01',
                maxDate: '+1970/01/14',
                beforeShowDay: disableDecember,
                onSelectDate: dateSelected,
                onChangeDateTime: setupDateTimePicker,
                onShow: setupDateTimePicker,
                closeOnDateSelect: false,
                closeOnWithoutClick: false,
                timepickerScrollbar: false
            });
        },
        error: function (response) {
            $('#appointment_date').datetimepicker({
                format: 'd/m/Y H:i',
                minDate: '-1970/01/01',
                maxDate: '+1970/01/14',
                beforeShowDay: disableDecember,
                onSelectDate: dateSelected,
                onChangeDateTime: setupDateTimePicker,
                onShow: setupDateTimePicker
            });
        }
    });
}

var disableDecember = function (date) {
    var disabledDates = [
        '01/01/2017', '02/01/2017', '03/01/2017', '04/01/2017', '05/01/2017',
        '06/01/2017', '07/01/2017', '08/01/2017', '09/01/2017', '10/01/2017',
        '11/01/2017', '12/01/2017', '13/01/2017', '14/01/2017', '15/01/2017',
        '16/01/2017', '17/01/2017', '18/01/2017', '19/01/2017', '20/01/2017', '21/01/2017',
        '21/12/2017','22/12/2017','23/12/2017','24/12/2017','25/12/2017','26/12/2017','27/12/2017', '28/12/2017', 
        '31/12/2017', '01/01/2018'
    ];

    var tempDate = date.getDate();
    if (tempDate < 10) {
        tempDate = "0" + tempDate;
    }

    var tempMonth = date.getMonth() + 1;
    if (tempMonth < 10) {
        tempMonth = "0" + tempMonth;
    }

    var dateStr = tempDate + "/" + tempMonth + "/" + date.getFullYear();
    if (disabledDates.indexOf(dateStr) > -1) {
        return [false];
    } else {
        return [true];
    }
};

var dateSelected = function (selectedDateTime) {
    selectedDateTime.setMinutes(00);
    selectedDateTime.setHours(10);
    $("#appointment_date").val(selectedDateTime.dateFormat('d/m/Y H:i'));
};

var compareDates = function (d1, d2) {
    d1.setHours(0, 0, 0, 0);
    d2.setHours(0, 0, 0, 0);
    if (d1.getTime() == d2.getTime()) {
        return true;
    } else {
        return false;
    }
};

var setupDateTimePicker = function (currentDateTime) {
    var allowTimes = [];
    var allowArray = [];
    var todaysDate = new Date();
    if (compareDates(currentDateTime, todaysDate)) {
        todaysDate = new Date();
        var hours = todaysDate.getHours();
        if (hours <= 7) {
            if (currentDateTime.getDay() == 2) {
                allowArray = ['10:00', '11:00', '12:00'];
            } else {
                allowArray = ['10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'];
            }
        } else {
            hours += 3;
            if (currentDateTime.getDay() == 2 && hours <= 13) {
                for (var i = hours; i <= 13; i++) {
                    allowArray.push(i + ":00");
                }
            } else if (hours <= 19) {
                for (var i = hours; i <= 19; i++) {
                    allowArray.push(i + ":00");
                }
            }
        }
    } else {
        if (currentDateTime.getDay() == 2) {
            allowArray = ['10:00', '11:00', '12:00'];
        } else {
            allowArray = ['10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'];
        }
    }

    var dbTime = [];
    for (var i in responseObj) {
        var dbDate = new Date(responseObj[i].appointment_datetime);
        time = (dbDate.getHours() < 10 ? "0" : "") + dbDate.getHours() + ":";
        time += (dbDate.getMinutes() < 10 ? "0" : "") + dbDate.getMinutes();
        if (compareDates(dbDate, currentDateTime)) {
            dbTime.push(time);
        }
    }

    for (var i in allowArray) {
        if (dbTime.indexOf(allowArray[i]) == dbTime.lastIndexOf(allowArray[i])) {
            allowTimes.push(allowArray[i]);
        }
    }

    if (currentDateTime.getDay() == 2) {
        this.setOptions({
            minTime: '10:00',
            maxTime: '13:00',
            allowTimes: allowTimes
        });
    } else {
        this.setOptions({
            minTime: '10:00',
            maxTime: '19:00',
            allowTimes: allowTimes
        });
    }
};